package com.jndv.jndvchatlibrary.web;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.zxing.client.android.MNScanManager;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.google.gson.Gson;
import com.hjq.http.listener.OnDownloadListener;
import com.jndv.jnbaseutils.IntentConstant;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.dowload.DownLoadImageService;
import com.jndv.jnbaseutils.dowload.ImageDownLoadCallBack;
import com.jndv.jnbaseutils.ui.base.JNBaseHeadActivity;
import com.jndv.jnbaseutils.utils.JNFileUtils;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.bean.ChatEntity;
import com.jndv.jndvchatlibrary.bean.CodeEvent;
import com.jndv.jndvchatlibrary.bean.JCWebFileBean;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.VideoPlayActivity;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCTransferActivity;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgImgContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCUriToPathUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCrecordButton;
import com.jndv.jndvchatlibrary.ui.crowd.utils.LogUtil;
import com.jndv.jndvchatlibrary.utils.BitmapUtils;
import com.jndv.jndvchatlibrary.utils.JCFileUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.jndv.jndvchatlibrary.utils.JCmyImagePicker;
import com.jndv.jndvchatlibrary.utils.JCzhihuImagePicker;
import com.jndv.jndvchatlibrary.utils.WebImageUploadUtils;
import com.jndv.jndvchatlibrary.web.util.WebJsObject;
import com.mingyuechunqiu.recordermanager.data.bean.RecordVideoOption;
import com.mingyuechunqiu.recordermanager.data.bean.RecordVideoRequestOption;
import com.mingyuechunqiu.recordermanager.data.bean.RecordVideoResultInfo;
import com.mingyuechunqiu.recordermanager.data.exception.RecorderManagerException;
import com.mingyuechunqiu.recordermanager.feature.record.RecorderManagerProvider;
import com.mingyuechunqiu.recordermanager.framework.RMRecordVideoResultCallback;
import com.qingmei2.rximagepicker_extension.MimeType;
import com.qingmei2.rximagepicker_extension_zhihu.ZhihuConfigurationBuilder;
import com.tencent.smtt.export.external.interfaces.PermissionRequest;
import com.wgd.choosefilelib.ChooseFile;
import com.wgd.choosefilelib.ChooseFileInfo;
import com.wgd.choosefilelib.ChooseFileUIConfig;
import com.wgd.choosefilelib.IFileChooseListener;
import com.qingmei2.rximagepicker.core.RxImagePicker;
import com.qingmei2.rximagepicker.entity.Result;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.tencent.smtt.export.external.interfaces.JsPromptResult;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.export.external.interfaces.WebResourceError;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.sdk.ValueCallback;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public class InfoWebActivity extends JNBaseHeadActivity implements OnClickListener {

    private static final String TAG = "InfoWebActivity";
    private WebView webView;
    private String webUrl;
    private ValueCallback<Uri> mUploadMessage;
    private ValueCallback<Uri[]> fileValueCallback;

    public static final int REQUEST_CHOOSE_PHOTO = 1;// 打开系统图集的请求码
    public static final int REQUEST_TAKE_PHOTO = 2;// 打开系统相机的请求码
    private final int REQUEST_DIALOG_ACTIVITY = 3;// 打开dialogAcitivity
    private static final int REQUEST_CUSTOM_TAKE_PHOTO = 4;// 打开自定义相机请求码

    private int RESULTE_CODE_VIDEO = 10;//选择视频文件

    private String taskCode = "";// 工单号
    private Vibrator vibrator;

    private MediaPlayer mPlayer;

    private String imagePath;// 系统照片地址
    private String customImageUri;// 自定义照片地址

//    Intent intent = new Intent();
    boolean isInit = false;// WebView是否初始化完毕

    ProgressBar progressBar;
    WebImageUploadUtils webImageUtil;

    private TextView title_content;
    private TextView errorText;
    private JCrecordButton audioRecord ;

    private String history_url = null;
//    是否来源于扫码页面
    boolean isFromCamera;
    private int loadNum = 0 ;//加载次数
    private long loadStartTime = 0 ;//开始加载时间
    private int progressBarNum = 0;
    WebJsObject mWebJsObject = null;

    ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(3);

    public static void startActivity(Activity activity, Intent intent){
        if (JNBaseUtilManager.isX5Downing){
            JCChatManager.showToast("内核正在加载，请稍后！");
        }
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean ShowTitle = getIntent().getBooleanExtra("ShowTitle", true);
        setShowTitle(ShowTitle);
        super.onCreate(savedInstanceState);
            if (JNBaseUtilManager.isX5Downing){
                JCChatManager.showToast("内核正在加载，请稍后！");
                finish();
            }
            setContentView(R.layout.activity_info_web);
            webImageUtil = new WebImageUploadUtils(this);
            initTitleView();
            initView();
            initData();
            webView.postDelayed(this::initWebView, 300);
    }
    /**
     * 初始化定时器
     */
    private void initTimerTask() {
        try {
            scheduledThreadPoolExecutor.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (System.currentTimeMillis()-loadStartTime>10*1000){
                            loadStartTime = System.currentTimeMillis();
                            Log.i(TAG, "scheduledThreadPoolExecutor=InfoWeb: "+ webUrl);
                            webView.loadUrl(webUrl);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "scheduledThreadPoolExecutor01: ", e);
                        e.printStackTrace();
                    }
                }
            }, 1000, 1000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            Log.e(TAG, "scheduledThreadPoolExecutor: ", e);
            e.printStackTrace();
        }
    }

    /**
     * 初始化标题栏View
     */
    private void initTitleView() {
        findViewById(R.id.back).setOnClickListener(v -> finish());
        isFromCamera=getIntent().getBooleanExtra(IntentConstant.INTENT_IS_FROM_CAMERA,false);
        String title = getIntent().getStringExtra("WEB_TITLE");
        title_content = findViewById(R.id.title_content);
        title_content.setText(title);
        Log.e(TAG, "initTitleView: " + title);
        findViewById(R.id.tv_pai).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                showActivityDialog();
            }
        });
        //一层层返回
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String now_url= webView.getUrl().trim().toLowerCase();
                Log.e(TAG, "onClick: "+history_url );
                Log.e(TAG, "onClick: "+ now_url);
                if(isFromCamera){
                    finish();
                }else {
                    if (history_url.equals(now_url)) {
                        finish();
                    } else {
                        if (webView.canGoBack()) {
                            webView.goBack();// 返回上一页面
                        } else {
                            finish();
                        }
                    }
                }
            }
        });
    }

    private void initView() {
        webView = findViewById(R.id.new_webView);
        audioRecord = findViewById(R.id.audioRecord);

        errorText = findViewById(R.id.web_error_text);
        progressBar = findViewById(R.id.progressBar1);
        // 提示音初始化
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mPlayer = MediaPlayer.create(InfoWebActivity.this, R.raw.chat_audio);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        mWebJsObject = new WebJsObject(InfoWebActivity.this, webView, audioRecord, new WebJsObject.WebJsListerner() {
            @Override
            public void onOperation(int type, Object... objects) {
                JCThreadManager.onMainHandler(new Runnable() {
                    @Override
                    public void run() {
                        if (0==type){
                            showLoadding();
                        }else if (1==type){
                            cancelLoadding();
                        }else if (2==type){
                            showLoadding();
                            try {
                                JCWebFileBean jcWebFileBean = (JCWebFileBean) objects[0];
//                                https://lmg.jj20.com/up/allimg/1113/031920120534/200319120534-7-1200.jpg
//                                JNFileUtils.downloadFile(InfoWebActivity.this, "https://lmg.jj20.com/up/allimg/1113/031920120534/200319120534-7-1200.jpg", "200319120534-7-1200.jpg", new OnDownloadListener() {
                                JNFileUtils.downloadFile(InfoWebActivity.this, jcWebFileBean.getUrl(), jcWebFileBean.getName(), new OnDownloadListener() {
                                    @Override
                                    public void onStart(File file) {
                                    }
                                    @Override
                                    public void onProgress(File file, int progress) {
                                    }
                                    @Override
                                    public void onComplete(File file) {
                                        JCChatManager.showToast("文件下载成功："+file.getPath());
                                        cancelLoadding();
                                    }
                                    @Override
                                    public void onError(File file, Exception e) {
                                        JCChatManager.showToast("文件下载失败！");
                                        cancelLoadding();
                                    }
                                    @Override
                                    public void onEnd(File file) {
                                        cancelLoadding();
                                    }
                                });
                            }catch (Exception e){
                                e.printStackTrace();
                                cancelLoadding();
                            }
                        }else if (3==type){
                            showSelecteVideoDialog();
                        }else if (4==type){
                            int num = 9 ;
                            try {
                                num = Integer.parseInt((String) objects[0]);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            showSelecteImageDialog(num);
                        }else {
                            cancelLoadding();
                        }
                    }
                });
            }
        });
    }

    private void initData() {
        Intent intent = getIntent();
        String webFlag = intent.getStringExtra("WEB_FLAG");
        webUrl = intent.getStringExtra("WEB_URL");
        if (null==webUrl)webUrl="";
        try {
            webUrl = URLDecoder.decode(webUrl, "utf-8");
        }catch (Exception e){
            e.printStackTrace();
        }
        webUrl = webUrl.replace("/bms-server-api/","/");
        JNLogUtil.e("==webUrl=="+webUrl);
    }

    @SuppressLint("JavascriptInterface")
    private void initWebView() {
        WebView.setWebContentsDebuggingEnabled(true);
        try {
            webView.clearFormData();
            webView.clearHistory();
        }catch (Exception e){
            e.printStackTrace();
        }

        // web配置
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);// 默认关闭，解决地图打开不完整有白块的问题

        // 百度地图加载不出来添加
        settings.setAllowFileAccess(true);
        settings.setBlockNetworkImage(false);
        settings.setBlockNetworkLoads(false);
        settings.setDatabaseEnabled(true);
        settings.setGeolocationEnabled(true);

        String dir = getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        settings.setGeolocationDatabasePath(dir);
        settings.setLoadsImagesAutomatically(true); // 加载图片

//        settings.setSupportZoom(true);// 页面支持缩放
//        settings.setBuiltInZoomControls(true);// 设置显示缩放按钮
        settings.setUseWideViewPort(true); // 设置此属性，可任意比例缩放
        settings.setLoadWithOverviewMode(true); // 自适应屏幕
        // 不使用缓存
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.requestFocusFromTouch();

        // 添加接口
        webView.addJavascriptInterface(mWebJsObject, "webJsObject");
        webView.addJavascriptInterface(this, "Jndv_Main");

        history_url = webUrl.trim().toLowerCase();
        Log.i(TAG, "InfoWeb: "+ webUrl);
        loadStartTime = System.currentTimeMillis();

        webView.setWebViewClient(new WebViewClient() {

            // 覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
//            李蒙 18257143292
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (url == null) return false;

                try {
                    if (url.startsWith("weixin://") //微信
                            || url.startsWith("alipays://") //支付宝
                            || url.startsWith("mailto://") //邮件
                            || url.startsWith("tel://")//电话
                            || url.startsWith("dianping://")//大众点评
                            || url.startsWith("tbopen://")//淘宝
                            || url.startsWith("openapp.jdmobile://")//淘宝
                            || url.startsWith("tmast://")//淘宝

                        //其他自定义的scheme
                    ) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                        startActivity(intent);
                        return true;
                    }
                } catch (Exception e) { //防止crash (如果手机上没有安装处理某个scheme开头的url的APP, 会导致crash)
                    return true;//没有安装该app时，返回true，表示拦截自定义链接，但不跳转，避免弹出上面的错误页面
                }
                Log.e(TAG, "shouldOverrideUrlLoading=: " + url);
                // 返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
//                TODO 2024.08.31 发现警保项目考试系统的登陆页面中会有一次重定向操作，重定向的地址还是登陆页面，所以造成重定向方法出现死循环。这里做兼容处理。
                if (url.contains("pages/public/login")){
                    Log.e(TAG, "shouldOverrideUrlLoading: ====00===");

                }else {
                    Log.e(TAG, "shouldOverrideUrlLoading: ====04===");
                    view.loadUrl(url);
                }
                return true;
            }

//            @Override
//            public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, com.tencent.smtt.export.external.interfaces.SslError sslError) {
//                super.onReceivedSslError(webView, sslErrorHandler, sslError);
//                sslErrorHandler.proceed(); // 接受所有网站的证书
//            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed(); // 接受所有网站的证书,这里支持https
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                // 这个方法在6.0才出现
                String data = "Page NO FOUND！";

                int statusCode = errorResponse.getStatusCode();
                Log.e(TAG, "onReceivedHttpError: "+statusCode );

                if (404 == statusCode || 500 == statusCode) {
//                    view.loadUrl("about:blank");// 避免出现默认的错误界面
//                    view.loadUrl("javascript:document.body.innerHTML=\"" + data + "\"");
                }

            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                // 断网或者网络连接超时
                Log.e(TAG, "onReceivedError: "+error.getErrorCode() );
                String data = "Page NO FOUND！";
                if (error.getErrorCode() == ERROR_HOST_LOOKUP || error.getErrorCode() == ERROR_CONNECT || error.getErrorCode() == ERROR_TIMEOUT) {
//                    view.loadUrl("about:blank"); // 避免出现默认的错误界面
//                    view.loadUrl(mErrorUrl);
//                    view.loadUrl("javascript:document.body.innerHTML=\"" + data + "\"");
                    if (++loadNum<2)webView.loadUrl(webUrl);
                }

            }
        });
        // 调出安卓系统图集
        webView.setWebChromeClient(new MyWebChromeClient());

        isInit = true;
        initTimerTask();
        webView.loadUrl(webUrl);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed: " + webView.canGoBack());
        if(isFromCamera){
            finish();
        }else {
            if (webView.canGoBack()) {
                webView.goBack();// 返回上一页面
            } else {
                finish();
                super.onBackPressed();
            }
        }
    }

    //按back键一层层返回
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.e(TAG, "onKeyDown: " + webView.canGoBack() + "  " + webView.getUrl());
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
//            webView.goBack();
            String now_url= webView.getUrl().trim().toLowerCase();
            if(isFromCamera){
                finish();
            }else{
                if (history_url.equals(now_url)) {
                    finish();
                } else {
                    if (webView.canGoBack()) {
                        webView.goBack();// 返回上一页面
                    } else {
                        finish();
                    }
                }
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        webView.loadUrl("javascript:exitWebView()");
        webView.setVisibility(View.GONE);
        webView.destroy();
    }

    public void shootVideo(){
        Log.e(TAG, "shootVideo: ===========");
        try {
            applyPermissions(new Consumer<Boolean>() {
                                 @SuppressLint("CheckResult")
                                 @Override
                                 public void accept(Boolean aBoolean) {
                                     if (aBoolean){
                                         Log.e(TAG, "selectFile: ==accept=========");
                                         RecordVideoRequestOption option = new RecordVideoRequestOption.Builder()
                                                 .setRecordVideoOption(new RecordVideoOption.Builder()
                                                         .setTimingHint("长按录制")
//                                        .setRecorderOption(recorderOption)
                                                         .build())
                                                 .setMaxDuration(60)
                                                 .build();

                                         RecorderManagerProvider.getRecordVideoRequester().startRecordVideo(InfoWebActivity.this, option, new RMRecordVideoResultCallback() {
                                             @Override
                                             public void onFailure(@NotNull RecorderManagerException e) {
//                                                 JCChatManager.showToast("录制失败，请重试！");
                                             }

                                             @Override
                                             public void onResponseRecordVideoResult(@NonNull RecordVideoResultInfo info) {
                                                 Log.e("JCFilesUpUtils", "onActivityResult: " + info.getDuration() + " " + info.getFilePath());
                                                 //发送小视频
                                                 int time = info.getDuration()/1000;
                                                 if (time<1) {
                                                     JCChatManager.showToast("时间不能少于1秒，请重试！");
                                                 }else {
                                                     mWebJsObject.upFilesAll(info.getFilePath());
                                                 }
                                             }
                                         });
                                         //申请的权限全部允许
                                     }else{
                                         //只要有一个权限被拒绝，就会执行
                                         Toast.makeText(InfoWebActivity.this, "未授权权限，功能不能使用!", Toast.LENGTH_SHORT).show();
                                     }
                                 }
                             }, Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ,Manifest.permission.READ_EXTERNAL_STORAGE
                    ,Manifest.permission.RECORD_AUDIO);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public void web_monitor(String rtspUrl) {
        Log.e(TAG, "web_monitor   rtspUrl: "+rtspUrl);
        if (!TextUtils.isEmpty(rtspUrl)) {
//            startActivity(new Intent(InfoWebActivity.this, VideoPlayActivity.class).putExtra("player_url", rtspUrl));
            startActivity(new Intent(InfoWebActivity.this, VideoPlayActivity.class).putExtra(IntentConstant.WEB_URL, rtspUrl));
        } else {
            Looper.prepare();
//            Toast.makeText(InfoWebActivity.this, "为获取到视频流地址", 0).show();
            Looper.loop();
        }
    }

    @JavascriptInterface
    public void backNine() {
        finish();
    }

    @JavascriptInterface
    public void setWebTitle(String title) {
        Log.e(TAG, "setWebTitle ==title="+title);
        title_content.setText(title);
    }

    @JavascriptInterface
    public void signIn() {
        Log.e(TAG, "执行签到程序 ");
        JNBaseUtilManager.setGpsStatus(0b001, -1, -1);
        JNBaseUtilManager.setGpsFrequency(3000);
        EventBus.getDefault().postSticky(new CodeEvent<>(ChatEntity.CODE_FRAGMENT_SIGNIN));
    }

    @JavascriptInterface
    public void signOut() {
        Log.e(TAG, "执行签退程序 ");
//        JNBaseUtilManager.stopSendGps();
        JNBaseUtilManager.setGpsStatus(0b000, -1, -1);
    }

    Handler handler=new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(msg.what==1){
                Toast.makeText(InfoWebActivity.this, "保存成功", Toast.LENGTH_SHORT).show();
            }else if(msg.what==2){
                Toast.makeText(InfoWebActivity.this, "保存失败", Toast.LENGTH_SHORT).show();
            }
        }
    };

    /**
     * 保存二维码到本地
     * @param imgUrl
     */
    @JavascriptInterface
    public void saveQrcode(String imgUrl){
        DownLoadImageService service = new DownLoadImageService(InfoWebActivity.this, imgUrl,
                new ImageDownLoadCallBack() {

                    @Override
                    public void onDownLoadSuccess(File file) {
                    }
                    @Override
                    public void onDownLoadSuccess(Bitmap bitmap) {
                        // 在这里执行图片保存方法
                        Log.d(TAG,"onDownLoadSuccess");
                        handler.sendEmptyMessage(1);
                    }

                    @Override
                    public void onDownLoadFailed() {
                        // 图片保存失败
                        Log.d(TAG,"onDownLoadFailed");
                        handler.sendEmptyMessage(2);
                    }
                });
        //启动图片下载线程
        new Thread(service).start();
    }

    /**
     * 分享二维码
     * @param imgUrl
     */
    @JavascriptInterface
    public void shareQrcode(String imgUrl){
        DownLoadImageService service = new DownLoadImageService(InfoWebActivity.this, imgUrl,
                new ImageDownLoadCallBack() {

                    @Override
                    public void onDownLoadSuccess(File file) {
                    }
                    @Override
                    public void onDownLoadSuccess(Bitmap bitmap) {
                        // 在这里执行图片保存方法
                        Log.d(TAG,"onDownLoadSuccess");

                        Uri imageUri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(),bitmap , null,null));
                        int whd = 100 ;
                        int whh = 100 ;
                        String type = "jpg";
                        try {
                            String[] wh = JCglideUtils.getBitmapWHFromUri(InfoWebActivity.this, imageUri);
                            type = wh[2];
                            try {
                                whd = Integer.parseInt(wh[0]);
                            }catch (Exception e){e.printStackTrace();}

                            try {
                                whh = Integer.parseInt(wh[1]);
                            }catch (Exception e){e.printStackTrace();}
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        JCMsgImgContentBean msgEntity = new JCMsgImgContentBean(JCUriToPathUtils.getRealPathFromUri(imageUri),whd,whh, type);
                        String contentText = new Gson().toJson(msgEntity);
                        String contentBase64 = Base64.encodeToString(contentText.getBytes(),Base64.NO_WRAP);
                        Intent intent=new Intent(InfoWebActivity.this, JCTransferActivity.class);
                        intent.putExtra("contentText",contentBase64);
                        intent.putExtra("uri",imageUri.toString());
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onDownLoadFailed() {
                        // 图片保存失败
                        Log.d(TAG,"onDownLoadFailed");
                    }
                });
        //启动图片下载线程
        new Thread(service).start();
    }

    /**
     * 定点巡逻上报gps
     */
    @JavascriptInterface
    public void fixedPointOn() {
        Log.e(TAG, "执行定点巡逻上报gps ");
        JNBaseUtilManager.setGpsStatus(-1, -1, 0b100);

    }

    /**
     * 定点巡逻停止上报gps
     */
    @JavascriptInterface
    public void fixedPointOff() {
        Log.e(TAG, "执行定点巡逻停止上报gps ");
        JNBaseUtilManager.setGpsStatus(-1, -1, 0b000);
    }

    class MyWebChromeClient extends WebChromeClient {

        @Override
        public void onPermissionRequest(PermissionRequest permissionRequest) {
            super.onPermissionRequest(permissionRequest);
            // 允许权限请求
            Log.e(TAG, "onPermissionRequest: ====="+new Gson().toJson(permissionRequest.getResources()));
            permissionRequest.grant(permissionRequest.getResources());
        }

        @Override
        public void onCloseWindow(WebView window) {
            super.onCloseWindow(window);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
            return super.onCreateWindow(view, dialog, userGesture, resultMsg);
        }

        /**
         * 覆盖默认的window.alert展示界面，避免title里显示为“：来自file:////”
         */
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {

            AlertDialog.Builder builder = new AlertDialog.Builder(InfoWebActivity.this);

            TextView textView = new TextView(InfoWebActivity.this);
            // textView.setText(getResources().getString(R.string.app_name));
            textView.setText(getString(R.string.app_name));
            textView.setTextSize(30);
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(Color.WHITE);

            builder.setCustomTitle(textView);
            // .setCenterTitle(" "+getResources().getString(R.string.app_name))
            builder.setMessage(message).setPositiveButton("确定", null);

            // 不需要绑定按键事件
            // 屏蔽keycode等于84之类的按键
            builder.setOnKeyListener(new OnKeyListener() {
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    Log.v(TAG, "keyCode==" + keyCode + "event=" + event);
                    return true;
                }
            });
            // 禁止响应按back键的事件
            builder.setCancelable(false);

            AlertDialog dialog = builder.create();

            if (!((Activity) view.getContext()).isFinishing()) {
                dialog.show();
            }
//            dialog.show();
            dialog.getWindow().setGravity(Gravity.CENTER);
            result.confirm();// 因为没有绑定事件，需要强行confirm,否则页面会变黑显示不了内容。
            return true;
            // return super.onJsAlert(view, url, message, result);
        }


        @Override
        public boolean onJsBeforeUnload(WebView view, String url, String message, JsResult result) {
            return super.onJsBeforeUnload(view, url, message, result);
        }

        /**
         * 覆盖默认的window.confirm展示界面，避免title里显示为“：来自file:////”
         */
        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

            TextView textView = new TextView(InfoWebActivity.this);
            textView.setText(getResources().getString(R.string.app_name));
            textView.setTextSize(30);
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(Color.WHITE);

            builder.setCustomTitle(textView);
            builder.setMessage(message).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm();
                }
            }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            });
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    result.cancel();
                }
            });

            // 屏蔽keycode等于84之类的按键，避免按键后导致对话框消息而页面无法再弹出对话框的问题
            builder.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

                    Log.v(TAG, "keyCode==" + keyCode + "event=" + event);
                    return true;
                }
            });
            // 禁止响应按back键的事件
            // builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }

        /**
         * 覆盖默认的window.prompt展示界面，避免title里显示为“：来自file:////”
         * window.prompt('请输入您的域名地址', '618119.com');
         */
        @Override
        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue,
                                  final JsPromptResult result) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            // builder.setCustomTitle(customTitleView)
            TextView textView = new TextView(InfoWebActivity.this);
            textView.setText(getResources().getString(R.string.app_name));
            textView.setTextSize(30);
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(Color.WHITE);

            builder.setCustomTitle(textView);

            final EditText et = new EditText(view.getContext());
            et.setSingleLine();
            et.setText(defaultValue);
            builder.setView(et).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.confirm(et.getText().toString());
                }

            }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    result.cancel();
                }
            });

            // 屏蔽keycode等于84之类的按键，避免按键后导致对话框消息而页面无法再弹出对话框的问题
            builder.setOnKeyListener(new OnKeyListener() {
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

                    Log.v(TAG, "keyCode==" + keyCode + "event=" + event);
                    return true;
                }
            });

            // 禁止响应按back键的事件
            // builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }

        @Override
        public void onReceivedIcon(WebView view, Bitmap icon) {
            super.onReceivedIcon(view, icon);
        }

        @Override
        public void onRequestFocus(WebView view) {
            super.onRequestFocus(view);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            // 网页加载进度条
            progressBar.setProgress(newProgress);
            loadStartTime = System.currentTimeMillis();
            if (newProgress == 100) {
                progressBar.setVisibility(View.GONE);
                try {
                    scheduledThreadPoolExecutor.shutdown();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
            Log.e(TAG, "openFileChooser: ===acceptType="+acceptType);
            Log.e(TAG, "openFileChooser: ===capture="+capture);
            mUploadMessage = uploadMsg;
            showActivityDialog(2);
        }

        @Override
        public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            Log.e(TAG, "onShowFileChooser 执行");
            String[] chooserParamsAcceptTypes = fileChooserParams.getAcceptTypes();
            boolean mimeTypeIm = false ;
            boolean mimeTypeVi = false ;
            for (String params : chooserParamsAcceptTypes) {
                Log.e(TAG, "onShowFileChooser 执行==params="+params);
//                image/*
                if (params.contains("image")) {
                    mimeTypeIm = true;
                    break;
                }else if (params.contains("video")) {
                    mimeTypeVi = true;
                    break;
                }
            }
            if (!mimeTypeIm&&!mimeTypeVi){
                fileValueCallback = filePathCallback;
                selectFile();
            }else if (mimeTypeIm&&!mimeTypeVi){
                fileValueCallback = filePathCallback;
                showActivityDialog(0);
            }else if (!mimeTypeIm&&mimeTypeVi){
                fileValueCallback = filePathCallback;
                selecteVideoPhoto();
            }else if (mimeTypeIm&&mimeTypeVi){
                fileValueCallback = filePathCallback;
                showActivityDialog(2);
            }
            return true;
        }
    }

    private void showSelecteVideoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("请选择视频").setPositiveButton("相册", (dialog, which) -> {
            dialog.dismiss();
            selecteVideoPhoto();
        }).setNegativeButton("拍摄", (dialog, which) -> {
            dialog.dismiss();
            shootVideo();
        }).setOnCancelListener(dialog -> {
            LogUtil.e("1116", "执行取消");
            cancelFilePathCallback();
        }).show();
    }

    private void selecteVideoPhoto() {
        try {
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, RESULTE_CODE_VIDEO);
//            Intent intent1 = new Intent();
//            intent1.setType("video/*");
//            intent1.setAction(Intent.ACTION_GET_CONTENT);
//            startActivityForResult(Intent.createChooser(intent1, "选择视频"), RESULTE_CODE_VIDEO);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showActivityDialog(int mimeType) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("请选择照片").setPositiveButton("相册", (dialog, which) -> {
            dialog.dismiss();
            takePhotoGallery(mimeType, 9);
        }).setNegativeButton("拍照", (dialog, which) -> {
            dialog.dismiss();
            takePhoto();
        }).setOnCancelListener(dialog -> {
            LogUtil.e("1116", "执行取消");
            cancelFilePathCallback();
        }).show();
    }
    private void showSelecteImageDialog(int num) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("请选择照片").setPositiveButton("相册", (dialog, which) -> {
            dialog.dismiss();
            takePhotoGallery(0, num);
        }).setNegativeButton("拍照", (dialog, which) -> {
            dialog.dismiss();
            takePhoto();
        }).setOnCancelListener(dialog -> {
            LogUtil.e("1116", "执行取消");
            cancelFilePathCallback();
        }).show();
    }

    /**
     *
     * @param mimeType 0=图片；1=视频；2=图片加视频
     */
    private void takePhotoGallery(int mimeType, int num) {
        Log.e(TAG, "takePhotoGallery: ====mimeType=="+mimeType);
        RxPermissions rxPermissions = new RxPermissions(InfoWebActivity.this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
        )
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean) {
                            //申请的权限全部允许
                            RxImagePicker
//                                    .create(JCmyImagePicker.class)
//                                    .openGallery(InfoWebActivity.this)
                                    .create(JCzhihuImagePicker.class)
                                    .openGalleryAsNormal(InfoWebActivity.this,
                                            new ZhihuConfigurationBuilder(
                                                    0==mimeType?MimeType.INSTANCE.ofImage():
                                                            1==mimeType?MimeType.INSTANCE.ofVideo()
                                                                    :MimeType.INSTANCE.ofAll()
                                                    , false)
                                                    .maxSelectable(num)
                                                    .spanCount(4)
                                                    .countable(false)
                                                    .build())             //打开微信相册选取图片
                                    .subscribe(new Consumer<Result>() {
                                        @Override
                                        public void accept(Result result) {
                                            Uri uri = result.getUri();
                                            Log.e(TAG, "=====01===null======");
                                            if (mUploadMessage != null) {
//                                                if (Build.BRAND.toLowerCase().equals("xiaomi")) {
//                                                    fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(uri, 90)});
//                                                } else {
                                                    fileValueCallback.onReceiveValue(new Uri[]{uri});
//                                                }
                                            } else if (fileValueCallback != null) {
//                                                if (Build.BRAND.toLowerCase().equals("xiaomi")) {
//                                                    fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(uri, 90)});
//                                                } else {
                                                    fileValueCallback.onReceiveValue(new Uri[]{uri});
//                                                }
                                            }else {
//                                                mWebJsObject.upFilesAll(uri.getPath());
                                                String path = JCUriToPathUtils.getRealPathFromUri(uri);
//                                                String path = webImageUtil.getPath(Uri.fromFile(new File(imagePath)));
//                                                int degree = BitmapUtils.readPictureDegree(path);
//                                                Uri compressUri = webImageUtil.getimage(path);
//                                                compressUri = compressUri != null ? compressUri : Uri.fromFile(new File(imagePath));

                                                //通过获取图片旋转角度调整图片不正问题
//                                                LogUtil.e(TAG, "onActivityResult: degree：" + degree);
//                                                if (degree == 90) {
//                                                    compressUri = xiaomiRotaingImageView(compressUri, 90);
//                                                } else if (degree == 180) {
//                                                    compressUri = xiaomiRotaingImageView(compressUri, 180);
//                                                } else if (degree == 270) {
//                                                    compressUri = xiaomiRotaingImageView(compressUri, 270);
//                                                }
//                                                path = JCUriToPathUtils.getRealPathFromUri(uri);
                                                if (null!=mWebJsObject)mWebJsObject.upFilesAll(path);
                                            }
                                            Log.e(TAG, "========null======");
                                            // 取消选择框后可以再次弹出
                                            mUploadMessage = null;
                                            fileValueCallback = null;
                                        }
                                    }, new Consumer<Throwable>() {
                                        @Override
                                        public void accept(Throwable throwable) throws Exception {
                                            JNLogUtil.e(TAG, "=====Throwable===null======", throwable);
                                        }
                                    }, new Action() {
                                        @Override
                                        public void run() throws Exception {
                                            JNLogUtil.e(TAG, "=====Action===null======");
                                            // 取消选择框后可以再次弹出
                                            cancelFilePathCallback();
                                        }
                                    });
                        } else {
                            //只要有一个权限被拒绝，就会执行
                            JCChatManager.showToast("未授权权限，部分功能不能使用");
                        }
                    }
                });
    }

    private void applyPermissions(Consumer<Boolean> consumer, String... permissions){
        JCThreadManager.onMainHandler(new Runnable() {
            @Override
            public void run() {
                try {
                    RxPermissions rxPermissions=new RxPermissions(InfoWebActivity.this);
                    rxPermissions.request(permissions)
                            .subscribe(consumer);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
    public void selectFile(){
        Log.e(TAG, "selectFile: ===========");
        try {
            applyPermissions(new Consumer<Boolean>() {
                                 @SuppressLint("CheckResult")
                                 @Override
                                 public void accept(Boolean aBoolean) {
                                     if (aBoolean){
                                         Log.e(TAG, "selectFile: ==accept=========");
//                                        JCThreadManager.onMainHandler(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                         if (null!=types&&types.length>0){
//                                             WGDPickerManager.getInstance().setmFileTypesAllTemporary(types);
//                                         }

/*
                                         Log.e(TAG, "accept: ====" + new Gson().toJson(WGDPickerManager.getInstance().getmFileTypesAll()));
                                         FilePickerAllPagActivity.start(InfoWebActivity.this, "文件选择", new BaseFragment.FragmentSelect() {
                                             @Override
                                             public void onSelecte(int type, List<FileEntity> files, Object... objects) {
                                                 Log.e(TAG, "onSelecte: ==type==" +type);
                                                 WGDPickerManager.getInstance().setmFileTypesAllTemporary(null);
                                                 if (type == BaseFragment.SELECTE_FILE_RESULT && null!=files&&files.size()>0) {
                                                     for (int i = 0; i < files.size(); i++) {
                                                         FileEntity fileEntity = files.get(i);
                                                         Log.e(TAG, "onSelecte: ==getPath==" +fileEntity.getPath() );
//                                                         upFilesAll(fileEntity.getPath());
                                                         fileValueCallback.onReceiveValue(new Uri[]{Uri.fromFile(fileEntity.getFile())});
//                                                                webView.evaluateJavascript("uploadReturnAddress("+new Uri[]{Uri.fromFile(fileEntity.getFile())}+")", null);
//                                                         webView.evaluateJavascript("javascript:uploadReturnAddress(\"g\")", null);
//                                                         webView.evaluateJavascript("javascript:uploadReturnAddress(\""+fileEntity.getPath()+"\")", null);
                                                         break;
                                                     }
                                                 }
                                                     cancelFilePathCallback();
                                             }
                                         });*/

                                         ChooseFile.create(InfoWebActivity.this).setUIConfig(new ChooseFileUIConfig.Builder().build())
                                                 .forResult(new IFileChooseListener() {
                                                     @Override
                                                     public void doChoose(@Nullable ChooseFileInfo chooseFileInfo) {
                                                         Log.e("0531", "doChoose: ==============" + chooseFileInfo.filePath);
                                                         try {
                                                            fileValueCallback.onReceiveValue(new Uri[]{Uri.fromFile(new File(chooseFileInfo.filePath))});
                                                         }catch (Exception e){
                                                             e.printStackTrace();
                                                         }
                                                     }
                                                 });
//                                            }
//                                        });
                                         //申请的权限全部允许
                                     }else{
                                         cancelFilePathCallback();
                                         //只要有一个权限被拒绝，就会执行
                                         Toast.makeText(InfoWebActivity.this, "未授权权限，功能不能使用!", Toast.LENGTH_SHORT).show();
                                     }
                                 }
                             }, Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ,Manifest.permission.READ_EXTERNAL_STORAGE);
        }catch (Exception e){
            e.printStackTrace();
            cancelFilePathCallback();
        }
    }

    private void takePhoto() {
        RxPermissions rxPermissions=new RxPermissions(InfoWebActivity.this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                ,Manifest.permission.READ_EXTERNAL_STORAGE
                ,Manifest.permission.CAMERA
        )
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean){
                            //申请的权限全部允许
                            RxImagePicker
                                    .create(JCmyImagePicker.class)
                                    .openCamera(InfoWebActivity.this)
//                                    .openCamera(JCChatManager.mContext)
                                    .subscribe(new Consumer<Result>() {
                                        @Override
                                        public void accept(Result result) {
                                            Uri uri = result.getUri();
                                            Log.e(TAG, uri.toString());
                                            imagePath = JCUriToPathUtils.getRealPathFromUri(uri);
                                            if (mUploadMessage != null) {
                                                    String path = webImageUtil.getPath(Uri.fromFile(new File(imagePath)));
                                                    int degree = BitmapUtils.readPictureDegree(path);
                                                    LogUtil.e(TAG, "1onActivityResult: " + path);
                                                    Uri compressUri = webImageUtil.getimage(path);
                                                    compressUri = compressUri != null ? compressUri : Uri.fromFile(new File(imagePath));

//                                                    这里是不是应该使用mUploadMessage
                                                    //通过获取图片旋转角度调整图片不正问题
                                                    if (degree == 0) {
                                                        fileValueCallback.onReceiveValue(new Uri[]{compressUri});
                                                    } else if (degree == 90) {
                                                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 90)});
                                                    } else if (degree == 180) {
                                                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 180)});
                                                    } else if (degree == 270) {
                                                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 270)});
                                                    } else {
                                                        fileValueCallback.onReceiveValue(new Uri[]{compressUri});
                                                    }

                                            } else if (fileValueCallback != null) {

                                                    String path = webImageUtil.getPath(Uri.fromFile(new File(imagePath)));
                                                    int degree = BitmapUtils.readPictureDegree(path);
                                                    Uri compressUri = webImageUtil.getimage(path);
                                                    compressUri = compressUri != null ? compressUri : Uri.fromFile(new File(imagePath));

                                                    //通过获取图片旋转角度调整图片不正问题
                                                    LogUtil.e(TAG, "onActivityResult: degree：" + degree);
                                                    if (degree == 0) {
                                                        fileValueCallback.onReceiveValue(new Uri[]{compressUri});
                                                    } else if (degree == 90) {
                                                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 90)});
                                                    } else if (degree == 180) {
                                                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 180)});
                                                    } else if (degree == 270) {
                                                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 270)});
                                                    } else {
                                                        fileValueCallback.onReceiveValue(new Uri[]{compressUri});
                                                    }
                                            }else {
                                                String path = webImageUtil.getPath(Uri.fromFile(new File(imagePath)));
                                                int degree = BitmapUtils.readPictureDegree(path);
                                                Uri compressUri = uri;
//                                                compressUri = compressUri != null ? compressUri : Uri.fromFile(new File(imagePath));

                                                try {
                                                    //通过获取图片旋转角度调整图片不正问题
                                                    LogUtil.e(TAG, "onActivityResult: degree：" + degree);
                                                    if (degree == 90) {
                                                        compressUri = xiaomiRotaingImageView(compressUri, 90);
                                                    } else if (degree == 180) {
                                                        compressUri = xiaomiRotaingImageView(compressUri, 180);
                                                    } else if (degree == 270) {
                                                        compressUri = xiaomiRotaingImageView(compressUri, 270);
                                                    }
                                                    path = JCUriToPathUtils.getRealPathFromUri(compressUri);
                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                }
                                                if (null!=mWebJsObject)mWebJsObject.upFilesAll(path);
                                            }
                                            // 取消选择框后可以再次弹出
                                            mUploadMessage = null;
                                            fileValueCallback = null;

                                            Log.e(TAG, "========null======");
                                        }
                                    }, new Consumer<Throwable>() {
                                        @Override
                                        public void accept(Throwable throwable) throws Exception {
                                            JNLogUtil.e(TAG, "=====Throwable===null======", throwable);
                                        }
                                    }, new Action() {
                                        @Override
                                        public void run() throws Exception {
                                            JNLogUtil.e(TAG, "=====Action===null======");
                                            // 取消选择框后可以再次弹出
                                            cancelFilePathCallback();
                                        }
                                    });
                        }else{
                            //只要有一个权限被拒绝，就会执行
                            JCChatManager.showToast("未授权权限，部分功能不能使用");
                        }
                    }
                });

    }

    private void cancelFilePathCallback() {
        try {
            if (mUploadMessage != null) {
                mUploadMessage.onReceiveValue(null);
                mUploadMessage = null;
            }
        }catch (Exception e){
            e.printStackTrace();
            mUploadMessage = null;
        }
        try {
            if (fileValueCallback != null) {
                fileValueCallback.onReceiveValue(null);
                fileValueCallback = null;
            }
        }catch (Exception e){
            e.printStackTrace();
            fileValueCallback = null;
        }

    }


    //解决小米手机从相册选择照片偏转90度的问题
    private Uri xiaomiRotaingImageView(Uri uri, int orientationDegree) {
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inSampleSize = 2;

//        Bitmap img = BitmapFactory.decodeFile(path,options);
        Bitmap img = null;
        try {
            img = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Matrix m = new Matrix();

        try {
            m.setRotate(orientationDegree, (float) img.getWidth() / 2, (float) img.getHeight() / 2);
            Bitmap bm1 = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), m, true);
            Uri newuri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), bm1, null, null));
//            Log.e(TAG, "xiaomiRotaingImageView: "+newuri );
            return newuri;
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Log.e(TAG, "xiaomiRotaingImageView: uri "+uri );
        return uri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        LogUtil.e(TAG, "onActivityResult: requestCode=" + requestCode);
        LogUtil.e(TAG, "onActivityResult: resultCode=" + resultCode);
        // 请求系统图集
        if (requestCode == REQUEST_CHOOSE_PHOTO) {
            if (mUploadMessage != null) {
                if (resultCode == RESULT_OK) {
                    LogUtil.e(TAG, "onActivityResult: " + intent.getData());
                    String path = webImageUtil.getPath(intent.getData());
                    Uri compressUri = webImageUtil.getimage(path);
                    if (compressUri == null) {
                        return;
                    }
                    if (compressUri != null) {
                        mUploadMessage.onReceiveValue(compressUri);
                    } else {
                        mUploadMessage.onReceiveValue(intent.getData());
                    }
                } else {
                    mUploadMessage.onReceiveValue(null);
                }
            } else if (fileValueCallback != null) {
                if (resultCode == RESULT_OK) {
                    if (Build.BRAND.toLowerCase().equals("xiaomi")) {
                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(intent.getData(), 90)});
                    } else {
                        fileValueCallback.onReceiveValue(new Uri[]{intent.getData()});
                    }
//                    Log.e(TAG, "onActivityResult2: "+intent.getData()+"-----"+ xiaomiRotaingImageView(intent.getData(),90) );
                } else {
                    fileValueCallback.onReceiveValue(null);
                }
            }
            // 取消选择框后可以再次弹出
            mUploadMessage = null;
            fileValueCallback = null;
        }
        // 请求系统相机
        if (requestCode == REQUEST_TAKE_PHOTO) {
            LogUtil.e("1116", "拍照 返回结果");

            if (mUploadMessage != null) {
                if (resultCode == RESULT_OK) {
                    String path = webImageUtil.getPath(Uri.fromFile(new File(imagePath)));
                    int degree = BitmapUtils.readPictureDegree(path);
                    LogUtil.e(TAG, "1onActivityResult: " + path);
                    Uri compressUri = webImageUtil.getimage(path);
                    compressUri = compressUri != null ? compressUri : Uri.fromFile(new File(imagePath));

//                    LogUtil.d("!!", "=======" + Uri.fromFile(new File(imagePath)));
//                    Log.e("!!!", "======path=" + path);

                    //通过获取图片旋转角度调整图片不正问题
                    if (degree == 0) {
                        fileValueCallback.onReceiveValue(new Uri[]{compressUri});
                    } else if (degree == 90) {
                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 90)});
                    } else if (degree == 180) {
                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 180)});
                    } else if (degree == 270) {
                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 270)});
                    } else {
                        fileValueCallback.onReceiveValue(new Uri[]{compressUri});
                    }
                } else {
                    mUploadMessage.onReceiveValue(null);
                }

            } else if (fileValueCallback != null) {
                if (resultCode == RESULT_OK) {
                    String path = webImageUtil.getPath(Uri.fromFile(new File(imagePath)));
                    int degree = BitmapUtils.readPictureDegree(path);
                    Uri compressUri = webImageUtil.getimage(path);
                    compressUri = compressUri != null ? compressUri : Uri.fromFile(new File(imagePath));

//                    LogUtil.d("", "======ss1=" + Uri.fromFile(new File(imagePath)));
//                    LogUtil.d("", "======ss2=" + compressUri);
//                    Log.e("!!!", "======path=" + path);
//                    Log.e("!!!", "======parentPath=" + path.substring(path.lastIndexOf("/")));
                    //通过获取图片旋转角度调整图片不正问题
                    LogUtil.e(TAG, "onActivityResult: degree：" + degree);
                    if (degree == 0) {
                        fileValueCallback.onReceiveValue(new Uri[]{compressUri});
                    } else if (degree == 90) {
                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 90)});
                    } else if (degree == 180) {
                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 180)});
                    } else if (degree == 270) {
                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 270)});
                    } else {
                        fileValueCallback.onReceiveValue(new Uri[]{compressUri});
                    }

//                    if (Build.BRAND.toLowerCase().equals("xiaomi")) {
//                        Log.e(TAG, "onActivityResult2: " + Build.BRAND);
////
//                    } else if (Build.BRAND.toLowerCase().equals("samsung")) {
//                        fileValueCallback.onReceiveValue(new Uri[]{xiaomiRotaingImageView(compressUri, 90)});
//                    } else {
//                        fileValueCallback.onReceiveValue(new Uri[]{compressUri});
//                    }


                } else {
                    fileValueCallback.onReceiveValue(null);
                }
            }
            // 取消选择框后可以再次弹出
            mUploadMessage = null;
            fileValueCallback = null;
        }
        // 请求自定义相机
        if (requestCode == REQUEST_CUSTOM_TAKE_PHOTO) {
            LogUtil.e("1116", "自定义相机-拍照 返回结果");

            if (mUploadMessage != null) {
                if (resultCode == RESULT_OK) {
//                    customImageUri = intent.getStringExtra("photo_path");
                    List<String> pathList = (List<String>) intent.getExtras().getSerializable("photo_path");
                    LogUtil.e(TAG, "onActivityResult11: " + pathList.size());
                    for (String customImageUri : pathList) {
                        LogUtil.e(TAG, "onActivityResult: customImageUri" + customImageUri);
                        String path = webImageUtil.getPath(Uri.fromFile(new File(customImageUri)));
                        LogUtil.e(TAG, "1onActivityResult: path" + path);
                        Uri compressUri = webImageUtil.getimage(path);
                        compressUri = compressUri != null ? compressUri : Uri.fromFile(new File(customImageUri));

//                    LogUtil.d("!!", "=======" + Uri.fromFile(new File(imagePath)));
//                    Log.e("!!!", "======path=" + path);
                        mUploadMessage.onReceiveValue(compressUri);
                    }
                } else {
                    mUploadMessage.onReceiveValue(null);
                }

            } else if (fileValueCallback != null) {
                if (resultCode == RESULT_OK) {
//                    customImageUri = intent.getStringExtra("photo_path");
                    List<String> pathList = (List<String>) intent.getExtras().getSerializable("photo_path");
                    LogUtil.e(TAG, "onActivityResult22: " + pathList.size());
                    for (String customImageUri : pathList) {
                        LogUtil.e(TAG, "onActivityResult: customImageUri" + customImageUri);
                    }
                    String path = webImageUtil.getPath(Uri.fromFile(new File(pathList.get(0))));
                    LogUtil.e(TAG, "2onActivityResult: " + path);
                    Uri compressUri = webImageUtil.getimage(path);
                    compressUri = compressUri != null ? compressUri : Uri.fromFile(new File(pathList.get(0)));
//                    LogUtil.d("", "======ss1=" + Uri.fromFile(new File(imagePath)));
                    LogUtil.d("1116", "======ss2=" + compressUri);
//                    Log.e("!!!", "======path=" + path);
//                    Log.e("!!!", "======parentPath=" + path.substring(path.lastIndexOf("/")));
                    fileValueCallback.onReceiveValue(new Uri[]{compressUri});

                } else {
                    fileValueCallback.onReceiveValue(null);
                }
            }
            // 取消选择框后可以再次弹出
            mUploadMessage = null;
            fileValueCallback = null;
        }

        if (requestCode == 110 && intent != null && resultCode == 200) {
            LogUtil.e("1116", "会执行吗1");
            String filePath = intent.getStringExtra("filePath");
            String res = intent.getStringExtra("result");
            LogUtil.d("1116", "======图片地址" + filePath);
            if (res != null) {
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                ByteArrayOutputStream baos;
                String base64 = "";
                if (bitmap != null) {
                    baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                    byte[] bitmapBytes = baos.toByteArray();
                    base64 = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
                }
                String loadWebUrl = "javascript:returnJson(" + res + ",\"" + base64 + "\")";
                webView.loadUrl(loadWebUrl);
                LogUtil.d("", "======" + loadWebUrl);
            }
        }
        if (requestCode == 120 && intent != null && resultCode == 200) {
            LogUtil.e("1116", "会执行吗2");
            try {
                String filePath = intent.getStringExtra("filePath");
                LogUtil.d("1116", "======图片地址" + filePath);
                String res = intent.getStringExtra("result");
                LogUtil.d("1116", "======识别信息" + res);
                // jpg
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);

                ByteArrayOutputStream baos;
                String base64 = "";
                if (bitmap != null) {
                    baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                    byte[] bitmapBytes = baos.toByteArray();
                    // data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB
                    // data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB
                    base64 = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
                    // LogUtil.d("","=======base64==="+base64.length()+" "+base64);
                }
                String loadWebUrl = "javascript:returnsfzJson(" + res + ",\"" + base64 + "\")";
                webView.loadUrl(loadWebUrl);
                LogUtil.d("1116", "======" + loadWebUrl);
                // LogUtil.i("","=======le_100"+"
                // "+loadWebUrl.substring(loadWebUrl.length()-100,loadWebUrl.length()));
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (requestCode == 140 && intent != null) {
            String url = intent.getStringExtra("url");
            String title = intent.getStringExtra("title");
            title_content.setText(title);
            webView.loadUrl(url);
        }
        if (requestCode == 150 && intent != null) {
            String str = intent.getStringExtra(MNScanManager.INTENT_KEY_RESULT_SUCCESS);
            ResultBean resultBean = new ResultBean("扫码回调", str);
            if (null!=mWebJsObject)mWebJsObject.appAction(new Gson().toJson(resultBean));
        }
        Log.e(TAG, "onActivityResult: ===000=");
        if (requestCode == RESULTE_CODE_VIDEO ) {
            Log.e(TAG, "onActivityResult: ===001=");
            if (resultCode == RESULT_OK && intent != null) {
                Uri video = intent.getData();
                showLoadding();
                JCThreadManager.getFile().execute(new Runnable() {
                    @Override
                    public void run() {
                        String path = JCFileUtils.getFilePathByUri(InfoWebActivity.this, video);
                        Log.e(TAG, "onActivityResult: ===002="+path);
                        JCThreadManager.onMainHandler(new Runnable() {
                            @Override
                            public void run() {
                                //                "mp4","avi","3gp"
//                if (path.endsWith("mp4") || path.endsWith("avi") || path.endsWith("3gp")){
                                mWebJsObject.upFilesAll(path);
//                }else {
//                    JCChatManager.showToast("文件类型错误！");
//                }
                            }
                        });
                    }
                });
            }
        }

    }


}

package com.jndv.jndvchatlibrary.web;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;

import com.wega.library.loadingDialog.LoadingDialog;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import java.io.File;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;

public class PDFViewActivity extends JCbaseFragmentActivity {

    String url;
    PDFView pdfView;
    private LoadingDialog loadingDialog = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_pdf);
        loadingDialog = new LoadingDialog(this);
        url=getIntent().getStringExtra("WEB_URL");
        Log.d("zhang","url=="+url);

        String fileName=url.substring(url.lastIndexOf("/")+1);
        String a = fileName;
        if (fileName.toLowerCase(Locale.ROOT).endsWith("pdf")){
            a = fileName.substring(0, fileName.lastIndexOf("."));
        }
        setCenterTitle(a);

        showMenuClick(false);
        pdfView=findViewById(R.id.pdfView);
        List<String> dataList = JNBasePreferenceSaves.getDataList(JNBasePreferenceSaves.KEY_PDF_FILE_NAME);
        if(dataList.contains(fileName)){
            pdfView.fromFile(new File(getExternalFilesDir(null).getAbsolutePath(),fileName))
                    .enableAnnotationRendering(true)
                    .scrollHandle(new DefaultScrollHandle(this))
                    .spacing(10) // in dp
                    .load();
        }else {
            downloadFile(fileName);
        }
    }

    private void downloadFile(String fileName){
        showLoadding();
        OkHttpUtils//
                .get()//
                .url(url)//
                .build()//
                .execute(new FileCallBack(getExternalFilesDir(null).getAbsolutePath(), fileName)//
                {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("zhang","onError==="+e);
                        cancelLoadding();
                        JCChatManager.showToast("加载失败");
                    }

                    @Override
                    public void onResponse(File response, int id) {
                        Log.d("zhang","onResponse==="+response);
                        cancelLoadding();
                        List<String> dataList = JNBasePreferenceSaves.getDataList(JNBasePreferenceSaves.KEY_PDF_FILE_NAME);
                        dataList.add(fileName);
                        JNBasePreferenceSaves.setDataList(JNBasePreferenceSaves.KEY_PDF_FILE_NAME,dataList);
                        pdfView.fromFile(new File(getExternalFilesDir(null).getAbsolutePath(),fileName))
                                .enableAnnotationRendering(true)
                                .scrollHandle(new DefaultScrollHandle(PDFViewActivity.this))
                                .spacing(10) // in dp
                                .load();
                    }
                });
    }
}

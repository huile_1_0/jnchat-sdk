package com.jndv.jndvchatlibrary.web.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;

import static android.content.Context.NOTIFICATION_SERVICE;


public class Notificator {
    private static final String TAG = "Notificator";
    public static long lastNotificatorTime = 0;
    public static long currentRmoteID = 0;

    public static void updateSystemNotification(Context context, String title,
                                                String content, boolean IsTone, Intent trigger, int notificationID,
                                                long remoteID) {
        currentRmoteID = remoteID;
        if (IsTone
                && ((System.currentTimeMillis() / 1000) - lastNotificatorTime) > 2) {
            // Uri notification = RingtoneManager
            // .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            // Ringtone r = RingtoneManager.getRingtone(context, notification);
            // r.play();
            // lastNotificatorTime = System.currentTimeMillis() / 1000;
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title).setContentText(content);

        // Creates the PendingIntent
        PendingIntent notifyPendingIntent = PendingIntent.getActivities(
                context, 0, new Intent[]{trigger},
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Puts the PendingIntent into the notification builder
        builder.setContentIntent(notifyPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(NOTIFICATION_SERVICE);

        Notification build = builder.build();
        build.flags = Notification.FLAG_AUTO_CANCEL;
        // mId allows you to update the notification later on.
        mNotificationManager.cancelAll();
        mNotificationManager.notify(notificationID, build);
    }

    public static void cancelSystemNotification(Context context, int nId) {
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.cancel(nId);
    }

    public static void cancelAllSystemNotification(Context context) {
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
    }

    public static void udpateApplicationNotification(Context context,
                                                     boolean flag, Intent intent) {
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(NOTIFICATION_SERVICE);
        if (flag) {
            PendingIntent notifyPendingIntent = PendingIntent.getActivities(
                    context, 0, new Intent[]{intent},
                    PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(
                    context).setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getText(R.string.app_name))
                    .setContentText(context.getText(R.string.status_bar_title))
                    .setAutoCancel(false).setContentIntent(notifyPendingIntent);
            Notification noti = builder.build();
            noti.flags |= Notification.FLAG_NO_CLEAR;

            mNotificationManager.notify(
                    PublicIntent.APPLICATION_STATUS_BAR_NOTIFICATION, noti);
        } else {
            mNotificationManager
                    .cancel(PublicIntent.APPLICATION_STATUS_BAR_NOTIFICATION);

        }
    }

    public static void closeNotification(Context context) {
        try {
            NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                manager.deleteNotificationChannel(""+GlobalConstant.WEBMSG_NOTIFICATION);
            }
            manager.cancel(GlobalConstant.WEBMSG_NOTIFICATION);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void udpateNotification(Context context, String content, Intent intent) {
        Log.e(TAG, "udpateNotification: ==confirmCallBack==02===" );
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,  PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.e(TAG, "udpateNotification: ==confirmCallBack==03===" );
            NotificationManager manager=null;
            manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel channel=new NotificationChannel(GlobalConstant.WEBMSG_NOTIFICATION+"","NAME", NotificationManager.IMPORTANCE_HIGH);

            //设置通知出现时的闪光灯
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
//设置通知出现时的震动
            channel.enableVibration(true);

            manager.createNotificationChannel(channel);

            Notification notify = new Notification.Builder(context,GlobalConstant.WEBMSG_NOTIFICATION+"")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("您有一条新消息")
                    .setContentText(content)
                    .setTicker(content)
                    .setWhen(System.currentTimeMillis())
                    .setNumber(1)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setOngoing(true)
                    .build();
            // 通过通知管理器来发起通知。如果id不同，则每click，在statu那里增加一个提示
            manager.notify(GlobalConstant.WEBMSG_NOTIFICATION, notify);
        }else {
            Log.e(TAG, "udpateNotification: ==confirmCallBack==04===" );
            NotificationManager notificationManager;
            notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            Notification notification = null;
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                    .setContentTitle("您有一条新消息")
                    .setContentText(content)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setContentIntent(pendingIntent)//设置意图
                    .setAutoCancel(true)
                    .setOngoing(true);
            notification = notificationBuilder.build();
            notificationManager.notify(GlobalConstant.WEBMSG_NOTIFICATION, notification);
        }
    }
}

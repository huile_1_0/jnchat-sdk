package com.jndv.jndvchatlibrary.web.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNSaveBusinessApi;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipP2PActivity;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipVideoActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.UUID;

import io.reactivex.functions.Consumer;

public class WebCallUtils {
    private static final String TAG = "WebCallUtils";

    public interface CallBackListener{
        void onCall(int type, Object... objects);
    }
    public CallBackListener mCallBackListener ;

    public void setCallBackListener(CallBackListener callBackListener) {
        this.mCallBackListener = callBackListener;
    }

    /**
     * 创建通话：音频通话；视频通话；
     * @param activity
     * @param type 0=语音；1=视频
     * @param sip
     */
    public static void createCall(FragmentActivity activity, String type, String sip, String businessId, String busdata, String calluuid, CallBackListener callBackListener){
        Log.e(TAG, "createCall: =webToAAction=====type="+type+"===sip="+sip);
        RxPermissions rxPermissions=new RxPermissions(activity);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ,Manifest.permission.READ_EXTERNAL_STORAGE
                        ,Manifest.permission.RECORD_AUDIO)
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean) {
                            //申请的权限全部允许
                            String mySipNumber = JNBasePreferenceSaves.getUserSipId();
                            if (TextUtils.isEmpty(mySipNumber)) {
                                Toast.makeText(activity, "请先配置sip账号！", Toast.LENGTH_SHORT).show();
                            } else {
                                if (mySipNumber.equals(sip)) {
                                    Toast.makeText(activity, "无法和自己通话！", Toast.LENGTH_SHORT).show();
                                } else {
                                    String uuid = calluuid;
                                    if (null==uuid||TextUtils.isEmpty(uuid)){
                                        uuid = UUID.randomUUID().toString();
                                    }
                                    String finalUuid = uuid;
                                    Log.e(TAG, "accept: ==PjSip==webToAAction==========businessId=="+businessId+"==busdata==" +busdata);
                                    JNBaseHttpUtils.getInstance().SaveBusiness(EasyHttp.post(activity)
                                            , businessId, uuid, "1", busdata
                                            , new OnHttpListener<JNSaveBusinessApi.Bean>() {
                                        @Override
                                        public void onSucceed(JNSaveBusinessApi.Bean result) {
                                            Log.e(TAG, "createCall: =webToAAction=====003=");
                                            Intent intent;
                                            if (TextUtils.equals("0", type)){
                                                intent = new Intent(activity, JCPjSipP2PActivity.class);
                                            }else {
                                                intent = new Intent(activity, JCPjSipVideoActivity.class);
                                            }
                                            if (null!=busdata && !TextUtils.isEmpty(busdata)){
                                                Log.e(TAG, "accept: ==PjSip==webToAAction=====01=====businessId=="+businessId+"==busdata==" +busdata);
                                                intent.putExtra("BusinessData", busdata);
                                                intent.putExtra("BusinessId", businessId);
                                            }
                                            intent.putExtra("calluuid", finalUuid);
                                            intent.putExtra("tag", "outing");
                                            intent.putExtra("number", sip.replace(" ", ""));
                                            activity.startActivity(intent);
                                            if (null!=callBackListener)callBackListener.onCall(0, type, sip);
                                        }

                                        @Override
                                        public void onFail(Exception e) {
                                            JCChatManager.showToast("存储业务数据失败！");
                                        }
                                    });

                                }
                            }
                        }
                    }
                });
    }


}

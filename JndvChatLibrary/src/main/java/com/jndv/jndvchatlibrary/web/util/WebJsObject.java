package com.jndv.jndvchatlibrary.web.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.provider.MediaStore.Images.Media;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.hjq.http.EasyHttp;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNSaveBusinessApi;
import com.jndv.jnbaseutils.ui.bean.WebActionBean;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.google.gson.Gson;
import com.google.zxing.client.android.CaptureActivity;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.IntentConstant;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.bean.JCWebFileBean;
import com.jndv.jndvchatlibrary.bean.WebMonitorBean;
import com.jndv.jndvchatlibrary.http.api.JCFilesUpApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.CameraActivity;
import com.jndv.jndvchatlibrary.ui.VideoPlayActivity;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCrecordButton;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;
import com.jndv.jndvchatlibrary.utils.GetDeviceIdUtil;
import com.jndv.jnbaseutils.ui.bean.JNWebBean;
import com.jndv.jndvchatlibrary.utils.JSharedPreferences;
import com.jndv.jndvchatlibrary.web.PDFViewActivity;
import com.wgd.choosefilelib.ChooseFile;
import com.wgd.choosefilelib.ChooseFileInfo;
import com.wgd.choosefilelib.ChooseFileUIConfig;
import com.wgd.choosefilelib.IFileChooseListener;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.tencent.smtt.sdk.WebView;
//import com.wgd.wgdfilepickerlib.BaseFragment;
//import com.wgd.wgdfilepickerlib.FilePickerAllPagActivity;
//import com.wgd.wgdfilepickerlib.WGDPickerManager;
//import com.wgd.wgdfilepickerlib.bean.FileEntity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.reactivex.functions.Consumer;

/**
 * Web页面与Android交互 向web页传递图片,相册等
 */
public class WebJsObject {
    private final String TAG = "WebJsObject";
    private FragmentActivity activity;
    private WebView webView;
    private final int REQUEST_PHOTO = 0;
    JCrecordButton audioRecord ;
    private WebJsListerner mWebJsListerner ;

    public interface WebJsListerner{
//        0==type 开启加载弹框；1==type 关闭弹框; 2==TYPE 下载文件
        void onOperation(int type, Object... objects);
    }

    public WebJsObject(AppCompatActivity activity, WebView webView, JCrecordButton audioRecord ) {
        super();
        this.activity = activity;
        this.webView = webView;
        this.audioRecord = audioRecord;
    }

    public WebJsObject(FragmentActivity activity, WebView webView, JCrecordButton audioRecord, WebJsListerner webJsListerner ) {
        super();
        this.activity = activity;
        this.webView = webView;
        this.audioRecord = audioRecord;
        this.mWebJsListerner = webJsListerner;
    }

    // 调用安卓系统相册
    @JavascriptInterface
    public void addPhotoFromAndroid() {
        Intent intent = new Intent(Intent.ACTION_PICK, Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(intent, REQUEST_PHOTO);
    }

    // 获取经纬度
    @JavascriptInterface
    public String getLatitude() {
        return JNBaseUtilManager.getLatitudeWgs84();
    }

    // 获取经纬度
    @JavascriptInterface
    public String getLongitude() {
        return JNBaseUtilManager.getLongitudeWgs84();
    }

    // 获取经纬度
    @JavascriptInterface
    public String getLngLat() {
        String str = JNBaseUtilManager.getLongitudeWgs84()+","+JNBaseUtilManager.getLatitudeWgs84();
        return str;
    }

    //操作日志上报
    @JavascriptInterface
    public void upOperationLog(String action, String actionId, String actionType){
        JNBaseUtilManager.upLog(activity,action,actionId,actionType);
        Log.d("JsOperation", "upOperationLog:操作日志上报 ");
    }

    @JavascriptInterface
    public String getDeviceBrand() {
        return Build.BRAND.replace(" ", "_");
    }

    @JavascriptInterface
    public String getDeviceModel() {
        return Build.MODEL.replace(" ", "_");
    }

    @JavascriptInterface
    public String getDeviceID() {
        return JSharedPreferences.getConfigStrValue(activity, GlobalConstant.KEY_DEVICES_ID, GetDeviceIdUtil.getDeviceId(activity));
    }

    // 识别1:车牌识别，2：身份证识别
    @JavascriptInterface
    public void shiBie(int type) {
        Log.d("JsOperation", "shiBie: 车牌识别身份证识别");
//        Intent intent = new Intent(activity, DiscernActivity.class);
//        switch (type) {
//            case 1:
//                intent.putExtra("type", 1);
//                ((InfoWebActivity) activity).startActivityForResult(intent, 110);
//                break;
//            case 2:
//                intent.putExtra("type", 2);
//                ((InfoWebActivity) activity).startActivityForResult(intent, 120);
////                Intent intentScan=new Intent(activity,CaptureActivity.class);
////                intentScan.putExtra("tag",2);
////                ((InfoWebActivity)activity).startActivityForResult(intentScan,140);
//                break;
//            default:
//                break;
//        }
    }

    @JavascriptInterface
    public void takeFacePic() {
        Intent intent = new Intent(activity, CameraActivity.class);
        activity.startActivityForResult(intent, 130);
    }

    //调用二维码扫描
    @JavascriptInterface
    public void goScan(){
        activity.startActivity(new Intent(activity, CaptureActivity.class));
        activity.finish();
    }
    //调用二维码扫描
    @JavascriptInterface
    public void goScanReturn(){
        Intent intentScan=new Intent(activity,CaptureActivity.class);
        intentScan.putExtra("tag",2);
        activity.startActivityForResult(intentScan,150);
    }

    /**
     * 调用web页面方法
     * @param str 上传的信息
     */
    public void appAction(String str){
        String action = "javascript:appAction("+str+")";
        Log.e(TAG, "upFilesAll=onSucceed: =22=str=action==" + action);
        evaluateJavascript(action);
    }

    @JavascriptInterface
    public void showPdfFile(String url){
        if (!url.startsWith("http")){
            url = JNBaseUtilManager.getLoginServerUrl() + url;
        }
        Intent intent=new Intent(activity, PDFViewActivity.class);
        intent.putExtra(IntentConstant.WEB_URL,url);
        activity.startActivity(intent);
    }

    @JavascriptInterface
    public void playVideo(String url){
        if (!url.startsWith("http")){
            url = JNBaseUtilManager.getLoginServerUrl() + url;
        }
        Intent intent=new Intent(activity, VideoPlayActivity.class);
        intent.putExtra(IntentConstant.WEB_URL,url);
        activity.startActivity(intent);

//        Intent intent = new Intent(getActivity(), JCMonitorPlayerActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.putExtra("player_url", result.getData().getRtsp());
//        intent.putExtra("title", name);
//        startActivity(intent);
    }

    private void applyPermissions(Consumer<Boolean> consumer, String... permissions){
        JCThreadManager.onMainHandler(new Runnable() {
            @Override
            public void run() {
                try {
                    RxPermissions rxPermissions=new RxPermissions((Activity) activity);
                    rxPermissions.request(permissions)
                            .subscribe(consumer);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void selectFile(){
        Log.e(TAG, "selectFile: =====null======");
        selectFile(null);
    }
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void selectFile(String[] types){
        Log.e(TAG, "selectFile: ==========="+new Gson().toJson(types));
        try {
            applyPermissions(new Consumer<Boolean>() {
                                 @SuppressLint("CheckResult")
                                 @Override
                                 public void accept(Boolean aBoolean) {
                                     if (aBoolean){
                                         Log.e(TAG, "selectFile: ==accept=========");
                                         ChooseFile.create((FragmentActivity) activity).setUIConfig(new ChooseFileUIConfig.Builder().build())
                                                 .forResult(new IFileChooseListener() {
                                                     @Override
                                                     public void doChoose(@Nullable ChooseFileInfo chooseFileInfo) {
                                                         Log.e("0531", "doChoose: ==============" + chooseFileInfo.filePath);
                                                         upFilesAll(chooseFileInfo.filePath);
                                                     }
                                                 });
                                     }else{
                                         //只要有一个权限被拒绝，就会执行
                                         Toast.makeText(activity, "未授权权限，功能不能使用!", Toast.LENGTH_SHORT).show();
                                     }
                                 }
                             }, Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ,Manifest.permission.READ_EXTERNAL_STORAGE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void shootVideo(){
        Log.e(TAG, "shootVideo: ===========");
        if (null!=mWebJsListerner)mWebJsListerner.onOperation(3);
    }

    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void shootImage(String num){
        Log.e(TAG, "shootVideo: ===========");
        if (num==null||num.isEmpty()){
            num = "9";
        }

        if (null!=mWebJsListerner)mWebJsListerner.onOperation(4, num);
    }
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void gps_track_start(){
        Log.e(TAG, "gps_track_start: ===========");
        JNBaseUtilManager.setGpsFrequency(1000);
    }
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void gps_track_start(int num){
        if (num<=100){
            num = 100;
        }
        Log.e(TAG, "gps_track_start: =========num=="+num);
        JNBaseUtilManager.setGpsFrequency(num);
    }
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void gps_track_end(){
        Log.e(TAG, "gps_track_end: ===========");
        JNBaseUtilManager.recoverGpsFrequency();
    }

    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void recordAudio(){
        Log.e(TAG, "recordAudio: ===========");
        if (null!=audioRecord){
            JCThreadManager.onMainHandler(new Runnable() {
                @Override
                public void run() {
                    audioRecord.setVisibility(View.VISIBLE);
                    audioRecord.setOnFinishedRecordListener(new JCrecordButton.OnFinishedRecordListener() {
                        @Override
                        public void onFinishedRecord(String audioPath) {
                            Log.e(TAG, "onFinishedRecord: ==" + audioPath);
                            upFilesAll(audioPath);
//                            webView.evaluateJavascript("javascript:uploadReturnAddress(\""+audioPath+"\")", null);
                            audioRecord.setVisibility(View.GONE);
                        }
                    });
                }
            });
        }
    }

    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void finishPage(){
        activity.finish();
    }
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void tokenClose(){
        JCChatManager.showToast("tokenClose");
//        if (null!=mWebJsListerner)mWebJsListerner.onOperation(5);
    }

    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void downLoadFile(String o){
        try {
            if (null==o) Log.e(TAG, "downLoadFile: =====null===");
            else Log.e(TAG, "downLoadFile: =====jcWebFileBean===" + new Gson().toJson(o));
            JCWebFileBean jcWebFileBean = new Gson().fromJson(o, JCWebFileBean.class);
            if (null!=jcWebFileBean){
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(2, jcWebFileBean);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * web页面调用Android统一接口
     * @param data
     */
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void webToAAction(String data){
        try {
            Log.e(TAG, "webToAAction: ========"+data);
//            JCChatManager.showToast(data);
            JSONObject bean = new JSONObject(data);
//            WebActionBean<String> bean = new Gson().fromJson(data, WebActionBean.class);
            if (TextUtils.equals("上报调度信息", bean.getString("action"))){
                try {
                    JSONObject object = bean.getJSONObject("data");
//                    String userList = object.getString("userList");
                    String uuid = object.getString("uuid");
                    String huigu_status = object.getString("huigu_status");
                    String user_identity = object.getString("user_identity");
                    String dataStr = (String) bean.getString("data");

                    if (null!=mWebJsListerner)mWebJsListerner.onOperation(7, uuid, dataStr, huigu_status, user_identity);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else if (TextUtils.equals("startCallDispatch", bean.getString("action"))){
                if (JNBaseUtilManager.isIsCalling()){
                    JCChatManager.showToast("正在通话中，不能再次创建！");
                    return;
                }
                try {
//
                    JSONObject object = bean.getJSONObject("data");
//                    调度类型：0:无效 1: 一对一语音 2:一对一视频 3:视频会议 4：纯语音会议
                    int type = 0;
                    try {
                        type = object.getInt("type");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    String id = "";
                    try {
                        id = object.getString("dispatchid");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    String calluuid = "";
                    try {
                        calluuid = object.getString("calluuid");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if (null==calluuid||TextUtils.isEmpty(calluuid)){
                        calluuid = UUID.randomUUID().toString();
                    }
                    if (1==type){
                        String uid = "";
                        String sipId = "";
                        String phoneId = "";
                        String name = "";
                        try {
                            JSONObject callee = object.getJSONObject("callee");
                            uid = callee.getString("uid");
                            sipId = callee.getString("sipId");
                            phoneId = callee.getString("phoneId");
                            name = callee.getString("name");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        String busdata = "";
                        try {
                            busdata = object.getString("busData");
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if (null!=mWebJsListerner)mWebJsListerner.onOperation(16, "0", uid, sipId, phoneId, name, calluuid, busdata, id);

                    }else if (2==type){
                        String uid = "";
                        String sipId = "";
                        String phoneId = "";
                        String name = "";
                        try {
                            JSONObject callee = object.getJSONObject("callee");
                            uid = callee.getString("uid");
                            sipId = callee.getString("sipId");
                            phoneId = callee.getString("phoneId");
                            name = callee.getString("name");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        String busdata = "";
                        try {
                            busdata = object.getString("busData");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        if (null!=mWebJsListerner)mWebJsListerner.onOperation(16, "1", uid, sipId, phoneId, name, calluuid, busdata, id);
                    }else if (3==type){
                        String userList = "";
                        try {
                            userList = object.getString("users");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        String dispatchtitle ="";
                        try {
                            dispatchtitle = object.getString("dispatchtitle");
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        String busdata = "";
                        try {
                            busdata = object.getString("busData");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        if (null!=mWebJsListerner)mWebJsListerner.onOperation(5, id, busdata, calluuid);
                        Map<String, String> map = new HashMap<>();
                        map.put("businessId", id);
                        map.put("businessData", busdata);
                        map.put("businessType", "0");
                        map.put("userList", userList);
                        map.put("title", dispatchtitle);
                        map.put("calluuid", calluuid);
                        JNBaseUtilManager.createMeetQuick(activity, map);
                    }else if (4==type){
//                        String userList = object.getString("users");
//                        String dispatchtitle = object.getString("dispatchtitle");
//                        if (null!=mWebJsListerner)mWebJsListerner.onOperation(5, id);
//                        JNBaseUtilManager.createMeetQuick(activity, id, userList, dispatchtitle);
                    }else {

                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG, "webToAAction: =======0030=", e);
                }

            }else if (TextUtils.equals("insertPartMember", bean.getString("action"))){
                try {
                    JSONObject para = bean.getJSONObject("para");
                    String userList = para.getString("users");
                    String id = bean.getString("busID");
                    if (null!=mWebJsListerner)mWebJsListerner.onOperation(6, id, userList);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else if (TextUtils.equals("顶部按钮显示", bean.getString("action"))){
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(8);
            }else if (TextUtils.equals("顶部按钮隐藏", bean.getString("action"))){
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(9);
            }else if (TextUtils.equals("弹框关闭", bean.getString("action"))){
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(10);
            }else if (TextUtils.equals("调度结束", bean.getString("action"))){
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(11);
            }else if (TextUtils.equals("消息数量变化", bean.getString("action"))){
                try {
                    JSONObject object = bean.getJSONObject("data");
                    String num = object.getString("num");
                    String id = object.getString("dispatchid");
                    try {
                        Integer.parseInt(num);
                    }catch (Exception e){
                        e.printStackTrace();
                        num="0";
                    }
                    if (null!=mWebJsListerner)mWebJsListerner.onOperation(12, id, num);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else if (TextUtils.equals("离开调度", bean.getString("action"))){
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(13);
            }else if (TextUtils.equals("显示监控播放弹框", bean.getString("action"))){
                try {
                    WebMonitorBean webMonitorBean = new Gson().fromJson(bean.getString("data"), WebMonitorBean.class);
                    if (null!=mWebJsListerner)mWebJsListerner.onOperation(14, webMonitorBean);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else if (TextUtils.equals("关闭监控播放弹框", bean.getString("action"))){
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(15);
            }else if (TextUtils.equals("queryCurCallStatus", bean.getString("action"))){
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(17);
            }else if (TextUtils.equals("startCallIntercom", bean.getString("action"))){
                Log.e(TAG, "webToAAction: ==============startCallIntercom");
            }else if (TextUtils.equals("thrCallStart", bean.getString("action"))){//三方呼叫
                Log.e(TAG, "webToAAction: ==============thrCallStart");
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(18, bean);
            }else if (TextUtils.equals("thrCallAddUsers", bean.getString("action"))){//三方加人
                Log.e(TAG, "webToAAction: ==============thrCallAddUsers");
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(19, bean);
            }else if (TextUtils.equals("thrCallClose", bean.getString("action"))){//三方结束
                Log.e(TAG, "webToAAction: ==============thrCallClose");
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(20, bean);
            }else if (TextUtils.equals("webCtrToJoinThrWay", bean.getString("action"))){//web通知客户端加入三方通话
                Log.e(TAG, "webToAAction: ==============webCtrToJoinThrWay");
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(21, bean);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /**
     * 创建会议，其中userList为要邀请的用户列表，用户信息格式为
     * "{"userUuid":"用户UUID"
     * ,"uid":"用户sip号"
     * ,"name":"用户名称"
     * ,"head":"用户头像"
     * ,"isMute":"是否静音"
     * ,"isVideoOff":"是否"
     * ,"state”:”状态”
     * ,”terminalEquipment”:”会议成员参会时的终端设备类型【0：SIP设备，1：手机，2：办公室电话，3：对讲机】”}”
     * @param userList
     */
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void createMeetQuick(String userList){
        try {
            Map<String, String> map = new HashMap<>();
            map.put("businessId", "");
            map.put("userList", userList);
            map.put("title", "");
            JNBaseUtilManager.createMeetQuick(activity, map);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 创建预定会议
     * 其中content的格式为{
     *         "extend":"",//扩展字段（暂无意义）
     *                 "title":"",//会议标题
     *                 "startTime":"",//开始时间
     *                 "duration":"",//会议时长
     *                 "meetAttribute":"",//会议属性【0：一次性，1：周期性】
     *                 "validTime":"",//有效期
     *                 "users":"[{
     *                 "uid":"1",//参会者id
     *                 "name":"1",//参会者名称
     *                 "head":"",//参会者头像
     *                 "isMute":"0",//参会者静音状态
     *                 "isVideoOff":"0",//参会者视频状态
     *                 "state":"0"//参会者入会状态
     *     },]
     *             ",//参会人员列表
     *             "layoutMode":"0",//布局模式
     *             "layoutStruct":"0",//布局类型（1.2.3.4.9.讲演）
     *             "layoutUsers":"[{
     *             "uid":"1",//参会者id
     *             "name":"1",//参会者名称
     *             "head":"",//参会者头像
     *             "isMute":"0",//参会者静音状态
     *             "isVideoOff":"0",//参会者视频状态
     *             "state":"0"//参会者入会状态
     * },]",//布局画面
     *         "isAllMute":"0",//是否全体静音
     *         "isOpenPass":"0",//是否开启会议密码
     *         "isShare":"0",//参会者是否可以共享
     *         "isInvite":"0",//参会者是否可以邀请
     *         "isRecord":"0",//参会者是否可以录制
     *         "isAddMute":"0",//成员入会时是否静音
     *         "isUserOpenMute":"0",//是否允许成员自我解除静音
     * //"isAddVideo":"0",//成员入会时是否开启摄像头
     *         "compereUid":"0",//主持人uid
     *         "relation":"0",//关联信息，相当于将会议分类，后期方便通过类型查找记录，例如：即时通讯中群组id，标识本群中的会议记录；或者某一部门的会议记录等。
     *         "uuid":"0",//创建会议的uuid
     *         "meetPass":"0",//会议的密码
     *         }
     */
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void createMeetReserve(String content){
        try {
            JNBaseUtilManager.createMeetReserve(activity,content);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 加入会议
     * 其中content的数据格式为
     * {
     * "meetCode":"0",//会议号
     * "meetPass":"0",//会议密码
     * "isMute":"0",//是否静音：0关闭；1打开
     * "isVideoOff":"0",//参会者视频状态
     * }
     */
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void addMeet(String content){
        try {
            JNBaseUtilManager.addMeet(content);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 打开页面:0=预定会议；1=加入会议；2=会议主页；3=历史会议
     */
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void startMeetActivity(int type){
        try {
            JNBaseUtilManager.startMeetActivity(type);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 发送站内消息
     * type为0=文字；
     *  { "users":[{ "uid":"",//用户ID "sipAddress":"",//用户sip域地址 "javaAddress":"",//用户java域地址
     *      "name":"",//用户名称 "mobile":"",//用户手机号 } ],//要群发的用户列表
     *      "content":{ "text":"",//文字文本 "fontSize":0,//文字字体大小
     *      "fontColor":0,//文字字体颜色 "fontType":0,//文字字体类型 } }
     * 1=tts；
     *  { "users":[{ "uid":"",//用户ID "sipAddress":"",//用户sip域地址 "javaAddress":"",//用户java域地址
     *       "name":"",//用户名称 "mobile":"",//用户手机号 } ],//要群发的用户列表
     *       "content":{ "calltype":"",// "comment":{ "1":"",// "2":"",// "3":"",// },//
     *       "endtime":"",//结束时间 "maxcall":"",// "text":"",// } }
     * 2=图片；
     *  { "users":[{ "uid":"",//用户ID "sipAddress":"",//用户sip域地址 "javaAddress":"",//用户java域地址
     *       "name":"",//用户名称 "mobile":"",//用户手机号 } ],//要群发的用户列表
     *       "content":{ "url":"",//图片文件地址，必须全路径 "thumbnailUrl":"",//缩略图文件地址，必须全路径
     *       "width":"",//图片宽 "height":""//图片高 } }
     * 3=传入整体消息格式
     *
     * @param type
     * @param content
     */
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void sendMsg(int type, String content){
        try {
            JNBaseUtilManager.sendMsg(type, content);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 查询当前会议信息
     */
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void getMeettingInfo(){
        try {
            if (JNBaseUtilManager.isIsCalling()&&JNBaseUtilManager.isIsMeeting()){
                appAction(JNBaseUtilManager.getInPhoneNumber());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 查询当前会议信息
     */
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void startUserCall(String number){
        try {
            JNBaseUtilManager.startUserCall(number);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void upFilesAll(String path){
        if (null!=mWebJsListerner)mWebJsListerner.onOperation(0);
        JCFilesUpUtils.upFilesAll(path, new OnHttpListener<JCFilesUpApi.Bean>() {
            @Override
            public void onSucceed(JCFilesUpApi.Bean result) {
                try {
                    Log.e(TAG, "upFilesAll=onSucceed: =22=str=" );
                    if (null!=result){
                        Log.e(TAG, "upFilesAll=onSucceed: =22=str=result=" + new Gson().toJson(result));
                        if (null!=result.getUrlArr()&&result.getUrlArr().size()>0) {
                            if (TextUtils.equals("success",result.getUrlArr().get(0).getUpload())){
                                String filePath = result.getUrlArr().get(0).getTailUrl();
                                String tailThumbnailUrl = result.getUrlArr().get(0).getTailThumbnailUrl();
//                                if (!filePath.startsWith("http")){
//                                    filePath = JNBaseUtilManager.getFilesUrl(result.getUrlArr().get(0).getTailUrl());
//                                }
                                String name = "";
                                try {
                                    name = path.substring(path.lastIndexOf("/")+1);
                                    name = name.substring(0, name.lastIndexOf("."));
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                JCWebFileBean jcWebFileBean = new JCWebFileBean();
                                jcWebFileBean.setName(name);
                                jcWebFileBean.setUrl(filePath);
                                jcWebFileBean.setTailThumbnailUrl(tailThumbnailUrl);
                                JNWebBean<JCWebFileBean> jnWebBean = new JNWebBean<>("上传文件回调", jcWebFileBean);
//                                String str = "{message:"上传文件回调", data:{}}";
                                String str = new Gson().toJson(jnWebBean);
                                Log.e(TAG, "upFilesAll=onSucceed: =22=str=js==" + str);
                                appAction(str);
//                                evaluateJavascript(str);
                            }else {
                                JCWebFileBean jcWebFileBean = new JCWebFileBean();
                                jcWebFileBean.setName("上传失败！");
                                JNWebBean<JCWebFileBean> jnWebBean = new JNWebBean<>("上传文件回调", jcWebFileBean);
                                String str = new Gson().toJson(jnWebBean);
                                Log.e(TAG, "upFilesAll=onSucceed: =22=str=js==" + str);
                                appAction(str);
//                                evaluateJavascript("javascript:uploadReturnAddress("+new Gson().toJson(jcWebFileBean)+")");
                            }
                            if (null!=mWebJsListerner)mWebJsListerner.onOperation(1);
                        }
                    }else {
                        Log.e(TAG, "upFilesAll=onSucceed: =22=str=null==result" );
                        JCWebFileBean jcWebFileBean = new JCWebFileBean();
                        jcWebFileBean.setName("上传失败！");
                        JNWebBean<JCWebFileBean> jnWebBean = new JNWebBean<>("上传文件回调", jcWebFileBean);
                        String str = new Gson().toJson(jnWebBean);
                        Log.e(TAG, "upFilesAll=onSucceed: =22=str=js==" + str);
                        appAction(str);
                        if (null!=mWebJsListerner)mWebJsListerner.onOperation(1);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(Exception e) {
                JNLogUtil.e(TAG,"==upFilesAll==onFail==01==");
                JCWebFileBean jcWebFileBean = new JCWebFileBean();
                jcWebFileBean.setName("上传失败！");
                JNWebBean<JCWebFileBean> jnWebBean = new JNWebBean<>("上传文件回调", jcWebFileBean);
                String str = new Gson().toJson(jnWebBean);
                Log.e(TAG, "upFilesAll=onSucceed: =22=str=js==" + str);
                appAction(str);
                JCThreadManager.showMainToast("上传文件过大！");
//                evaluateJavascript("javascript:uploadReturnAddress(\""+new Gson().toJson(new JCWebFileBean())+"\")");
                if (null!=mWebJsListerner)mWebJsListerner.onOperation(1);
            }
        });
    }

    private void evaluateJavascript(String str){
        JCThreadManager.onMainHandler(new Runnable() {
            @Override
            public void run() {
                webView.evaluateJavascript(str, null);
            }
        });
    }

}

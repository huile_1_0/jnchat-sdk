package com.jndv.jndvchatlibrary.web;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.ui.base.JNBaseFragment;
import com.jndv.jndvchatlibrary.databinding.ActivityInfoWebBinding;
import com.jndv.jndvchatlibrary.web.util.WebJsObject;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.export.external.interfaces.WebResourceError;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import org.jetbrains.annotations.NotNull;

public class WebTokenFragment extends JNBaseFragment {

    private String TAG = "WebTokenFragment";
    public static boolean started = false ;
    ActivityInfoWebBinding binding ;

    private String webUrl = JNBaseUtilManager.getLoginServerUrl()+"/admin/#/app/token?token=";
//    private String webUrl = "http://192.168.13.55:8806/admin/#/app/token?token=";//张飞虎
//    private String webUrl = "http://192.168.13.29:8806/admin/#/app/token?token=";//姜博皓
    private int loadNum = 0 ;//加载次数

    WebJsObject mWebJsObject;
    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = ActivityInfoWebBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);
        started = true;
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.newWebView.destroy();
        started = false ;
    }
    private void init(View view){
        String token = JNBaseUtilManager.getToken();
        if (null==token||token.isEmpty()){
            JNBaseUtilManager.showToast("打开web页面，token为空！");
            return;
        }
        try {
            String url = getArguments().getString("Url");
            if (null!=url&&!TextUtils.isEmpty(url)){
                webUrl = url;
            }
        }catch (Exception e){}
        if (null!=webUrl&& !TextUtils.isEmpty(webUrl)){
            webUrl = webUrl + token;
            initWebView();
        }
    }

    @SuppressLint({"JavascriptInterface", "SetJavaScriptEnabled"})
    private void initWebView() {
        Log.e(TAG, "initWebView: ===0000===="+webUrl);
        WebView.setWebContentsDebuggingEnabled(true);
        try {
//            webView.clearFormData();
//            webView.clearHistory();
        }catch (Exception e){
            e.printStackTrace();
//            JCChatManager.showToast("缓存清理异常检测！");
        }

        // web配置
        WebSettings settings = binding.newWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);// 默认关闭，解决地图打开不完整有白块的问题

        // 百度地图加载不出来添加
        settings.setAllowFileAccess(true);
        settings.setBlockNetworkImage(false);
        settings.setBlockNetworkLoads(false);
        settings.setDatabaseEnabled(true);
        settings.setGeolocationEnabled(true);
        String dir = getActivity().getDir("database", Context.MODE_PRIVATE).getPath();
        settings.setGeolocationDatabasePath(dir);
        settings.setLoadsImagesAutomatically(true); // 加载图片

        settings.setUseWideViewPort(true); // 设置此属性，可任意比例缩放
        settings.setLoadWithOverviewMode(true); // 自适应屏幕
        // 不使用缓存
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        // 如果webView中需要用户手动输入用户名、密码或其他，则webview必须设置支持获取手势焦点。

        binding.newWebView.requestFocusFromTouch();
        String method = "setVideoPlaybackRequiresUserGesture";
//val为true或false。

//兼容视频
        try {
            if (binding.newWebView.getX5WebViewExtension() != null) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("require", true);
//        bundle.set
                binding.newWebView.getX5WebViewExtension().invokeMiscMethod(method, bundle);
                Bundle data = new Bundle();
                data.putBoolean("standardFullScreen", false);
                //true表示标准全屏，false表示X5全屏；不设置默认false，
                data.putBoolean("supportLiteWnd", false);
                //false：关闭小窗；true：开启小窗；不设置默认true，
                data.putInt("DefaultVideoScreen", 1);
                //1：以页面内开始播放，2：以全屏开始播放；不设置默认：1
                binding.newWebView.getX5WebViewExtension().invokeMiscMethod("setVideoParams", data);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // chromium, enable hardware acceleration
//        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        // 添加接口
        binding.newWebView.addJavascriptInterface(mWebJsObject, "webJsObject");
        binding.newWebView.addJavascriptInterface(this, "Jndv_Main");
//        history_url = webUrl.trim().toLowerCase();
//        Log.i(TAG, "InfoWeb: "+ webUrl);
//        loadStartTime = System.currentTimeMillis();
        binding.newWebView.loadUrl(webUrl);

        binding.newWebView.setWebViewClient(new WebViewClient() {

            // 覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (url == null) return false;

                try {
                    if (url.startsWith("weixin://") //微信
                            || url.startsWith("alipays://") //支付宝
                            || url.startsWith("mailto://") //邮件
                            || url.startsWith("tel://")//电话
                            || url.startsWith("dianping://")//大众点评
                            || url.startsWith("tbopen://")//淘宝
                            || url.startsWith("openapp.jdmobile://")//淘宝
                            || url.startsWith("tmast://")//淘宝

                        //其他自定义的scheme
                    ) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                        startActivity(intent);
                        return true;
                    }
                } catch (Exception e) { //防止crash (如果手机上没有安装处理某个scheme开头的url的APP, 会导致crash)
                    return true;//没有安装该app时，返回true，表示拦截自定义链接，但不跳转，避免弹出上面的错误页面
                }
                Log.e(TAG, "shouldOverrideUrlLoading: " + url);
                // 返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed(); // 接受所有网站的证书,这里支持https
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                // 这个方法在6.0才出现
                String data = "Page NO FOUND！";

                int statusCode = errorResponse.getStatusCode();
                Log.e(TAG, "onReceivedHttpError: "+statusCode );

                if (404 == statusCode || 500 == statusCode) {
//                    view.loadUrl("about:blank");// 避免出现默认的错误界面
//                    view.loadUrl("javascript:document.body.innerHTML=\"" + data + "\"");
                }

            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                // 断网或者网络连接超时
                Log.e(TAG, "onReceivedError: "+error.getErrorCode() );
                String data = "Page NO FOUND！";
                if (error.getErrorCode() == ERROR_HOST_LOOKUP || error.getErrorCode() == ERROR_CONNECT || error.getErrorCode() == ERROR_TIMEOUT) {
//                    view.loadUrl("about:blank"); // 避免出现默认的错误界面
//                    view.loadUrl(mErrorUrl);
//                    view.loadUrl("javascript:document.body.innerHTML=\"" + data + "\"");
                    if (++loadNum<2)binding.newWebView.loadUrl(webUrl);
                }

            }
        });

    }

}

package com.jndv.jndvchatlibrary;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.ehome.manager.JNPjSip;
import com.ehome.sipservice.SipConnectState;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNGetCallBusinessApi;
import com.jndv.jnbaseutils.ui.bean.MeetInfoBean;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jnbaseutils.utils.PermissionsUtil;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipP2PActivity;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipVideoActivity;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNLogUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

/**
 * Author: wangguodong
 * Date: 2022/6/15
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: sip监听
 */
public class MyPjSipListener implements JNPjSip.PjSipListener {

    private int nowsState = SipConnectState.disconnected;
    private long downTime = 0 ;

    @SuppressLint("SuspiciousIndentation")
    @Override
    public void onRegistration(boolean isSuccessed) {
        JNLogUtil.e("pjsip", "initPJSIP SipConnectState onRegistration: " + isSuccessed);
        if (!isSuccessed){
            if (null!=JCChatManager.getmAllOperateListener())
                JCChatManager.getmAllOperateListener().sipConnectState(SipConnectState.disconnected);
                nowsState = SipConnectState.disconnected;
//                JCChatManager.nowsState = SipConnectState.disconnected;
            if (nowsState == SipConnectState.connected){
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SIP_CONNECT_NO,""));
            }
        }else {
            JNLogUtil.e("pjsip", "initPJSIP SipConnectState onRegistration: nowsState=" + nowsState);
            if (null!=JCChatManager.getmAllOperateListener())JCChatManager.getmAllOperateListener().sipConnectState(SipConnectState.connected);
            nowsState = SipConnectState.connected;
//            JCChatManager.nowsState = SipConnectState.connected;
            if (nowsState == SipConnectState.disconnected){
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SIP_CONNECT_OK,""));
            }
        }
    }

    @Override
    public void onIncomingCall(String accountID, int callID, String displayName, String remoteUri, boolean isVideo,boolean isMeeting, String msgHead) {
        JNLogUtil.e("pjsip", "p2p onIncomingCall: accountID:" + accountID
                + " callID:" + callID + " displayName:" + displayName + " remoteUri:"
                + remoteUri + " isVideo:" + isVideo);
        try {
            //clustercall 是集群对讲 conference是会议 threewaycall是三方通话成员
            // thrsos是三方通话主叫 throthersos是三方通话第三方
            String calltype = JNPjSip.getPrivateFromMsgHead("X-calltype", msgHead);
            String jnctr = JNPjSip.getPrivateFromMsgHead("X-jn_ctr", msgHead);//业务数据
            String Calluuid = JNPjSip.getPrivateFromMsgHead("X-Calluuid", msgHead);//唯一标识
            Log.e("calltype", "p2p onIncomingCall: ===jc=========calltype=" + calltype);
            if(!TextUtils.equals("clustercall", calltype)
                    && !TextUtils.equals("conference", calltype)){//会议
                //      普通通话（音频、视频）和会议通话，只处理一个，有当前呼入进来的，新invite直接拒绝
                if (JNBaseUtilManager.isIsCalling()){
                    Log.e("pjsip", "onIncomingCall: ==pp==onTerminate=====");
                    JNPjSip.getInstance(JCChatManager.mContext).onTerminate(callID);
                    return;
                }
                String remoteNum = "";
                if(TextUtils.isEmpty(remoteUri)){
                    remoteNum = displayName;
                }else{
//                    TODO 因为切换KA后FS服务端发送过来的remoteUri是固定值，所以这里不再使用这种方式
                    remoteNum = remoteUri.split("@")[0];
                }
                JNBaseUtilManager.setInPhoneNumber(remoteNum);
                int type = 3 ;
                if (TextUtils.equals("thrsos", calltype)){//三方通话-警，接警人
                    type = 4 ;
                }else if (TextUtils.equals("throthersos", calltype)){//三方通话-警，三方参与人
                    type = 5 ;
                }else if (TextUtils.equals("threewaycall", calltype)){//三方通话，普通
                    type = 6 ;
                }else {
                    try {
                        String hasBusi = JNPjSip.getVFromMsgHead("hasBusi", jnctr);
                        String dispatchID = JNPjSip.getVFromMsgHead("dispatchID", jnctr);
                        if (TextUtils.equals("1", hasBusi)){
                            String finalRemoteNum = remoteNum;
                            JNBaseHttpUtils.getInstance().getCallBusinessInfo(null, dispatchID, remoteNum, new OnHttpListener<JNGetCallBusinessApi.Bean>() {
                                @Override
                                public void onSucceed(JNGetCallBusinessApi.Bean result) {
                                    if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                        boolean isListener = false;
                                        if (null!=JNBaseUtilManager.getJnBaseListener())
                                            isListener = JNBaseUtilManager.getJnBaseListener().onResult(0,2, isVideo, finalRemoteNum, callID, Calluuid, result);
                                        if (!isListener)openCallActivity(isVideo, finalRemoteNum, callID, Calluuid, result);
                                    }
                                }

                                @Override
                                public void onFail(Exception e) {
                                    boolean isListener = false;
                                    if (null!=JNBaseUtilManager.getJnBaseListener())
                                        isListener = JNBaseUtilManager.getJnBaseListener().onResult(1,2, isVideo, finalRemoteNum, callID, Calluuid);
                                    if (!isListener)openCallActivity(isVideo, finalRemoteNum, callID, Calluuid, null);
                                }
                            });
                            return;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                boolean isListener = false;
                if (null!=JNBaseUtilManager.getJnBaseListener())
                    isListener = JNBaseUtilManager.getJnBaseListener().onResult(0, type, isVideo, remoteNum, callID, Calluuid);
                if (!isListener) openCallActivity(isVideo, remoteNum, callID, Calluuid, null);
            }
        }catch (Exception e){
            JNLogUtil.e("====onIncomingCall==========", e);
//            boolean isListener = false;
//            if (null!=JNBaseUtilManager.getJnBaseListener())
//                isListener = JNBaseUtilManager.getJnBaseListener().onResult(0,3, isVideo, displayName, callID, "");
//            if (!isListener) openCallActivity(isVideo, displayName, callID, "", null);
        }
    }

    private void openCallActivity(boolean isVideo, String remoteNum, int callID, String Calluuid, JNGetCallBusinessApi.Bean result){
        Intent intent;
        JNBaseUtilManager.setIsVideo(isVideo);
        if (isVideo) {
            JNLogUtil.e("=========startActivity==001==");
            intent = new Intent(JCChatManager.mContext, JCPjSipVideoActivity.class);
        } else {
            JNLogUtil.e("=========startActivity==002==");
            intent = new Intent(JCChatManager.mContext, JCPjSipP2PActivity.class);
        }
        if (null!=result&&null!=result.getData()&&null!=result.getData().getBusinessId()
                &&!TextUtils.isEmpty(result.getData().getBusinessId())){
            intent.putExtra("BusinessData", result.getData().getBusinessData());
        }
        intent.putExtra("calluuid", Calluuid);
        intent.putExtra("tag", "incoming");
        intent.putExtra("number", remoteNum);
        intent.putExtra("callIDIn", callID);
        JNLogUtil.e("PjSipP2PActivity","=========startActivity==callID=="+callID);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            JNLogUtil.e("=========isAppRunning===="+PermissionsUtil.isAppRunning());
        if (PermissionsUtil.isAppRunning()){//在前台
            JNLogUtil.e("=========startActivity==00000011=="+downTime);
            JNLogUtil.e("=========startActivity==00000012=="+System.currentTimeMillis());
            if (System.currentTimeMillis()-downTime>500){
                downTime = System.currentTimeMillis();
                intent.putExtra("ccctag", "startActivity");
                if (null!=JNBaseUtilManager.getActivity())JNBaseUtilManager.getActivity().startActivity(intent);
                else
                    JCChatManager.mContext.startActivity(intent);
//                        JCChatManager.start();
                JNLogUtil.e("==RingtoneCommand01=======");
                JNPjSip.getInstance(JCChatManager.mContext).startRingtoneCommand();
            }
        }else {//在后台
            JNLogUtil.e("=========showNotificationO====");
            if (PermissionsUtil.isBackgroundStartAllowed()){
                initWindowOrNotify(intent, isVideo, remoteNum);
            }else {
                //                JNBaseUtilManager.showToast( remoteNum+"邀请你进行"+(isVideo?"视频":"语音")+"通话！请打开"+JCChatManager.mContext.getString(R.string.app_name)+"接听");
                JNBaseUtilManager.startShowToast( remoteNum+"邀请你进行"+(isVideo?"视频":"语音")+"通话！请打开"+JCChatManager.mContext.getString(R.string.app_name)+"接听");
                JNLogUtil.e("==RingtoneCommand02=======");
                JNPjSip.getInstance(JCChatManager.mContext).startRingtoneCommand();
            }
        }
    }

    @Override
    public void onOutgoingCall(String accountID, int callID, String number) {
        JNLogUtil.e("pjsip", "onOutgoingCall: ");

    }

    @Override
    public void onConnectState(int state) {
        JNLogUtil.e("pjsip", "SipConnectState onConnectState: "+state);
        if (state == SipConnectState.disconnected){
            if (null!=JCChatManager.getmAllOperateListener())JCChatManager.getmAllOperateListener().sipConnectState(SipConnectState.disconnected);
            if (nowsState==SipConnectState.connected){//刚断开连接
                nowsState = SipConnectState.disconnected;
//                JCChatManager.nowsState = SipConnectState.disconnected;
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SIP_CONNECT_NO,""));
            }
        }else {
            if (null!=JCChatManager.getmAllOperateListener())JCChatManager.getmAllOperateListener().sipConnectState(SipConnectState.connected);
            if (nowsState==SipConnectState.disconnected){//刚重新连接
                nowsState = SipConnectState.connected;
//                JCChatManager.nowsState = SipConnectState.connected;
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SIP_CONNECT_OK,""));
            }
        }
    }

    private void initWindowOrNotify(Intent intent, boolean isVideo, String finalRemoteNum){

        JNLogUtil.e("=========showNotificationO==initWindowOrNotify==");
        intent.putExtra("ccctag", "Window");
        if (PermissionsUtil.checkFloatPermission(JCChatManager.mContext)) {
            JNLogUtil.e("=========showNotificationO==checkFloatPermission==");
            JNBaseUtilManager.createFloatView(intent, isVideo?1:0, finalRemoteNum);
            JNLogUtil.e("==RingtoneCommand03=======");
            JNPjSip.getInstance(JCChatManager.mContext).startRingtoneCommand();
        }else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                int num = JNBaseUtilManager.callNum++;
                intent.putExtra("notificationNum", num);
                JNBaseUtilManager.showNotificationO(intent, isVideo?1:0, num, finalRemoteNum, R.mipmap.ic_launcher);
                JNLogUtil.e("==RingtoneCommand04=======");
                JNPjSip.getInstance(JCChatManager.mContext).startRingtoneCommand();
            }else {
                JNLogUtil.e("=========showNotification====");
                JNBaseUtilManager.showNotification("来电通知！","你有一个来电",intent, R.mipmap.ic_launcher);
                JNLogUtil.e("==RingtoneCommand05=======");
                JNPjSip.getInstance(JCChatManager.mContext).startRingtoneCommand();
            }
        }
    }

}

package com.jndv.jndvchatlibrary.bean;

import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;

/**
 * Author: wangguodong
 * Date: 2022/12/13
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 位置信息实体类
 */
public class LocationBean {
    private double latitude = 0;// 定位坐标（百度）
    private double longitude = 0;// 定位坐标（百度）
    private int speed = -1;// 速度
    private int direction = -1;// 方向
    private int gpsAccuracyStatus;//重置
    private int locType;
    private String networkLocationType="";

    public LocationBean(double latitude, double longitude, int speed, int direction) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.direction = direction;
    }

    public LocationBean(double latitude, double longitude, int speed, int direction, int gpsAccuracyStatus, int locType, String networkLocationType) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.direction = direction;
        this.gpsAccuracyStatus = gpsAccuracyStatus;
        this.locType = locType;
        this.networkLocationType = networkLocationType;
    }

    public int getGpsAccuracyStatus() {
        return gpsAccuracyStatus;
    }

    public void setGpsAccuracyStatus(int gpsAccuracyStatus) {
        this.gpsAccuracyStatus = gpsAccuracyStatus;
    }

    public int getLocType() {
        return locType;
    }

    public void setLocType(int locType) {
        this.locType = locType;
    }

    public String getNetworkLocationType() {
        return networkLocationType;
    }

    public void setNetworkLocationType(String networkLocationType) {
        this.networkLocationType = networkLocationType;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}

package com.jndv.jndvchatlibrary.bean;

public class SessionDataBean {
    private int create_time;
    private String first_content;
    private int session_id;
    private String session_party_domain_addr;
    private String remark;
    private String session_party_domain_addr_user;
    private String type;
    private int is_no_disturb;
    private int unread_number;
    private int is_top;
    private String other_pic;
    private String user_id;
    private String last_time;
    private String first_time;
    private String other_name;
    private String other_id;
    private int status;

    public int getCreate_time() {
        return create_time;
    }

    public void setCreate_time(int create_time) {
        this.create_time = create_time;
    }

    public String getFirst_content() {
        return first_content;
    }

    public void setFirst_content(String first_content) {
        this.first_content = first_content;
    }

    public int getSession_id() {
        return session_id;
    }

    public void setSession_id(int session_id) {
        this.session_id = session_id;
    }

    public String getSession_party_domain_addr() {
        return session_party_domain_addr;
    }

    public void setSession_party_domain_addr(String session_party_domain_addr) {
        this.session_party_domain_addr = session_party_domain_addr;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSession_party_domain_addr_user() {
        return session_party_domain_addr_user;
    }

    public void setSession_party_domain_addr_user(String session_party_domain_addr_user) {
        this.session_party_domain_addr_user = session_party_domain_addr_user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIs_no_disturb() {
        return is_no_disturb;
    }

    public void setIs_no_disturb(int is_no_disturb) {
        this.is_no_disturb = is_no_disturb;
    }

    public int getUnread_number() {
        return unread_number;
    }

    public void setUnread_number(int unread_number) {
        this.unread_number = unread_number;
    }

    public int getIs_top() {
        return is_top;
    }

    public void setIs_top(int is_top) {
        this.is_top = is_top;
    }

    public String getOther_pic() {
        return other_pic;
    }

    public void setOther_pic(String other_pic) {
        this.other_pic = other_pic;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLast_time() {
        return last_time;
    }

    public void setLast_time(String last_time) {
        this.last_time = last_time;
    }

    public String getFirst_time() {
        return first_time;
    }

    public void setFirst_time(String first_time) {
        this.first_time = first_time;
    }

    public String getOther_name() {
        return other_name;
    }

    public void setOther_name(String other_name) {
        this.other_name = other_name;
    }

    public String getOther_id() {
        return other_id;
    }

    public void setOther_id(String other_id) {
        this.other_id = other_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}

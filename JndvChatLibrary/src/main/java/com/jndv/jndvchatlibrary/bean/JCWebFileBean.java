package com.jndv.jndvchatlibrary.bean;

public class JCWebFileBean {
    private String name = "";
    private String url = "";
    private String tailThumbnailUrl = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTailThumbnailUrl() {
        return tailThumbnailUrl;
    }

    public void setTailThumbnailUrl(String tailThumbnailUrl) {
        this.tailThumbnailUrl = tailThumbnailUrl;
    }
}

package com.jndv.jndvchatlibrary.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.io.Serializable;

/**
 * getSessionList接口、getDeptInfo接口
 * 获取到的信息都会转化为此实体对象
 * 但相同的sipID字段在两种情况下却没统一转化到一个字段上！！！！！
 * 一个IM_Number一个otherId，使用居然是分开的
 * 两种数据在同一个列表中，同时使用时必报空，必闪退！！！
 * 不知这坑关联多少，暂不给做合并，先兼容下别闪退吧。
 * */
public class OrganizationPersonBean implements MultiItemEntity, Serializable {

    //========TTS消息专用字段========
    public int id;
    public String deptname;
    public String account;
    public String nickName;
    public String avatarurl;
    public String mobile;
    public String IM_Number;
    public String Office_Number;
    public String Gateway_Number;
    public String Djj_Number;
    public String MobilePrefix;
    public String FSStatus;
    public String deptUserDomainAddr;
    public int itemType;//1:机构 2：人员

    //===========转发消息专用字段==============
    public String otherId;
    public String otherName;
    public String type;
    public int sessionId;

    @Override
    public int getItemType() {
        return itemType;
    }

    public String getIM_Number() {
        return IM_Number;
    }
}

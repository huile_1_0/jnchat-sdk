package com.jndv.jndvchatlibrary;

import android.app.Application;
import android.content.Intent;

import androidx.fragment.app.FragmentActivity;
import com.google.auto.service.AutoService;
import com.jndv.jnbaseutils.IBaseJNRouterChat;
import com.jndv.jnbaseutils.ui.base.JNBaseFragment;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipP2PActivity;

/**
 * Author: wangguodong
 * Date: 2022/12/12
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 模块路由器的实现类
 */
@AutoService(IBaseJNRouterChat.class)
public class JCBaseJNRouterChatImpl implements IBaseJNRouterChat {


    @Override
    public void initSip() {

    }

    @Override
    public void setTitleState(int intitleState) {

    }

    @Override
    public void initManager(Application application, boolean isOpenLog) {

    }

    @Override
    public void setService(String serviceUrl) {

    }

    @Override
    public void startUserSelectActivity(FragmentActivity activity, JNBaseFragment.FragmentSelect jnSelecteLitener, Object... objects) {
//        Bundle bundle = new Bundle();
//        bundle.putString("Type", "1");
//        bundle.putString("okStr", "确认");
//        try {
//            ChatContant.TRANSFER_PERSON_LIST_OLD.clear();
//            List<OrganizationPersonBean> list = (List<OrganizationPersonBean>) objects[0];
//            ChatContant.TRANSFER_PERSON_LIST_OLD.addAll(list);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
////        JNOrganizationSelectFragment jnOrganizationSelectFragment
//        JCUserSelecteFragment jcUserSelecteFragment = new JCUserSelecteFragment();
//        JNBaseFragmentActivity.start(activity, jcUserSelecteFragment, "组织结构", bundle, jnSelecteLitener, true);
    }

    //  发送站内消息
    @Override
    public void sendMsg(int type, String content) {
        //发送站内消息:type为0=文字；1=tts；2=图片；3=传入整体消息格式
        if (0==type){
            sendMsgText(content);
        }else if (1==type){
            sendMsgTTS(content);
        }else if (2==type){
            sendMsgImg(content);
        }else {
//            sendMsg(content);
        }
//        JMMessageUtils.sendMessage(msgbean, JnMeetManager.mContext);
    }

    @Override
    public String getRouterName() {
        return "ChatRouter";
    }

    @Override
    public void startUserCall(String number) {
        Intent intent;
        intent = new Intent(JCChatManager.mContext, JCPjSipP2PActivity.class);
        intent.putExtra("tag", "outing");
        intent.putExtra("number", number);
        JCChatManager.mContext.startActivity(intent);
    }

    public void sendMsgText(String content) {

    }

    public void sendMsgTTS(String content) {

    }

    public void sendMsgImg(String content) {

    }

}

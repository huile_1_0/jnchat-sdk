package com.jndv.jndvchatlibrary;

import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.baidu.location.LocationClient;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.ehome.manager.JNPjSip;
import com.jndv.jnbaseutils.http.api.JNSaveBusinessApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.ehome.sipservice.CallInfoState;
import com.ehome.sipservice.VpnConnectState;
import com.google.gson.Gson;
import com.hjq.gson.factory.GsonFactory;
import com.hjq.http.EasyConfig;
import com.hjq.http.EasyHttp;
import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestInterceptor;
import com.hjq.http.listener.OnHttpListener;
import com.hjq.http.model.HttpHeaders;
import com.hjq.http.model.HttpParams;
import com.hjq.http.request.GetRequest;
import com.hjq.http.request.PostRequest;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNGetServerInfo;
import com.jndv.jnbaseutils.http.api.JNGetUserDetails;
import com.jndv.jnbaseutils.http.api.JNUserLoginToken;
import com.jndv.jnbaseutils.http.model.JNRequestHandler;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jnbaseutils.utils.PermissionsUtil;
import com.jndv.jnbaseutils.utils.SSLSocketClient;
import manager.JNVpnListener;
import manager.JNVpnManager;
import com.jndv.jndvchatlibrary.bean.LocationBean;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipP2PActivity;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipVideoActivity;
import com.jndv.jndvchatlibrary.http.model.JCRequestServer;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCMsgNotificationConfig;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgLoginContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCChatMsgFactory;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCconstants;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCmyPjSipMessageListener;
import com.jndv.jndvchatlibrary.ui.location.service.LocationService;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;
import com.jndv.jndvchatlibrary.utils.JSharedPreferences;
import com.jndv.jndvchatlibrary.utils.MqttConstant;
//import com.jndv.androidvideotranscodelib.FFmpegTranscodeManager;
//import com.jndv.androidvideotranscodelib.callback.StreamDataCallback;
//import com.jndv.jndvgb28181sdk.JNGBManager;
//import com.jndv.jndvgb28181sdk.bean.ServerInfoBean;
//import com.jndv.jndvgb28181sdk.callback.DeviceLocationCallback;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.tencent.mmkv.MMKV;
//import com.wgd.wgdfilepickerlib.WGDPickerManager;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;


/**
 * Author: wangguodong
 * Date: 2022/2/12
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 即时通讯模块管理工具类，进行一些配置及初始化
 */
public class JCChatManager {
    private static String TAG = "JCChatManager";
    public static Context mContext;
    public static int pageSize = 20;//分页数据中每页数据量
    public static int pageSizeSession = 200;//会话列表分页数据中每页数据量
    private static Application mApplication;

    private static JCMsgNotificationConfig jcMsgNotificationConfig;
    public static LocationService locationService;

//    sip注册状态
//    public static int nowsState = SipConnectState.disconnected;

    static NetworkConnectChangedReceiver mNetworkChangeReceiver = new NetworkConnectChangedReceiver();

    public static boolean showMonitor = false;
    private static boolean cleanMeetinged = false;
    static boolean islogOut = false ;//sip登陆是否已退出，这里主要是因为java服务有时会间隔100ms发送异地登录消息几百条（不要想着服务端解决问题，所以这里加兼容处理）
    static BasePopupView popupView = null;
//    static AppCompatActivity JNBaseUtilManager.getActivity();
    static String inspurName;
    static String inspurPass;
    static LoginListener mLoginListener;
    static boolean isGetServer = false ;//是否正在获取服务器信息
    static boolean isInMethod = false ;//
    static boolean isUseVpn = true ;//是否使用VPN
    public static String VPN_FILE_NAME = "vpn_dev.ovpn";
    static Handler hanler=new Handler();

    public static int getTitleState() {
        return JNBaseUtilManager.getTitleState();
    }

    /**
     * 设置标题栏类型
     * 会初始化标题栏背景色、图标及文字颜色等
     *
     * @param intitleState
     */
    public static void setTitleState(int intitleState) {
        JNBaseUtilManager.setTitleState(intitleState);
    }

    public static int getTitleColor() {
        return JNBaseUtilManager.getTitleColor();
    }

    public static void setTitleColor(int titleColor) {
        JNBaseUtilManager.setTitleColor(titleColor);
    }

    public static int getTitleTVColor() {
        return JNBaseUtilManager.getTitleTVColor();
    }

    public static void setTitleTVColor(int titleTVColor) {
        JNBaseUtilManager.setTitleTVColor(titleTVColor);
    }

    public static int getTitleIVBack() {
        return JNBaseUtilManager.getTitleIVBack();
    }

    public static void setTitleIVBack(int titleIVBack) {
        JNBaseUtilManager.setTitleIVBack(titleIVBack);
    }

    public static int getTitleIVMenu() {
        return JNBaseUtilManager.getTitleIVMenu();
    }

    public static void setTitleIVMenu(int titleIVMenu) {
        JNBaseUtilManager.setTitleIVMenu(titleIVMenu);
    }

    public static JCMsgNotificationConfig getJcMsgNotificationConfig() {
        return jcMsgNotificationConfig;
    }

    public static Application getApplication() {
        return mApplication;
    }

    public interface LoginListener {
        void onSuccess();

        void onFail();
    }

    public interface VpnListener {
        void onSuccess();

        void onFail();

        void onDisConnect();
    }

    private static VpnListener mVpnListener;

    public interface LocationListener {
        LocationBean onLocation();

        void onChangeFrequency(int num);
    }

    private static LocationListener mLocationListener;

    private static AllOperateListener mAllOperateListener;

    public static void setAllOperateListener(AllOperateListener allOperateListener) {
        mAllOperateListener = allOperateListener;
    }

    public static AllOperateListener getmAllOperateListener() {
        return mAllOperateListener;
    }

    /**
     * 全局监听器
     * 应该是abstract类，除部分必须的监听方法，方法应不强制监听
     */
    public abstract static class AllOperateListener {
        /**
         * 消息页面消息列表得点击事件回调
         *
         * @param jcSessionListBean
         * @return false=未处理，则需要默认处理方式；true=已处理，将不走默认处理方式
         */
        public boolean chatListOnClickItem(JCSessionListBean jcSessionListBean) {
            return false;
        }

        /**
         * 账号在其他地方登录，此设备账号被顶出
         */
        public abstract void outLoginOther();

        /***
         * sip通道的链接状态
         * @param state
         */
        public abstract void sipConnectState(int state);

        /**
         * VPN 链接状态改变
         * 如果监听了此接口，请谨慎接收状态改变广播！
         *
         * @param state 0=断线；1=在线
         */
        public abstract void vpnConnectState(int state);

        /**
         * 会话列表拉取信息状态
         *
         * @param state 0=结束；1=拉取中
         */
        public void getChatListState(int state) {
        }

        /**
         * 跳转显示url的web页面activity
         *
         * @param url
         * @param title
         * @return false=未处理，则需要默认处理方式；true=已处理，将不走默认处理方式
         */
        public boolean webShowActivity(String url, String title, boolean ShowTitle) {
            return false;
        }
    }

    private static boolean notSetDefault = true;

    private static JNPjSip.PjSipListener myPjSipListener;
    private static JNPjSip.PjSipListener registPjSipListener;

    /**
     * 即时通讯模块初始化
     *
     * @param application Application
     * @param
     */
    public static void init(Application application, String appKey, String sourceName, String loginServerUrl, boolean isOpenLog
    ) {
        JNLogUtil.isOpenLog = isOpenLog;
        mApplication = application;
        mContext = application.getApplicationContext();
//        JNBaseUtilManager.filesServerUrl = serverUrl;
//        if (!JNBaseUtilManager.filesServerUrl.endsWith("/")) {
//            JNBaseUtilManager.filesServerUrl = JNBaseUtilManager.filesServerUrl + "/";
//        }
        if (loginServerUrl.endsWith("/")) {
            loginServerUrl = loginServerUrl.substring(0, loginServerUrl.length() - 1);
        }
//        JNBasePreferenceSaves.saveJavaUrl(serverUrl);
//        JCconstants.setHostUrl(serverUrl);//设置网络框架的url
        JNBaseUtilManager.init(application, appKey, sourceName, loginServerUrl);
        JNVpnManager.getInstance().initVPN(application);
        JCChatMsgFactory.init();//初始化消息管理工厂
        // MMKV 初始化
        MMKV.initialize(mContext);

        //网络框架初始化
//        initEasyHttp(application);
        //初始化下拉刷新组件
//        initSmartRefresh(application);

//        WGDPickerManager.getInstance().setMaxCount(1);
//        WGDPickerManager.getInstance().setmFileTypesAll(
//                new String[]{"m4a", "mid", "xmf", "ogg", "wav", "mp3", "jpg", "gif", "png", "jpeg", "bmp"
//                        , "apk", "ppt", "pptx", "xls", "xlt", "xlsx", "xltx", "doc", "docx", "dot", "dotx"
//                        , "pdf", "txt", "chm", "mp4", "avi", "3gp"});

        initSDK(true);//初始化百度地图
        JNBaseUtilManager.initManager(application, isOpenLog);//初始化会议模块
//        JNBaseUtilManager.setService(serverUrl);
//        if (1 == JNSpUtils.getInt(mApplication, JNPjSipConstants.PJSIP_ISLOGIN, 0)) {
//            JNLogUtil.d("JCChatManager==initsip...");
//        }
        JNBaseUtilManager.initWindowManager();
    }
    /**
     * 即时通讯模块初始化
     *
     * @param application Application
     *
     */
    public static void init(Application application, String appKey, String sourceName, boolean isOpenLog
    ) {
        JNLogUtil.d("life", "==JCChatManager==init==");
        JNLogUtil.isOpenLog = isOpenLog;
        mApplication = application;
        mContext = application.getApplicationContext();
        JNBaseUtilManager.init(application, appKey, sourceName, "");
        JNVpnManager.getInstance().initVPN(application);
        JCChatMsgFactory.init();//初始化消息管理工厂
        // MMKV 初始化
        MMKV.initialize(mContext);

        //网络框架初始化
//        initEasyHttp(application);
        //初始化下拉刷新组件
//        initSmartRefresh(application);

//        WGDPickerManager.getInstance().setMaxCount(1);
//        WGDPickerManager.getInstance().setmFileTypesAll(
//                new String[]{"m4a", "mid", "xmf", "ogg", "wav", "mp3", "jpg", "gif", "png", "jpeg", "bmp"
//                        , "apk", "ppt", "pptx", "xls", "xlt", "xlsx", "xltx", "doc", "docx", "dot", "dotx"
//                        , "pdf", "txt", "chm", "mp4", "avi", "3gp"});

        initSDK(true);//初始化百度地图
        JNBaseUtilManager.initManager(application, isOpenLog);//初始化会议模块
        JNBaseUtilManager.initWindowManager();
    }

    public static void initActivity(AppCompatActivity activity){
        try {
            JNBaseUtilManager.setActivity(activity);
            JNVpnManager.getInstance().initActivity(JNBaseUtilManager.getActivity(), new JNVpnListener() {
                @Override
                public void onState(String state) {
                    setStatus(state);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void initVPN(String userId, String psw, String vpnName, AppCompatActivity activity, VpnListener vpnListener) {
        if (!isInMethod)initActivity(activity);
        initVPN(userId, psw, vpnName, vpnListener);
    }
    public static void initVPN(String userId, String psw, String vpnName, VpnListener vpnListener) {
        try {
            mVpnListener = vpnListener;
//            if (null == mAppPackage || TextUtils.isEmpty(mAppPackage)) {
            if (null == JNBaseUtilManager.getActivity() ) {
                if (null != mVpnListener) mVpnListener.onFail();
            } else {
                JNVpnManager.getInstance().startVpn(userId, psw, vpnName);
            }
        } catch (Exception e) {
            JNLogUtil.e("=JCChatManager=====", e);
        }

    }

    public static void onDestroy(AppCompatActivity activity) {
        try {
            if (mNetworkChangeReceiver != null) {
                activity.unregisterReceiver(mNetworkChangeReceiver);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            EventBus.getDefault().unregister(activity);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static boolean isConnect() {
        return JNPjSip.getInstance(mContext).isSipConnect() && isVpnConnect();
    }

    public static boolean isSipConnect() {
        return JNPjSip.getInstance(mContext).isSipConnect() ;
    }

    public static boolean isVpnConnect() {
        if (!isUseVpn){
            return true;
        }
        String state = JNVpnManager.getInstance().getStatus();
        if (TextUtils.equals(state, "CONNECTED")) {
            return true;
        }
        return false;
    }

    public static void setStatus(String connectionState) {
        if (connectionState != null) {
            JNLogUtil.e("chat202203", "=JCChatManager=vpn=connectionState: " + connectionState);
            switch (connectionState) {
                case "DISCONNECTED":
                    if (null != mVpnListener) mVpnListener.onDisConnect();
                    break;
                case "CONNECTED":
                    Toast.makeText(JNBaseUtilManager.getActivity(), "vpn已连接", Toast.LENGTH_SHORT).show();
                    //这里需要发送粘性事件
//                    if (!TextUtils.equals("http://192.168.1.1", JNBaseConstans.getHostUrl())){
//                        EventBus.getDefault().postSticky(new JNCodeEvent<>(JNEventBusType.CODE_SESSION_LIST_UPDATE_NET, ""));
//                    }
                    if (mVpnListener == null) {
                        Log.d("jnpjsip", "mVpnListener==null");
                        if (null != mAllOperateListener)
                            mAllOperateListener.vpnConnectState(VpnConnectState.connected);
                        JNBaseUtilManager.setIsVpnDisConnect(false);
                        EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_VPN_CONNECT_OK,"VPN注册成功"));
                        initPJSIP();
                    } else {
                        Log.d("jnpjsip", "mVpnListener不是null");
                        mVpnListener.onSuccess();
                    }
                    break;
                case "WAIT"://等待
                    break;
                case "AUTH"://认证
                    break;
                case "RECONNECTING"://正在重连
                    break;
                case "AUTH_FAILED"://鉴权失败
                    showToast("账号密码错误！");
                    if (null != mVpnListener) mVpnListener.onFail();
                    break;
                case "NONETWORK"://无网络
                    if (null != mVpnListener) mVpnListener.onDisConnect();
                    break;
                default:
                    JNLogUtil.e("chat202203", "=JCChatManager=vpn=connectionState=default=: " + connectionState);
                    if (null != mVpnListener) mVpnListener.onDisConnect();
            }
        }
    }

    public static void setLocationListener(LocationListener locationListener) {
        mLocationListener = locationListener;
    }

    public static LocationListener getmLocationListener() {
        return mLocationListener;
    }

    /**
     * 初始化地图SDK
     *
     * @param status
     */
    private static void initSDK(boolean status) {
        LocationClient.setAgreePrivacy(status);
        SDKInitializer.setAgreePrivacy(getApplication(), status);
        JCChatManager.locationService = new LocationService(getApplication());
        try {
            SDKInitializer.initialize(getApplication());
            SDKInitializer.setCoordType(CoordType.BD09LL);
            SDKInitializer.setHttpsEnable(true);
            Log.e("JCChatManager", "initSDK: =========isHttpsEnable==" +SDKInitializer.isHttpsEnable());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void initCalling() {
        JNLogUtil.e("=====01====initCalling==00000001111==");
        if (!TextUtils.isEmpty(JNBaseUtilManager.getInPhoneNumber())) {
            if (JNBaseUtilManager.getInPhoneNumber().length() == 9) {
                JNBaseUtilManager.initMeetCalling();
            } else {
                Intent intent;
                if (JNBaseUtilManager.isIsVideo()) {
                    intent = new Intent(JCChatManager.mContext, JCPjSipVideoActivity.class);
                } else {
                    intent = new Intent(JCChatManager.mContext, JCPjSipP2PActivity.class);
                }
                intent.putExtra("tag", "incoming");
                intent.putExtra("number", JNBaseUtilManager.getInPhoneNumber());
//                CallInfoState callInfoState = JNPjSip.getInstance(mContext).getCallInfoState(JNBaseUtilManager.getInPhoneNumber());
                try {
                    CallInfoState callInfoState = JNPjSip.getInstance(mContext).getCallInfoState(JNBaseUtilManager.getInPhoneNumber());
                    if (null!=callInfoState){
                        intent.putExtra("CallInfoState", callInfoState);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        if (PermissionsUtil.isAppRunning()) {//在前台
                            intent.putExtra("ccctag", "startActivity");
                            JNBaseUtilManager.getActivity().startActivity(intent);
                            JNLogUtil.e("=====01====initCalling==00000001==");
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

//    /**
//     * 国标28181SDK
//     * 启动国标28181服务
//     *
//     * @param serverInfoBean
//     */
//    public static void startGBSDK(ServerInfoBean serverInfoBean) {
//        JNGBManager.getInstance().initJNGBSdk_jna(serverInfoBean);
//    }
//
//    /**
//     * 国标28181SDK
//     * 设置国标28181服务的位置信息上报监听
//     *
//     * @param deviceLocationCallback
//     */
//    public static void setGBSDKLocationCallBack(DeviceLocationCallback deviceLocationCallback) {
//        JNGBManager.getInstance().JNGB_SetDeviceInfoCallback_jna(deviceLocationCallback);
//    }
//
//    /**
//     * 国标28181SDK
//     * 发送H264视频流数据到国标28181平台
//     *
//     * @param data
//     * @param isIFrame
//     * @param iDataLen
//     * @param pts
//     */
//    public static void sendGBSDKVideo(byte[] data, boolean isIFrame, int iDataLen, long pts) {
//        JNGBManager.getInstance().BK_28181_SendVideoData(data, iDataLen, isIFrame, pts);
//    }
//
//    /**
//     * 国标28181SDK
//     * 将url中的视频流数据发送到国标28181平台
//     *
//     * @param url rtsp
//     */
//    public static void startGBSDKVideoTranscode(String url) {
//        FFmpegTranscodeManager.getInstance().startFFDemuxTranscode(url, new StreamDataCallback() {
//            @Override
//            public int onData(byte[] data, int isIFrame, int iDataLen, long pts, int state) {
//                new Handler(Looper.getMainLooper()).post(new Runnable() {
//                    @Override
//                    public void run() {
//                        JNGBManager.getInstance().BK_28181_SendVideoData(data, iDataLen, 1 == isIFrame, pts);
//                    }
//                });
//                return 0;
//            }
//
//            @Override
//            public void onFail(int state) {
//                new Handler(Looper.getMainLooper()).post(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(mContext, "视频流解码失败！", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        });
//    }


    /**
     * 设置选择文件时全部文件筛选条件
     *
     * @param mFileTypesAll
     */
    public static void setFileMsgAllTypes(String[] mFileTypesAll) {
//        WGDPickerManager.getInstance().setmFileTypesAll(mFileTypesAll);
    }

    /**
     * 设置即时通讯SDK的业务服务地址
     * 如果文件服务地址为空也将设置为此地址
     * @param serverUrl
     */
    private static void setServiceUrl(String serverUrl){
        if (serverUrl.endsWith("/")) {
            serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
        }
//        JNBasePreferenceSaves.saveJavaUrl(serverUrl);
        Log.e("0417", "setServiceUrl: =====serverUrl=="+serverUrl );
        JNBasePreferenceSaves.saveJavaUrl(serverUrl);
        JCconstants.setHostUrl(serverUrl);//设置网络框架的url
        if (null==JNBaseUtilManager.filesServerUrl||TextUtils.isEmpty(JNBaseUtilManager.filesServerUrl)){
            JNBaseUtilManager.setFilesServerUrl(serverUrl);
        }
        JNBaseUtilManager.setService(serverUrl);
//        由于开启对讲功能时会立马请求对讲在Android设备上的状态，所以需要服务地址处理后再开启
        JNBaseUtilManager.startClusterIntercom(JNBaseUtilManager.getActivity(), true);
    }

    /**
     * 获取文件服务器地址
     *
     * @return
     */
//    public static String getFilesServerUrl() {
//        return JNBaseUtilManager.filesServerUrl;
//    }

    /**
     * 设置主会话Activity
     * 调用这个方法时一般是登陆页跳转到主页，不需要主页再次走登陆流程，
     * 所以这里需要单独尝试开启对讲
     * @param mActivity
     */
    public static void setActivity(AppCompatActivity mActivity) {
        JNBaseUtilManager.setActivity(mActivity);
        JNBaseUtilManager.startClusterIntercom(JNBaseUtilManager.getActivity(), true);
    }

    /**
     * 设置文件服务器地址
     *
     * @param filesServerUrl
     */
    public static void setFilesServerUrl(String filesServerUrl) {
        JNBaseUtilManager.setFilesServerUrl(filesServerUrl);
    }

    /**
     * 初始化消息通知信息
     *
     * @param iconId
     * @param title
     * @param pendingIntent PendingIntent.getActivity(context, 1, new Intent(context, MainActivity.class), PendingIntent.FLAG_CANCEL_CURRENT)
     */
    public static void initMsgNotification(int iconId, String title, PendingIntent pendingIntent) {
        jcMsgNotificationConfig = new JCMsgNotificationConfig(iconId, title, pendingIntent, null);
    }

    public static void initMsgNotification(int iconId, String title, Intent intent) {
        jcMsgNotificationConfig = new JCMsgNotificationConfig(iconId, title, null, intent);
    }


    private static void doLoginToken(){
        doLoginToken(null);
    }
    private static void doLoginToken(OnHttpListener mOnHttpListener){
        if (null==mOnHttpListener){
            mOnHttpListener = new OnHttpListener<JNUserLoginToken.Bean>() {
                @Override
                public void onSucceed(JNUserLoginToken.Bean result) {
                    if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                        JNBaseUtilManager.saveToken(result.getData());
                        loginUserBase();
                    }else {
                        if (null!=mLoginListener)mLoginListener.onFail();
                    }
                }

                @Override
                public void onFail(Exception e) {
                    if (null!=mLoginListener)mLoginListener.onFail();
//                        Toast.makeText(LoginActivity.this, "登录失败", Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"onFail..."+e);
//                        cancelFailLoadding();
                }
            };
        }

        JNUserLoginToken jnUserLoginToken = new JNUserLoginToken()
                .setAccount(inspurName)
                .setPassword(inspurPass)
                .setSourceName(JNBaseUtilManager.getSourceName());
        try {
            PostRequest postRequest = EasyHttp.post(JNBaseUtilManager.getActivity());
            if (JNBaseUtilManager.getLoginServerUrl() != null&& !TextUtils.isEmpty(JNBaseUtilManager.getLoginServerUrl())) {
                postRequest.server(JNBaseUtilManager.getLoginServerUrl());
            }
            postRequest.api(jnUserLoginToken)
                    .json(new Gson().toJson(jnUserLoginToken))
                    .request(mOnHttpListener);

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    /**
     * 即时通讯功能模块
     * 用户登录
     * 模块初始化成功后必须登录
     *
     * @param
     */
    public static void reLoginUser() {
        try {
            loginUserSip();
        } catch (Exception e) {
            JNLogUtil.e("=JCChatManager=onResume=", e);
        }
    }

    /**
     * 即时通讯功能模块
     * 用户登录
     * 模块初始化成功后必须登录
     *
     * @param
     */
    public static void loginUser(String name, String pass, AppCompatActivity activity, boolean isInMethods, LoginListener loginListener) {
        try {
            loginUser(name, pass,"vpn.ovpn", activity, isInMethods, loginListener);
        } catch (Exception e) {
            JNLogUtil.e("=JCChatManager=onResume=", e);
        }
    }

    /**
     * 即时通讯功能模块
     * 用户登录
     * 模块初始化成功后必须登录
     *
     * @param
     */
    public static void loginUserNoVPN(String name, String pass, AppCompatActivity activity, boolean isInMethods, LoginListener loginListener) {
        try {
            isUseVpn = false ;
            mLoginListener = loginListener;
//            VPN_FILE_NAME = vpnName;
            JNBaseUtilManager.setActivity(activity);
            inspurName = name;
            inspurPass = pass;
            JNLogUtil.e("=JCChatManager=inspurPass=" + inspurPass);
            isInMethod = isInMethods;
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            activity.registerReceiver(mNetworkChangeReceiver, filter);
            if (null != mAllOperateListener)
                mAllOperateListener.vpnConnectState(VpnConnectState.connected);
            JNBaseUtilManager.setIsVpnDisConnect(false);
            doLoginToken();
        } catch (Exception e) {
            JNLogUtil.e("=JCChatManager=onResume=", e);
        }
    }

    /**
     * 即时通讯功能模块
     * 用户登录
     * 模块初始化成功后必须登录
     *
     * @param
     */
    public static void loginUser(String name, String pass, String vpnName, AppCompatActivity activity, boolean isInMethods, LoginListener loginListener) {
        try {
            isUseVpn = true ;
            mLoginListener = loginListener;
            VPN_FILE_NAME = vpnName;
            JNBaseUtilManager.setActivity(activity);
            inspurName = name;
            inspurPass = pass;
            JNLogUtil.e("=JCChatManager=inspurPass=" + inspurPass);
            isInMethod = isInMethods;
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            activity.registerReceiver(mNetworkChangeReceiver, filter);
            loginUserSip();
        } catch (Exception e) {
            JNLogUtil.e("=JCChatManager=onResume=", e);
        }
    }


    /**
     * 即时通讯功能模块
     * 用户登录
     * 模块初始化成功后必须登录
     *
     * @param
     */
    private static void loginUserSip() {
        JNLogUtil.d("jnpjsip", "=JCChatManager==loginUser: ==1====");

        mVpnListener = new VpnListener() {
            @Override
            public void onSuccess() {
                JNLogUtil.d("chat202203", "=JCChatManager==initVPN:onSuccess: ===loginUser==");
                if (null != mAllOperateListener)
                    mAllOperateListener.vpnConnectState(VpnConnectState.connected);
                JNBaseUtilManager.setIsVpnDisConnect(false);
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_VPN_CONNECT_OK,"VPN注册成功"));
                JCThreadManager.onMainHandler(new Runnable() {
                    @Override
                    public void run() {
//                        loginUser(userId, psw, EasyHttp.get(activity));
                        doLoginToken();
                    }
                }, 2000);//这里有的手机有的版本会出现vpn连接成功，但是接口请求失败的情况。经试验，延迟2s请求该接口能成功
            }

            @Override
            public void onFail() {
                Log.e("chat202203", "onFail: vpnConnectState: ============");
                if (null != mLoginListener) mLoginListener.onFail();
            }

            @Override
            public void onDisConnect() {
                Log.e("chat202203", "onDisConnect: vpnConnectState: ============");
                if (null != mAllOperateListener)
                    mAllOperateListener.vpnConnectState(VpnConnectState.disconnected);
                JNBaseUtilManager.setIsVpnDisConnect(true);
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_VPN_CONNECT_NO,"VPN注册失败"));
            }
        };
        if (!JNVpnManager.getInstance().isConnect()) {
            Log.e("chat202203", "vpnStart: vpnConnectState: ==001==");
            initVPN(inspurName, inspurPass, VPN_FILE_NAME, JNBaseUtilManager.getActivity(), mVpnListener);
        } else {
            if (TextUtils.equals("CONNECTED", JNVpnManager.getInstance().getStatus())) {
//                偶现的VPN状态回调不到上层
                if (null != mAllOperateListener)
                    mAllOperateListener.vpnConnectState(VpnConnectState.connected);
                JNBaseUtilManager.setIsVpnDisConnect(false);
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_VPN_CONNECT_OK,"VPN注册成功"));
                Log.e("chat202203", "onDisConnect: vpnConnectState: ==002==");
            }
//            loginUser(userId, psw, EasyHttp.get(activity));
            doLoginToken();
        }

    }

    public static void showPermissionDialog(Activity activity) {
        popupView = new XPopup.Builder(activity)
                .asConfirm("权限提醒", "请打开悬浮窗权限" + (PermissionsUtil.isHaveBackPer() ? "和后台弹出界面权限" : "") + "，否则将不能后台接听通话！",
                        "取消", "确定",
                        new OnConfirmListener() {
                            @Override
                            public void onConfirm() {
                                if (null != popupView) popupView.dismiss();
                                JCChatManager.showToast("请打开悬浮窗权限" + (PermissionsUtil.isHaveBackPer() ? "和后台弹出界面权限" : "") + "，否则将影响后台接听通话！");
                                PermissionsUtil.gotoPermission();
                            }
                        }, new OnCancelListener() {
                            @Override
                            public void onCancel() {
                                if (null != popupView) popupView.dismiss();
                            }
                        }, false);
        if (null != popupView) popupView.show();
    }

    /**
     * 即时通讯功能模块
     * 用户登录
     * 模块初始化成功后必须登录
     */
    private static void loginUserBase() {
        try {
            JNGetUserDetails jnGetUserDetails = new JNGetUserDetails()
                    .setSourceName(JNBaseUtilManager.getSourceName());
            PostRequest postRequest = EasyHttp.post(JNBaseUtilManager.getActivity());
            if (JNBaseUtilManager.getLoginServerUrl() != null&& !TextUtils.isEmpty(JNBaseUtilManager.getLoginServerUrl())) {
                postRequest.server(JNBaseUtilManager.getLoginServerUrl());
            }
            postRequest
                    .api(jnGetUserDetails)
                    .json(new Gson().toJson(jnGetUserDetails))
                    .request(new OnHttpListener<JNGetUserDetails.Bean>() {
                        @Override
                        public void onSucceed(JNGetUserDetails.Bean result) {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                MMKV.defaultMMKV().putBoolean("USER_INIT_PSW", result.getData().isInitPwd());
                                MMKV.defaultMMKV().putString("USER_DEPT_TYPE", result.getData().getLyDeptType());
                                String organizationId="";
                                String organizationName="";
                                String zoningCode="";
                                isGetServer = true;
                                try {
                                    if (null!=result.getData()&&null!=result.getData().getOthers()&&result.getData().getOthers().size()>0){
                                        if (result.getData().getOthers().size()>1){
                                            String s = MMKV.defaultMMKV().getString(JNBaseConstans.MMKV_KEY_USER_ROLE_UUID+result.getData().getUuid(), "");
                                            Log.e(TAG, "onSucceed: ===loginUserBase===getUuid==" + s );
//                                        4b78be6f-033b-4397-9976-6e2cbe67bfdb
                                            if (null==s||TextUtils.isEmpty(s)){
                                                boolean isChange = false;
                                                for (JNGetUserDetails.UserOtherBean mUserOtherBean:result.getData().getOthers()
                                                ) {
                                                    if (TextUtils.equals("1", mUserOtherBean.getIsMain())){
                                                        changeDuRole(result.getData().getUuid(), mUserOtherBean);
                                                        isChange = true;
                                                    }
                                                }
                                                if (!isChange){
                                                    changeDuRole(result.getData().getUuid(), result.getData().getOthers().get(0));
                                                }
//                                            showPopup(result.getData().getOthers(), result.getData().getUuid());
                                            }else {
                                                JNGetUserDetails.UserOtherBean userOtherBean = null;
                                                for (JNGetUserDetails.UserOtherBean mUserOtherBean:result.getData().getOthers()
                                                ) {
                                                    if (TextUtils.equals(s, mUserOtherBean.getDuUuid())){
                                                        organizationId = mUserOtherBean.getDeptUuid();
                                                        zoningCode = mUserOtherBean.getSourceZoningCode();
                                                        userOtherBean = mUserOtherBean;
                                                    }
                                                }
                                                if (null==userOtherBean)userOtherBean = result.getData().getOthers().get(0);
                                                changeDuRole(result.getData().getUuid(), userOtherBean);
                                            }
                                        }else {
                                            changeDuRole(result.getData().getUuid(), result.getData().getOthers().get(0));
                                        }
                                    }else {
                                        JCChatManager.showToast("未找到用户身份信息！");
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                                Log.e(TAG, "onSucceed: =====organizationId=="+organizationId);
                                Log.e(TAG, "onSucceed: =====organizationName=="+organizationName);
//                                inspurName, inspurPass
                                initUserInfo(result.getData().getAccount(), result.getData().getIdCardNum()
                                        ,result.getData().getMobile(), result.getData().getHeadIcon()
                                        , result.getData().getNickname(), result.getData().getImNum()
                                        , inspurPass,result.getData().getUuid()
                                );
                                getServerInfo();
                            }else {
                                if (null!=mLoginListener)mLoginListener.onFail();
                            }
                        }

                        @Override
                        public void onFail(Exception e) {
                            if (null!=mLoginListener)mLoginListener.onFail();
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void saveRole(String uuid, JNGetUserDetails.UserOtherBean userOtherBean){
//        JNBasePreferenceSaves.saveOrganzationId(userOtherBean.getDeptUuid());
        JNBasePreferenceSaves.saveZoningCode(userOtherBean.getSourceZoningCode());
        MMKV.defaultMMKV().putString(JNBaseConstans.MMKV_KEY_USER_ROLE_UUID+uuid, userOtherBean.getDuUuid());
        MMKV.defaultMMKV().putString(JNBaseConstans.MMKV_KEY_USER_ROLE_BEAN+uuid, new Gson().toJson(userOtherBean));
    }

    /**
     * 获取ServerInfo
     *
     */
    private static void getServerInfo() {
        try {
            JNGetServerInfo jnGetServerInfo = new JNGetServerInfo();
            GetRequest postRequest = EasyHttp.get(JNBaseUtilManager.getActivity());
            if (JNBaseUtilManager.getLoginServerUrl() != null&& !TextUtils.isEmpty(JNBaseUtilManager.getLoginServerUrl())) {
                postRequest.server(JNBaseUtilManager.getLoginServerUrl());
            }
            postRequest
//                    .server("http://api.jndv.org")
                    .api(jnGetServerInfo)
//                    .json(new Gson().toJson(jnGetServerInfo))
                    .request(new OnHttpListener<JNGetServerInfo.Bean>() {
                        @Override
                        public void onSucceed(JNGetServerInfo.Bean result) {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                initUrlConstant(result.getData());
                                EventBus.getDefault().postSticky(new JNCodeEvent<>(JNEventBusType.CODE_SESSION_LIST_UPDATE_NET, ""));

                                JNLogUtil.e("chat202212", "=JCChatManager==loginUser: ========0032====");
//                                Toast.makeText(mContext, "获取成功！", Toast.LENGTH_SHORT).show();
                                JCThreadManager.onMainHandler(new Runnable() {
                                    @Override
                                    public void run() {
                                        String deptType = MMKV.defaultMMKV().getString("USER_DEPT_TYPE", "0");
                                        if (!TextUtils.equals("9", deptType)){
                                            //    打开悬浮窗权限设置页，偶现的addView时WindowManager失败导致闪退问题，已经使用了VPNserver双进程守护了，这里可以考虑放弃使用
                                            if (!PermissionsUtil.checkFloatPermission(JCChatManager.mContext)
                                                    || !PermissionsUtil.isBackgroundStartAllowed()) {
                                                showPermissionDialog(JNBaseUtilManager.getActivity());
                                            }
                                        }

                                        String uuid = MMKV.defaultMMKV().getString(JNBaseConstans.MMKV_KEY_USER_UUID, "");
                                        String s = MMKV.defaultMMKV().getString(JNBaseConstans.MMKV_KEY_USER_ROLE_UUID+uuid, "");

                                        initPJSIP();
                                        if (null!=s&&!TextUtils.isEmpty(s)){
                                            if (null != mLoginListener) mLoginListener.onSuccess();
                                        }
                                        isGetServer = false;
                                    }
                                });
                                JNBaseUtilManager.initUseMap(mContext, MqttConstant.MQTT_SERVER, "admin", "Jndv@12345");
                                MQTTManager.getInstance().init(mContext);
                            }else {
                                if (null!=mLoginListener)mLoginListener.onFail();
                            }
                        }

                        @Override
                        public void onFail(Exception e) {
                            if (null!=mLoginListener)mLoginListener.onFail();
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 获取ServerInfo
     *
     */
    private static void changeDuRole(String uuid, JNGetUserDetails.UserOtherBean userOtherBean) {
        if (null==userOtherBean)return;
//        changeDuRole(uuid, userOtherBean.getDeptUuid(), userOtherBean.getSourceZoningCode(), userOtherBean.getDuUuid());
//    }
//
//    /**
//     *
//     * @param uuid
//     * @param organizationId
//     * @param zoningCode
//     * @param duUuid
//     */
//    private static void changeDuRole(String uuid, String organizationId, String zoningCode, String duUuid) {
        try {
            JNBaseHttpUtils.getInstance().ChangeDuRole(EasyHttp.post(JNBaseUtilManager.getActivity()), userOtherBean.getDuUuid(), new OnHttpListener<String>() {
                @Override
                public void onSucceed(String result) {
                    saveRole(uuid, userOtherBean);

                    if (!isGetServer&&null != mLoginListener) mLoginListener.onSuccess();
                }

                @Override
                public void onFail(Exception e) {
                    if (null!=mLoginListener)mLoginListener.onFail();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void initUrlConstant(List<JNGetServerInfo.ServerInfoBean> serverInfoList) {
        Log.e(TAG, "initUrlConstant: ===JCServerInfo==" + new Gson().toJson(serverInfoList));
        if(serverInfoList==null || serverInfoList.size()==0){
            return;
        }
        for(JNGetServerInfo.ServerInfoBean info:serverInfoList){
            JNBasePreferenceSaves.saveString(info.getServerType()+"*IP", info.getServerIp());
            JNBasePreferenceSaves.saveString(info.getServerType()+"*PORT", info.getPort());
            if(TextUtils.equals(info.getServerType(),"MQTT")){
                //MQTT地址
                MqttConstant.MQTT_SERVER="tcp://"+info.getServerIp()+":"+info.getPort();
                Log.e(TAG, "initUrlConstant: ===mqtturl==" + MqttConstant.MQTT_SERVER);
            }else if(TextUtils.equals(info.getServerType(),"IMAPI")){
                String ip = info.getServerIp();
                if (null!=info.getPort()&&!TextUtils.isEmpty(info.getPort())&&!TextUtils.equals("null", info.getPort())){
                    ip = ip+":"+info.getPort();
                }
                setServiceUrl(ip);
            }else if(TextUtils.equals(info.getServerType(),"IMS")){
                //sip地址
                String imHost=info.getServerIp();
                String imPort = info.getPort();
                Log.e(TAG, "initUrlConstant: ===imHost==" + imHost);
                Log.e(TAG, "initUrlConstant: ===imPort==" + imPort);
                if (!TextUtils.isEmpty(imPort))
                    JNSpUtils.setString(mContext, JNPjSipConstants.PJSIP_PORT, imPort);
                if (!TextUtils.isEmpty(imHost))
                    JNSpUtils.setString(mContext, JNPjSipConstants.PJSIP_HOST, imHost);
                String imHost2 = JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_HOST, JNPjSipConstants.PJSIP_HOST_DEFAULT);
                String imPort2 = JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_PORT, JNPjSipConstants.PJSIP_PORT_DEFAULT);
                Log.e(TAG, "initUrlConstant: ===imHost2==" + imHost2);
                Log.e(TAG, "initUrlConstant: ===imPort2==" + imPort2);
            }else if(TextUtils.equals(info.getServerType(),"MNAS")){
                //图片服务地址----------弃用
//                String ip = info.getServerIp();
//                if (null!=info.getPort()&&!TextUtils.isEmpty(info.getPort())&&!TextUtils.equals("null", info.getPort())){
//                    ip = ip + ":"+info.getPort();
//                }
//                setFilesServerUrl(ip);
                Log.e(TAG, "initUrlConstant: ===FilesServerUrl==" + JNBaseUtilManager.filesServerUrl);
            }
            setFilesServerUrl(JNBaseUtilManager.getLoginServerUrl());
        }
    }

    private static void initUserInfo(String account, String idCard,String mobile, String headUrl, String name
            , String imNumber, String psw, String userUUID
    ) {
        try {
            Log.e("chat202203", "loginUser: vpnConnectState: ==10002==account="+account+"=imNumber="+imNumber);
            JNBasePreferenceSaves.saveUserAccount(account);
            JNBasePreferenceSaves.saveUserId(imNumber);
            MMKV.defaultMMKV().putString(JNBaseConstans.MMKV_KEY_USER_UUID, userUUID);
            JNBasePreferenceSaves.saveUserMobil(mobile);
            JNBasePreferenceSaves.saveUserHead(headUrl);
            JNBasePreferenceSaves.saveUserName(name);
            JNBasePreferenceSaves.saveUserIdCard(idCard);

            JSharedPreferences.putConfigStrValue(JNBaseUtilManager.getActivity(), ChatContant.PREFERENCES_APP_ACCOUNT, account);
            JSharedPreferences.putConfigStrValue(JNBaseUtilManager.getActivity(), ChatContant.PREFERENCES_APP_NICKNAME, name);
            JSharedPreferences.putConfigStrValue(JNBaseUtilManager.getActivity(), ChatContant.PREFERENCES_APP_IDCARD, idCard);
            JSharedPreferences.putConfigStrValue(JNBaseUtilManager.getActivity(), ChatContant.PREFERENCES_APP_HEADICON, headUrl);

            JNSpUtils.setInt(mContext, JNPjSipConstants.PJSIP_ISLOGIN, 1);
            JNSpUtils.setString(mContext, JNPjSipConstants.PJSIP_NAME, name);
            JNSpUtils.setString(mContext, JNPjSipConstants.PJSIP_HEAD, headUrl);
            JNSpUtils.setString(mContext, JNPjSipConstants.PJSIP_NUMBER, imNumber);
            JNSpUtils.setString(mContext, JNPjSipConstants.PJSIP_PSWD, psw);

        } catch (Exception e) {
            JNLogUtil.e("=JCChatManager==initUserInfo==", e);
        }
    }

    public static void clearListeners(){
        try {
            JNPjSip.getInstance(mContext).clearPjSipListeners();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void initPJSIP() {
        String sipNum = JNSpUtils.getString(mContext, JNPjSipConstants.PJSIP_NUMBER);
        if (null==sipNum||sipNum.isEmpty()){
            return;
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    //登录SIP
                    Log.e("chat202203", "loginUser: vpnConnectState: ==10003==");
            JNLogUtil.e("=JCChatManager====initPJSIP===========");
                    JNPjSip pjSip = JNPjSip.getInstance(mContext);

                    if (null == myPjSipListener) {
                JNLogUtil.e("=JCChatManager===initPJSIP=====01======");
                        myPjSipListener = new MyPjSipListener();
                        pjSip.addPjSipListeners(myPjSipListener);
                    } else {
                JNLogUtil.e("=JCChatManager====initPJSIP=====02======");
                        pjSip.addPjSipListeners(myPjSipListener);
                        if (!notSetDefault) {
                            pjSip.addPjSipListeners(new MyPjSipListener());
                        }
                    }
                    if (null==registPjSipListener){
                        registPjSipListener = new JNPjSip.PjSipListener() {
                            @Override
                            public void onRegistration(boolean isSuccessed) {
                                JNLogUtil.e("pjsip","=JCChatManager===initPJSIP=====06===onRegistration==isSuccessed="+isSuccessed);
                                if (isSuccessed){
                                    islogOut = false;
                                    if (!cleanMeetinged){
                                        cleanMeetinged = true;
                                        JNBaseUtilManager.cleanMeeting(JNBaseUtilManager.getActivity());
                                    }

                                    JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                                            JNContentEncryptUtils.encodeContent(new Gson().toJson(new JCMsgLoginContentBean(JNBasePreferenceSaves.getUserAccount())))
                                            , JCmessageType.LOGIN_SUCCESS, "0123456789", JNBasePreferenceSaves.getUserSipId()
//                                            , JNBasePreferenceSaves.getSipAddress(), "0123456789", JNBasePreferenceSaves.getSipAddress()
                                            , JNBasePreferenceSaves.getSipAddress(), JNBasePreferenceSaves.getUserSipId(), JNBasePreferenceSaves.getSipAddress()
                                            , JCSessionType.CHAT_SYSTEM, JNBasePreferenceSaves.getJavaAddress(), JNBasePreferenceSaves.getJavaAddress());
                                    JCMessageUtils.sendMessage(jCimMessageBean, JCChatManager.mContext);

                                }
                            }

                            @Override
                            public void onIncomingCall(String accountID, int callID, String displayName, String remoteUri, boolean isVideo, boolean isMeeting, String msgHead) {

                            }

                            @Override
                            public void onOutgoingCall(String accountID, int callID, String number) {

                            }

                            @Override
                            public void onConnectState(int state) {

                            }
                        };
                    }
                    pjSip.addPjSipListeners(registPjSipListener);
                    try {
//                JNLogUtil.e("=JCChatManager===initPJSIP=====04======");
                        pjSip.addMessageListenersClean(new JCmyPjSipMessageListener());
                    } catch (Exception e) {
                        JNLogUtil.e("=JCChatManager===initPJSIP=====05======", e);
                        e.printStackTrace();
                    }
                    try {
                        JNLogUtil.e("initPJSIP","=JCChatManager===initPJSIP=====06======");
                        //先注册广播，保持监听状态
                        pjSip.sipBroadcastRegister();
                        pjSip.sipRegister();
                    } catch (Exception e) {
                        JNLogUtil.e("=JCChatManager===initPJSIP=====07======", e);
                        e.printStackTrace();
                    }
                    JNBaseUtilManager.initSip();
                } catch (Exception e) {
                    JNLogUtil.e("=JCChatManager==initPJSIP==", e);
                }
            }
        });
    }

    public static void loginOutOther() {
        try {
            showToast("你的账号在其他设备登录！");
            logOut();
            EventBus.getDefault().post(new JNCodeEvent<String>(JNEventBusType.CODE_SIP_LOGIN_OUT, ""));
            if (null != mAllOperateListener) mAllOperateListener.outLoginOther();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==logOut==", e);
        }
    }

    public static void logOut() {
        try {
            JNSpUtils.setInt(mContext, JNPjSipConstants.PJSIP_ISLOGIN, 0);
            JNPjSip.getInstance(mContext).logOut();
            if (!islogOut){
                islogOut = true;
            }
            MMKV.defaultMMKV().encode("loginMsgId","");
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==logOut==SipServiceCommand==", e);
        }
    }

    private static void initEasyHttp(Application application) {
        try {
            // 网络请求框架初始化
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .sslSocketFactory(SSLSocketClient.getSSLSocketFactory())
                    .hostnameVerifier(SSLSocketClient.getHostnameVerifier())
                    .build();

            EasyConfig.with(okHttpClient)
                    // 是否打印日志
                    .setLogEnabled(true)
                    // 设置服务器配置
                    .setServer(new JCRequestServer())
                    // 设置请求处理策略
                    .setHandler(new JNRequestHandler(application))
                    // 设置请求重试次数
                    .setRetryCount(2)
                    .setInterceptor(new IRequestInterceptor() {
                        @Override
                        public void interceptArguments(IRequestApi api, HttpParams params, HttpHeaders headers) {
                            headers.put(JNBaseConstans.KEY_APPKEY, JNBaseUtilManager.getAppKey());
                            headers.put(JNBaseConstans.KEY_TOKEN, JNBaseUtilManager.getToken());
                        }
                    })
                    .into();

            // 设置 Json 解析容错监听
            GsonFactory.setJsonCallback((typeToken, fieldName, jsonToken) -> {
                // 上报到 Bugly 错误列表
//            CrashReport.postCatchedException(new IllegalArgumentException(
//                    "类型解析异常：" + typeToken + "#" + fieldName + "，后台返回的类型为：" + jsonToken));
            });
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==initEasyHttp===", e);
        }
    }

//    private static void initSmartRefresh(Application application) {
//        // 设置全局的 Header 构建器
//        SmartRefreshLayout.setDefaultRefreshHeaderCreator((cx, layout) ->
//                new MaterialHeader(application).setColorSchemeColors(ContextCompat.getColor(application, R.color.colorPrimaryChat)));
//        // 设置全局的 Footer 构建器
//        SmartRefreshLayout.setDefaultRefreshFooterCreator((cx, layout) -> new JNSmartBallPulseFooter(application));
//        // 设置全局初始化器
//        SmartRefreshLayout.setDefaultRefreshInitializer((cx, layout) -> {
//            // 刷新头部是否跟随内容偏移
//            layout.setEnableHeaderTranslationContent(true)
////                    // 刷新尾部是否跟随内容偏移
//                    .setEnableFooterTranslationContent(true)
////                    // 加载更多是否跟随内容偏移
//                    .setEnableFooterFollowWhenNoMoreData(true)
////                    // 内容不满一页时是否可以上拉加载更多
//                    .setEnableLoadMoreWhenContentNotFull(false)
////                    // 仿苹果越界效果开关
//                    .setEnableOverScrollDrag(false);
//        });
//    }

    public static void showToast(String text) {
        JCThreadManager.showMainToast(text);
    }

    public static void showToastLong(String text) {
        JCThreadManager.showMainToastLong(text);
    }

}

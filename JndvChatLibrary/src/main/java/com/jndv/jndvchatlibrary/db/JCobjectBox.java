package com.jndv.jndvchatlibrary.db;

import android.content.Context;
//import com.jndv.jndvchatlibrary.ui.chat.bean.MyObjectBox;

import com.jndv.jnbaseutils.db.JNObjectBox;

import io.objectbox.BoxStore;

/**
 * Author: wangguodong
 * Date: 2022/2/12
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 数据库管理类
 */
public class JCobjectBox {
//    private static BoxStore mBoxStore;
//
//    public static void init(Context context) {
//        if (null==mBoxStore)mBoxStore = MyObjectBox.builder()
//                .androidContext(context.getApplicationContext())
//                .build();
//    }
//
//    public static BoxStore get() { return mBoxStore; }
    public static BoxStore get() {
        return JNObjectBox.getBoxStore(); }
}

package com.jndv.jndvchatlibrary;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.example.jncontrolmqttmanager.mqtt.JNMqttListener;
import com.example.jncontrolmqttmanager.mqtt.MQTTSDK;
import com.example.jncontrolmqttmanager.mqtt.Qos;
import com.example.jncontrolmqttmanager.mqtt.Topic;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgWebNoticeContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.utils.MqttConstant;
import com.tencent.mmkv.MMKV;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MQTTManager {

    private static final String TAG = "MQTTManager";
    private static final String TOPIC_MSG_HANDLING = "handling";
    public static MQTTManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final MQTTManager INSTANCE = new MQTTManager();
    }
    public  String getTopicPhone(String number, String topic){
        return Topic.getTopic("phone", number, topic);
    }

    HashMap<String, MqttMsgListener> map = new HashMap<>();
    private List<MqttMsgListener> mMqttMsgListenerList = new ArrayList<>();

    public interface MqttMsgListener{
        void onReceiveMessage(JCbaseIMMessage message, int isLook);
    }

    public void addMqttMsgListenerList(String key, MqttMsgListener mMqttMsgListener) {
//TODO        因为现在系统的多域逻辑狗屁不是，所以这里直接切去域地址，不在应用多域逻辑
        key = key.substring(0, key.lastIndexOf("_"));
        Log.e(TAG, "addMqttMsgListenerList: ==========key="+key);
        map.put(key, mMqttMsgListener);
    }
    public void removeMqttMsgListenerList(String key) {
//TODO        因为现在系统的多域逻辑狗屁不是，所以这里直接切去域地址，不在应用多域逻辑
        key = key.substring(0, key.lastIndexOf("_"));
        map.remove(key);
    }

    public void addMqttMsgListenerListAll(MqttMsgListener mMqttMsgListener) {
        this.mMqttMsgListenerList.add(mMqttMsgListener);
    }
    public void removeMqttMsgListenerListAll(MqttMsgListener mMqttMsgListener) {
        this.mMqttMsgListenerList.remove(mMqttMsgListener);
    }

    public void init(Context context){
        initMQTT(context, MqttConstant.MQTT_SERVER, "admin", "Jndv@12345");
    }
    public void init(Context context, MqttMsgListener mqttMsgListener){
        addMqttMsgListenerListAll(mqttMsgListener);
        initMQTT(context, MqttConstant.MQTT_SERVER, "admin", "Jndv@12345");
    }

    public void initMQTT(Context context, String server, String name, String pass){
        MQTTSDK.getInstance().init(context, server, name, pass, true ,"Main", new JNMqttListener() {

            @Override
            public void onSuccess() {
//                alarm/990213355059341/handling
//                alarm/d56347e2-0fca-45b6-b526-22b0486c92df/handling
//                {"text":"任务测试啦啦啦啦啦啦啦","type":"日常任务","sipParameterJson":{"bigType":"task","bigTypeTitle":"任务","displayMode":"Popup","eventczsx":"","id":"217614a6-a2d6-48ee-afca-5ac7108e6e91","info":"任务测试啦啦啦啦啦啦啦","jsOrgOrStation":"2","lat":"","lon":"","remark":"","secondType":"日常任务","seq":"-671716434","sjLevel":"","smallType":"待办任务","url":"%2fadmin_old%2fsrc%2fnoticeAndTask%2ftaskDetail.html%3fid%3d217614a6-a2d6-48ee-afca-5ac7108e6e91","urlMobile":"%2fadmin%2f%23%2fapp%2fbms%2frenwuguanli%2ftaskList_detail.vue%3fid%3d217614a6-a2d6-48ee-afca-5ac7108e6e91","userAccount":"dgpcs","userContact":"","userDepartCode":"371302003D001","userDepartName":"东关派出所","userName":"东关派出所管理员","userSipId":"10002","userSipType":3,"users":[{"account":"zt","departCode":"","departName":"","head":"","name":"zt","uid":"990213355059341"}],"msgId":"89c6756d-9de6-46f3-b8f8-8aa2ad3a8bb7"},"fromName":"任务"}
                Log.d(TAG,"initMQTT...success..gps..account=="+ JNBasePreferenceSaves.getUserAccount());
                String account = JNBasePreferenceSaves.getUserAccount();
                MQTTSDK.getInstance().subscribe(Topic.getTopic("alarm", account, TOPIC_MSG_HANDLING),
                        Qos.QOS_TWO);
                String sip = JNSpUtils.getString(context, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
                MQTTSDK.getInstance().subscribe(Topic.getTopic("alarm", sip, TOPIC_MSG_HANDLING),
                        Qos.QOS_TWO);
                String uuid = MMKV.defaultMMKV().getString(JNBaseConstans.MMKV_KEY_USER_UUID, "");
                MQTTSDK.getInstance().subscribe(Topic.getTopic("alarm", uuid, TOPIC_MSG_HANDLING),
                        Qos.QOS_TWO);
            }

            @Override
            public void onFailure(Throwable exception) {
                Log.d(TAG,"initMQTT...onFailure..."+exception);
            }

            @Override
            public void connectionLost(Throwable cause) {
                Log.d(TAG,"initMQTT...connectionLost..."+cause);

            }

            @Override
            public void messageArrived(String topic, String message) {
                try {
                        Log.d(TAG,"=confirmCallBack=mqtt..."+message);
                    JCMsgWebNoticeContentBean bean = new Gson().fromJson(message
                            , JCMsgWebNoticeContentBean.class);
                    String account = JNBasePreferenceSaves.getUserAccount();
                    String sip = JNSpUtils.getString(context, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
                    String uuid = MMKV.defaultMMKV().getString(JNBaseConstans.MMKV_KEY_USER_UUID, "");
                    if(TextUtils.equals(topic, Topic.getTopic("alarm", sip, TOPIC_MSG_HANDLING))
                            ||TextUtils.equals(topic, Topic.getTopic("alarm", uuid, TOPIC_MSG_HANDLING))
                            ||TextUtils.equals(topic, Topic.getTopic("alarm", account, TOPIC_MSG_HANDLING))
                    ){
                        String contentBase64 = Base64.encodeToString(message.getBytes(),Base64.NO_WRAP);
                        JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                                contentBase64,JCmessageType.NOTIFY, bean.getSipParameterJson().getUserSipId(), bean.getSipParameterJson().getUserSipId()
                                , JNBasePreferenceSaves.getSipAddress(), JNBasePreferenceSaves.getUserAccount(), JNBasePreferenceSaves.getSipAddress()
                                , JCSessionType.CHAT_SYSTEM, JNBasePreferenceSaves.getJavaAddress(),"");
                        jCimMessageBean.setFromName(bean.getFromName());
                        int isLook = 0 ;
//        因为现在系统的多域逻辑狗屁不是，所以这里直接切去域地址，不在应用多域逻辑
                        MqttMsgListener mqttMsgListener = map.get(bean.getSipParameterJson().getUserSipId()+"_"+JCSessionType.CHAT_SYSTEM);
//                        MqttMsgListener mqttMsgListener = map.get(bean.getSipParameterJson().getUserSipId()+"_"+JCSessionType.CHAT_SYSTEM+"_"+JNBasePreferenceSaves.getJavaAddress());
                        if (null!=mqttMsgListener){
                            isLook = 1 ;
                            mqttMsgListener.onReceiveMessage(jCimMessageBean, isLook);
                        }
                        if (null!=mMqttMsgListenerList){
                            for (MqttMsgListener mMqttMsgListener:mMqttMsgListenerList) {
                                mMqttMsgListener.onReceiveMessage(jCimMessageBean, isLook);
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG, "messageArrived: ==========", e);
                }
            }

            @Override
            public void deliveryComplete(String message) {

            }
        });
    }

    public void disconnect(){
        MQTTSDK.getInstance().disconnect();
    }

}

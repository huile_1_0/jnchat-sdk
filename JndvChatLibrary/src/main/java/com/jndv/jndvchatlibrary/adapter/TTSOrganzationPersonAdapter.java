package com.jndv.jndvchatlibrary.adapter;

import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ChatContant;
import com.jndv.jndvchatlibrary.bean.OrganizationPersonBean;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import java.util.ArrayList;
import java.util.List;

public class TTSOrganzationPersonAdapter extends BaseMultiItemQuickAdapter<OrganizationPersonBean, BaseViewHolder> {

    public TTSOrganzationPersonAdapter(List<OrganizationPersonBean> data) {
        super(data);
        addItemType(1, R.layout.item_list_orgnization);
        addItemType(2, R.layout.item_list_person);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrganizationPersonBean item) {
        switch (helper.getItemViewType()){
            case 1://机构
                JCglideUtils.loadCircleImage(mContext, R.drawable.jc_organzation_depart, helper.getView(R.id.iv_depart_head));
                TextView tvDepartName=helper.getView(R.id.tv_depart_name);
                tvDepartName.setText(item.deptname);
                break;
            case 2://人员
                TextView tvUserName=helper.getView(R.id.tv_user_name);
                tvUserName.setText(item.nickName);
                CheckBox checkBox=helper.getView(R.id.checkBox);
                checkBox.setOnCheckedChangeListener(null);
                checkBox.setChecked(isSelect(item));
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        Log.d("zhang","onCheckedChanged=="+isChecked);
                        if(isChecked){
//                            ChatContant.TTS_PERSON_LIST.add(item);
                        }else {
//                            ChatContant.TTS_PERSON_LIST.remove(item);
                        }
                        if(listener!=null){
                            listener.onSelectCount(ChatContant.TTS_PERSON_LIST.size());
                        }
                    }
                });
                break;
        }
    }

    private boolean isSelect(OrganizationPersonBean item){
        if(ChatContant.TTS_PERSON_LIST.size()==0)
            return false;
        List<Integer> idList=new ArrayList<>();
//        for(OrganizationPersonBean bean: ChatContant.TTS_PERSON_LIST){
//            idList.add(bean.id);
//        }
        return idList.contains(item.id);
    }

    public interface OnSelectCountListener{
        void onSelectCount(int count);
    }

    public OnSelectCountListener listener;

    public void setOnSelectCountListener(OnSelectCountListener onSelectCountListener){
        this.listener=onSelectCountListener;
    }

}



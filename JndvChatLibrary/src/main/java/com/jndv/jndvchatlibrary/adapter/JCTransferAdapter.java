package com.jndv.jndvchatlibrary.adapter;

import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ChatContant;
import com.jndv.jndvchatlibrary.bean.OrganizationPersonBean;

import java.util.ArrayList;
import java.util.List;

public class JCTransferAdapter extends BaseQuickAdapter<OrganizationPersonBean, BaseViewHolder> {

    public JCTransferAdapter(int layoutResId, @Nullable List<OrganizationPersonBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrganizationPersonBean item) {
        TextView tvUserName=helper.getView(R.id.tv_user_name);
        tvUserName.setText(item.otherName);
        CheckBox checkBox=helper.getView(R.id.checkBox);
        checkBox.setOnCheckedChangeListener(null);
        checkBox.setChecked(isSelect(item));
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("zhang","onCheckedChanged=="+isChecked);
                if(isChecked){
                    ChatContant.TRANSFER_PERSON_LIST.add(item);
                }else {
                    ChatContant.TRANSFER_PERSON_LIST.remove(item);
                }
                if(listener!=null){
                    listener.onSelectCount(ChatContant.TRANSFER_PERSON_LIST.size());
                }
            }
        });
    }

    private boolean isSelect(OrganizationPersonBean item){
        if(ChatContant.TRANSFER_PERSON_LIST.size()==0)
            return false;
        List<String> idList=new ArrayList<>();
        for(OrganizationPersonBean bean: ChatContant.TRANSFER_PERSON_LIST){
            idList.add(bean.otherId);
        }
        return idList.contains(item.otherId);
    }

    public interface OnSelectCountListener{
        void onSelectCount(int count);
    }

    public OnSelectCountListener listener;

    public void setOnSelectCountListener(OnSelectCountListener onSelectCountListener){
        this.listener=onSelectCountListener;
    }

}

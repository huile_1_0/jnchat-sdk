package com.jndv.jndvchatlibrary.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.bean.TTSMsgBean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TTSMsgAdapter extends BaseQuickAdapter<TTSMsgBean.DataBean.TTSDataBean, BaseViewHolder> {

    public TTSMsgAdapter(int layoutResId, @Nullable List<TTSMsgBean.DataBean.TTSDataBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TTSMsgBean.DataBean.TTSDataBean item) {
        helper.setText(R.id.tv_time,conversionTime(item.getCreate_time()));
        helper.setText(R.id.tv_content,item.getVoice_content());
    }

    //传入时间戳即可
    public String conversionTime(String timeStamp) {
        //yyyy-MM-dd HH:mm:ss 转换的时间格式  可以自定义
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        //转换
        String time = sdf.format(new Date(Long.valueOf(timeStamp+"000")));
        return time;
    }
}

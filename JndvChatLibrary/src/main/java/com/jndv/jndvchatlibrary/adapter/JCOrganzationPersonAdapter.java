package com.jndv.jndvchatlibrary.adapter;

import android.text.TextUtils;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.ChatContant;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.bean.OrganizationPersonBean;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import java.util.ArrayList;
import java.util.List;

public class JCOrganzationPersonAdapter extends BaseMultiItemQuickAdapter<OrganizationPersonBean, BaseViewHolder> {

    public JCOrganzationPersonAdapter(List<OrganizationPersonBean> data) {
        super(data);
        addItemType(1, R.layout.item_list_orgnization);
        addItemType(2, R.layout.item_list_person);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrganizationPersonBean item) {
        switch (helper.getItemViewType()){
            case 1://机构
                JCglideUtils.loadCircleImage(mContext, R.drawable.jc_organzation_depart, helper.getView(R.id.iv_depart_head));
                TextView tvDepartName=helper.getView(R.id.tv_depart_name);
                tvDepartName.setText(item.deptname);
                break;
            case 2://人员
                TextView tvUserName=helper.getView(R.id.tv_user_name);
                tvUserName.setText(item.nickName);
                CheckBox checkBox=helper.getView(R.id.checkBox);
                checkBox.setOnCheckedChangeListener(null);
                if (isSelectOld(item)){
                    checkBox.setClickable(false);
                }else {
                    checkBox.setClickable(true);
                    checkBox.setChecked(isSelect(item));
                }
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Log.d("zhang","onCheckedChanged=="+isChecked);
                            if(TextUtils.equals(item.IM_Number, JNBasePreferenceSaves.getUserSipId())){
                                Toast.makeText(mContext,"不能与自己通信",Toast.LENGTH_SHORT).show();
                                checkBox.setChecked(false);
                                return;
                            }
                            if(isChecked){
                                item.otherId=item.IM_Number;
                                item.otherName=item.nickName;
                                item.type="1";
                                ChatContant.TRANSFER_PERSON_LIST.add(item);
                            }else {
                                ChatContant.TRANSFER_PERSON_LIST.remove(item);
                            }
                            if(listener!=null){
                                listener.onSelectCount(ChatContant.TRANSFER_PERSON_LIST.size());
                            }
                        }
                    });
                break;
        }
    }

    private boolean isSelect(OrganizationPersonBean item){
        if(ChatContant.TRANSFER_PERSON_LIST.size()==0)
            return false;
        List<String> idList=new ArrayList<>();
        for(OrganizationPersonBean bean: ChatContant.TRANSFER_PERSON_LIST){
            idList.add(bean.IM_Number);
        }
        return idList.contains(item.IM_Number);
    }
    private boolean isSelectOld(OrganizationPersonBean item){
        if(ChatContant.TRANSFER_PERSON_LIST_OLD.size()==0)
            return false;
        for(OrganizationPersonBean bean: ChatContant.TRANSFER_PERSON_LIST_OLD){
            if (TextUtils.equals(bean.IM_Number, item.IM_Number))return true;
        }
        return false;
    }

    public interface OnSelectCountListener{
        void onSelectCount(int count);
    }

    public OnSelectCountListener listener;

    public void setOnSelectCountListener(OnSelectCountListener onSelectCountListener){
        this.listener=onSelectCountListener;
    }

}



package com.jndv.jndvchatlibrary.thraed;

import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.jndv.jndvchatlibrary.JCChatManager;
import com.lzh.easythread.Callback;
import com.lzh.easythread.EasyThread;

public final class JCThreadManager {

    private final static EasyThread io;//
    private final static EasyThread cache;
    private final static EasyThread file;

    public static EasyThread getIO () {
        return io;
    }

    public static EasyThread getCache() {
        return cache;
    }

    public static EasyThread getFile() {
        return file;
    }

    static {
        io = EasyThread.Builder.createFixed(6).setName("IO").setPriority(7).setCallback(new DefaultCallback()).build();
        cache = EasyThread.Builder.createCacheable().setName("cache").setCallback(new DefaultCallback()).build();
        file = EasyThread.Builder.createFixed(4).setName("file").setPriority(3).setCallback(new DefaultCallback()).build();
    }

    private static class DefaultCallback implements Callback {

        @Override
        public void onError(String threadName, Throwable t) {

        }

        @Override
        public void onCompleted(String threadName) {

        }

        @Override
        public void onStart(String threadName) {

        }
    }

    public static void showMainToast(String text){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(JCChatManager.mContext, text, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public static void showMainToastLong(String text){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(JCChatManager.mContext, text, Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void onMainHandler(Runnable runnable){
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public static void onMainHandler(Runnable runnable, long delayed){
        new Handler(Looper.getMainLooper()).postDelayed(runnable, delayed);
    }

}

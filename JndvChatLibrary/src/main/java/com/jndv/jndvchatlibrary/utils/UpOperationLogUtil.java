package com.jndv.jndvchatlibrary.utils;


public class UpOperationLogUtil {
    private static final String TAG = "UpOperationLogUtil";
    private volatile static UpOperationLogUtil instance;

    public static UpOperationLogUtil getInstance() {
        if (instance == null) {
            synchronized (UpOperationLogUtil.class) {
                if (instance == null) {
                    instance = new UpOperationLogUtil();
                }
            }
        }
        return instance;
    }
}

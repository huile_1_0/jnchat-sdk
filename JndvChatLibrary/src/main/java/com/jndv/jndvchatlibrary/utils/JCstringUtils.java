package com.jndv.jndvchatlibrary.utils;

import android.text.TextUtils;

/**
 * Author: wangguodong
 * Date: 2022/2/14
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 字符串使用工具类
 */
public class JCstringUtils {

    /**
     * counter ASCII character as one, otherwise two
     * 获取字符串的字节数量
     *
     * @param str
     * @return count
     */
    public static int counterChars(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            int tmp = (int) str.charAt(i);
            if (tmp > 0 && tmp < 127) {
                count += 1;
            } else {
                count += 2;
            }
        }
        return count;
    }

}

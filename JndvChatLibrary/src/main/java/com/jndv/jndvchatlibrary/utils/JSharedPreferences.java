package com.jndv.jndvchatlibrary.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;

/**
 * 名称为config 的SharedPreferences
 *
 * @author 注释作者0723
 */
public class JSharedPreferences {

    private static final String CONFIG_NAME = "config";

    public JSharedPreferences() {
    }

    /**
     * 对应getString(key, null)
     *
     * @author 注释作者0723
     */
    public static String getConfigStrValue(Context context, String key) {
        if (context == null) {
            throw new NullPointerException(" context is null");
        }
        SharedPreferences sf = context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        return sf.getString(key, null);
    }

    /**
     * 对应getString(key, defaultStr)
     *
     * @author 注释作者0723
     */
    public static String getConfigStrValue(Context context, String key, String defaultStr) {
        if (context == null) {
            throw new NullPointerException(" context is null");
        }
        SharedPreferences sf = context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        return sf.getString(key, defaultStr);
    }

    /**
     * 对应putInt(key, value)
     *
     * @author 注释作者0723
     */
    public static boolean putConfigIntValue(Context context, String key, int value) {
        SharedPreferences sf = context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = sf.edit();
        e.putInt(key, value);
        return e.commit();
    }

    /**
     * 对应getInt(key, defaultVal)
     *
     * @author 注释作者0723
     */
    public static int getConfigIntValue(Context context, String key, int defaultVal) {
        SharedPreferences sf = context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        return sf.getInt(key, defaultVal);
    }

    /**
     * 对应getBoolean(key, defaultVal)
     *
     * @author 注释作者0723
     */
    public static boolean getConfigBooleanValue(Context context, String key, boolean defaultVal) {
        SharedPreferences sf = context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        return sf.getBoolean(key, defaultVal);
    }

    /**
     * 对应putString(key, value)
     *
     * @author 注释作者0723
     */
    public static boolean putConfigStrValue(Context context, String key, String value) {
        SharedPreferences sf = context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = sf.edit();
        e.putString(key, value);
        return e.commit();
    }

    /**
     * 对应putInt(key, value)
     *
     * @author 注释作者0723
     */
    public static boolean putIntValue(Context context, String key, int value) {
        SharedPreferences sf = context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = sf.edit();
        e.putInt(key, value);
        return e.commit();
    }

    /**
     * 对应putBoolean(key, value)
     *
     * @author 注释作者0723
     */
    public static boolean putBooleanValue(Context context, String key, boolean value) {
        SharedPreferences sf = context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = sf.edit();
        e.putBoolean(key, value);
        return e.commit();
    }

    /**
     * if (keys == null || values == null) { return false; }<br/>
     * if (keys.length != values.length) { throw new RuntimeException}<br/>
     * putString一一对应存入
     *
     * @author 注释作者0723
     */
    public static boolean putStrValue(Context context, String[] keys, String[] values) {
        if (keys == null || values == null) {
            return false;
        }
        if (keys.length != values.length) {
            throw new RuntimeException(" keys's length is different with values's length");
        }
        SharedPreferences sp = context.getSharedPreferences(CONFIG_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = sp.edit();
        for (int index = 0; index < keys.length; index++) {
            e.putString(keys[index], values[index]);
        }
        return e.commit();
    }

    public static String getPath(Context context, Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};

            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param context
     * @return true if current network is connected , otherwise false
     */
    public static boolean checkCurrentAviNetwork(Context context) {
        if (context == null) {
            throw new NullPointerException("Invalid context object");
        }
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // need to check mobile !=null, because no mobile network data in PAD
        if (wifi.isConnected() || (mobile != null && mobile.isConnected())) {
            return true;
        } else {
            return false;
        }
    }

}

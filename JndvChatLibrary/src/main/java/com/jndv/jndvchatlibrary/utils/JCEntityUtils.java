package com.jndv.jndvchatlibrary.utils;

/**
 * 实体类
 */
public class JCEntityUtils {
    //群相关
    public static final String CROWD_ID="crowdId";//群名
    public static final String USER_ID="userId";//用户名
    public static final String GROUP_NUMBERS="groupNumbers";//群成员

    public static final String DOMAIN_ADDR="domain_addr";//
    public static final String GROUP_DOMAIN_ADDR="group_domain_addr";//群sip

    public static final String GROUP_DOMAIN_ADDR_USER="group_domain_addr_user";//群java


    public static final String IS_FROM_CROWD="isFromCrowd";//是否来自于群主或群管理员
    public static final String INVITE_PERMISSION="invite_permission";//邀请权限



    //Mpush相关
    public static final String MPUSH_SERVER_PATH = "mpush_server_path";//mpush服务器地址
    public static final String MPUSH_USER_ID = "mpush_user_id";//mpush用户id

    //会话相关
    public static final String SESSION_OTHER_ID = "sessionOtherId";//会话ID，对方id
    public static final String SESSION_ID = "sessionId";//会话记录id
    public static final String SESSION_TYPE = "sessionType";//会话类型
    public static final String SESSION_ADDRESS = "sessionAddress";//会话sip域地址
    public static final String SESSION_ADDRESS_JAVA = "sessionAddressJava";//会话java域地址
    public static final String SESSION_READ_LAST_TIME = "sessionReadLastTime";//会话最后一条已读消息的时间
    public static final String SESSION_NOT_LINE_MSG_NUM = "sessionNotLineMsgNum";//会话未读消息数量
    //置顶
    public static final String SESSION_TOP_FLAG="session_top_flag";
    //群聊状态
    public static final String GROUP_STATUS="group_status";
}

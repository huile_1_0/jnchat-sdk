package com.jndv.jndvchatlibrary.utils;

/**
 * Author: wangguodong
 * Date: 2022/5/12
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 即时通讯，二维码信息基类
 */
public class JCQRcodeBeanBase {
    public static final int QRCODE_TYPE_USER = 1;
    public static final int QRCODE_TYPE_CROWD = 2;

    private int type = 0 ;
    private String msg ="";//具体内容

    public JCQRcodeBeanBase(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}

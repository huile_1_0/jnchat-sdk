package com.jndv.jndvchatlibrary.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.text.TextUtils;
import android.util.DisplayMetrics;


import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConfig;
import com.jndv.jndvchatlibrary.ui.crowd.utils.LogUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class BitmapUtils {

    public static Context context;

    public static void init(Context context) {
        BitmapUtils.context = context;
    }

    public BitmapUtils() {
    }

    public static Bitmap loadAvatarFromPath(String path) {
        boolean isOwnerAvatar = true;
        if (path == null) {
            isOwnerAvatar = false;
        } else {
            File f = new File(path);
            if (!f.exists()) {
                isOwnerAvatar = false;
                LogUtil.e("BitmapUtils loadAvatarFromPath --> FAILED! Because parse Path , file isn't exist! path is : " + path);
                File parent = new File(f.getParent());
                File[] listFiles = parent.listFiles();
                for (File file : listFiles) {
                    LogUtil.e("BitmapUtils loadAvatarFromPath --> current directory file list is : " + file.getName());
                }
            }

            if (f.isDirectory()) {
                isOwnerAvatar = false;
                LogUtil.e("BitmapUtils loadAvatarFromPath --> FAILED! Because parse Path , get a Directory! path is : " + path);
            }
        }

        Bitmap temp = null;
        if (!TextUtils.isEmpty(path)) {
            BitmapFactory.Options opt = new BitmapFactory.Options();
            temp = BitmapFactory.decodeFile(path, opt);
            if (temp == null) {
                isOwnerAvatar = false;
                LogUtil.i(" bitmap object is null");
            }
        }

        Bitmap avatar = null;
        if (isOwnerAvatar) {
            avatar = getCompressdAvatar(temp);
            LogUtil.d("decode result: width " + avatar.getWidth() + "  height:" + avatar.getHeight());
        } else {
            if (GlobalConfig.GLOBAL_DENSITY_LEVEL == DisplayMetrics.DENSITY_XHIGH
                    | GlobalConfig.GLOBAL_DENSITY_LEVEL == GlobalConfig.DENSITY_XXHIGH) {
                avatar = BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.soil_iv_default);
            } else {
                avatar = BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.soil_iv_default);
            }
        }
        return avatar;
    }

    public static Bitmap getPhoneFriendAvatar() {
        Bitmap temp = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.avatar_phonefriend);
        return getCompressdAvatar(temp);
    }

    private static Bitmap getCompressdAvatar(Bitmap temp) {
        int width, height = 0;
        if (GlobalConfig.GLOBAL_DENSITY_LEVEL == DisplayMetrics.DENSITY_HIGH) {
            width = 70;
            height = 70;
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == DisplayMetrics.DENSITY_MEDIUM) {
            width = (int) (70 / 1.5 + 0.5);
            height = (int) (70 / 1.5 + 0.5);
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == DisplayMetrics.DENSITY_LOW) {
            width = (int) (70 / 1.5 * 0.7 + 0.5);
            height = (int) (70 / 1.5 * 0.7 + 0.5);
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == DisplayMetrics.DENSITY_XHIGH) {
            width = (int) (70 / 1.5 * 2 + 0.5);
            height = (int) (70 / 1.5 * 2 + 0.5);
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == GlobalConfig.DENSITY_XXHIGH) {
            width = (int) (70 * 2 + 0.5);
            height = (int) (70 * 2 + 0.5);
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == GlobalConfig.DENSITY_XXXHIGH) {
            width = (int) (70 * 2.67);
            height = (int) (70 * 2.67);
        } else {
            width = 70;
            height = 70;
        }
        temp = Bitmap.createScaledBitmap(temp, width, height, true);
        return temp;
    }

    public static int getAvatarSize() {
        int width = 0;
        if (GlobalConfig.GLOBAL_DENSITY_LEVEL == DisplayMetrics.DENSITY_HIGH) {
            width = 70;
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == DisplayMetrics.DENSITY_MEDIUM) {
            width = (int) (70 / 1.5 + 0.5);
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == DisplayMetrics.DENSITY_LOW) {
            width = (int) (70 / 1.5 * 0.7 + 0.5);
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == DisplayMetrics.DENSITY_XHIGH) {
            width = (int) (70 / 1.5 * 2 + 0.5);
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == GlobalConfig.DENSITY_XXHIGH) {
            width = (int) (70 * 2 + 0.5);
        } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL == GlobalConfig.DENSITY_XXXHIGH) {
            width = (int) (70 * 2.67);
        } else {
            width = 70;
        }
        return width;
    }

    public static Bitmap getCompressedBitmap(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            throw new NullPointerException(" file is null");
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Config.ALPHA_8;
        BitmapFactory.decodeFile(filePath, options);
        // 计算压缩比
        if (options.outWidth >= 4096 || options.outHeight >= 4096) {
            options.inSampleSize = 8;
        } else if (options.outWidth >= 2048 || options.outHeight >= 2048) {
            options.inSampleSize = 4;
        } else {
            options.inSampleSize = 2;
        }

        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inInputShareable = true;// 。当系统内存不够时候图片自动被回收
        options.inPurgeable = true;
        options.inPreferredConfig = null;
        Bitmap bit = BitmapFactory.decodeFile(filePath, options);
       return compress(bit);
    }

    public static Bitmap compress(Bitmap bit){
        if (bit.getWidth() < 500 && bit.getHeight() < 500) {
            if (GlobalConfig.GLOBAL_DENSITY_LEVEL < GlobalConfig.DENSITY_XXXHIGH) {
                bit = ThumbnailUtils.extractThumbnail(bit, bit.getWidth() / 2,
                        bit.getHeight() / 2, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
            } else {
                bit = ThumbnailUtils.extractThumbnail(bit, bit.getWidth(),
                        bit.getHeight(), ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
            }
        } else {
            bit = ThumbnailUtils.extractThumbnail(bit, bit.getWidth() / 2,
                    bit.getHeight() / 2, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
        }

        if (bit.getWidth() < 50 || bit.getHeight() < 50) {
            if (GlobalConfig.GLOBAL_DENSITY_LEVEL > DisplayMetrics.DENSITY_HIGH) {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bit,
                        bit.getWidth() * 3, bit.getHeight() * 3, false);
                bit.recycle();
                return createScaledBitmap;
            }
        } else if (bit.getWidth() < 100 || bit.getHeight() < 100) {
            if (GlobalConfig.GLOBAL_DENSITY_LEVEL == GlobalConfig.DENSITY_XXXHIGH) {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bit,
                        bit.getWidth() * 6, bit.getHeight() * 6, false);
                bit.recycle();
                return createScaledBitmap;
            } else if (GlobalConfig.GLOBAL_DENSITY_LEVEL > DisplayMetrics.DENSITY_HIGH) {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bit,
                        bit.getWidth() * 2, bit.getHeight() * 2, false);
                bit.recycle();
                return createScaledBitmap;
            }
        } else if (bit.getWidth() < 200 || bit.getHeight() < 200) {
            if (GlobalConfig.GLOBAL_DENSITY_LEVEL == GlobalConfig.DENSITY_XXXHIGH) {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bit,
                        bit.getWidth() * 2, bit.getHeight() * 2, false);
                bit.recycle();
                return createScaledBitmap;
            }
            // else if (GlobalConfig.GLOBAL_DENSITY_LEVEL >
            // DisplayMetrics.DENSITY_HIGH){
            // Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bit,
            // bit.getWidth() * 2, bit.getHeight() * 2, false);
            // bit.recycle();
            // return createScaledBitmap;
            // }
        }
        return bit;
    }

    public static Bitmap getDocCompressBitmap(String filePath, int[] r) {
        if (TextUtils.isEmpty(filePath)) {
            throw new NullPointerException(" file is null");
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Config.ALPHA_8;
        BitmapFactory.decodeFile(filePath, options);

        double wr = (double) options.outWidth / GlobalConfig.SCREEN_WIDTH;
        double hr = (double) options.outHeight / GlobalConfig.SCREEN_HEIGHT;
        double ratio = Math.min(wr, hr);
        float cross = 1.0f;
        while ((cross * 2) <= ratio) {
            cross *= 2;
        }
        options.inSampleSize = (int) cross;
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inInputShareable = true;// 。当系统内存不够时候图片自动被回收
        options.inPurgeable = true;
        options.inPreferredConfig = null;
        Bitmap bit = BitmapFactory.decodeFile(filePath, options);
        if (bit.getWidth() < 200 || bit.getHeight() < 200) {
            if (GlobalConfig.GLOBAL_DENSITY_LEVEL == GlobalConfig.DENSITY_XXXHIGH) {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bit,
                        bit.getWidth() * 2, bit.getHeight() * 2, false);
                bit.recycle();
                r[0] = createScaledBitmap.getWidth();
                r[1] = createScaledBitmap.getHeight();
                return createScaledBitmap;
            }
        }
        r[0] = bit.getWidth();
        r[1] = bit.getHeight();
        return bit;
    }

    // 计算缩放比
    public static Bitmap getImageThumbnail(String imagePath, int width,
                                           int height) {
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Config.ALPHA_8;
        BitmapFactory.decodeFile(imagePath, options);

        if (options.outWidth >= 2048 || options.outHeight >= 2048) {
            options.inSampleSize = 8;
        } else if (options.outWidth > 1080 || options.outHeight > 1080) {
            options.inSampleSize = 4;
        } else {
            options.inSampleSize = 2;
        }

        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inInputShareable = true;// 。当系统内存不够时候图片自动被回收
        options.inPurgeable = true;
        options.inPreferredConfig = null;

        // 重新读入图片，读取缩放后的bitmap，注意这次要把options.inJustDecodeBounds 设为 false
        bitmap = BitmapFactory.decodeFile(imagePath, options);
        // 利用ThumbnailUtils来创建缩略图，这里要指定要缩放哪个Bitmap对象
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height,
                ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
        return bitmap;
    }

    // public static void getCompressedBitmapBounds(String file, int[] r) {
    //
    // BitmapFactory.Options options = new BitmapFactory.Options();
    // options.inJustDecodeBounds = true;
    // options.inPreferredConfig = Bitmap.Config.ALPHA_8;
    // double wr = (double) options.outWidth / GlobalConfig.SCREEN_WIDTH;
    // double hr = (double) options.outHeight / GlobalConfig.SCREEN_HEIGHT;
    // double ratio = Math.min(wr, hr);
    // float cross = 1.0f;
    // while ((cross * 2) <= ratio) {
    // cross *= 2;
    // }
    // options.inSampleSize = (int) cross;
    // options.inDither = false;
    // options.inInputShareable = true;// 。当系统内存不够时候图片自动被回收
    // options.inPurgeable = true;
    // options.inPreferredConfig = null;
    //
    // BitmapFactory.decodeFile(file, options);
    // r[0] = options.outWidth;
    // r[1] = options.outHeight;
    // }

    public static void getFullBitmapBounds(String file, int[] r) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Config.ALPHA_8;
        BitmapFactory.decodeFile(file, options);
        r[0] = options.outWidth;
        r[1] = options.outHeight;
    }

    public static int getBitmapRotation(String imgpath) {
        int digree = 0;
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imgpath);
        } catch (IOException e) {
            e.printStackTrace();
            exif = null;
        }
        if (exif != null) {
            // 读取图片中相机方向信息
            int ori = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            // 计算旋转角度
            switch (ori) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    digree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    digree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    digree = 270;
                    break;
                default:
                    digree = 0;
                    break;
            }
        }
        return digree;
    }

    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public static Bitmap createScaledBitmap(Bitmap unscaledBitmap,
                                            int dstWidth, int dstHeight) {
        Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(),
                unscaledBitmap.getHeight(), dstWidth, dstHeight);
        Rect dstRect = calculateDstRect(unscaledBitmap.getWidth(),
                unscaledBitmap.getHeight(), dstWidth, dstHeight);
        Bitmap scaledBitmap = Bitmap.createBitmap(dstRect.width(),
                dstRect.height(), Config.ARGB_8888);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.drawBitmap(unscaledBitmap, srcRect, dstRect, new Paint(
                Paint.FILTER_BITMAP_FLAG));
        return scaledBitmap;
    }

    public static Rect calculateSrcRect(int srcWidth, int srcHeight,
                                        int dstWidth, int dstHeight) {
        // float srcAspect = (float) srcWidth / (float) srcHeight;
        // float dstAspect = (float) dstWidth / (float) dstHeight;
        // if (srcAspect > dstAspect) {
        // final int srcRectWidth = (int) (srcHeight * dstAspect);
        // final int srcRectLeft = (srcWidth - srcRectWidth) / 2;
        // return new Rect(srcRectLeft, 0, srcRectLeft + srcRectWidth,
        // srcHeight);
        // } else {
        // final int srcRectHeight = (int) (srcWidth / dstAspect);
        // final int scrRectTop = (int) (srcHeight - srcRectHeight) / 2;
        // return new Rect(0, scrRectTop, srcWidth, scrRectTop + srcRectHeight);
        // }
        return new Rect(0, 0, srcWidth, srcHeight);
    }

    public static Rect calculateDstRect(int srcWidth, int srcHeight,
                                        int dstWidth, int dstHeight) {
        float srcAspect = (float) srcWidth / (float) srcHeight;
        float dstAspect = (float) dstWidth / (float) dstHeight;
        if (srcAspect > dstAspect) {
            return new Rect(0, 0, dstWidth, (int) (dstWidth / srcAspect));
        } else {
            return new Rect(0, 0, (int) (dstHeight * srcAspect), dstHeight);
        }
        // return new Rect(0, 0, dstWidth, dstHeight);
    }

    public static int calculateSampleSize(int srcWidth, int srcHeight,
                                          int dstWidth, int dstHeight) {
        // final float srcAspect = (float) srcWidth / (float) srcHeight;
        // final float dstAspect = (float) dstWidth / (float) dstHeight;
        // if (srcAspect > dstAspect) {
        // return srcWidth / dstWidth;
        // } else {
        // return srcHeight / dstHeight;
        // }
        int scaleFactor = 1;
        if ((dstWidth > 0) || (dstHeight > 0)) {
            return scaleFactor = Math.min(srcWidth / dstWidth, srcHeight
                    / dstHeight);
        }
        return scaleFactor;
    }

//    public static ImageView getView(Context ct, VMessageImageItem imageItem) {
//        ImageView imgView = new ImageView(ct);
//        imgView.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
//        String filepath = imageItem.getFilePath();
//        int degree = readPictureDegree(filepath);
//        Bitmap bitmap =rotaingImageView(degree, BitmapFactory.decodeFile(filepath));
//        //从路径加载出图片
//        imgView.setImageBitmap(bitmap);//ImageView显示图片
//        return imgView;
//    }


    //获取图片的旋转角度
    public static int readPictureDegree(String path) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }

    /**
     * 旋转图片
     *
     * @param angle
     * @param bitmap
     * @return Bitmap
     */
    public static Bitmap rotaingImageView(int angle, Bitmap bitmap) {
        if (bitmap!=null){
            //旋转图片 动作
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            // 创建新的图片
            Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap!=null?bitmap.getWidth():0, bitmap!=null?bitmap.getHeight():0, matrix, true);
            return resizedBitmap;
        }else {
            return null;
        }

    }
}

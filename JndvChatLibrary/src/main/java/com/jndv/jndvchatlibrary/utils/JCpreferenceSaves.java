package com.jndv.jndvchatlibrary.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jndv.jndvchatlibrary.JCChatManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/14
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 文件存储使用工具类，用于存储：用户信息、等
 */
public class JCpreferenceSaves {

    private static final String KEY_USER_ID = "user_id";//用户ID
    private static final String KEY_USER_NAME = "user_name";//
    private static final String KEY_USER_HEAD = "user_head";//
    private static final String KEY_USER_ACCOUNT = "user_account";//
    private static final String KEY_USERID= "user_id";//
    private static final String KEY_USER_MOBILE = "user_mobile";//
//    public static final String KEY_USER_zoningCode = "user_zoningCode";//
    public static final String KEY_USER_rtspServer = "user_rtspServer";//
    public static final String KEY_LOGIN_STATE = "user_login_state";//

    private static final String KEY_JAVA_URL = "java_url";//
//    private static final String KEY_JAVA_PORT = "java_port";//

    private static final String KEY_USER_ORGANZATIONID = "user_organizationId";//

    public static final String KEY_DOMAIN_ADDR="domain_addr";//SIP域地址

    public static final String KEY_PERMISSION_DIALOG_CAMERA="PERMISSION_DIALOG_CAMERA";//0==未申请；1=已申请过

    public static final String KEY_PDF_FILE_NAME="pdf_file_name";


  /*  public static void saveUserId(String account) {
        saveString(KEY_USER_ID, account);

    } */
//    public static void saveUserId(String account) {
//        saveInt(KEY_USER_ID, Integer.parseInt(account));
//
//    }

    /**
     * 用户SIP号码
     * @return
     */
    public static String getUserSipId() {
        return JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_NUMBER,"0");
    }

    /**
     * 用户ID
     * 自增ID
     * @return
     */
//    public static int getUserId() {
//        return getInt(KEY_USER_ID);
//        //return getString(KEY_USER_ID);
//    }



    public static void saveUserName(String account) {
        saveString(KEY_USER_NAME, account);
    }

    public static String getUserName() {
        return getString(KEY_USER_NAME);
    }
//
//    public static void saveLoginSate(int state) {//1=登录
//        saveInt(KEY_LOGIN_STATE, state);
//    }
//
//    public static int getLoginSate() {
//        return getInt(KEY_LOGIN_STATE);
//    }

    public static void saveOrganzationId(String organizationId) {
//        JNBasePreferenceSaves.saveOrganzationId(organizationId);
//        saveString(KEY_USER_ORGANZATIONID, organizationId);
        JNSpUtils.setString(JCChatManager.mContext, JNPjSipConstants.PJSIP_ORGANZATIONID, organizationId);
    }

    public static String getOrganzationId() {
//        return JNBasePreferenceSaves.getOrganzationId();
        return JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_ORGANZATIONID);
//        return getString(KEY_USER_ORGANZATIONID);
    }

    public static void saveUserHead(String account) {
        saveString(KEY_USER_HEAD, account);
    }

    public static String getUserHead() {
        return getString(KEY_USER_HEAD);
    }

    public static void saveUserAccount(String account) {
        saveString(KEY_USER_ACCOUNT, account);
    }
    public static String getUserAccount() {
        return getString(KEY_USER_ACCOUNT);
    }

    public static void saveUserId(String userId) {
        saveString(KEY_USERID, userId);
    }
//JNBasePreferenceSaves.getUserId()
//    public static String getUserId() {
//        return getString(KEY_USERID);
//    }

    public static void saveUserMobil(String account) {
        saveString(KEY_USER_MOBILE, account);
    }
    public static String getUserMobil() {
        return getString(KEY_USER_MOBILE);
    }

    public static void saveJavaUrl(String javaUrl) {
        saveString(KEY_JAVA_URL, javaUrl);
    }
    public static String getJavaUrl() {
        return getString(KEY_JAVA_URL);
    }

    public static void saveString(String key, String value) {
        if (null==value)value="";
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(String key) {
        return getSharedPreferences().getString(key, null);
    }

    public static void saveInt(String key, int value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int getInt(String key) {
        return getSharedPreferences().getInt(key, 0);
    }

    public static void saveFloat(String key, float value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static float getFloat(String key) {
        return getSharedPreferences().getFloat(key, 0);
    }

    /**
     * 保存List
     *
     * @param tag
     * @param datalist
     */
    public static <T> void setDataList(String tag, List<T> datalist) {
        if (null == datalist || datalist.size() == 0)
            return;
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        Gson gson = new Gson();
        //转换成json数据，再保存
        String strJson = gson.toJson(datalist);
        editor.clear();
        editor.putString(tag, strJson);
        editor.apply();
    }

    /**
     * 获取List
     *
     * @param tag
     * @return
     */
    public static <T> List<T> getDataList(String tag) {
        List<T> datalist = new ArrayList<T>();
        String strJson = getSharedPreferences().getString(tag, null);
        if (null == strJson) {
            return datalist;
        }
        Gson gson = new Gson();
        datalist = gson.fromJson(strJson, new TypeToken<List<T>>() {
        }.getType());
        return datalist;
    }

    public static void cleanString(String key) {
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        edit.remove(key);
        edit.apply();
    }

    private static SharedPreferences getSharedPreferences() {
        return JCChatManager.mContext.getSharedPreferences("JCchatLibrary", Context.MODE_PRIVATE);
    }

    /**
     * 获取java域地址
     * @return
     */
    public static String getJavaAddress() {
        String url = getString(KEY_JAVA_URL);
        if (null==url|| TextUtils.isEmpty(url)){
            return "";
        }else if (url.contains("://")){
            String str = url.substring(url.indexOf("://")+3);
            return str;
        }else {
            return url;
        }
    }
    /**
     * 获取SIP域地址
     * @return
     */
    public static String getSipAddress() {
        String port = JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_PORT, JNPjSipConstants.PJSIP_PORT_DEFAULT);
        if (null==port|| TextUtils.isEmpty(port)){
            return JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_HOST, JNPjSipConstants.PJSIP_HOST_DEFAULT);
        }else {
            return JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_HOST, JNPjSipConstants.PJSIP_HOST_DEFAULT)+":"+port;
        }
    }

    /**
     * 获取SIP域的IP
     * @return
     */
    public static String getSipIp() {
        return JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_HOST, "");
    }
    /**
     * 获取SIP域的Port
     * @return
     */
    public static String getSipPort() {
        return JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_PORT, "");
    }
}

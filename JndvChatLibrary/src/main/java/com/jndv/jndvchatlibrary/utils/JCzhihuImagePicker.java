package com.jndv.jndvchatlibrary.utils;

import android.content.Context;

import com.qingmei2.rximagepicker.entity.Result;
import com.qingmei2.rximagepicker.entity.sources.Camera;
import com.qingmei2.rximagepicker.entity.sources.Gallery;
import com.qingmei2.rximagepicker.ui.ICustomPickerConfiguration;
import com.qingmei2.rximagepicker_extension_zhihu.ui.ZhihuImagePickerActivity;

import io.reactivex.Observable;

/**
 * Author: wangguodong
 * Date: 2022/2/17
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 图片选择器使用的接口
 */
public interface JCzhihuImagePicker {
    @Gallery(componentClazz = ZhihuImagePickerActivity.class,
//    @Gallery(componentClazz = JNImageVideoPickerActivity.class,
            openAsFragment = false)
    Observable<Result> openGalleryAsNormal(Context context, ICustomPickerConfiguration config);

    @Gallery(componentClazz = ZhihuImagePickerActivity.class,
            openAsFragment = false)
    Observable<Result> openGalleryAsDracula(Context context, ICustomPickerConfiguration config);

    @Camera
    Observable<Result> openCamera(Context context);

}

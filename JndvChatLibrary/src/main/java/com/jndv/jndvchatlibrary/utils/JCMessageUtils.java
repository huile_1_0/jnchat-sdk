package com.jndv.jndvchatlibrary.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.ehome.sipservice.SipServiceCommand;
import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.http.api.JCFilesUpApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.http.api.JCImagesUpApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgImgContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgNotifyContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgVoiceContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgWebNoticeContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCNotifyType;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;

import org.greenrobot.eventbus.EventBus;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/4/19
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 消息发送工具
 */
public class JCMessageUtils {

    public static String TAG="SipService";
//    public static String TAG="JCMessageUtils";
    //    发送消息
    public static void sendACKMessage(JCimMessageBean bean, Context context) {
            Log.d(TAG,"jc...sendACKMessage...");
//        JCChatManager.showToast("==sendACKMessage==SEND=="+bean.getFromSipID()+"==to=="+bean.getToSipID());
        String userID = JNSpUtils.getString(context,"myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
        SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm()
                , "text/groupmsgack+json",false);
    }
    //    发送消息
    public static void sendMessage(JCimMessageBean bean, Context context) {
        //这里必须保证先注册完sip，否则userId是null
        String userID = JNSpUtils.getString(context,"myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
        Log.d(TAG,"jc...sendMessage...userID=="+userID);
        if(!TextUtils.isEmpty(userID)){
            SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                    ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm()
                    , JCSessionType.getTypeSIP(bean.getSessionType()),false);
        }
    }
    //发送警航消息
    public static void sendFlightMessage(JCimMessageBean bean, Context context) {
        String userID = JNSpUtils.getString(context,"myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
        Log.d(TAG, "sendFlightMessage: ======userID=="+userID);
        SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm()
                , JCSessionType.getFlightTypeSIP(bean.getSessionType(),bean.getMsgType()),false);
    }
    //    消息重发
    public static void reSendMessage(JCimMessageBean bean, Context context) {
        String userID = JNSpUtils.getString(context,"myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
        Log.d(TAG, "reSendMessage: ======userID=="+userID);
        if (TextUtils.equals(bean.getMsgType(),""+ JCmessageType.TEXT)){
            SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                    ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm()
                    , JCSessionType.getTypeSIP(bean.getSessionType()),false);
        }else if (TextUtils.equals(bean.getMsgType(),""+JCmessageType.IMG)){
            bean.setStatus(""+JCmessageStatusType.sendIng);
            EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>
                    (JNEventBusType.CODE_MESSAGE_UPDATE, "",bean));
            JCMsgImgContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(bean.getContent(),Base64.NO_WRAP)), JCMsgImgContentBean.class);
//            JCMsgImgContentBean msgEntity = new Gson().fromJson(bean.getContent(), JCMsgImgContentBean.class);
            if (!TextUtils.isEmpty(msgEntity.getUrl())){
                SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                        ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm(),
                        JCSessionType.getTypeSIP(bean.getSessionType()),false);
                return;
            }

            JCFilesUpUtils.upImage(msgEntity.getPath(), msgEntity.getType(), new OnHttpListener<JCImagesUpApi.Bean>() {
                @Override
                public void onSucceed(JCImagesUpApi.Bean result) {
                    try {
//                                                        Box<JCimMessageBean > box = JCobjectBox.get().boxFor(JCimMessageBean.class);
                        if (TextUtils.equals("success", result.getMsg())){
                            JCMsgImgContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(bean.getContent(),Base64.NO_WRAP)), JCMsgImgContentBean.class);
                            msgEntity.setUrl(JNBaseUtilManager.getFilesUrl(result.getUrlArr().get(0).getTailUrl()));
//                                                            msgEntity.setUri(null);
                            String msgEntityText = new Gson().toJson(msgEntity);
                            bean.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    bean.setStatus(""+JCmessageStatusType.sendSuccessceS);
                                    SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                                            ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm()
                                            , JCSessionType.getTypeSIP(bean.getSessionType()),false);
                                }
                            });
                        }else {
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    bean.setStatus(""+JCmessageStatusType.sendFail);
                                    EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>
                                            (JNEventBusType.CODE_MESSAGE_UPDATE, "",bean));
                                }
                            });
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail(Exception e) {
                    JCThreadManager.onMainHandler(new Runnable() {
                        @Override
                        public void run() {
                            bean.setStatus(""+JCmessageStatusType.sendFail);
                            EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>
                                    (JNEventBusType.CODE_MESSAGE_UPDATE, "",bean));
                        }
                    });
                }
            });
        }else if (TextUtils.equals(bean.getMsgType(),""+JCmessageType.VOICE)){
            bean.setStatus(""+JCmessageStatusType.sendIng);
            EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>
                    (JNEventBusType.CODE_MESSAGE_UPDATE, "",bean));
            JCMsgVoiceContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(bean.getContent(),Base64.NO_WRAP)), JCMsgVoiceContentBean.class);
//            JCMsgImgContentBean msgEntity = new Gson().fromJson(bean.getContent(), JCMsgImgContentBean.class);

            if (!TextUtils.isEmpty(msgEntity.getUrl())){
                SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                        ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm(),
                        JCSessionType.getTypeSIP(bean.getSessionType()),false);
                return;
            }

            JCFilesUpUtils.upCommonFile(msgEntity.getPath(), JCmessageType.VOICE, new OnHttpListener<String>() {
                @Override
                public void onSucceed(String url) {
                    try {

                        if (TextUtils.isEmpty(url)){
                            JCThreadManager.onMainHandler(() -> {
                                bean.setStatus(""+JCmessageStatusType.sendFail);
                                EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>
                                        (JNEventBusType.CODE_MESSAGE_UPDATE, "",bean));
                            });
                        }else {
                            msgEntity.setUrl(JNBaseUtilManager.getFilesUrl(url));
//
                            String msgEntityText = new Gson().toJson(msgEntity);
                            bean.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
                            JCThreadManager.onMainHandler(() -> {
                                bean.setStatus(""+JCmessageStatusType.sendSuccessceS);
                                SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                                        ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm()
                                        , JCSessionType.getTypeSIP(bean.getSessionType()),false);
                            });
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail(Exception e) {
                    JCThreadManager.onMainHandler(new Runnable() {
                        @Override
                        public void run() {
                            bean.setStatus(""+JCmessageStatusType.sendFail);
                            EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>
                                    (JNEventBusType.CODE_MESSAGE_UPDATE, "",bean));
                        }
                    });
                }
            });
        }else if(TextUtils.equals(bean.getMsgType(),""+ JCmessageType.AUDIO_VIDEO_RECORD)){
            SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                    ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm()
                    , JCSessionType.getTypeSIP(JCSessionType.CHAT_PERSION),false);
        }else if(TextUtils.equals(bean.getMsgType(),""+ JCmessageType.LOCATION)){
            SipServiceCommand.sendMessageToBuddy(context,new Gson().toJson(bean),userID
                    ,"sip:"+bean.getToSipID()+"@"+bean.getToRealm()
                    , JCSessionType.getTypeSIP(bean.getSessionType()),false);
        }
    }

    /**
     * 消息撤回或者消息被撤回
     * @param msgId
     */
    public static void withdrawMessage(String msgId){
        try {
            Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
            QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
            JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, msgId,QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();
            if (null!=localMessage){
                localMessage.setMsgType(""+JCmessageType.WITHDRAW);
                imMsgBox.put(localMessage);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 消息撤回或者消息被撤回
     * @param
     */
    public static void saveMessage(JCimMessageBean msg){
        try {
            Log.e("downloadFile", "onComplete: ==001=" );
            Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
            QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
            JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, msg.getMsgID(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();
            Log.e("downloadFile", "onComplete: ==002=" );
            if (null!=localMessage){
                Log.e("downloadFile", "onComplete: ==003=" );
                localMessage.setContent(msg.getContent());
                imMsgBox.put(localMessage);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 消息已读回执处理
     * ACK消息处理
     * @param message
     */
    public static void initReadMsgNum(JCbaseIMMessage message) {
        try {
            if (TextUtils.equals(""+JCmessageType.READACK, message.getMsgType())){
                Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
                JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, message.getContent(),QueryBuilder.StringOrder.CASE_SENSITIVE)
//                        .equal(JCimMessageBean_.sessionType, sessionType,QueryBuilder.StringOrder.CASE_SENSITIVE)
                        .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                        .build().findFirst();
                if (null!=localMessage){
                    localMessage.setRead_num(localMessage.getRead_num()+1);
                    imMsgBox.put(localMessage);
                }
            }
        }catch (Exception e){
            JNLogUtil.d("===initReadMsgNum==",e);
        }
    }
    /**
     * 通知消息，
     * 消息被撤回流程处理
     *
     * @param message
     */
    public static boolean initNotifyMsg(JCbaseIMMessage message) {
        try {
            if (TextUtils.equals(""+JCmessageType.NOTIFY, message.getMsgType())){
                JNLogUtil.d("===initNotifyMsg==message.getContent()=="+new String(Base64.decode(message.getContent(),Base64.NO_WRAP)));
                JCMsgNotifyContentBean bean = new Gson().fromJson(new String(Base64.decode(message.getContent(),Base64.NO_WRAP)), JCMsgNotifyContentBean.class);
                if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_msgWithdraw)){
                    Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                    QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
                    JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, bean.getText(),QueryBuilder.StringOrder.CASE_SENSITIVE)
//                        .equal(JCimMessageBean_.sessionType, sessionType,QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .build().findFirst();
                    if (null!=localMessage){
                        localMessage.setMsgType(""+JCmessageType.WITHDRAW);
                        imMsgBox.put(localMessage);
                    }
                    return false;
                }else if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_UpdateGroupInfo)){
//                    通知消息_群信息修改
                    EventBus.getDefault().post(new JNCodeEvent<JCimMessageBean>(JNEventBusType.CODE_MESSAGE_GROUP_INFO_UPDATE,"群信息修改", (JCimMessageBean) message));
                    return true;
                }else if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_CreateGroupNotice)){
//                    通知消息_群公告发布
                    EventBus.getDefault().post(new JNCodeEvent<JCimMessageBean>(JNEventBusType.CODE_MESSAGE_GROUP_NOTICE_SEND,"群公告发布", (JCimMessageBean) message));
                    return true;
                }else if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_GroupKicksMember)){
//                    通知消息_踢出群成员
                    return false;
                }else if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_QuitGroupChat)){
//                    通知消息_群成员退出群聊
                    return false;
                }else if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_DisbandGroup)){
//                    通知消息_群聊解散
                    return false;
                }else if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_JoinGroupAudit)){
//                   通知消息_入群申请审核通知
                    return false;
//                }else if (TextUtils.equals(bean.getType(),""+JCNotifyType.NOTIFY_SingleNotice)){
                }else {
                    String msgContent = new String(Base64.decode(message.getContent(),Base64.NO_WRAP));
                    Log.d("webmsg", "NOTIFY_SingleNotice: "+msgContent);
                    JCMsgWebNoticeContentBean contentInfo = new Gson().fromJson(msgContent, JCMsgWebNoticeContentBean.class);
                    EventBus.getDefault().post(new JNCodeEvent<>(JNEventBusType.CODE_POP_UP_NOTIFY_MSG, "",contentInfo));
                }
                return false;
            }
        }catch (Exception e){
            JNLogUtil.d("===initNotifyMsg==",e);
        }
        return false;
    }

    /**
     * 通知消息，消息被撤回流程处理
     *
     * @param message
     */
    public static JCimMessageBean getNotifyMsg(JCbaseIMMessage message) {
        try {
            if (TextUtils.equals(""+JCmessageType.NOTIFY, message.getMsgType())){
                JCMsgNotifyContentBean bean = new Gson().fromJson(new String(Base64.decode(message.getContent(),Base64.NO_WRAP)), JCMsgNotifyContentBean.class);
                if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_msgWithdraw)){
                    Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                    QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
                    JNLogUtil.e("JCchatFragment","==JCMessageUtils==getNotifyMsg==bean.getText()==" + bean.getText());
                    JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, bean.getText(),QueryBuilder.StringOrder.CASE_SENSITIVE)
//                        .equal(JCimMessageBean_.sessionType, sessionType,QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .build().findFirst();
                    if (null!=localMessage){
                        localMessage.setMsgType(""+JCmessageType.WITHDRAW);
                        JNLogUtil.e("==JCMessageUtils==getNotifyMsg==001==" );
                        return localMessage;
                    }
                    JNLogUtil.e("==JCMessageUtils==getNotifyMsg==002==" );
                }else if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_SingleNotice)){
                    JNLogUtil.e("==JCMessageUtils==getNotifyMsg==111111111111==" );
                }
            }
        }catch (Exception e){
            JNLogUtil.d("===initReadMsgNum==",e);
        }
        return (JCimMessageBean) message;
    }

}

package com.jndv.jndvchatlibrary.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
//import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;

import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;

/**
 * Author: wangguodong
 * Date: 2022/2/14
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 图片加载管理工具
 */
public class JCglideUtils {

    private static final String TAG = "JCglideUtils";

    /**
     * 加载普通图片（http://或者file://）
     */
    public static void loadImageToView(Context context, String url,final View view) {
        url = manageUrl(url);
        RequestOptions options = new RequestOptions().placeholder(R.drawable.jc_image_loading)
                .error(R.drawable.jc_loading_error);
        Glide.with(context)
                .load(url)
                .apply(options)
                .into(new CustomTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull @NotNull Drawable resource, @Nullable @org.jetbrains.annotations.Nullable Transition<? super Drawable> transition) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            view.setBackground(resource);
                        }
                    }

                    @Override
                    public void onLoadCleared(@Nullable @org.jetbrains.annotations.Nullable Drawable placeholder) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            view.setBackground(placeholder);
                        }
                    }
                });

    }

    /**
     * 加载普通图片（http://或者file://）
     */
    public static void loadImageBg(Context context, String url, ImageView imageView) {
        url = manageUrl(url);
        Glide.with(context).load(url).placeholder(R.drawable.jc_image_loading).diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.jc_loading_error).dontAnimate().into(imageView);
    }

    /**
     * 加载普通图片（http://或者file://）
     */
    public static void loadImage(Context context, String url, ImageView imageView) {
        url = manageUrl(url);
        Glide.with(context).load(url).placeholder(R.drawable.jc_image_loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.jc_loading_error).dontAnimate().into(imageView);
    }

    public static void loadImage(Context context, Uri uri, ImageView imageView) {
        Glide.with(context).load(uri).placeholder(R.drawable.jc_image_loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.jc_loading_error).dontAnimate().into(imageView);
    }

    /**
     * 加载普通图片（http://或者file://）
     */
    public static void loadOriginHeadImage(Context context, String url, ImageView imageView) {
        url = manageUrl(url);
        Glide.with(context).load(url).placeholder(R.drawable.jc_image_loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.jc_register_person).dontAnimate().into(imageView);
    }

    /**
     * 加载普通图片（http://或者file://）
     */
    public static void loadImage(Context context, String url, ImageView imageView, RequestListener<Drawable> requestListener) {
        url = manageUrl(url);
        Glide.with(context).load(url).placeholder(R.drawable.jc_image_loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.jc_loading_error)
                .dontAnimate()
                .addListener(requestListener)
                .into(imageView);
    }
    /**
     * 加载普通图片（http://或者file://）
     */
    public static void loadImage(Context context, String url, ImageView imageView, RequestBuilder<Drawable> requestBuilder) {
        url = manageUrl(url);
        Glide.with(context).load(url).placeholder(R.drawable.jc_image_loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL).error(requestBuilder)
                .dontAnimate()
                .into(imageView);
    }
    /**
     * 加载普通图片（http://或者file://）
     */
    public static RequestBuilder<Drawable> loadImageBuilder(Context context, String url) {
        url = manageUrl(url);
        return Glide.with(context).load(url).placeholder(R.drawable.jc_image_loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.jc_loading_error)
                .dontAnimate();
    }
    /**
     * 加载普通图片（http://或者file://）
     */
    public static RequestBuilder<Drawable> loadImageBuilder(Context context, String url, int widht , int hight) {
        url = manageUrl(url);
        return Glide.with(context).load(url).placeholder(R.drawable.jc_image_loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.jc_loading_error).override(widht,hight)
                .dontAnimate();
    }
    /**
     * 加载普通图片（http://或者file://）
     */
    public static void loadImage(Context context, String url, ImageView imageView, int widht , int hight, RequestBuilder<Drawable> requestBuilder) {
        url = manageUrl(url);
        Glide.with(context).load(url).placeholder(R.drawable.jc_image_loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(requestBuilder)
                .dontAnimate()
                .override(widht,hight).into(imageView);
    }
    /**
     * 加载普通图片（http://或者file://）
     */
    public static void loadImage(Context context, String url, ImageView imageView,int widht ,int hight) {
        url = manageUrl(url);
        Glide.with(context).load(url).placeholder(R.drawable.jc_image_loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.jc_loading_error).dontAnimate().override(widht,hight).into(imageView);
    }

    /**
     * 加载为圆形图片（一般为头像加载）
     */
    public static void loadCircleImage(Context context, String url, ImageView imageView) {
        url = manageUrl(url);
//        GlideUrl glideUrl = new GlideUrl(url, new LazyHeaders.Builder()
//                .addHeader("ETag","5fb1c772-82e4").build());
        RequestOptions options = new RequestOptions().placeholder(R.drawable.jc_register_person)
                .error(R.drawable.jc_register_person)
//                .circleCrop()
                .transform(new RoundedCorners(5))
                ;

        Glide.with(context).load(url).dontAnimate().apply(options)
                .into(imageView);
    }

    public static void loadCircleAvatar(Context context, String url, ImageView imageView){
        url = manageUrl(url);
        RequestOptions options = RequestOptions.bitmapTransform(new CircleCrop())
                .placeholder(R.drawable.jc_register_person)
                .error(R.drawable.jc_register_person);
        Glide.with(context).load(url)
                .apply(options)
                .into(imageView);
    }

    /**
     * 加载为圆形图片（一般为头像加载）
     */
    public static void loadCircleImage(Context context, Uri uri, ImageView imageView) {
        RequestOptions options = new RequestOptions().placeholder(R.drawable.jc_register_person)
                .error(R.drawable.jc_register_person)
                .circleCrop()
                ;
        Glide.with(context).load(uri).dontAnimate().apply(options)
                .into(imageView);
    }

    /**
     * 加载为圆形图片（一般为头像加载）
     */
    public static void loadCircleImageFromWH(Context context, String url, ImageView imageView, int width, int height) {
        url = manageUrl(url);
        RequestOptions options = new RequestOptions().placeholder(R.drawable.jc_register_person)
                .error(R.drawable.jc_register_person)
                .circleCrop() ;
        Glide.with(context).load(url).dontAnimate().apply(options)
                .override(width, height)
                .into(imageView);
    }

    /**
     * 加载本地图片（资源文件）
     */
    public static void loadLocalImage(Context context, int resId, ImageView imageView) {
        Glide.with(context).load(resId).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    /**
     * 加载为圆形图片（一般为头像加载）
     */
    public static void loadCircleImage(Context context, int resId, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .circleCrop() ;
        Glide.with(context).load(resId).dontAnimate().apply(options)
                .into(imageView);
    }

    private static String manageUrl(String url){
//        files/images
        if (null != url && !url.contains("http") && url.contains("files/images")){
            return JNBaseUtilManager.getFilesUrl(url);
        }
        return null==url?"":url ;
    }

    /**
     * 通过uri获取图片的宽高
     * @param context
     * @param uri
     * @return
     */
    public static String[] getBitmapWHFromUri(Context context, Uri uri){
        String[] sizes=new String[3];
        try {
//            Bitmap bitmap = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri));
//            sizes[0]=bitmap.getWidth();
//            sizes[1]=bitmap.getHeight();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;//这个参数设置为true才有效，
            Bitmap bmp = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri),null,options);
//            Bitmap bmp = BitmapFactory.decodeFile(path, options);//这里的bitmap是个空
            if(bmp==null){
                Log.e("通过options获取到的bitmap为空","===");
            }
            int outHeight=options.outHeight;
            int outWidth= options.outWidth;
            String outMimeType = options.outMimeType;
            sizes[0]=""+outWidth;
            sizes[1]=""+outHeight;
            sizes[2]=outMimeType;

            Log.e("RxImagePicker","     outWidth="+outWidth+"+++outHeight="+outHeight+"+++outMimeType="+outMimeType);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return sizes;
    }

    /**
     * 加载图片
     * @param context
     * @param bitmap
     * @param imageView
     */
    public static void loadImage(Context context, Bitmap bitmap, ImageView imageView){
        if(bitmap!=null) {
            if (isValidContext(context)) {
                RequestOptions options = new RequestOptions().placeholder(R.drawable.jc_image_loading)
                        .error(R.drawable.jc_loading_error);
                Glide.with(context)
                        .load(bitmap)
                        .apply(options)
                        .into(imageView);
            } else {
                Log.i("xucc", "Picture loading failed,context is null");
            }
        }
    }

    /**
     * 加载图片
     * @param context
     */
    public static void loadImage(Context context, String url, SimpleTarget<Bitmap> simpleTarget){
        Glide.with(context)
                //如果传入的url是http://..... .gif(尾缀是.gif)
                //需要添加 方法处理,其他格式的图片不需要添加
                .asBitmap()
                .placeholder(R.drawable.jc_image_loading)
		        .dontAnimate()
                .load(url)
                .into(simpleTarget);

    }
    /**
     * 加载图片
     * @param context
     */
    public static void loadImage(Context context, Uri uri, SimpleTarget<Bitmap> simpleTarget){
        Glide.with(context)
                //如果传入的url是http://..... .gif(尾缀是.gif)
                //需要添加 方法处理,其他格式的图片不需要添加
                .asBitmap()
                .placeholder(R.drawable.jc_image_loading)
		        .dontAnimate()
                .load(uri)
                .into(simpleTarget);

    }

    public static boolean isValidContext(final Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            if (activity.isDestroyed() || activity.isFinishing()) {
                return false;
            }
        }
        return true;
    }
}

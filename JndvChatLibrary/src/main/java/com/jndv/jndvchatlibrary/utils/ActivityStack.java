package com.jndv.jndvchatlibrary.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import androidx.fragment.app.FragmentActivity;
import com.ehome.manager.utils.JNLogUtil;
import java.util.ArrayList;
import java.util.List;

public class ActivityStack {

    private final static String TAG = "ActivityHolder";
    private List<FragmentActivity> activityList = new ArrayList<FragmentActivity>();
    ;
    private static ActivityStack instance;

    private ActivityStack() {
    }

    public static synchronized ActivityStack getInstance() {
        if (instance == null) {
            instance = new ActivityStack();
        }
        return instance;
    }

    /**
     * add the activity in to a list end
     *
     * @param activity
     */
    public void addActivity(FragmentActivity activity) {
        try {
            if (activity != null && activityList != null) {
                if (checkActivityIsVasivle(activity)) {
                    removeActivity(activity);
                    activityList.add(activityList.size(), activity);
                } else {
                    activityList.add(activity);
                }
                for (int i = 0; i < activityList.size(); i++) {
                    JNLogUtil.d("addActivity ==[" + i + "]" + " " + activityList.get(i));
                }

            }
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    /**
     * 获取当前Activity（堆栈中最后一个压入的）
     */
    public Activity currentActivity() {
        Activity activity = activityList.get(0);
        return activity;
    }

    /**
     * 结束当前Activity（堆栈中最后一个压入的）
     */
    public void finishCurActivity() {
        Activity activity = currentActivity();
        activityList.remove(0);
        activity.finish();
    }

    /**
     * finish all the activity in the list.
     * <p/>
     * <p/>
     * the activity calling this method hold the context
     */
    public void finishAllActivity() {
        if (activityList != null) {
            int size = activityList.size();
            for (int i = size - 1; i >= 0; i--) {
                FragmentActivity activity = activityList.get(i);
                if (activity != null) {
                    activity.finish();
                }
                JNLogUtil.d("finishAllActivity ==[" + i + "]" + " " + activity);
                activityList.remove(activity);
                // activityList.clear();
            }
        }

    }

    /**
     * finish all the activity in the list.
     * <p/>
     * <p/>
     * the activity calling this method hold the context
     */
    public void finishOtherActivity(FragmentActivity activityCurrent) {
        if (activityList != null) {
            int size = activityList.size();
            for (int i = size - 1; i >= 0; i--) {
                FragmentActivity activity = activityList.get(i);
                if (activity != null && activity != activityCurrent) {
                    if (activity != null) {
                        activity.finish();
                    }
                    JNLogUtil.d("finishAllActivity ==[" + i + "]" + " " + activity);
                    activityList.remove(activity);
                }
            }
        }

    }

    /**
     * remove the finished activity in the list.
     *
     * @param activity the activity is removed from activityList
     */
    public void removeActivity(FragmentActivity activity) {
        try {
            if (activityList != null) {
                activityList.remove(activity);
                JNLogUtil.d("removeActivity==" + " " + activity + "activityList.size===" + activityList.size());
            }
        } catch (Exception e) {
            JNLogUtil.e("removeActivity" + e.getMessage());
        }
    }


    public void pop(Class<? extends FragmentActivity> activity) {
        for (Activity a : activityList) {
            JNLogUtil.d(a.getClass().getName() + "--->" + activity.getName());
            if (a.getClass().getName().equals(activity.getName())) {
                if (!a.isFinishing()) {
                    a.finish();
                }
            }
        }
        activityList.remove(activity);
    }


    public boolean checkActivityIsVasivle(FragmentActivity activity) {
        JNLogUtil.d(" " + activityList.contains(activity));
        return activityList.contains(activity);
    }

    /**
     * <p>
     * 功能 干掉除home外的所有activity
     * </p>
     *
     * @author sss 时间 2013年12月11日 上午5:38:59
     */
    public void finishWorkActivity() {
        Activity act = null;
        for (int index = 0; index < activityList.size(); index++) {
            act = activityList.remove(index);
            if (act == null) {
                continue;
            }
            // if(act.getClass()==HomeActivity.class){
            // continue;
            // }
            act.finish();
        }
    }


    public static boolean isTopActivity(FragmentActivity activity) {
        boolean isTop = false;
        ActivityManager am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        if (cn.getClassName().contains(activity.getClass().getSimpleName())) {
            isTop = true;
        }
        return isTop;
    }
}

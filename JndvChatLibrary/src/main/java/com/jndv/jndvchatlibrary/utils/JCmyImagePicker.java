package com.jndv.jndvchatlibrary.utils;

import android.content.Context;

import com.qingmei2.rximagepicker.entity.Result;
import com.qingmei2.rximagepicker.entity.sources.Camera;
import com.qingmei2.rximagepicker.entity.sources.Gallery;

import io.reactivex.Observable;

/**
 * Author: wangguodong
 * Date: 2022/2/17
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 图片选择器使用的接口
 */
public interface JCmyImagePicker {
        // 打开相册选择图片
    @Gallery
    Observable<Result> openGallery(Context context);

        // 打开相机拍照
    @Camera
    Observable<Result> openCamera(Context context);
}

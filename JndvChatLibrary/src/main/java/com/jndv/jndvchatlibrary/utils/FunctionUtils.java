package com.jndv.jndvchatlibrary.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;
import com.jndv.jndvchatlibrary.ui.crowd.utils.LogUtil;
import com.tencent.mmkv.MMKV;
import com.tencent.smtt.sdk.CookieManager;
import com.tencent.smtt.sdk.CookieSyncManager;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Call;

/**
 * web页面设置cookie工具类
 * 没用起来，这里先注释掉
 */
public class FunctionUtils {
    private static final String TAG = "FunctionUtils";

    /**
     * 模拟登陆，获取cookie
     */
    static String dataStringSessionId;

    public static void getCookie(final Context context, final String webUrl) {
        LogUtil.e("FunctionUtils", "=======getCookie  MainApplication.sDomainName  " + GlobalConstant.getDomainBMS());
        String username= MMKV.defaultMMKV().getString(JNBaseConstans.MMKV_KEY_USER_NAME,"");
        String userpass=MMKV.defaultMMKV().getString(JNBaseConstans.MMKV_KEY_USER_PASS,"");
//        http://192.168.8.191/bms-server-api/api/users/login?
        String urlV = (GlobalConstant.getDomainBMS().startsWith("http") ? GlobalConstant.getDomainBMS() : "http://" + GlobalConstant.getDomainBMS()) + "/api/users/login?"
                + "account=" + username
                + "&pwd=" + AESCBCUtils.encrypt_AES(userpass);
        OkHttpUtils.get()
                .url(urlV)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onResponse(String response, int id) {
                        //初始化cookie
                        CookieSyncManager.createInstance(context);
                        CookieManager cookieManager = CookieManager.getInstance();
                        cookieManager.setAcceptCookie(true);
                        cookieManager.removeAllCookie();
                        Log.e("072233", "webUrl::" + webUrl);
                        Log.e("072233", "dataStringSessionId::" + dataStringSessionId);
                        cookieManager.setCookie(webUrl, dataStringSessionId);
                        Log.e("072233", "" + response);
                        cookieManager.setCookie(webUrl, "cookieUserBMID=BMID=" + LocalSharedPreferencesStorage.getConfigStrValue(context, "xcookie", GlobalConstant.sCookieVlues) + "&"
                                + "UserAccount=" + username
                                + "&passWord=" + userpass
//			    		+ LocalSharedPreferencesStorage.getConfigStrValue(context, "xcookie", MainApplication.sCookieVlues));
                                + GlobalConstant.sCookieVlues);//cookies是在HttpClient中获得的cookie
                        CookieSyncManager.getInstance().sync();
                        Log.e("112233", "cookieUserBMID=BMID=" + LocalSharedPreferencesStorage.getConfigStrValue(context, "xcookie", GlobalConstant.sCookieVlues) + "&"
                                + "UserAccount=" + username
                                + "&passWord=" + userpass
                                + GlobalConstant.sCookieVlues);
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                    }
                });
    }

    /**
     * 计算年龄
     * @param idCard 身份证号
     * @return
     */
    public static String countAge(String idCard) {
        if(TextUtils.isEmpty(idCard))
            return "未知";
        if (idCard.length() != 18 && idCard.length() != 15) {
            return "未知";
        }
        String year;
        String monthDay;
        if (idCard.length() == 18) {
            year = idCard.substring(6,10);
            monthDay = idCard.substring(10,14);
        } else {
            year = "19" + idCard.substring(6, 8);
            monthDay = idCard.substring(8, 12);
        }
        //获取当前时间字符串如：2022-1128
        String nowTimeStr = new SimpleDateFormat("yyyy-MMdd").format(new Date());
        String yearNow = nowTimeStr.substring(0, 4);// 当前年份
        String monthDayNow = nowTimeStr.substring(5, 9);// 当前月日
        int age = Integer.parseInt(yearNow) - Integer.parseInt(year);
        //age减一的情况 ：用户月日大于当前月日（开头可以为0的4位数int）
        if (Integer.parseInt(monthDay) > Integer.parseInt(monthDayNow)) {
            age = age - 1;
        }
        return age+"";
    }

}

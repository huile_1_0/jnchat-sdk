package com.jndv.jndvchatlibrary.utils;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCAddFriendApi;
import com.jndv.jndvchatlibrary.http.api.JCDelFriendApi;
import com.jndv.jndvchatlibrary.http.api.JCDeleteMessageApi;
import com.jndv.jndvchatlibrary.http.api.JCMyFriendApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCFriendBean;
import com.jndv.jnbaseutils.chat.JCFriendBean_;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;


/**
 * Author: wangguodong
 * Date: 2022/3/4
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 好友相关的工具类
 */
public class JCFriendUtil {
    private static boolean isGetFriend = false;//是否请求过好友列表

    /**
     *
     * @param activity
     * @param friendId
     * @param friendAddr
     * @param remark
     * @param friendName
     * @param friendPic
     * @param friendDomainAddrUser
     */
    public static void addFrend(Activity activity, String friendId, String friendAddr, String remark
            , String friendName, String friendPic, String friendDomainAddrUser){
        addFrend(activity, friendId, friendAddr, remark, friendName, friendPic, friendDomainAddrUser,null);
    }

    /**
     *
     * @param activity
     * @param friendId
     * @param friendAddr
     * @param remark
     * @param friendName
     * @param friendPic
     * @param friendDomainAddrUser
     * @param onHttpListener
     */
    public static void addFrend(Activity activity, String friendId, String friendAddr, String remark
            , String friendName, String friendPic, String friendDomainAddrUser, OnHttpListener<JCAddFriendApi.Bean> onHttpListener){
        if (null==onHttpListener){
            onHttpListener = new OnHttpListener<JCAddFriendApi.Bean>() {
                @Override
                public void onSucceed(JCAddFriendApi.Bean result) {
                    try {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
//                                EventBus.getDefault().post(new JNCodeEvent<String>(JNEventBusType.CODE_MESSAGE_DELETE, "", message.getMsgID()));
                            Toast.makeText(activity, "添加成功！", Toast.LENGTH_SHORT).show();
                        }else if (TextUtils.equals("1001000002", result.getCode())){
//                                EventBus.getDefault().post(new JNCodeEvent<String>(JNEventBusType.CODE_MESSAGE_DELETE, "", message.getMsgID()));
                            Toast.makeText(activity, "已成为好友！", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(activity, "添加失败！", Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFail(Exception e) {
                    Toast.makeText(activity, "添加失败！", Toast.LENGTH_SHORT).show();
                }
            };
        }
        JCAddFriendApi jcAddFriendApi = new JCAddFriendApi()
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setOperationUserId(JNBasePreferenceSaves.getUserSipId())
                .setFriendId(friendId)
                .setFriendDomainAddr(friendAddr)
                .setRemark(remark)
                .setFriendName(friendName)
                .setFriendPic(null==friendPic|| TextUtils.isEmpty(friendPic) ?"1":friendPic)
                .setFriendDomainAddrUser(friendDomainAddrUser);
        Log.e("JCFriendUtil", "addFrend: ========================="+new Gson().toJson(jcAddFriendApi));
        EasyHttp.post((LifecycleOwner) activity)
                .api(jcAddFriendApi)
                .json(new Gson().toJson(jcAddFriendApi))
                .request(onHttpListener);
    }

    public static void deletFrend(Activity activity, String friendId, String friendAddr) {
        deletFrend(activity, friendId, friendAddr, null);
    }
    public static void deletFrend(Activity activity, String friendId, String friendAddr, OnHttpListener<JCDeleteMessageApi.Bean> onHttpListener){
        if (null==onHttpListener){
            onHttpListener = new OnHttpListener<JCDeleteMessageApi.Bean>() {
                @Override
                public void onSucceed(JCDeleteMessageApi.Bean result) {
                    try {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            Toast.makeText(activity, "删除成功！", Toast.LENGTH_SHORT).show();
//                        }else if (1001000002==result.getCode()){
//                            Toast.makeText(activity, "已成为好友！", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(activity, "删除失败！", Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFail(Exception e) {
                    Toast.makeText(activity, "删除失败！", Toast.LENGTH_SHORT).show();
                }
            };
        }
        JCDelFriendApi jcDelFriendApi = new JCDelFriendApi()
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setOperationUserId(JNBasePreferenceSaves.getUserSipId())
                .setFriendId(friendId)
                .setFriendDomainAddr(friendAddr);
        EasyHttp.delete((LifecycleOwner) activity)
                .api(jcDelFriendApi)
//                .json(new Gson().toJson(jcDelFriendApi))
                .request(onHttpListener);
    }

    /**
     * 获取好友列表
     * @param lifecycleOwner
     */
    public static void initGetFriendData(LifecycleOwner lifecycleOwner) {
        if (!isGetFriend){
            getFriendListData(lifecycleOwner);
        }
    }
    /**
     * 获取好友列表
     * @param lifecycleOwner
     */
    public static void getFriendListData(LifecycleOwner lifecycleOwner) {
        Log.d("ping","getFriendListData222...start");
        getFriendListData(lifecycleOwner, new OnHttpListener<JCMyFriendApi.Bean>() {
            @Override
            public void onSucceed(JCMyFriendApi.Bean result) {
                Log.d("ping","getFriendListData222...end");
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                    if (null!=result.getData()&&result.getData().size()>0){
                        for (int i = 0; i < result.getData().size(); i++) {
                            JCMyFriendApi.Bean.DataBean dataBean = result.getData().get(i);
                            initLocalFriend(dataBean);
                        }
                    }
                }
            }

            @Override
            public void onFail(Exception e) {

            }
        });
    }

    /**
     * 获取好友列表
     * @param lifecycleOwner
     */
    public static void getFriendListData(LifecycleOwner lifecycleOwner, OnHttpListener<JCMyFriendApi.Bean> onHttpListener) {
        isGetFriend = true;
        EasyHttp.get(lifecycleOwner)
                .api(new JCMyFriendApi()
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                ).request(onHttpListener);
    }

    /**
     * 保存本地好友数据
     * @param dataBean
     */
    public static void initLocalFriend(JCMyFriendApi.Bean.DataBean dataBean){
        try {
            Box<JCFriendBean> imMsgBox = JCobjectBox.get().boxFor(JCFriendBean.class);
            QueryBuilder<JCFriendBean> builder = imMsgBox.query();
            JCFriendBean jcFriendBean = builder.equal(JCFriendBean_.fid, dataBean.getId()).build().findFirst();
            if (null!=jcFriendBean){
                setInfo(dataBean, jcFriendBean);
                jcFriendBean.setSaveUid(JNBasePreferenceSaves.getUserSipId());
                imMsgBox.put(jcFriendBean);
            }else {
                JCFriendBean friendBean = new JCFriendBean();
                setInfo(dataBean, jcFriendBean);
                friendBean.setSaveUid(JNBasePreferenceSaves.getUserSipId());
                imMsgBox.put(friendBean);
            }
        }catch (Exception e){
            JNLogUtil.e("====获取到好友列表===去处理保存本地==",e);
        }

    }

    public static void setInfo(JCMyFriendApi.Bean.DataBean dataBean, JCFriendBean friendBean) {
        if(friendBean==null)
            return;
        friendBean.setFid(dataBean.getId());
        friendBean.setUserId(dataBean.getUserId());
        friendBean.setFriendId(dataBean.getFriendId());
        friendBean.setDomainAddr(dataBean.getDomainAddr());
        friendBean.setRemark(dataBean.getRemark());
        friendBean.setFriendName(dataBean.getFriendName());
        friendBean.setFriendPic(dataBean.getFriendPic());
        friendBean.setCreateTime(dataBean.getCreateTime());
        friendBean.setDomainAddrUser(dataBean.getDomainAddrUser());
    }

    public static void setInfo(JCAddFriendApi.Bean.DataBean dataBean, JCFriendBean friendBean) {
        try{
            friendBean.setFid(dataBean.getId());
            friendBean.setUserId(dataBean.getUserId());
            friendBean.setFriendId(dataBean.getFriendId());
            friendBean.setDomainAddr(dataBean.getDomainAddr());
            friendBean.setRemark(dataBean.getRemark());
            friendBean.setFriendName(dataBean.getFriendName());
            friendBean.setFriendPic(dataBean.getFriendPic());
            friendBean.setCreateTime(dataBean.getCreateTime());
            friendBean.setDomainAddrUser(dataBean.getDomainAddrUser());
        }catch (Exception e){
            JNLogUtil.e("",e);
        }
    }

    /**
     * 保存本地好友数据
     * @param dataBean
     */
    public static void getLocalFriend(JCMyFriendApi.Bean.DataBean dataBean){
        try {
            Box<JCFriendBean> imMsgBox = JCobjectBox.get().boxFor(JCFriendBean.class);
            QueryBuilder<JCFriendBean> builder = imMsgBox.query();
            JCFriendBean jcFriendBean = builder.equal(JCFriendBean_.fid, dataBean.getId()).build().findFirst();
            if (null!=jcFriendBean){
                setInfo(dataBean, jcFriendBean);
                jcFriendBean.setSaveUid(JNBasePreferenceSaves.getUserSipId());
                imMsgBox.put(jcFriendBean);
            }else {
                JCFriendBean friendBean = new JCFriendBean();
                setInfo(dataBean, jcFriendBean);
                friendBean.setSaveUid(JNBasePreferenceSaves.getUserSipId());
                imMsgBox.put(friendBean);
            }
        }catch (Exception e){
            JNLogUtil.e("====获取到好友列表===去处理保存本地==",e);
        }
    }

}

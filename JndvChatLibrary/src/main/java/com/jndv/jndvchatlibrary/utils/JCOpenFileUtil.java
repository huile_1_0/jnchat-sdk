package com.jndv.jndvchatlibrary.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.core.content.FileProvider;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgFileContentBean;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCBagImageFragment;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCBigVideoFragment;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCUriToPathUtils;

import java.io.File;
import java.io.Serializable;

/**
 * Author: wangguodong
 * Date: 2022/7/11
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 文件打开工具类
 */
public class JCOpenFileUtil {

    private static String TAG = "JCOpenFileUtil";
    private static Context context;
    private static String authority = ".jndvchatlibrary.fileProvider";
    private static final String[] AUDIO = new String[]{"m4a","mid","xmf","ogg","wav","mp3"};
    private static final String[] IMAGE = new String[]{"jpg","gif","png","jpeg","bmp"};//OK
    private static final String[] APK = new String[]{"apk"};//OK
    private static final String[] PPT = new String[]{"ppt","pptx"};//OK
    private static final String[] EXCEL = new String[]{"xls","xlt","xlsx","xltx"};//OK
    private static final String[] WORD = new String[]{"doc","docx","dot","dotx"};//OK
    private static final String[] PDF = new String[]{"pdf"};//OK
    private static final String[] TXT = new String[]{"txt"};//OK
    private static final String[] CHM = new String[]{"chm"};
    private static final String[] VIDEO = new String[]{"mp4","avi","3gp"};//OK
/*

    public static void openFile(Context mContext, String filePath, String fileUrl, JCimMessageBean message){
        context = mContext;

        JNLogUtil.e("==JCOpenFileUtil==filePath="+filePath);
        JNLogUtil.e("==JCOpenFileUtil==fileUrl="+fileUrl);
        if (isType(IMAGE, filePath)) {//图片
            try {
                Log.e("bindContentView",filePath);
                JCBagImageFragment jcBagImageFragment = new JCBagImageFragment();
                Bundle bundle = new Bundle();
                bundle.putString(JCBagImageFragment.INPATH, filePath);
                bundle.putString(JCBagImageFragment.INURL, fileUrl);
                bundle.putSerializable(JCBagImageFragment.MESSAGE,message);
                JCbaseFragmentActivity.start(context,jcBagImageFragment,"查看大图", bundle);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if (isType(VIDEO, filePath)){
            try {
                JCBigVideoFragment jcBigVideoFragment = new JCBigVideoFragment();
                Bundle bundle = new Bundle();
                bundle.putString(JCBigVideoFragment.INPATH, filePath);
                bundle.putString(JCBigVideoFragment.INURL, fileUrl);
                JCbaseFragmentActivity.start(context, jcBigVideoFragment, getFileName("播放视频", filePath, fileUrl), bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if (isType(AUDIO, filePath)){
            try {
                JCBigVideoFragment jcBigVideoFragment = new JCBigVideoFragment();
                Bundle bundle = new Bundle();
                bundle.putString(JCBigVideoFragment.INPATH, filePath);
                bundle.putString(JCBigVideoFragment.INURL, fileUrl);
                bundle.putInt(JCBigVideoFragment.INTYPE, 1);
                JCbaseFragmentActivity.start(context, jcBigVideoFragment, getFileName("播放音频", filePath, fileUrl), bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
//            JNLogUtil.e("==JCOpenFileUtil==startActivity="+filePath);
//            JNLogUtil.e("==JCOpenFileUtil==startActivity="+filePath);
            Intent intent = JCOpenFileUtil.getTypeIntent(mContext, filePath);
            if (null==intent){
                JCChatManager.showToast("没有安装支持的打开软件！");
            }else {
                try {
                    context.startActivity(intent);
                }catch (Exception e){
                    e.printStackTrace();
                    JCChatManager.showToast("打开失败，请检查是否安装支持的软件！");
                }
            }

        }
    }
*/

    public static void openFile(Context mContext, File file, JCimMessageBean message){
        context = mContext;
        String filePath = file.getPath();
//        if (filePath.contains("content:")){
//            filePath = JCFileUtils.getFilePathByUri(context, Uri.fromFile(file));
//        }
        String fileType = "";
        String fileUrl = "";
        try {
            JCMsgFileContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(message.getContent()), JCMsgFileContentBean.class);
            fileType = bean.getFileType();
            fileUrl = bean.getUrl();
        }catch (Exception e){

        }
        JNLogUtil.e("==JCOpenFileUtil==fileType="+fileType);
        JNLogUtil.e("==JCOpenFileUtil==filePath="+filePath);

        JNLogUtil.e("==JCOpenFileUtil==filePath=222="+ JCUriToPathUtils.getRealPathFromUri(Uri.fromFile(file)));
        if (isType(IMAGE, file)||TextUtils.equals("Image", fileType)) {//图片
            try {
                Log.e("bindContentView","==JCOpenFileUtil==Image=01=");
                JCBagImageFragment jcBagImageFragment = new JCBagImageFragment();
                Bundle bundle = new Bundle();
                bundle.putString(JCBagImageFragment.INPATH, filePath);
                bundle.putString(JCBagImageFragment.INURL, fileUrl);
                bundle.putSerializable(JCBagImageFragment.MESSAGE,message);
                JCbaseFragmentActivity.start(context,jcBagImageFragment,"查看大图", bundle);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if (isType(VIDEO, file, fileType)||TextUtils.equals("Video", fileType)){
            try {
                JCBigVideoFragment jcBigVideoFragment = new JCBigVideoFragment();
                Bundle bundle = new Bundle();
                bundle.putString(JCBigVideoFragment.INPATH, filePath);
                bundle.putString(JCBigVideoFragment.INURL, fileUrl);
                JCbaseFragmentActivity.start(context, jcBigVideoFragment, getFileName("播放视频", filePath, ""), bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if (isType(AUDIO, file, fileType)||TextUtils.equals("Audio", fileType)){
            try {
                JCBigVideoFragment jcBigVideoFragment = new JCBigVideoFragment();
                Bundle bundle = new Bundle();
                bundle.putString(JCBigVideoFragment.INPATH, filePath);
                bundle.putString(JCBigVideoFragment.INURL, fileUrl);
                bundle.putInt(JCBigVideoFragment.INTYPE, 1);
                JCbaseFragmentActivity.start(context, jcBigVideoFragment, getFileName("播放音频", filePath, ""), bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
//            JNLogUtil.e("==JCOpenFileUtil==startActivity="+filePath);
//            JNLogUtil.e("==JCOpenFileUtil==startActivity="+filePath);
            Intent intent = JCOpenFileUtil.getTypeIntent(mContext, file, fileType);
            if (null==intent){
                JCChatManager.showToast("没有安装支持的打开软件！");
            }else {
                try {
                    context.startActivity(intent);
                }catch (Exception e){
                    e.printStackTrace();
                    JCChatManager.showToast("打开失败，请检查是否安装支持的软件！");
                }
            }
        }
    }

    private static String getFileName(String name, String filePath, String fileUrl){
        if (!TextUtils.isEmpty(filePath)){
            name = filePath.substring(filePath.lastIndexOf("/")+1);
        }else {
            name = fileUrl.substring(fileUrl.lastIndexOf("/")+1);
        }
        return name ;
    }

    private static boolean isType(String[] type, String filePath){
        for (int i = 0; i < type.length; i++) {
            if (filePath.toLowerCase().endsWith(type[i]))return true;
        }
        return false;
    }

    private static boolean isType(String[] type, File file){
        return isType(type, file, "");
    }
    private static boolean isType(String[] type, File file, String fileType){
        try {
            String filePath = file.getPath() ;
            String mimeType = "" ;
            Log.e(TAG, "isType: ====01==filePath==" + filePath) ;
            Uri mUri = Uri.fromFile(file);
            Log.e(TAG, "isType: ====02==mUri==" + mUri);
            Log.e(TAG, "isType: ====02==mUri.getPath()==" + mUri.getPath());
//            if (filePath.contains("content:")){
            try {
                mimeType = context.getContentResolver().getType(mUri);
                Log.e(TAG, "isType: ====02==mimeType==" + mimeType);
            }catch (Exception e){
                e.printStackTrace();
            }
//            }
            if (null==mimeType||mimeType.isEmpty()){
                try {
                    String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
                    mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                    Log.e(TAG, "isType: ====03==mimeType==" + mimeType);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            if (null==mimeType||mimeType.isEmpty()){
                mimeType = JCFileUtils.getFilePathByUri(context, Uri.fromFile(file));
                Log.e(TAG, "isType: ====04==mimeType==" + mimeType);
            }
            if (mimeType != null){
                Log.e(TAG, "isType: ======mimeType==" + mimeType);
                return isType(type, mimeType);
            }else {
                if (null!=fileType&&!fileType.isEmpty()){
                    mimeType = fileType;
                    return isType(type, mimeType);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public static Intent getTypeIntent(Context mContext, File file, String fileType) {
        String filePath = file.getPath();
        if (filePath.contains("content:")){
            filePath = JCFileUtils.getFilePathByUri(context, Uri.fromFile(file));
        }
//        Log.e(TAG, "getTypeIntent: =init:==JCOpenFileUtil=======getName==" + file.getName());
        context = mContext;
        if (!authority.startsWith(context.getApplicationInfo().packageName))
            authority = context.getApplicationInfo().packageName + authority;
        return getAllIntent(file);
//        FileUriExposedException: file:///content%3A/media/external/downloads/238 exposed beyond app through Intent.getData()
        /*
        if (isType(AUDIO, filePath)||TextUtils.equals("Audio", fileType)){//音频
//            return getAudioFileIntent(fileUrl);
            return getAudioFileIntent(file);
        } else if (isType(IMAGE, filePath)||TextUtils.equals("Image", fileType)){//图片
            return getImageFileIntent(file);
        } else if (isType(APK, filePath)) {
            return getApkFileIntent(file);
        } else if (isType(PPT, filePath)||TextUtils.equals("PPT", fileType)) {//
            return getPptFileIntent(file);
//        } else if (isType(EXCEL, filePath)||TextUtils.equals("XLS", fileType)) {//
//            return getExcelFileIntent(file);
        } else if (isType(WORD, filePath)||TextUtils.equals("XLS", fileType)||TextUtils.equals("Text", fileType)) {
            return getWordFileIntent(file);
        } else if (isType(PDF, filePath)||TextUtils.equals("PDF", fileType)) {
            return getPdfFileIntent(file);
        } else if (isType(TXT, filePath)||TextUtils.equals("Text", fileType)) {
            return getTextFileIntent(file);
        } else if (isType(CHM, filePath)) {
            return getChmFileIntent(file);
        }else if (isType(VIDEO, filePath)||TextUtils.equals("Video", fileType)){
            return getVideoFileIntent(file);
        }else{
            return getAllIntent(file);
        }*/
    }

    // Android获取一个用于打开APK文件的intent
    public static Intent getAllIntent(File file) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Intent.ACTION_VIEW);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
            JNLogUtil.e("lhl","=OpenFile=all=="+"uri+"+uri+"");
            intent.setDataAndType(uri, "*/*");
        }catch (Exception e){
            e.printStackTrace();
            Uri uri = Uri.fromFile(file);
            JNLogUtil.e("lhl","=OpenFile=getAllIntent==uri+"+uri.toString()+"");
            intent.setDataAndType(uri, "*/*");
        }
        return intent;
    }

    // Android获取一个用于打开APK文件的intent
    public static Intent getApkFileIntent(File file) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Intent.ACTION_VIEW);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
            JNLogUtil.e("lhl","uri+"+uri+"");
//        Uri uri = Uri.fromFile(new File(param));
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
        }catch (Exception e){
            e.printStackTrace();
        }
        return intent;
    }

    // Android获取一个用于打开VIDEO文件的intent
    public static Intent getVideoFileIntent(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
            JNLogUtil.e("lhl","uri+"+uri+"");
            intent.setDataAndType(uri, "video/*");
        }catch (Exception e){
            e.printStackTrace();
        }
        return intent;
    }

    // Android获取一个用于打开AUDIO文件的intent
    public static Intent getAudioFileIntent(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("oneshot", 0);
        intent.putExtra("configchange", 0);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
            JNLogUtil.e("lhl","uri+"+uri+"");
//        Uri uri = Uri.fromFile(new File(param));
//        intent.setDataAndType(uri, "audio");
            intent.setDataAndType(uri, "audio/*");
        }catch (Exception e){
            e.printStackTrace();
        }
        return intent;
    }

    // Android获取一个用于打开Html文件的intent
    public static Intent getHtmlFileIntent(String param) {
        Uri uri = Uri.parse(param).buildUpon().encodedAuthority(authority).scheme("content")
                .encodedPath(param).build();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(uri, "text/html");
        return intent;
    }

    // Android获取一个用于打开图片文件的intent
    public static Intent getImageFileIntent(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
//        Uri uri = Uri.fromFile(new File(param ));
            JNLogUtil.e("lhl","uri+"+uri+"");
            //Uri uri = Uri.fromFile(new File(param));
            intent.setDataAndType(uri, "image/*");
        }catch (Exception e){
            e.printStackTrace();
        }
        return intent;
    }

    // Android获取一个用于打开PPT文件的intent
    public static Intent getPptFileIntent(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
            JNLogUtil.e("lhl","=OpenFile=Ppt=="+"uri+"+uri+"");
//        Uri uri = Uri.fromFile(new File(param));
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        }catch (Exception e){
            e.printStackTrace();
        }
        return intent;
    }

    // Android获取一个用于打开Excel文件的intent
    public static Intent getExcelFileIntent(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
            JNLogUtil.e("lhl","=OpenFile=Excel=="+"uri+"+uri+"");
//        Uri uri = Uri.fromFile(new File(param));
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        }catch (Exception e){
            e.printStackTrace();
        }
        return intent;
    }

    // Android获取一个用于打开Word文件的intent
    public static Intent getWordFileIntent(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }

//        Uri uri = Uri.fromFile(new File(param));
            JNLogUtil.e("lhl","=OpenFile=Word=="+"uri+"+uri+"");
            intent.setDataAndType(uri, "application/msword");
        }catch (Exception e){
            e.printStackTrace();
        }
        return intent;
    }

    // Android获取一个用于打开CHM文件的intent
    public static Intent getChmFileIntent(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
            JNLogUtil.e("lhl","=OpenFile=Chm=="+"uri+"+uri+"");
//        Uri uri = Uri.fromFile(new File(param));
            intent.setDataAndType(uri, "application/x-chm");
        }catch (Exception e){
            e.printStackTrace();
        }
        return intent;
    }

    // Android获取一个用于打开文本文件的intent
    public static Intent getTextFileIntent(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
            JNLogUtil.e("lhl","=OpenFile=text=="+"uri+"+uri+"");
            intent.setDataAndType(uri, "text/plain");
        }catch (Exception e){
            e.printStackTrace();
        }
//        if (paramBoolean) {
//            Uri uri1 = Uri.parse(param);
//            intent.setDataAndType(uri1, "text/plain");
//        } else {
//            Uri uri2 = Uri.fromFile(new File(param));
//            intent.setDataAndType(uri2, "text/plain");
//        }
        return intent;
    }

    // Android获取一个用于打开PDF文件的intent
    public static Intent getPdfFileIntent(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            Uri uri = Uri.fromFile(file);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(context, authority, file);
                }
            }catch (Exception e){

            }
            JNLogUtil.e("lhl","uri+"+uri+"");
//        Uri uri = Uri.fromFile(new File(param));
            intent.setDataAndType(uri, "application/pdf");
        }catch (Exception e){
            e.printStackTrace();
        }
        return intent;
    }


}

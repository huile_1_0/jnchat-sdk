package com.jndv.jndvchatlibrary.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnDownloadListener;
import com.hjq.http.model.FileContentResolver;
import com.hjq.http.model.HttpMethod;
import com.jndv.jnbaseutils.utils.JNLogUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * @ProjectName: JndvChat
 * @Package: com.jndv.jndvchatlibrary.utils
 * @ClassName: JCFileUtils
 * @Description: java类作用描述
 * @Author: SunQinzheng
 * @CreateDate: 2022/2/25 上午 9:36
 */
public class JCFileUtils {

    public static String TAG = "JCFileUtils";

    public static String getFilePathByUri(Context context, Uri uri) {
        Log.e(TAG, "getFilePathByUri:  start  == 01 " +uri.getScheme() );
        String path = null;
        // 以 file:// 开头的
        if (ContentResolver.SCHEME_FILE.equals(uri.getScheme())) {
            path = uri.getPath();
            Log.e(TAG, "getFilePathByUri:  start  == 02 " +path);
            return path;
        }
        // 以 content:// 开头的，比如 content://media/extenral/images/media/17766
        if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme()) && Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {//&& Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT

            Log.e(TAG, "getFilePathByUri:  content  == 01 " );
            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.Media.DATA}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    if (columnIndex > -1) {
                        path = cursor.getString(columnIndex);
                    }
                }
                cursor.close();
            }
            Log.e(TAG, "getFilePathByUri:  content  == 02 " +path);
            return path;
        }
        // 4.4及之后 7.0之前 的 是以 content:// 开头的，比如 content://com.android.providers.media.documents/document/image%3A235700
        if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme()) && Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Log.e(TAG, "getFilePathByUri:  content  == 03 " );
            if (DocumentsContract.isDocumentUri(context, uri)) {
                if (isExternalStorageDocument(uri)) {
                    // ExternalStorageProvider
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        path = Environment.getExternalStorageDirectory() + "/" + split[1];
                        Log.e(TAG, "getFilePathByUri:  content  == 04 " +path);
                        return path;
                    }
                } else if (isDownloadsDocument(uri)) {
                    // DownloadsProvider
                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"),
                            Long.valueOf(id));
                    path = getDataColumn(context, contentUri, null, null);
                    Log.e(TAG, "getFilePathByUri:  content  == 05 " +path);
                    return path;
                } else if (isMediaDocument(uri)) {
                    // MediaProvider
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{split[1]};
                    path = getDataColumn(context, contentUri, selection, selectionArgs);
                    Log.e(TAG, "getFilePathByUri:  content  == 06 " +path);
                    return path;
                }
            } else {
                Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.Media.DATA}, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        if (columnIndex > -1) {
                            path = cursor.getString(columnIndex);
                        }
                    }
                    cursor.close();
                }
                Log.e(TAG, "getFilePathByUri:  content  == 07 " +path);
                return path;
            }
        }

        //7.0之后获取路径的方法
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Log.e(TAG, "getFilePathByUri:   7.0  == 01" );
            try {
                Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                Log.e(TAG, "getFilePathByUri:   7.0  == 02 =nameIndex=" +nameIndex);
                returnCursor.moveToFirst();
                String name = (returnCursor.getString(nameIndex));
                Log.e(TAG, "getFilePathByUri:   7.0  == 03 =name=" +name);
                File file = new File(context.getFilesDir(), name);
                InputStream inputStream = context.getContentResolver().openInputStream(uri);
                FileOutputStream outputStream = new FileOutputStream(file);
                int read = 0;
                int maxBufferSize = 1 * 1024 * 1024;
                int bytesAvailable = inputStream.available();

                int bufferSize = Math.min(bytesAvailable, maxBufferSize);

                final byte[] buffers = new byte[bufferSize];
                while ((read = inputStream.read(buffers)) != -1) {
                    outputStream.write(buffers, 0, read);
                }
                returnCursor.close();
                inputStream.close();
                outputStream.close();
                Log.e(TAG, "getFilePathByUri:   7.0  == 04 ==" +file.getPath());
                return file.getPath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    /**
     * 将媒体选择器路径转换为绝对路径
     *
     * @param context
     * @param contentUri
     * @return
     */
    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public static final int SIZETYPE_B = 1;//获取文件大小单位为B的double值
    public static final int SIZETYPE_KB = 2;//获取文件大小单位为KB的double值
    public static final int SIZETYPE_MB = 3;//获取文件大小单位为MB的double值
    public static final int SIZETYPE_GB = 4;//获取文件大小单位为GB的double值

    /**
     * 获取文件指定文件的指定单位的大小
     *
     * @param filePath 文件路径
     * @param sizeType 获取大小的类型1为B、2为KB、3为MB、4为GB
     * @return double值的大小
     */
    public static double getFileOrFilesSize(String filePath, int sizeType) {
        File file = new File(filePath);
        long blockSize = 0;
        try {
            if (file.isDirectory()) {
                blockSize = getFileSizes(file);
            } else {
                blockSize = getFileSize(file);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("获取文件大小", "获取失败!");
        }
        return FormetFileSize(blockSize, sizeType);
    }

    /**
     * 调用此方法自动计算指定文件或指定文件夹的大小
     *
     * @param filePath 文件路径
     * @return 计算好的带B、KB、MB、GB的字符串
     */
    public static String getAutoFileOrFilesSize(String filePath) {
        File file = new File(filePath);
        long blockSize = 0;
        try {
            if (file.isDirectory()) {
                blockSize = getFileSizes(file);
            } else {
                blockSize = getFileSize(file);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("获取文件大小", "获取失败!");
        }
        return FormetFileSize(blockSize);
    }

    /**
     * 获取指定文件大小
     *
     * @param
     * @return
     * @throws Exception
     */
    private static long getFileSize(File file) throws Exception {
        long size = 0;
        if (file.exists()) {
            FileInputStream fis = null;
            fis = new FileInputStream(file);
            size = fis.available();
        } else {
            file.createNewFile();
            Log.e("获取文件大小", "文件不存在!");
        }
        return size;
    }

    /**
     * 获取指定文件夹
     *
     * @param f
     * @return
     * @throws Exception
     */
    private static long getFileSizes(File f) throws Exception {
        long size = 0;
        File flist[] = f.listFiles();
        for (int i = 0; i < flist.length; i++) {
            if (flist[i].isDirectory()) {
                size = size + getFileSizes(flist[i]);
            } else {
                size = size + getFileSize(flist[i]);
            }
        }
        return size;
    }

    /**
     * 转换文件大小
     *
     * @param fileS
     * @return
     */
    private static String FormetFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        String wrongSize = "0B";
        if (fileS == 0) {
            return wrongSize;
        }
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "KB";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "MB";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "GB";
        }
        return fileSizeString;
    }

    /**
     * 转换文件大小,指定转换的类型
     *
     * @param fileS
     * @param sizeType
     * @return
     */
    private static double FormetFileSize(long fileS, int sizeType) {
        DecimalFormat df = new DecimalFormat("#.00");
        double fileSizeLong = 0;
        switch (sizeType) {
            case SIZETYPE_B:
                fileSizeLong = Double.valueOf(df.format((double) fileS));
                break;
            case SIZETYPE_KB:
                fileSizeLong = Double.valueOf(df.format((double) fileS / 1024));
                break;
            case SIZETYPE_MB:
                fileSizeLong = Double.valueOf(df.format((double) fileS / 1048576));
                break;
            case SIZETYPE_GB:
                fileSizeLong = Double.valueOf(df.format((double) fileS / 1073741824));
                break;
            default:
                break;
        }
        return fileSizeLong;
    }


    public static Bitmap getNetVideoBitmap(String videoUrl) {
        Bitmap bitmap = null;

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        JNLogUtil.e("======getNetVideoBitmap==" + "11111111111111111");

        try {
            //根据url获取缩略图
            retriever.setDataSource(videoUrl, new HashMap());
            JNLogUtil.e("======getNetVideoBitmap==" + "2222222222222222");
            //获得第一帧图片
            bitmap = retriever.getFrameAtTime();
            JNLogUtil.e("======getNetVideoBitmap==" + "3333333333333333");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            JNLogUtil.e("======getNetVideoBitmap==" + "444444444444444");
        } finally {
            try {
                retriever.release();
            }catch (Exception e){
                e.printStackTrace();
            }
            JNLogUtil.e("======getNetVideoBitmap==" + "5555555555555555");
        }
        JNLogUtil.e("======getNetVideoBitmap==" + "666666666666666666");
        return bitmap;
    }

    public static Bitmap getLocalVideoBitmap(String videoPath) {
        Bitmap bitmap = null;

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            //根据path获取缩略图
            retriever.setDataSource(videoPath);
            //获得第一帧图片
            bitmap = retriever.getFrameAtTime(0, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } finally {
            try {
                retriever.release();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return bitmap;
    }


    public static void downloadFile(FragmentActivity activity, String url, String name, OnDownloadListener mOnDownloadListener){
        Log.e(TAG, "downloadFile: =jc==02=url="+url);
        Log.e(TAG, "downloadFile: =jc==02=name="+name);
        try {
            if (null==name|| TextUtils.isEmpty(name)){
                if (url.contains(".")){
                    name = url.substring(url.lastIndexOf("/")+1);
                }
            }else if (!name.contains(".")){
                if (url.contains(".")){
                    String str = url.substring(url.lastIndexOf("."));
                    name = name + str;
                }else {
                    name = name + ".txt";
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if (null==name|| TextUtils.isEmpty(name)){
            name = System.currentTimeMillis() + ".txt";
        }
        File outputFile;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            ContentValues values = new ContentValues();
//            values.put(MediaStore.Downloads.DISPLAY_NAME, name);
//            // 生成一个新的 uri 路径
//            Uri outputUri = activity.getContentResolver().insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, values);
////            Uri outputUri = activity.getContentResolver().insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, values);
////            Uri outputUri = activity.getContentResolver().insert(MediaStore.Xxx.Media.EXTERNAL_CONTENT_URI, values);
//            // 适配 Android 10 分区存储特性
//            outputFile = new FileContentResolver(activity, outputUri);
            Log.e(TAG, "downloadFile: ===01==");

            outputFile = new File(activity.getExternalFilesDir(null), name);
//            if (!outputFile.exists()) {
//                outputFile.mkdirs();
//            }
        } else {
            outputFile = new File(Environment.getExternalStorageDirectory()+"/JNDV", name);
            if (!outputFile.exists()) {
                outputFile.mkdirs();
            }
            Log.e(TAG, "downloadFile: =jc==02==");
        }
        Log.e(TAG, "downloadFile: =jc==03=="+outputFile.getPath());
        EasyHttp.download(activity)
//                .method(HttpMethod.POST)
                .method(HttpMethod.GET)
                .file(outputFile)
//                .file(new File(Environment.getExternalStorageDirectory()+"/jndv", name))
                .url(url)
//                .md5(JNMD5Util.encryptLower(url))
                .listener(mOnDownloadListener).start();
    }


}

package com.jndv.jndvchatlibrary.chatSIP;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.jndv.jndvchatlibrary.R;

/**
 * Pjsip账号相关配置dialog
 * */
public class JCPjSipEditorDialog {
    //Pjsip账号配置页面
    public static void showPjSipEditorDialog(final Context context, final JCCommDialogListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog dialog = builder.create();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_pjsip_editor, null);
        final EditText numberEt = view.findViewById(R.id.pjsip_number);
        final EditText pswd = view.findViewById(R.id.pjsip_pswd);
        final EditText host = view.findViewById(R.id.pjsip_host);
        final EditText port = view.findViewById(R.id.pjsip_port);
        Button cancle = view.findViewById(R.id.pjsip_cancle);
        Button confrim = view.findViewById(R.id.pjsip_confrim);

        //设置弹入弹出动画
        Window window = dialog.getWindow();
      //  window.setWindowAnimations(R.style.AnimBottom);

        numberEt.setText(JNSpUtils.getString(context, JNPjSipConstants.PJSIP_NUMBER));
        pswd.setText(JNSpUtils.getString(context, JNPjSipConstants.PJSIP_PSWD));
        host.setText(JNSpUtils.getString(context, JNPjSipConstants.PJSIP_HOST));
        port.setText(JNSpUtils.getString(context, JNPjSipConstants.PJSIP_PORT));

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        confrim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JNSpUtils.setString(context, JNPjSipConstants.PJSIP_NUMBER, numberEt.getText().toString().trim());
                JNSpUtils.setString(context, JNPjSipConstants.PJSIP_PSWD, pswd.getText().toString().trim());
                JNSpUtils.setString(context, JNPjSipConstants.PJSIP_HOST, host.getText().toString().trim());
                JNSpUtils.setString(context, JNPjSipConstants.PJSIP_PORT, port.getText().toString().trim());
                listener.itemClick(1, "");
                dialog.dismiss();
            }
        });

        dialog.setView(view);
        dialog.show();
    }
}

package com.jndv.jndvchatlibrary.chatSIP;

import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ehome.manager.JNPjSip;
import com.ehome.manager.utils.JNLogUtil;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.sipservice.CallInfoState;
import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JCSipCallRecordBean;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNGetUserDetails;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCGetUserDetailsApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.base.NoBaseActivity;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgAudioVideoRecordContentBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserDetailBean;
import com.jndv.jndvchatlibrary.ui.crowd.utils.DateUtil;
import com.jndv.jndvchatlibrary.utils.JCDateUtils;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.pjsip.pjsua2.pjsip_inv_state;

import java.util.HashMap;

import io.objectbox.Box;

/**
 * 点对点语音通话
 * */
public class JCPjSipP2PActivity extends NoBaseActivity implements Handler.Callback, View.OnClickListener {
    private static final String TAG = "PjSipP2PActivity";
    ImageView pjsipP2pAvatar;
    TextView pjsipP2pName;
    TextView pjsip_p2p_num;
    TextView pjsipP2pConnectedTime;
    TextView pjsipP2pStatus;
    ImageView pjsipP2pHfImg;
    LinearLayout pjsipHf;
    LinearLayout pjsipHangup;
    ImageView pjsipMuteImg;
    LinearLayout pjsipMute;
    LinearLayout pjsipP2pOutging;
    LinearLayout pjsipIncomingReject;
    LinearLayout pjsipIncomingAccept;
    LinearLayout pisipP2pIncoming;
    AppCompatActivity mActivity;

    private JNPjSip pjSip;
    private String tag;
    private String businessData;
    private String businessId;
    private String calluuid;
    private String phoneNumber = null;
    private String name;
    private String mode;

    private static final int UPDATE_TIME = 1;
    private long mTimeLine = 0;
    private boolean isMute = false;

    private AudioManager audioManager = null;
    private boolean isOutSpeaker = false;//是否外放
    private boolean isConnected = false;//是否接通

    private Handler mHandler = null;
    private int callIDIn = -1;

    private int callState = -1;
    private int switchType = 0;//接通状态：0=未接通需提示手动接通；1=自动接通

    static JCbaseFragment.JCFragmentSelect mJCFragmentSelect ;

    public static void setJCFragmentSelect(JCbaseFragment.JCFragmentSelect mJCFragmentSelect) {
        JCPjSipP2PActivity.mJCFragmentSelect = mJCFragmentSelect;
    }

    private Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                Log.e(TAG, "init: name = 005 save =" );
                saveRecord();
                JNLogUtil.e(TAG,"=stopRingtoneCommand==JCPjSipP2PActivity==Handler==runnable==");
                if (null!=pjSip)pjSip.stopRingtoneCommand();
                finish();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        JNLogUtil.e(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pjsip_p2p);
        try {
            this.getSupportActionBar().hide();
        }catch (Exception e){
            JNLogUtil.e("=========", e);
        }
        JNBaseUtilManager.setIsCalling(true);
        init();
    }

    private void init() {
//        callIDIn = getIntent().getIntExtra("callID", -1);
        mActivity = this;
        pjSip = JNPjSip.getInstance(this);
        mHandler = new Handler(this);
        pjsipP2pAvatar = findViewById(R.id.pjsip_p2p_avatar);
        pjsipP2pName = findViewById(R.id.pjsip_p2p_name);
        pjsip_p2p_num = findViewById(R.id.pjsip_p2p_num);
        pjsipP2pConnectedTime = findViewById(R.id.pjsip_p2p_connected_time);
        pjsipP2pStatus = findViewById(R.id.pjsip_p2p_status);
        pjsipP2pHfImg = findViewById(R.id.pjsip_p2p_hf_img);
        pjsipHf = findViewById(R.id.pjsip_hf);
        pjsipHangup = findViewById(R.id.pjsip_hangup);
        pjsipMuteImg = findViewById(R.id.pjsip_mute_img);
        pjsipMute = findViewById(R.id.pjsip_mute);
        pjsipP2pOutging = findViewById(R.id.pjsip_p2p_outging);
        pjsipIncomingReject = findViewById(R.id.pjsip_incoming_reject);
        pjsipIncomingAccept = findViewById(R.id.pjsip_incoming_accept);
        pisipP2pIncoming = findViewById(R.id.pisip_p2p_incoming);

        pjsipHf.setOnClickListener(this);
        pjsipHangup.setOnClickListener(this);
        pjsipMute.setOnClickListener(this);
        pjsipIncomingAccept.setOnClickListener(this);
        pjsipIncomingReject.setOnClickListener(this);

        businessData = getIntent().getExtras().getString("BusinessData");
        businessId = getIntent().getExtras().getString("BusinessId");
        calluuid = getIntent().getExtras().getString("calluuid");
        tag = getIntent().getExtras().getString("tag");
        name = getIntent().getExtras().getString("name");
        phoneNumber = getIntent().getExtras().getString("number");

        callIDIn = getIntent().getExtras().getInt("callIDIn", -1);
        switchType = getIntent().getExtras().getInt("switchType", 0);
        Log.e(TAG, "init: "+phoneNumber );
        Log.e(TAG, "init: =webToAAction==calluuid==="+calluuid );
        Log.e(TAG, "init: ==ccctag: "+getIntent().getExtras().getString("ccctag") );
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
//        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        audioManager.setMode(AudioManager.MODE_RINGTONE);//MODE_NORMAL
        audioManager.setSpeakerphoneOn(true);//外放
//        audioManager.setSpeakerphoneOn(false);//语音通话默认听筒

        JNBaseUtilManager.cleanCall();
        NotificationManager manger = (NotificationManager)this.getSystemService(NOTIFICATION_SERVICE);
//        int notificationNum = getIntent().getIntExtra("notificationNum", 0);
        manger.cancelAll();

        if (TextUtils.isEmpty(phoneNumber)) {
            JCChatManager.showToast("号码错误！！");
            finish();
        }else if (TextUtils.equals(phoneNumber, JNBasePreferenceSaves.getUserSipId())) {
            JCChatManager.showToast("无法和自己通话！！");
            finish();
        }

        if (null == name || TextUtils.isEmpty(name)){
            JNBaseHttpUtils.getInstance().GetUserDetailMore(
                    EasyHttp.post(JCPjSipP2PActivity.this), phoneNumber, new OnHttpListener<JNGetUserDetails.Bean>() {
                        @Override
                        public void onSucceed(JNGetUserDetails.Bean result) {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                name = result.getData().getNickname();
                                JCThreadManager.onMainHandler(new Runnable() {
                                    @Override
                                    public void run() {
                                        pjsipP2pName.setText(name);
                                        pjsip_p2p_num.setText(phoneNumber);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFail(Exception e) {

                        }
                    });
        }

        if (tag.equals("outing")) {
            pjsipP2pName.setText(phoneNumber);
            pisipP2pIncoming.setVisibility(View.GONE);
            pjsipP2pOutging.setVisibility(View.VISIBLE);
            if (phoneNumber != null){
                Log.e(TAG, "accept: ==PjSip==webToAAction=====02=====businessId=="+businessId+"==businessData==" +businessData);

                HashMap<String, String> optionStr = new HashMap<>();
                if (null!=businessData&&!TextUtils.isEmpty(businessData)){
                    Log.e(TAG, "accept: ==PjSip==webToAAction=====03=====businessId=="+businessId+"==businessData==" +businessData);
                    optionStr.put("X-jn_ctr", "<hasBusi=1;dispatchID="+businessId+">");
                }
                optionStr.put("X-Calluuid", "<"+calluuid+">");
                callState = pjSip.onCall(phoneNumber, false, optionStr);
            }
        } else if (tag.equals("incoming")) {
            pjsipP2pName.setText(phoneNumber);
            pisipP2pIncoming.setVisibility(View.VISIBLE);
            pjsipP2pOutging.setVisibility(View.GONE);
            callState = JNPjSipConstants.PJSIP_CODE_SUCCESS;
        }

        pjSip.addCallStateListenersClean(new JNPjSip.PjSipCallStateListener() {
            @Override
            public void onCallState(String status, int code, String accountID, int callID, long connectTimestamp, boolean isLocalHold, boolean isLocalMute) {
                Log.e(TAG, "onCallState: status:" + status + " code:" + code + " accountID:" + accountID + " callID:" + callID + " connectTimestamp:" + connectTimestamp + "  isLocalHold:" + isLocalHold + " isLocalMute:" + isLocalMute);
                if (TextUtils.equals(tag, "outing")){
                    if (-1==callIDIn)callIDIn=callID;
                }
//                if (callIDIn!=callID)return; PJSIP_INV_STATE_EARLY
                if (!TextUtils.isEmpty(status)) {
                    if (code == pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED) {
                        changeSpeakerphone();

                        if(tag.equals("outing")) {
                            //开始计时
                            Message.obtain(mHandler, UPDATE_TIME).sendToTarget();
                        }
                        pjsipP2pConnectedTime.setVisibility(View.VISIBLE);
                        isConnected = true;
                        callIDIn = callID;
                        Log.e(TAG, "onCallState: ==111===callIDIn=="+callIDIn);
                    }else if (code == pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED) {
                        if (callID!=callIDIn)return;
                        if (pjsipP2pStatus != null) {
                            pjsipP2pStatus.setText(status);
                        }
                        try {
                            handler.removeCallbacks(runnable);
                            handler.postDelayed(runnable, JNBaseConstans.SIP_CLOSE_TIME);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }else if (status.contains("通话结束")) {
                        if (callID!=callIDIn)return;
                        if (pjsipP2pStatus != null) {
                            pjsipP2pStatus.setText(status);
                        }
                        try {
                            handler.removeCallbacks(runnable);
                            handler.postDelayed(runnable, JNBaseConstans.SIP_CLOSE_TIME);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
//                        handler.removeCallbacks(runnableCloseWeb);
//                        handler.postDelayed(runnableCloseWeb, 100);
                    }
                }
            }

            @Override
            public void onOutgoingCall(String status, int code, String accountID, int callID) {
                if (TextUtils.equals(tag, "outing")){
                    if (-1==callIDIn)callIDIn=callID;
                }
            }
        });
        try {
            if(tag.equals("incoming")&&1==switchType){
                pjSip.onAnswer(callIDIn, false);
                pisipP2pIncoming.setVisibility(View.GONE);
                pjsipP2pOutging.setVisibility(View.VISIBLE);
                //开始计时
                Message.obtain(mHandler, UPDATE_TIME).sendToTarget();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void saveRecord() {
        boolean isOuting = false;
        if (tag.equals("outing")) {
            isOuting = true;
        } else if (tag.equals("incoming")) {
            isOuting = false;
        }
        try {
            JCSipCallRecordBean jcSipCallRecordBean = new JCSipCallRecordBean(0, name, phoneNumber, DateUtil.getNowYMD(), isOuting, isConnected, businessData, calluuid);
            jcSipCallRecordBean.setTime(JCDateUtils.calculateFixedTime(mTimeLine));
            Box<JCSipCallRecordBean> callRecordBox = JCobjectBox.get().boxFor(JCSipCallRecordBean.class);
            callRecordBox.put(jcSipCallRecordBean);
            if(isOuting) {
                JCMsgAudioVideoRecordContentBean jcMsgAudioVideoRecordContentBean= new JCMsgAudioVideoRecordContentBean();
                jcMsgAudioVideoRecordContentBean.setConversionTime(JCDateUtils.calculateFixedTime(mTimeLine));
                jcMsgAudioVideoRecordContentBean.setIsconnected(isConnected);
                JNLogUtil.e("==JCPjSipP2PActivity=webToAAction=jcSipCallRecordBean==" + new Gson().toJson(jcSipCallRecordBean));
                if (null!=mJCFragmentSelect)mJCFragmentSelect.onSelecte(0, jcMsgAudioVideoRecordContentBean);
                EventBus.getDefault().post(new JNCodeEvent<>(JNEventBusType.CODE_ON_CLOSE_AUDIO_VIDEO, "",jcSipCallRecordBean));
            }
        }catch (Exception e){
            JNLogUtil.e("==JCPjSipP2PActivity==saveRecord==",e);
        }

    }

    @Override
    public boolean handleMessage(Message message) {
        switch (message.what) {
            case UPDATE_TIME:
                if (mTimeLine != -1) {
                    mTimeLine++;
                    if (pjsipP2pConnectedTime != null)
                        pjsipP2pConnectedTime.setText(JCDateUtils.calculateFixedTime(mTimeLine));
                    Message m = Message.obtain(mHandler, UPDATE_TIME);
                    mHandler.sendMessageDelayed(m, 1000);
                }
                break;

        }
        return false;
    }

    private long firstTime = 0;

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.pjsip_hf) {//免提
            Log.d(TAG, "audioManager.isSpeakerphoneOn()1: "+audioManager.isSpeakerphoneOn());
            if (isOutSpeaker) {
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
                audioManager.setSpeakerphoneOn(false);//听筒
                Log.d(TAG, "audioManager.isSpeakerphoneOn()2: "+audioManager.isSpeakerphoneOn());
                isOutSpeaker = false;
                pjsipP2pHfImg.setImageResource(R.drawable.message_voice_lounder);
            } else {
                audioManager.setMode(AudioManager.MODE_NORMAL);
                audioManager.setSpeakerphoneOn(true);//外放
                Log.d(TAG, "audioManager.isSpeakerphoneOn()2: "+audioManager.isSpeakerphoneOn());
                isOutSpeaker = true;
                pjsipP2pHfImg.setImageResource(R.drawable.message_voice_lounder_pressed);
            }
        } else if (id == R.id.pjsip_hangup) {//挂断
            if (System.currentTimeMillis()-firstTime>500){
                firstTime = System.currentTimeMillis();
                pjSip.onTerminate(callIDIn);
                mHandler.removeMessages(UPDATE_TIME);
//                handler.postDelayed(runnable, JNBaseConstans.SIP_CLOSE_TIME);
            }
        } else if (id == R.id.pjsip_mute) {//静音
            pjSip.toggleCallMute(callIDIn);
            if (isMute) {
                isMute = false;
                Log.e(TAG, "onClick: 关闭静音");
                pjsipMuteImg.setImageResource(R.drawable.message_voice_mute);
            } else {
                isMute = true;
                Log.e(TAG, "onClick: 开启静音");
                pjsipMuteImg.setImageResource(R.drawable.message_voice_mute_pressed);
            }
        } else if (id == R.id.pjsip_incoming_reject) {//拒接
//                pjSip.declineIncomingCall();//拒绝来电 使用此方法后，对方再拨打，我方收不到了
            if (System.currentTimeMillis()-firstTime>500){
                firstTime = System.currentTimeMillis();
                pjSip.onTerminate(callIDIn);
//                handler.postDelayed(runnable, JNBaseConstans.SIP_CLOSE_TIME);
            }
        } else if (id == R.id.pjsip_incoming_accept) {//接听
            pjSip.onAnswer(callIDIn,false);
            pisipP2pIncoming.setVisibility(View.GONE);
            pjsipP2pOutging.setVisibility(View.VISIBLE);
            //开始计时
            Message.obtain(mHandler, UPDATE_TIME).sendToTarget();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        pjSip.sipBroadcastUnRegister();
                Log.e(TAG, "onPause: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        pjSip.sipBroadcastRegister();
        Log.e(TAG, "onResume: ");
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy: ");
        try {
//            if(pjSip!=null){
//                if (callState == JNPjSipConstants.PJSIP_CODE_SUCCESS){
//                    pjSip.onTerminate(callIDIn);
//                    pjSip=null;
//                }
//            }
            audioManager.setMode(AudioManager.MODE_NORMAL);
        }catch (Exception e){
            e.printStackTrace();
        }
        //MODE_NORMAL
        super.onDestroy();
//        if (null!=pjSip)pjSip.stopRingtoneCommand();
    }

    private void changeSpeakerphone(){
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        if (JNBaseUtilManager.isHaveMicroPhone()){
            if (JNBaseUtilManager.isUseMicroPhone()){
                audioManager.setSpeakerphoneOn(false);//听筒
            }else {
                audioManager.setMode(AudioManager.MODE_NORMAL);
                audioManager.setSpeakerphoneOn(true);//外放
                isOutSpeaker = true;
                pjsipP2pHfImg.setImageResource(R.drawable.message_voice_lounder_pressed);
            }
        }else {
            audioManager.setSpeakerphoneOn(false);//听筒
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void codeEvent(JNCodeEvent codeEvent) {
        switch (codeEvent.getCode()) {
            case JNEventBusType.CODE_IVS_AUDIO_CALL_OK:
                try {
                    if (isConnected){
                        changeSpeakerphone();
                    }else {
                        pjSip.onAnswer(callIDIn,false);
                        pisipP2pIncoming.setVisibility(View.GONE);
                        pjsipP2pOutging.setVisibility(View.VISIBLE);
                        //开始计时
                        Message.obtain(mHandler, UPDATE_TIME).sendToTarget();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            pjSip.onTerminate(callIDIn);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public JNPjSip getPjSip(){
        return pjSip;
    }


}

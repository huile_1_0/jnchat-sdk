package com.jndv.jndvchatlibrary.chatSIP;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ehome.manager.JNPjSip;
import com.ehome.manager.utils.JNLogUtil;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.sipservice.CallInfoState;
import com.ehome.sipservice.SipCall;
import com.ehome.sipservice.SipServiceCommand;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNGetUserDetails;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jnbaseutils.utils.JNGlideUtils;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.http.api.JCGetUserDetailsApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.base.NoBaseActivity;
import com.jndv.jnbaseutils.chat.JCSipCallRecordBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgAudioVideoRecordContentBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserDetailBean;
import com.jndv.jndvchatlibrary.ui.crowd.utils.DateUtil;
import com.jndv.jndvchatlibrary.utils.JCDateUtils;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.pjsip.pjsua2.VideoPreview;
import org.pjsip.pjsua2.VideoWindow;
import org.pjsip.pjsua2.VideoWindowHandle;
import org.pjsip.pjsua2.pjsip_inv_state;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.objectbox.Box;

/**
 * 点对点视频通话
 */
public class JCPjSipVideoActivity extends NoBaseActivity implements View.OnClickListener, Handler.Callback {
    private static final String TAG = "PjSipVideoActivity";
    /**
     * 视频通话控件
     */
    private FrameLayout videoFrame;
    //本地视频
    private SurfaceView surfacePreview;
    //对端视频
    private SurfaceView surfaceInComingVideo;
    private LinearLayout hangUpButton;
    private LinearLayout closeCameraButton;
    private LinearLayout muteAudioButton;
    private TextView callNameTv;
    private TextView callStateTv;
    private TextView callTipCenterTv;//tv_tip_center
    private TextView callTime;

    private ImageView avatarImageView;
    private ConstraintLayout messageLayout;

    private ImageView muteIv;
    private ImageView videoIv;
    private ImageView changeCamera;

    /**
     * 被叫接听界面
     */
    private ConstraintLayout incomingLayout;
    private ImageView headImageView;
    private TextView incomingName;
    private TextView incomingNum;
    private LinearLayout rejectLL;
    private LinearLayout acceptLL;

    private VideoPreviewHandler previewHandler = new VideoPreviewHandler();
    private VideoIncomingHandler incomingHandler = new VideoIncomingHandler();

    private JNPjSip pjSip;
    private SipCall sipCall = null;

    private boolean startOsd = false ;//是否已经设置叠加文字Osd

    private VideoWindow vidWid = null;
    private VideoPreview vidPrev = null;

    private String tag;
    private String phoneNumber = null;
    private String name = "";
    private String businessData;
    private String businessId;
    private String calluuid;
    private int switchType = 0;//接通状态：0=未接通需提示手动接通；1=已接通

    private static final int UPDATE_TIME = 1;
    private long mTimeLine = 0;

    private Handler mHandler;
    private boolean isConnected = false;//是否接通

    private AudioManager audioManager = null;
    private int callState = -1;

    private int callIDIn = -1;

    static JCbaseFragment.JCFragmentSelect mJCFragmentSelect;

    public static void setJCFragmentSelect(JCbaseFragment.JCFragmentSelect mJCFragmentSelect) {
        JCPjSipVideoActivity.mJCFragmentSelect = mJCFragmentSelect;
    }

    private Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                Log.e(TAG, "init: video 05 save=" );
                saveRecord();
                JNLogUtil.e("=stopRingtoneCommand==JCPjSipVideoActivity==Handler==runnable==");
                if (null != pjSip) pjSip.stopRingtoneCommand();
                JNLogUtil.e(TAG, "=stopRingtoneCommand==001");
                activityFinish();
                JNLogUtil.e(TAG, "=stopRingtoneCommand==002");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void activityFinish() {
        finish();
//        Runtime.getRuntime().gc();
//        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("zhangming","进入JCPjSipVideoActivity...");
        setContentView(R.layout.activity_pj_sip_video);
        try {
            this.getSupportActionBar().hide();
        } catch (Exception e) {
            JNLogUtil.e("=========", e);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
            }
        }
        JNBaseUtilManager.setIsCalling(true);
        initViews();
        JNLogUtil.e(TAG, "onCreate: ");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "对不起，没有权限，无法正常使用相机", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        }
    }

    private void initViews() {
//        callIDIn = getIntent().getIntExtra("callID", -1);
        pjSip = JNPjSip.getInstance(this);
        mHandler = new Handler(this);
        videoFrame = findViewById(R.id.video_frame);
        surfacePreview = findViewById(R.id.p2p_out_video_local_surface);
        surfaceInComingVideo = findViewById(R.id.p2p_out_video_remote_surface);
        hangUpButton = findViewById(R.id.p2p_out_video_hangUp_button);
        closeCameraButton = findViewById(R.id.p2p_out_video_camera_button);
        muteAudioButton = findViewById(R.id.p2p_out_video_mute_button);
        callNameTv = findViewById(R.id.conversation_fragment_video_outing_call_name);
        callStateTv = findViewById(R.id.conversation_fragment_video_outing_waiting_text);
        callTipCenterTv = findViewById(R.id.tv_tip_center);
        callTime = findViewById(R.id.p2p_out_video_connected_time_duration);
        messageLayout = findViewById(R.id.conversation_fragment_outing_video_card_container);
        avatarImageView = findViewById(R.id.conversation_fragment_video_outing_call_avatar);
        muteIv = findViewById(R.id.p2p_out_video_mute_img);
        videoIv = findViewById(R.id.p2p_out_video_connected_open_or_close_camera_image);
        changeCamera = findViewById(R.id.p2p_out_video_reverse_camera_small);

        incomingLayout = findViewById(R.id.incoming_layout);
        headImageView = findViewById(R.id.pjsip_p2p_avatar);
        incomingName = findViewById(R.id.pjsip_p2p_name);
        incomingNum = findViewById(R.id.pjsip_p2p_num);
        rejectLL = findViewById(R.id.pjsip_incoming_reject);
        acceptLL = findViewById(R.id.pjsip_incoming_accept);

        hangUpButton.setOnClickListener(this);
        closeCameraButton.setOnClickListener(this);
        muteAudioButton.setOnClickListener(this);
        changeCamera.setOnClickListener(this);
        rejectLL.setOnClickListener(this);
        acceptLL.setOnClickListener(this);

        JNBaseUtilManager.cleanCall();

        NotificationManager manger = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
//        int notificationNum = getIntent().getIntExtra("notificationNum", 0);
        manger.cancelAll();

        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
//        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        audioManager.setMode(AudioManager.MODE_RINGTONE);
        audioManager.setSpeakerphoneOn(true);//外放

        tag = getIntent().getExtras().getString("tag");
//        name = getIntent().getExtras().getString("name");
        phoneNumber = getIntent().getExtras().getString("number");
        businessData = getIntent().getExtras().getString("BusinessData");
        businessId = getIntent().getExtras().getString("BusinessId");
        calluuid = getIntent().getExtras().getString("calluuid");

        callIDIn = getIntent().getExtras().getInt("callIDIn", -1);
        switchType = getIntent().getExtras().getInt("switchType", 0);
        if (TextUtils.isEmpty(phoneNumber)) {
            JCChatManager.showToast("号码错误！！");
//            return;
            activityFinish();
        } else if (TextUtils.equals(phoneNumber, JNBasePreferenceSaves.getUserSipId())) {
            JCChatManager.showToast("无法和自己通话！！");
//            return;
            activityFinish();
        }

        Log.e(TAG, "init: video name="+name );
        if (null == name || TextUtils.isEmpty(name)){
            Log.e(TAG, "init: video 01=" );
            JNBaseHttpUtils.getInstance().GetUserDetailMore(
                    EasyHttp.post(JCPjSipVideoActivity.this), phoneNumber, new OnHttpListener<JNGetUserDetails.Bean>() {
                        @Override
                        public void onSucceed(JNGetUserDetails.Bean result) {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                name = result.getData().getNickname();
                                JCThreadManager.onMainHandler(new Runnable() {
                                    @Override
                                    public void run() {
                                        incomingName.setText(name);
                                        incomingNum.setText(phoneNumber);
                                        callNameTv.setText(name);
                                        if (startOsd)startOSDTimer();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFail(Exception e) {

                        }
                    });
        }
        SipServiceCommand.updateVideoCodecPriorities(JCPjSipVideoActivity.this, 0);
        if (tag.equals("incoming")) {
            incomingLayout.setVisibility(View.VISIBLE);
            videoFrame.setVisibility(View.GONE);
            incomingName.setText(phoneNumber);
            callState = JNPjSipConstants.PJSIP_CODE_SUCCESS;

        } else if (tag.equals("outing")) {
            incomingLayout.setVisibility(View.GONE);
            videoFrame.setVisibility(View.VISIBLE);
            callNameTv.setText(phoneNumber);
            if (phoneNumber != null){
                HashMap<String, String> optionStr = new HashMap<>();
                if (null!=businessData&&!TextUtils.isEmpty(businessData)){
                    optionStr.put("X-jn_ctr", "<hasBusi=1;dispatchID="+businessId+">");
                }
                optionStr.put("X-Calluuid", "<"+calluuid+">");
                callState = pjSip.onCall(phoneNumber, true, optionStr);
            }
        }

        //当场景中有多个SurfaceView的时候，上层的SurfaceView可能会被下层的遮挡，这个时候需要使用setZOrderOnTop(true)或者setZOrderMediaOverlay(true)来控制SurfaceView的显示层次。
        surfacePreview.setZOrderMediaOverlay(true);
        surfaceInComingVideo.getHolder().addCallback(incomingHandler);
        surfacePreview.getHolder().addCallback(previewHandler);
        pjSip.addCallStateListenersClean(new JNPjSip.PjSipCallStateListener() {
            @Override
            public void onCallState(String status, int code, String accountID, int callID, long connectTimestamp, boolean isLocalHold, boolean isLocalMute) {
                Log.e(TAG, "onCallState: ");
                if (TextUtils.equals(tag, "outing")){
                    if (-1==callIDIn)callIDIn=callID;
                }
//                if (callIDIn!=callID)return;
                if (code == pjsip_inv_state.PJSIP_INV_STATE_CONNECTING) {

                } else if (code == pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED) {
                    //开始计时
                    changeSpeakerphone();
                    Message.obtain(mHandler, UPDATE_TIME).sendToTarget();
                    onConfigurationChanged(getResources().getConfiguration());
                    setupVideoSurface();
                    isConnected = true;
                    callIDIn = callID;
                    startOSDTimer();
                    Log.e(TAG, "onCallState: ==111===callIDIn=="+callIDIn);
                } else if (code == pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED) {
                    if (callID!=callIDIn)return;
                    callState = JNPjSipConstants.PJSIP_CODE_FAIL;
                    JNLogUtil.e(TAG,"====onCallState==end==PJSIP_INV_STATE_DISCONNECTED==finish==callID="+callID);
                    try {
                        callStateTv.setText(status);
                        callTipCenterTv.setVisibility(View.VISIBLE);
                        handler.removeCallbacks(runnable);
                        handler.postDelayed(runnable, JNBaseConstans.SIP_CLOSE_TIME);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (status.contains("通话结束")) {
                    if (callID!=callIDIn)return;
                    callState = JNPjSipConstants.PJSIP_CODE_FAIL;
                    JNLogUtil.e(TAG,"====onCallState==end==通话结束==finish==");
                    try {
                        callStateTv.setText(status);
                        callTipCenterTv.setVisibility(View.VISIBLE);
                        handler.removeCallbacks(runnable);
                        handler.postDelayed(runnable, JNBaseConstans.SIP_CLOSE_TIME);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onOutgoingCall(String status, int code, String accountID, int callID) {
                if (TextUtils.equals(tag, "outing")){
                    if (-1==callIDIn)callIDIn=callID;
                }
            }
        });

//        pjSip.setmWindowListener(new JNPjSip.WindowListener() {
//            @Override
//            public void getVideoWindow(VideoWindow videoWindow, VideoPreview videoPreview) {
//                Log.e(TAG, "getVideoWindow: ");
//                vidWid = videoWindow;
//                vidPrev = videoPreview;
//
////                onConfigurationChanged(getResources().getConfiguration());
////                setupVideoSurface();
//            }
//        });

        if (vidWid == null) {
            surfaceInComingVideo.setVisibility(View.GONE);
        }

        if (vidPrev == null) {
            surfacePreview.setVisibility(View.GONE);
        }
        try {
            if(tag.equals("incoming")&&1==switchType){
                pjSip.onAnswer(callIDIn, true);
                incomingLayout.setVisibility(View.GONE);
                videoFrame.setVisibility(View.VISIBLE);
                changeCamera.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void changeSpeakerphone(){
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        if (JNBaseUtilManager.isHaveMicroPhone()){
            if (JNBaseUtilManager.isUseMicroPhone()){
                audioManager.setSpeakerphoneOn(false);//听筒
            }else {
                audioManager.setMode(AudioManager.MODE_NORMAL);
                audioManager.setSpeakerphoneOn(true);//外放
            }
        }else {
            audioManager.setSpeakerphoneOn(false);//听筒
        }
    }

    private void startOSDTimer(){
        startOsd = true;
        if (null!=pjSip)pjSip.setVideoOSD(name);
    }

    private void saveRecord() {
        boolean isOuting = false;
        if (tag.equals("outing")) {
            isOuting = true;
        } else if (tag.equals("incoming")) {
            isOuting = false;
        }
        try {
            JCSipCallRecordBean jcSipCallRecordBean = new JCSipCallRecordBean(1, name, phoneNumber, DateUtil.getNowYMD(), isOuting, isConnected, businessData, calluuid);
//            JCSipCallRecordBean jcSipCallRecordBean = new JCSipCallRecordBean(name, phoneNumber, DateUtil.getNowYMD(), isOuting, isConnected);
//            jcSipCallRecordBean.setType(1);
            ExecutorService executorService= Executors.newFixedThreadPool(1);
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    Box<JCSipCallRecordBean> callRecordBox = JCobjectBox.get().boxFor(JCSipCallRecordBean.class);
                    callRecordBox.put(jcSipCallRecordBean);
                }
            });
            if (isOuting) {
                JCMsgAudioVideoRecordContentBean jcMsgAudioVideoRecordContentBean = new JCMsgAudioVideoRecordContentBean();
                jcMsgAudioVideoRecordContentBean.setConversionTime(JCDateUtils.calculateFixedTime(mTimeLine));
                jcMsgAudioVideoRecordContentBean.setIsconnected(isConnected);
                JNLogUtil.e("====CODE_ON_CLOSE_AUDIO_VIDEO===001===");
                if (null != mJCFragmentSelect)
                    mJCFragmentSelect.onSelecte(0, jcMsgAudioVideoRecordContentBean);
                EventBus.getDefault().post(new JNCodeEvent<>(JNEventBusType.CODE_ON_CLOSE_AUDIO_VIDEO, "", jcSipCallRecordBean));
            }
            JNLogUtil.e("====CODE_ON_CLOSE_AUDIO_VIDEO===000===");
        } catch (Exception e) {
            JNLogUtil.e("==JCPjSipP2PActivity==saveRecord==", e);
        }

    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case UPDATE_TIME:
                if (mTimeLine != -1) {
                    mTimeLine++;
                    if (callTime != null)
                        callTime.setText(JCDateUtils.calculateFixedTime(mTimeLine));
                    Message m = Message.obtain(mHandler, UPDATE_TIME);
                    mHandler.sendMessageDelayed(m, 1000);
                }
                break;

        }
        return false;
    }

    private void setupVideoSurface() {
        closeCameraButton.setVisibility(View.VISIBLE);
        muteAudioButton.setVisibility(View.VISIBLE);
        callTime.setVisibility(View.VISIBLE);
        messageLayout.setVisibility(View.GONE);

        surfaceInComingVideo.setVisibility(View.VISIBLE);
//        previewHandler.updateVideoPreview(surfacePreview.getHolder(), true);
        surfacePreview.setVisibility(View.VISIBLE);
        changeCamera.setVisibility(View.VISIBLE);
    }

    private boolean isMute = true;
    private boolean isVideo = true;

    private long firstTime = 0;

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.p2p_out_video_hangUp_button) {//
            //            endExecutorScan();
            Log.d("pjsip", "挂断state==" + callState);
            if (callState == JNPjSipConstants.PJSIP_CODE_SUCCESS)
                try {
                    if (System.currentTimeMillis() - firstTime > 500) {
                        Log.d("pjsip", "pjSip.onTerminate()");
                        firstTime = System.currentTimeMillis();
                        callTipCenterTv.setVisibility(View.VISIBLE);
                        if(pjSip.mCurrentCallId==-1){
                            finish();
                        }else {
                            pjSip.onTerminate(callIDIn);
                        }
//                        handler.postDelayed(runnable, JNBaseConstans.SIP_CLOSE_TIME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            else activityFinish();

//                previewHandler.updateVideoPreview(surfacePreview.getHolder(),true);
//                surfacePreview.setVisibility(View.VISIBLE);
        } else if (id == R.id.p2p_out_video_camera_button) {//                stopPreview();
            pjSip.setVideoMute(callIDIn, isVideo);//
            if (isVideo) {
                videoIv.setImageResource(R.drawable.message_video_pressed);
            } else {
                videoIv.setImageResource(R.drawable.conversation_connected_camera_button);
            }
            isVideo = !isVideo;
        } else if (id == R.id.p2p_out_video_mute_button) {//                mute();
            if (isMute) {
                muteIv.setImageResource(R.drawable.message_voice_mute_pressed);
            } else {
                muteIv.setImageResource(R.drawable.message_voice_mute);
            }
            isMute = !isMute;
            pjSip.toggleCallMute(callIDIn);
//            pjSip.setVideoMuteParams(isMute?isVideo?1:3:2);
        } else if (id == R.id.p2p_out_video_reverse_camera_small) {
            pjSip.switchVideoCaptureDevice(callIDIn);
        } else if (id == R.id.pjsip_incoming_reject) {//被叫拒绝
            if (System.currentTimeMillis() - firstTime > 500) {
                firstTime = System.currentTimeMillis();
                pjSip.onTerminate(callIDIn);
            }
//            handler.postDelayed(runnable, JNBaseConstans.SIP_CLOSE_TIME);
//            finish();
        } else if (id == R.id.pjsip_incoming_accept) {//被叫同意
            Log.d("zhangming","被叫同意...");
            pjSip.onAnswer(callIDIn, true);
            incomingLayout.setVisibility(View.GONE);
            videoFrame.setVisibility(View.VISIBLE);
            changeCamera.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void codeEvent(JNCodeEvent codeEvent) {
        switch (codeEvent.getCode()) {
            case JNEventBusType.CODE_IVS_AUDIO_CALL_OK:
                try {
                    if (isConnected){
                        changeSpeakerphone();
                    }else {
                        pjSip.onAnswer(callIDIn, true);
                        incomingLayout.setVisibility(View.GONE);
                        videoFrame.setVisibility(View.VISIBLE);
                        changeCamera.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        try {
            WindowManager wm;
            Display display;
            int rotation;

            wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
            display = wm.getDefaultDisplay();
            rotation = display.getRotation();
            System.out.println("Device orientation changed: " + rotation);
            pjSip.changeVideoOrientation(callIDIn,rotation);
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    @Override
    protected void onPause() {
        super.onPause();
//        pjSip.sipBroadcastUnRegister();
        Log.e(TAG, "onPause: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        pjSip.sipBroadcastRegister();
        Log.e(TAG, "onResume: ");
    }

    @Override
    protected void onDestroy() {
        try {
            if(pjSip!=null){
                if (callState == JNPjSipConstants.PJSIP_CODE_SUCCESS){
                    pjSip.onTerminate(callIDIn);
                    pjSip=null;
                }
            }
            audioManager.setMode(AudioManager.MODE_NORMAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private void updateVideoWindow(boolean show) {
        if (vidWid != null && vidPrev != null) {
            VideoWindowHandle vidWH = new VideoWindowHandle();
            if (show) {
                vidWH.getHandle().setWindow(surfaceInComingVideo.getHolder().getSurface());
            } else {
                vidWH.getHandle().setWindow(null);
            }
            try {
                vidWid.setWindow(vidWH);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private class VideoIncomingHandler implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            Log.d("zhangming", "IncomingSurfaceCreated: ");
        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
//            updateVideoWindow(true);
            Log.d("zhangming",  "IncomingSurfaceChanged: i="+i+"==i1="+i1);
            pjSip.setupIncomingVideoFeed(callIDIn, surfaceHolder.getSurface());
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            Log.d("zhangming",  "IncomingSurfaceDestroyed: ");
//            pjSip.setupIncomingVideoFeed(surfaceHolder.getSurface());
        }
    }

    private class VideoPreviewHandler implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            Log.d("zhangming","VideoPreview...surfaceCreated");
        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
//            updateVideoPreview(surfaceHolder, true);
            Log.d("zhangming","VideoPreview...surfaceChanged");
            toStartPreview(surfaceHolder);
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            Log.d("zhangming","VideoPreview...surfaceDestroyed");
//            updateVideoPreview(surfaceHolder, false);
//            pjSip.stopPreview();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (callState == JNPjSipConstants.PJSIP_CODE_SUCCESS)
                pjSip.onTerminate(callIDIn);
            else activityFinish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public JNPjSip getPjSip() {
        return pjSip;
    }


    private void toStartPreview(SurfaceHolder surfaceHolder) {
        try {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    pjSip.startPreview(callIDIn, surfaceHolder.getSurface());
                }
            }, 2000);
        } catch (Exception e) {
            JNLogUtil.d("===toStartPreview===", e);
        }

    }

}

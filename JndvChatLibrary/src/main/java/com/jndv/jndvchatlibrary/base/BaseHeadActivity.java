package com.jndv.jndvchatlibrary.base;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;


import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.zhy.autolayout.AutoLinearLayout;
import com.zhy.autolayout.AutoRelativeLayout;

/**
 * 简单返回的头部
 */
public abstract class BaseHeadActivity extends BaseActivity {
    protected TextView titleTv;
    protected TextView titleRight;
    protected ImageView moreRight;
    protected ImageView titleBack;
    protected AutoLinearLayout back;
    protected AutoRelativeLayout top_chat_root;
    private boolean isShowTitle = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_basehead_layout);
        initBaseView();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View.inflate(this, layoutResID, (ViewGroup) findViewById(R.id.base_content));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void initTitle() {
        top_chat_root.setBackgroundColor(getResources().getColor(JCChatManager.getTitleColor()));
        titleTv.setTextColor(getResources().getColor(JCChatManager.getTitleTVColor()));
        titleRight.setTextColor(getResources().getColor(JCChatManager.getTitleTVColor()));
        titleBack.setBackground(getResources().getDrawable(JCChatManager.getTitleIVBack()));
        
        if(moreRight!=null) {
            moreRight.setBackground(getResources().getDrawable(JCChatManager.getTitleIVMenu()));
        }
    }

    /**
     * 调用后 才能得到titleTv否则为空
     */
    private void initBaseView() {
        top_chat_root = (AutoRelativeLayout) findViewById(R.id.top_chat_root);
        titleTv = (TextView) findViewById(R.id.title_content);
        back = (AutoLinearLayout) findViewById(R.id.back);
        titleBack = (ImageView) findViewById(R.id.iv_back);
        titleRight = (TextView) findViewById(R.id.right);
        titleRight.setEnabled(true);
        BaseTitleClick baseTitleClick = new BaseTitleClick();
        back.setOnClickListener(baseTitleClick);
        titleRight.setOnClickListener(baseTitleClick);
        titleTv.setOnClickListener(baseTitleClick);
        if (isShowTitle()){
            top_chat_root.setVisibility(View.VISIBLE);
            initTitle();
        }else {
            top_chat_root.setVisibility(View.GONE);
        }
    }

    public boolean isShowTitle() {
        return isShowTitle;
    }

    public void setShowTitle(boolean showTitle) {
        isShowTitle = showTitle;
    }

    @Override
    public void addBroadcast(IntentFilter filter) {
    }

    @Override
    public void receiveBroadcast(Intent intent) {

    }

    @Override
    public void receiveMessage(Message msg) {

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 设置中间标题
     *
     * @param titleText
     */
    public void setCenterTitle(String titleText) {
        if (titleText != null) {
            if (titleTv != null) {
                titleTv.setText(titleText);
            }
        }
    }

    /**
     * @param text
     * @param drawableRes 设置右侧的按钮，可显示文字或图片
     */
    public void setTitleRight(String text, Drawable drawableRes) {
        if (titleRight == null) {
            return;
        }
        if (text == null && drawableRes == null) {
            titleRight.setVisibility(View.GONE);
        } else {
            titleRight.setVisibility(View.VISIBLE);
        }
        if (text != null) {
            titleRight.setText(text);
            //titleRight.setBackgroundResource(R.color.colorTransparents);
        }
        if (drawableRes != null) {
            titleRight.setBackgroundDrawable(drawableRes);
            titleRight.setText("");
        }

    }

    /**
     * 标题按钮的点击事件
     */
    private class BaseTitleClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.back) {
                onBackClick();
            } else if (id == R.id.right) {
                onRightClick();
            }else if (id ==R.id.title_content){
                onTitleClick();
            }
        }
    }

    /**
     * 标题Title，
     */
    protected void onTitleClick() {
    }

    /**
     * 标题中右边的部分，
     */
    protected void onRightClick() {
    }

    /**
     * 返回按钮的点击事件
     */
    public void onBackClick() {
        back();
    }

//    protected void toDetail(long id) {
//        if (!GlobalHolder.getInstance().isServerConnected()) {
//            showToast(getString(R.string.error_connect_to_server));
//            return;
//        }
////        if (CommonUtil.isFastClick()){
////            return;
////        }
//        PviewImRequest.invokeNative(PviewImRequest.NATIVE_GET_USER_INFO, id);
//
//
//        mHandler.postDelayed(() -> runOnUiThread(() -> {
//            Intent intent = new Intent(mContext, InformationDetailActivity.class);
//            //intent.putExtra("isSelf",false);
//            intent.putExtra("userId", id);
//            startActivity(intent);
//        }),100);
//
//    }
}
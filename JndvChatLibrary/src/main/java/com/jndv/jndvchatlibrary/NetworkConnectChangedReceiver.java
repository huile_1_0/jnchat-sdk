package com.jndv.jndvchatlibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.ehome.sipservice.SipServiceCommand;

import org.greenrobot.eventbus.EventBus;

public class NetworkConnectChangedReceiver extends BroadcastReceiver {

    String TAG="jnpjsip";
    int connectState=1;//0:未连接 1:已连接

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive: ====" + intent.getAction());
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
            ConnectivityManager manager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                if (activeNetwork.isConnected()) {
                    //连接vpn和sip
                    Log.d(TAG,"NetworkConnectChangedReceiver...连接上网络了...");
                    if(connectState==0){
//                        EventBus.getDefault().post(new RefreshVpnEvent());
                        JCChatManager.reLoginUser();
//                        JCChatManager.loginUser(JCChatManager.inspurName, JCChatManager.inspurPass,
//                                JCChatManager.mActivity, JCChatManager.mLoginListener
//                        );
                    }
                    connectState=1;
                } else {
                    Log.d(TAG, "当前没有网络连接，请确保你已经打开网络 ");
                    connectState=0;
                    SipServiceCommand.logout(context);
                }
            } else {   // not connected to the internet
                Log.d(TAG, "当前没有网络连接，请确保你已经打开网络 ");
                connectState=0;
                SipServiceCommand.logout(context);
            }
        }
    }



}

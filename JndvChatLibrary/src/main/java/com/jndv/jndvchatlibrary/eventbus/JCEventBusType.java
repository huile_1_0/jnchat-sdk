package com.jndv.jndvchatlibrary.eventbus;//package com.jndv.jndvchatlibrary.eventbus;
//
//public class JCEventBusType {
//
//    public static final int CODE_MESSAGE_WITHDRAW = 1 ;// 消息撤销
//    public static final int CODE_MESSAGE_DELETE = 2 ;// 消息撤销
//    public static final int CODE_MESSAGE_READ_USER_LIST = 3 ;// 消息详情，已读未读人员列表
//    public static final int CODE_MESSAGE_UPDATE = 4 ;// 更新消息,1-聊天会话中更新消息发送状态
//    public static final int CODE_SESSION_LIST_UPDATE_MSG = 5 ;// 更新消息列表中的最新一条消息信息
//    public static final int CODE_SESSION_LIST_UPDATE_NONUM = 6 ;// 更新消息列表中的未读数量
//    public static final int CODE_SESSION_LIST_UPDATE_INFO = 7 ;// 更新消息列表中的信息，免打扰，置顶,退群状态等
//    public static final int CODE_SESSION_LIST_UPDATE_NET = 8 ;// 刷新列表
//
//    public static final int CODE_MESSAGE_CROWD_DELETE = 9 ;// 更新消息列表中的信息，免打扰，置顶等
//
//    public static final int CODE_MESSAGE_GROUP_INFO_UPDATE = 10 ;// 更新群组信息
//    public static final int CODE_MESSAGE_GROUP_NOTICE_SEND = 11 ;// 群公告发布
//    public static final int CODE_MESSAGE_TRANSMIT =12 ;// 消息转发
//
//
//    public static final int CODE_SIP_CONNECT_OK = 13 ;// sip重连成功
//    public static final int CODE_SIP_CONNECT_NO = 14 ;// sip断开链接
//
//    public static final int CODE_SIP_LOGIN_OUT = 15 ;// 异地登录
//
//    public static final int CODE_ON_SELECT_LOCATION = 100 ;// 选择地址发送
//    public static final int CODE_ON_CLOSE_AUDIO_VIDEO = 101 ;// 选择地址发送
//    public static final int CODE_SESSION_LIST_DELETE = 16 ;// 删除会话信息
//
//    public static final int CODE_POP_UP_NOTIFY_MSG = 2020;
//
//
//
//
//}

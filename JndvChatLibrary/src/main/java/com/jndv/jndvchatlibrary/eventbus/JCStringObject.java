package com.jndv.jndvchatlibrary.eventbus;

/**
 * Author: wangguodong
 * Date: 2022/2/22
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 只有一条文字的对象实体
 */
public class JCStringObject {
    String str ;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}

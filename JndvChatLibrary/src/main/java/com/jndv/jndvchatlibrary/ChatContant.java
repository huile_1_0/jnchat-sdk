package com.jndv.jndvchatlibrary;

import com.jndv.jndvchatlibrary.bean.OrganizationPersonBean;
import com.jndv.jnbaseutils.ui.bean.Node;

import java.util.ArrayList;
import java.util.List;

public interface ChatContant {
    List<Node> TTS_PERSON_LIST=new ArrayList<>();
    /**
     * getSessionList接口、getDeptInfo接口
     * 获取到的信息都会转化为OrganizationPersonBean实体对象，都会存放到这个list列表中
     * 但相同的sipID字段在两个接口中却没统一转化到一个字段上！！！！！
     * 一个IM_Number一个otherId，使用居然是分开的
     * 在点击发送按钮，去转发消息时没办法区分，获取到的数据有为空数据，使用时必闪退！！！
     * */
    List<OrganizationPersonBean> TRANSFER_PERSON_LIST=new ArrayList<>();
    List<OrganizationPersonBean> TRANSFER_PERSON_LIST_OLD=new ArrayList<>();


    public static final String PREFERENCES_APP_TOKEN= "appToken";
    public static final String PREFERENCES_APP_ACCOUNT= "appAccount";
    public static final String PREFERENCES_APP_NICKNAME= "appNickName";
    public static final String PREFERENCES_APP_USERID= "appUserId";
//    public static final String PREFERENCES_APP_ZONINGCODE= "appZoningCode";
    public static final String PREFERENCES_APP_IDCARD= "appIdCard";
    public static final String PREFERENCES_APP_HEADICON= "appHeadIcon";

}

package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * Author: wangguodong
 * Date: 2022/3/22
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 获取用户信息，重要的是SIP号和SIP端口
 */
public class JCUpdateUserHeadApi implements IRequestApi {
    //    http://192.168.2.205:8000/user-info/getUserDetail?userId=29080
    @Override
    public String getApi() {
        return "/user-avatar/updateUserAvatar";
    }

    private String domainAddrUser;
    private String userId;
    private String picUrl;
    private String picName;
    private String mobile;
    private String sex;
    private String userAge;
    private String password;
    private String nickname;

    public JCUpdateUserHeadApi setDomainAddr(String domainAddr) {
        this.domainAddrUser = domainAddr;
        return this;
    }

    public JCUpdateUserHeadApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JCUpdateUserHeadApi setPicUrl(String picUrl) {
        this.picUrl = picUrl;
        return this;
    }

    public JCUpdateUserHeadApi setPicName(String picName) {
        this.picName = picName;
        return this;
    }

    public JCUpdateUserHeadApi setNickName(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public JCUpdateUserHeadApi setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public JCUpdateUserHeadApi setGender(String sex) {
        this.sex = sex;
        return this;
    }

    public JCUpdateUserHeadApi setAge(String userAge) {
        this.userAge = userAge;
        return this;
    }

    public JCUpdateUserHeadApi setPassword(String password) {
        this.password = password;
        return this;
    }

    public JCUpdateUserHeadApi() {
    }

}

package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

public class JCFriendRelationShipApi implements IRequestApi {

    @Override
    public String getApi() {
        return "/address-book/getFriendInfo";
    }

    private String operationUserId;
    private String friendId;

    private String friendDomainAddr;

    public JCFriendRelationShipApi setOperationUserId(String operationUserId) {
        this.operationUserId = operationUserId;
        return this;
    }

    public JCFriendRelationShipApi setFriendId(String friendId) {
        this.friendId = friendId;
        return this;
    }

    public JCFriendRelationShipApi setFriendDomainAddr(String friendDomainAddr) {
        this.friendDomainAddr = friendDomainAddr;
        return this;
    }
}

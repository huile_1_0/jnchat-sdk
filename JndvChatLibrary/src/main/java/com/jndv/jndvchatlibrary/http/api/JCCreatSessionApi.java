package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;
import com.jndv.jnbaseutils.chat.JCSessionListBean;

/**
 * Author: wangguodong
 * Date: 2022/4/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 我收藏消息接口
 */
public class JCCreatSessionApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/session/create";
    }

//    domainAddr	是	string	域地址
//userId	是	number	用户SIP id
//sessionPartyId	是	number	会话方SIP id（私聊为对方用户 id、群聊为群 id）
//sessionPartyDomainAddr	是	string	会话方所在域地址不能为空
//type	是	number	会话类型【1：私聊；2：群聊】
//sessionPartyName	是	string	会话方名称
//sessionPartyPic	是	string	会话方头像

    private String domainAddr;
    private String userId;
    private String sessionPartyId;
    private String sessionPartyDomainAddr;
    private String sessionPartyDomainAddrUser;
    private String type;
    private String sessionPartyName;
    private String sessionPartyPic;
    private int requestBy;

    public JCCreatSessionApi setRequestBy(int requestBy) {
        this.requestBy = requestBy;
        return this;
    }

    public JCCreatSessionApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCCreatSessionApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JCCreatSessionApi setSessionPartyId(String sessionPartyId) {
        this.sessionPartyId = sessionPartyId;
        return this;
    }

    public JCCreatSessionApi setSessionPartyDomainAddr(String sessionPartyDomainAddr) {
        this.sessionPartyDomainAddr = sessionPartyDomainAddr;
        return this;
    }

    public JCCreatSessionApi setSessionPartyDomainAddrUser(String sessionPartyDomainAddrUser) {
        this.sessionPartyDomainAddrUser = sessionPartyDomainAddrUser;
        return this;
    }

    public JCCreatSessionApi setSessionPartyName(String sessionPartyName) {
        this.sessionPartyName = sessionPartyName;
        return this;
    }

    public JCCreatSessionApi setSessionPartyPic(String sessionPartyPic) {
        this.sessionPartyPic = sessionPartyPic;
        return this;
    }

    public JCCreatSessionApi setType(String type) {
        this.type = type;
        return this;
    }

    public final static class Bean{
        /**
         * code : 200
         * msg :
         */
        private String code;
        private String msg;
        private JCSessionListBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public JCSessionListBean getData() {
            return data;
        }

        public void setData(JCSessionListBean data) {
            this.data = data;
        }
    }
}

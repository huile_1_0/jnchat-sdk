package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 分页获取入群邀请审核记录
 */
public class JCJoinGroupAuditUpdateApi implements IRequestApi {
    /**
     * domainAddr	是	string	(审核人)用户SIP域地址
     * domainAddrUser	是	string	(审核人)用户Java域地址
     * auditStatus	是	number	审核结果，1—-通过，2—-拒绝
     * id	是	number	邀请入群申请记录id
     * userId	是	string	(审核人)用户SIP id
     * groupId	是	string	群聊 id
     */
    @Override
    public String getApi() {
        return "/join-group-audit/update";
    }

    private String userId;
    private String domainAddr;
    private String domainAddrUser;
    private String groupId;
    private String auditStatus;
    private String id;

    public JCJoinGroupAuditUpdateApi(String userId, String domainAddr, String domainAddrUser, String groupId, String auditStatus, String id) {
        this.userId = userId;
        this.domainAddr = domainAddr;
        this.domainAddrUser = domainAddrUser;
        this.groupId = groupId;
        this.auditStatus = auditStatus;
        this.id = id;
    }
}
package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCReadUserBean;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 获取群消息已读未读人员列表接口
 */
public class JCMessageReadUserApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/groupMsgState/readUnreadPersons";
    }
//  http://192.168.2.205:8000/groupMsgState/readUnreadPersons?domainAddr=192.168.1.5&groupMsgId=0
    private String domainAddr;
    private String groupMsgUuid;

    public JCMessageReadUserApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCMessageReadUserApi setGroupMsgUuid(String groupMsgUuid) {
        this.groupMsgUuid = groupMsgUuid;
        return this;
    }

    public final static class Bean{
        /**
         * code : 200
         * msg :
         */
        private String code;
        private String msg;
        private ReadBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public ReadBean getData() {
            return data;
        }

        public void setData(ReadBean data) {
            this.data = data;
        }

        public class ReadBean{
            private List<JCReadUserBean> read ;
            private List<JCReadUserBean> unread ;

            public List<JCReadUserBean> getRead() {
                return read;
            }

            public void setRead(List<JCReadUserBean> read) {
                this.read = read;
            }

            public List<JCReadUserBean> getUnread() {
                return unread;
            }

            public void setUnread(List<JCReadUserBean> unread) {
                this.unread = unread;
            }
        }

    }
}

package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;
import com.jndv.jnbaseutils.chat.JCSessionListBean;

import java.util.List;

//我的会话列表接口
public class JCSessionListApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/session/getSessionList";
    }

    private String domainAddr;
    private String userId;
    private int pageNo;
    private int pageSize;

    public JCSessionListApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCSessionListApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JCSessionListApi setPageNo(int pageNo) {
        this.pageNo = pageNo;
        return this;
    }

    public JCSessionListApi setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public final static class Bean{

        /**
         * code : 200
         * msg :
         * data : [{"id":0,"other_id":0,"domain_addr":"***","other_type":"***","other_name":"***","other_pic":"***","remarks":"***","is_top":0,"is_nodisturbing":0,"create_time":"***","unread_number":"***","last_time":"***","first_time":"***","first_content":"***","status":0}]
         */

        private String code;
        private String msg;
        private List<JCSessionListBean> data;

        @Override
        public String toString() {
            return "Bean{" +
                    "code=" + code +
                    ", msg='" + msg + '\'' +
                    ", data=" + data +
                    '}';
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public List<JCSessionListBean> getData() {
            return data;
        }

        public void setData(List<JCSessionListBean> data) {
            this.data = data;
        }


    }
}
package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 获取组织机构及用户信息
 */
public class JCGetOrganizationMonitorApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/department/getDeptAndMonitorInfo";
    }
    private String zoningCode;

    public JCGetOrganizationMonitorApi setZoningCode(String zoningCode) {
        this.zoningCode = zoningCode;
        return this;
    }

    public final static class Bean{

        /**
         * code : 200
         * msg :
         * data : [{"id":0,"other_id":0,"domain_addr":"***","other_type":"***","other_name":"***","other_pic":"***","remarks":"***","is_top":0,"is_nodisturbing":0,"create_time":"***","unread_number":"***","last_time":"***","first_time":"***","first_content":"***","status":0}]
         */

        private String code;
        private String msg;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            List<DepartBean> departs;//组织机构

            List<MonitorBean> accountInfoAndState;

            public List<DepartBean> getDeparts() {
                return departs;
            }

            public void setDeparts(List<DepartBean> departs) {
                this.departs = departs;
            }

            public List<MonitorBean> getMonitor() {
                return accountInfoAndState;
            }

            public void setMonitor(List<MonitorBean> users) {
                this.accountInfoAndState = users;
            }
        }

        public static class DepartBean {
            /**
             * "id":100,
             * "deptname":"兰山区"*/
            private int id;
            private String deptname;
            private String ZoningCode;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getDeptname() {
                return deptname;
            }

            public void setDeptname(String deptname) {
                this.deptname = deptname;
            }

            public String getZoningCode() {
                return ZoningCode;
            }

            public void setZoningCode(String zoningCode) {
                ZoningCode = zoningCode;
            }
        }

        public static class MonitorBean {
            /**
             * "IPCType": "0",
             *                 "OnlineState": 0,
             *                 "IOState": "",
             *                 "DisplayName": "兰山测试",
             *                 "Lon": 0.000000,
             *                 "id": 11924,
             *                 "PZoneCode": "371302",
             *                 "Lat": 0.000000,
             *                 "AccountName": "lsgly"
             * * */
            private String IPCType;
            private String OnlineState;
            private String IOState;
            private String DisplayName;
            private String Lon;
            private String Lat;
            private int id;
            private String PZoneCode;
            private String AccountName;

            public String getIPCType() {
                return IPCType;
            }

            public void setIPCType(String IPCType) {
                this.IPCType = IPCType;
            }

            public String getOnlineState() {
                return OnlineState;
            }

            public void setOnlineState(String onlineState) {
                OnlineState = onlineState;
            }

            public String getIOState() {
                return IOState;
            }

            public void setIOState(String IOState) {
                this.IOState = IOState;
            }

            public String getDisplayName() {
                return DisplayName;
            }

            public void setDisplayName(String displayName) {
                DisplayName = displayName;
            }

            public String getLon() {
                return Lon;
            }

            public void setLon(String lon) {
                Lon = lon;
            }

            public String getLat() {
                return Lat;
            }

            public void setLat(String lat) {
                Lat = lat;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPZoneCode() {
                return PZoneCode;
            }

            public void setPZoneCode(String PZoneCode) {
                this.PZoneCode = PZoneCode;
            }

            public String getAccountName() {
                return AccountName;
            }

            public void setAccountName(String accountName) {
                AccountName = accountName;
            }
        }

    }
}

package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

public class JCDelFriendApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/address-book/delFriend";
    }

    private String domainAddr;
    private String operationUserId;
    private String friendId;
    private String friendDomainAddr;

    public JCDelFriendApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCDelFriendApi setOperationUserId(String operationUserId) {
        this.operationUserId = operationUserId;
        return this;
    }

    public JCDelFriendApi setFriendId(String friendId) {
        this.friendId = friendId;
        return this;
    }

    public JCDelFriendApi setFriendDomainAddr(String friendDomainAddr) {
        this.friendDomainAddr = friendDomainAddr;
        return this;
    }

    public static final class Bean{

        /**
         * code : 200
         * msg :
         * data : {}
         */

        private String code;
        private String msg;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
        }
    }
}

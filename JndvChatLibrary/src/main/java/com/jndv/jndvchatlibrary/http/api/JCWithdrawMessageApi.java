package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 我收藏消息接口
 */
public class JCWithdrawMessageApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/messageRecord/withdraw";
    }

    private String domainAddr;
    private String userId;
    private String msgUuid;

    public JCWithdrawMessageApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCWithdrawMessageApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JCWithdrawMessageApi setMsgUuid(String msgUuid) {
        this.msgUuid = msgUuid;
        return this;
    }

    public final static class Bean{
        /**
         * code : 200
         * msg :
         */
        private String code;
        private String msg;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

    }
}

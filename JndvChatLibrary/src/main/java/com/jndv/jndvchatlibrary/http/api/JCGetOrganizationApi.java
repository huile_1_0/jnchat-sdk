package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 获取组织机构及用户信息
 */
public class JCGetOrganizationApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/department/getDeptInfo";
    }
//    http://192.168.2.205:8000/department/getDeptInfo?parentid=0
    private String parentid;

    public JCGetOrganizationApi setParentid(String parentid) {
        this.parentid = parentid;
        return this;
    }

    public final static class Bean{

        /**
         * code : 200
         * msg :
         * data : [{"id":0,"other_id":0,"domain_addr":"***","other_type":"***","other_name":"***","other_pic":"***","remarks":"***","is_top":0,"is_nodisturbing":0,"create_time":"***","unread_number":"***","last_time":"***","first_time":"***","first_content":"***","status":0}]
         */

        private String code;
        private String msg;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            List<DepartBean> departs;
            List<UserBean> users;

            public List<DepartBean> getDeparts() {
                return departs;
            }

            public void setDeparts(List<DepartBean> departs) {
                this.departs = departs;
            }

            public List<UserBean> getUsers() {
                return users;
            }

            public void setUsers(List<UserBean> users) {
                this.users = users;
            }
        }

        public static class DepartBean {
            /**
             * "id":100,
             * "deptname":"兰山区"*/
            private int id;
            private String deptname;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getDeptname() {
                return deptname;
            }

            public void setDeptname(String deptname) {
                this.deptname = deptname;
            }
        }
        public static class UserBean {
            /**
             * id	Long	用户id - 主键
             * account	string	用户账号
             * nickName	string	用户名称
             * avatarurl	string	用户头像（不带http地址，需客户端自己拼接）
             * mobile	string	手机号
             * IM_Number	string	IM内线号（唯一，与FreeSwitch绑定）
             * Office_Number	string	办公室内线号
             * Gateway_Number	string	中继线号码（关联的中继线）
             * Djj_Number	string	对讲机（对讲机内线号码）
             * MobilePrefix	string	手机号前缀，默认为空，外地号码前为0
             * FSStatus	string	fs同步状态（0，未同步或失败，1初始同步，密码未同步；2是完全初始化，密码也一致）当前数据库存储值为空，
             * */
            private int id;
            private String account;
            private String nickName;
            private String avatarurl;
            private String mobile;
            private String IM_Number;
            private String Office_Number;
            private String Gateway_Number;
            private String Djj_Number;
            private String MobilePrefix;
            private String FSStatus;
            private String deptUserDomainAddr;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getAccount() {
                return account;
            }

            public void setAccount(String account) {
                this.account = account;
            }

            public String getNickName() {
                return nickName;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public String getAvatarurl() {
                return avatarurl;
            }

            public void setAvatarurl(String avatarurl) {
                this.avatarurl = avatarurl;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getIM_Number() {
                return IM_Number;
            }

            public void setIM_Number(String IM_Number) {
                this.IM_Number = IM_Number;
            }

            public String getOffice_Number() {
                return Office_Number;
            }

            public void setOffice_Number(String office_Number) {
                Office_Number = office_Number;
            }

            public String getGateway_Number() {
                return Gateway_Number;
            }

            public void setGateway_Number(String gateway_Number) {
                Gateway_Number = gateway_Number;
            }

            public String getDjj_Number() {
                return Djj_Number;
            }

            public void setDjj_Number(String djj_Number) {
                Djj_Number = djj_Number;
            }

            public String getMobilePrefix() {
                return MobilePrefix;
            }

            public void setMobilePrefix(String mobilePrefix) {
                MobilePrefix = mobilePrefix;
            }

            public String getFSStatus() {
                return FSStatus;
            }

            public void setFSStatus(String FSStatus) {
                this.FSStatus = FSStatus;
            }

            public String getDeptUserDomainAddr() {
                return deptUserDomainAddr;
            }

            public void setDeptUserDomainAddr(String deptUserDomainAddr) {
                this.deptUserDomainAddr = deptUserDomainAddr;
            }
        }

    }
}

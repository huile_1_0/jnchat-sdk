package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 分页获取入群邀请审核记录
 */
public class JCGroupInvitationApprovalApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/group-invitation-approval/page";
    }

    private String domainAddr;
    private String userId;
    private String approvalStatus;
    private int pageNo;
    private int pageSize;

    public JCGroupInvitationApprovalApi(String domainAddr, String userId, String approvalStatus, int pageNo, int pageSize) {
        this.domainAddr = domainAddr;
        this.userId = userId;
        this.approvalStatus = approvalStatus;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }
}
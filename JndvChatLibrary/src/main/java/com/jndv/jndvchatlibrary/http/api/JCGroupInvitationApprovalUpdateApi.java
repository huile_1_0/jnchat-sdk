package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 分页获取入群邀请审核记录
 */
public class JCGroupInvitationApprovalUpdateApi implements IRequestApi {

    /**
     * domainAddr	是	string	用户SIP域地址
     * domainAddrUser	是	string	用户Java域地址
     * approvalStatus	是	number	审核结果，1—-通过，2—-拒绝
     * id	是	number	邀请入群申请记录id
     * userId	是	string	用户SIP id
     */

    @Override
    public String getApi() {
        return "/group-invitation-approval/update";
    }

    private String userId;
    private String domainAddr;
    private String domainAddrUser;
    private String approvalStatus;
    private String id;

    public JCGroupInvitationApprovalUpdateApi(String userId, String domainAddr, String domainAddrUser, String approvalStatus, String id) {
        this.userId = userId;
        this.domainAddr = domainAddr;
        this.domainAddrUser = domainAddrUser;
        this.approvalStatus = approvalStatus;
        this.id = id;
    }
}
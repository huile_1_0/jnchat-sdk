package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;
import com.jndv.jnbaseutils.chat.JCimMessageBean;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 获取消息记录（下拉加载、上拉加载）。
 */
public class JCMessageRecordApi implements IRequestApi {
    String apiName = "/messageRecord/page";

    @Override
    public String getApi() {
        return apiName;
    }

    public JCMessageRecordApi setApiName(String apiName) {
        this.apiName = apiName;
        return this;
    }
    private String domainAddr;
    private String userId;
    private String sessionId;//会话列表中获取到的会话自增ID
    private String sessionPartyId;
    private String sessionPartyDomainAddr;
    private String sessionType;

    private String beginTime;
    private String slideType;//滑动类型：1（上滑）2（下拉）

    private String pageNo;
    private String pageSize;

    public JCMessageRecordApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCMessageRecordApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JCMessageRecordApi setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public JCMessageRecordApi setPageNo(String pageNo) {
        this.pageNo = pageNo;
        return this;
    }

    public JCMessageRecordApi setPageSize(String pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public JCMessageRecordApi setSessionPartyId(String sessionPartyId) {
        this.sessionPartyId = sessionPartyId;
        return this;
    }

    public JCMessageRecordApi setSessionPartyDomainAddr(String sessionPartyDomainAddr) {
        this.sessionPartyDomainAddr = sessionPartyDomainAddr;
        return this;
    }

    public JCMessageRecordApi setSessionType(String sessionType) {
        this.sessionType = sessionType;
        return this;
    }

    public JCMessageRecordApi setBeginTime(String beginTime) {
        this.beginTime = beginTime;
        return this;
    }

    public JCMessageRecordApi setSlideType(String slideType) {
        this.slideType = slideType;
        return this;
    }

    public final static class Bean{

        /**
         * code : 200
         * msg :
         * data : [{"id":0,"other_id":0,"domain_addr":"***","other_type":"***","other_name":"***","other_pic":"***","remarks":"***","is_top":0,"is_nodisturbing":0,"create_time":"***","unread_number":"***","last_time":"***","first_time":"***","first_content":"***","status":0}]
         */

        private String code;
        private String msg;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            private int total;
            private List<JCimMessageBean> list;

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<JCimMessageBean> getList() {
                return list;
            }

            public void setList(List<JCimMessageBean> list) {
                this.list = list;
            }
        }
    }
}

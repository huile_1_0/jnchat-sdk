package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * Author: wangguodong
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 获取用户信息，重要的是SIP号和SIP端口
 *
 */
public class JCGetUserDetailsApi implements IRequestApi {
//    http://192.168.2.205:8000/user-info/getUserDetail?userId=29080
    @Override
    public String getApi() {
        return "/user-info/getUserDetail";
    }

    private String userSIPId;
    private String domainAddr;

    public JCGetUserDetailsApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCGetUserDetailsApi setUserId(String userId) {
        this.userSIPId = userId;
        return this;
    }

    public JCGetUserDetailsApi() {
    }

    public JCGetUserDetailsApi(String userSIPId, String domainAddr) {
        this.userSIPId = userSIPId;
        this.domainAddr = domainAddr;
    }
}

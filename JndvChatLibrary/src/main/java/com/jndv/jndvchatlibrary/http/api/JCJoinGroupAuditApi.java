package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * 分页获取入群邀请审核记录
 */
public class JCJoinGroupAuditApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/join-group-audit/page";
    }

    private String domainAddr;
    private String domainAddrUser;
    private String userId;
    private String groupId;
    private String auditStatus;
    private String groupDomainAddr;
    private String groupDomainAddrUser;
    private int pageNo;
    private int pageSize;

    public JCJoinGroupAuditApi(String domainAddr, String domainAddrUser, String userId, String groupId, String auditStatus, String groupDomainAddr, String groupDomainAddrUser, int pageNo, int pageSize) {
        this.domainAddr = domainAddr;
        this.domainAddrUser = domainAddrUser;
        this.userId = userId;
        this.groupId = groupId;
        this.auditStatus = auditStatus;
        this.groupDomainAddr = groupDomainAddr;
        this.groupDomainAddrUser = groupDomainAddrUser;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }
}
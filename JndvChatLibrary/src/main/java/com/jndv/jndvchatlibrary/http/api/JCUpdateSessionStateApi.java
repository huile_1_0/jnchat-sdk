package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

//更新会话信息
public class JCUpdateSessionStateApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/session/state";
    }
//domainAddr	是	string	域地址
//sessionId	是	number	会话 id
//top	是	boolean	是否置顶
//noDisturb	是	boolean	是否免打扰
    private String domainAddr;
    private String sessionId;
    private int top;
    private int noDisturb;

    public JCUpdateSessionStateApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCUpdateSessionStateApi setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public JCUpdateSessionStateApi setTop(int top) {
        this.top = top;
        return this;
    }

    public JCUpdateSessionStateApi setNoDisturb(int noDisturb) {
        this.noDisturb = noDisturb;
        return this;
    }

    public static final class Bean{

        /**
         * code : 200
         * msg : 成功
         * data : {}
         */

        private String code;
        private String msg;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
        }
    }
}

package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 我收藏消息接口
 */
public class JCGetCollectMessageApi implements IRequestApi {
    String interfaceName = "/msgCollect/{userId}/list";

    @Override
    public String getApi() {
        return interfaceName;
    }

    private String domainAddr;
    private String userId;

    public JCGetCollectMessageApi setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
        return this;
    }

    public JCGetCollectMessageApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCGetCollectMessageApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public final static class Bean{
        /**
         * code : 200
         * msg :
         */
        private String code;
        private String msg;
        private List<ColloctBean> data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public List<ColloctBean> getData() {
            return data;
        }

        public void setData(List<ColloctBean> data) {
            this.data = data;
        }
    }

    public final static class ColloctBean{

        private String id;
        private String userId;
        private String body;
        private String type;
        private String createTime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }
    }
}

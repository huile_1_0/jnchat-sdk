package com.jndv.jndvchatlibrary.http.api;



import com.hjq.http.config.IRequestApi;

/**
 * Author: wangguodong
 * Date: 2022/5/9
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCAddFriendApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/address-book/addFriend";
    }
    //domainAddr	是	string	域地址
//operationUserId	是	number	用户SIP id
//friendId	是	number	好友SIP id
//friendDomainAddr	是	string	好友所在sip域
//remark	否	string	好友备注
//friendName	是	string	好友昵称
//friendPic	是	string	好友头像
//friendDomainAddrUser	是	string	好友所在java域
    private String domainAddr;
    private String operationUserId;
    private String friendId;
    private String friendDomainAddr;
    private String remark;
    private String friendName;
    private String friendPic;
    private String friendDomainAddrUser;

    public JCAddFriendApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCAddFriendApi setOperationUserId(String operationUserId) {
        this.operationUserId = operationUserId;
        return this;
    }

    public JCAddFriendApi setFriendId(String friendId) {
        this.friendId = friendId;
        return this;
    }

    public JCAddFriendApi setFriendDomainAddr(String friendDomainAddr) {
        this.friendDomainAddr = friendDomainAddr;
        return this;
    }

    public JCAddFriendApi setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public JCAddFriendApi setFriendName(String friendName) {
        this.friendName = friendName;
        return this;
    }

    public JCAddFriendApi setFriendPic(String friendPic) {
        this.friendPic = friendPic;
        return this;
    }

    public JCAddFriendApi setFriendDomainAddrUser(String friendDomainAddrUser) {
        this.friendDomainAddrUser = friendDomainAddrUser;
        return this;
    }

    public static final class Bean{

        /**
         * code : 200
         * msg :
         * data : {}
         */

        private String code;
        private String msg;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public final static class DataBean{

            private int id;
            private String userId;
            private String friendId;
            private String domainAddr;
            private String remark;
            private String createTime;
            private String friendName;
            private String friendPic;
            private String domainAddrUser;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getFriendId() {
                return friendId;
            }

            public void setFriendId(String friendId) {
                this.friendId = friendId;
            }

            public String getDomainAddr() {
                return domainAddr;
            }

            public void setDomainAddr(String domainAddr) {
                this.domainAddr = domainAddr;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getFriendName() {
                return friendName;
            }

            public void setFriendName(String friendName) {
                this.friendName = friendName;
            }

            public String getFriendPic() {
                return friendPic;
            }

            public void setFriendPic(String friendPic) {
                this.friendPic = friendPic;
            }

            public String getDomainAddrUser() {
                return domainAddrUser;
            }

            public void setDomainAddrUser(String domainAddrUser) {
                this.domainAddrUser = domainAddrUser;
            }
        }
    }
}


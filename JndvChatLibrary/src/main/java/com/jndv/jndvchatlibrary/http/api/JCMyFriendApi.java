package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 获取离线消息列表接口。首次进入聊天页面使用
 */
public class JCMyFriendApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/address-book/getAddressBook";
    }
//    http://192.168.2.205:8000/offline-message-record/page?domainAddr=192.168.1.5&pageNo=1&pageSize=20&userId=29283&sessionPartyId=31100&sessionPartyDomainAddr=192.168.1.5&sessionType=1
    private String userId;

    public JCMyFriendApi setUserId(String operationUserId) {
        this.userId = operationUserId;
        return this;
    }

    public final static class Bean{

        /**
         * code : 200
         * msg :
         * data : [{"id":0,"other_id":0,"domain_addr":"***","other_type":"***","other_name":"***","other_pic":"***","remarks":"***","is_top":0,"is_nodisturbing":0,"create_time":"***","unread_number":"***","last_time":"***","first_time":"***","first_content":"***","status":0}]
         */

        private String code;
        private String msg;
        private List<DataBean> data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * id	number	自增 id
             * userId	number	用户SIP id
             * friendId	number	好友SIP id
             * domainAddr	string	好友所处域地址
             * remark	string	备注
             * friendName	string	好友昵称
             * friendPic	string	好友头像
             * createTime	number	发布时间
             */

            private int id;
            private String userId;
            private String friendId;
            private String domainAddr;//sip域地址
            private String remark;
            private String friendName;
            private String friendPic;
            private String createTime;
            private String domainAddrUser;//java域地址

            public String getDomainAddrUser() {
                return domainAddrUser;
            }

            public void setDomainAddrUser(String domainAddrUser) {
                this.domainAddrUser = domainAddrUser;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getFriendId() {
                return friendId;
            }

            public void setFriendId(String friendId) {
                this.friendId = friendId;
            }

            public String getDomainAddr() {
                return domainAddr;
            }

            public void setDomainAddr(String domainAddr) {
                this.domainAddr = domainAddr;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getFriendName() {
                return friendName;
            }

            public void setFriendName(String friendName) {
                this.friendName = friendName;
            }

            public String getFriendPic() {
                return friendPic;
            }

            public void setFriendPic(String friendPic) {
                this.friendPic = friendPic;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }
        }
    }
}

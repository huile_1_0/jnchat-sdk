package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;
import com.hjq.http.config.IRequestPath;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 我收藏消息接口
 */
public class JCFilesUpApi implements IRequestApi {
    @Override
    public String getApi() {
//        这里服务端又在serverinfo中将头storage-server-api直接返回了，这里需要删除头
        return "/storage-server-api/Control/uploads";
//        这里服务端需要区分项目所以加了头storage-server-api
//        return "/storage-server-api/Control/uploads";
    }

    private List<String> files;

    public List<String> getFiles() {
        return files;
    }

    public JCFilesUpApi setFiles(List<String> files) {
        this.files = files;
        return this;
    }

    public final static class ImageBean{
        private String base64;

        public ImageBean(String base64) {
            this.base64 = base64;
        }

        public String getBase64() {
            return base64;
        }

        public void setBase64(String base64) {
            this.base64 = base64;
        }
    }

    public final static class FileUrl{
        private String upload;
        private String url;
        private String tailUrl;
        private String tailThumbnailUrl;

        public String getTailUrl() {
            return tailUrl;
        }

        public String getTailThumbnailUrl() {
            return tailThumbnailUrl;
        }

        public String getUrl() {
            return url;
        }

        public String getUpload() {
            return upload;
        }
    }
    public final static class Bean{
//        {"msg":"success","type":"png","urlArr":[{"url":"group1/M02/00/E4/wKgBGGLVAdaAN8n6AAAxrhPskOo576.png"}]}
//        {"urlArr":[{"upload":"success","url":"group1/M00/00/DC/wKgBGGLOhSSAXZOaAABMAFKhJj4545.xls"}]}
//        {
//            "msg" : "success",
//                "type" : "jpeg",
//                "urlArr" : [
//            {
//                "url" : "group1/M04/00/7E/wKgBGGJTlgCAbWuTAAJG4t40k4w478.png"
//            }
//    	]
//        }
        private String msg;
        private String type;
        private List<FileUrl> urlArr;

        public String getType() {
            return type;
        }

        public List<FileUrl> getUrlArr() {
            return urlArr;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

    }
}

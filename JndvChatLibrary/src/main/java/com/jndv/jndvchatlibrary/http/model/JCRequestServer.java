package com.jndv.jndvchatlibrary.http.model;

import com.hjq.http.config.IRequestServer;
import com.hjq.http.model.BodyType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCconstants;

/**
 *    desc   : 服务器配置
 */
public class JCRequestServer implements IRequestServer {

    @Override
    public String getHost() {
        return JCconstants.HOST_URL;
    }

    @Override
    public String getPath() {
        return "";
    }

    @Override
    public BodyType getType() {
        // 以表单的形式提交参数
        return BodyType.FORM;
    }
}
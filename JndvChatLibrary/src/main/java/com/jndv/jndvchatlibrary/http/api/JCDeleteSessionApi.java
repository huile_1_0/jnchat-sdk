package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

//删除我的会话接口
public class JCDeleteSessionApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/session/delete";
    }

    private String domainAddr;
    private String operationUserId;
    private String conversationId;

    public JCDeleteSessionApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCDeleteSessionApi setOperationUserId(String operationUserId) {
        this.operationUserId = operationUserId;
        return this;
    }

    public JCDeleteSessionApi setConversationId(String conversationId) {
        this.conversationId = conversationId;
        return this;
    }

    public static final class Bean{

        /**
         * code : 200
         * msg : 成功
         * data : {}
         */

        private String code;
        private String msg;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
        }
    }
}

package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 我收藏消息接口
 */
public class JCDeleteMessageApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/messageRecord/deleteMsg";
    }

    private String domainAddr;
    private String uuid;
    private String userId;

    public JCDeleteMessageApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCDeleteMessageApi setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }
    public JCDeleteMessageApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public final static class Bean{
        /**
         * code : 200
         * msg :
         */
        private String code;
        private String msg;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

    }

}

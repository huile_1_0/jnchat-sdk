package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;
import com.jndv.jnbaseutils.chat.JCimMessageBean;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 获取离线消息列表接口。首次进入聊天页面使用
 */
public class JCNotLineMessageApi implements IRequestApi {
    String apiName = "/offline-message-record/page";

    @Override
    public String getApi() {
        return apiName;
    }

    private String domainAddr;
    private String userId;
    private String sessionPartyId;
    private String sessionPartyDomainAddr;
    private String sessionType;
    private String pageNo;
    private String pageSize;

    public JCNotLineMessageApi setApiName(String apiName) {
        this.apiName = apiName;
        return this;
    }
    public JCNotLineMessageApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCNotLineMessageApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JCNotLineMessageApi setPageNo(String pageNo) {
        this.pageNo = pageNo;
        return this;
    }

    public JCNotLineMessageApi setPageSize(String pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public JCNotLineMessageApi setSessionPartyId(String sessionPartyId) {
        this.sessionPartyId = sessionPartyId;
        return this;
    }

    public JCNotLineMessageApi setSessionPartyDomainAddr(String sessionPartyDomainAddr) {
        this.sessionPartyDomainAddr = sessionPartyDomainAddr;
        return this;
    }

    public JCNotLineMessageApi setSessionType(String sessionType) {
        this.sessionType = sessionType;
        return this;
    }

    public final static class Bean{

        /**
         * code : 200
         * msg :
         * data : [{"id":0,"other_id":0,"domain_addr":"***","other_type":"***","other_name":"***","other_pic":"***","remarks":"***","is_top":0,"is_nodisturbing":0,"create_time":"***","unread_number":"***","last_time":"***","first_time":"***","first_content":"***","status":0}]
         */

        private String code;
        private String msg;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            private List<JCimMessageBean> list;
            private int total;

            public List<JCimMessageBean> getList() {
                return list;
            }

            public void setList(List<JCimMessageBean> list) {
                this.list = list;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }
        }

    }
}

package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 图片上传
 */
public class JCImagesUpApi implements IRequestApi {
    @Override
    public String getApi() {
        //        这里服务端又在serverinfo中将头storage-server-api直接返回了，这里需要删除头
        return "/storage-server-api/Control/saveImg";
    }

    private String type;//图片类型 jpg等
    private String groupName;
    private String isYs;
    private List<ImageBean> picImgArr;

    public JCImagesUpApi setType(String type) {
        this.type = type;
        return this;
    }

    public JCImagesUpApi setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public JCImagesUpApi setIsYs(String isYs) {
        this.isYs = isYs;
        return this;
    }

    public JCImagesUpApi setPicImgArr(List<ImageBean> picImgArr) {
        this.picImgArr = picImgArr;
        return this;
    }

    public final static class ImageBean{
        private String base64;

        public ImageBean(String base64) {
            this.base64 = base64;
        }

        public String getBase64() {
            return base64;
        }

        public void setBase64(String base64) {
            this.base64 = base64;
        }
    }

    public final static class ImageUrl{
        private String url;
        private String tailUrl;

        public String getUrl() {
            return url;
        }

        public String getTailUrl() {
            return tailUrl;
        }
    }
    public final static class Bean{
//        {
//            "msg" : "success",
//                "type" : "jpeg",
//                "urlArr" : [
//            {
//                "url" : "group1/M04/00/7E/wKgBGGJTlgCAbWuTAAJG4t40k4w478.png"
//            }
//    	]
//        }
        private String msg;
        private String type;
        private List<ImageUrl> urlArr;
        public String getType() {
            return type;
        }

        public List<ImageUrl> getUrlArr() {
            return urlArr;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

    }
}

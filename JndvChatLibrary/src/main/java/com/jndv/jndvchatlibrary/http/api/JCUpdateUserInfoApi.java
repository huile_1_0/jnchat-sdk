package com.jndv.jndvchatlibrary.http.api;

import com.hjq.http.config.IRequestApi;

/**
 * Author: wangguodong
 * Date: 2022/3/22
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 获取用户信息，重要的是SIP号和SIP端口
 */
public class JCUpdateUserInfoApi implements IRequestApi {
//    http://192.168.2.205:8000/user-info/getUserDetail?userId=29080
    @Override
    public String getApi() {
        return "/user-info/updateUserInfo";
    }

    private String domainAddrUser;
    private String userId;
    private String picUrl;
    private String picName;
    private String mobile;
    private String sex;
    private String userAge;
    private String password;
    private String nickname;

    public JCUpdateUserInfoApi setDomainAddr(String domainAddr) {
        this.domainAddrUser = domainAddr;
        return this;
    }

    public JCUpdateUserInfoApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JCUpdateUserInfoApi setPicUrl(String picUrl) {
        this.picUrl = picUrl;
        return this;
    }

    public JCUpdateUserInfoApi setPicName(String picName) {
        this.picName = picName;
        return this;
    }

    public JCUpdateUserInfoApi setNickName(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public JCUpdateUserInfoApi setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public JCUpdateUserInfoApi setGender(String sex) {
        this.sex = sex;
        return this;
    }

    public JCUpdateUserInfoApi setAge(String userAge) {
        this.userAge = userAge;
        return this;
    }

    public JCUpdateUserInfoApi setPassword(String password) {
        this.password = password;
        return this;
    }

    public JCUpdateUserInfoApi() {
    }

}

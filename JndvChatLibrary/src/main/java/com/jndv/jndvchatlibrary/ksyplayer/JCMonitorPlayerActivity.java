package com.jndv.jndvchatlibrary.ksyplayer;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.annotation.OptIn;
import androidx.media3.common.MediaItem;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.Player;
import androidx.media3.common.util.UnstableApi;
import androidx.media3.exoplayer.ExoPlayer;
import androidx.media3.exoplayer.rtsp.RtspMediaSource;
import androidx.media3.exoplayer.source.MediaSource;
import androidx.media3.ui.PlayerView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.tencent.smtt.sdk.WebView;

/**
 * Author: wangguodong
 * Date: 2022/7/20
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 监控播放类
 */
public class JCMonitorPlayerActivity extends JCbaseFragmentActivity {

    private static final String TAG = "MonitorPlayerActivity";
    WebView wv_btm;
    ImageView iv_all;
    boolean isAll = false;
    // 主码流地址，子码流地址
    String path_url, web_url, title;

    ExoPlayer player;
    PlayerView playerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor_player);
        init();
    }
    /**
     * 初始化
     */
    public void init() {
        showLoadding();
        // 视频播放地址
        path_url = getIntent().getStringExtra("player_url");
        web_url = getIntent().getStringExtra("web_url");
        title = getIntent().getStringExtra("title");
        setCenterTitle(title);
        goneTopLine();
        JNLogUtil.d(TAG, "==播放地址=" + path_url);
        iv_all = (ImageView) findViewById(R.id.iv_all);
        wv_btm = (WebView) findViewById(R.id.wv_btm);

        iv_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAll){
//                    showTitle();
//                    setTranslucentStatus(JCMonitorPlayerActivity.this, false, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                    wv_btm.setVisibility(View.VISIBLE);
//                    iv_all.setBackgroundResource(R.drawable.ic_ping_all);
                }else {
                    //
//                    hindTitle();
//                    setTranslucentStatus(JCMonitorPlayerActivity.this, true, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                    wv_btm.setVisibility(View.GONE);
//                    iv_all.setBackgroundResource(R.drawable.ic_ping_all_no);
                }
                isAll  = !isAll;
            }
        });

        initPlayer();
    }


    @OptIn(markerClass = UnstableApi.class) private void initPlayer(){
        player = new ExoPlayer.Builder(this).build();
        String path = path_url;
//        String path = "rtsp://admin:jndv12345@192.168.1.37";
//    String path = "rtsp://172.20.96.189:554/rtp/37130000012008800000_37130202121326010072";
        MediaSource mediaSource =
                new RtspMediaSource.Factory().setForceUseRtpTcp(true).createMediaSource(MediaItem.fromUri(path));
//                new RtspMediaSource.Factory().createMediaSource(MediaItem.fromUri(path_url));
        ExoPlayer player = new ExoPlayer.Builder(this).build();
        player.setMediaSource(mediaSource);

        playerView = findViewById(R.id.player_view);
        playerView.setPlayer(player);
        player.addListener(new Player.Listener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                Player.Listener.super.onPlayerStateChanged(playWhenReady, playbackState);
                Log.e(TAG, "onPlayerStateChanged: ========" +playbackState);
//                Player.COMMAND_CHANGE_MEDIA_ITEMS
                player.play();
            }

            @Override
            public void onIsPlayingChanged(boolean isPlaying) {
                Player.Listener.super.onIsPlayingChanged(isPlaying);
                Log.e(TAG, "onIsPlayingChanged: ==isPlaying=="+isPlaying);
                if (true){
                    cancelLoadding();
                }
            }

            @Override
            public void onIsLoadingChanged(boolean isLoading) {
                Player.Listener.super.onIsLoadingChanged(isLoading);
                Log.e(TAG, "onIsLoadingChanged: ==isLoading=="+isLoading);
            }

            @Override
            public void onPlayerError(PlaybackException error) {
                Player.Listener.super.onPlayerError(error);
                Log.e(TAG, "onPlayerError: ==error=="+new Gson().toJson(error));
            }

            @Override
            public void onPlaybackStateChanged(int playbackState) {
                Player.Listener.super.onPlaybackStateChanged(playbackState);
                Log.e(TAG, "onPlaybackStateChanged: ==playbackState=="+playbackState);
            }

            @Override
            public void onPlayerErrorChanged(@Nullable PlaybackException error) {
                Player.Listener.super.onPlayerErrorChanged(error);
                Log.e(TAG, "onPlayerErrorChanged: ==error=="+new Gson().toJson(error));
            }

            @Override
            public void onPlayWhenReadyChanged(boolean playWhenReady, int reason) {
                Player.Listener.super.onPlayWhenReadyChanged(playWhenReady, reason);
                Log.e(TAG, "onPlayWhenReadyChanged: ==playWhenReady=="+playWhenReady);
            }
        });
        player.prepare();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            JNLogUtil.d(TAG, "==LANDSCAPE==");
            isAll = true;
            hindTitle();
            setTranslucentStatus(JCMonitorPlayerActivity.this, true, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            wv_btm.setVisibility(View.GONE);
            iv_all.setBackgroundResource(R.drawable.ic_ping_all_no);
        } else if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            JNLogUtil.d(TAG, "==PORTRAIT==");
            isAll = false;
            showTitle();
            setTranslucentStatus(JCMonitorPlayerActivity.this, false, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            wv_btm.setVisibility(View.VISIBLE);
            iv_all.setBackgroundResource(R.drawable.ic_ping_all);
        }
    }

    /**
     * 播放释放
     */
    private void videoPlayEnd() {
        cancelLoadding();
        if (player != null) {
            player.release();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        JNLogUtil.d(TAG, "=====onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoPlayEnd();
        JNLogUtil.d(TAG, "=====onDestroy");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        JNLogUtil.d(TAG, "=====onStop");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (KeyEvent.KEYCODE_BACK == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
            videoPlayEnd();
        }
        return super.onKeyDown(keyCode, event);
    }

}

package com.jndv.jndvchatlibrary.ksyplayer;//package com.jndv.jndvchatlibrary.ksyplayer;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.content.res.Configuration;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.MotionEvent;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.RelativeLayout;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.Nullable;
//import androidx.annotation.OptIn;
//import androidx.media3.common.MediaItem;
//import androidx.media3.common.PlaybackException;
//import androidx.media3.common.Player;
//import androidx.media3.common.util.UnstableApi;
//import androidx.media3.exoplayer.ExoPlayer;
//import androidx.media3.exoplayer.rtsp.RtspMediaSource;
//import androidx.media3.exoplayer.source.MediaSource;
//import androidx.media3.ui.PlayerView;
//
//import com.google.gson.Gson;
//import com.jndv.jndvchatlibrary.R;
//import com.jndv.jndvchatlibrary.ui.base.NoBaseActivity;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created by Cool on 2017/4/20. 视频播放
// */
//
//public class VideoPlayerActivity extends NoBaseActivity {
////    KSYTextureView ksytextureView;
//    String TAG = "VideoPlayerActivity——ksy";
//    ProgressDialog dialog;
//    // 主码流地址，子码流地址
//    String path_url, path_url_sub = "";
//    ExoPlayer player;
//    PlayerView playerView;
//
//    // 是否子码流
//    boolean isSub = true;
//    private Spinner spinner;
//    private String[] spinnerItems = {"子码流", "主码流"};
//    private boolean isFirst = true;
//    private RelativeLayout layout;
//    private static final int CODE_SHOWBACKLINE = 66;
//    private static final int CODE_HIDEBACKLINE = 77;
//
//    @SuppressLint("HandlerLeak")
//    private Handler handler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case CODE_SHOWBACKLINE:
//                    if (layout.getVisibility() == View.GONE) {
//                        layout.setVisibility(View.VISIBLE);
//                        handler.removeMessages(CODE_HIDEBACKLINE);
//                        handler.sendEmptyMessageDelayed(CODE_HIDEBACKLINE, 4000);
//                    } else {
//                        handler.sendEmptyMessage(CODE_HIDEBACKLINE);
//                    }
//
//                    break;
//                case CODE_HIDEBACKLINE:
//                    if (layout.getVisibility() == View.VISIBLE) {
//                        layout.setVisibility(View.GONE);
//                        handler.removeMessages(CODE_SHOWBACKLINE);
//                    }
//                    break;
//            }
//        }
//    };
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        init();
//        handler.sendEmptyMessageDelayed(CODE_HIDEBACKLINE, 4000);
//    }
//
//    @Override
//    public void setRequestedOrientation(int requestedOrientation) {
////		super.setRequestedOrientation(requestedOrientation);
//        return;
//    }
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        // TODO Auto-generated method stub
//        super.onConfigurationChanged(newConfig);
//    }
//
//    /**
//     * 初始化
//     */
//    public void init() {
//        setContentView(R.layout.activity_video);
//        findViewById(R.id.test).setVisibility(View.GONE);
//
//        spinner = (Spinner) findViewById(R.id.video_spinner);
//        layout = (RelativeLayout) findViewById(R.id.rl_top_layout);
//
//        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(VideoPlayerActivity.this,
//                R.layout.monitor_spinner_style, spinnerItems);
//        spinner.setAdapter(spinnerAdapter);
//        dialog = new ProgressDialog(this);
//        dialog.setTitle("提示");
//        dialog.setMessage("加载中...");
//        dialog.show();
//        dialog.setCancelable(true);
//        dialog.setCanceledOnTouchOutside(false);
//        // 视频播放地址
//        path_url = getIntent().getStringExtra("player_url");
//        getSubRtsp(path_url);
//
//        isSub = SkySpUtil.getConfigBooleanValue(this, "isSub", true);
//
//        Log.d("播放地址：", "===" + path_url);
//
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                if (isFirst) {
//                    isFirst = false;
//                    return;
//                }
//                switch (position) {
//                    case 0:
//                        reloadMainSub(path_url_sub);
//                        Log.e("0723", "视频流子：" + path_url_sub);
//                        Toast.makeText(VideoPlayerActivity.this, "切换到子码流", Toast.LENGTH_SHORT).show();
//                        break;
//                    case 1:
//                        reloadMainSub(path_url);
//                        Log.e("0723", "视频流主：" + path_url);
//                        Toast.makeText(VideoPlayerActivity.this, "切换到主码流", Toast.LENGTH_SHORT).show();
//                        break;
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
///*
//        // 测试地址
//        // path_url =
//        // "rtsp://60.213.32.232:8554/37132900001320003586@3713290000";
//        ksytextureView = (KSYTextureView) findViewById(R.id.ksytextureView);
//        ksytextureView.setKeepScreenOn(true);
//        // 设置监听器
//        ksytextureView.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
//        ksytextureView.setOnCompletionListener(mOnCompletionListener);
//        ksytextureView.setOnPreparedListener(mOnPreparedListener);
//        ksytextureView.setOnInfoListener(mOnInfoListener);
//        // ksytextureView.setOnVideoSizeChangedListener(mOnVideoSizeChangeListener);
//        ksytextureView.setOnErrorListener(mOnErrorListener);
//        ksytextureView.setOnSeekCompleteListener(mOnSeekCompletedListener);
//        // 设置播放参数
//        // ksytextureView.setScreenOnWhilePlaying(true);
//        ksytextureView.setBufferTimeMax(2.0f);
//        // 超时设置(播放两秒后闪退加大arg0)
//        ksytextureView.setTimeout(20, 30);
//        // 填充模式(比例VIDEO_SCALING_MODE_SCALE_TO_FIT)会有黑边
//        // 裁剪模式(VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)会有裁剪
//        // 全屏模式(VIDEO_SCALING_MODE_NOSCALE_TO_FIT)画面会有拉伸
//        ksytextureView.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);
//        // 硬解264&265
//        ksytextureView.setDecodeMode(KSYMediaPlayer.KSYDecodeMode.KSY_DECODE_MODE_AUTO);
//        // ......
//        // (其它参数设置)
//        // ......
//        // 设置播放地址并准备
//        try {
//            Log.e("0723", "isSub::" + isSub);
//            if (isSub) {
//                ksytextureView.setDataSource(path_url_sub);
//
//            } else {
//                ksytextureView.setDataSource(path_url);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        ksytextureView.prepareAsync();
//        */
//
//        initPlayer();
////        reloadMainSub(path_url);
//    }
//
//    @OptIn(markerClass = UnstableApi.class) private void initPlayer(){
//        player = new ExoPlayer.Builder(this).build();
//        String path = path_url;
////        String path = "rtsp://admin:jndv12345@192.168.1.37";
////    String path = "rtsp://172.20.96.189:554/rtp/37130000012008800000_37130202121326010072";
//        MediaSource mediaSource =
//                new RtspMediaSource.Factory().setForceUseRtpTcp(true).createMediaSource(MediaItem.fromUri(path));
////                new RtspMediaSource.Factory().createMediaSource(MediaItem.fromUri(path_url));
//        ExoPlayer player = new ExoPlayer.Builder(this).build();
//        player.setMediaSource(mediaSource);
//
//        playerView = findViewById(R.id.player_view);
//        playerView.setPlayer(player);
//        player.addListener(new Player.Listener() {
//            @Override
//            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
//                Player.Listener.super.onPlayerStateChanged(playWhenReady, playbackState);
//                Log.e(TAG, "onPlayerStateChanged: ========" +playbackState);
////                Player.COMMAND_CHANGE_MEDIA_ITEMS
//                player.play();
//            }
//
//            @Override
//            public void onIsPlayingChanged(boolean isPlaying) {
//                Player.Listener.super.onIsPlayingChanged(isPlaying);
//                Log.e(TAG, "onIsPlayingChanged: ==isPlaying=="+isPlaying);
//                if (true){
//                    cancelLoadding();
//                }
//            }
//
//            @Override
//            public void onIsLoadingChanged(boolean isLoading) {
//                Player.Listener.super.onIsLoadingChanged(isLoading);
//                Log.e(TAG, "onIsLoadingChanged: ==isLoading=="+isLoading);
//            }
//
//            @Override
//            public void onPlayerError(PlaybackException error) {
//                Player.Listener.super.onPlayerError(error);
//                Log.e(TAG, "onPlayerError: ==error=="+new Gson().toJson(error));
//            }
//
//            @Override
//            public void onPlaybackStateChanged(int playbackState) {
//                Player.Listener.super.onPlaybackStateChanged(playbackState);
//                Log.e(TAG, "onPlaybackStateChanged: ==playbackState=="+playbackState);
//            }
//
//            @Override
//            public void onPlayerErrorChanged(@Nullable PlaybackException error) {
//                Player.Listener.super.onPlayerErrorChanged(error);
//                Log.e(TAG, "onPlayerErrorChanged: ==error=="+new Gson().toJson(error));
//            }
//
//            @Override
//            public void onPlayWhenReadyChanged(boolean playWhenReady, int reason) {
//                Player.Listener.super.onPlayWhenReadyChanged(playWhenReady, reason);
//                Log.e(TAG, "onPlayWhenReadyChanged: ==playWhenReady=="+playWhenReady);
//            }
//        });
//        player.prepare();
//    }
//
//    /**
//     * 获取子码流
//     */
//    public void getSubRtsp(String url) {
//        try {
//            path_url_sub = url.substring(0, url.length() - 11) + "_1" + url.substring(url.length() - 11, url.length());
//            Log.e("", "========tt获取子码流=" + path_url_sub);
//            // Toast.makeText(this,url,1).show();
//        } catch (Exception e) {
//            // TODO: handle exception
//        }
//    }
//
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_UP:
//                handler.sendEmptyMessage(CODE_SHOWBACKLINE);
//                break;
//        }
//        return true;
//    }
//
//    /**
//     * 开始播放
//     */
////    private IMediaPlayer.OnPreparedListener mOnPreparedListener = new IMediaPlayer.OnPreparedListener() {
////        @Override
////        public void onPrepared(IMediaPlayer mp) {
////            if (ksytextureView != null) {
////                // 开始播放视频
////                ksytextureView.start();
////            }
////        }
////    };
//
//    /**
//     * 重播视频源--主，子码流切换
//     */
//    public void reloadMainSub(String url) {
//        dialog.show();
////        ksytextureView.reload(url, true);
//        Log.e("", "========tt重播=" + url);
//        // Toast.makeText(this,url,1).show();
//    }
//
//    /**
//     * 播放释放
//     */
//    private void videoPlayEnd() {
//        if (dialog.isShowing()) {
//            dialog.dismiss();
//        }
////        if (ksytextureView != null) {
////            // 释放播放器
////            ksytextureView.release();
////        }
//        if (player != null) {
//            player.release();
//        }
//    }
////
////    private IMediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener = new IMediaPlayer.OnBufferingUpdateListener() {
////        @Override
////        public void onBufferingUpdate(IMediaPlayer mp, int percent) {
////            long duration = ksytextureView.getDuration();
////            long progress = duration * percent / 100;
////        }
////    };
////
////    private IMediaPlayer.OnSeekCompleteListener mOnSeekCompletedListener = new IMediaPlayer.OnSeekCompleteListener() {
////        @Override
////        public void onSeekComplete(IMediaPlayer mp) {
////            Log.e(TAG, "onSeekComplete...............");
////        }
////    };
////
////    private IMediaPlayer.OnCompletionListener mOnCompletionListener = new IMediaPlayer.OnCompletionListener() {
////        @Override
////        public void onCompletion(IMediaPlayer mp) {
////            Toast.makeText(VideoPlayerActivity.this, "播放完成！", Toast.LENGTH_LONG).show();
////            videoPlayEnd();
////        }
////    };
////
////    private IMediaPlayer.OnErrorListener mOnErrorListener = new IMediaPlayer.OnErrorListener() {
////        @Override
////        public boolean onError(IMediaPlayer mp, int what, int extra) {
////            switch (what) {
////                case 1:// 未知的播放器错误
////
////                    break;
////                case -10013:// 不支持的音频编码类型（有时完全播放不出来的，有时出现画面但是报错
////                    break;
////                default:
////                    Log.e(TAG, "OnErrorListener, Error:" + " what:" + what + ",extra:" + extra);
////            }
////            dialog.dismiss();
////            Toast.makeText(getApplicationContext(), "加载超时，请稍后再试！", Toast.LENGTH_LONG).show();
////            // Toast.makeText(getApplicationContext(),
////            // "播放错误:\n"+path_url,Toast.LENGTH_LONG).show();
////            return false;
////        }
////    };
////
////    public IMediaPlayer.OnInfoListener mOnInfoListener = new IMediaPlayer.OnInfoListener() {
////        @Override
////        public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i1) {
////            Log.d(TAG, "Buffering Start." + i + "  " + i1);
////            switch (i) {
////                case IMediaPlayer.MEDIA_INFO_RELOADED:
////                    // Log.d(TAG, "Succeed to reload video.");
////                    dialog.dismiss();
////                    break;
////                case 3:
////                    dialog.dismiss();
////                    Log.d(TAG, "Buffering Start.");
////                    break;
////                // case KSYMediaPlayer.MEDIA_INFO_BUFFERING_START:
////                // Log.d(TAG, "Buffering Start.");
////                // break;
////                // case KSYMediaPlayer.MEDIA_INFO_BUFFERING_END:
////                // Log.d(TAG, "Buffering End.");
////                // break;
////                // case KSYMediaPlayer.MEDIA_INFO_AUDIO_RENDERING_START:
////                // Toast.makeText(VideoPlayerActivity2.this, "Audio Rendering
////                // Start", Toast.LENGTH_SHORT).show();
////                // break;
////                // case KSYMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
////                // Toast.makeText(VideoPlayerActivity2.this, "Video Rendering
////                // Start", Toast.LENGTH_SHORT).show();
////                // break;
////                // case KSYMediaPlayer.MEDIA_INFO_RELOADED:
////                // Toast.makeText(VideoPlayerActivity2.this, "Succeed to reload
////                // video.", Toast.LENGTH_SHORT).show();
////                // Log.d(TAG, "Succeed to mPlayerReload video.");
////                // return false;
////            }
////            return false;
////        }
////    };
//
////    private IMediaPlayer.OnMessageListener mOnMessageListener = new IMediaPlayer.OnMessageListener() {
////        @Override
////        public void onMessage(IMediaPlayer iMediaPlayer, String name, String info, double number) {
////            Log.e(TAG, "name:" + name + ",info:" + info + ",number:" + number);
////        }
////    };
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        videoPlayEnd();
//        Log.i("", "=====onDestroy");
//    }
//
//    @Override
//    protected void onStop() {
//        // TODO Auto-generated method stub
//        super.onStop();
//        videoPlayEnd();
//        Log.i("", "=====onStop");
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        // TODO Auto-generated method stub
//        if (KeyEvent.KEYCODE_BACK == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
//            videoPlayEnd();
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    /**
//     * 探测视频信息
//     */
//    /*
//    public void getVideoInfo(String url) {
//        TextView test = (TextView) findViewById(R.id.test);
//        KSYProbeMediaInfo probeMediaInfo = new KSYProbeMediaInfo();
//        int probeTimeOutSecond = 10; // 超时阈值，单位为秒
//        // http请求的头
//        Map<String, String> headers = new HashMap<>();
//        // headers.put("Referer", "http://www.test.com");
//        // 探测视频信息
//        probeMediaInfo.probeMediaInfo(url, probeTimeOutSecond, headers, true);
//
//        Log.i(TAG, "Media format:" + probeMediaInfo.getMediaFormat().toString() + ", bitrate:"
//                + probeMediaInfo.getMediaBitrate() + ", video stream count:" + probeMediaInfo.getVideoStreamCount()
//                + ", audio stream count:" + probeMediaInfo.getAudioStreamCount());
//        test.append("Media format:" + probeMediaInfo.getMediaFormat().toString() + ", bitrate:"
//                + probeMediaInfo.getMediaBitrate() + ", video stream count:" + probeMediaInfo.getVideoStreamCount()
//                + ", audio stream count:" + probeMediaInfo.getAudioStreamCount());
//        ArrayList<KSYProbeMediaInfo.KSYProbeMediaData> videoArray = probeMediaInfo.getVideoStreams();
//        for (KSYProbeMediaInfo.KSYProbeMediaData video : videoArray) {
//            Log.i(TAG, "Video codec type:" + video.getVideoCodecType().toString() + ",width:" + video.getVideoWidth()
//                    + ", height:" + video.getVideoHeight());
//            test.append("\nVideo codec type:" + video.getVideoCodecType().toString() + ",width:" + video.getVideoWidth()
//                    + ", height:" + video.getVideoHeight());
//        }
//
//        ArrayList<KSYProbeMediaInfo.KSYProbeMediaData> audioArray = probeMediaInfo.getAudioStreams();
//        for (KSYProbeMediaInfo.KSYProbeMediaData audio : audioArray) {
//            Log.i(TAG, "Audio codec type:" + audio.getAudioCodecType().toString() + ", channel:"
//                    + audio.getAudioChannel() + ", sample rate:" + audio.getAudioSampleRate());
//            test.append("\nAudio codec type:" + audio.getAudioCodecType().toString() + ", channel:"
//                    + audio.getAudioChannel() + ", sample rate:" + audio.getAudioSampleRate());
//            Log.i(TAG, "Audio bitrate:" + audio.getAudioBitrate() + ", frame size:" + audio.getAudioFrameSize()
//                    + ", fmt:" + audio.getAudioFormat());
//            test.append("\nAudio bitrate:" + audio.getAudioBitrate() + ", frame size:" + audio.getAudioFrameSize()
//                    + ", fmt:" + audio.getAudioFormat());
//        }
//    }
//    */
//}

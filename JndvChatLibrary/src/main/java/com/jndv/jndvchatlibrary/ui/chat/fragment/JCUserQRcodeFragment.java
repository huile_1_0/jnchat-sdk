package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.zxing.client.android.utils.ZXingUtils;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.databinding.FragmentJcuserqrcodeBinding;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCUserQRcodeBean;
import com.jndv.jndvchatlibrary.utils.JCQRcodeBeanBase;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;


/**
 * Author: wangguodong
 * Date: 2022/5/4
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 用户二维码展示页面
 */
public class JCUserQRcodeFragment extends JCbaseFragment {
    private FragmentJcuserqrcodeBinding binding;

    public static final String CODE_INTYPE = "CODE_INTYPE" ;
    public static final String CODE_INNAME = "CODE_INNAME" ;
    public static final String CODE_INHEAD = "CODE_INHEAD" ;
    public static final String CODE_INSIPID = "CODE_INSIPID" ;
    public static final String CODE_INSIPADDRESS = "CODE_INSIPADDRESS" ;
    public static final String CODE_INJAVAADDRESS = "CODE_INJAVAADDRESS" ;
    private int type = 0 ;//0=我的；1=其他用户的
    private String name;
    private String head;
    private String sipId;
    private String sipAddress;
    private String javaAddress;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentJcuserqrcodeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);
        return root;
    }

    private void init(View view) {
        Bundle bundle = getArguments();
        if (bundle != null){
            type = bundle.getInt(CODE_INTYPE, 0);
            name = bundle.getString(CODE_INNAME);
            head = bundle.getString(CODE_INHEAD);
            sipId = bundle.getString(CODE_INSIPID);
            sipAddress = bundle.getString(CODE_INSIPADDRESS);
            javaAddress = bundle.getString(CODE_INJAVAADDRESS);
        }
        if (0==type){
            name = JNBasePreferenceSaves.getUserName();
            head = JNBasePreferenceSaves.getUserHead();
            sipId = JNBasePreferenceSaves.getUserSipId();
            sipAddress = JNBasePreferenceSaves.getSipAddress();
            javaAddress = JNBasePreferenceSaves.getJavaAddress();
        }
        JCglideUtils.loadCircleImage(getActivity(), head, binding.ivUserHead);
        binding.tvUserName.setText(name);
        JCUserQRcodeBean bean = new JCUserQRcodeBean(name,head,sipId,sipAddress,javaAddress);
        JCQRcodeBeanBase jcqRcodeBeanBase = new JCQRcodeBeanBase(JCQRcodeBeanBase.QRCODE_TYPE_USER,new Gson().toJson(bean));
        Bitmap qrBitmap= ZXingUtils.createQRCodeImage(new Gson().toJson(jcqRcodeBeanBase));
        JCglideUtils.loadImage(getActivity(), qrBitmap, binding.ivQrcode);
    }

}

package com.jndv.jndvchatlibrary.ui.crowd.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCChatEntity;
import com.jndv.jndvchatlibrary.ui.crowd.utils.LogUtil;
import com.zhy.autolayout.AutoLayoutActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;

/**
 * 基类
 */
public abstract class JCBaseActivity extends AutoLayoutActivity {

    protected Handler mHandler;
    protected LocalReceiver localReceiver;
    private boolean isNeedBroadcast = true;
    private static final String TAG = "BaseActivity";
    protected Context mContext;
    private boolean isNeedHandler = true;
    private boolean isNeedAvatar = true;
//    private BitmapManager.BitmapChangedListener mAvatarChangedListener = this::receiveNewAvatar;


    @Override
    protected void onResume() {
        super.onResume();
        // TODO: 2022/2/18 保活操作 
//        LogUtil.e("生命周期", "BaseActivity onResume" + getClass().getName());
//        if (!IsServiceRunning.isServiceRunning(this, "JNIService")) { // 检测JNIService运行状态
//            LogUtil.e("如果JNIService没有在运行，则开启服务");
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                try {
//                    startForegroundService(new Intent(getApplicationContext(), JNIService.class));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                // startForegroundService(new Intent(getApplicationContext(), ConferenceService.class));
//            } else {
//
//                try {
//                    startService(new Intent(getApplicationContext(), JNIService.class));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                // startService(new Intent(getApplicationContext(), ConferenceService.class));
//            }
//
////            startService(new Intent(this, ConferenceService.class));
//        }
//
//        if ((!getClass().getName().equals(loginActivityPN)) && (!getClass().getName().equals(guideActivityPN)))
//            if (!isActive) {
//                // app 从后台唤醒，进入前台
//                isActive = true;
//                Log.e("生命周期", "onResume: 从后台唤醒进入前台");
//                UpOperationLogUtil.getInstance().upLog(mContext, "从后台唤醒进入前台", "", "1");
//                ;
//            }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        LogUtil.e("生命周期", "BaseActivity onRestart" + getClass().getName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.e("生命周期", "BaseActivity onCreate" + getClass().getName());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mContext = this;
//        imReLogin();
        EventBus.getDefault().register(this);

        if (isNeedHandler) {
            mHandler = new LocalHandler(this);
        }

//        if (isNeedAvatar) {
//            BitmapManager.getInstance().registerBitmapChangedListener(mAvatarChangedListener);
//        }

        // registerBroadcast();
//        JndvApplication.getInstance().addActivity(this);
//        exitApp();

        isActive = isAppOnForeground();
        Log.e("生命周期", "onCreate: " + isActive);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void codeEvent(JNCodeEvent codeEvent) {
        switch (codeEvent.getCode()) {
            case JNEventBusType
                    .CODE_SIP_LOGIN_OUT://异地登录
                finish();
                break;
        }
    }


//    private String loginActivityPN = LoginActivity.class.getName();
//    private String guideActivityPN = GuideActivity.class.getName();

    //判断是否正常登录,如果未正常登录直接退出（解决native崩溃CrashHandle无法捕捉到，导致的不走登录逻辑直接通过前一个activity重启的问题（5.0以上：当前堆栈中存在两个 Activity：Act1 -> Act2，如果 Act2 发生了 Crash ，那么系统会重启 Act1））
//    private void exitApp() {
//        Log.e(TAG, "exitApp: " + JndvApplication.isLogin);
//        Log.e(TAG, "exitApp: " + loginActivityPN);
//        Log.e(TAG, "exitApp: " + getClass().getName());
//        if ((!JndvApplication.isLogin) && (!getClass().getName().equals(loginActivityPN)) && (!getClass().getName().equals(guideActivityPN))) {
//            Log.e(TAG, "exitApp: 退出");
//            ImRequest.getInstance().ImLogout();
//            SpUtil.setInt(JndvApplication.getInstance().getBaseContext(), ChatEntity.USER_LOGIN_STATUS, 0);
//
//            JndvApplication.getInstance().finishExit();
//            JndvApplication.getInstance().exitAllActivity();
//            //退出程序
//            android.os.Process.killProcess(android.os.Process.myPid());
//            System.exit(0);
//        }
//    }
    // TODO: 2022/2/18 im重新连接 
//    private void imReLogin() {
//        Log.e(TAG, "imReLogin: " + JndvApplication.isLogin);
//        Log.e(TAG, "imReLogin: " + loginActivityPN);
//        Log.e(TAG, "imReLogin: " + getClass().getName());
//        if ((!JndvApplication.isLogin) && (!getClass().getName().equals(loginActivityPN)) && (!getClass().getName().equals(guideActivityPN))) {
//            Toast.makeText(mContext, "IM重新登录！", Toast.LENGTH_SHORT).show();
//            Log.e(TAG, "imReLogin: IM重新登录");
//            ImRequest.getInstance().ImLogout();
//            SpUtil.setInt(JndvApplication.getInstance().getBaseContext(), ChatEntity.USER_LOGIN_STATUS, 0);
//
//            String im_ip = JSharedPreferences.getConfigStrValue(mContext, GlobalConstant.KEY_IM_IP);
//            String im_port = JSharedPreferences.getConfigStrValue(mContext, GlobalConstant.KEY_IM_PORT);
//
//            String username = SpUtil.getString(mContext, ChatEntity.CURRENT_USER_NAME);
//            String password = SpUtil.getString(mContext, ChatEntity.CURRENT_PASSWORD);
//
//            String push_id = SpUtil.getString(mContext, "getPushid");
//            if (TextUtils.isEmpty(push_id)) {
//                push_id = UUID.randomUUID().toString();
//            }
//            if (push_id.length() == 36) { // 移动4g 碰上 12位账号 碰上 pushid为36位(uuid 36位)大概率登不上,协议丢数据 // 暂时规避方案
//                push_id += push_id;
//            }
//
//            ConfigRequest.getInstance().setServerAddress(im_ip, Integer.parseInt(im_port));
//            mUserService.login(username, password, null, push_id);
//
//            GlobalHolder.getInstance().clearAll();
//            GlobalConstant.sAccountName = username;
//            GlobalConstant.IM_IP = im_ip;
//            GlobalConstant.IM_PORT = im_port;
//        }
//
//    }

    @Override
    protected void onPause() {
        super.onPause();
        LogUtil.e("生命周期", "BaseActivity onPause" + getClass().getName());
        hideSoftKeyboard();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("生命周期", "BaseActivity onStop" + getClass().getName());
//        if ((!getClass().getName().equals(loginActivityPN)) && (!getClass().getName().equals(guideActivityPN)))
//            if (!isAppOnForeground()) {
//                isActive = false; // 全局变量 记录当前已经进入后台 app 进入后台
//                Log.e("生命周期", "app 进入后台运行");
////                UpOperationLogUtil.getInstance().upLog(mContext, "app进入后台运行", "", "1");
//            }
    }



    protected View getId(int id) {
        return findViewById(id);
    }

    protected void back() {
        finish();
    }

//    // FIXME: 2019/10/30 感觉不应该出现在公共方法里,这个方法所有Activity都可以使用吗???可以在用到的某一类上面再封装一层基类
//
//    protected void toMessageSend(WordModel bean, int type) {
//        if (!GlobalHolder.getInstance().isServerConnected()) {
//            showToast(getString(R.string.error_connect_to_server));
//            return;
//        }
//        Intent intent = new Intent(this, MessageSendActivity.class);
//        //intent.setAction(PublicIntent.START_CONVERSACTION_ACTIVITY);
//        intent.putExtra("type", type);
//
//        intent.putExtra("uid", bean.getId());
//        intent.putExtra("uName", bean.getWord());
//        startActivity(intent);
//
//    }
//    // FIXME: 2019/10/30 感觉不应该出现在公共方法里,这个方法所有Activity都可以使用吗???可以在用到的某一类上面再封装一层基类
//
//    protected void toMessageSend(int type, long userId) {
//        if (!GlobalHolder.getInstance().isServerConnected()) {
//            showToast(getString(R.string.error_connect_to_server));
//            return;
//        }
//        Intent intent = new Intent(this, MessageSendActivity.class);
//        //intent.setAction(PublicIntent.START_CONVERSACTION_ACTIVITY);
//        intent.putExtra("type", type);
//        intent.putExtra("uid", userId);
////        intent.putExtra("uName", type == ChatEntity.GROUP_TYPE_CROWD ? CommonUtil.getCrowdNameById(userId) : CommonUtil.getUserNameById(userId));
//        intent.putExtra("uName", type == GroupType.GROUP_TYPE_CROWD ? CommonUtil.getCrowdNameById(userId) :
//                (GlobalHolder.getInstance().getUser(userId).getNickName() == null ?
//                        GlobalHolder.getInstance().getUser(userId).getAccount() : GlobalHolder.getInstance().getUser(userId).getNickName()));
//        startActivity(intent);
//    }
//    // FIXME: 2019/10/30 感觉不应该出现在公共方法里,这个方法所有Activity都可以使用吗???可以在用到的某一类上面再封装一层基类
//
//    protected void toPersonInformation(long userId) {
//        if (!GlobalHolder.getInstance().isServerConnected()) {
//            showToast(getString(R.string.error_connect_to_server));
//            return;
//        }
//        Intent intent = new Intent(this, InformationDetailActivity.class);
//        intent.putExtra("userId", userId);
//        startActivity(intent);
//    }

    @Override
    protected void onDestroy() {
        try {
            EventBus.getDefault().unregister(this);
        }catch (Exception e){
            JNLogUtil.e("==JCBaseActivity==onDestroy==",e);
        }
        if (isNeedBroadcast && localReceiver != null) {
            try {
                unregisterReceiver(localReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (isNeedHandler)
            mHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
        LogUtil.e("生命周期", "BaseActivity onDestroy" + getClass().getName());
        // unregisterReceiver(receiver);
//        JndvApplication.isState = "";
//        JndvApplication.getInstance().removeActivity(this);
    }


    public abstract void receiveBroadcast(Intent intent);

//    public abstract void receiveNewAvatar(User targetUser, Bitmap bnewAvatarm);

    public abstract void receiveMessage(Message msg);

//    /**
//     * post请求接口
//     *
//     * @param path   路径
//     * @param params 参数
//     * @param what   handler发送消息接收的what
//     */
//    public void postHttp(String path, Map<String, String> params, int what) {
//
//        new Thread(() -> {
//            URL url;
//            try {
//                url = new URL(GlobalConstant.getDomain(mContext) + path);
////                url = new URL("http://192.168.1.69:62615" + path);
//
//                LogUtil.w("postHttp url:" + url.toString());
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setReadTimeout(5000);
//                conn.setRequestMethod("POST"); //post请求  注意要大写
//                conn.setConnectTimeout(5000);
//                //conn.setRequestProperty("", newValue)
//                conn.setDoOutput(true);
//                conn.getOutputStream().write(CommonUtil.getMapToString(params).getBytes());
//
//                int code = conn.getResponseCode();
//                if (code == 200) {
//                    InputStream is = conn.getInputStream();
//                    //StreamTools.ReadStream(is);
//                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
//                    int len = 0;
//                    byte[] buffer = new byte[1024];
//                    while ((len = is.read(buffer)) != -1) {
//
//                        bao.write(buffer, 0, len);
//                    }
//                    is.close();
//                    Message mes = new Message();
//                    mes.what = what;
//                    mes.obj = bao.toString();
//                    mHandler.sendMessage(mes);
//                    bao.close();
//
//                } else {
//                    LogUtil.i("结果码", String.valueOf(code));
//                    Message mes = new Message();
//                    mes.what = ChatEntity.RESULT_ERROR;
//                    mHandler.sendMessage(mes);
//
//                }
//            } catch (Exception e) {
//                LogUtil.i("post请求异常", e.toString());
////                runOnUiThread(() -> showToast("获取服务器数据失败，请重试！"));
//                Message mes = new Message();
//                mes.what = ChatEntity.RESULT_ERROR;
//                mHandler.sendMessage(mes);
//
//            }
//        }).start();
//    }

    protected void showToast(int resId) {
        showToast(getString(resId));
    }

    protected void showToast(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    public static String JsonPost(final String path, final JSONObject json) {

        BufferedReader in = null;
        String result = "";
        OutputStream os = null;
        try {
            URL url = new URL(path);
// 然后我们使用httpPost的方式把lientKey封装成Json数据的形式传递给服务器
// 在这里呢我们要封装的时这样的数据
// 我们把JSON数据转换成String类型使用输出流向服务器写
            String content = String.valueOf(json);
// 现在呢我们已经封装好了数据,接着呢我们要把封装好的数据传递过去
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
// 设置允许输出
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
// 设置User-Agent: Fiddler
            conn.setRequestProperty("ser-Agent", "Fiddler");
// 设置contentType
            conn.setRequestProperty("Content-Type", "application/json");
            os = conn.getOutputStream();
            os.write(content.getBytes());
            os.flush();
// 定义BufferedReader输入流来读取URL的响应
// Log.i("-----send", "end");

            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            if (conn.getResponseCode() == 200) {
                while ((line = in.readLine()) != null) {
                    result += line;
                }
            }
        } catch (SocketTimeoutException e) {
// Log.i("错误", "连接时间超时");
            e.printStackTrace();
            return "错误";
        } catch (MalformedURLException e) {
// Log.i("错误", "jdkfa");
            e.printStackTrace();
            return "错误";
        } catch (ProtocolException e) {
// Log.i("错误", "jdkfa");
            e.printStackTrace();
            return "错误";
        } catch (IOException e) {
// Log.i("错误", "jdkfa");
            e.printStackTrace();
            return "错误";
        }// 使用finally块来关闭输出流、输入流
        finally {
            try {
                if (os != null) {
                    os.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public void sendJsonPost(String path, String Json, int what) {
        Log.e(TAG, "sendJsonPost: " + Json);
        new Thread(new Runnable() {
            @Override
            public void run() {
                // HttpClient 6.0被抛弃了
                String result = "";
                BufferedReader reader = null;
                try {
                    String urlPath = path;
                    URL url = new URL(urlPath);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);
                    conn.setUseCaches(false);
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Charset", "UTF-8");
                    //设置超时时间
                    conn.setConnectTimeout(5000);
                    conn.setReadTimeout(5000);
                    // 设置文件类型:
                    conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    // 设置接收类型否则返回415错误
                    //conn.setRequestProperty("accept","*/*")此处为暴力方法设置接受所有类型，以此来防范返回415;
                    conn.setRequestProperty("accept", "application/json");
                    // 往服务器里面发送数据
                    if (Json != null && !TextUtils.isEmpty(Json)) {
                        byte[] writebytes = Json.getBytes();
                        Log.d("hlhupload", Json);
                        // 设置文件长度
                        conn.setRequestProperty("Content-Length", String.valueOf(writebytes.length));
                        OutputStream outwritestream = conn.getOutputStream();
                        outwritestream.write(Json.getBytes());
                        outwritestream.flush();
                        outwritestream.close();
                        Log.d("hlhupload", "doJsonPost: conn" + conn.getResponseCode());
                    }
                    if (conn.getResponseCode() == 200) {
                        reader = new BufferedReader(
                                new InputStreamReader(conn.getInputStream()));
                        result = reader.readLine();
                        Log.d(TAG, "hlhupload" + result);
                        Message mes = new Message();
                        mes.what = what;
                        mes.obj = result.toString();
                        mHandler.sendMessage(mes);
                    } else {
                        if (what == JCChatEntity.CHATRECORD) {
                            Message mes = new Message();
                            mes.what = what;
                            mes.obj = "服务器错误";
                            mHandler.sendMessage(mes);
                            Log.e(TAG, "run: 服务器错误");
                        }
                    }
                    Log.e(TAG, "run: 111");
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "run: 222");
                    if (what == JCChatEntity.CHATRECORD) {
                        Message mes = new Message();
                        mes.what = what;
                        mes.obj = "服务器错误";
                        mHandler.sendMessage(mes);
                        Log.e(TAG, "run: 服务器错误");
                    }
                } finally {
                    Log.e(TAG, "run: 333");
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();

//        return result;
    }

    /*
     * get请求接口
     * */
//    public void getHttp(String path, int what) {
//        LogUtil.i(TAG, "getHttp::" + path);
//
//        new Thread(() -> {
//            URL url;
////            try{
//            try {
//                url = new URL(path);
//
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setReadTimeout(5000);
//                conn.setRequestMethod("GET");//post请求  注意要大写
//                conn.setConnectTimeout(5000);
//                //conn.setRequestProperty("", newValue)
//                conn.setDoOutput(true);
//
//                // LogUtil.e(CommonUtil.getMapToString(params));
//                int code = conn.getResponseCode();
//                if (code == 200) {
//                    InputStream is = conn.getInputStream();
//                    //StreamTools.ReadStream(is);
//                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
//                    int len;
//                    byte[] buffer = new byte[1024];
//                    while ((len = is.read(buffer)) != -1) {
//
//                        bao.write(buffer, 0, len);
//                    }
//                    is.close();
//                    Message mes = new Message();
//                    mes.what = what;
//                    mes.obj = bao.toString();
//                    Log.e(TAG, "getHttp: " + what + "  " + mes.obj);
//                    mHandler.sendMessage(mes);
//                    bao.close();
//
//                } else {
//                    //  LogUtil.i("结果码", String.valueOf(code));
//                    //runOnUiThread(() -> showToast("结果码:" + String.valueOf(code)));
//                    Message mes = new Message();
//                    mes.what = ChatEntity.RESULT_ERROR;
//                    mes.obj = "结果码:" + code;
//                    mHandler.sendMessage(mes);
//
//                }
//
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//                Message mes = new Message();
//                mes.what = ChatEntity.RESULT_ERROR;
//                mes.obj = getString(R.string.error_connect_to_server) + "MalformedURLException";
//                mHandler.sendMessage(mes);
//            } catch (ProtocolException e) {
//                e.printStackTrace();
//                Message mes = new Message();
//                mes.what = ChatEntity.RESULT_ERROR;
//                mes.obj = getString(R.string.error_connect_to_server) + "ProtocolException";
//                mHandler.sendMessage(mes);
//            } catch (IOException e) {
//                e.printStackTrace();
//                LogUtil.e("1116start" + path);
//                Message mes = new Message();
//                mes.what = ChatEntity.RESULT_ERROR;
//                mes.obj = getString(R.string.error_connect_to_server) + "IOException";
//                mHandler.sendMessage(mes);
//            }
//           /* }
//            catch (Exception e) {
//               // LogUtil.i("post请求异常", e.toString());
//                //runOnUiThread(() -> showToast("post请求异常:" + e.getMessage()));
//                Message mes = new Message();
//                mes.what = ChatEntity.RESULT_ERROR;
//                mes.obj ="post请求异常" ;
//                handler.sendMessage(mes);
//
//            }*/
//        }).start();
//    }

    class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            receiveBroadcast(intent);
        }
    }


    private static class LocalHandler extends Handler {
        private final WeakReference<JCBaseActivity> mActivity;

        private LocalHandler(JCBaseActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            if (mActivity.get() == null) {
                return;
            }

            mActivity.get().receiveMessage(msg);
        }
    }

    //创建会议
//    protected void createMeeting() {
//        if (!GlobalHolder.getInstance().isServerConnected()) {
//            showToast(getString(R.string.error_connect_to_server));
//            return;
//        }
//        if (!CommonUtil.isFastClick()) {
//            startActivity(new Intent(this, ConferenceCreateActivity.class));
//        }
//    }


    //创建群组
    protected void createCrowd() {
        // TODO: 2022/2/18 创建群组 
//        if (!GlobalHolder.getInstance().isServerConnected()) {
//            showToast(getString(R.string.error_connect_to_server));
//            return;
//        }
//        if (!CommonUtil.isFastClick()) {
////            Toast.makeText(this, "创建群", Toast.LENGTH_SHORT).show();
//            Intent createCrowdIntent = new Intent(this, CrowdCreateActivity.class);
//            createCrowdIntent.putExtra("mode", false);
//            startActivity(createCrowdIntent);
//        }
    }

    /**
     * 隐藏软键盘(只适用于Activity，不适用于Fragment)
     */
    protected void hideSoftKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private boolean isActive;

    /**
     * 程序是否在前台运行
     *
     * @return
     */
    public boolean isAppOnForeground() {
        // Returns a list of application processes that are running on the device
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = getApplicationContext().getPackageName();

        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            // The name of the process that this object is associated with.
            if (appProcess.processName.equals(packageName) && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;
    }
}

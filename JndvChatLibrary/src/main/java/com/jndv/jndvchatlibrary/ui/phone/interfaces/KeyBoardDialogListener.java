package com.jndv.jndvchatlibrary.ui.phone.interfaces;

import android.view.MotionEvent;
import android.view.View;

import com.jndv.jndvchatlibrary.ui.phone.view.DigitsEditText;

public interface KeyBoardDialogListener {
    void itemClick(View view, String number);
    void itemLongClick(View view);
    void itemTouch(View view, MotionEvent motionEvent);
    void getDigitsView( DigitsEditText digits);
}
package com.jndv.jndvchatlibrary.ui.crowd.utils;

import java.security.MessageDigest;

/**
 * Author: wangguodong
 * Date: 2022/7/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: MD5加密工具类
 */
public class MD5Util {

    /**
     * 字符串加密
     *
     * @param raw 字符串
     * @return 加密后的字符串
     */
    public static String encrypt(String raw) {
        String encryptString = raw;
        try {
            // 创建一个MD5算法对象
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 给算法对象加载待加密的原始数据
            md.update(raw.getBytes());
            // 调用digest方法完成哈希计算
            byte[] digests = md.digest();
            int md5Str;
            StringBuilder buf = new StringBuilder();
            for (byte digest : digests) {
                md5Str = digest;
                if (md5Str < 0) {
                    md5Str += 256;
                }
                if (md5Str < 16) {
                    buf.append("0");
                }
                buf.append(Integer.toHexString(md5Str)); // 把字节数组逐位转换为十六进制数
            }
            encryptString = buf.toString().toLowerCase(); // 拼装加密字符串
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null==encryptString?"":encryptString; // 输出大写的加密串
    }

    /**
     * 字符串加密
     *
     * @param raw 字符串
     * @return 加密后的字符串
     */
    public static String encryptUpper(String raw) {
        String encryptString = raw;
        try {
            // 创建一个MD5算法对象
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 给算法对象加载待加密的原始数据
            md.update(raw.getBytes());
            // 调用digest方法完成哈希计算
            byte[] digests = md.digest();
            int md5Str;
            StringBuilder buf = new StringBuilder();
            for (byte digest : digests) {
                md5Str = digest;
                if (md5Str < 0) {
                    md5Str += 256;
                }
                if (md5Str < 16) {
                    buf.append("0");
                }
                buf.append(Integer.toHexString(md5Str)); // 把字节数组逐位转换为十六进制数
            }
            encryptString = buf.toString(); // 拼装加密字符串
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptString.toUpperCase(); // 输出大写的加密串
    }

    public static String MD5(String str) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

        char[] charArray = str.toCharArray();
        byte[] byteArray = new byte[charArray.length];

        for (int i = 0; i < charArray.length; i++) {
            byteArray[i] = (byte) charArray[i];
        }
        byte[] md5Bytes = md5.digest(byteArray);

        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16) {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString().toLowerCase();
    }


    /**
     * 字符串加密
     *
     * @param raw 字符串
     * @return 加密后的字符串
     */
    public static String encryptLower(String raw) {
        String encryptString = raw;
        try {
            // 创建一个MD5算法对象
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 给算法对象加载待加密的原始数据
            md.update(raw.getBytes());
            // 调用digest方法完成哈希计算
            byte[] digests = md.digest();
            int md5Str;
            StringBuilder buf = new StringBuilder();
            for (byte digest : digests) {
                md5Str = digest;
                if (md5Str < 0) {
                    md5Str += 256;
                }
                if (md5Str < 16) {
                    buf.append("0");
                }
                buf.append(Integer.toHexString(md5Str)); // 把字节数组逐位转换为十六进制数
            }
            encryptString = buf.toString().toLowerCase(); // 拼装加密字符串
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null==encryptString?"":encryptString; // 输出大写的加密串
    }


}


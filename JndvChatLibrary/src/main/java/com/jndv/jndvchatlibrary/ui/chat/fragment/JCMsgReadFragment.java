package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestBuilder;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.ui.base.JNBaseFragment;
import com.jndv.jnbaseutils.utils.TimeUtils;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcmsgreadBinding;
import com.jndv.jnbaseutils.utils.JNLogUtil;
//import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCReadUserBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgImgContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgTextContentBean;
import com.jndv.jnbaseutils.ui.JNSimpleDividerDecoration;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/23
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 群消息的已读未读人员列表
 */
public class JCMsgReadFragment extends JNBaseFragment {
//public class JCMsgReadFragment extends JCbaseFragment {
    private JCMsgReadViewModel jcMsgReadViewModel;
    private FragmentJcmsgreadBinding binding;

    private int showPos = 0 ;

    private RecyclerView recyclerView ;
    private CommonAdapter adapter ;
    private List<JCReadUserBean> showDatas = new ArrayList<>();
    private List<JCReadUserBean> readDatas = new ArrayList<>();
    private List<JCReadUserBean> unreadDatas = new ArrayList<>();
    public static final String MESSAGEITEM = "messageitem";
    private JCimMessageBean jCimMessageBean ;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        jcMsgReadViewModel =
                new ViewModelProvider(this).get(JCMsgReadViewModel.class);
        binding = FragmentJcmsgreadBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
//        jcMsgReadViewModel.init(, this);
        init();
//        EventBus.getDefault().register(this);
        return root;
    }
    private void init(){
        Bundle bundle = getArguments();
        if (bundle != null){
            jCimMessageBean = (JCimMessageBean) bundle.getSerializable(MESSAGEITEM);
//            jCimMessageBean.setContent("消息内容content字符串");
//            jCimMessageBean.setFromName("发送人的名称");
//            jCimMessageBean.setFromHead("https://pics1.baidu.com/feed/32fa828ba61ea8d30677d54da256de47241f588c.jpeg?token=3316adbe73fdc48c0c367fb9b4082d1d");
            jcMsgReadViewModel.init(jCimMessageBean.getMsgID(), JCMsgReadFragment.this);
        }

        if (null!=jCimMessageBean){
            binding.tvUsername.setText(jCimMessageBean.getFromName());
            binding.tvTime.setText(TimeUtils.getInstance().conversionTime(jCimMessageBean.getSendTime()));
            if (TextUtils.equals(jCimMessageBean.getMsgType(), ""+JCmessageType.TEXT)){
                JCMsgTextContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP)), JCMsgTextContentBean.class);
                binding.tvContent.setText(msgEntity.getText());
                binding.ivImage.setVisibility(View.GONE);
                binding.tvContent.setVisibility(View.VISIBLE);
            }else if (TextUtils.equals(jCimMessageBean.getMsgType(), ""+JCmessageType.IMG)){
                try {
                    String decodeBase64 = new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP));
                    JNLogUtil.e("==decodeBase64="+decodeBase64);
                    JCMsgImgContentBean bean = new Gson().fromJson(decodeBase64, JCMsgImgContentBean.class);

                    if (null!=bean.getPath() || !TextUtils.isEmpty(bean.getPath())){
                        Log.e("netdata", "bindContentView: ========003======");
                        try {
                            RequestBuilder<Drawable> requestBuilder = JCglideUtils.loadImageBuilder(getActivity(), bean.getUrl());
                            JCglideUtils.loadImage(getActivity(), bean.getPath(), binding.ivImage, requestBuilder);
                        }catch (Exception e){
                            e.printStackTrace();
                            if (!TextUtils.isEmpty(bean.getUrl())){
                                JCglideUtils.loadImage(getActivity(), bean.getUrl(), binding.ivImage);
                            }
                        }
                    }else {
                        Log.e("netdata", "bindContentView: ========004======");
                        if (!TextUtils.isEmpty(bean.getUrl())){
                            JCglideUtils.loadImage(getActivity(), bean.getUrl(), binding.ivImage);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                binding.tvContent.setVisibility(View.GONE);
                binding.ivImage.setVisibility(View.VISIBLE);
            }else if (TextUtils.equals(jCimMessageBean.getMsgType(), ""+JCmessageType.AUDIO_VIDEO_RECORD)){
                JCMsgTextContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP)), JCMsgTextContentBean.class);
                binding.tvContent.setText("音视频通话记录");
                binding.ivImage.setVisibility(View.GONE);
                binding.tvContent.setVisibility(View.VISIBLE);
            }else if (TextUtils.equals(jCimMessageBean.getMsgType(), ""+JCmessageType.LOCATION)){
                JCMsgTextContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP)), JCMsgTextContentBean.class);
                binding.tvContent.setText(msgEntity.getText());
                binding.ivImage.setVisibility(View.GONE);
                binding.tvContent.setVisibility(View.VISIBLE);
            }
        }

        recyclerView = binding.recycleView;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new JNSimpleDividerDecoration(getActivity()));
        adapter = new CommonAdapter<JCReadUserBean>(getActivity(), R.layout.jc_item_read_user, showDatas) {
            @Override
            protected void convert(ViewHolder holder, JCReadUserBean jcReadUserBean, int position) {
                JCglideUtils.loadCircleImage(getActivity(), jcReadUserBean.getAvatar(), holder.getView(R.id.iv_user_head));
                ((TextView)holder.getView(R.id.tv_user_name)).setText(jcReadUserBean.getNickname());
            }
        };
        recyclerView.setAdapter(adapter);

        binding.tvTitleRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1==showPos){
                    showPos = 0;
                    showDatas.clear();
                    showDatas.addAll(readDatas);
                    adapter.notifyDataSetChanged();
                    binding.tvTitleRead.setTextColor(getResources().getColor(R.color.button_text_color_blue));
                    binding.tvTitleUnread.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });
        binding.tvTitleUnread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (0==showPos){
                    showPos = 1;
                    showDatas.clear();
                    showDatas.addAll(unreadDatas);
                    adapter.notifyDataSetChanged();
                    binding.tvTitleRead.setTextColor(getResources().getColor(R.color.black));
                    binding.tvTitleUnread.setTextColor(getResources().getColor(R.color.button_text_color_blue));
                }
            }
        });
        jcMsgReadViewModel.getReadDataChange().observe(getViewLifecycleOwner(), new Observer<List<JCReadUserBean>>() {
            @Override
            public void onChanged(List<JCReadUserBean> jcReadUserBeans) {
                readDatas = jcReadUserBeans;
                binding.tvTitleRead.setText(jcReadUserBeans.size()+"人已读");
                if (0==showPos){
                    showDatas.clear();
                    showDatas.addAll(readDatas);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        jcMsgReadViewModel.getUnReadDataChange().observe(getViewLifecycleOwner(), new Observer<List<JCReadUserBean>>() {
            @Override
            public void onChanged(List<JCReadUserBean> jcReadUserBeans) {
                unreadDatas = jcReadUserBeans;
                binding.tvTitleUnread.setText(jcReadUserBeans.size()+"人未读");
                if (1==showPos){
                    showDatas.clear();
                    showDatas.addAll(unreadDatas);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        jcMsgReadViewModel.getMessageReadUser();
    }
}

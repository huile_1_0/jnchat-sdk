package com.jndv.jndvchatlibrary.ui.chat.bean;

import com.jndv.jnbaseutils.chat.JCSessionListBean;

import java.io.Serializable;

/**
 * Author: wangguodong
 * Date: 2022/4/22
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 会话列表数据实体类
 */
public class JCSessionListSelecteBean implements Serializable {
    JCSessionListBean jcSessionListBean;
    boolean selected;

    public JCSessionListSelecteBean(JCSessionListBean jcSessionListBean, boolean selected) {
        this.jcSessionListBean = jcSessionListBean;
        this.selected = selected;
    }

    public JCSessionListBean getJcSessionListBean() {
        return jcSessionListBean;
    }

    public void setJcSessionListBean(JCSessionListBean jcSessionListBean) {
        this.jcSessionListBean = jcSessionListBean;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}

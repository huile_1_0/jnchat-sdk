package com.jndv.jndvchatlibrary.ui.chat.bean.flyreceive;

import java.util.List;

public class DroneTaskDataBean {
    private Text text;
    private String type;
    public void setText(Text text) {
        this.text = text;
    }
    public Text getText() {
        return text;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }

    public class Point {

        private String move;
        private double lng;
        private int pcontrol;
        private int flyheight;
        private int flyspeed;
        private int sort;
        private int pyaw;
        private double lat;
        public void setMove(String move) {
            this.move = move;
        }
        public String getMove() {
            return move;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
        public double getLng() {
            return lng;
        }

        public void setPcontrol(int pcontrol) {
            this.pcontrol = pcontrol;
        }
        public int getPcontrol() {
            return pcontrol;
        }

        public void setFlyheight(int flyheight) {
            this.flyheight = flyheight;
        }
        public int getFlyheight() {
            return flyheight;
        }

        public void setFlyspeed(int flyspeed) {
            this.flyspeed = flyspeed;
        }
        public int getFlyspeed() {
            return flyspeed;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }
        public int getSort() {
            return sort;
        }

        public void setPyaw(int pyaw) {
            this.pyaw = pyaw;
        }
        public int getPyaw() {
            return pyaw;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }
        public double getLat() {
            return lat;
        }

    }

    public class Data {

        private String planefactoryname;
        private String flyersip;
        private String flyername;
        private String flyerid;
        private String planename;
        private int level;
        private int lineheight;
        private String lineid;
        private String starttime;
        private String title;
        private List<Point> point;
        private String lineyaw;
        private int retlienheight;
        private String planevideo;
        private String planesip;
        private String planecode;
        private String planeid;
        private String planesn;
        private String flyermobile;
        private int linespeed;
        private String linecontrol;
        private String id;
        private int state;
        public void setPlanefactoryname(String planefactoryname) {
            this.planefactoryname = planefactoryname;
        }
        public String getPlanefactoryname() {
            return planefactoryname;
        }

        public void setFlyersip(String flyersip) {
            this.flyersip = flyersip;
        }
        public String getFlyersip() {
            return flyersip;
        }

        public void setFlyername(String flyername) {
            this.flyername = flyername;
        }
        public String getFlyername() {
            return flyername;
        }

        public void setFlyerid(String flyerid) {
            this.flyerid = flyerid;
        }
        public String getFlyerid() {
            return flyerid;
        }

        public void setPlanename(String planename) {
            this.planename = planename;
        }
        public String getPlanename() {
            return planename;
        }

        public void setLevel(int level) {
            this.level = level;
        }
        public int getLevel() {
            return level;
        }

        public void setLineheight(int lineheight) {
            this.lineheight = lineheight;
        }
        public int getLineheight() {
            return lineheight;
        }

        public void setLineid(String lineid) {
            this.lineid = lineid;
        }
        public String getLineid() {
            return lineid;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }
        public String getStarttime() {
            return starttime;
        }

        public void setTitle(String title) {
            this.title = title;
        }
        public String getTitle() {
            return title;
        }

        public void setPoint(List<Point> point) {
            this.point = point;
        }
        public List<Point> getPoint() {
            return point;
        }

        public void setLineyaw(String lineyaw) {
            this.lineyaw = lineyaw;
        }
        public String getLineyaw() {
            return lineyaw;
        }

        public void setRetlienheight(int retlienheight) {
            this.retlienheight = retlienheight;
        }
        public int getRetlienheight() {
            return retlienheight;
        }

        public void setPlanevideo(String planevideo) {
            this.planevideo = planevideo;
        }
        public String getPlanevideo() {
            return planevideo;
        }

        public void setPlanesip(String planesip) {
            this.planesip = planesip;
        }
        public String getPlanesip() {
            return planesip;
        }

        public void setPlanecode(String planecode) {
            this.planecode = planecode;
        }
        public String getPlanecode() {
            return planecode;
        }

        public void setPlaneid(String planeid) {
            this.planeid = planeid;
        }
        public String getPlaneid() {
            return planeid;
        }

        public void setPlanesn(String planesn) {
            this.planesn = planesn;
        }
        public String getPlanesn() {
            return planesn;
        }

        public void setFlyermobile(String flyermobile) {
            this.flyermobile = flyermobile;
        }
        public String getFlyermobile() {
            return flyermobile;
        }

        public void setLinespeed(int linespeed) {
            this.linespeed = linespeed;
        }
        public int getLinespeed() {
            return linespeed;
        }

        public void setLinecontrol(String linecontrol) {
            this.linecontrol = linecontrol;
        }
        public String getLinecontrol() {
            return linecontrol;
        }

        public void setId(String id) {
            this.id = id;
        }
        public String getId() {
            return id;
        }

        public void setState(int state) {
            this.state = state;
        }
        public int getState() {
            return state;
        }

    }

    public class Text {

        private String msg;
        private String code;
        private List<Data> data;
        private int count;
        public void setMsg(String msg) {
            this.msg = msg;
        }
        public String getMsg() {
            return msg;
        }

        public void setCode(String code) {
            this.code = code;
        }
        public String getCode() {
            return code;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }
        public List<Data> getData() {
            return data;
        }

        public void setCount(int count) {
            this.count = count;
        }
        public int getCount() {
            return count;
        }

    }
}

package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.databinding.FragmentFrontpageMeBinding;
import com.jndv.jndvchatlibrary.databinding.FragmentJcUserDetailBinding;
import com.jndv.jndvchatlibrary.http.api.JCImagesUpApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCUpdateUserDetailActivity;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCUserDetailViewModel;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.jndv.jndvchatlibrary.utils.JCmyImagePicker;

//import com.jndv.jndvmeetlibrary.ui.JMVideoConversationActivity;
import com.qingmei2.rximagepicker.core.RxImagePicker;
import com.qingmei2.rximagepicker.entity.Result;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link JCUserDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JCFrontPageMeFragment extends JCbaseFragment {

    private String userId;
    private String crowdId;
    private int type;
    private String invite_permission;
    private String group_domain_addr;
    private String domain_addr;
    private FragmentFrontpageMeBinding meBinding;
    private JCUserDetailViewModel viewModel;
    private FragmentActivity activity;
    String headUrl ;

    public JCFrontPageMeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getString(JCEntityUtils.USER_ID, JNBasePreferenceSaves.getUserSipId());
            crowdId = getArguments().getString(JCEntityUtils.CROWD_ID);
            type = getArguments().getInt(JCEntityUtils.IS_FROM_CROWD, 0);
            group_domain_addr = getArguments().getString(JCEntityUtils.GROUP_DOMAIN_ADDR);
            domain_addr = getArguments().getString(JCEntityUtils.DOMAIN_ADDR);
            invite_permission = getArguments().getString(JCEntityUtils.INVITE_PERMISSION);
        } else {
            userId = JNBasePreferenceSaves.getUserSipId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel =
                new ViewModelProvider(this).get(JCUserDetailViewModel.class);
        meBinding = FragmentFrontpageMeBinding.inflate(inflater, container, false);
        activity = getActivity();
        initView();
        initData();
        return meBinding.getRoot();
    }

    private void initView() {
        viewModel = new JCUserDetailViewModel();
        viewModel.init(activity);
        meBinding.detailTvName.setOnClickListener(view -> {
//            JMVideoConversationActivity.start(getContext(),"0");
        });

        meBinding.tvLogout.setOnClickListener(view -> {
            if (null!=JCChatManager.getmAllOperateListener())JCChatManager.getmAllOperateListener().outLoginOther();

        });

        setJcFragmentSelect(new JCFragmentSelect() {
            @Override
            public void onSelecte(int type, Object... objects) {
                if (type == SELECTE_TYPE_PIC_CROP){
//                    Bitmap bitmap = (Bitmap) objects[1];
                    JCFilesUpUtils.upImage((Uri) objects[0], "", new OnHttpListener<JCImagesUpApi.Bean>() {
                        @Override
                        public void onSucceed(JCImagesUpApi.Bean result) {
                            try {
//                                剪切成功，调用上传接口
                                headUrl = JNBaseUtilManager.getFilesUrl(result.getUrlArr().get(0).getTailUrl());
                                Log.d("imageUpload", "headUrl: "+headUrl);
                                viewModel.updateUserInfo(headUrl);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(Exception e) {
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    JCChatManager.showToast("图片上传失败！");
                                }
                            });
                        }
                    });
                }
            }
        });

        meBinding.detailIvQr.setOnClickListener(v -> {
            JCUserQRcodeFragment jcUserQRcodeFragment = new JCUserQRcodeFragment();
            JCbaseFragmentActivity.start(getActivity(), jcUserQRcodeFragment, "个人二维码");
        });
        meBinding.ivArrowToUserInfo.setOnClickListener(v -> {
            JCUpdateUserDetailActivity.start(getContext(),userId);
        });
        meBinding.detailIvHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxPermissions rxPermissions = new RxPermissions(getActivity());
                rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                        , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
                )
                        .subscribe(new Consumer<Boolean>() {
                            @SuppressLint("CheckResult")
                            @Override
                            public void accept(Boolean aBoolean) {
                                if (aBoolean) {
                                    //申请的权限全部允许
                                    RxImagePicker
                                            .create(JCmyImagePicker.class)
                                            .openGallery(getActivity())
                                            .subscribe(new Consumer<Result>() {
                                                @Override
                                                public void accept(Result result) {
                                                    Uri uri = result.getUri();
                                                    JNLogUtil.e("uri", uri.toString());
                                                    startCropActivity(uri, 1, 1, 200, 200);
                                                }
                                            });
                                } else {
                                    //只要有一个权限被拒绝，就会执行
                                    JCChatManager.showToast("未授权权限，部分功能不能使用");
                                }
                            }
                        });
            }
        });

        meBinding.tvShowIdnum.setOnClickListener(view -> {
            meBinding.tvIdnumText.setVisibility(View.VISIBLE);
        });
    }

    private void initData() {
        viewModel.getUpdateData().observeForever(new Observer<JCRespondBean>() {
            @Override
            public void onChanged(JCRespondBean jcRespondBean) {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, jcRespondBean.getCode())){
                    JCChatManager.showToast("操作成功！");
                    JCglideUtils.loadCircleImage(getActivity(), headUrl, meBinding.detailIvHead);
                    JNBasePreferenceSaves.saveUserHead(headUrl);
                }else {
                    JCChatManager.showToast("操作失败！");
                }
            }
        });
        viewModel.getUserDetail(userId)
                .getUserData().observe(activity, data -> {
            if (data != null) {
                meBinding.detailTvName.setText(data.getNickname());
                JCglideUtils.loadCircleImage(getActivity(), data.getHeadIcon(), meBinding.detailIvHead);

//                JCglideUtils.loadImageToView(activity, data.getAvatarUrl(), meBinding.userInfoContainer.detailIvHead);
                meBinding.tvImnumText.setText(data.getImNum());
                meBinding.detailTvAccount.setText(data.getAccount());
                meBinding.tvPhonenumText.setText(data.getMobile());
                String pass = JNBasePreferenceSaves.getString("userSipPass");
                if(!TextUtils.isEmpty(pass)){
                    meBinding.tvPwdText.setText(pass);
                }
                // TODO: 2022/9/3 sip和账户打通后需要添加性别、年龄、身份证号
                JNLogUtil.e("==user==data.getGender()="+data.getSex());
                String sex = null==data.getSex()?"":data.getSex();
                switch (sex){
                    case "1":
                        meBinding.tvGenderText.setText("男");
                        break;
                    case "2":
                        meBinding.tvGenderText.setText("女");
                        break;
                    default:
                        meBinding.tvGenderText.setText("未知");
                        break;
                }
                meBinding.tvAgeText.setText(data.getAge());
                meBinding.tvIdnumText.setText(data.getIdCardNum());


//                JCUserQRcodeBean bean = new JCUserQRcodeBean(data.getNickname(), data.getAvatarUrl(), data.getImNumber(), JNBasePreferenceSaves.getSipAddress(), JNBasePreferenceSaves.getJavaAddress());
//                JCQRcodeBeanBase jcqRcodeBeanBase = new JCQRcodeBeanBase(JCQRcodeBeanBase.QRCODE_TYPE_USER, new Gson().toJson(bean));
//                Bitmap qrBitmap = CodeUtils.createImage(new Gson().toJson(jcqRcodeBeanBase), 280, 280, null);
//                JCglideUtils.loadImage(getActivity(), qrBitmap, meBinding.detailIvQr);
            }
        });

    }


    public static JCFrontPageMeFragment newInstance(String userId, String crowdId, String domain_addr, String group_domain_addr, int type,String invite_permission) {
        JCFrontPageMeFragment fragment = new JCFrontPageMeFragment();
        Bundle args = new Bundle();
        args.putString(JCEntityUtils.USER_ID, userId);
        args.putString(JCEntityUtils.CROWD_ID, crowdId);
        args.putString(JCEntityUtils.INVITE_PERMISSION, invite_permission);
        args.putInt(JCEntityUtils.IS_FROM_CROWD, type);
        args.putString(JCEntityUtils.DOMAIN_ADDR, domain_addr);
        args.putString(JCEntityUtils.GROUP_DOMAIN_ADDR, group_domain_addr);
        fragment.setArguments(args);
        return fragment;
    }

}

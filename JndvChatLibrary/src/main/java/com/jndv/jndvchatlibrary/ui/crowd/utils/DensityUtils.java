package com.jndv.jndvchatlibrary.ui.crowd.utils;

import android.content.Context;


public class DensityUtils {

	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}


	public static boolean judgeIsBigPad() {
		double src = GlobalConfig.SCREEN_WIDTH / GlobalConfig.SCREEN_HEIGHT;
		double des = 3 / 4;
		if (GlobalConfig.PROGRAM_IS_PAD && src != des) {
			return true;
		}
		return false;
	}

}

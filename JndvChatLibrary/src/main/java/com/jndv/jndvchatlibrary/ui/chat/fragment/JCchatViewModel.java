package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.http.api.JCMessageRecordApi;
import com.jndv.jndvchatlibrary.http.api.JCNotLineMessageApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCmsgModelItem;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgNotifyContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCNotifyType;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;
/**
 * Author: wangguodong
 * Date: 2022/2/11
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 聊天页面中，数据管理工具，获取本地、网络消息列表操作实现
 */
public class JCchatViewModel extends ViewModel {

    private MutableLiveData<List<JCimMessageBean>> datas;
    private MutableLiveData<JCmsgModelItem> dataChange;

    private String otherId;//对方id
    private String sessionId;//会话id
    private String sessionType;//会话类型
    private String sessionaddress;//会话域地址
//    private Fragment fragment;
    private LifecycleOwner lifecycleOwner;

    public JCchatViewModel() {
        datas = new MutableLiveData<>();
        dataChange = new MutableLiveData<>();
    }

    public void init(String otherId,String sessionId, String sessionType, String sessionaddress, LifecycleOwner lifecycleOwner){
        this.otherId = otherId ;
        this.sessionId = sessionId ;
        this.sessionType = sessionType ;
        this.sessionaddress = sessionaddress ;
        this.lifecycleOwner = lifecycleOwner ;
    }

    public LiveData<List<JCimMessageBean>> getMessageList() {
        return datas;
    }
    public LiveData<JCmsgModelItem> getDataChange(){
        return dataChange;
    }

    /**
     * 获取消息列表
     * 一般为首次进入页面
     * 先获取本地的最新的N条
     * 再获取网络离线消息列表
     */
    public void getMessageData(){
        if (null==otherId)return;
//        1、先获取本地数据库
//        2、再获取网络数据
        Log.e("netdata", "getMessageData: ============otherId=" + otherId);
        Log.e("netdata", "getMessageData: ============sessionType=" + sessionType);
        QueryBuilder<JCimMessageBean> builder = JCobjectBox.get().boxFor(JCimMessageBean.class).query();
        List<JCimMessageBean> dataLocal = builder.equal(JCimMessageBean_.sessionId, otherId,QueryBuilder.StringOrder.CASE_SENSITIVE)
                .equal(JCimMessageBean_.sessionType, sessionType,QueryBuilder.StringOrder.CASE_SENSITIVE)
                .orderDesc(JCimMessageBean_.receiveTime)//降序
                .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
//                .order(JCimMessageBean_.receiveTime)//升序
                .build().find(0,20);
        dataLocal = sortData(dataLocal);
        JNLogUtil.e("netdata", "onSucceed: ========getMessageData=======" + new Gson().toJson(dataLocal));
        datas.setValue(dataLocal);
        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
        jCmsgModelItem.setDatas(dataLocal);
        jCmsgModelItem.setType(0);
        dataChange.setValue(jCmsgModelItem);
        getNotLineMessage();
    }

//  排序
    public List<JCimMessageBean> sortData(List<JCimMessageBean> mList) {
        try {
            Collections.sort(mList, new Comparator<JCimMessageBean>() {
                @Override
                public int compare(JCimMessageBean o1, JCimMessageBean o2) {
                    long time1 = Long.parseLong(o1.getReceiveTime());
                    long time2 = Long.parseLong(o2.getReceiveTime());
                    if (time1 > time2){
                        return 1;
                    }
                    return -1;
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        return mList;
    }

    /**
     * 给列表中调价一条数据
     * 添加一条消息
     * @param message
     */
    public void addData(JCbaseIMMessage message){
        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
        jCmsgModelItem.setItem(message);
        jCmsgModelItem.setType(0);
        dataChange.setValue(jCmsgModelItem);
    }

    /**
     * 更新消息
     * @param message
     */
    public void updateData(JCbaseIMMessage message){
        JNLogUtil.e("JCchatFragment", "===001==");
        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
        jCmsgModelItem.setItem(message);
        jCmsgModelItem.setIndex(-1);
        jCmsgModelItem.setType(2);
        dataChange.setValue(jCmsgModelItem);
    }
    /**
     * 更新列表中信息的已读状态
     * @param message
     */
    public void updateDataRead(JCbaseIMMessage message){
        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
        jCmsgModelItem.setItem(message);
        jCmsgModelItem.setIndex(-1);
        jCmsgModelItem.setType(6);
        dataChange.setValue(jCmsgModelItem);
    }

    /**
     * 获取离线消息列表
     *
     */
    public void getNotLineMessage(){
        if (null==otherId)return;
        EasyHttp.get(lifecycleOwner)
                .api(new JCNotLineMessageApi()
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                        .setSessionPartyId(otherId)
                        .setSessionPartyDomainAddr(sessionaddress)
                        .setSessionType(""+sessionType)
                        .setPageNo("1")
                        .setPageSize(""+ JCChatManager.pageSize)
                )
                .request(new OnHttpListener<JCNotLineMessageApi.Bean>() {
                    @Override
                    public void onSucceed(JCNotLineMessageApi.Bean result) {
//                        0587872a-b674-4ce0-acad-50de90b15b29
                        JNLogUtil.e("onSucceed: ========getNotLineMessage=======" + new Gson().toJson(result));
                        try {
                            if (null!=result && null!=result.getData()&&result.getData().getList().size()>0){
                                List<JCimMessageBean> data = new ArrayList<>();
                                Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                                JNLogUtil.e("onSucceed: ==getNotLineMessage======001=======" +result.getData().getList().size());
                                for (int i = 0; i < result.getData().getList().size(); i++) {
                                    JNLogUtil.e("onSucceed: ==getNotLineMessage======002=======" );
                                    QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
                                    JCimMessageBean jCimMessageBean = result.getData().getList().get(i);
                                    JNLogUtil.e("onSucceed: ==getNotLineMessage======003=======" );
                                    //会话邀请信息不展示
                                    if(TextUtils.equals(jCimMessageBean.getMsgType(), JCSessionType.SESSION_CREATE_MEETING)){
                                        continue;
                                    }
                                    if (TextUtils.equals(jCimMessageBean.getMsgType(),""+ JCmessageType.NOTIFY)){
                                        JNLogUtil.e("onSucceed: ==getNotLineMessage======004=======" );
                                        JCMsgNotifyContentBean bean = new Gson().fromJson(new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP)), JCMsgNotifyContentBean.class);
                                        JNLogUtil.e("onSucceed: ==getNotLineMessage======005=======" );
                                        if (TextUtils.equals(bean.getType(), ""+ JCNotifyType.NOTIFY_msgWithdraw)){
                                            JNLogUtil.e("onSucceed: ==getNotLineMessage======006=======" );
                                            continue;
                                        }
                                    }
                                    JCimMessageBean messageBean = builder.equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                                            .equal(JCimMessageBean_.msgID, jCimMessageBean.getMsgID(),QueryBuilder.StringOrder.CASE_SENSITIVE).build().findFirst();
                                    if (null!=messageBean){
                                        if (TextUtils.equals(messageBean.getMsgType(), JCmessageType.FILE)){
                                            jCimMessageBean.setContent(messageBean.getContent());
                                        }
                                        jCimMessageBean.setId(messageBean.getId());
//                                        jCimMessageBean.setStatus(messageBean.getStatus());
                                        jCimMessageBean.setStatus(""+ JCmessageStatusType.sendSuccessceS);
                                        jCimMessageBean.setReceiveTime(messageBean.getReceiveTime());
                                    }else {
                                        jCimMessageBean.setStatus(""+ JCmessageStatusType.sendSuccessceS);
                                        jCimMessageBean.setReceiveTime(jCimMessageBean.getSendTime());
//                                        jCimMessageBean.setReceiveTime(""+System.currentTimeMillis());
                                    }
                                    jCimMessageBean.setSaveUserId(JNBasePreferenceSaves.getUserSipId());
                                    imMsgBox.put(jCimMessageBean);
                                    data.add(jCimMessageBean);
                                }
                                JNLogUtil.e("onSucceed: ==getNotLineMessage======007=======" +data.size());
                                JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
                                jCmsgModelItem.setDatas(data);
                                jCmsgModelItem.setType(7);
                                dataChange.setValue(jCmsgModelItem);
                                JNLogUtil.e("onSucceed: ==getNotLineMessage======008=======" +data.size());
                            }
                            EventBus.getDefault().post(new JNCodeEvent
                                    (JNEventBusType.CODE_SESSION_LIST_UPDATE_NONUM,sessionId));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFail(Exception e) {
                        netFail();
                    }
                });
    }

    /**
     * 获取旧消息列表
     * 下拉加载消息
     */
    public void getOldMessage(long time){
        if (null==otherId)return;
        Log.e("JCchatFragment", "getNewMessage: ==============time==" + time);
        getMessageRecord(time, "2", 0);

    }

    /**
     * 获取消息列表
     * 上拉加载消息
     * addType  0=第一页直接替换；1=添加到底部add(0,datas)
     */
    public void getNewMessage(long time, int addType){
        JNLogUtil.e("weidu", "getNewMessage: ==============time==" + time);
//        String host = JCSpUtils.getString(fragment.getContext(), JCPjSipConstants.PJSIP_HOST, JCPjSipConstants.PJSIP_HOST_DEFAULT);
//        String port = JCSpUtils.getString(fragment.getContext(), JCPjSipConstants.PJSIP_PORT, JCPjSipConstants.PJSIP_PORT_DEFAULT);
        if (null==otherId)return;
        getMessageRecord(time, "1", addType);
    }

    public void getMessageRecord(long time,String slideType, int addType){
        EasyHttp.get(lifecycleOwner)
                .api(new JCMessageRecordApi()
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                        .setSessionId(sessionId)
                        .setSessionPartyId(otherId)
                        .setSessionPartyDomainAddr(sessionaddress)
                        .setSessionType(""+sessionType)
                        .setBeginTime(""+time)
                        .setSlideType(slideType)
                        .setPageNo("1")
                        .setPageSize(""+ JCChatManager.pageSize)
                )
                .request(new OnHttpListener<JCMessageRecordApi.Bean>() {
                    @Override
                    public void onSucceed(JCMessageRecordApi.Bean result) {
                        try {
                            Log.e("JCchatFragment", "getNewMessage: =======onSucceed=======result==" + new Gson().toJson(result));
                            if (null!=result && null!=result.getData()){
                                if (slideType.equals("1")){
                                    if (0==addType){
                                        List<JCimMessageBean> data = initDataList(result.getData().getList());
                                        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
                                        jCmsgModelItem.setDatas(data);
                                        jCmsgModelItem.setType(8);
                                        dataChange.setValue(jCmsgModelItem);
                                    }else {
                                        List<JCimMessageBean> data = initDataList(result.getData().getList());
                                        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
                                        jCmsgModelItem.setDatas(data);
                                        jCmsgModelItem.setType(4);
                                        dataChange.setValue(jCmsgModelItem);
                                    }
                                }else {
                                    List<JCimMessageBean> data = new ArrayList<>();
                                    try {
                                        Log.e("JCchatFragment", "getOldMessage: =======onSucceed=======result==" + new Gson().toJson(result));
                                        if (null!=result && null!=result.getData() && null!=result.getData().getList()){
                                            data = initDataList(result.getData().getList());
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    Log.e("JCchatFragment", "getOldMessage: =======onSucceed=======data.size()==" + data.size());
//                        Log.e("JCchatFragment", "getOldMessage: =======onSucceed=======result==" + new Gson().toJson(data));

                                    JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
                                    jCmsgModelItem.setDatas(data);
                                    jCmsgModelItem.setType(3);
                                    dataChange.setValue(jCmsgModelItem);
                                }

                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFail(Exception e) {
                        netFail();
                    }
                });
    }

    private void netFail(){
        Log.e("JCchatFragment", "getNewMessage: =======netFail=======result==" );
        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
        jCmsgModelItem.setType(5);
        dataChange.setValue(jCmsgModelItem);
    }

    private List<JCimMessageBean> initDataList(List<JCimMessageBean> result){
        List<JCimMessageBean> data = new ArrayList<>();
        Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
        for (int i = 0; i < result.size(); i++) {
            QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
            JCimMessageBean jCimMessageBean = result.get(i);
            JCimMessageBean messageBean = builder.equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCimMessageBean_.msgID, jCimMessageBean.getMsgID(),QueryBuilder.StringOrder.CASE_SENSITIVE).build().findFirst();
            if (null!=messageBean){
                jCimMessageBean.setId(messageBean.getId());
                if (TextUtils.equals(""+JCmessageStatusType.sendIng,messageBean.getStatus()))
                    jCimMessageBean.setStatus(""+ JCmessageStatusType.sendSuccessceS);
                    else jCimMessageBean.setStatus(messageBean.getStatus());
                if (TextUtils.isEmpty(jCimMessageBean.getReceiveTime()))
                    jCimMessageBean.setReceiveTime(jCimMessageBean.getSendTime());
//                goUpdateMsg(jCimMessageBean);
            }else {
                jCimMessageBean.setStatus(""+ JCmessageStatusType.sendSuccessceS);
                jCimMessageBean.setReceiveTime(jCimMessageBean.getSendTime());
                imMsgBox.put(jCimMessageBean);
//                data.add(jCimMessageBean);
            }
            data.add(jCimMessageBean);
        }
        return data;
    }


}
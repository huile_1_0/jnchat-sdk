package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
 * @ProjectName: JndvChat
 * @Package: com.jndv.jndvchatlibrary.ui.chat.bean
 * @ClassName: JCMsgVoiceContentBean
 * @Description: 语音消息内容类
 * @Author: SunQinzheng
 * @CreateDate: 2022/2/19 上午 11:11
 */
 public class JCMsgVoiceContentBean {
    /**
     * "extend":"",//扩展字段（暂无意义）
     * "url":"",//语音文件地址
     * "time":"",//语音时长
     */

    private String time;
    private String url;
    private String path;

    public JCMsgVoiceContentBean(String time, String path) {
        this.time = time;
        this.path = path;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "JCMsgVoiceContentBean{" +
                "time='" + time + '\'' +
                ", url='" + url + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}

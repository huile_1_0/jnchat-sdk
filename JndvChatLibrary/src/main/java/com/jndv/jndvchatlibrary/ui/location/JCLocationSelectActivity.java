package com.jndv.jndvchatlibrary.ui.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;

import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.offline.MKOLSearchRecord;
import com.baidu.mapapi.map.offline.MKOfflineMap;
import com.baidu.mapapi.map.offline.MKOfflineMapListener;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeOption;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionResult;
import com.baidu.mapapi.search.sug.SuggestionSearch;
import com.baidu.mapapi.search.sug.SuggestionSearchOption;
import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.ui.location.adapter.AddressSelectAdapter;
import com.jndv.jndvchatlibrary.ui.location.adapter.DefaultItemDecoration;
import com.jndv.jndvchatlibrary.ui.location.util.FullscreenInputWorkaroundUtil;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;


/**
 * 功能描述:地址选择
 * Created on 2020/6/17.
 * @author  lyw
 */
public class JCLocationSelectActivity extends AppCompatActivity implements AddressSelectAdapter.OnAddressSelectItemClickListener{
    private final String TAG ="JCLocationSelectActivity";
    private RecyclerView mJARecyclerView;
    private TextView cancleTv,searchAddressTv,confirmTv,searchNoDataTv;
    private MapView mBmapView;
    private EditText mSearchAddressEt;
    private LinearLayout searchNoDataLayout,searchNonmalLayout,searchEditLayout;
    private ImageView centerImage,locationIv,clearInputIv,mCheckTipIv;

    private BaiduMap mBaiduMap;
    private LocationClient mLocationClient;
    //是否首次定位
    boolean isFirstLoc = true;
    private AddressSelectAdapter adapter;
    private List<PoiInfo> mFilterList = new ArrayList<>();
    //选择的位置信息
    private PoiInfo selPoiDetailInfo;
    // 当前经纬度
    double mLantitude;
    double mLongtitude;
    LatLng mLoactionLatLng;
    // MapView中央对于的屏幕坐标
    Point mCenterPoint = null;
    // 地理编码
    GeoCoder mGeoCoder = null;
    List<PoiInfo> mInfoList;
    PoiInfo mCurentInfo;
    MyBDLocationListner mListner = null;
    private Animation centerAnimation;
    //是否使用反地理编码
    private boolean isUseGeoCoder = true;
    //是否需要动画
    private boolean isNeedAnimation = true;
    private InputMethodManager inputMethodManager;
    private SuggestionSearch mSuggestionSearch;
    private String changeContent;
    private MKOfflineMap mOffline;
    //选中的CityCode
    private String selectCityCode;
    private ArrayList<MKOLSearchRecord> mkolSearchRecords;
    private RotateAnimation mAnimation;
    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_address_select_layout);
        FullscreenInputWorkaroundUtil.assistActivity(this);
        mContext = this;
        //设置状态栏透明
//        StatusBarUtils.makeStatusBarTransparent(SelectAddressActivity.this);
        //请求位置权限
        RxPermissions rxPermissions = new RxPermissions(JCLocationSelectActivity.this);
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION
                        , Manifest.permission.ACCESS_COARSE_LOCATION
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean) {
                            initView();
                            initEvent();
                            inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            //得到城市code
                            mOffline = new MKOfflineMap();
                            mOffline.init(new MKOfflineMapListener() {
                                @Override
                                public void onGetOfflineMapState(int i, int i1) {

                                }
                            });
                        } else {
                            //只要有一个权限被拒绝，就会执行
                            Toast.makeText(JCLocationSelectActivity.this, "未授权权限，部分功能不能使用", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mBmapView != null) {
            mBmapView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBmapView != null) {
            mBmapView.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBmapView != null) {
            mBmapView.onDestroy();
        }
        mBaiduMap.setMyLocationEnabled(false);
        if (mLocationClient != null) {
            mLocationClient.stop();
        }
        if (mGeoCoder != null) {
            mGeoCoder.destroy();
        }
        if (mSuggestionSearch != null) {
            mSuggestionSearch.destroy();
        }
        if (mOffline != null) {
            mOffline.destroy();
        }
    }

    private void initView() {
        mJARecyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        cancleTv = (TextView)findViewById(R.id.cancle_tv);
        searchAddressTv = (TextView)findViewById(R.id.search_address_tv);
        confirmTv = (TextView)findViewById(R.id.confirm_tv);
        mBmapView = (MapView)findViewById(R.id.bmapView);
        mSearchAddressEt = (EditText)findViewById(R.id.search_address_et);
        searchNoDataTv = (TextView)findViewById(R.id.search_no_data_tv);
        searchNoDataLayout = (LinearLayout)findViewById(R.id.search_no_data_layout);
        searchNonmalLayout = (LinearLayout)findViewById(R.id.search_nonmal_layout);
        searchEditLayout = (LinearLayout)findViewById(R.id.search_edit_layout);
        centerImage = (ImageView)findViewById(R.id.center_image);
        locationIv = (ImageView)findViewById(R.id.location_iv);
        clearInputIv = (ImageView)findViewById(R.id.clear_input_iv);
        mCheckTipIv = (ImageView)findViewById(R.id.check_tip_iv);
    }

    private void initEvent() {
        mBaiduMap = mBmapView.getMap();
        adapter = new AddressSelectAdapter(this);
        adapter.setOnAddressSelectItemClickListener(this);
        mJARecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mJARecyclerView.addItemDecoration(new DefaultItemDecoration(this, R.dimen.common_utils_divider_height, R.color.src_line_c9));
        mJARecyclerView.setAdapter(adapter);
        mSearchAddressEt.setHint("搜索地点");
        searchAddressTv.setText("搜索地点");
        mSearchAddressEt.addTextChangedListener(new SearchDevicesTextWatcher());
        cancleTv.setText("取消");
        cancleTv.setOnClickListener(v->{
            finish();
        });
        confirmTv.setText("确定");
        confirmTv.setOnClickListener(v->{
            try {
                Log.d("baiduLocation", "selPoiDetailInfo.address: "+selPoiDetailInfo.address+"selPoiDetailInfo.latitude: "+selPoiDetailInfo.getLocation().latitude+"selPoiDetailInfo.longitude: "+selPoiDetailInfo.getLocation().longitude);
                EventBus.getDefault().post(new JNCodeEvent<PoiInfo>(JNEventBusType.CODE_ON_SELECT_LOCATION, "",selPoiDetailInfo));
                finish();
            }catch (Exception e){
                e.printStackTrace();
            }
        });


        //加载动画
        mAnimation = new RotateAnimation(0, 359, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mAnimation.setDuration(1000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setInterpolator(new LinearInterpolator());
        mCheckTipIv.setColorFilter(getResources().getColor(R.color.src_c1));
        mCheckTipIv.setImageResource(R.mipmap.icon_login_loading);
        centerAnimation = AnimationUtils.loadAnimation(this, R.anim.center_anim);

        initMyLocation();
        mBaiduMap.setCompassEnable(false);


        searchNonmalLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchNonmalLayout.setVisibility(View.GONE);
                searchEditLayout.setVisibility(View.VISIBLE);
                showInput(mSearchAddressEt);
            }
        });

        clearInputIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchNonmalLayout.setVisibility(View.VISIBLE);
                searchEditLayout.setVisibility(View.GONE);
                mSearchAddressEt.setText("");
                mSearchAddressEt.clearFocus();
                hideSoftInputWindow(v);
                mFilterList.clear();
                if (mLoactionLatLng != null) {
                    MapStatus.Builder builder = new MapStatus.Builder();
                    builder.target(mLoactionLatLng).zoom(17.0f);
                    mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus
                            (builder.build()));
                    isUseGeoCoder = true;
                    isNeedAnimation = false;
                } else {
                }
            }
        });

        locationIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoactionLatLng != null) {
                    MapStatus.Builder builder = new MapStatus.Builder();
                    builder.target(mLoactionLatLng).zoom(17.0f);
                    mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus
                            (builder.build()));
                    isUseGeoCoder = true;
                    isNeedAnimation = true;
                } else {
                }
            }
        });
    }

    /**
     * 隐藏键盘
     */
    private void hideSoftInputWindow(View view) {
        if (inputMethodManager.isActive()) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * 显示键盘
     * @param et 输入焦点
     */
    public void showInput(final EditText et) {
        et.requestFocus();
        if (inputMethodManager.isActive()) {
            inputMethodManager.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    /**
     * 获取我的定位
     */
    private void initMyLocation() {
        // 初始化地图
        mBmapView.showZoomControls(false);
        MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(17.0f);
        mBaiduMap.setMapStatus(msu);
        mInfoList = new ArrayList<PoiInfo>();
        mCenterPoint = mBaiduMap.getMapStatus().targetScreen;
        mLoactionLatLng = mBaiduMap.getMapStatus().target;

        // 定位
        mBaiduMap.setMyLocationEnabled(true);
        try {
            mLocationClient = new LocationClient(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mListner = new MyBDLocationListner();
        mLocationClient.registerLocationListener(mListner);
        LocationClientOption option = new LocationClientOption();
        option.setCoorType("bd09ll");
        mLocationClient.setLocOption(option);
        mLocationClient.start();

        // 地理编码
        mGeoCoder = GeoCoder.newInstance();
        mGeoCoder.setOnGetGeoCodeResultListener(mGeoListener);

        showLoading(true);

        mBaiduMap.setOnMapClickListener(new BaiduMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mBaiduMap.clear();
                adapter.changeSelected(0);
                mJARecyclerView.smoothScrollToPosition(0);
                MapStatusUpdate u = MapStatusUpdateFactory
                        .newLatLng(latLng);
                mBaiduMap.animateMapStatus(u);
                isUseGeoCoder = true;
            }

            @Override
            public void onMapPoiClick(MapPoi mapPoi) {
                LatLng position = mapPoi.getPosition();
                mBaiduMap.clear();
                MapStatusUpdate u = MapStatusUpdateFactory
                        .newLatLng(position);
                mBaiduMap.animateMapStatus(u);
                isUseGeoCoder = true;

            }
        });


        mBaiduMap.setOnMapTouchListener(new BaiduMap.OnMapTouchListener() {
            @Override
            public void onTouch(MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    isUseGeoCoder = true;
                    isNeedAnimation = true;
                }
            }
        });

        mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {
            @Override
            public void onMapStatusChangeStart(MapStatus mapStatus) {
            }

            @Override
            public void onMapStatusChangeStart(MapStatus mapStatus, int i) {
            }

            @Override
            public void onMapStatusChange(MapStatus mapStatus) {
            }

            @SuppressLint("LongLogTag")
            @Override
            public void onMapStatusChangeFinish(MapStatus mapStatus) {
                mBaiduMap.clear();
                if (isNeedAnimation) {
                    centerImage.startAnimation(centerAnimation);
                }
                LatLng target = mapStatus.target;
                if (isUseGeoCoder) {
                    showLoading(true);
                    Log.e(TAG, "onMapStatusChangeFinish: target="+new Gson().toJson(target));
                    mGeoCoder.reverseGeoCode((new ReverseGeoCodeOption())
                            .location(target).newVersion(1) );
                    adapter.changeSelected(0);
                    mJARecyclerView.smoothScrollToPosition(0);
                }
            }
        });
        mSuggestionSearch = SuggestionSearch.newInstance();
        mSuggestionSearch.setOnGetSuggestionResultListener(listener);
    }


    OnGetSuggestionResultListener listener = new OnGetSuggestionResultListener() {
        @SuppressLint("LongLogTag")
        @Override
        public void onGetSuggestionResult(SuggestionResult suggestionResult) {
            Log.e(TAG, "onGetSuggestionResult:  0001");
            //处理sug检索结果
            List<SuggestionResult.SuggestionInfo> allSuggestions = suggestionResult.getAllSuggestions();
            mInfoList.clear();
            if (allSuggestions == null) {
                Log.e(TAG, "onGetSuggestionResult:  null");
            }else {
                for (SuggestionResult.SuggestionInfo suggestionInfo : allSuggestions) {
                    if (suggestionInfo == null) {
                        continue;
                    }
                    PoiInfo info = new PoiInfo();
                    info.address = suggestionInfo.getKey();
                    LatLng pt = suggestionInfo.pt;
                    info.location = pt;
                    info.city = suggestionInfo.city;
                    info.name = suggestionInfo.getKey();
                    mInfoList.add(info);

                }
                selPoiDetailInfo = mInfoList.get(0);
                mkolSearchRecords = mOffline.searchCity(selPoiDetailInfo.getCity());
                selectCityCode = String.valueOf(mkolSearchRecords.get(0).cityID);
            }
            handleRelativeAddressResult(mInfoList, changeContent);
        }
    };

    @Override
    public void onAddressSelectItemClick(PoiInfo info, int position) {
        isUseGeoCoder = false;
        isNeedAnimation = true;
        mBaiduMap.clear();
        LatLng la = info.location;
        MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(la);
        mBaiduMap.animateMapStatus(u);
        selPoiDetailInfo = info;
//        Log.d("baiduLocation", "info.address: "+info.address+"selPoiDetailInfo.latitude: "+info.getLocation().latitude+"selPoiDetailInfo.longitude: "+info.getLocation().longitude);
        adapter.changeSelected(position);
        if (position != 0) {
            mkolSearchRecords = mOffline.searchCity(selPoiDetailInfo.getCity());
            selectCityCode = String.valueOf(mkolSearchRecords.get(0).cityID);
        }

    }


    // 定位监听器
    private class MyBDLocationListner implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            if (location == null || mBmapView == null){
                return;
            }
            if (isFirstLoc) {
                MyLocationData data = new MyLocationData.Builder()
                        .accuracy(location.getRadius())
                        .latitude(location.getLatitude())
                        .longitude(location.getLongitude())
                        .build();
                mBaiduMap.setMyLocationData(data);
                MyLocationConfiguration config = new MyLocationConfiguration(
                        MyLocationConfiguration.LocationMode.NORMAL, false, null);
                mBaiduMap.setMyLocationConfigeration(config);
                mLantitude = location.getLatitude();
                mLongtitude = location.getLongitude();
                LatLng currentLatLng = new LatLng(mLantitude, mLongtitude);
                mLoactionLatLng = new LatLng(mLantitude, mLongtitude);
                isFirstLoc = false;
                MapStatusUpdate u = MapStatusUpdateFactory
                        .newLatLng(currentLatLng);
                mBaiduMap.animateMapStatus(u);
                mGeoCoder.reverseGeoCode((new ReverseGeoCodeOption())
                        .location(currentLatLng));
            }
        }
    }

    /**
     * 地理编码监听器
     */
    OnGetGeoCoderResultListener mGeoListener = new OnGetGeoCoderResultListener() {
        @SuppressLint("LongLogTag")
        @Override
        public void onGetGeoCodeResult(GeoCodeResult result) {
            Log.e(TAG, "onGetReverseGeoCodeResult:    0002" );
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onGetReverseGeoCodeResult(ReverseGeoCodeResult result) {
//            {"e":0,"j":0,"error":"RESULT_NOT_FOUND","status":240}
//            {"e":0,"j":0,"error":"SEARCH_SERVER_INTERNAL_ERROR","status":0}
            Log.e(TAG, "onGetReverseGeoCodeResult:    0001" );
            if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
                Log.e(TAG, "onGetReverseGeoCodeResult:    00010 result=" + new Gson().toJson(result));
                if (result.error == SearchResult.ERRORNO.NETWORK_ERROR) {
                    Log.e(TAG, "onGetReverseGeoCodeResult:    00011" );
                    Toast.makeText(JCLocationSelectActivity.this,"网络异常",Toast.LENGTH_SHORT).show();
                }else {
                    JCChatManager.showToast("信息获取失败！");
                }
                showLoading(false);
            } else {
                showLoading(false);
                mCurentInfo = new PoiInfo();
                mCurentInfo.address = result.getAddress();
                mCurentInfo.location = result.getLocation();
                mCurentInfo.name = result.getAddress();
                mCurentInfo.city = String.valueOf(result.getCityCode());
                selectCityCode = String.valueOf(result.getCityCode());
                mInfoList.clear();
                mInfoList.add(mCurentInfo);
                if (result.getPoiList() != null) {
                    mInfoList.addAll(result.getPoiList());
                }
                adapter.setPoiAddrList(mInfoList);
                adapter.notifyDataSetChanged();
                selPoiDetailInfo = mInfoList.get(0);
            }
        }
    };


    class SearchDevicesTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int before) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            changeContent = charSequence.toString();
            if (changeContent != null && !changeContent.isEmpty() || !"".equals(changeContent)) {
                mSuggestionSearch.requestSuggestion(new SuggestionSearchOption().city("中国").keyword(changeContent));
            }else {
                if (mLoactionLatLng != null) {
                    MapStatus.Builder builder = new MapStatus.Builder();
                    builder.target(mLoactionLatLng).zoom(17.0f);
                    mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus
                            (builder.build()));
                    isUseGeoCoder = true;
                    isNeedAnimation = false;
                } else {
                }
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    }

    /**
     * 处理搜索地点结果
     *
     * @param mInfoList
     */
    @SuppressLint("LongLogTag")
    private void handleRelativeAddressResult(List<PoiInfo> mInfoList, String content) {
        Log.e(TAG, "handleRelativeAddressResult:    0003" );
        mJARecyclerView.setAdapter(adapter);
        adapter.setPoiAddrList(mInfoList);
        if (mInfoList.size() == 0) {
            searchNoDataLayout.setVisibility(View.VISIBLE);
            String formatContent = String.format("没有找到“%1$s”相关内容", content);
            SpannableStringBuilder builder = new SpannableStringBuilder(formatContent);
            ForegroundColorSpan blueSpan = new ForegroundColorSpan(getResources().getColor(R.color.src_text_c16));
            int start = formatContent.indexOf(content);
            int end = start + content.length();
            builder.setSpan(blueSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            searchNoDataTv.setText(builder);
        } else {
            searchNoDataLayout.setVisibility(View.GONE);
        }
    }


    /**
     * Loading的显示
     * @param isShowLoading
     */
    private void showLoading(final boolean isShowLoading){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isShowLoading) {
                    mCheckTipIv.setVisibility(View.VISIBLE);
                    mCheckTipIv.setImageResource(R.mipmap.icon_login_loading);
                    mCheckTipIv.setAnimation(mAnimation);
                    mCheckTipIv.getAnimation().start();
                }else {
                    mCheckTipIv.clearAnimation();
                    mCheckTipIv.setVisibility(View.GONE);
                }
            }
        });
    }
}


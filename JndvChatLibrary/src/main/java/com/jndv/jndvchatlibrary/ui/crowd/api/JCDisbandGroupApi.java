package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

public class JCDisbandGroupApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/group/disbandGroup";
    }

    private String domainAddr;
    private String userId;
    private String groupId;

    public JCDisbandGroupApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCDisbandGroupApi setGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    public JCDisbandGroupApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public final static class Bean {

        private String code;
        private String msg;
        private Data data;

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public Data getData() {
            return data;
        }

        public class Data {

        }


    }
}

package com.jndv.jndvchatlibrary.ui.crowd.entity;

import android.text.TextUtils;
import android.util.Log;


import com.jndv.jndvchatlibrary.ui.crowd.utils.LogUtil;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * 与工作版一样，不允许随意改动、有三个引用类有问题引用应该有些问题
 */
public class JCUser implements Comparable<JCUser>, Serializable {
    private static final String TAG = "User";
    private String crowdrole; // 是否是群主  1 群主，2群员

    public String getCrowdrole() {
        return crowdrole;
    }

    public void setCrowdrole(String crowdrole) {
        this.crowdrole = crowdrole;
    }

    // Server transfer fields
    // 通过OnGetGroupUserInfo传来
    // 登录用的帐号字符串
    private String mAccount;
    private int mAccountType = 0;
    private String mAddress;

    private String mAvatarPath;//本地路径，不使用
    private String mAvatarLocation;//头像uri地址，统一使用这个

    private int mAuthtype = 0;// 取值0允许任何人，1需要验证，2不允许任何人

    private Date mBirthday;

    private String mStringBirthday;
    // bsystemavatar='1'
    private String mEmail;
    private String mFax;
    // homepage='http://wenzongliang.com'
    private long mUserId;
    private String mJob;
    private String mMobile;

    private String mName;

    // privacy='0'
    private String mSex;
    private String mSignature;
    private String mTelephone;

    // group
    private String mCompany;
    private String mDepartment;
    // end Server transfer fields
    // custom fields
    private boolean isCurrentLoggedInUser;
    private JCNetworkStateCode mResult;
    private DeviceType mType;
    private Status mStatus;
    // 登录后显示的昵称
    private String mNickName;
    private String mCommentName;

    private Set<JCGroup> mBelongsJCGroup;
    private String abbra;
    // For GroupMemberActivity
    public boolean isShowDelete;
    private static HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
    // This value indicate this object is dirty, construct locally without any
    // user information
    private boolean isFromService;
    // 会议中的快速入会用户
    private boolean isRapidInitiation = false;
    public boolean isContain;

    private String mCellPhone;
    private String mTitle;

    public String imnumber;//IM通讯
    public String officenumber;//办公室内线号
    public String djjnumber;//对讲机号

    public String getImnumber() {
        return imnumber;
    }

    public void setImnumber(String imnumber) {
        this.imnumber = imnumber;
    }

    public String getOfficenumber() {
        return officenumber;
    }

    public void setOfficenumber(String officenumber) {
        this.officenumber = officenumber;
    }

    public String getDjjnumber() {
        return djjnumber;
    }

    public void setDjjnumber(String djjnumber) {
        this.djjnumber = djjnumber;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getmDepartment() {
        return mDepartment;
    }

    public void setmDepartment(String mDepartment) {
        this.mDepartment = mDepartment;
    }

    public String getCellPhone() {
        return mCellPhone;
    }

    public void setCellPhone(String mCellPhone) {
        this.mCellPhone = mCellPhone;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public JCUser(long mUserId) {
        this(mUserId, null, null, null);
    }

    public JCUser(String account, int accounttype, long mUserId, String nickname) {
        this.mAccount = account;
        this.mAccountType = accounttype;
        this.mUserId = mUserId;
        this.mNickName = nickname;
    }

    public JCUser(long mUserId, String nickName) {
        this(mUserId, nickName, null, null);
    }

    private JCUser(long mUserId, String name, String email, String signature) {
        this.mUserId = mUserId;
        this.mNickName = name;
        //通讯录联系人的姓名
        this.mName = name;
        this.mEmail = email;
        this.mSignature = signature;
        mBelongsJCGroup = new CopyOnWriteArraySet<JCGroup>();
        isCurrentLoggedInUser = false;
        this.mStatus = Status.OFFLINE;
        initAbbr();
        this.isFromService = false;
    }

    private void initAbbr() {
        abbra = "";
        if (this.mNickName != null) {
            format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            char[] cs = this.mNickName.toCharArray();
            for (char c : cs) {
                try {
                    String[] ars = PinyinHelper.toHanyuPinyinStringArray(c, format);
                    if (ars != null && ars.length > 0) {
                        abbra += ars[0].charAt(0);
                    }
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            }
            if (abbra.equals("")) {
                abbra = this.mNickName.toLowerCase(Locale.getDefault());
            }
        }
    }

    public boolean isCurrentLoggedInUser() {
        return isCurrentLoggedInUser;
    }

    public void setCurrentLoggedInUser(boolean isCurrentLoggedInUser) {
        this.isCurrentLoggedInUser = isCurrentLoggedInUser;
    }

    public long getmUserId() {
        return mUserId;
    }

    public void setmUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public JCNetworkStateCode getmResult() {
        return mResult;
    }

    public void setmResult(JCNetworkStateCode mResult) {
        this.mResult = mResult;
    }

    public String getNickName() {
        return mNickName == null ? getName() : mNickName;
    }

    public String getDisplayName() {
//        boolean isFriend = GlobalHolder.getInstance().isFriend(this);
//        if (isFriend) {
//            if (mCommentName != null && !TextUtils.isEmpty(mCommentName)) {
//                Log.e( "UsergetDisplayName: ",isFriend+" mCommentName: "+mCommentName );
//                return mCommentName;
//            }
//            if (mNickName != null && !TextUtils.isEmpty(mNickName)) {
//                Log.e( "UsergetDisplayName: ",isFriend+" mNickName: "+mNickName );
//                return mNickName;
//            }
//            if (mName != null && !TextUtils.isEmpty(mName)) {
//                Log.e( "UsergetDisplayName: ",isFriend+" mName: "+mName );
//                return mName;
//            }
//            Log.e("UsergetDisplayName: ","未知用户 "+isFriend );
//            return "未知用户";
//        } else {
            if (mNickName != null && !TextUtils.isEmpty(mNickName)) {
                Log.e( "UsergetDisplayName: "," mNickName: "+mNickName );
                return mNickName;
            }
            if (mName != null && !TextUtils.isEmpty(mName)) {
                Log.e( "UsergetDisplayName: "," mName: "+mName );
                return mName;
            }
            if (mCommentName != null && !TextUtils.isEmpty(mCommentName)) {
                Log.e( "UsergetDisplayName: "," mCommentName: "+mCommentName );
                return mCommentName;
            }
            return "未知用户";
//        }

    }

    public void setNickName(String nickName) {
        this.mNickName = nickName;
        initAbbr();
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setEmail(String mail) {
        this.mEmail = mail;
    }

    public String getSignature() {
        return mSignature;
    }

    public DeviceType getDeviceType() {
        return mType;
    }

    public void setDeviceType(DeviceType type) {
        this.mType = type;
    }

    public void setSignature(String signature) {
        this.mSignature = signature;
    }

    public Set<JCGroup> getBelongsGroup() {
        return mBelongsJCGroup;
    }

    public void setmBelongsJCGroup(Set<JCGroup> belongsJCGroup) {
        this.mBelongsJCGroup = belongsJCGroup;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mCellPhone) {
        this.mMobile = mCellPhone;
    }

    public String getmAvatarLocation() {
        return mAvatarLocation;
    }

    public void setmAvatarLocation(String mAvatarLocation) {
        this.mAvatarLocation = mAvatarLocation;
    }

    public String getCompany() {
        if (TextUtils.isEmpty(mCompany)) {
            mCompany = loadCompany(this.getFirstBelongsGroup());
        }
        return mCompany;
    }


    private String loadCompany(JCGroup g) {
        if (g == null) {
            return "";
        }
        if (g.getParent() != null) {
            return loadCompany(g.getParent());
        } else {
            return g.getName();
        }
    }

    public String getDepartment() {
        // first group is real department
        JCGroup g = this.getFirstBelongsGroup();
        if (this.getFirstBelongsGroup() != null) {
            mDepartment = this.getFirstBelongsGroup().getName();
        }
        if (g != null) {
            if (g.getParent() == null) {
                mDepartment = "";
            } else {
                mDepartment = g.getName();
            }
        }
        return mDepartment;
    }

    public void setDepartment(String mDepartment) {
        this.mDepartment = mDepartment;
    }

    public String getSex() {
        return mSex;
    }

    public void setSex(String mGender) {
        this.mSex = mGender;
    }

    public Date getBirthday() {
        return mBirthday;
    }

    public String getBirthdayStr() {
        if (mBirthday != null) {
            DateFormat sd = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            return sd.format(mBirthday);
        } else {
            return mStringBirthday;
        }
    }

    public String getmStringBirthday() {
        return mStringBirthday;
    }

    public void setmStringBirthday(String mStringBirthday) {
        this.mStringBirthday = mStringBirthday;
    }

    public void setBirthday(Date mBirthday) {
        this.mBirthday = mBirthday;
    }

    public String getTelephone() {
        return mTelephone;
    }

    public void setTelephone(String mTelephone) {
        this.mTelephone = mTelephone;
    }

    public String getJob() {
        return mJob;
    }

    public void setJob(String mJob) {
        this.mJob = mJob;
    }

    public Status getmStatus() {
        return mStatus;
    }

    public String getArra() {
        return this.abbra;
    }

    public void updateStatus(Status mStatus) {
        this.mStatus = mStatus;
    }

    public String getFax() {
        return this.mFax;
    }

    public void setFax(String fax) {
        this.mFax = fax;
    }

    public int getAuthtype() {
        return this.mAuthtype;
    }

    public void setAuthtype(int authtype) {
        this.mAuthtype = authtype;
    }

    public String getAccount() {
        return this.mAccount;
    }

    public void setAccount(String acc) {
        this.mAccount = acc;
    }

    public String getAvatarPath() {
        return mAvatarPath;
    }

    public void setAvatarPath(String mAvatarPath) {
        this.mAvatarPath = mAvatarPath;
    }

    public void addUserToGroup(JCGroup g) {
        if (this.mBelongsJCGroup == null || g == null) {
            LogUtil.e(" group is null , can't add user to this group");
            return;
        }
        this.mBelongsJCGroup.add(g);
    }

    public void removeUserFromGroup(JCGroup g) {
        if (this.mBelongsJCGroup == null || g == null) {
            LogUtil.e(" group is null , can't remove user to this group");
            return;
        }
        this.mBelongsJCGroup.remove(g);
    }

    public String getCommentName() {
        return mCommentName;
    }

    public void setCommentName(String comentName) {
        this.mCommentName = comentName;
    }

    public boolean isFromService() {
        return isFromService;
    }

    public void setFromService(boolean isFromService) {
        this.isFromService = isFromService;
    }

    public int getAccountType() {
        return mAccountType;
    }

    public void setAccountType(int mAccountType) {
        this.mAccountType = mAccountType;
    }

    public JCGroup getFirstBelongsGroup() {
        if (this.mBelongsJCGroup.size() > 0) {
            for (JCGroup g : mBelongsJCGroup) {
                if (g.getGroupType() == JCGroupType.GROUP_TYPE_DEPARTMENT||g.getGroupType()== JCGroupType.GROUP_TYPE_CONTACT) {
                    return g;
                }
            }
        }
        return null;
    }

//    public synchronized Bitmap getAvatarBitmap() {
//        if (mAccountType == GlobalConstant.ACCOUNT_TYPE_PHONE_FRIEND) {
//            return BitmapUtil.getPhoneFriendAvatar();
//        } else {
//            Bitmap avatar = GlobalHolder.getInstance().getUserAvatar(this.mUserId);
//            if (avatar == null || avatar.isRecycled()) {
//                if (mAvatarPath != null) {
//                    return loadAvatarBitmap(mAvatarPath);
//                } else {
//                    if (!TextUtils.isEmpty(mNickName) && !TextUtils.isEmpty(mAvatarLocation)) {
//                        PviewImRequest.invokeNative(PviewImRequest.NATIVE_GET_USER_AVATAR,
//                                this.mUserId, mNickName, mAvatarLocation);
//                    } else {
//                        return loadAvatarBitmap(null);
//                    }
//                }
//            }
//            return avatar;
//        }
//    }

//    private Bitmap loadAvatarBitmap(String mAvatarPath) {
//        Bitmap bitmap = BitmapUtil.loadAvatarFromPath(mAvatarPath);
//        if (bitmap != null) {
//            if (!bitmap.isRecycled()) {
//                GlobalHolder.getInstance().mAvatarBmHolder.put(mUserId, bitmap);
//                return bitmap;
//            } else
//                throw new RuntimeException(
//                        "User loadAvatarBitmap --> Loading avatar from file path faield... bitmap is recycled!");
//
//        } else
//            throw new RuntimeException(
//                    "User loadAvatarBitmap --> Loading avatar from file path faield... bitmap is null!");
//    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JCUser other = (JCUser) obj;
        if (mUserId != other.mUserId)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (mUserId ^ (mUserId >>> 32));
        return result;
    }

    @Override
    public int compareTo(JCUser another) {

        // make sure current user align first position 确保当前用户与第一个位置对齐
//        if (this.mUserId == GlobalHolder.getInstance().getCurrentUserId()) {
//            return -1;
//        }
//        if (another.getmUserId() == GlobalHolder.getInstance().getCurrentUserId()) {
//            return 1;
//        }
//
//        if (another.getmStatus() == this.mStatus) {
//            return this.abbra.compareTo(another.abbra);
//        }
//
//        if (this.mStatus == Status.ONLINE || this.mStatus == Status.LEAVE || this.mStatus == Status.DO_NOT_DISTURB
//                || this.mStatus == Status.BUSY) {
//            if (another.mStatus == Status.ONLINE || another.getmStatus() == Status.LEAVE
//                    || another.getmStatus() == Status.DO_NOT_DISTURB || another.getmStatus() == Status.BUSY) {
//                return this.abbra.compareTo(another.abbra);
//            } else {
//                return -1;
//            }
//        } else if (another.mStatus == Status.ONLINE || another.getmStatus() == Status.LEAVE
//                || another.getmStatus() == Status.DO_NOT_DISTURB || another.getmStatus() == Status.BUSY) {
//            return 1;
//        }
//
//        return this.abbra.compareTo(another.abbra);
        return 1;
    }

    public static JCUser fromXml(int uID, String xml) {
        String nickName = extraAttri("nickname='", "'", xml);
        String signature = extraAttri("sign='", "'", xml);
        String job = extraAttri("job='", "'", xml);
        String telephone = extraAttri("telephone='", "'", xml);
        String mobile = extraAttri("mobile='", "'", xml);
        String address = extraAttri("address='", "'", xml);
        String email = extraAttri("email='", "'", xml);
        String bir = extraAttri("birthday='", "'", xml);
        String account = extraAttri("account='", "'", xml);
        String createconf = extraAttri("createconf='", "'", xml);

        DateFormat dp = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        JCUser u = new JCUser(uID, nickName);
        u.setSignature(signature);
        u.setTitle(job);
        u.setTelephone(telephone);
        u.setCellPhone(mobile);
        u.setAddress(address);
        u.setEmail(email);
        u.setAccount(account);
        if (bir != null && bir.length() > 0) {
            try {
                u.setBirthday(dp.parse(bir));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return u;
    }

    private static String extraAttri(String startStr, String endStr, String xml) {
        int pos = xml.indexOf(startStr);
        if (pos == -1) {
            return null;
        }
        int end = xml.indexOf(endStr, pos + startStr.length());
        if (end == -1) {
            return null;
        }
        return xml.substring(pos + startStr.length(), end);
    }

//    public String toXml() {
//        DateFormat dp = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
//        String xml = "<user " + " address='"
//                + (this.getAddress() == null ? "" : EscapedCharactersProcessing.convert(this.getAddress())) + "' "
//                + "authtype='" + this.getAuthtype() + "' " + "birthday='"
//                + (this.mBirthday == null ? "" : dp.format(this.mBirthday)) + "' " + "job='"
//                + (this.getJob() == null ? "" : this.getJob()) + "' " + "mobile='"
//                + (this.getMobile() == null ? "" : EscapedCharactersProcessing.convert(this.getMobile())) + "' "
//                + "nickname='"
//                + (this.getDisplayName() == null ? "" : EscapedCharactersProcessing.convert(this.getDisplayName()))
//                + "'  " + "sex='" + (this.getSex() == null ? "" : this.getSex()) + "'  " + "sign='"
//                + (this.getSignature() == null ? "" : EscapedCharactersProcessing.convert(this.getSignature())) + "' "
//                + "telephone='" + (this.getTelephone() == null ? "" : this.getTelephone()) + "'> "
//                + "<videolist/> </user> ";
//        return xml;
//    }

    /**
     * @param xml <xml><user account='wenzl1' address='地址' authtype='1'
     *            birthday='1997-12-30' bsystemavatar='1'
     *            email='youxiang@qww.com' fax='22222'
     *            homepage='http://wenzongliang.com' id='130' job='职务'
     *            mobile='18610297182' nickname='显示名称' privacy='0' sex='1'
     *            sign='签名' telephone='03702561038'/></xml>
     * @return
     */
    public static List<JCUser> paserXml(String xml) {
        List<JCUser> l = new ArrayList<JCUser>();

        InputStream is = null;

        DateFormat dp = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            is = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();
            NodeList gList = doc.getElementsByTagName("user");
            Element element;
            for (int i = 0; i < gList.getLength(); i++) {
                element = (Element) gList.item(i);
                String strId = element.getAttribute("id");
                if (strId == null || strId.isEmpty()) {
                    continue;
                }

                JCUser u = new JCUser(Long.parseLong(strId));

                u.setNickName(getAttribute(element, "nickname"));
                u.setCommentName(getAttribute(element, "commentname"));

                u.setAccount(getAttribute(element, "account"));
                u.setSignature(getAttribute(element, "sign"));
                u.setSex(getAttribute(element, "sex"));
                u.setTelephone(getAttribute(element, "telephone"));
                u.setMobile(getAttribute(element, "mobile"));

                u.setFax(getAttribute(element, "fax"));
                u.setJob(getAttribute(element, "job"));

                u.setEmail(getAttribute(element, "email"));
                u.setAddress(getAttribute(element, "address"));
                u.setmStringBirthday(getAttribute(element, "birthday"));

                String authType = getAttribute(element, "authtype");
                if (authType == null) {
                    u.setAuthtype(0);
                } else {
                    u.setAuthtype(Integer.parseInt(authType));
                }
                u.setFromService(true);
                l.add(u);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return l;
    }

    private static String getAttribute(Element el, String name) {
//        Attr atr = el.getAttributeNode(name);
//        if (atr != null) {
//            return EscapedCharactersProcessing.reverse(atr.getValue());
//        }
        return null;
    }

    public boolean isRapidInitiation() {
        return isRapidInitiation;
    }

    public void setRapidInitiation(boolean isRapidInitiation) {
        this.isRapidInitiation = isRapidInitiation;
    }

    public enum DeviceType {
        CELL_PHONE(2), PC(1), UNKNOWN(-1);
        private int code;

        private DeviceType(int code) {
            this.code = code;
        }

        public int toIntValue() {
            return code;
        }

        public static DeviceType fromInt(int type) {
            switch (type) {
                case 1:// pc
                    return PC;
                case 2:// 安卓
                case 3:// IOS
                case 4:// sip,h323
                    return CELL_PHONE;
                default:
                    return UNKNOWN;
            }
        }
    }

    public enum Status {
        LEAVE(2), BUSY(3), DO_NOT_DISTURB(4), HIDDEN(5), ONLINE(1), OFFLINE(0), UNKNOWN(-1);
        private int code;

        private Status(int code) {
            this.code = code;
        }

        public int toIntValue() {
            return code;
        }

        public static Status fromInt(int status) {
            switch (status) {
                case 0:
                    return OFFLINE;
                case 1:
                    return ONLINE;
                case 2:
                    return LEAVE;
                case 3:
                    return BUSY;
                case 4:
                    return DO_NOT_DISTURB;
                case 5:
                    return HIDDEN;
                default:
                    return UNKNOWN;
            }
        }
    }

    // 后期追加
    private boolean groupChatSelectState;

    public boolean isGroupChatSelectState() {
        return groupChatSelectState;
    }

    public void setGroupChatSelectState(boolean groupChatSelectState) {
        this.groupChatSelectState = groupChatSelectState;
    }


    /**
     * 获取可以用的name
     *
     * @return
     */
    public String getAvailableName() {

        if (mCommentName != null && !TextUtils.isEmpty(mCommentName)) {
            return mCommentName;
        }
        if (mNickName != null && !TextUtils.isEmpty(mNickName)) {
            return mNickName;
        }
        if (mName != null && !TextUtils.isEmpty(mName)) {
            return mName;
        }
        return "未知用户";

    }

    @Override
    public String toString() {
        return "User{" +
                "mAccount='" + mAccount + '\'' +
                ", mAccountType=" + mAccountType +
                ", mAddress='" + mAddress + '\'' +
                ", mAvatarPath='" + mAvatarPath + '\'' +
                ", mAvatarLocation='" + mAvatarLocation + '\'' +
                ", mAuthtype=" + mAuthtype +
                ", mBirthday=" + mBirthday +
                ", mStringBirthday='" + mStringBirthday + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mFax='" + mFax + '\'' +
                ", mUserId=" + mUserId +
                ", mJob='" + mJob + '\'' +
                ", mMobile='" + mMobile + '\'' +
                ", mName='" + mName + '\'' +
                ", mSex='" + mSex + '\'' +
                ", mSignature='" + mSignature + '\'' +
                ", mTelephone='" + mTelephone + '\'' +
                ", mCompany='" + mCompany + '\'' +
                ", mDepartment='" + mDepartment + '\'' +
                ", isCurrentLoggedInUser=" + isCurrentLoggedInUser +
                ", mResult=" + mResult +
                ", mType=" + mType +
                ", mStatus=" + mStatus +
                ", mNickName='" + mNickName + '\'' +
                ", mCommentName='" + mCommentName + '\'' +
                ", mBelongsGroup=" + mBelongsJCGroup +
                ", abbra='" + abbra + '\'' +
                ", isShowDelete=" + isShowDelete +
                ", isFromService=" + isFromService +
                ", isRapidInitiation=" + isRapidInitiation +
                ", isContain=" + isContain +
                ", mCellPhone='" + mCellPhone + '\'' +
                ", mTitle='" + mTitle + '\'' +
                ", groupChatSelectState=" + groupChatSelectState +
                '}';
    }
}

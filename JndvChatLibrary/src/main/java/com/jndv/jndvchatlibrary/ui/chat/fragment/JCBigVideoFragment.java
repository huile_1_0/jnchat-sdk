package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.jndv.jndvchatlibrary.databinding.FragmentJcbigvideoBinding;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.chat.utils.MyMediaController;

import net.util.NetworkUtil;

import java.util.HashMap;

/**
 * Author: wangguodong
 * Date: 2022/5/6
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 图片全屏查看
 */
public class JCBigVideoFragment extends JCbaseFragment {
    private FragmentJcbigvideoBinding binding;
    private String path = "";
    private String url = "";
    private int type = 0;//0=视频；1=音频
    public static final String INURL = "inurl";
    public static final String INPATH = "inpath";
    public static final String INTYPE = "intype";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentJcbigvideoBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        initView();

        return root;
    }

    private void initView() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            path = bundle.getString(INPATH);
            url = bundle.getString(INURL);
            type = bundle.getInt(INTYPE);
            Log.e("TAG", "init: ===============01=path=" + path);
        }
        if (0==type){

//            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
//            try {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.4.2; zh-CN; MW-KW-001 Build/JRO03C) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 UCBrowser/1.0.0.001 U4/0.8.0 Mobile Safari/533.1");
//                mmr.setDataSource(url, headers);
//                int width = Integer.parseInt(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));//宽
//                int height = Integer.parseInt(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));//高
//                JNLogUtil.e("==JCBigVideoFragment==mmr==width=="+width);
//                JNLogUtil.e("==JCBigVideoFragment==mmr==height=="+height);
//                Display display = getActivity().getWindowManager().getDefaultDisplay();
//                int w = display.getWidth();
//                int h = display.getHeight();
//                JNLogUtil.e("==JCBigVideoFragment==mmr==W=="+w);
//                JNLogUtil.e("==JCBigVideoFragment==mmr==h=="+h);

//                int newH = w*height/width;
//                int newW = w;
//                if (newH>h){
//                    newH = h;
////                    newW = h*width/height;
//                }
//                ViewGroup.LayoutParams layoutParams = binding.vvMsgVideo.getLayoutParams();
//                layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
//                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                binding.vvMsgVideo.setLayoutParams(layoutParams);
//            }catch (Exception e){
//                JNLogUtil.e("",e);
//            }
//            try{
//                mmr.release();
//            }catch (Exception e){
//                JNLogUtil.e("",e);
//            }

        }else {
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            int w = display.getWidth();
            ViewGroup.LayoutParams layoutParams = binding.vvMsgVideo.getLayoutParams();
            layoutParams.height = 9*w/16;
            layoutParams.width = w;
            binding.vvMsgVideo.setLayoutParams(layoutParams);
            binding.ivChangpian.setVisibility(View.VISIBLE);
        }
        init();
    }
    private void init() {

        MyMediaController myMediaController = new MyMediaController(getActivity());
        //设置视频控制器
        binding.vvMsgVideo.setMediaController(myMediaController);

        //播放完成回调
        binding.vvMsgVideo.setOnCompletionListener(new MyPlayerOnCompletionListener());

        if (NetworkUtil.urlIsConnect(url)) {
            binding.vvMsgVideo.setVideoURI(Uri.parse(url));
        } else {
            binding.vvMsgVideo.setVideoPath(path);
        }

        binding.vvMsgVideo.seekTo(1);
        myMediaController.show(0);
//        binding.vvMsgVideo.start();
        binding.vvMsgVideo.setOnCompletionListener(mp -> binding.vvMsgVideo.seekTo(1));
        //only this showed the controller for me!!
        binding.vvMsgVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
//                mVideoView.start();
                myMediaController.show(0);
            }
        });
    }

    class MyPlayerOnCompletionListener implements MediaPlayer.OnCompletionListener {

        @Override
        public void onCompletion(MediaPlayer mp) {
            Toast.makeText(getActivity(), "播放完成了", Toast.LENGTH_SHORT).show();
        }
    }


}

package com.jndv.jndvchatlibrary.ui.crowd.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.zxing.client.android.utils.ZXingUtils;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCMsgNotifyListFragment;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCCrowdBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCGroupAllBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCGroupNumbersBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCQrResult;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.entity.JCUser;
import com.jndv.jndvchatlibrary.ui.crowd.utils.CommonUtil;
import com.jndv.jndvchatlibrary.ui.crowd.utils.DialogManager;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCCrowdViewModel;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jndvchatlibrary.utils.JCQRcodeBeanBase;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 群详情页面
 */
public class JCCrowdDetailActivity extends JCBaseHeadActivity implements View.OnClickListener {
//
    private static final String TAG = "CrowdDetailActivity";
    private String RULE_ALLOW_ALL = "0";
    private String RULE_QUALIFICATION = "1";

    private int TYPE_BRIEF = 1;
    private int TYPE_UPDATE_MEMBERS = 3;

    private final static int REQUEST_UPDATE_CROWD_DONE = 1;
    private final static int REQUEST_QUIT_CROWD_DONE = 2;

    private int pos;
    private List<JCCrowdBean> datas; // 所有群数据
    private JCCrowdBean JCCrowdBean; // 本群数据
    private List<JCUser> JCUserList; //本群所有用户
    private List<JCGroupNumbersBean> gridJCUserList = new ArrayList<>(); //本群所有用户
    private String groupId =""; // 本群id
    private TextView mNameTV; // 顶部群名称
    private LinearLayout mBriefTV; // 群简介
    private TextView crowd_detail_brief; // 群简介
    private GridView crowd_detail_gv;
    private TextView crowd_detail_tv_btn_all; // 查看全部成员
    private TextView crowd_detail_radio_text; // 非群主看到的，替换的RadioGroup
    private RadioGroup mAdminBox; // 群组看到的下面多选框
    private TextView mQuitButton; // 最下面按钮（解散该群，退出该群）
    private RelativeLayout tvJoin;
    private CrowdGridAdapter crowdGridAdapter; // Grid
    private Dialog mDialog;
    private JCCrowdViewModel jcCrowdViewModel;
    private ActivityResultLauncher<Intent> intentActivityResultLauncher;
    private String identityPermission;
    private String invite_permission;
    private LadingTicketDialog ladingDialog;

    private String group_domain_addr="";
    private String group_domain_addr_user;
    private int session_id;
    private RelativeLayout rl_show_qr_dialog;

//    private CrowdGroupRequest service = new CrowdGroupRequest();

    private State mState = State.NONE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crowd_detail_new);
        initIntent();
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void codeEventD(JNCodeEvent<JCimMessageBean> codeEvent) {
        switch (codeEvent.getCode()) {
            case JNEventBusType
                    .CODE_MESSAGE_GROUP_INFO_UPDATE://
                if (TextUtils.equals("" + groupId, codeEvent.getData().getSessionId())
                        && TextUtils.equals(group_domain_addr, codeEvent.getData().getSessionRealm())) {
                    if (null != jcCrowdViewModel)
                        jcCrowdViewModel.getGroupAll(groupId, group_domain_addr);
                }

                break;
            case JNEventBusType
                    .CODE_MESSAGE_GROUP_NOTICE_SEND://
                if (TextUtils.equals("" + groupId, codeEvent.getData().getSessionId())
                        && TextUtils.equals(group_domain_addr, codeEvent.getData().getSessionRealm())) {
                    if (null != jcCrowdViewModel) jcCrowdViewModel.getGroupNotice(groupId);
                }
                break;
        }
    }

    private void initData() {
        jcCrowdViewModel = new ViewModelProvider(this).get(JCCrowdViewModel.class);
        jcCrowdViewModel.initData(this);
        jcCrowdViewModel.getGroupNotice(groupId);
        jcCrowdViewModel.getMutableLiveData().observe(this, bean -> {
            try {
                if (bean.getData() == null || bean.getData().size() == 0) {
                    crowd_detail_brief.setText("无公告");
                } else {
                    crowd_detail_brief.setText(bean.getData().get(bean.getData().size() - 1).getContent());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
        jcCrowdViewModel.getGroupAllMutableLiveData().observe(this, bean -> {
            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, bean.getCode())) {
                try {
                    mNameTV.setText(bean.getData().getGroup_info().getCgName());
                    setCenterTitle(getString(R.string.crowd_detail_title)+"("+bean.getData().getGroup_numbers().size()+")");
                    gridJCUserList.clear();
                    gridJCUserList.addAll(bean.getData().getGroup_numbers());
                    crowdGridAdapter.notifyDataSetChanged();
                    identityPermission = bean.getData().getIdentity_permission();
                    invite_permission = bean.getData().getInvite_permission();

                    initRules(bean);
                    initUiAfterData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (TextUtils.equals("1001000000", bean.getCode())) {
                JCSessionListBean jcSessionListBean = JCSessionUtil.getJCSessionListBean(session_id);
                if (jcSessionListBean!=null) {
                    jcSessionListBean.setStatus(2);
                    JCobjectBox.get().boxFor(JCSessionListBean.class).put(jcSessionListBean);
                }
                CommonUtil.showToast(JCCrowdDetailActivity.this, "已退出群聊！");
                finish();
            }
        });
        jcCrowdViewModel.getGroupAll(groupId, group_domain_addr);
    }
    private void initUiAfterData() {
        // 判断是否是群主
        if (isCreator()) { // 群主
            mAdminBox.setVisibility(View.VISIBLE);
            crowd_detail_radio_text.setVisibility(View.GONE);
            mQuitButton.setText(R.string.crowd_detail_qulification_dismiss_button);
            tvJoin.setVisibility(View.VISIBLE);
        } else { // 群员
            mAdminBox.setVisibility(View.GONE);
            crowd_detail_radio_text.setVisibility(View.VISIBLE);
            mQuitButton.setText(R.string.crowd_detail_qulification_quit_button);
            tvJoin.setVisibility(View.GONE);
        }
    }

    private void initIntent() {
        Intent intent = getIntent();
        if(!TextUtils.isEmpty(intent.getStringExtra(JCEntityUtils.CROWD_ID))){
            groupId = intent.getStringExtra(JCEntityUtils.CROWD_ID);
        }
        Log.d(TAG,"groupId=="+groupId);
        session_id = intent.getIntExtra(JCEntityUtils.SESSION_ID, 0);
        group_domain_addr = intent.getStringExtra(JCEntityUtils.GROUP_DOMAIN_ADDR);
        group_domain_addr_user = intent.getStringExtra(JCEntityUtils.GROUP_DOMAIN_ADDR_USER);
        initState();
        intentActivityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                //此处是跳转的result回调方法
                if (result.getData() != null && result.getResultCode() == Activity.RESULT_OK) {
//                        result.getData().getStringExtra(NewWordActivity.EXTRA_REPLY);
                }
            }
        });
    }

    private void initState() {
        JCSessionListBean jcSessionListBean = JCSessionUtil.getJCSessionListBean(session_id);
        if (null == jcSessionListBean) return;
        if (1 == jcSessionListBean.getStatus()) {
            JCChatManager.showToast("群聊已解散!");
            finish();
        } else if (2 == jcSessionListBean.getStatus()) {
            JCChatManager.showToast("已退出群聊!");
            finish();
        } else if (3 == jcSessionListBean.getStatus()) {
            JCChatManager.showToast("被踢出群聊!");
            finish();
        }
    }

    private void initView() {
        setCenterTitle(getString(R.string.crowd_detail_title));
        moreRight.setVisibility(View.GONE);
        moreRight.setOnClickListener(this);
        mNameTV = findViewById(R.id.crowd_detail_name);
        crowd_detail_brief = findViewById(R.id.crowd_detail_brief);
        mBriefTV = findViewById(R.id.crowd_detail_brief_button);
        mBriefTV.setOnClickListener(this);

        crowd_detail_tv_btn_all = findViewById(R.id.crowd_detail_tv_btn_all);
        crowd_detail_tv_btn_all.setOnClickListener(this);
        crowd_detail_radio_text = findViewById(R.id.crowd_detail_radio_text);

        rl_show_qr_dialog = findViewById(R.id.rl_show_qr_dialog);
        rl_show_qr_dialog.setOnClickListener(this);


        mAdminBox = findViewById(R.id.crowd_detail_radio_group);
        mQuitButton = findViewById(R.id.crowd_detail_button_text);
        tvJoin = findViewById(R.id.tv_join);
        mQuitButton.setOnClickListener(this);
        tvJoin.setOnClickListener(this);
        crowd_detail_gv = findViewById(R.id.crowd_detail_gv);
        crowdGridAdapter = new CrowdGridAdapter();
        crowd_detail_gv.setAdapter(crowdGridAdapter);
        crowd_detail_gv.setOnItemClickListener((parent, view, position, id) -> {
            // TODO: 2022/2/18 邀请入群模式
            if (position == gridJCUserList.size()) {
                //type (默认0普通成员，1.管理员 2.群主)
                //群主邀请改为所有人邀请，判断邀请权限
                if (hasInvitePermission()) {
                    Intent addFriendIntent = new Intent(mContext, JCCrowdNumbersInviteActivity.class);
                    addFriendIntent.putExtra(JCEntityUtils.CROWD_ID, groupId);
                    addFriendIntent.putExtra(JCEntityUtils.GROUP_NUMBERS, getNumbersStr());
                    addFriendIntent.putExtra(JCEntityUtils.GROUP_DOMAIN_ADDR, group_domain_addr);
                    addFriendIntent.putExtra(JCEntityUtils.GROUP_DOMAIN_ADDR_USER, group_domain_addr_user);

                    startActivity(addFriendIntent);
                } else {
                    Toast.makeText(JCCrowdDetailActivity.this, "当前用户没有邀请权限！请联系群主或管理员", Toast.LENGTH_SHORT).show();
                }

            } else {

                JCCrowdUserDetailActivity.start(mContext, gridJCUserList.get(position).getUser_id(), groupId, gridJCUserList.get(position).getDomain_addr(), group_domain_addr, isCreator() ? 2 : 0,gridJCUserList.get(position).getInvite_permission());

            }
        });

        mAdminBox.setOnCheckedChangeListener((group, checkedId) -> {
            Log.e("mAdminBox",checkedId+"++++");
            String renzheng="0";
            if (checkedId == R.id.radio0) {
                renzheng="0";
            } else if (checkedId == R.id.radio1) {
                renzheng="1";
            }

            if (null!=jcCrowdViewModel)jcCrowdViewModel.updateRenZheng(group_domain_addr, groupId, renzheng);
        });

    }

    private boolean isCreator() {
        if (!TextUtils.isEmpty(identityPermission)) {
            if (identityPermission.equals("0")) {
                return false;
            } else if (identityPermission.equals("2")) {
                return true;
            }
        }
        return false;
    }

    //是否有邀请权限
    private boolean hasInvitePermission() {
        if (!TextUtils.isEmpty(invite_permission)) {
            if (invite_permission.equals("0")) {
                return false;
            } else if (invite_permission.equals("1")) {
                return true;
            }
        }
        return false;
    }

    /**
     * 入群规则
     */
    private void initRules(JCRespondBean<JCGroupAllBean> bean) {
        for (int i = 0; i < mAdminBox.getChildCount(); i++) {
            if (mAdminBox.getChildAt(i) instanceof RadioButton) {
                RadioButton rb = (RadioButton) mAdminBox.getChildAt(i);
                if (rb.getTag().equals(RULE_ALLOW_ALL) && bean.getData().getGroup_info().getRenzheng() == 0) {
                    mAdminBox.check(rb.getId());
                    crowd_detail_radio_text.setText(getString(R.string.crowd_rules_item_allows_anybody));
                } else if (rb.getTag().equals(RULE_QUALIFICATION) && bean.getData().getGroup_info().getRenzheng() == 1) {
                    mAdminBox.check(rb.getId());
                    crowd_detail_radio_text.setText(getString(R.string.crowd_rules_item_allows_need_authentication));
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TYPE_BRIEF) {
            //更新简介
        }
        if (requestCode == TYPE_UPDATE_MEMBERS) {
//            mMembersCountsTV.setText(crowdBean.getUserList().size() + "");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogManager.getInstance().clearDialogObject();
//        service.clearCalledBack();
    }


    @Override
    public void receiveBroadcast(Intent intent) {
        String action = intent.getAction();
        if (action == null)
            return;
        switch (action) {
            case GlobalConstant.JNI_BROADCAST_GROUP_USER_ADDED: // 群成员增加回调
            case GlobalConstant.JNI_BROADCAST_GROUP_USER_REMOVED: // 组成员退出回调
                // TODO: 2022/2/18 组成员退出
//                GroupUserObject obj = intent.getParcelableExtra("obj");
//                if (obj.getmType() == GroupType.GROUP_TYPE_CROWD) {
//                    datas = GlobalHolder.getInstance().getCrowdBeanList();
//                    if (isBelongs(crowdId)) {
//                        crowdBean = datas.get(pos);
//                    }
//                    userList = crowdBean.getUserList();
//                    gridUserList = new ArrayList<>();
//                    for (int i = 0; i < (userList.size() > 4 ? 4 : userList.size()); i++) {
//                        gridUserList.add(userList.get(i));
//                    }
//
//                    gridUserList.add(new User(0, ""));
//                    crowd_detail_tv_headcount.setText("总人数：" + userList.size());
//                    crowdGridAdapter.notifyDataSetChanged();
//                }
                break;
            case GlobalConstant.JNI_BROADCAST_KICED_CROWD:
            case GlobalConstant.BROADCAST_GROUP_DELETED_NOTIFICATION: // 群解散广播
                // TODO: 2022/2/18 群解散
//                GroupUserObject obj1 = intent.getParcelableExtra("group");
//                if (obj1 == null) {
//                    LogUtil.e("CrowdDetailActivity", "Received the broadcast to quit the crowd group , but crowd id is wroing... ");
//                    return;
//                }
//                if (obj1.getmGroupId() == crowdId) {
//                    finish();
//                    boolean isUser = crowdBean.getCreatoruserid() == GlobalHolder.getInstance().getCurrentUserId();
//
//                    showToast(isUser ? "群已被解散" : "退出群成功");
//                }
                break;

            case GlobalConstant.BROADCAST_USER_COMMENT_NAME_NOTIFICATION:

                long uid = intent.getLongExtra("modifiedUser", -1);
                updateUser(uid);
                break;

            case GlobalConstant.JNI_BROADCAST_GROUP_UPDATED:
                // TODO: 2022/2/18 修改群资料
//                LogUtil.e("修改群资料。。。。。");
//                long tempCrowdId = intent.getLongExtra("gid", 0);
//                // Update content
//                if (tempCrowdId == crowdId) {
//                    crowdBean = GlobalHolder.getInstance().getCrowdBean(tempCrowdId);
//                    if (crowdBean != null) {
//                        initRules();
//                        crowd_detail_brief.setText(crowdBean.getSummary());
//                        crowd_detail_tv_headcount.setText("总人数：" + crowdBean.getUserList().size());
//                        mNameTV.setText(crowdBean.getName());
//                    }
//                }

                break;
        }
    }

    private void showDialog() {
        mDialog = DialogManager
                .getInstance()
                .showNoTitleDialog(
                        DialogManager.getInstance().new DialogInterface(
                                mContext,
                                null,
                                getText(R.string.crowd_detail_quit_confirm_title),
                                getText(R.string.activiy_contact_group_button_confirm),
                                getText(R.string.activiy_contact_group_button_cancel)) {

                            @Override
                            public void confirmCallBack() {
                                if (isCreator()) {
                                    jcCrowdViewModel.disbandGroup(groupId, session_id);//解散
                                } else {
                                    jcCrowdViewModel.quitGroupChat(groupId, group_domain_addr, session_id);//退出
                                }
                            }

                            @Override
                            public void cannelCallBack() {
                                mDialog.dismiss();
                            }
                        });
        // TODO: 2022/2/18 判断是否是群主后设置解散/退出群确认框
        if (isCreator()) {
            DialogManager.getInstance()
                    .setDialogContent(getText(R.string.crowd_detail_dismiss_confirm_title));
        } else {
            DialogManager.getInstance().setDialogContent(getText(R.string.crowd_detail_quit_confirm_title));
        }
        mDialog.show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.crowd_detail_tv_btn_all) { // 查看全部成员
            // TODO: 2022/2/18 查看全部成员
            Intent intent = new Intent(mContext, JCCrowdMembersActivity.class);
            intent.putExtra(JCEntityUtils.CROWD_ID, groupId);
            intent.putExtra(JCEntityUtils.IS_FROM_CROWD, isCreator() ? 2 : 0);
            intent.putExtra(JCEntityUtils.GROUP_DOMAIN_ADDR, group_domain_addr);
            startActivity(intent);
        } else if (id == R.id.crowd_detail_button_text) { // 退出解散
            // TODO: 2022/2/18 退出解散
            showDialog();
        } else if (id == R.id.crowd_detail_brief_button) { // 群简介
            // TODO: 2022/2/18 群简介
//            int type = 0;
//            if (v == mBriefTV) {
//                type = TYPE_BRIEF;
//            }
//            Intent i = new Intent(mContext, CrowdContentUpdateActivity.class);
//            i.putExtra("type", 1);
//            i.putExtra("cid", crowdBean.getId());
//            startActivityForResult(i, type);

            Intent intent = new Intent(this, JCCrowdContentUpdateActivity.class);
            intent.putExtra("isCreator", isCreator());
            intent.putExtra("content", crowd_detail_brief.getText().toString());
            intent.putExtra(JCEntityUtils.CROWD_ID, groupId);
            intentActivityResultLauncher.launch(intent);

        }else if (id==R.id.tv_join){
            //跳转到新用户进群审核列表页面

            JCMsgNotifyListFragment listFragment=new JCMsgNotifyListFragment();
            Bundle bundle=new Bundle();
            bundle.putInt("flag",2);
            bundle.putString("groupId",groupId);
            bundle.putString("groupDomainAddr",group_domain_addr);
            bundle.putString("groupDomainAddrUser",group_domain_addr_user);
            JCbaseFragmentActivity.start(this,listFragment,"通知列表",bundle);
        }else if (id==R.id.menu || id==R.id.rl_show_qr_dialog){
            //群二维码生成
            JCQrResult result = new JCQrResult(groupId, group_domain_addr, group_domain_addr_user);
            JCQRcodeBeanBase beanBase = new JCQRcodeBeanBase(JCQRcodeBeanBase.QRCODE_TYPE_CROWD, new Gson().toJson(result));
            Bitmap qrBitmap= ZXingUtils.createQRCodeImage(new Gson().toJson(beanBase));
            ladingDialog = new LadingTicketDialog(this, qrBitmap, "邀请码内容", new Gson().toJson(beanBase), "");
            ladingDialog.show();
        }
    }

    enum State {
        NONE, PENDING
    }

    @SuppressLint("HandlerLeak")
    private Handler mLocalHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REQUEST_UPDATE_CROWD_DONE:
                    synchronized (mState) {
                        mState = State.NONE;
                    }
                    break;
                case REQUEST_QUIT_CROWD_DONE:
                    handleQuitDone();
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                    break;

            }
        }

    };

    private void handleQuitDone() {
        // TODO: 2022/2/18 删除缓存的群信息
        // Remove cache crowd
//        GlobalHolder.getInstance().removeCrowdBean(crowdBean.getId());
        finish();
    }


    private class CrowdGridAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return gridJCUserList == null ? 0 : gridJCUserList.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return gridJCUserList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.crowd_detail_grid_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.imageView = convertView.findViewById(R.id.crowd_detail_grid_item_iv);
                viewHolder.textView = convertView.findViewById(R.id.crowd_detail_grid_item_tv);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            if (position == gridJCUserList.size()) {
                viewHolder.textView.setText("邀请");
                if (!CommonUtil.isDestroy((Activity) mContext)) {
                    Glide.with(mContext).load(R.drawable.crowd_add_friend).into(viewHolder.imageView);
                }
            } else {
                String name = gridJCUserList.get(position).getNickname();
                if (TextUtils.isEmpty(name)) {
                    name = gridJCUserList.get(position).getUser_id();
//                    viewHolder.textView.setText(gridJCUserList.get(position).getNickname());
                }
                viewHolder.textView.setText(name);
                if (!CommonUtil.isDestroy((Activity) mContext)) {
//                        JCglideUtils.loadCircleImage(mContext, gridJCUserList.get(position).getAvatar(), viewHolder.imageView);
                        JCglideUtils.loadOriginHeadImage(mContext, gridJCUserList.get(position).getAvatar(), viewHolder.imageView);
                    // TODO: 2022/2/18 加载头像

                }

            }
            return convertView;
        }

        private class ViewHolder {
            TextView textView;
            ImageView imageView;
        }
    }


    //用户昵称修改，监听更新
    private void updateUser(long userId) {
        // TODO: 2022/2/18 昵称更改
//        if (userId == -1l) {
//            LogUtil.e("ContactsTabFragment BROADCAST_USER_COMMENT_NAME_NOTIFICATION ---> update user comment name failed , get id is -1");
//            return;
//        }
//        User u = GlobalHolder.getInstance().getUser(userId);
//        for (int i = 0; i < userList.size(); i++) {
//            if (u.getmUserId() == userList.get(i).getmUserId()) {
//                if (u.getNickName() != null) {
//                    GlobalHolder.getInstance().getCrowdBeanList().get(pos).getUserList().get(i).setNickName(u.getNickName());
//                    userList.get(i).setNickName(u.getNickName());
//
//                }
//            }
//        }
//
//        gridUserList = new ArrayList<>();
//        for (int i = 0; i < (userList.size() > 4 ? 4 : userList.size()); i++) {
//            gridUserList.add(userList.get(i));
//        }
//
//        gridUserList.add(new User(0, ""));
//        crowdGridAdapter.notifyDataSetChanged();

    }


    public String[] getNumbersStr() {
        String[] numbers = new String[gridJCUserList.size()];
        for (int i = 0; i < gridJCUserList.size(); i++) {
            numbers[i] = gridJCUserList.get(i).getUser_id();

        }

        return numbers;
    }

}

package com.jndv.jndvchatlibrary.ui.chat.viewholder.collect;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.http.api.JCGetCollectMessageApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgVideoContentBean;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jndvchatlibrary.utils.JCFileUtils;

/**
 * Author: wangguodong
 * Date: 2022/6/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 收藏消息，viewHolder实体
 */
public class JCCollectMsgVideoViewHolder extends JCCollectMsgBaseViewHolder{
    private ImageView iv_video_background;
    int w = 240;
    int h = 240 ;
    public JCCollectMsgVideoViewHolder(Context context, View itemView) {
        super(context, itemView);
    }

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_video;
    }

    @Override
    protected void showContentView(JCGetCollectMessageApi.ColloctBean colloctBean) {
        iv_video_background = findViewById(R.id.iv_video_background);
        try {
            JCbaseIMMessage message = new Gson().fromJson(colloctBean.getBody(), JCimMessageBean.class);
            JCMsgVideoContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(message.getContent()), JCMsgVideoContentBean.class);
            JNLogUtil.e("======SendTime==" + message.getSendTime());
            JNLogUtil.e("======ReceiveTime==" + message.getReceiveTime());
//            tvMessage.setText(bean.getTime());
            JCThreadManager.getIO().execute(new Runnable() {
                @Override
                public void run() {
                    if (isReceivedMessage(message)){
                        Bitmap urlThumbnail = JCFileUtils.getNetVideoBitmap(bean.getUrl());
                        if (urlThumbnail == null) {
                            Bitmap pathThumbnail = JCFileUtils.getLocalVideoBitmap(bean.getPath());
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    if (pathThumbnail != null) {
                                        initWH(pathThumbnail.getWidth(), pathThumbnail.getHeight());
                                        iv_video_background.setImageBitmap(pathThumbnail);
                                    }else {
                                        initWH(240, 240);
                                        iv_video_background.setBackgroundResource(R.drawable.jc_loading_error);
                                    }
                                }
                            });
                        } else {
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    initWH(urlThumbnail.getWidth(), urlThumbnail.getHeight());
                                    iv_video_background.setImageBitmap(urlThumbnail);
                                }
                            });
                        }
                    }else {
                        Bitmap pathThumbnail = JCFileUtils.getLocalVideoBitmap(bean.getPath());
                        if (pathThumbnail != null) {
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    initWH(pathThumbnail.getWidth(), pathThumbnail.getHeight());
                                    iv_video_background.setImageBitmap(pathThumbnail);
                                }
                            });
                        }else {
                            Bitmap urlThumbnail = JCFileUtils.getNetVideoBitmap(bean.getUrl());
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    if (null!=urlThumbnail){
                                        initWH(urlThumbnail.getWidth(), urlThumbnail.getHeight());
                                        iv_video_background.setImageBitmap(urlThumbnail);
                                    }else {
                                        initWH(240, 240);
                                        iv_video_background.setBackgroundResource(R.drawable.jc_loading_error);
                                    }
                                }
                            });
                        }
                    }
                }
            });

        }catch (Exception e){
            JNLogUtil.e("==JCCollectMsgTextViewHolder==showContentView==", e);
//            tvMessage.setText("未识别的消息");
        }
    }

    private void initWH(int width, int height){
        if (width>0&&height>0){
            if (width>240){
                h = 240*height/width;
            }
            if (h<240){
                h=240;
                w = 240*width/height;
            }
        }
        setLayoutParams(w,h,iv_video_background);
    }

}

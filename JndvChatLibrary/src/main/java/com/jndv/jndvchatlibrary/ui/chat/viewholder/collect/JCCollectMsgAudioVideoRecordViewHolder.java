package com.jndv.jndvchatlibrary.ui.chat.viewholder.collect;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.http.api.JCGetCollectMessageApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgTextContentBean;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;

/**
 * Author: wangguodong
 * Date: 2022/6/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 收藏消息，viewHolder实体
 */
public class JCCollectMsgAudioVideoRecordViewHolder extends JCCollectMsgBaseViewHolder{
    private TextView tvMessage;
    public JCCollectMsgAudioVideoRecordViewHolder(Context context, View itemView) {
        super(context, itemView);
    }

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_text;
    }

    @Override
    protected void showContentView(JCGetCollectMessageApi.ColloctBean colloctBean) {
        JNLogUtil.e("===JCCollectMsgTextViewHolder===showContentView====");
        tvMessage = findViewById(R.id.tv_msg_text);
        try {
            JCbaseIMMessage message = new Gson().fromJson(colloctBean.getBody(), JCimMessageBean.class);
            JCMsgTextContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(message.getContent()), JCMsgTextContentBean.class);
            tvMessage.setTextColor(Color.parseColor(bean.getFontColor()));
            tvMessage.setTextSize(Float.parseFloat(""+bean.getFontSize()));
            JNLogUtil.e("======SendTime=="+message.getSendTime());
            JNLogUtil.e("======ReceiveTime=="+message.getReceiveTime());
            tvMessage.setText("通话时长："+message.getContent());
        }catch (Exception e){
            JNLogUtil.e("==JCCollectMsgTextViewHolder==showContentView==", e);
            tvMessage.setText("未识别的消息");
        }
    }
}

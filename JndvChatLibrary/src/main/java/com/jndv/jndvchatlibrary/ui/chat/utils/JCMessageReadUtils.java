package com.jndv.jndvchatlibrary.ui.chat.utils;

import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCMessageReadUserApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jnbaseutils.chat.JCSessionType;
import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/8/4
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 消息已读数量处理工具
 */
public class JCMessageReadUtils {

    public interface JCMessageReadListenner{
        void onUpdateMessage(JCbaseIMMessage message);
    }

    public static void updateGroupMsgReadAll(LifecycleOwner lifecycleOwner, JCbaseIMMessage message, JCMessageReadListenner listenner){
        JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======01============");
        if (message.getRead_num()!=-1 && TextUtils.equals(message.getSessionType(), ""+JCSessionType.CHAT_GROUP)){
//          群聊中，消息需要已读的群成员总数量，发消息之后加入群聊的群成员不需要计算在已读群成员总量。总量<=0是未获取这个总量信息。
            JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======02============");
            if (message.getReadNumAll()<=0){
                JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======03============");
                Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
                JCimMessageBean localMessage = builder
                        .equal(JCimMessageBean_.msgID, message.getMsgID(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                        .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                        .build().findFirst();
                if (null!=localMessage){
                    JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======04============");
                    if (localMessage.getReadNumAll()<=0){
                        //查询
                        JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======05============");
                        getMessageReadUser(lifecycleOwner, localMessage, listenner);
                    }
                    JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======06============");
                }else{//本地数据库没有此消息，那就去查询需要已读的群成员数量
                    JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======07============");
                    getMessageReadUser(lifecycleOwner, (JCimMessageBean) message, listenner);
                }
            }
        }
    }

    public static void getMessageReadUser(LifecycleOwner lifecycleOwner, JCimMessageBean message, JCMessageReadListenner listenner){
        EasyHttp.get(lifecycleOwner)
                .api(new JCMessageReadUserApi()
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setGroupMsgUuid(message.getMsgID())
                )
                .request(new OnHttpListener<JCMessageReadUserApi.Bean>() {
                    @Override
                    public void onSucceed(JCMessageReadUserApi.Bean result) {
                        try {
                            JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======08============");
                            if (null!=result && null!=result.getData()){
                                JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======09============");
                                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                    JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======10============");
                                    message.setReadNumAll(result.getData().getRead().size()+result.getData().getUnread().size());
                                    message.setSaveUserId(JNBasePreferenceSaves.getUserSipId());
                                    JCobjectBox.get().boxFor(JCimMessageBean.class).put(message);
                                }

                                if (null!=listenner){
                                    JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======11============");
                                    listenner.onUpdateMessage(message);
                                }

                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFail(Exception e) {
//                        netFail();
                        JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======12============");
                        e.printStackTrace();
                    }
                });
    }


}

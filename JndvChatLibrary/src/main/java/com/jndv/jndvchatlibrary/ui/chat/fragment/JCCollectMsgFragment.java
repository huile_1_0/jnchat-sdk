package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJccollectmsgBinding;
import com.jndv.jndvchatlibrary.http.api.JCGetCollectMessageApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.collect.JCCollectMsgAudioVideoRecordViewHolder;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.collect.JCCollectMsgBaseViewHolder;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.collect.JCCollectMsgFileViewHolder;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.collect.JCCollectMsgImagViewHolder;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.collect.JCCollectMsgLocationViewHolder;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.collect.JCCollectMsgNameCardViewHolder;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.collect.JCCollectMsgTextViewHolder;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.collect.JCCollectMsgVideoViewHolder;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.collect.JCCollectMsgVoiceViewHolder;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 收藏的消息列表fragment
 */
public class JCCollectMsgFragment extends JCbaseFragment implements OnRefreshLoadMoreListener {

    private static final String TAG = "JCCollectMsgFragment";
    private JCCollectMsgViewModel jcCollectMsgViewModel;
    private FragmentJccollectmsgBinding binding;
    private CommonAdapter adapter ;
    private List<JCGetCollectMessageApi.ColloctBean> dataBeanList;
    private Context mContext;
    private int page = 1;

    public static JCCollectMsgFragment newInstance() {
        return new JCCollectMsgFragment();
    }
    public static JCCollectMsgFragment newInstance(JCFragmentSelect jcFragmentSelect) {
        JCCollectMsgFragment jcCollectMsgFragment = new JCCollectMsgFragment();
        jcCollectMsgFragment.setJcFragmentSelect(jcFragmentSelect);
        return jcCollectMsgFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        jcCollectMsgViewModel =
                new ViewModelProvider(this).get(JCCollectMsgViewModel.class);
        binding = FragmentJccollectmsgBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);
//        EventBus.getDefault().register(this);
//        return super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initCameraPermission();
    }

    private void init(View view) {
        jcCollectMsgViewModel.init(this);
        mContext = getContext();
        toolInitObj();
        initData();
    }

    private void initData(){
        jcCollectMsgViewModel.getCollectMsgList().observeForever(new Observer<List<JCGetCollectMessageApi.ColloctBean>>() {
            @Override
            public void onChanged(List<JCGetCollectMessageApi.ColloctBean> jCsessionListModelItem) {
                try {
                    binding.listRefresh.finishRefresh();
                    jCsessionListModelItem = sortData(jCsessionListModelItem);
                    if (null!=jCsessionListModelItem && jCsessionListModelItem.size()>0){
                        dataBeanList.clear();
                        dataBeanList.addAll(jCsessionListModelItem);
                        adapter.notifyDataSetChanged();
                    }
                }catch (Exception e){
                    JNLogUtil.e("==JCCollectMsgFragment==initData==onChanged==",e);
                }
            }
        });
    }

    //  排序
    public List<JCGetCollectMessageApi.ColloctBean> sortData(List<JCGetCollectMessageApi.ColloctBean> mList) {
        try {
            Collections.sort(mList, new Comparator<JCGetCollectMessageApi.ColloctBean>() {
                @Override
                public int compare(JCGetCollectMessageApi.ColloctBean o1, JCGetCollectMessageApi.ColloctBean o2) {
                    long time1 = Long.parseLong(o1.getCreateTime());
                    long time2 = Long.parseLong(o2.getCreateTime());
                    if (time1 > time2){
                        return -1;
                    }
                    return 1;
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        return mList;
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void codeEvent(JNCodeEvent codeEvent) {
//        switch (codeEvent.getCode()) {
//
//        }
//    }

    /**
     * 初始化对象
     */
    private void toolInitObj() {
        dataBeanList = new ArrayList<>();
        binding.listRefresh.setEnableLoadMore(false);
        binding.listRefresh.setOnRefreshLoadMoreListener(this);
        adapter = new CommonAdapter<JCGetCollectMessageApi.ColloctBean>(getActivity()
                , R.layout.jc_item_collect_msg, dataBeanList) {
            @Override
            protected void convert(ViewHolder holder, JCGetCollectMessageApi.ColloctBean colloctBean, int position) {
                JCCollectMsgBaseViewHolder viewHolder = null;
                    JNLogUtil.e("==JCCollectMsgFragment==CommonAdapter==convert==001");
                    try {
                        JCbaseIMMessage message = new Gson().fromJson(colloctBean.getBody(), JCimMessageBean.class);
                        if (TextUtils.equals(message.getMsgType(), ""+JCmessageType.TEXT)){
                            JNLogUtil.e("==JCCollectMsgFragment==CommonAdapter==convert==002");
                            viewHolder = new JCCollectMsgTextViewHolder(getActivity(), holder.itemView);
                        }else if (TextUtils.equals(message.getMsgType(), ""+JCmessageType.IMG)){
                            viewHolder = new JCCollectMsgImagViewHolder(getActivity(), holder.itemView);
                        }else if (TextUtils.equals(message.getMsgType(), ""+JCmessageType.AUDIO_VIDEO_RECORD)){
                            viewHolder = new JCCollectMsgAudioVideoRecordViewHolder(getActivity(), holder.itemView);
                        }else if (TextUtils.equals(message.getMsgType(), ""+JCmessageType.LOCATION)){
                            viewHolder = new JCCollectMsgLocationViewHolder(getActivity(), holder.itemView);
                        }else if (TextUtils.equals(message.getMsgType(), ""+JCmessageType.FILE)){
                            viewHolder = new JCCollectMsgFileViewHolder(getActivity(), holder.itemView);
                        }else if (TextUtils.equals(message.getMsgType(), ""+JCmessageType.CARD)){
                            viewHolder = new JCCollectMsgNameCardViewHolder(getActivity(), holder.itemView);
                        }else if (TextUtils.equals(message.getMsgType(), ""+JCmessageType.VIDEO)){
                            viewHolder = new JCCollectMsgVideoViewHolder(getActivity(), holder.itemView);
                        }else if (TextUtils.equals(message.getMsgType(), ""+JCmessageType.VOICE)){
                            viewHolder = new JCCollectMsgVoiceViewHolder(getActivity(), holder.itemView);
                        }
                    }catch (Exception e){
                        JNLogUtil.e("==JCCollectMsgFragment==CommonAdapter==convert==001",e);
                    }
                if (null!=viewHolder){
                    JNLogUtil.e("==JCCollectMsgFragment==CommonAdapter==convert==004");
                    viewHolder.initContent(colloctBean);
                    viewHolder.setViewClick(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                JCbaseIMMessage message = new Gson().fromJson(colloctBean.getBody(), JCimMessageBean.class);
                                jcFragmentSelect.onSelecte(JCbaseFragment.SELECTE_TYPE_MSG, message);
                                getActivity().finish();
                            }catch (Exception e){
                                JNLogUtil.e("==JCCollectMsgFragment==CommonAdapter==convert==001",e);
                            }
                        }
                    });
                }else {
                    JNLogUtil.e("==JCCollectMsgFragment==CommonAdapter==convert==005");
                }
            }
        };
        binding.recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recycleView.setAdapter(adapter);
        binding.listRefresh.autoRefresh();
    }

    public void getChatListData(){
        jcCollectMsgViewModel.getCollectMsgListData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
//        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
//        page++;
//        getChatListData();
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
//        page = 1;
        getChatListData();
    }
}

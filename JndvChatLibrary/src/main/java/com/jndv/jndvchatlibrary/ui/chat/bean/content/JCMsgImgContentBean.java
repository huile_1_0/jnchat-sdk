package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
* @Title: JCMsgImgContentBean.java
* @Description: 图片消息内容类
* @author SunQinzheng
* @date 2022/2/19 上午 11:06
*/

public class JCMsgImgContentBean {
    /**
     {
     "extend":"",//扩展字段（暂无意义）
     "url":"",//图片文件地址
     "thumbnailUrl":"",//缩略图文件地址
     "width":"",//图片宽
     "height":""//图片高
     }
     */
    private String url;
    private String thumbnailUrl;
    private String type;
    private String path;
    private int width;
    private int height;

    public JCMsgImgContentBean() {
    }

    public JCMsgImgContentBean(String path) {
        this.path = path;
    }

    public JCMsgImgContentBean(String path, int width, int height, String type) {
        this.path = path;
        this.width = width;
        this.height = height;
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "JCMsgImgContentBean{" +
                "url='" + url + '\'' +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                ", width='" + width + '\'' +
                ", height='" + height + '\'' +
                '}';
    }
}

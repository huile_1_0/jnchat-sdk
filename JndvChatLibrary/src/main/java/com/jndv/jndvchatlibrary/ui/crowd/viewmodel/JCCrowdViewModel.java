package com.jndv.jndvchatlibrary.ui.crowd.viewmodel;

import android.text.TextUtils;
import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.http.api.JCGetUserDetailsApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCCreatGroupNoticeApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCDisbandGroupApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGetGroupAllApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGetGroupNoticeApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGroupRenZhengUpdateApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCQuitGroupChatApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCScanQRCodeIntoGroupApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCGroupAllBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserDetailBean;
import com.jndv.jndvchatlibrary.ui.crowd.utils.CommonUtil;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

public class JCCrowdViewModel extends ViewModel {
    private static final String TAG = "";

    private MutableLiveData<String> mText;
    private MutableLiveData<JCGetGroupNoticeApi.Bean> mutableLiveData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean<JCGroupAllBean>> groupAllMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean<Boolean>> scanQRCodeIntoGroupData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean<Boolean>> updateRenZhengData = new MutableLiveData<>();
    private MutableLiveData<JCUserDetailBean>  userData = new MutableLiveData<>();

    private ComponentActivity activity;

    public JCCrowdViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is crowd fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    public MutableLiveData<JCGetGroupNoticeApi.Bean> getMutableLiveData() {
        return mutableLiveData;
    }

    public MutableLiveData<JCRespondBean<JCGroupAllBean>> getGroupAllMutableLiveData() {
        return groupAllMutableLiveData;
    }

    public MutableLiveData<JCRespondBean<Boolean>> getScanQRCodeIntoGroupData() {
        return scanQRCodeIntoGroupData;
    }
    public MutableLiveData<JCRespondBean<Boolean>> getUpdateRenZhengData() {
        return updateRenZhengData;
    }

    public MutableLiveData<JCUserDetailBean> getUserData() {
        return userData;
    }

    public void initData(ComponentActivity activity) {
        this.activity = activity;
        //mutableLiveData的第一次赋值使用setValue
//        mutableLiveData.setValue(groupNoticeBean);
    }

    public void getGroupNotice(String groupId) {
        EasyHttp.get(activity)
                .api(new JCGetGroupNoticeApi()
                        .setDomainAddr(JNBasePreferenceSaves.getSipAddress())
                        .setGroupId(groupId)
                )
                .request(new OnHttpListener<JCGetGroupNoticeApi.Bean>() {
                    @Override
                    public void onSucceed(JCGetGroupNoticeApi.Bean result) {
                        mutableLiveData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });
    }

    public void createGroupNotice(String groupId,String content) {
        JCCreatGroupNoticeApi noticeApi=new JCCreatGroupNoticeApi(
                JNBasePreferenceSaves.getSipAddress(),
                JNBasePreferenceSaves.getUserSipId(),
                groupId,content
        );

        EasyHttp.post(activity)
                .api(noticeApi)
                .json(new Gson().toJson(noticeApi))
                .request(new OnHttpListener<JCRespondBean<Boolean>>() {
                    @Override
                    public void onSucceed(JCRespondBean<Boolean> result) {
                        if (result != null) {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                                CommonUtil.showToast(activity,"发布群公告成功");
                            } else {
                                CommonUtil.showToast(activity,result.getMsg());
                            }
                        } else {
                            CommonUtil.showToast(activity,"操作失败");
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });
    }

    public void getGroupAll(String groupId, String groupDomainaddr) {
        EasyHttp.get(activity)
                .api(new JCGetGroupAllApi()
                        .setDomainAddr(JNBasePreferenceSaves.getSipAddress())
                        .setGroupId(groupId)
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                        .setGroupDomainaddr(groupDomainaddr)
                )
                .request(new OnHttpListener<JCRespondBean<JCGroupAllBean>>() {
                    @Override
                    public void onSucceed(JCRespondBean<JCGroupAllBean> result) {
                        groupAllMutableLiveData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });
    }

    public void quitGroupChat(String groupId, String groupDomainaddr, int session_id) {
        JCQuitGroupChatApi api = new JCQuitGroupChatApi()
                .setDomainAddr(JNBasePreferenceSaves.getSipAddress())
                .setGroupId(groupId)
                .setUserId(JNBasePreferenceSaves.getUserSipId())
                .setGroupDomainaddr(groupDomainaddr);
        EasyHttp.put(activity)
                .api(api)
                .json(new Gson().toJson(api))
                .request(new OnHttpListener<JCQuitGroupChatApi.Bean>() {
                    @Override
                    public void onSucceed(JCQuitGroupChatApi.Bean result) {
//                        quitGroupChatMutableLiveData.postValue(result);
                        if (result != null) {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                                CommonUtil.showToast(activity,"操作成功！");
                                initSession(2, session_id);
                                activity.finish();
                            } else if (TextUtils.equals("1001000000", result.getCode())) {
                                CommonUtil.showToast(activity,"已退出！");
                                initSession(2, session_id);
//                                activity.finish();
                            } else {
                                if (result.getMsg().contains("content")){
                                    try {
                                        JSONObject O = new JSONObject(result.getMsg());
                                        CommonUtil.showToast(activity,O.getString("content"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }else {
                                    CommonUtil.showToast(activity,result.getMsg());
                                }
                            }
                        } else {
                            CommonUtil.showToast(activity,"操作失败");
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });
    }

    public void disbandGroup(String groupId, int session_id) {
        EasyHttp.delete(activity)
                .api(new JCDisbandGroupApi()
                        .setDomainAddr(JNBasePreferenceSaves.getSipAddress())
                        .setGroupId(groupId)
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                )
                .request(new OnHttpListener<JCDisbandGroupApi.Bean>() {
                    @Override
                    public void onSucceed(JCDisbandGroupApi.Bean result) {
                        if (result != null) {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                                CommonUtil.showToast(activity,"操作成功！");
                                initSession(1, session_id);
                                activity.finish();
                            } else {
                                if (result.getMsg().contains("content")){
                                    try {
                                        JSONObject O = new JSONObject(result.getMsg());
                                        CommonUtil.showToast(activity,O.getString("content"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }else {
                                    CommonUtil.showToast(activity,result.getMsg());
                                }
                            }
                        } else {
                            CommonUtil.showToast(activity,"操作失败");
                        }

                        // disbandGroupMutableLiveData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });
    }

    private void initSession(int status, int session_id){
        try {
            JCSessionListBean jcSessionListBean = JCSessionUtil.getJCSessionListBean(session_id);
            JNLogUtil.e("===group======jcSessionListBean===" + jcSessionListBean);
            if (null!=jcSessionListBean){
                jcSessionListBean.setStatus(status);
                JCobjectBox.get().boxFor(JCSessionListBean.class).put(jcSessionListBean);
            }
            EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_INFO,"", jcSessionListBean));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 扫码加入群聊
     *
     * @param groupId
     * @param groupDomainAddr
     * @param groupDomainAddrUser
     */
    public void scanQRCodeIntoGroup(String groupId, String groupDomainAddr, String groupDomainAddrUser) {

        JCScanQRCodeIntoGroupApi api = new JCScanQRCodeIntoGroupApi(JNBasePreferenceSaves.getSipAddress()
                , JNBasePreferenceSaves.getJavaAddress(), JNBasePreferenceSaves.getUserSipId(),
                groupDomainAddr, groupDomainAddrUser,
                JNBasePreferenceSaves.getUserName(), JNBasePreferenceSaves.getUserHead(), groupId
        );
        EasyHttp.post(activity)
                .api(api).json(new Gson().toJson(api))
                .request(new OnHttpListener<JCRespondBean<Boolean>>() {
                    @Override
                    public void onSucceed(JCRespondBean<Boolean> result) {
                        scanQRCodeIntoGroupData.postValue(result);

                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e("EasyHttp", "onFail: " + e.getMessage());
                    }
                });
    }

    //更新群的认证设置
    public void updateRenZheng(String groupDomainaddr,String groupId,  String renzheng) {
        JCGroupRenZhengUpdateApi api = new JCGroupRenZhengUpdateApi(
                JNBasePreferenceSaves.getSipAddress(),
                groupDomainaddr,
                JNBasePreferenceSaves.getUserSipId(),
                groupId,
                JNBasePreferenceSaves.getJavaAddress(),
                renzheng
        );

        EasyHttp.put(activity)
                .api(api)
                .json(new Gson().toJson(api))
                .request(new OnHttpListener<JCRespondBean<Boolean>>() {
                    @Override
                    public void onSucceed(JCRespondBean<Boolean> result) {
                        updateRenZhengData.postValue(result);

                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e("EasyHttp", "onFail: " + e.getMessage());
                    }
                });
    }

    //获取个人信息
//    public JCCrowdViewModel getUserDetail(String userId) {
//        EasyHttp.get(activity)
//                .api(new JCGetUserDetailsApi()
//                        .setUserId(userId)
//                )
//                .request(new OnHttpListener<JCRespondBean<JCUserDetailBean>>() {
//                    @Override
//                    public void onSucceed(JCRespondBean<JCUserDetailBean> result) {
//                        userData.postValue(result.getData());
//                    }
//
//                    @Override
//                    public void onFail(Exception e) {
//                        e.printStackTrace();
//                        Log.e(TAG, "onFail: " + e.getMessage());
//                    }
//                });
//
//        return this;
//    }


}
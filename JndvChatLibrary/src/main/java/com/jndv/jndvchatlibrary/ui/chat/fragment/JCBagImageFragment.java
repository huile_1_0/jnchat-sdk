package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.databinding.FragmentJcbagimageBinding;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.chat.fragment.popup.SlidePopup;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.lxj.xpopup.XPopup;
import com.tbruyelle.rxpermissions2.RxPermissions;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

/**
 * Author: wangguodong
 * Date: 2022/5/6
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 图片全屏查看
 */
public class JCBagImageFragment  extends JCbaseFragment {
    String TAG="JCBagImageFragment";
    private FragmentJcbagimageBinding binding;
    private String path = "";
    private String url = "";
//    private Uri uri ;
    public static final String INURI = "inuri";
    public static final String INURL = "inurl";
    public static final String INPATH = "inpath";
    public static final String MESSAGE = "msg";
    JCimMessageBean messageBean;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentJcbagimageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);

        return root;
    }

    private void init(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            path = bundle.getString(INPATH);
            url = bundle.getString(INURL);
//            uri = (Uri) bundle.getParcelable(INURI);
            messageBean= (JCimMessageBean) bundle.getSerializable(MESSAGE);

            Log.e(TAG, "init: ===============01=path=" + path);
            Log.e(TAG, "init: ===============01=url=" + url);
        }
        if (!TextUtils.isEmpty(path)){
            if (path.contains("content:"))
                loadImageUri();
            else
                loadImageUrl();
        }
    }

    private void loadImageUrl(){
        JCglideUtils.loadImage(getActivity(), path, new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
            @Override
            public void onResourceReady(@NonNull @NotNull Bitmap resource, @Nullable @org.jetbrains.annotations.Nullable Transition<? super Bitmap> transition) {
                initImage(resource);
            }

            @Override
            public void onLoadFailed(@Nullable @org.jetbrains.annotations.Nullable Drawable errorDrawable) {
                JCglideUtils.loadImage(getActivity(), url, new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
                    @Override
                    public void onResourceReady(@NonNull @NotNull Bitmap resource, @Nullable @org.jetbrains.annotations.Nullable Transition<? super Bitmap> transition) {
                        initImage(resource);
                    }

                    @Override
                    public void onLoadFailed(@Nullable @org.jetbrains.annotations.Nullable Drawable errorDrawable) {
                        JCChatManager.showToast("图片加载失败！");
                    }
                });
            }
        });
    }

    private void loadImageUri(){
        Uri uri = Uri.parse(path);
        Log.e(TAG, "init: ===============01=uri=" + uri);
        if (null==uri){
            JCChatManager.showToast("图片加载失败！");
            return;
        }
        Log.e(TAG, "init: ===============02=uri=" );
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            initImage(bitmap);
        }catch (Exception e){
            JCglideUtils.loadImage(getActivity(), url, new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
                @Override
                public void onResourceReady(@NonNull @NotNull Bitmap resource, @Nullable @org.jetbrains.annotations.Nullable Transition<? super Bitmap> transition) {
                    initImage(resource);
                }

                @Override
                public void onLoadFailed(@Nullable @org.jetbrains.annotations.Nullable Drawable errorDrawable) {
                    JCChatManager.showToast("图片加载失败！");
                }
            });
        }
    }

    SlidePopup slidePopup;

    private void initImage(Bitmap bitmap){
        binding.tivBag.setImageBitmap(resizeImage(bitmap));
        Bitmap bm = com.jndv.jndvchatlibrary.utils.BitmapUtil.getInstance().imgToBitmap(binding.tivBag);
        slidePopup=new SlidePopup(getActivity(),url,messageBean,bm);
        binding.tivBag.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new XPopup.Builder(getActivity())
                        .hasStatusBar(true)
                        .hasShadowBg(true)
                        .asCustom(slidePopup).show();
                return true;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        RxPermissions rxPermissions = new RxPermissions(getActivity());
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                            , Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(aBoolean -> {
                    if (!aBoolean) {
                        JCChatManager.showToast("未授予部分文件存储权限");
                    }
                });

    }

    // 利用矩阵并指定宽高
    public static Bitmap resizeImage(Bitmap bitmap) {
        int w = 0; int h = 0;
        // 原图的bitmap
        Bitmap BitmapOrg = bitmap;
        // 原图的宽高
        int width = BitmapOrg.getWidth();
        int height = BitmapOrg.getHeight();
        w=width;
        h=height;
        // 指定的新的宽高
        if (width>4000){
            w = 4000;
            h = w*height/width;
        }
        if (h>4000){
            h=4000;
            w = h*width/height;
        }
        int newWidth = 0==w?width:w;
        int newHeight = 0==h?height:h;
        JNLogUtil.e("===================resizeImage=newWidth="+newWidth);
        JNLogUtil.e("===================resizeImage=newHeight="+newHeight);
        // 计算的缩放比例
        float scaleWidth = ((float) newWidth)*1F / width*1F;
        float scaleHeight = ((float) newHeight)*1F / height*1F;
        // 用于缩放的矩阵
        Matrix matrix = new Matrix();
        // 矩阵缩放
        matrix.postScale(scaleWidth, scaleHeight);
        // 如果要旋转图片
        // matrix.postRotate(45);
        // 生成新的bitmap
        JNLogUtil.e("===================resizeImage=scaleWidth="+scaleWidth);
        JNLogUtil.e("===================resizeImage=scaleHeight="+scaleHeight);
        JNLogUtil.e("===================resizeImage=width="+width);
        JNLogUtil.e("===================resizeImage=height="+height);
        Bitmap resizedBitmap = Bitmap.createBitmap(BitmapOrg, 0, 0, width,
                height, matrix, true);
        return resizedBitmap;
    }

}

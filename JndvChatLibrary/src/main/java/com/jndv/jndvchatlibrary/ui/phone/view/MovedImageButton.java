package com.jndv.jndvchatlibrary.ui.phone.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.animation.OvershootInterpolator;

import androidx.appcompat.widget.AppCompatImageButton;

/**
 * Created by BlueFire on 2019/9/12  14:32
 * Describe:
 */
public class MovedImageButton extends AppCompatImageButton {
    private static final String TAG = "MovedImageButton";
    private int lastX;
    private int lastY;
    private float screenWidth;

    public MovedImageButton(Context context) {
        super(context);
    }

    public MovedImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MovedImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        screenWidth = dm.widthPixels;
    }

    int moveDownX = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        int tempDX = 0;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            lastX = x;
            lastY = y;
            moveDownX = (int) event.getRawX();
            Log.e(TAG, "onTouchEvent: 1  " + moveDownX);
        }

        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            int offsetX = x - lastX;
            int offsetY = y - lastY;
            //第一种方法
            layout(getLeft() + offsetX,
                    getTop() + offsetY,
                    getRight() + offsetX,
                    getBottom() + offsetY);

            //第二种方法
            //offsetLeftAndRight(offsetX);
            //offsetTopAndBottom(offsetY);
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            tempDX = (int) event.getRawX() - moveDownX;
            adsorbAnim(event.getRawX(), event.getRawY());
            Log.e(TAG, "onTouchEvent: 2  " + event.getRawX() + "-" + moveDownX + "=" + tempDX);
//            if (Math.abs(tempDX) < 6) {
//                Log.e(TAG, "onTouchEvent: 3  " + tempDX);
//                // do your things
//                return false;// 距离较小，当作click事件来处理
//            } else {
//                return true;
//            }
        }

//        switch (event.getAction()){
//            case MotionEvent.ACTION_DOWN:
//                lastX = x;
//                lastY = y;
//                break;
//            case MotionEvent.ACTION_MOVE:
//                int offsetX = x-lastX;
//                int offsetY = y-lastY;
//                //第一种方法
//                layout(getLeft()+offsetX,
//                        getTop()+offsetY,
//                        getRight()+offsetX,
//                        getBottom()+offsetY);
//
//                //第二种方法
//                //offsetLeftAndRight(offsetX);
//                //offsetTopAndBottom(offsetY);
//                break;
//            case MotionEvent.ACTION_UP:
//                adsorbAnim(event.getRawX(), event.getRawY());
//                break;
//        }
        Log.e(TAG, "onTouchEvent: 4  "+ super.onTouchEvent(event) );
        return super.onTouchEvent(event);
    }


    private void adsorbAnim(float rawX, float rawY) {
        //靠顶吸附
        if (rawY <= dp2px(getContext(), 200)) {//注意rawY包含了标题栏的高
            animate().setDuration(400)
                    .setInterpolator(new OvershootInterpolator())
//                    .yBy(-getY()-getHeight()/2.0f)
                    .yBy(-getY() - getHeight() / 4.0f)
                    .start();
        } else if (rawX >= screenWidth / 2) {//靠右吸附
            animate().setDuration(400)
                    .setInterpolator(new OvershootInterpolator())
//                    .xBy(screenWidth - getX() - getWidth()/2.0f)
                    .xBy(screenWidth - getX() - getWidth())
                    .start();
        } else {//靠左吸附
//            ObjectAnimator animator = ObjectAnimator.ofFloat(this, "x", getX(), -getWidth()/2.0f);
            ObjectAnimator animator = ObjectAnimator.ofFloat(this, "x", getX(), getWidth() / 4.0f);
            animator.setInterpolator(new OvershootInterpolator());
            animator.setDuration(400);
            animator.start();
        }
    }

    /**
     * dp转px
     *
     * @param context
     * @param dpVal
     * @return
     */
    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal, context.getResources().getDisplayMetrics());
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

public class JCGetGroupAllApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/group/getGrooupAll";
    }

    private String domainAddr;
    private String userId;
    private String groupId;
    private String groupDomainaddr;

    public JCGetGroupAllApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCGetGroupAllApi setGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    public JCGetGroupAllApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JCGetGroupAllApi setGroupDomainaddr(String groupDomainaddr) {
        this.groupDomainaddr = groupDomainaddr;
        return this;
    }
}

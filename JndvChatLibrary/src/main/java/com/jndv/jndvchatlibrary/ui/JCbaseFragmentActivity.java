package com.jndv.jndvchatlibrary.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.chat.JCsystemBarTintManager;
import com.jndv.jndvchatlibrary.utils.ActivityStack;
import com.wega.library.loadingDialog.LoadingDialog;

/**
 * Author: wangguodong
 * Date: 2022/2/15
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 基础activity，用于加载展示fragment
 *
 * git config --global user.email 1772889689@qq.com
 * git config --global user.name WGDrzjz
 *
 */
public class JCbaseFragmentActivity extends JCBaseHeadActivity {

    private LoadingDialog loadingDialog = null;
    static JCbaseFragment.JCFragmentSelect mJcFragmentSelect ;

    public static void start(Context context){
        Intent intent = new Intent(context, JCbaseFragmentActivity.class);
        if (!(context instanceof Activity)) intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    public static void start(Context context, JCbaseFragment mJCbaseFragment){
        start(context, mJCbaseFragment, null, null,null, true);
    }

    public static void start(Context context, JCbaseFragment mJCbaseFragment, String title){
        start(context, mJCbaseFragment, title, null,null, true);
    }

    public static void start(Context context, JCbaseFragment mJCbaseFragment, String title
            , Bundle bundle){
        start(context, mJCbaseFragment, title, bundle,null, true);
    }
    public static void start(Context context, JCbaseFragment mJCbaseFragment, String title
            , Bundle bundle, JCbaseFragment.JCFragmentSelect jcFragmentSelect, boolean isShowTitle){
        mJcFragmentSelect = jcFragmentSelect;
        Intent intent = new Intent(context, JCbaseFragmentActivity.class);
        if (!(context instanceof Activity)) intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("mJCbaseFragment", mJCbaseFragment);
        JNLogUtil.e("==JCbaseFragmentActivity==title=="+title);
        if (null!=title)intent.putExtra("title", title);
        intent.putExtra("isShowTitle", isShowTitle);
        if (null!=bundle)intent.putExtra("bundle", bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jc_activity_base);
        ActivityStack.getInstance().addActivity(this);

        /**
         * 设置状态栏颜色
         */
//        initSystemBar(this, R.color.colorPrimaryChat);
        initSystemBar();
//        initView();
        JCbaseFragment mJCbaseFragment = (JCbaseFragment) getIntent().getSerializableExtra("mJCbaseFragment");
        if (null!=mJCbaseFragment){
            Bundle bundle = getIntent().getBundleExtra("bundle");
            if (null!=bundle)mJCbaseFragment.setArguments(bundle);
            if (null!=mJcFragmentSelect)mJCbaseFragment.setJcFragmentSelect(mJcFragmentSelect);
            showFragment(mJCbaseFragment);
        }
        String title = getIntent().getStringExtra("title");
        setShowTitle(getIntent().getBooleanExtra("isShowTitle", true));
        JNLogUtil.e("==JCbaseFragmentActivity==onCreate==title=="+title);
        if (null!=title && !TextUtils.isEmpty(title)){
            setCenterTitle(title);
        }
        loadingDialog = new LoadingDialog(this);
    }

    protected void showFragment(JCbaseFragment mJCbaseFragment){
        mJCbaseFragment.setContainerId(R.id.fragment_msg);
        switchContent(mJCbaseFragment);
    }

    protected void addFragment(JCbaseFragment mJCbaseFragment){
        mJCbaseFragment.setContainerId(R.id.fragment_msg);
        switchContent(mJCbaseFragment, true);
    }

    /**
     * 改变系统标题栏颜色
     */
    public void initSystemBar() {
        if (JCChatManager.getTitleState() == JNBaseConstans.TITLE_STATE_LIGHT){
            setStatusBar();
        }
    }
    /**
     * 改变系统标题栏颜色
     * @param activity
     * @param color   color xml文件下的颜色
     */
    public static void initSystemBar(Activity activity, int color) {
        setTranslucentStatus(activity, true);
        JCsystemBarTintManager tintManager = new JCsystemBarTintManager(activity);
        tintManager.setStatusBarTintEnabled(true);
        // 使用颜色资源
        tintManager.setStatusBarTintResource(color);
    }

    /**
     * 设置系统标题栏的透明度
     * @param activity
     * @param on
     */
    @TargetApi(19)
    protected static void setTranslucentStatus(Activity activity, boolean on) {
        setTranslucentStatus(activity, on, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }
    protected static void setTranslucentStatus(Activity activity, boolean on, int state) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
//        final int bits = WindowManager.LayoutParams.FLAG_LAYOUT_IN_OVERSCAN;
        final int bits = state;
//        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    protected void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));//设置状态栏颜色
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏图标和文字颜色为暗色
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏图标和文字颜色为暗色
        }
    }

    public JCbaseFragment switchContent(JCbaseFragment fragment) {
        return switchContent(fragment, false);
    }

     public JCbaseFragment switchContent(JCbaseFragment fragment, boolean needAddToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        if (needAddToBackStack) {
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.add(fragment.getContainerId(), fragment);
        }else {
            fragmentTransaction.replace(fragment.getContainerId(), fragment);
        }
        try {
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fragment;
    }

    /**
     * 显示加载框
     */
    public void showLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.loading();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==showLoadding==", e);
        }
    }

    /**
     * 直接取消加载框
     */
    public void cancelLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.cancel();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==cancelLoadding==", e);
        }
    }

}

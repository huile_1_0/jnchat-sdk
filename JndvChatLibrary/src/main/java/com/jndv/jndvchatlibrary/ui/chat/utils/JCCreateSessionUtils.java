package com.jndv.jndvchatlibrary.ui.chat.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.hjq.http.request.PostRequest;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.http.api.JCCreatSessionApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCChatActivity;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;

import org.greenrobot.eventbus.EventBus;

import io.objectbox.Box;

/**
 * Author: wangguodong
 * Date: 2022/4/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 会话创建工具类
 */
public class JCCreateSessionUtils {
    static String TAG="JCCreateSessionUtils";
    static LoadingPopupView loadingPopupView = null;

    //    public static void creatSession(Fragment fragment, String sessionPartyId, String sessionPartyDomainAddr
//            , String sessionPartyDomainAddrUser, String type
//            , String sessionPartyName, String sessionPartyPic, boolean goChat){
//        creatSession(fragment, sessionPartyId,sessionPartyDomainAddr,sessionPartyDomainAddrUser,type
//                ,sessionPartyName,sessionPartyPic, new OnHttpListener<JCCreatSessionApi.Bean>() {
//                    @Override
//                    public void onSucceed(JCCreatSessionApi.Bean result) {
//                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
//                            Toast.makeText(fragment.getContext(),"创建成功！",Toast.LENGTH_SHORT).show();
//                            if (goChat){
//                                Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
//                                JCSessionListBean bean = result.getData();
//                                imMsgBox.put(bean);
//                                JCChatActivity.start(fragment.getContext(), bean);
//                            }
//                        }else {
//                            Toast.makeText(fragment.getContext(),"会话创建失败，稍后再试！",Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFail(Exception e) {
//                        Toast.makeText(fragment.getContext(),"会话创建失败，稍后再试！",Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//    }
    public static void creatSession(FragmentActivity activity, String sessionPartyId, String sessionPartyDomainAddr
            , String sessionPartyDomainAddrUser, String type
            , String sessionPartyName, String sessionPartyPic, boolean goChat){
        Log.d(TAG,"creatSession...222");
        creatSession(activity, sessionPartyId,sessionPartyDomainAddr,sessionPartyDomainAddrUser,type
                ,sessionPartyName,sessionPartyPic, new OnHttpListener<JCCreatSessionApi.Bean>() {
                    @Override
                    public void onSucceed(JCCreatSessionApi.Bean result) {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            Toast.makeText(activity,"创建成功！",Toast.LENGTH_SHORT).show();
                            if (goChat){
                                Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                                JCSessionListBean bean = result.getData();
                                imMsgBox.put(bean);
                                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_NET,"创建会话！"));
                                JCChatActivity.start(activity, bean);
                            }
                        }else {
//                            1001003000    会话对象已存在
                            Toast.makeText(activity,result.getMsg(),Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        Toast.makeText(activity,"会话创建失败，稍后再试！",Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //只适用于转发图片生成的会话
    public static void creatSession2(FragmentActivity activity, String sessionPartyId, String sessionPartyDomainAddr
            , String sessionPartyDomainAddrUser, String type
            , String sessionPartyName, String sessionPartyPic) {
        Log.d(TAG,"creatSession...333");
        creatSession(activity, sessionPartyId, sessionPartyDomainAddr, sessionPartyDomainAddrUser, type
                , sessionPartyName, sessionPartyPic, new OnHttpListener<JCCreatSessionApi.Bean>() {
                    @Override
                    public void onSucceed(JCCreatSessionApi.Bean result) {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                            Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                            JCSessionListBean bean = result.getData();
                            imMsgBox.put(bean);
                            EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_NET, "创建会话！"));
                        } else {
//                            1001003000    会话对象已存在
                            Toast.makeText(activity, result.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        Toast.makeText(activity, "会话创建失败，稍后再试！", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //    public static void creatSession(Fragment fragment, String sessionPartyId, String sessionPartyDomainAddr
//            , String sessionPartyDomainAddrUser, String type
//            , String sessionPartyName, String sessionPartyPic){
//        creatSession(fragment, sessionPartyId,sessionPartyDomainAddr,sessionPartyDomainAddrUser,type,sessionPartyName,sessionPartyPic, null);
//
//    }
    public static void creatSession(FragmentActivity activity, String sessionPartyId, String sessionPartyDomainAddr
            , String sessionPartyDomainAddrUser, String type
            , String sessionPartyName, String sessionPartyPic){
        Log.d(TAG,"creatSession...444");
        creatSession(activity, sessionPartyId,sessionPartyDomainAddr,sessionPartyDomainAddrUser,type,sessionPartyName,sessionPartyPic, null);
    }
//    public static void creatSession(Fragment fragment, String sessionPartyId, String sessionPartyDomainAddr
//            , String sessionPartyDomainAddrUser, String type
//            , String sessionPartyName, String sessionPartyPic, OnHttpListener onHttpListener){
//        if (null==onHttpListener){
//            new OnHttpListener<JCCreatSessionApi.Bean>() {
//                @Override
//                public void onSucceed(JCCreatSessionApi.Bean result) {
//                    Log.e("netdata", "onSucceed: ========creatSession=======" + new Gson().toJson(result));
//                    try {
////                            这里应该是想办法获取到新创建的会话信息
//                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
//                            Toast.makeText(fragment.getContext(),"创建成功！",Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(fragment.getContext(),"创建失败！",Toast.LENGTH_SHORT).show();
//                        }
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//                @Override
//                public void onFail(Exception e) {
////                        netFail();
//                    Toast.makeText(fragment.getContext(),"创建失败！",Toast.LENGTH_SHORT).show();
//                }
//            };
//        }
//        PostRequest postRequest = EasyHttp.post(fragment);
//        creatSession(fragment.getContext(), postRequest,sessionPartyId,sessionPartyDomainAddr,sessionPartyDomainAddrUser,type,sessionPartyName,sessionPartyPic, onHttpListener);
//    }

    public static void creatSession(FragmentActivity activity, String sessionPartyId, String sessionPartyDomainAddr, String sessionPartyDomainAddrUser, String type
            , String sessionPartyName, String sessionPartyPic, OnHttpListener<JCCreatSessionApi.Bean> onHttpListener){
        if (null==onHttpListener){
            new OnHttpListener<JCCreatSessionApi.Bean>() {
                @Override
                public void onSucceed(JCCreatSessionApi.Bean result) {
                    Log.e("netdata", "onSucceed: ========creatSession=======" + new Gson().toJson(result));
                    try {
//                            这里应该是想办法获取到新创建的会话信息
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_NET,"创建会话！"));
                            Toast.makeText(activity,"创建成功！",Toast.LENGTH_SHORT).show();
                        }else {
                            //                            1001003000    会话对象已存在
                            Toast.makeText(activity,"创建失败！",Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFail(Exception e) {
                    Toast.makeText(activity,"创建失败！",Toast.LENGTH_SHORT).show();
                }
            };
        }
        PostRequest postRequest = EasyHttp.post(activity);
        creatSession(activity, postRequest,sessionPartyId,sessionPartyDomainAddr,sessionPartyDomainAddrUser,type,sessionPartyName,sessionPartyPic, onHttpListener);
    }


    /**
     * 创建会话
     * "domainAddr" : "192.168.1.5:5060",
     *     	"sessionPartyDomainAddr" : "192.168.1.5:5060",
     *     	"sessionPartyDomainAddrUser" : "192.168.2.205:8000",
     *     	"sessionPartyId" : "1045",
     *     	"sessionPartyName" : "",
     *     	"sessionPartyPic" : "",
     *     	"type" : "1",
     *     	"userId" : "1042"
     */
    private static void creatSession(Context context, PostRequest postRequest
            , String sessionPartyId, String sessionPartyDomainAddr, String sessionPartyDomainAddrUser, String type
            , String sessionPartyName, String sessionPartyPic, OnHttpListener<JCCreatSessionApi.Bean> onHttpListener){
        if (null==sessionPartyId)return;
        try {
            if(loadingPopupView==null){
                loadingPopupView = new XPopup.Builder(context).asLoading("正在加载中");
            }
            //这里会出现context为null的情况，如果为null，就不显示加载框
            if(context!=null){
                loadingPopupView.show();
            }
        }catch (Exception e){
            JNLogUtil.e("==JCCreateSessionUtils==creatSession==LoadingPopupView==",e);
        }
        JCCreatSessionApi jcCreatSessionApi = new JCCreatSessionApi()
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setUserId(JNBasePreferenceSaves.getUserSipId())
                .setSessionPartyId(sessionPartyId)
                .setSessionPartyDomainAddr(sessionPartyDomainAddr)
                .setSessionPartyDomainAddrUser(sessionPartyDomainAddrUser)
                .setType(""+type)
                .setRequestBy(1)
                .setSessionPartyName(sessionPartyName)
                .setSessionPartyPic(null==sessionPartyPic|| TextUtils.isEmpty(sessionPartyPic)?"1":sessionPartyPic);
        postRequest.api(jcCreatSessionApi)
                .json(new Gson().toJson(jcCreatSessionApi))
                .request(new OnHttpListener<JCCreatSessionApi.Bean>() {
                        @Override
                        public void onSucceed(JCCreatSessionApi.Bean result) {
                            Log.d(TAG,"onSucceed...231");
                            //如果是发给自己的sip消息，不展示该消息
                            if(result.getData()!=null &&
                                    TextUtils.equals(result.getData().getOther_id(),JNBasePreferenceSaves.getUserSipId())){
                                loadingPopupView.dismiss();
                                return;
                            }
                            try {
                                if (null!=onHttpListener)onHttpListener.onSucceed(result);
                            }catch (Exception e){e.printStackTrace();}
                            try {
                                if (null!=loadingPopupView)loadingPopupView.dismiss();
                            }catch (Exception e){
                                JNLogUtil.e("==JCCreateSessionUtils==creatSession==LoadingPopupView==dismiss==",e);
                            }
                        }

                        @Override
                        public void onFail(Exception e) {
                            if (null!=onHttpListener)onHttpListener.onFail(e);
                            try {
                                if (null!=loadingPopupView)loadingPopupView.dismiss();
                            }catch (Exception ex){
                                JNLogUtil.e("==JCCreateSessionUtils==creatSession==LoadingPopupView==dismiss==",ex);
                            }
                        }
                    });
    }


}

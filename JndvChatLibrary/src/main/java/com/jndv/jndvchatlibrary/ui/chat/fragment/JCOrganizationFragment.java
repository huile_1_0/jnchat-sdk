package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcorganizationBinding;
import com.jndv.jndvchatlibrary.http.api.JCGetOrganizationApi;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jnbaseutils.ui.bean.Node;
import com.jndv.jnbaseutils.ui.JNSimpleDividerDecoration;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/4/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 组织机构及人员列表fragment
 */
public class JCOrganizationFragment extends JCbaseFragment {
    private FragmentJcorganizationBinding binding;
    private JCOrganizationListViewModel jcOrganizationListViewModel;
    private CommonAdapter adapter ;
    private List<Node> datas = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        jcOrganizationListViewModel =
                new ViewModelProvider(this).get(JCOrganizationListViewModel.class);
        binding = FragmentJcorganizationBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);
//        EventBus.getDefault().register(this);
        return root;
    }

    private void init(View view) {
        jcOrganizationListViewModel.init(this);
        initRecycleView();
        initViewData();
    }

    private void initViewData(){/*

        jcOrganizationListViewModel.getOrganizationList().observeForever(new Observer<JCOrganizationListViewModel.JCOrganzationListModelItem>() {
            @Override
            public void onChanged(JCOrganizationListViewModel.JCOrganzationListModelItem jcOrganzationListModelItem) {
                if (jcOrganzationListModelItem.getType()==0){
                    List<Node> list = new ArrayList<>();
                    if (null!=jcOrganzationListModelItem&&null!=jcOrganzationListModelItem.getData()){
                        if (null!=jcOrganzationListModelItem.getData().getDeparts() && jcOrganzationListModelItem.getData().getDeparts().size()>0){
                            for (int i = 0; i < jcOrganzationListModelItem.getData().getDeparts().size(); i++) {
                                Node node = new Node();
                                node.setType(1);
                                node.setLevel(0);
                                node.setNid(jcOrganzationListModelItem.getData().getDeparts().get(i).getId());
                                node.setContent(new Gson().toJson(jcOrganzationListModelItem.getData().getDeparts().get(i)));
                                list.add(node);
                            }
                        }
                        if (null!=jcOrganzationListModelItem.getData().getUsers() && jcOrganzationListModelItem.getData().getUsers().size()>0){
                            for (int i = 0; i < jcOrganzationListModelItem.getData().getUsers().size(); i++) {
                                Node node = new Node();
                                node.setType(0);
                                node.setNid(jcOrganzationListModelItem.getData().getUsers().get(i).getId());
                                node.setContent(new Gson().toJson(jcOrganzationListModelItem.getData().getUsers().get(i)));
                                list.add(node);
                            }
                        }
                        if (-1==jcOrganzationListModelItem.getNid()){
                            datas.addAll(list);
                            adapter.notifyDataSetChanged();
                        }else {
                            for (int i = 0; i < datas.size(); i++) {
                                if (jcOrganzationListModelItem.getNid() == datas.get(i).getNid()){
                                    Node node = datas.get(i);
                                    node.setExpand(true);
                                    if (null!=list&&list.size()>0){
                                        for (int j = 0; j < list.size(); j++) {
                                            Node nodej = list.get(j);
                                            nodej.setLevel(node.getLevel()+1);
                                            list.set(j,nodej);
                                        }
                                        node.setChildren(list);
                                    }

                                    datas.set(i, node);
                                    adapter.notifyItemChanged(i);
                                    return;
                                }else if (null!=datas.get(i).getChildren() && datas.get(i).getChildren().size()>0){
                                    List<Node> listo = datas.get(i).getChildren();
                                    NodeBean nodeBean = initAdaterData(jcOrganzationListModelItem,list, listo);
                                    if (null!=nodeBean){
                                        Node node1 = datas.get(i);
                                        listo.set(nodeBean.getPos(),nodeBean.getNode());
                                        node1.setChildren(listo);
                                        datas.set(i, node1);
                                        adapter.notifyItemChanged(i);
                                    }
                                }
                            }
                        }

                    }
                }
            }
        });
        */
    }

    public class NodeBean{
        private Node node;
        private int pos;

        public Node getNode() {
            return node;
        }

        public void setNode(Node node) {
            this.node = node;
        }

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }
    }

    private NodeBean initAdaterData(JCOrganizationListViewModel.JCOrganzationListModelItem jcOrganzationListModelItem, List<Node> list, List<Node> data){
        /*for (int i = 0; i < data.size(); i++) {
            if (jcOrganzationListModelItem.getNid() == data.get(i).getNid()){
                Node node = data.get(i);
                node.setExpand(true);
                if (null!=list&&list.size()>0){
                    for (int j = 0; j < list.size(); j++) {
                        Node nodej = list.get(j);
                        nodej.setLevel(node.getLevel()+1);
                        list.set(j,nodej);
                    }
                    node.setChildren(list);
                }
                NodeBean nodeBean = new NodeBean();
                nodeBean.setNode(node);
                nodeBean.setPos(i);
                return nodeBean;
            }else if (null!=data.get(i).getChildren() && data.get(i).getChildren().size()>0){
                List<Node> listo = data.get(i).getChildren();
                NodeBean nodeBean = initAdaterData(jcOrganzationListModelItem,list, listo);
                if (null!=nodeBean){
                    Node node1 = data.get(i);
//                    Node node = listo.get(nodeBean.getPos());
                    listo.set(nodeBean.getPos(),nodeBean.getNode());
                    node1.setChildren(listo);
                    NodeBean nodeBean1 = new NodeBean();
                    nodeBean1.setNode(node1);
                    nodeBean1.setPos(i);
                    return nodeBean;
                }
            }
        }*/
        return null;
    }

    private void initRecycleView(){
        binding.rvFriend.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvFriend.addItemDecoration(new JNSimpleDividerDecoration(getActivity()));
        adapter = new CommonAdapter<Node>(getActivity(), R.layout.jc_item_organixation, datas) {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            protected void convert(ViewHolder holder, Node node, int position) {
                if (0==node.getType()){
                    try {
                        holder.getView(R.id.ll_friend).setVisibility(View.VISIBLE);
                        holder.getView(R.id.ll_depart_all).setVisibility(View.GONE);
                        JCGetOrganizationApi.Bean.UserBean userBean = new Gson().fromJson(node.getContent(),JCGetOrganizationApi.Bean.UserBean.class);
                        JCglideUtils.loadCircleImage(getActivity(), userBean.getAvatarurl(), holder.getView(R.id.iv_user_head));
                        ((TextView)holder.getView(R.id.tv_user_name)).setText(getName(userBean));
                    }catch (Exception e){e.printStackTrace();}
                }else {
                    try {
                        holder.getView(R.id.pb_organzation_progress).setVisibility(View.GONE);
                        holder.getView(R.id.ll_friend).setVisibility(View.GONE);
                        holder.getView(R.id.ll_depart_all).setVisibility(View.VISIBLE);
                        JCGetOrganizationApi.Bean.DepartBean departBean = new Gson().fromJson(node.getContent(),JCGetOrganizationApi.Bean.DepartBean.class);
//                        JCglideUtils.loadCircleImage(getActivity(), userBean.getAvatarurl(), holder.getView(R.id.iv_user_head));
//                        ((TextView)holder.getView(R.id.tv_user_name)).setText(getName(userBean));
                        ((TextView)holder.getView(R.id.tv_name)).setText(departBean.getDeptname());
                        if (node.isExpand()){//打开了
                            holder.getView(R.id.iv_left_jiantou).setBackground(getResources().getDrawable(R.drawable.jn_jiantou_down));
                            initChildrenList(holder, node);
                        }else {
                            holder.getView(R.id.iv_left_jiantou).setBackground(getResources().getDrawable(R.drawable.jn_jiantou_right));
                            holder.getView(R.id.rv_children).setVisibility(View.GONE);
                        }
                        holder.getView(R.id.ll_depart_title).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (node.isExpand()){
                                    node.setExpand(false);
                                    datas.set(position, node);
                                    adapter.notifyItemChanged(position);
                                }else {
                                    if (null!=node.getChildren() && node.getChildren().size()>0){
                                        node.setExpand(true);
                                        datas.set(position, node);
                                        adapter.notifyItemChanged(position);
                                    }else {
                                        holder.getView(R.id.pb_organzation_progress).setVisibility(View.VISIBLE);
//                                        jcOrganizationListViewModel.getOrganzationListData(""+departBean.getId(), node.getNid());
                                    }
                                }
                            }
                        });
                    }catch (Exception e){e.printStackTrace();}
                }

            }
        };
        binding.rvFriend.setAdapter(adapter);

    }

    public String getName(JCGetOrganizationApi.Bean.UserBean userBean){
        String name = "";
        if (null!=userBean){
            if (null!=userBean.getNickName()&&!TextUtils.isEmpty(userBean.getNickName())){
                name = userBean.getNickName();
            }else if (null!=userBean.getAccount()&&!TextUtils.isEmpty(userBean.getAccount())){
                name = userBean.getAccount();
            }else if (null!=userBean.getIM_Number()&&!TextUtils.isEmpty(userBean.getIM_Number())){
                name = userBean.getIM_Number();
            }
        }
        return name;
    }

    public void initChildrenList(ViewHolder holder, Node node){
        holder.getView(R.id.rv_children).setVisibility(View.VISIBLE);
        RecyclerView recyclerView = holder.getView(R.id.rv_children);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new JNSimpleDividerDecoration(getActivity()));
        List<Node> datas1 = new ArrayList<>();
        if (null!=node && null!=node.getChildren())datas1.addAll(node.getChildren());
        CommonAdapter<Node> adapter1 = new CommonAdapter<Node>(getActivity(), R.layout.jc_item_organixation, datas1) {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            protected void convert(ViewHolder holder, Node node, int position) {
                holder.getView(R.id.ll_user_item).setPadding(node.getLevel() * 30, 12, 12, 12);
                if (0==node.getType()){
                    try {
                        holder.getView(R.id.ll_friend).setVisibility(View.VISIBLE);
                        holder.getView(R.id.ll_depart_all).setVisibility(View.GONE);
                        JCGetOrganizationApi.Bean.UserBean userBean = new Gson().fromJson(node.getContent(),JCGetOrganizationApi.Bean.UserBean.class);
                        JCglideUtils.loadCircleImage(getActivity(), userBean.getAvatarurl(), holder.getView(R.id.iv_user_head));
                        ((TextView)holder.getView(R.id.tv_user_name)).setText(getName(userBean));
                    }catch (Exception e){e.printStackTrace();}
                }else {
                    try {
                        holder.getView(R.id.pb_organzation_progress).setVisibility(View.GONE);
                        holder.getView(R.id.ll_friend).setVisibility(View.GONE);
                        holder.getView(R.id.ll_depart_all).setVisibility(View.VISIBLE);
                        JCGetOrganizationApi.Bean.DepartBean departBean = new Gson().fromJson(node.getContent(),JCGetOrganizationApi.Bean.DepartBean.class);
//                        JCglideUtils.loadCircleImage(getActivity(), userBean.getAvatarurl(), holder.getView(R.id.iv_user_head));
                        ((TextView)holder.getView(R.id.tv_name)).setText(departBean.getDeptname());
                        if (node.isExpand()){//打开了
//                            JCglideUtils.loadCircleImage(getActivity(), , );
                            holder.getView(R.id.iv_left_jiantou).setBackground(getResources().getDrawable(R.drawable.jn_jiantou_down));
                            initChildrenList(holder, node);
                        }else {
                            holder.getView(R.id.iv_left_jiantou).setBackground(getResources().getDrawable(R.drawable.jn_jiantou_right));
                            holder.getView(R.id.rv_children).setVisibility(View.GONE);
                        }
                        holder.getView(R.id.ll_depart_title).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (node.isExpand()){
                                    node.setExpand(false);
                                    datas.set(position, node);
                                    adapter.notifyItemChanged(position);
                                }else {
                                    if (null!=node.getChildren() && node.getChildren().size()>0){
                                        node.setExpand(true);
                                        datas.set(position, node);
                                        adapter.notifyItemChanged(position);
                                    }else {
                                        holder.getView(R.id.pb_organzation_progress).setVisibility(View.VISIBLE);
//                                        jcOrganizationListViewModel.getOrganzationListData(""+departBean.getId(), node.getNid());
                                    }
                                }
                            }
                        });
                    }catch (Exception e){e.printStackTrace();}
                }

            }
        };
        recyclerView.setAdapter(adapter1);
    }

}

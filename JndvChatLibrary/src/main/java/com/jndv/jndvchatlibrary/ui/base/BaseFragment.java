package com.jndv.jndvchatlibrary.ui.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


//import io.reactivex.disposables.CompositeDisposable;

/**
 * 基类
 */
public abstract class BaseFragment extends Fragment {


//    protected CompositeDisposable compositeDisposable;

    protected View mContentView;
    protected Context mContext;

   /* private boolean isViewCreated;//视图是否已经创建
    private boolean isUiVisible;//该fragment是否对用户可见
*/
    private boolean isViewCreated; // 界面是否已创建完成
    private boolean isVisibleToUser; // 是否对用户可见
    private boolean isDataLoaded; // 数据是否已请求
    protected Handler mHandler;
    private LocalReceiver localReceiver;
    private boolean isNeedBroadcast=true;
    private boolean isNeedAvatar=true;
    private boolean isNeedHandler=true;





    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        compositeDisposable = new CompositeDisposable();

    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContentView = inflater.inflate(setLayoutResourceID(), container, false);

        initView(mContentView);
        if (isNeedBroadcast) {
            initReceiver();
        }

        if (isNeedHandler) {
            mHandler = new LocalHandler(this);
        }


        //loadData();
        return mContentView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isViewCreated = true;
       // tryLoadData();
    }

    @Override
    public void onDestroy() {
        if (isNeedBroadcast && localReceiver != null) {
            try {
                mContext.unregisterReceiver(localReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (isNeedHandler) {
            mHandler.removeCallbacksAndMessages(null);
        }

        super.onDestroy();

    }

    public abstract void addBroadcast(IntentFilter filter);

    public abstract void receiveBroadcast(Intent intent);

//    public abstract void receiveNewAvatar(User targetUser, Bitmap bnewAvatarm);

    public abstract void receiveMessage(Message msg);
    protected abstract int setLayoutResourceID();

    protected abstract void initView(View v);

    //protected abstract void loadData();

    protected View getId(int id) {
        return mContentView.findViewById(id);
    }

    public View getContentView() {
        return mContentView;
    }

    public Context getmContext() {
        return mContext;
    }


    protected void showToast(String message){
        Toast.makeText(mContext,message, Toast.LENGTH_SHORT).show();
    }


    protected void initReceiver() {
        localReceiver = new LocalReceiver();
        IntentFilter filter = new IntentFilter();
//        filter.addCategory(ChatEntity.JNI_BROADCAST_CATEGORY);
//        filter.addCategory(PublicIntent.DEFAULT_CATEGORY);
        addBroadcast(filter);
        mContext.registerReceiver(localReceiver, filter);
    }

    private static class LocalHandler extends Handler {
        private final BaseFragment baseFragment;

        private LocalHandler(BaseFragment fragment) {
            baseFragment = fragment;
        }

        @Override
        public void handleMessage(Message msg) {
            if (baseFragment== null) {
                return;
            }

            baseFragment.receiveMessage(msg);
        }
    }

    class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            receiveBroadcast(intent);

        }
    }

    /**延迟加载    子类必须重写此方法 */

    protected abstract void lazyLoad();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isViewCreated = false;
       // isUiVisible = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        tryLoadData();
    }

   /* @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isViewCreated = true;
        tryLoadData();
    }*/

    public void tryLoadData() {
        if (isViewCreated && isVisibleToUser && !isDataLoaded) {
            lazyLoad();
            isDataLoaded = true;
        }
    }

    protected int getColor(int id){
        return getResources().getColor(id);

    }



}

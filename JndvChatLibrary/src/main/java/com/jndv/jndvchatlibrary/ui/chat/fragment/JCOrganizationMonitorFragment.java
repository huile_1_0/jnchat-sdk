package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNGetVideoTree;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcorganizationnewBinding;
import com.jndv.jnbaseutils.http.api.JNGetMonitorRtspApi;
import com.jndv.jndvchatlibrary.ksyplayer.JCMonitorPlayerActivity;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jnbaseutils.ui.bean.Node;
import com.jndv.jnbaseutils.ui.JNSimpleDividerDecoration;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/4/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 组织机构及监控列表fragment
 */
public class JCOrganizationMonitorFragment extends JCbaseFragment implements OnRefreshLoadMoreListener {
    private FragmentJcorganizationnewBinding binding;
    private JCOrganizationMonitorViewModel jcOrganizationMonitorViewModel;
    private CommonAdapter adapter ;
    private List<Node> datas = new ArrayList<>();
//    private String nid = "0";
    private String zoningCode ;
//    private String domainName;//域名
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        jcOrganizationMonitorViewModel =
                new ViewModelProvider(this).get(JCOrganizationMonitorViewModel.class);
        binding = FragmentJcorganizationnewBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);
//        EventBus.getDefault().register(this);
        return root;
    }

    private void init(View view) {
        jcOrganizationMonitorViewModel.init(this);
        Bundle bundle = getArguments();
        if (bundle != null){
//            nid = bundle.getString("nid");
            zoningCode = bundle.getString("zoningCode");
        }

        initRecycleView();
        getData();
    }

    @Override
    public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {
        binding.listRefresh.finishLoadMore(1000);
    }

    @Override
    public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
//        jcOrganizationMonitorViewModel.getOrganzationListData(nid, -1);
        getData();
    }

    private void initRecycleView(){
        binding.listRefresh.setEnableLoadMore(false);
        binding.listRefresh.setOnRefreshLoadMoreListener(this);
        binding.recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        JNSimpleDividerDecoration divider = new JNSimpleDividerDecoration(getActivity());
        divider.setPadding(30,20);
        binding.recycleView.addItemDecoration(divider);
        adapter = new CommonAdapter<Node>(getActivity(), R.layout.jc_item_organixation_monitor, datas) {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            protected void convert(ViewHolder holder, Node node, int position) {
                if (0==node.getType()){
                    try {
                        JNGetVideoTree.DataBean monitorBean = new Gson().fromJson(node.getContent(), JNGetVideoTree.DataBean .class);
                        Log.e("monitorBean", "convert: ============monitorBean==" +new Gson().toJson(monitorBean));
//                        JCglideUtils.loadCircleImage(getActivity(), monitorBean.getAvatarurl(), holder.getView(R.id.iv_user_head));
                        ((TextView)holder.getView(R.id.tv_user_name)).setText(getName(monitorBean));
                        holder.getView(R.id.iv_state).setVisibility(View.VISIBLE);
                        JCglideUtils.loadCircleImage(getActivity(), R.drawable.jc_chat_action_call_v, holder.getView(R.id.iv_user_head));

                        if (TextUtils.equals("11",monitorBean.getStatus())){//在线
                            holder.getView(R.id.iv_state).setBackgroundResource(R.drawable.ic_state_online);
                        }else {
                            holder.getView(R.id.iv_state).setBackgroundResource(R.drawable.ic_state_offline);
                        }

                        holder.getView(R.id.ll_friend).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showLoadding();
                                getMonitorData(monitorBean.getDeviceId(), monitorBean.getChannelId(), getName(monitorBean));
                            }
                        });
                    }catch (Exception e){e.printStackTrace();}
                }else {
                    try {
                        holder.getView(R.id.iv_state).setVisibility(View.GONE);
                        JNGetVideoTree.DataBean departBean = new Gson().fromJson(node.getContent(), JNGetVideoTree.DataBean.class);
                        ((TextView)holder.getView(R.id.tv_user_name)).setText(departBean.getName());
                        JCglideUtils.loadCircleImage(getActivity(), R.drawable.jc_organzation_depart, holder.getView(R.id.iv_user_head));
                        holder.getView(R.id.ll_friend).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle = new Bundle();
                                bundle.putString("zoningCode", node.getZoningCode());
                                JCbaseFragmentActivity.start(getActivity()
                                        , new JCOrganizationMonitorFragment(), departBean.getName(), bundle);
//                                if (null!=jcFragmentSelect){
//                                    jcFragmentSelect.onSelecte(JCbaseFragment.SELECTE_TYPE_MSG,node.getNid(), departBean.getDeptname());
//                                }
                            }
                        });
                    }catch (Exception e){e.printStackTrace();}
                }
            }
        };
        binding.recycleView.setAdapter(adapter);
    }

    private void getData(){
        JNBaseHttpUtils.getInstance().GetVideoTree(EasyHttp.post(this), zoningCode, JNGetVideoTree.Bean.class, new OnHttpListener<JNGetVideoTree.Bean>() {
            @Override
            public void onSucceed(JNGetVideoTree.Bean result) {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                    List<Node> list = new ArrayList<>();
                    if (null!=result.getData() && result.getData().size()>0){
                        for (int i = 0; i < result.getData().size(); i++) {
                            Node node = new Node();
                            node.setType(TextUtils.equals("5", result.getData().get(i).getOrgType())?0:1);
                            node.setLevel(0);
                            node.setZoningCode(result.getData().get(i).getZoningCode());
                            node.setContent(new Gson().toJson(result.getData().get(i)));
                            list.add(node);
                        }
                    }
                    datas.clear();
                    datas.addAll(list);
                    adapter.notifyDataSetChanged();
                }else {
                    JNBaseUtilManager.showToast("请求失败："+result.getMsg());
                }
                binding.listRefresh.finishRefresh();
                binding.listRefresh.finishLoadMore();
            }

            @Override
            public void onFail(Exception e) {
                JNBaseUtilManager.showToast("请求失败！");
                binding.listRefresh.finishRefresh();
                binding.listRefresh.finishLoadMore();
            }
        });
    }

    public String getName(JNGetVideoTree.DataBean  userBean){
        String name = "";
        if (null!=userBean){
            if (null!=userBean.getName()&&!TextUtils.isEmpty(userBean.getName())){
                name = userBean.getName();
            }else if (null!=userBean.getChannelName()&&!TextUtils.isEmpty(userBean.getChannelName())){
                name = userBean.getChannelName();
            }
        }
        return name;
    }

    /**
     *  获取监控播放地址
     *  成功后跳转播放页面
     *  C#
     *   http://api.jndv.org/ivs-server-api/api/play/start/37130201011181660001/37130201011320000004
     */
    public void getMonitorData(String deviceId, String channelId, String name) {
        JNBaseHttpUtils.getInstance().getMonitorData(EasyHttp.get(this), deviceId, channelId, new OnHttpListener<JNGetMonitorRtspApi.Bean>() {
            @Override
            public void onSucceed(JNGetMonitorRtspApi.Bean result) {
                try {
                    if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                        Intent intent = new Intent(getActivity(), JCMonitorPlayerActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("player_url", result.getData().getRtsp());
                        intent.putExtra("title", name);
                        startActivity(intent);
                    }else {
                        JCChatManager.showToast(result.getMsg());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                cancelLoadding();
            }
            @Override
            public void onFail(Exception e) {
                JCChatManager.showToast("请求失败，请重试！");
                cancelLoadding();
            }
        });
    }

}

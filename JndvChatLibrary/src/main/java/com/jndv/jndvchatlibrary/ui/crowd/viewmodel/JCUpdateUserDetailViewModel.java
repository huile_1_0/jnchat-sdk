package com.jndv.jndvchatlibrary.ui.crowd.viewmodel;

import android.text.TextUtils;
import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNGetUserDetails;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.http.api.JCDelFriendApi;
import com.jndv.jndvchatlibrary.http.api.JCGetUserDetailsApi;
import com.jndv.jndvchatlibrary.http.api.JCUpdateUserHeadApi;
import com.jndv.jndvchatlibrary.http.api.JCUpdateUserInfoApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCAdministratorKicksMemberApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGroupMemberInvitePerUpdateApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserDetailBean;


public class JCUpdateUserDetailViewModel extends ViewModel {

    private static final String TAG = "JCUpdateUserDetail";
    private MutableLiveData<JNGetUserDetails.UserDetailBean> userData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean> respondData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean> updateData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean> totalUpdateData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean> headUpdateData = new MutableLiveData<>();
    private String domainAddr = "192.168.2.205:8000";
    private ComponentActivity activity;

    public JCUpdateUserDetailViewModel() {
    }

    public void init(ComponentActivity activity) {
        this.activity = activity;
    }

    public MutableLiveData<JNGetUserDetails.UserDetailBean> getUserData() {
        return userData;
    }

    public MutableLiveData<JCRespondBean> getRespondData() {
        return respondData;
    }
    public MutableLiveData<JCRespondBean> getUpdateData() {
        return updateData;
    }
    public MutableLiveData<JCRespondBean> getTotalUpdateData() {
        return totalUpdateData;
    }
    public MutableLiveData<JCRespondBean> getHeadUpdateData() {
        return headUpdateData;
    }


    public JCUpdateUserDetailViewModel getUserDetail(String userId) {
        JNBaseHttpUtils.getInstance().GetUserDetail(EasyHttp.post(activity), userId, new OnHttpListener<JNGetUserDetails.Bean>() {
            @Override
            public void onSucceed(JNGetUserDetails.Bean result) {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode()))
                userData.postValue(result.getData());
            }

            @Override
            public void onFail(Exception e) {

            }
        });
//        EasyHttp.get(activity)
//                .api(new JCGetUserDetailsApi()
//                        .setUserId(userId)
//                )
//                .request(new OnHttpListener<JCRespondBean<JCUserDetailBean>>() {
//                    @Override
//                    public void onSucceed(JCRespondBean<JCUserDetailBean> result) {
//                        userData.postValue(result.getData());
//                    }
//
//                    @Override
//                    public void onFail(Exception e) {
//                        e.printStackTrace();
//                        Log.e(TAG, "onFail: " + e.getMessage());
//                    }
//                });

        return this;
    }

    public JCUpdateUserDetailViewModel updateUserInfo(String pic) {
        JCUpdateUserHeadApi api = new JCUpdateUserHeadApi()
                .setUserId(JNBasePreferenceSaves.getUserSipId())
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setPicName(JNBasePreferenceSaves.getUserName())
                .setPicUrl(pic);
        EasyHttp.put(activity)
                .api(api)
                .json(new Gson().toJson(api)).request(new OnHttpListener<JCRespondBean<String>>() {
            @Override
            public void onSucceed(JCRespondBean<String> result) {
                headUpdateData.postValue(result);
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
                JCChatManager.showToast("操作失败！");
                Log.e(TAG, "onFail: " + e.getMessage());
            }
        });

        return this;
    }

    public JCUpdateUserDetailViewModel updateTotalUserInfo(String pic,String name,String phoneNum,String gender,String age) {
        JCUpdateUserInfoApi api = new JCUpdateUserInfoApi()
                .setUserId(JNBasePreferenceSaves.getUserSipId())
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setPicUrl(pic)
                .setPicName(pic)
                .setNickName(name)
                .setMobile(phoneNum)
                .setGender(gender)
                .setAge(age);
        EasyHttp.put(activity)
                .api(api)
                .json(new Gson().toJson(api)).request(new OnHttpListener<JCRespondBean<String>>() {
            @Override
            public void onSucceed(JCRespondBean<String> result) {
                totalUpdateData.postValue(result);
            }

            @Override
            public void onFail(Exception e) {
                e.printStackTrace();
                JCChatManager.showToast("操作失败！");
                Log.e(TAG, "onFail: " + e.getMessage());
            }
        });
        return this;
    }




}
package com.jndv.jndvchatlibrary.ui.crowd.ui;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCNumbersData;
import com.jndv.jndvchatlibrary.ui.crowd.utils.CommonUtil;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import java.util.List;

public class SelectListAdapter extends BaseAdapter {
    private Context mContext;
    private List<JCNumbersData> mList;


    public SelectListAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public List<JCNumbersData> getmList() {
        return mList;
    }

    public void setmList(List<JCNumbersData> mList) {
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.group_chat_create_list_item, parent, false);

            viewHolder.imageView = convertView.findViewById(R.id.group_chat_create_list_item_icon);
            viewHolder.textView = convertView.findViewById(R.id.group_chat_create_list_item_name);
            viewHolder.checkBox = convertView.findViewById(R.id.group_chat_create_list_item_cb);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.checkBox.setChecked(mList.get(position).isGroupChatSelectState());
        viewHolder.checkBox.setClickable(false);//已经被邀请则不能编辑
        if (mList.get(position).getFriendName().isEmpty()){

            viewHolder.textView.setText(String.valueOf(mList.get(position).getFriendId()));
        }else {
            viewHolder.textView.setText(mList.get(position).getFriendName());
        }

        viewHolder.checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mList.get(position).setGroupChatSelectState(isChecked);
        });
        if (!CommonUtil.isDestroy((Activity) mContext)) {
            JCglideUtils.loadCircleImage(mContext, mList.get(position).getFriendPic(), viewHolder.imageView);

//            Glide.with(mContext)
//                    .load(mList.get(position).getFriendPic())
//                    .placeholder(R.mipmap.soil_iv_default)
//                    .error(R.mipmap.soil_iv_default)
//                    .into(viewHolder.imageView);
        }

        return convertView;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView textView;
        CheckBox checkBox;

    }
}


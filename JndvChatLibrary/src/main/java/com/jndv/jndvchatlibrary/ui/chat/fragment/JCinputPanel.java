package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgTextContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgVoiceContentBean;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCactionsManager;
import com.jndv.jnbaseutils.chat.listUi.JCbaseActions;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jnbaseutils.chat.listUi.JCcontainer;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCrecordButton;

import com.jndv.jndvchatlibrary.utils.JCstringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/14
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 底部信息输入组件
 */
public class JCinputPanel {

    private boolean isKeyboardShowed = false ;//软键盘是否显示
    private boolean isTextAudioSwitchShow = true;//是否需要显示文本和语音切换按钮

    protected JCcontainer container;
    protected View view;

    protected FrameLayout textAudioSwitchLayout; // 切换文本，语音按钮布局
    protected JCrecordButton audioRecordBtn; // 录音按钮,按住录音，松开发送的按钮
    protected EditText messageEditText;// 文本消息编辑框
    protected ImageView emoji_button;// 展开、关闭表情按钮

    protected ImageView buttonAudioMessage ;//切换到语音按钮（麦克风图标）
    protected ImageView buttonTextMessage ;//切换到文字按钮（键盘图标）

    protected ImageView buttonMoreFuntionInText ;//更多发送选项按钮
    protected Button buttonSendMessage ;//发送文本消息按钮

    protected LinearLayout moreActionsLayout ;//更多按钮列表

    public JCinputPanel(JCcontainer container, View view){
        this.container = container ;
        this.view = view ;
        init();
    }

    public void reload(JCcontainer container){
        this.container = container ;
    }

    private void init(){
        initViews();
        initInputBarListener();
        initTextEdit();
        initBQmm();
        initActions();
        initRecordListener();
    }
    private void initViews(){
        audioRecordBtn = (JCrecordButton) view.findViewById(R.id.audioRecord);
        moreActionsLayout = (LinearLayout) view.findViewById(R.id.actionsLayout);

        messageEditText = (EditText) view.findViewById(R.id.editTextMessage);
        textAudioSwitchLayout = (FrameLayout) view.findViewById(R.id.switchLayout);

        buttonAudioMessage = (ImageView) view.findViewById(R.id.buttonAudioMessage);
        buttonTextMessage = (ImageView) view.findViewById(R.id.buttonTextMessage);

        emoji_button = (ImageView) view.findViewById(R.id.emoji_button);
        buttonMoreFuntionInText = (ImageView) view.findViewById(R.id.buttonMoreFuntionInText);
        buttonSendMessage = (Button) view.findViewById(R.id.buttonSendMessage);

        if (isTextAudioSwitchShow){
            textAudioSwitchLayout.setVisibility(View.VISIBLE);
        }else {
            textAudioSwitchLayout.setVisibility(View.GONE);
        }
    }

    /**
     * 设置录音按钮监听
     */
    private void initRecordListener(){
        audioRecordBtn.setOnFinishedRecordListener(s -> {
            MediaPlayer mediaPlayer = new MediaPlayer();
            try {
                mediaPlayer.setDataSource(s);
                mediaPlayer.prepare();
                int duration = mediaPlayer.getDuration();
                int times = duration / 1000;
                if (times<1){
                    JCChatManager.showToast("时间太短");
//                    JCChatManager.showToast("时间不能少于1秒，请重试！");
                }else sendVoice(times, s);
                mediaPlayer.release();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void initActions(){
        List<JCbaseActions> actions = new ArrayList<>();
        List<JCbaseActions> inactions = new ArrayList<>();
        if (TextUtils.equals(JCSessionType.CHAT_GROUP, container.sessionType)){
            actions.addAll(JCchatFactory.getGroupActions());
        }else {
            actions.addAll(JCchatFactory.getP2PActions());
        }
        if (null!=actions && actions.size() > 0){
            for (int i = 0; i < actions.size(); i++) {
                JCbaseActions jCbaseActions = actions.get(i);
                jCbaseActions.setContainer(container);
                jCbaseActions.setIndex(i);
                inactions.add(jCbaseActions);
            }
        }

        JCactionsManager.init(moreActionsLayout, inactions);
    }

    private void initInputBarListener() {
        textAudioSwitchLayout.setOnClickListener(clickListener);

        buttonAudioMessage.setOnClickListener(clickListener);
        buttonTextMessage.setOnClickListener(clickListener);

        emoji_button.setOnClickListener(clickListener);

        buttonMoreFuntionInText.setOnClickListener(clickListener);
        buttonSendMessage.setOnClickListener(clickListener);
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initTextEdit() {
        messageEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        messageEditText.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    switchToTextLayout(true);
                }
                return false;
            }
        });

        messageEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                messageEditText.setHint("");
                checkSendButtonEnable(messageEditText);
            }
        });

        messageEditText.addTextChangedListener(new TextWatcher() {
            private int start;
            private int count;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                this.start = start;
                this.count = count;
                //设置联想窗口显示的位置
//                BQMM.getInstance().startShortcutPopupWindowByoffset(s.toString(),buttonSendMessage,0,40);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkSendButtonEnable(messageEditText);
//                MoonUtil.replaceEmoticons(container.activity, s, start, count);

//                if (watcher != null) {
//                    watcher.afterTextChanged(s, start, count);
//                }

                /**
                 * 限制输入字符串长度，5000字节
                 */
                int editEnd = messageEditText.getSelectionEnd();
                messageEditText.removeTextChangedListener(this);
                while (JCstringUtils.counterChars(s.toString()) > 5000 && editEnd > 0) {
                    s.delete(editEnd - 1, editEnd);
                    editEnd--;
                }
                messageEditText.setSelection(editEnd);
                messageEditText.addTextChangedListener(this);

//                sendTypingCommand();
            }
        });
    }

    private void initBQmm() {
        buttonSendMessage.setOnClickListener(v -> {
            final String content = messageEditText.getText().toString();
            if (!TextUtils.isEmpty(content)) {
//                if(container.sessionType==JCSessionType.CHAT_GROUP)判断是否群组聊天和是否还在群里
                JCMsgTextContentBean bean = new JCMsgTextContentBean(content);
//
                JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                        new Gson().toJson(bean),""+ JCmessageType.TEXT, container.jCsessionChatInfoBean.getSessionChatId(), JNSpUtils.getString(container.activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
                , JNBasePreferenceSaves.getSipAddress(), container.jCsessionChatInfoBean.getSessionChatId(), container.jCsessionChatInfoBean.getSessionaddress()
                , ""+container.sessionType, JNBasePreferenceSaves.getJavaAddress(),container.jCsessionChatInfoBean.getSessionaddressJava());

                container.jcviewPanelInterface.sendMessage(jCimMessageBean);
                Log.d("message", "sendMessageToBuddy: "+new Gson().toJson(jCimMessageBean));

                messageEditText.setText(null);

            }
        });

    }

    /**
     * 显示发送或更多
     *
     * @param editText
     */
    private void checkSendButtonEnable(EditText editText) {
//        if (isRobotSession) {
//            return;
//        }
        String textMessage = editText.getText().toString();

        Log.i("-----", "checkSendButtonEnable: ------textMessage------"+textMessage);
//        if (!TextUtils.isEmpty(StringUtil.removeBlanks(textMessage)) && editText.hasFocus()) {//删除了空格
        if (!TextUtils.isEmpty(textMessage) ) {//&& editText.hasFocus()
            buttonMoreFuntionInText.setVisibility(View.GONE);
            buttonSendMessage.setVisibility(View.VISIBLE);
        } else {
            buttonSendMessage.setVisibility(View.GONE);
            buttonMoreFuntionInText.setVisibility(View.VISIBLE);
        }
    }

    // 点击edittext，切换键盘和更多布局
    private void switchToTextLayout(boolean needShowInput) {
        hideActionPanelLayout();

        audioRecordBtn.setVisibility(View.GONE);
        messageEditText.setVisibility(View.VISIBLE);

        buttonTextMessage.setVisibility(View.GONE);
        buttonAudioMessage.setVisibility(View.VISIBLE);

        buttonSendMessage.setVisibility(View.VISIBLE);
        buttonMoreFuntionInText.setVisibility(View.GONE);

        if (needShowInput) {
//            uiHandler.postDelayed(showTextRunnable, SHOW_LAYOUT_DELAY);
            showInputMethod(messageEditText);
        } else {
            hideInputMethod();
        }
    }

    // 切换成音频，收起键盘，按钮切换成键盘
    private void switchToAudioLayout() {
        messageEditText.setVisibility(View.GONE);
        audioRecordBtn.setVisibility(View.VISIBLE);
        hideInputMethod();
//        hideEmojiLayout();
        hideActionPanelLayout();

        buttonAudioMessage.setVisibility(View.GONE);
        buttonTextMessage.setVisibility(View.VISIBLE);
    }


    // 隐藏键盘布局
    private void hideInputMethod() {
        isKeyboardShowed = false;
//        uiHandler.removeCallbacks(showTextRunnable);
        InputMethodManager imm = (InputMethodManager) container.activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(messageEditText.getWindowToken(), 0);
        messageEditText.clearFocus();
    }

    // 显示键盘布局
    private void showInputMethod(EditText editTextMessage) {
        editTextMessage.requestFocus();
        //如果已经显示,则继续操作时不需要把光标定位到最后
        if (!isKeyboardShowed) {
            editTextMessage.setSelection(editTextMessage.getText().length());
            isKeyboardShowed = true;
        }

        InputMethodManager imm = (InputMethodManager) container.activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editTextMessage, 0);

//        container.moduleProxy.onInputPanelExpand();//这里是放开给MessageFragment的接口，如果需要再加
    }

    // 显示更多布局
    private void showActionPanelLayout() {
        moreActionsLayout.setVisibility(View.VISIBLE);
        hideInputMethod();

//        container.moduleProxy.onInputPanelExpand();
    }

    // 点击“+”号按钮，切换更多布局和键盘
    private void toggleActionPanelLayout() {
        if (null!= moreActionsLayout && moreActionsLayout.getVisibility() == View.GONE) {
            showActionPanelLayout();
        } else {
            hideActionPanelLayout();
        }
    }

    private void hideActionPanelLayout(){
        if (null!=moreActionsLayout)moreActionsLayout.setVisibility(View.GONE);
    }

    private void hideAllLayout(){
        hideActionPanelLayout();
        hideInputMethod();
    }

    /**
     * 点击事件
     */
    View.OnClickListener clickListener = v -> {
        int id = v.getId();
        if (id == R.id.buttonAudioMessage) {//切换到语音按钮
            switchToAudioLayout();// 显示语音发送的布局
        } else if (id == R.id.buttonTextMessage) {//切换到文本按钮
            switchToTextLayout(true);// 显示文本发送的布局
        } else if (id == R.id.buttonMoreFuntionInText) {//点击展开，关闭更多发送选项
            toggleActionPanelLayout();
        }
    };

    protected void sendVoice( int seconds,  String filePath) {
        StringBuilder time=new StringBuilder();
        if (seconds<60){
            time.append(seconds).append("″");
        }else if (seconds<60*60){
            time.append(seconds/60).append("′").append(seconds%60).append("″");
        }

        JCMsgVoiceContentBean voiceContentBean = new JCMsgVoiceContentBean(time.toString(), filePath);
        String contentText = new Gson().toJson(voiceContentBean);
        String contentBase64 = Base64.encodeToString(contentText.getBytes(),Base64.NO_WRAP);
//
            JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                    contentBase64,""+ JCmessageType.VOICE, container.jCsessionChatInfoBean.getSessionChatId(), JNSpUtils.getString(container.activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
                    , JNBasePreferenceSaves.getSipAddress(), container.jCsessionChatInfoBean.getSessionChatId(), container.jCsessionChatInfoBean.getSessionaddress()
                    , ""+container.sessionType, JNBasePreferenceSaves.getJavaAddress(),container.jCsessionChatInfoBean.getSessionaddressJava());

            container.jcviewPanelInterface.sendFileMessage(jCimMessageBean,0);

            Log.e("sendVoice",filePath);
            Log.e("sendVoice",seconds+"++++");

        JCFilesUpUtils.upCommonFile(filePath,JCmessageType.VOICE, new OnHttpListener<String>() {
            @Override
            public void onSucceed(String url) {
                try {
                    if (TextUtils.isEmpty(url)){
                        JCThreadManager.onMainHandler(() -> container.jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(), JCmessageStatusType.sendFail));
                    }else {
                        voiceContentBean.setUrl(JNBaseUtilManager.getFilesUrl(url));
                        String msgEntityText = new Gson().toJson(voiceContentBean);
                        jCimMessageBean.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
                        JCThreadManager.onMainHandler(() -> container.jcviewPanelInterface.sendFileMessage(jCimMessageBean,1));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(Exception e) {
                JCThreadManager.onMainHandler(() -> container.jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(),JCmessageStatusType.sendFail));
            }
        });
    }

}

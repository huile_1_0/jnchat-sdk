package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.ehome.sipservice.SipServiceCommand;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.ui.base.JNBaseFragment;
import com.jndv.jnbaseutils.ui.base.JNBaseFragmentActivity;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.MQTTManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcchatBinding;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jnbaseutils.chat.listUi.JCsessionChatInfoBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCmsgModelItem;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgNotifyContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCMessageReadUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCNotifyType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jnbaseutils.chat.listUi.JCcontainer;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jnbaseutils.chat.listUi.JCviewPanelInterface;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCMessageListener;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCmessageListenerManager;
import com.jndv.jnbaseutils.utils.MediaPlayerUtil;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;
import com.jndv.jnbaseutils.chat.JCSessionType;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/2/11
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 聊天页面Fragment
 */
public class JCchatFragment extends JNBaseFragment implements JCviewPanelInterface {
//public class JCchatFragment extends JCbaseFragment implements JCviewPanelInterface {
    private JCchatViewModel jCchatViewModel;
    private FragmentJcchatBinding binding;
    private String otherId;//对方ID，用户id或者群组id
    private String sessionaddress;//会话SIP域地址
    private String sessionaddressJava;//会话JAVA域地址
    private long readLastTime;//最新已读消息发送时间
    private String sessionType;//会话类型
    private int notLineNum = 0;//
    private int sessionId = 0;//会话id
    protected JCmessageListPanel messageListPanel;
    protected JCinputPanel jCinputPanel;
    private int newMsgNum = 0;//未查看到的新消息条数
    private String lastMsgId="-1";
    int groupStatus;//群聊状态

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        jCchatViewModel =
                new ViewModelProvider(this).get(JCchatViewModel.class);

        binding = FragmentJcchatBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);
        EventBus.getDefault().register(this);
        return root;
    }

    private void init(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            otherId = bundle.getString(JCEntityUtils.SESSION_OTHER_ID);
            sessionType = bundle.getString(JCEntityUtils.SESSION_TYPE);
            sessionId = bundle.getInt(JCEntityUtils.SESSION_ID);
            sessionaddress = bundle.getString(JCEntityUtils.SESSION_ADDRESS);
            sessionaddressJava = bundle.getString(JCEntityUtils.SESSION_ADDRESS_JAVA);
            readLastTime = bundle.getLong(JCEntityUtils.SESSION_READ_LAST_TIME);
            notLineNum = bundle.getInt(JCEntityUtils.SESSION_NOT_LINE_MSG_NUM, 0);
            groupStatus=bundle.getInt(JCEntityUtils.GROUP_STATUS,0);
            jCchatViewModel.init(otherId, "" + sessionId, "" + sessionType, sessionaddress, this);
        }
        if (null == otherId) otherId = "";

        Log.e("JCchatFragment", "init: ====otherId=" + otherId);
        Log.e("JCchatFragment", "init: ====sessionType=" + sessionType);
        Log.e("JCchatFragment", "init: ====sId=" + sessionId);
        Log.e("JCchatFragment", "init: ====sessionaddress=" + sessionaddress);

        if (JCSessionType.CHAT_SYSTEM.equals(sessionType)){
            view.findViewById(R.id.messageToolBox).setVisibility(View.GONE);
        }else if(JCSessionType.CHAT_GROUP.equals(sessionType) && groupStatus!=0){
            view.findViewById(R.id.messageToolBox).setVisibility(View.GONE);
        }

        JCsessionChatInfoBean bean = new JCsessionChatInfoBean(otherId, sessionaddress, sessionaddressJava);
        JCcontainer container = new JCcontainer(getActivity(), "" + sessionId, sessionType, this
                , bean, notLineNum);
        initData();
        if (messageListPanel == null) {
            messageListPanel = new JCmessageListPanel(container, view);
        } else {
//            messageListPanel.update(container,view);
        }

        if (jCinputPanel == null) {
            jCinputPanel = new JCinputPanel(container, view);
        } else {
            jCinputPanel.reload(container);
        }
        initDragView();
        initDragBtmView();
        String addreass = "";
        try {
            addreass = sessionaddress;
//            addreass = sessionaddress.substring(0, sessionaddress.indexOf(":"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String key = otherId + "_" + sessionType + "_" + addreass;
//        1050_1_192.168.1.5:5060
        JNLogUtil.e("JCchatFragment", "========addMessageListener=======key=" + key);
        MQTTManager.getInstance().addMqttMsgListenerList(key, new MQTTManager.MqttMsgListener() {
            @Override
            public void onReceiveMessage(JCbaseIMMessage message, int isLook) {
                JNLogUtil.e("JCchatFragment", "==onReceiveMessage==MQTTManager==01===key=" + key);
                initReceiveMessage(message);
            }
        });
        JCmessageListenerManager.addMessageListener(key, new JCMessageListener() {
            @Override
            public void onReceiveMessage(JCbaseIMMessage message, int isLoo) {
                JNLogUtil.e("JCchatFragment", "==onReceiveMessage====01===key=" + key);
                initReceiveMessage(message);
            }

            @Override
            public void onSendMessageState(JCbaseIMMessage message, int state) {
                try {
                    readLastTime = Long.parseLong(message.getSendTime());
                } catch (Exception e) {

                }
                JNLogUtil.e("JCchatFragment", "JCmyPjSipMessageListener===onSendMessageState++ip=" + message.getMsgID());
                try {
                    if (TextUtils.equals("" + JCmessageType.READACK, message.getMsgType())) {
                        JNLogUtil.e("===onSendMessageState====JCmessageType.READACK=======");
                        return;
                    }
                    QueryBuilder<JCimMessageBean> builder = JCobjectBox.get().boxFor(JCimMessageBean.class).query();
                    JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, message.getMsgID(), QueryBuilder.StringOrder.CASE_SENSITIVE)
//                        .equal(JCimMessageBean_.sessionType, sessionType,QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .build().findFirst();
                    if (null != localMessage) {
                        localMessage.setStatus("" + state);
//                        JCobjectBox.get().boxFor(JCimMessageBean.class).put(localMessage);
                        messageListPanel.updateData(localMessage);
//                        EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_SESSION_LIST_UPDATE_MSG, "" + sessionId, localMessage));
                    } else {
                        messageListPanel.updateData(message);
//                        EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_SESSION_LIST_UPDATE_MSG, "" + sessionId, message));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    messageListPanel.updateData(message);
//                    EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_SESSION_LIST_UPDATE_MSG, "" + sessionId, message));
                }
//                JCMessageReadUtils.updateGroupMsgReadAll(JCchatFragment.this, message, new JCMessageReadUtils.JCMessageReadListenner() {
//                    @Override
//                    public void onUpdateMessage(JCbaseIMMessage message) {
//                        JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll==17=="+message.getReadNumAll()+"===="+message.getRead_num());
//                        messageListPanel.updateData(message);
//                    }
//                });
            }
        });
    }

    private void initReceiveMessage(JCbaseIMMessage message){
        JNLogUtil.e("JCchatFragment", "==onReceiveMessage==getMsgID=" + message.getMsgID());
        if(message.getMsgID().equals(lastMsgId)){
            return;
        }
        lastMsgId=message.getMsgID();
        try {
            readLastTime = Long.parseLong(message.getSendTime());
        } catch (Exception e) {

        }
        try {
            JCimMessageBean jCimMessageBean = (JCimMessageBean) message;
            JNLogUtil.e("JCchatFragment", "==onReceiveMessage==getId=" + jCimMessageBean.getId());
//                    jCimMessageBean.setReceiveTime("" + System.currentTimeMillis());
            JNLogUtil.e("JCchatFragment", "==onReceiveMessage====jCimMessageBean.getMsgType()==" + jCimMessageBean.getMsgType());
            if (TextUtils.equals("" + JCmessageType.READACK, jCimMessageBean.getMsgType())) {
                //这里是已读回执，需要更新已读数量
                jCchatViewModel.updateDataRead(message);
            } else if (TextUtils.equals("" + JCmessageType.NOTIFY, message.getMsgType())) {
                jCchatViewModel.updateData(JCMessageUtils.getNotifyMsg(message));
                JCMsgNotifyContentBean bean = new Gson().fromJson(new String(Base64.decode(message.getContent(), Base64.NO_WRAP)), JCMsgNotifyContentBean.class);

                if (TextUtils.equals(bean.getType(), JCNotifyType.NOTIFY_SingleNotice + "")) {
                    jCchatViewModel.addData(message);
                    jCimMessageBean.setId(0);
                    JCobjectBox.get().boxFor(JCimMessageBean.class).put(jCimMessageBean);
                }
            } else {
                jCchatViewModel.addData(message);
                jCimMessageBean.setId(0);
                JCobjectBox.get().boxFor(JCimMessageBean.class).put(jCimMessageBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onDestroy() {
        try {
//            readLastTime = Long.parseLong(message.getSendTime());
            JCSessionListBean sessionListBean = null;
            if (0 != sessionId) {
                sessionListBean = JCSessionUtil.getJCSessionListBean(sessionId);
            } else {
                sessionListBean = JCSessionUtil.getJCSessionListBean(otherId, sessionType, sessionaddress);
            }
            if (null != sessionListBean) {
                sessionListBean.setFirst_time("" + readLastTime);
                Box<JCSessionListBean> sessionBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                sessionBox.put(sessionListBean);
                QueryBuilder<JCSessionListBean> builder = sessionBox.query();
                JCSessionListBean dataBean = builder.equal(JCSessionListBean_.session_id
                        , sessionId).build().findFirst();
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_INFO, "", dataBean));
            }
        } catch (Exception e) {

        }
        super.onDestroy();
    }

    /**
     * 设置数据监听
     */
    private void initData(){
        if (null!=jCchatViewModel)jCchatViewModel.getDataChange().observeForever( new Observer<JCmsgModelItem>() {
            @Override
            public void onChanged(JCmsgModelItem jCmsgModelItem) {
                if (null!=jCmsgModelItem) {
                    if (null!=messageListPanel)messageListPanel.initData(jCmsgModelItem);
                }
            }
        });
    }

    @Override
    public void hindDragBtmView() {
        try {
            newMsgNum = 0;

            binding.llDragBtnBtm.setVisibility(View.GONE);
        }catch (Exception e){
            JNLogUtil.e("====hindDragBtmView======", e);
        }
    }

    @Override
    public void addDragBtmView() {
        try {
            if (binding.llDragBtnBtm.getVisibility() != View.VISIBLE)
                binding.llDragBtnBtm.setVisibility(View.VISIBLE);
            binding.llDragBtnBtm.setText(++newMsgNum + "条新消息");
        }catch (Exception e){
            JNLogUtil.e("====hindDragBtmView======", e);
        }
    }

    public void initDragBtmView() {
        binding.llDragBtnBtm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messageListPanel.gotoBottom();
                newMsgNum = 0;
                binding.llDragBtnBtm.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void initDragViewData(long time) {
        try {
            if (binding.llDragBtn.getVisibility() == View.VISIBLE) {
                if (-1 == time || time <= readLastTime) {
                    binding.llDragBtn.setVisibility(View.GONE);
                    notLineNum = 0;
                }
            }
        } catch (Exception e) {
            JNLogUtil.e("==initDragViewData====", e);
        }
    }

    @Override
    public void makeMethodCall(String type, Object... objects) {
        if (TextUtils.equals(type, "getMessageData")){
            jCchatViewModel.getMessageData();
        }else if (TextUtils.equals(type, "getNewMessage")){
            long time = System.currentTimeMillis();
            int addType = 1 ;
            try {
                time = (long) objects[0];
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                addType = (int) objects[1];
            }catch (Exception e){
                e.printStackTrace();
            }
            jCchatViewModel.getNewMessage(time, addType);
        }else if (TextUtils.equals(type, "getOldMessage")){
            long time = System.currentTimeMillis();
            try {
                time = (long) objects[0];
            }catch (Exception e){
                e.printStackTrace();
            }
            jCchatViewModel.getOldMessage(time);
        }
    }

    public void initDragView() {
        if (notLineNum > JCChatManager.pageSize)
            binding.llDragBtn.setVisibility(View.VISIBLE);
        binding.llDragBtn.setText(notLineNum + "条未读");
        binding.llDragBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JNLogUtil.e("20220301", "onClick: =========llDragBtn=========onClick=");
                jCchatViewModel.getNewMessage(readLastTime, 0);
                binding.llDragBtn.setVisibility(View.GONE);
                notLineNum = 0;
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void codeEvent(JNCodeEvent codeEvent) {
        switch (codeEvent.getCode()) {
            case JNEventBusType
                    .CODE_MESSAGE_WITHDRAW://消息撤销
                messageListPanel.withdrawMessage((String) codeEvent.getData());
                break;
            case JNEventBusType.CODE_MESSAGE_CHANGE_UI://消息撤销
                messageListPanel.gotoBottomIs(300);
                break;
            case JNEventBusType
                    .CODE_MESSAGE_DELETE://消息删除
                try {
                    JNLogUtil.e("===CODE_MESSAGE_DELETE===getData==" + (String) codeEvent.getData());
                    JNLogUtil.e("===CODE_MESSAGE_DELETE===getMessage==" + codeEvent.getMessage());
                    JNLogUtil.e("===CODE_MESSAGE_DELETE===otherId==" + otherId);
                    messageListPanel.deleteMessage((String) codeEvent.getData());
                    JNLogUtil.e("===CODE_MESSAGE_DELETE===go==remove==");
                    Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                    QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
                    JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, codeEvent.getData().toString(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .build().findFirst();
                    if (null != localMessage) {
                        boolean isremove = imMsgBox.remove(localMessage);
                        JNLogUtil.e("===CODE_MESSAGE_DELETE===isremove==" + isremove);
                    }

                }catch (Exception e){
                    JNLogUtil.e("====", e);
                }

                break;
            case JNEventBusType
                    .CODE_MESSAGE_READ_USER_LIST://跳转消息已读未读人员列表
//                if (null!=jcActivityFragmentInterface){
                try {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(JCMsgReadFragment.MESSAGEITEM, (JCimMessageBean) codeEvent.getData());
                    JCMsgReadFragment contentFragment = new JCMsgReadFragment();
//                contentFragment.setArguments(bundle);
                    JNBaseFragmentActivity.start(getActivity(), contentFragment, "已读人员", bundle);
//                    JCbaseFragmentActivity.start(getActivity(), contentFragment, "已读人员", bundle);
                }catch (Exception e ){
                    JNLogUtil.e("====", e);
                }

//                }
                break;

            case JNEventBusType.CODE_MESSAGE_UPDATE:
                try {
                    Log.e("downloadFile", "onComplete: ==0010=" +codeEvent.getMessage());
//                    Log.e("codeEvent", codeEvent.getMessage());
                    JCimMessageBean bean = (JCimMessageBean) codeEvent.getData();
                    messageListPanel.updateData(bean);
                }catch (Exception e){
                    JNLogUtil.e("====", e);
                }

                break;
            case JNEventBusType.CODE_MESSAGE_TRANSMIT://
//                Log.e("codeEvent", codeEvent.getMessage());
//                transmitMessage((JCimMessageBean) codeEvent.getData());
                try {
                    JCimMessageBean messageBean = (JCimMessageBean) codeEvent.getData();
                    if (TextUtils.equals(sessionaddress, messageBean.getSessionRealm())
                            && TextUtils.equals(otherId, messageBean.getToSipID())){
                        messageListPanel.addData(messageBean);
                    }
                }catch (Exception e){
                    JNLogUtil.e("====", e);
                }

                break;

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        String key = otherId + "_" + sessionType + "_" + sessionaddress;
        JCmessageListenerManager.removeMessageListener(key);
        MQTTManager.getInstance().removeMqttMsgListenerList(key);
        EventBus.getDefault().unregister(this);
        MediaPlayerUtil.release();
        MediaPlayerUtil.getInstances().getAnimationMap().clear();
    }

    @Override
    public boolean sendMessage(JCbaseIMMessage msg) {
        if (JCSessionUtil.isGroupStateOk(sessionId)) {
            try {
                messageListPanel.gotoBottom();
                ((JCimMessageBean) msg).setContent(Base64.encodeToString(msg.getContent().getBytes(), Base64.NO_WRAP));
                JNLogUtil.e("sendMessage: ======================sendMessage=======" + "sip:" + otherId + "@" + sessionaddress);
                String userID = JNSpUtils.getString(getActivity(), "myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
                if (TextUtils.equals("" + JCmessageType.READACK, msg.getMsgType())) {
                    ((JCimMessageBean) msg).setId(0);
                    if (sessionType.equals(JCSessionType.CHAT_GROUP)) {
                        Log.d("SipService","msg==chat=01=");
                        SipServiceCommand.sendMessageToBuddy(getActivity(), new Gson().toJson(msg), userID
                                , "sip:" + otherId + "@" + sessionaddress, "text/groupmsgack+json", false);
                    } else {
                        Log.d("SipService","msg==chat=02=");
                        SipServiceCommand.sendMessageToBuddy(getActivity(), new Gson().toJson(msg), userID
                                , "sip:" + otherId + "@" + sessionaddress, "text/p2pmsgack+json", false);
                    }
                } else {
                    Box<JCimMessageBean> beanBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                    JCimMessageBean bean = ((JCimMessageBean) msg);
                    beanBox.put(bean);
                    messageListPanel.addData(bean);
                    ((JCimMessageBean) msg).setId(0);
                    Log.d("SipService","msg==chat=03=");
                    HashMap<String, String> map = new HashMap<>();
                    map.put("android-new","13579");
                    SipServiceCommand.sendMessageToBuddy(getActivity(), new Gson().toJson(msg), userID
                            , "sip:" + otherId + "@" + sessionaddress, JCSessionType.getTypeSIP(sessionType), false, map);
                }
//                messageListPanel.gotoBottom();
            } catch (Exception e) {
                JNLogUtil.e("==JCChatFragment==sendFileMessage==", e);
            }
        }
        return false;
    }

    @Override
    public boolean sendFileMessage(JCbaseIMMessage msg, int type) {
        if (JCSessionUtil.isGroupStateOk(sessionId)) {
            try {
                messageListPanel.gotoBottom();
                Box<JCimMessageBean> beanBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                if (0 == type) {
                    beanBox.put((JCimMessageBean) msg);
                    messageListPanel.addData(msg);
                } else {
                    String userID = JNSpUtils.getString(getActivity(), "myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
                    JCimMessageBean bean = beanBox.query().equal(JCimMessageBean_.msgID, msg.getMsgID(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .build().findFirst();
                    bean.setContent(msg.getContent());
                    beanBox.put(bean);
                    messageListPanel.updateData(bean);
                    ((JCimMessageBean) msg).setId(0);
                    Log.d("SipService","msg==chat=04=");
                    SipServiceCommand.sendMessageToBuddy(getActivity(), new Gson().toJson(msg), userID
                            , "sip:" + otherId + "@" + sessionaddress, JCSessionType.getTypeSIP(sessionType), false);
                }
            } catch (Exception e) {
                JNLogUtil.e("==JCChatFragment==sendFileMessage==", e);
            }
        }
        return false;
    }

    @Override
    public boolean updateMessageState(String msgID, int state) {
        try {
            JNLogUtil.d("==JCFileAction==updateMessageState==msgID==" + msgID + "==state==" + state);
            JNLogUtil.d("==JCChatFragment==updateMessageState==msgID==" + msgID + "==state==" + state);
            Box<JCimMessageBean> box = JCobjectBox.get().boxFor(JCimMessageBean.class);
            JCimMessageBean bean = box.query().equal(JCimMessageBean_.msgID, msgID, QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();
            bean.setStatus("" + state);
            box.put(bean);
            messageListPanel.updateData(bean);
        } catch (Exception e) {
            JNLogUtil.e("==JCFileAction==updateMessageState==", e);
            JNLogUtil.e("==JCChatFragment==updateMessageState==", e);
        }

        return false;
    }
}


package com.jndv.jndvchatlibrary.ui.chat.utils.refresh;

/**
 * Created by limxing on 16/7/23.
 *
 * https://github.com/limxing
 * Blog: http://www.leefeng.me
 */
public interface JCOnItemClickListener {
    void onClick(int position);

    void onLongClick(int po);
}

package com.jndv.jndvchatlibrary.ui.chat.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCIntercomApplyBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMeetingBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgNotifyContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgTextContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgWebNoticeContentBean;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 会话列表adapter
 *
 * */
public class JCchatListAdapter extends RecyclerView.Adapter<JCchatListAdapter.ViewHolder> {
    private static final String TAG = "JCchatListAdapter";
    private Context mContext;
    private List<JCSessionListBean>list=new ArrayList<>();

    public JCchatListAdapter(Context mContext, List<JCSessionListBean> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.jc_item_message,parent,false));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.e(TAG, "onBindViewHolder: "+list.get(position).getOther_name()+"===="+list.get(position).getOther_id());
        if (list.get(position).getType().equals(JCSessionType.CHAT_SYSTEM)){
            switch (list.get(position).getOther_id()){
                case "10001"://事件
                    holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_shijian));
                    break;
                case "10002"://任务
                    holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_renwu));
                    break;
                case "10003"://审批
                    holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_shenpi));
                    break;
                case "10004"://通知公告
                    holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_notify));
                    break;
                case "10005"://调度
                    holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_diaodu));
                    break;
                case "10006"://告警
                    holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_gaojing));
                    break;
            }
        }else if (list.get(position).getType().equals(JCSessionType.CHAT_SYSTEM_NOTIFY)){
            holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_system));
        }else if (list.get(position).getType().equals(JCSessionType.SESSION_CLUSTER_INTERCOM)){
            holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_system));
        }else if(list.get(position).getType().equals(JCSessionType.CHAT_TTS)){
            holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_gaojing));
        }else if(list.get(position).getType().equals(JCSessionType.MEETING)){
            holder.iv_head.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.jc_list_renwu));
        }else {
            JCglideUtils.loadCircleImage(mContext, list.get(position).getOther_pic(), holder.iv_head);
        }

        holder.nameTv.setText(getName(position));
        JNLogUtil.e("=====getSession========getLast_time==="+list.get(position).getLast_time());
        if (list.get(position).getLast_time()<=0){
            JNLogUtil.e("====getSession=========getCreate_time==="+list.get(position).getCreate_time());
            holder.timeTv.setText(getTime(list.get(position).getCreate_time()));
        }else {
            holder.timeTv.setText(getTime(""+list.get(position).getLast_time()));
        }

        holder.msgTv.setText(getMessage(position));
        if (1==list.get(position).getIs_top()){
            holder.session_item.setBackgroundColor(mContext.getResources().getColor(R.color.gray_bg));
        }else {
            holder.session_item.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        if (list.get(position).getUnread_number()>0){
            holder.tv_no_red_num.setVisibility(View.VISIBLE);
            if (1!=list.get(position).getIs_no_disturb()){
                holder.tv_no_red_num.setText(""+list.get(position).getUnread_number());
            }else {
                holder.tv_no_red_num.setText(" ");
            }
        }else {
            holder.tv_no_red_num.setVisibility(View.GONE);
            if(list.get(position).getType().equals(JCSessionType.CHAT_SYSTEM_NOTIFY)){
                holder.msgTv.setText("");
            }
        }
        if (1==list.get(position).getIs_no_disturb()){
            holder.iv_disturb.setVisibility(View.VISIBLE);
        }else {
            holder.iv_disturb.setVisibility(View.GONE);
        }
    }

    private String getMessage(int pot){
        JNLogUtil.e("======pot=="+pot);
        String msg = "";
        try {
            if(TextUtils.equals(list.get(pot).getType(),JCSessionType.MEETING)){
                if(!TextUtils.isEmpty(list.get(pot).getFirst_content())){
                    String decodeContent = new String(Base64.decode(list.get(pot).getFirst_content(),Base64.NO_WRAP));
                    JCMeetingBean jcMeetingBean = new Gson().fromJson(decodeContent, JCMeetingBean.class);
                    msg = jcMeetingBean.extend;
                }
            }else {
                String message = list.get(pot).getFirst_content();
//                {"create_time":"1726106588","first_content":"","first_time":"","id":36,"is_no_disturb":0,"is_top":0,"last_time":0,"other_id":"10577","other_name":"0003","other_pic":"","remark":"","session_id":33614,"session_party_domain_addr":"192.168.7.15:5062","session_party_domain_addr_user":"192.168.7.10:8000","status":0,"type":"2","unread_number":0,"user_id":"990215546690123"}
                JNLogUtil.e("==getSession====message==" + new Gson().toJson(list.get(pot)));
                JNLogUtil.e("==getSession====message==" + message);
                JCimMessageBean jCimMessageBean = new Gson().fromJson(message, JCimMessageBean.class);
                msg = getMsgStr(jCimMessageBean);
            }
        }catch (Exception e){
            JNLogUtil.e("==getSession==02==Exception==", e);
        }
        try {
            msg = msg.trim();
            msg = msg.replace("\r","");
            msg = msg.replace("\r\n","\n");
        }catch (Exception e){
            e.printStackTrace();
        }
        return msg;
    }

    private String getMsgStr(JCimMessageBean jCimMessageBean){
        String info = "";
        try {
            JNLogUtil.e("==getSession====MsgType==" + jCimMessageBean.getMsgType());
            if (null!=jCimMessageBean){
                if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.WITHDRAW)){
                    info = "撤回了一条消息";
                } else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.TEXT)){
                    String decodeContent = new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP));
                    JCMsgTextContentBean jcMsgTextContentBean = new Gson().fromJson(decodeContent, JCMsgTextContentBean.class);
                    info = jcMsgTextContentBean.getText();
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.IMG)){
                    info = "[图片]";
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.CARD)){
                    info = "[名片]";
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.VOICE)){
                    info = "[语音消息]";
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.VIDEO)){
                    info = "[视频消息]";
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.AUDIO_VIDEO_RECORD)){
                    info = "[音视频消息记录]";
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.FILE)){
                    info = "[文件]";
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.LOCATION)){
                    info = "[位置消息]";
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.NOTIFY)){
                    String decodeContent = new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP));
                    JNLogUtil.e("通知消息"+"======message=="+decodeContent);
                    JCMsgNotifyContentBean notifyContentBean = new Gson().fromJson(decodeContent, JCMsgNotifyContentBean.class);
                    info = notifyContentBean.getText();
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.SYSTEM_ALARM)//告警
//                ){
                ||TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.SYSTEM_DISPATCH)//
                ||TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.SYSTEM_EVENT)//事件
                ||TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.SYSTEM_NOTICE)//公告
                ||TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.SYSTEM_TASK)){//任务
                    JCMsgWebNoticeContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(jCimMessageBean.getContent())
                            , JCMsgWebNoticeContentBean.class);
                    info = "一条未处理消息！";
                    if (null!=bean){
                        if (null==bean.getSipParameterJson()||null==bean.getSipParameterJson().getInfo()
                                ||bean.getSipParameterJson().getInfo().isEmpty()){
                            info = bean.getType();
                        }else {
                            info = bean.getSipParameterJson().getInfo();
                        }
                    }
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), JCmessageType.MSG_INTERCOM_308)){
                    String str = JNContentEncryptUtils.decryptContent(jCimMessageBean.getContent());
                    Log.e(TAG, "getMsgStr: ==" +str);//{"groupnum":"124"}
                    JCIntercomApplyBean bean = new Gson().fromJson(str
                            , JCIntercomApplyBean.class);
                    String fromName = bean.getFromName();
                    String groupname = bean.getGroupname();
                    if (null==fromName)fromName="";
                    if (null==groupname)groupname="";
                    info = fromName+"邀请你加入对讲频道"+groupname;
                }else {
                    info="一条未处理消息！";
                    JSONObject jsonObject=new JSONObject(JNContentEncryptUtils.decryptContent(jCimMessageBean.getContent()));
                    String text = jsonObject.getString("text");
                    if(!TextUtils.isEmpty(text)){
                        info=text;
                    }
                }
            }
        }catch (Exception e){
//            e.printStackTrace();
            Log.e(TAG, "==getSession===008===getMsgStr: ", e);
        }
        return info;
    }

    private String getName(int pot){
        return getName(list.get(pot));
    }
    public static String getName(JCSessionListBean dataBean){
        if(null==dataBean)return "";
        String name =dataBean.getOther_id();
        try {
            if (!TextUtils.isEmpty(dataBean.getRemarks())){
                name = dataBean.getRemarks();
            }else if (!TextUtils.isEmpty(dataBean.getOther_name())){
                name = dataBean.getOther_name();
            }else {
                String message = dataBean.getFirst_content();
                JCimMessageBean jCimMessageBean = new Gson().fromJson(message, JCimMessageBean.class);
                //这里的第一个if为了处理群聊邀请name错误显示成sip号的情况
                if(TextUtils.isEmpty(jCimMessageBean.getFromName())){
                    String decodeContent = new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP));
                    JCMsgTextContentBean jcMsgTextContentBean = new Gson().fromJson(decodeContent, JCMsgTextContentBean.class);
                    name=jcMsgTextContentBean.getFromName();
                }
                if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(),jCimMessageBean.getToSipID())
                        && !TextUtils.isEmpty(jCimMessageBean.getFromName())
                        && TextUtils.equals(jCimMessageBean.getSessionType(),JCSessionType.CHAT_PERSION)){
                    name = jCimMessageBean.getFromName();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return name;
    }

    private String getTime(String timeStr){
        SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
        SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        long time = 0;
        try {
            time = Long.parseLong(timeStr);
            if (time< 10000000000L){
                time = time * 1000;
            }
        }catch (Exception e){
//            e.printStackTrace();
//            time = calendar.getTime().getTime();
            JNLogUtil.e("=====getSession=====Exception===time==="+time);
            return "";
        }

        String t1 = "";

        Calendar d2 = Calendar.getInstance();
        d2.setTime(new Date(time));
        if (d2.get(Calendar.YEAR) < calendar.get(Calendar.YEAR)){//去年
            t1 = format3.format(d2.getTime());
        }else if (d2.get(Calendar.DAY_OF_MONTH) < calendar.get(Calendar.DAY_OF_MONTH)-1){//昨天之前
            t1 = format.format(d2.getTime());
        }else if (d2.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)-1) {//昨天
            t1 = "昨天"+format2.format(d2.getTime());
        }else if (d2.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {//今天
            t1 = format2.format(d2.getTime());
        }else {
            t1 = format.format(d2.getTime());
        }
        JNLogUtil.e("=====getSession======t1==="+t1);
        return t1;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public final class ViewHolder extends RecyclerView.ViewHolder{

        private final TextView nameTv;
        private final TextView timeTv;
        private final TextView msgTv;
        private final TextView tv_no_red_num;
        private final LinearLayout session_item;
        private final ImageView iv_head;
        private final ImageView iv_disturb;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTv =itemView.findViewById(R.id.text_name);
            timeTv=itemView.findViewById(R.id.time);
            msgTv=itemView.findViewById(R.id.msg_tv);
            session_item=itemView.findViewById(R.id.session_item);
            tv_no_red_num=itemView.findViewById(R.id.tv_no_red_num);
            iv_head=itemView.findViewById(R.id.iv_head);
            iv_disturb=itemView.findViewById(R.id.iv_disturb);
        }
    }

    public interface ItemClickCallBack{
        void delete(int mPosition);
        void goUp(int mPosition);
    }
}

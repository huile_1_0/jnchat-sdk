package com.jndv.jndvchatlibrary.ui.chat.utils;

import android.content.ContentResolver;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.google.gson.Gson;
import com.hjq.http.body.JsonBody;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.utils.SSLSocketClient;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.http.api.JCFilesUpApi;
import com.jndv.jndvchatlibrary.http.api.JCImagesUpApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Author: wangguodong
 * Date: 2022/4/9
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 图片、文件上传工具类
 */
public class JCFilesUpUtils {

    /**
     * @param uri
     * @param intype
     */
    public static void upImage(Uri uri, String intype, OnHttpListener onHttpListener) {
        if (null == onHttpListener) {
            onHttpListener = new OnHttpListener<JCImagesUpApi.Bean>() {
                @Override
                public void onSucceed(JCImagesUpApi.Bean result) {
                    try {
                        if (TextUtils.equals("success", result.getMsg())) {
                            JCThreadManager.showMainToast("成功！");
                        } else {
                            JCThreadManager.showMainToast("失败！");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail(Exception e) {
                    JCThreadManager.showMainToast("失败！");
                }
            };
        }
        final OnHttpListener monHttpListener = onHttpListener;
        JCThreadManager.getFile().execute(new Runnable() {
            @Override
            public void run() {
                String mtype = "jpg";
                try {
                    mtype = intype.substring(intype.lastIndexOf("/") + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (null==mtype||TextUtils.isEmpty(mtype))mtype="jpg";
                JNLogUtil.e("upImage: ============type="+mtype);
                JNLogUtil.e("upImage: ============url="+JNBaseUtilManager.filesServerUrl +"storage-server-api/Control/saveImg");
                List<JCImagesUpApi.ImageBean> list = new ArrayList<>();
                list.add(new JCImagesUpApi.ImageBean(imageToBase64(uri)));

                Request request = new Request.Builder()
                        .url(JNBaseUtilManager.getFilesUrl("storage-server-api/Control/saveImg"))
                        .post(new JsonBody(new Gson().toJson(new JCImagesUpApi()
                                .setType(mtype)
                                .setGroupName("")
                                .setIsYs("1")
                                .setPicImgArr(list)))) // 不写,默认是GET请求
                        .build();

                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .sslSocketFactory(SSLSocketClient.getSSLSocketFactory())
                        .hostnameVerifier(SSLSocketClient.getHostnameVerifier())
                        .build();

                Call call = okHttpClient.newCall(request);
                // 异步调用
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        JNLogUtil.e("upImage: =============", e);
                        monHttpListener.onFail(e);
//                        failed to connect to /192.168.1.24 (port 9090) from /10.90.92.67 (port 49022) after 10000ms
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response)
                            throws IOException {
                        String resp = response.body().string();
                        JNLogUtil.e("upImage: ====onResponse========="+resp);
                        try {
                            monHttpListener.onSucceed(new Gson().fromJson(resp, JCImagesUpApi.Bean.class));
//                            monHttpListener.onSucceed(new Gson().fromJson(resp, JCImagesUpApi.Bean.class));
                        }catch (Exception e){
                            e.printStackTrace();
                            monHttpListener.onFail(e);
                        }

//                        {"contentLength":103,"contentTypeString":"text/plain;charset\u003dUTF-8","source":{"buffer":{"size":0},"closed":false,"source":{"bytesRemaining":103,"bytesRead":0,"closed":false,"timeout":{"deadlineNanoTime":0,"hasDeadline":false,"timeoutNanos":0}}}}
//                        Log.e("netdata", new Gson().toJson(response));
//                        Log.e("netdata", );
                    }
                });
            }
        });

    }

    /**
     * 将图片转换成Base64编码的字符串
     */
    public static String imageToBase64(Uri uri) {
//        if(TextUtils.isEmpty(path)){
//            return null;
//        }
        InputStream is = null;
        byte[] data = null;
        String result = null;
        try {
//            File file = new File(uri.getPath());
//            is = new FileInputStream(file);
//            ContentResolver.openInputStream(
            is = JCChatManager.mContext.getContentResolver().openInputStream(uri);
//            is = new FileInputStream(uri);
            //创建一个字符流大小的数组。
            data = new byte[is.available()];
            //写入数组
            is.read(data);
            //用默认的编码格式进行编码
            result = Base64.encodeToString(data, Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.e("netdata", "imageToBase64: ===" + result);
        return result;
    }

    /**
     * @param path
     * @param intype
     */
    public static void upImage(String path, String intype, OnHttpListener onHttpListener) {
        if (null == onHttpListener) {
            onHttpListener = new OnHttpListener<JCImagesUpApi.Bean>() {
                @Override
                public void onSucceed(JCImagesUpApi.Bean result) {
                    try {
                        if (TextUtils.equals("success", result.getMsg())) {
                            JCThreadManager.showMainToast("成功！");
                        } else {
                            JCThreadManager.showMainToast("失败！");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail(Exception e) {
                    JCThreadManager.showMainToast("失败！");
                }
            };
        }
        final OnHttpListener monHttpListener = onHttpListener;
        JCThreadManager.getFile().execute(new Runnable() {
            @Override
            public void run() {
                String mtype = "jpg";
                try {
                    mtype = intype.substring(intype.lastIndexOf("/") + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (null == mtype || TextUtils.isEmpty(mtype)) mtype = "jpg";
                Log.e("netdata", "upImage: ============type=" + mtype);
                List<JCImagesUpApi.ImageBean> list = new ArrayList<>();
                list.add(new JCImagesUpApi.ImageBean(imageToBase64(path)));


                Request request = new Request.Builder()
//                        .url("http://192.168.1.24:9090/Control/saveImg")
                        .url(JNBaseUtilManager.getFilesUrl("storage-server-api/Control/saveImg"))
                        .post(new JsonBody(new Gson().toJson(new JCImagesUpApi()
                                .setType(mtype)
                                .setGroupName("")
                                .setIsYs("1")
                                .setPicImgArr(list)))) // 不写,默认是GET请求
                        .build();
                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .sslSocketFactory(SSLSocketClient.getSSLSocketFactory())
                        .hostnameVerifier(SSLSocketClient.getHostnameVerifier())
                        .build();
                Call call = okHttpClient.newCall(request);
                // 异步调用
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        Log.e("JCFilesUpUtils", e.getMessage());
                        monHttpListener.onFail(e);
//                        failed to connect to /192.168.1.24 (port 9090) from /10.90.92.67 (port 49022) after 10000ms
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response)
                            throws IOException {
                        String resp = response.body().string();
                        JNLogUtil.e("upImage: ====onResponse========="+resp);
                        try {
                            monHttpListener.onSucceed(new Gson().fromJson(resp, JCImagesUpApi.Bean.class));
//                            monHttpListener.onSucceed(new Gson().fromJson(resp, JCImagesUpApi.Bean.class));
                        }catch (Exception e){
                            e.printStackTrace();
                            monHttpListener.onFail(e);
                        }
//                        {"contentLength":103,"contentTypeString":"text/plain;charset\u003dUTF-8","source":{"buffer":{"size":0},"closed":false,"source":{"bytesRemaining":103,"bytesRead":0,"closed":false,"timeout":{"deadlineNanoTime":0,"hasDeadline":false,"timeoutNanos":0}}}}
//                        Log.e("netdata", new Gson().toJson(response));
//                        Log.e("netdata", );
                    }
                });
            }
        });

    }
    /**
     * @param path
     */
    public static void upFilesAll( String path, OnHttpListener onHttpListener) {
        if (null == onHttpListener) {
            onHttpListener = new OnHttpListener<JCFilesUpApi.Bean>() {
                @Override
                public void onSucceed(JCFilesUpApi.Bean result) {
                    try {
                        if (TextUtils.equals("success", result.getMsg())) {
                            JCThreadManager.showMainToast("成功！");
                        } else {
                            JCThreadManager.showMainToast("失败！");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail(Exception e) {
                    JCThreadManager.showMainToast("失败！");
                }
            };
        }
        final OnHttpListener monHttpListener = onHttpListener;
        JCThreadManager.getFile().execute(new Runnable() {
            @Override
            public void run() {
//                JNLogUtil.e("==upFilesAll=========length=="+s.length());
//                TODO   20240830 因接口报404，与Java沟通后这里添加storage-server-api。因这个地方已经多次更改所以记录一下。
                JNLogUtil.e("==upFilesAll==========="+JNBaseUtilManager.getFilesUrl("/storage-server-api/Control/uploads"));
                File file = new File(path);
                MultipartBody.Builder builder = new MultipartBody.Builder();
                builder.setType(MultipartBody.FORM);
                builder.addFormDataPart("files", file.getName(),
                        RequestBody.create(MediaType.parse("application/octet-stream"), file));
//                builder.addFormDataPart("files", new Gson().toJson(list));
                Request request = new Request.Builder()
//                        .url("http://192.168.1.24:9090/Control/uploads")
                        .url(JNBaseUtilManager.getFilesUrl("storage-server-api/Control/uploads"))
                        .post(builder.build()) //
                        .build();
                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .sslSocketFactory(SSLSocketClient.getSSLSocketFactory())
                        .hostnameVerifier(SSLSocketClient.getHostnameVerifier())
                        .build();
                Call call = okHttpClient.newCall(request);
                // 异步调用
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        JNLogUtil.e("JCFilesUpUtils==upFilesAll==01==", e);
                        monHttpListener.onFail(e);
//                        failed to connect to /192.168.1.24 (port 9090) from /10.90.92.67 (port 49022) after 10000ms
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response)
                            throws IOException {
//                        {"urlArr":[]}
                        String body = response.body().string();
                        JNLogUtil.e("JCFilesUpUtils==upFilesAll=="+response.toString()+"====\n=="+ body + "++++");
                        if(200==response.code())
                            monHttpListener.onSucceed(new Gson().fromJson(body, JCFilesUpApi.Bean.class));
                        else monHttpListener.onFail(null);
                    }
                });
            }
        });
//        https://imapi.jndv.com/group1/M01/01/14/wKgBGGMSoWGAeWbhAWXGktTLdgA373.Mp4
//Response{protocol=http/1.1, code=200, message=, url=https://imapi.jndv.com/Control/uploads}====
//    =={"urlArr":[{"upload":"success","url":"group1/M01/01/14/wKgBGGMSoWGAeWbhAWXGktTLdgA373.Mp4"}]}++++
    }

    /**
     * 将图片转换成Base64编码的字符串
     */
    public static String imageToBase64(String path) {
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        InputStream is = null;
        byte[] data = null;
        String result = null;
        try {
            File file = new File(path);
            is = new FileInputStream(file);
//            is = JCchatManager.mContext.getContentResolver().openInputStream(uri);
//            is = new FileInputStream(uri);
            //创建一个字符流大小的数组。
            data = new byte[is.available()];
            //写入数组
            is.read(data);
            //用默认的编码格式进行编码
            result = Base64.encodeToString(data, Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        JNLogUtil.e("netdata", "imageToBase64: ===" + result);
        return result;
    }


    /**
     * 上传文件
     *
     * @param path
     */
    public static void upCommonFile(String path, String type, OnHttpListener onHttpListener) {
        if (null == onHttpListener) {
            onHttpListener = new OnHttpListener<String>() {
                @Override
                public void onSucceed(String result) {
                    JCThreadManager.showMainToast(result);
                }

                @Override
                public void onFail(Exception e) {
                    JCThreadManager.showMainToast("失败！");
                }
            };
        }
        OnHttpListener finalOnHttpListener = onHttpListener;
        JCThreadManager.getFile().execute(() -> {
            File file = new File(path);
            MultipartBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file", file.getName(),
                            RequestBody.create(MediaType.parse("application/octet-stream"), file))
                    .build();

            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.append("storage-server-api/Control/");
//            urlBuilder.append("http://192.168.1.24:9090/Control/uploadVideo");
//            urlBuilder.append("https://imapi.jndv.com/Control/uploadVideo");
            if (TextUtils.equals(type, JCmessageType.VOICE)) {
                urlBuilder.append("uploadAudio");
            } else if (TextUtils.equals(type, JCmessageType.VIDEO)) {
                urlBuilder.append("uploadVideo");
            } else {
                urlBuilder.append("uploads");
            }
            Request request = new Request.Builder()
                    .url(JNBaseUtilManager.getFilesUrl(urlBuilder.toString()))
                    .post(requestBody)
                    .build();
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .sslSocketFactory(SSLSocketClient.getSSLSocketFactory())
                    .hostnameVerifier(SSLSocketClient.getHostnameVerifier())
                    .build();
            Call call = okHttpClient.newCall(request);
            // 异步调用
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    JNLogUtil.e("JCFilesUpUtils", e.getMessage());
                    finalOnHttpListener.onFail(e);
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) {
                    JNLogUtil.e("JCFilesUpUtils",  "++++++" + new Gson().toJson(response));
                    if (response.body() != null) {
                        try {
                            String url = response.body().string();
                            JNLogUtil.e("JCFilesUpUtils", url + "++++++");
                            finalOnHttpListener.onSucceed(url);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        });

    }


    /**
     * 获取文件MimeType
     *
     * @param
     * @return
     */
    private static String getMimeType(String fileName) {
        FileNameMap filenameMap = URLConnection.getFileNameMap();
        String contentType = filenameMap.getContentTypeFor(fileName);
        if (contentType == null) {
            contentType = "application/octet-stream"; //* exe,所有的可执行程序
        }

        Log.e("JCFilesUpUtils", "文件格式---" + contentType);
        return contentType;
    }

}

package com.jndv.jndvchatlibrary.ui.chat.bean.flyreceive;

public class DroneVerifyDataBean {
    private Text text = new Text();
    private String type;
    public void setText(Text text) {
        this.text = text;
    }
    public Text getText() {
        return text;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }

    public class Text {

        private String ZoningCode ;
        private String nickname ;
        private String sip;
        private String account;
        public void setZoningCode (String ZoningCode ) {
            this.ZoningCode  = ZoningCode ;
        }
        public String getZoningCode () {
            return ZoningCode ;
        }

        public void setNickname (String nickname ) {
            this.nickname  = nickname ;
        }
        public String getNickname () {
            return nickname ;
        }

        public void setSip(String sip) {
            this.sip = sip;
        }
        public String getSip() {
            return sip;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }
    }
}

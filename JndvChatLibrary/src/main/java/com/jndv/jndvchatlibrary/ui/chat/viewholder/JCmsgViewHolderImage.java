package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.RequestBuilder;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgImgContentBean;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCBagImageFragment;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/3/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 图片消息
 */
public class JCmsgViewHolderImage extends JCmsgViewHolderBase {

    private ImageView ivMessage;

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_image;
    }

    @Override
    protected void inflateContentView() {
        ivMessage = findViewById(R.id.iv_image);
    }

    @Override
    protected void bindContentView() {
        Log.e("bindContentView",message.getContent());
        Log.e("netdata", "bindContentView: ========001======");
        try {
//            JCMsgImgContentBean bean = new Gson().fromJson(message.getContent(), JCMsgImgContentBean.class);
            String decodeBase64 = new String(Base64.decode(message.getContent(),Base64.NO_WRAP));
            JNLogUtil.e("==decodeBase64="+decodeBase64);
            JCMsgImgContentBean bean = new Gson().fromJson(decodeBase64, JCMsgImgContentBean.class);
//            Log.e("netdata", "bindContentView: ========002======"+message.getContent());
//            int w = 240;
//            int h = 240 ;
//            if (bean.getWidth()>0&&bean.getHeight()>0){
//                if (bean.getWidth()>240){
//                    h = 240*bean.getHeight()/bean.getWidth();
//                }
//                if (h<240){
//                    h=240;
//                    w = 240*bean.getWidth()/bean.getHeight();
//                }
//            }
//            setLayoutParams(w,h,ivMessage);
            initImageView(bean.getWidth(), bean.getHeight());
            if (null!=bean.getPath() || !TextUtils.isEmpty(bean.getPath())){
                Log.e("netdata", "bindContentView: ========003======");
                try {
                    RequestBuilder<Drawable> requestBuilder = JCglideUtils.loadImageBuilder(context, bean.getUrl());
                    JCglideUtils.loadImage(context, bean.getPath(), ivMessage, requestBuilder);
                }catch (Exception e){
                    e.printStackTrace();
                    if (!TextUtils.isEmpty(bean.getUrl())){
                        JCglideUtils.loadImage(context, bean.getUrl(), ivMessage);
                    }
                }
//                try {
//                    RequestBuilder<Drawable> requestBuilder = JCglideUtils.loadImageBuilder(context, bean.getUrl(), w, height);
//                    JCglideUtils.loadImage(context, bean.getPath(), ivMessage, w, h, requestBuilder);
//                }catch (Exception e){
//                    e.printStackTrace();
//                    if (!TextUtils.isEmpty(bean.getUrl())){
//                        JCglideUtils.loadImage(context, bean.getUrl(), ivMessage, w, height);
//                    }
//                }
            }else {
                Log.e("netdata", "bindContentView: ========004======");
                if (!TextUtils.isEmpty(bean.getUrl())){
                    JCglideUtils.loadImage(context, bean.getUrl(), ivMessage);
//                    JCglideUtils.loadImage(context, bean.getUrl(), ivMessage, w, height);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initImageView(int width, int height){
        int w = 240;
        int h = 240 ;
        if (width>0&&height>0){
            h = 240*height/width;
            if (h>450){
                h=450;
                ivMessage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }else if (h<130){
                h=130;
                ivMessage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }else {
                ivMessage.setScaleType(ImageView.ScaleType.FIT_XY);
            }
        }
        Log.e("netdata", "bindContentView: =============w="+w+"==h="+h);
        setLayoutParams(w,h,ivMessage);
    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
//        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_copy), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_transmit), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_collect), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_withdraw), 1));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_delete), 2));
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected void onItemClick() {
//        super.onItemClick();
        try {
            String decodeBase64 = new String(Base64.decode(message.getContent(),Base64.NO_WRAP));
            JCMsgImgContentBean bean = new Gson().fromJson(decodeBase64, JCMsgImgContentBean.class);
            Log.e("bindContentView",bean.toString());
//            JCMsgImgContentBean bean = new Gson().fromJson(message.getContent(), JCMsgImgContentBean.class);
            JCBagImageFragment jcBagImageFragment = new JCBagImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString(JCBagImageFragment.INPATH, bean.getPath());
            bundle.putString(JCBagImageFragment.INURL, bean.getUrl());
            bundle.putSerializable(JCBagImageFragment.MESSAGE, (JCimMessageBean)message);
            JCbaseFragmentActivity.start(context,jcBagImageFragment,"查看大图", bundle);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;


import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.ui.crowd.asagent.JCUserTypeInfo;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCChatEntity;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCConversationNotificationObject;


import java.util.List;

/**
 * 全局配置常量
 */
public class GlobalConstant {

    public static final String KEY_WEB_IP = "";
    public static final String KEY_WEB_PORT = "";
    public static final String KEY_IM_IP = "im_ip";
    public static final String KEY_IM_PORT = "im_port";
    public static final String KEY_AS_IP = "monitorip";
    public static final String KEY_AS_PORT = "monitorport";

    public static final String KEY_APP_USER_NAME = "appusername"; //业务账号
    public static final String KEY_APP_PASSWORD = "apppassword"; //账号的密码

    public static final String KEY_DEVICES_ID = "devices_id"; // 设备ID

    //行程，时间等信息
    public static final String ACCOUNT_SIGN_VISIT_REPORT_INFO = "/Platform/WorkAttend/Ashx/WorkAttendAbout.ashx?method=AccountSignVisitReportInfo&account=";

    //事件上报 事件报警 数量 接口
    public static final String ACCOUNT_EVENT_HANDLE_INFO = "/Platform/WorkAttend/Ashx/AppIndexHandle.ashx?method=EventInfo&account=";
    //部门事件数量 接口
    public static final String ACCOUNT_DEPARTEVENT = "/Platform/WorkAttend/Ashx/AppIndexHandle.ashx?method=AccountDepartEvent&account=";
    //部门动态（事件、报警数量）
    public static final String DEPART_TRENDS = "/Platform/WorkAttend/Ashx/AppIndexHandle.ashx?method=DepartTrends&account=";

    //部门动态（今日高发）
    public static final String EVENT_HIGH_INCIDENCE = "/Platform/WorkAttend/Ashx/AppIndexHandle.ashx?method=EventHighIncidence&account=";
    //部门动态（七日趋势图）
    public static final String SEVEN_EVENT_TRENDS = "/Platform/WorkAttend/Ashx/AppIndexHandle.ashx?method=SevenEventTrends&account=";

    /**
     * 域名
     */
    public static String DOMAIN = "http://192.168.1.18:8816";
    //图片服务器地址
//    public static String PIC_URL="";
    public static String IM_IP = "";
    public static String IM_PORT = "";
    public static String AS_IP = "";
    public static String AS_PORT = "";
    //获取聊天记录的token
    public static String RECORDTOKEN = "";
    public static String getDomainBMS() {
        try {
            String domain  = JNBasePreferenceSaves.getString("BMS*IP");
            String port = JNBasePreferenceSaves.getString("BMS*PORT");
            if (null==domain)domain="";
            Log.e("TAG", "getDomainBMS: ========domain=="+domain);
            if (null!=port&&!TextUtils.isEmpty(port)&&!TextUtils.equals("null", port)){
                domain = domain+":"+port;
            }
            domain = domain.startsWith("http") ? domain : ("http://" + domain);
            return domain;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    // ---------------------------------------------------------------------------------------------

    public static String main_home_title = "淄博网格";

    //主页

    public static String MAIN_NETCACHE_DATA = "main_netcache_data";//主页的缓存数据
    public static String APPWORK_MAIN_XCOOKIE = "xcookie";//sm cookie key

    /**
     * 报警状态
     */
    public static boolean isAlarm = false;

    /**
     * cookie值
     */
    public static String sCookieVlues = "";
    /**
     * 网络状态
     */
    public static boolean isNetError = false;
    /**
     * 用户名
     */
    public static String sAccountName = "";

    public static String sPassword = "";

    /**
     * sip账号/无人机sip账号
     */
    public static String sSipNum = "";
    /**
     * sip密码
     */
    public static String sSipPassword = "";
    /**
     * 飞手账号
     */
    public static String sFlightAccountName = "";
    /**
     * 飞手密码
     */
    public static String sFlightPassword = "";
    //飞手姓名
    public static String mFhname = "";
    //飞手校验状态
    public static boolean mFhsipstate = false;
    //飞手单位编号（学员）
    public static String mFhzonecode = "";

    //教员sip账号
    public static String mJysip = "";
    //教员姓名
    public static String mJyname = "";
    //教员校验状态
    public static boolean mJysipstate = false;
    //教员单位编号
    public static String mJyzonecode = "";

    //无人机登录状态
    public static boolean mPlansipstate = false;
    //无人机单位编码
    public static String mPlanzonecode = "";


    /**
     * 周边警力用户类型
     */
    public static List<JCUserTypeInfo> sUserTypes = null;

    // 定位数据+
    public static double[] locations;// 转化之后的坐标
    public static double latitude = 0;// 定位坐标
    public static double longitude = 0;
    public static int speed = -1;
    public static int direction = -1;
    public static int locType;//定位类型
    public static int gpsAccuracyStatus;//GPS质量
    public static double loc_altitude=0;//高度
    public static String networkLocationType;//在网络定位结果的情况下，获取网络定位结果是通过基站定位得到的还是通过wifi定位得到的还是GPS得结果
    public static boolean isConnectNetwork=true;//网络连接状态
    public static boolean isAsLogin=false;//As登录状态
    public static String logAngle ="-1";
    public static String logSpeed ="";

    // ---------------------------------------------------------------------------------------------

    // BroadcastReceiver 常量字段
    private static final String BASE = "grid";

    public static final String LOGIN_SUCCEED = "meet.demo.login.succeed";
    public static final String LOGIN_FAILURE = "meet.demo.login.failure";
    public static final String GET_GROUPS_SUCCEED = "com.jndv.user.succeed";
    public static final String JNI_BROADCAST_CATEGORY = "com.pview.jni.broadcast" + BASE;
    public static final String JNI_BROADCAST_CONNECT_STATE_NOTIFICATION = "com.pview.jni.broadcast.connect_state_notification" + BASE;
    public static final String JNI_BROADCAST_USER_STATUS_NOTIFICATION = "com.pview.jni.broadcast.user_stauts_notification" + BASE;

    // 拷贝 JNIService
    public static final String JNI_BROADCAST_GROUP_NOTIFICATION = "com.pview.jni.broadcast.group_geted" + BASE;
    public static final String JNI_BROADCAST_GROUP_USER_UPDATED_NOTIFICATION = "com.pview.jni.broadcast.group_user_updated" + BASE;
    public static final String JNI_BROADCAST_GROUP_UPDATED = "com.pview.jni.broadcast.group_updated" + BASE;
    public static final String JNI_BROADCAST_GROUP_JOIN_FAILED = "com.pview.jni.broadcast.group_join_failed" + BASE;
    public static final String JNI_BROADCAST_NEW_MESSAGE = "com.pview.jni.broadcast.new.message" + BASE;
    public static final String JNI_BROADCAST_MESSAGE_SENT_RESULT = "com.pview.jni.broadcast.message_sent_result" + BASE;
    public static final String JNI_BROADCAST_NEW_CONF_MESSAGE = "com.pview.jni.broadcast.new.conf.message" + BASE;
    public static final String JNI_BROADCAST_CONFERENCE_INVATITION = "com.pview.jni.broadcast.conference_invatition_new" + BASE;
    public static final String JNI_BROADCAST_CONFERENCE_REMOVED = "com.pview.jni.broadcast.conference_removed" + BASE;
    public static final String JNI_BROADCAST_CONFERENCE_REMOVED_SIP_CALL = "com.pview.jni.broadcast.conference_removed_sip_call" + BASE;
    public static final String JNI_BROADCAST_CONFERENCE_CONF_SYNC_OPEN_VIDEO = "com.pview.jni.broadcast.conference_confSyncOpenVideo" + BASE;
    public static final String JNI_BROADCAST_CONFERENCE_CONF_SYNC_CLOSE_VIDEO = "com.pview.jni.broadcast.conference_confSyncCloseVideo" + BASE;
    public static final String JNI_BROADCAST_CONFERENCE_CONF_VOD_OPEN_VIDEO = "com.pview.jni.broadcast.conference_vodOpenVideo" + BASE;
    public static final String JNI_BROADCAST_GROUP_USER_REMOVED = "com.pview.jni.broadcast.group_user_removed" + BASE;
    public static final String JNI_BROADCAST_GROUP_USER_ADDED = "com.pview.jni.broadcast.group_user_added" + BASE;
    public static final String JNI_BROADCAST_VIDEO_CALL_CLOSED = "com.pview.jni.broadcast.video_call_closed" + BASE;
    public static final String JNI_BROADCAST_CONTACTS_AUTHENTICATION = "com.pview.jni.broadcast.friend_authentication" + BASE;
    public static final String JNI_BROADCAST_NEW_QUALIFICATION_MESSAGE = "com.pview.jni.broadcast.new.qualification_message" + BASE;
    public static final String JNI_BROADCAST_CONFERENCE_CONF_SYNC_CLOSE_VIDEO_TO_MOBILE = "com.pview.jni.broadcast.conference_OnConfSyncCloseVideoToMobile" + BASE;
    public static final String JNI_BROADCAST_CONFERENCE_CONF_SYNC_OPEN_VIDEO_TO_MOBILE = "com.pview.jni.broadcast.conference_OnConfSyncOpenVideoToMobile" + BASE;
    public static final String JNI_BROADCAST_GROUPS_LOADED = "com.pview.jni.broadcast.groups_loaded" + BASE;
    public static final String JNI_BROADCAST_OFFLINE_MESSAGE_END = "com.pview.jni.broadcast.offline_message_end" + BASE;
    // Current user kicked by crowd master key crowd : crowdId
    public static final String JNI_BROADCAST_KICED_CROWD = "com.pview.jni.broadcast.kick_crowd" + BASE;
    // Crowd invitation with key crowd
    public static final String JNI_BROADCAST_CROWD_INVATITION = "com.pview.jni.broadcast.crowd_invatition" + BASE;
    // Broadcast for joined new discussion key gid
    public static final String BROADCAST_CROWD_NEW_UPLOAD_FILE_NOTIFICATION = "com.pview.jni.broadcast.new.upload_crowd_file_message" + BASE;

    // JSharedPreferences key
    public static final String LOGIN_USER_NAME = "login_user_name";

    // --------------摘自PviewGlobalConstants------------------
    /**
     * 用户登录错误信息：组织不可用
     */
    public static final int ERR_LOGIN_ORGDISABLED = 5;

    /**
     * 用户注销登录
     */
    public static final int ERR_LOGIN_OUT = 6;
    /**
     * 未读标识，适用于各种场景。
     */
    public static final int READ_STATE_UNREAD = 0;

    /**
     * 已读标识，适用于各种场景。
     */
    public static final int READ_STATE_READ = 1;

    /**
     * 账户类型：未注册用户，用于快速入会
     */
    public static final int ACCOUNT_TYPE_NON_REGISTERED = 2;

    /**
     * 账户类型：电话联系人(仅PC能用，移动端屏蔽)
     */
    public static final int ACCOUNT_TYPE_PHONE_FRIEND = 3;

    /**
     * 用户状态:在线
     */
    public static final int USER_STATUS_ONLINE = 1;

    /**
     * 用户状态:离线
     */
    public static final int USER_STATUS_OFFLINE = 0;

    /**
     * 用户状态:离开
     */
    public static final int USER_STATUS_LEAVING = 2;

    /**
     * 用户状态:繁忙
     */
    public static final int USER_STATUS_BUSY = 3;

    /**
     * 用户状态:请勿打扰
     */
    public static final int USER_STATUS_DO_NOT_DISTURB = 4;

    /**
     * 用户状态:隐身
     */
    public static final int USER_STATUS_HIDDEN = 5;

    /**
     * 会议进入时错误信息：该会议已被删除
     */
    public static final int CONF_ERROR_CONFOVER = 204;

    /**
     * 会议进入时错误信息：与服务器断开连接
     */
    public static final int CONF_ERROR_SERVERDISCONNECT = 206;

    /**
     * 会议中语音激励状态：未激活
     */
    public static final int CONF_VOICE_ACTIVATION_NO = 0;

    /**
     * 会议中语音激励状态：已激活
     */
    public static final int CONF_VOICE_ACTIVATION_YES = 1;

    /**
     * 会议中文档的类型：正常文档
     */
    public static final int DOC_TYPE_NORMAL = 3;

    /**
     * 会议中文档的类型：白板
     */
    public static final int DOC_TYPE_BLACK = 4;

    /**
     * 文件的发送类型：在线发送
     */
    public static final int FILE_TYPE_ONLINE = 1;

    /**
     * 文件的发送类型：离线发送
     */
    public static final int FILE_TYPE_OFFLINE = 2;

    /**
     * 文件的发送类型：离线发送，未加密
     */
    public static final int FILE_ENCRYPT_TYPE = 1;

    /**
     * 文件传输中状态：正在发送
     */
    public static final int FILE_TRANS_SENDING = 10;

    /**
     * 文件传输中状态：正在下载
     */
    public static final int FILE_TRANS_DOWNLOADING = 11;

    /**
     * 文件传输中状态：传输错误
     */
    public static final int FILE_TRANS_ERROR = 13;

    /**
     * 点对点语音留言状态：开始录音
     */
    public static final int RECORD_TYPE_START = 0x0001;

    /**
     * 点对点语音留言状态：结束录音
     */
    public static final int RECORD_TYPE_STOP = 0x0002;

    public static final int MESSAGE_SHOW_TIME = 14;

    public static final int MESSAGE_NOT_SHOW_TIME = 15;

    public static final int UNKOWN = 0;
    public static final int EVIDEODEVTYPE_VIDEO = 1;
    public static final int EVIDEODEVTYPE_CAMERA = 2;
    public static final int EVIDEODEVTYPE_FILE = 3;
    public static final int EVIDEODEVTYPE_VIDEOMIXER = 4;


    // P2P
    public static final String BROADCAST_VIDEO_CALL_CLOSED = "com.pview.jni.broadcast.video_call_closed" + BASE;
    public static final String BROADCAST_CONNECT_STATE = "com.pview.jni.broadcast.video_call_closed" + BASE;

    /**
     * 应用的访问的域名
     */
    public static final String JNDV_BASEURL = "JNDV_BASEURL";
    public static final String JNDV_MAINWEBVIEWURL = "JNDV_MAINWEBVIEWURL"; //主页weibview需要的url 注:不包含域名
    /**
     * 登录的用户名
     */
    public static final String JNDV_USERNAME = "JNDV_USERNAME";
    /**
     * 登录的密码
     */
    public static final String JNDV_PASSWORD = "JNDV_PASSWORD";
    /**
     * 判断是否已经登录
     */
    public static final String HAS_LOGIN = "HAS_LOGIN";

//    public static final String PlatformMembers=DOMAIN+"Platform/Members/";
//    public static final String UserHandler=DOMAIN+"AjaxHandle/UserHandler.ashx?";//修改密码，发送验证码
//    public static final String UserUpdate=PlatformMembers+"UserUpdate.ashx?";//修改个人信息
//    public static final String AvatarUpload=PlatformMembers+"AvatarUpload.ashx?";//修改头像

    // -------------------------------PublicIntent PublicIntent PublicIntent -----------------------
    public static final String TAG_CONTACT = "contacts";
    public static final String TAG_ORG = "org";
    public static final String TAG_GROUP = "group";
    public static final String TAG_CONF = "conference";
    public static final String TAG_COV = "conversation";
    public static final String TAG_MORE = "more";

    public static final int MESSAGE_NOTIFICATION_ID = 1;
    public static final int VIDEO_NOTIFICATION_ID = 2;
    public static final int APPLICATION_STATUS_BAR_NOTIFICATION = 3;
    public static final int WEBMSG_NOTIFICATION = 4;

    //public static final String DEFAULT_CATEGORY = "com.pview.grid";

    //======================================================================================
    //                            for P2P conversation
    //======================================================================================
    /**
     * Used to start conversation UI
     */
    public static final String START_CONVERSACTION_ACTIVITY = "com.pview.cj.start_conversation_activity";

    public static final String START_P2P_CONVERSACTION_ACTIVITY = "com.pview.cj.start_p2p_conversation_activity";

    public static final String START_VIDEO_IMAGE_GALLERY = "com.pview.cj.image_gallery";

    public static final String UPDATE_CONVERSATION = "com.pview.cj.update_conversation";

    /**
     * Start conference create activity<br>
     * key uid: pre-selected user id
     * key gid: pre-selected group id
     */
    public static final String START_CONFERENCE_CREATE_ACTIVITY = "com.pview.cj.start_conference_create_activity";

    public static final String START_ABOUT_ACTIVITY = "com.pview.cj.start_about_activity";

    public static final String START_SETTING_ACTIVITY = "com.pview.cj.start_setting_activity";

    //======================================================================================
    //                            for conference
    //======================================================================================

    public static final String START_GROUP_CREATE_ACTIVITY = "com.pview.cj.start_group_create_activity";

    public static final String NOTIFY_CONFERENCE_ACTIVITY = "com.pview.cj.notify_conference_activity";

    //======================================================================================
    //                            for crowd
    //======================================================================================

    public static final String START_CROWD_FILES_ACTIVITY = "com.pview.cj.start_crowd_files_activity";

    public static final String SHOW_CROWD_CONTENT_ACTIVITY = "com.pview.cj.crowd_content_activity";

    public static final String SHOW_CROWD_DETAIL_ACTIVITY = "com.pview.cj.crowd_detail_activity";

    public static final String SHOW_CONTACT_DETAIL_ACTIVITY = "com.pview.cj.contact_detail_activity";

    public static final String SHOW_CONTACT_DETAIL_DETAIL_ACTIVITY = "com.pview.cj.contact_detail_detail_activity";

    //======================================================================================
    //                            for discussion board
    //======================================================================================

    /**
     * for DiscussionBoardCreateActivity <br>
     * Intent parameters:<br>
     * mode : true means in invitation mode, otherwise in create mode
     */
    public static final String START_DISCUSSION_BOARD_CREATE_ACTIVITY = "com.pview.cj.discussion_board_create_activity";

    /**
     * for DiscussionBoardDetailActivity<br>
     * Intent key:<br>
     * cid  : discussion board id<br>
     */
    public static final String SHOW_DISCUSSION_BOARD_DETAIL_ACTIVITY = "com.pview.cj.discussion_board_detail_activity";

    /**
     * for DiscussionBoardTopicUpdateActivity<br>
     * Intent key:<br>
     * cid  : discussion board id<br>
     */
    public static final String SHOW_DISCUSSION_BOARD_TOPIC_ACTIVITY = "com.pview.cj.discussion_board_topic_activity";

    /**
     * key : crowd : object of crowd
     * key : authdisable : disable crowd authentication even through crowd need authentication
     */
    public static final String SHOW_CROWD_APPLICATION_ACTIVITY = "com.pview.cj.crowd_application_activity";

    /**
     * Start searching activity
     * key : type 0 crowd   1: member
     */
    public static final String START_SEARCH_ACTIVITY = "com.pview.cj.start_search_activity";

    // ===========================Broadcast==============================
    //
    //
    /**
     * extras key: obj value:
     * {@link JCConversationNotificationObject}
     */
    public static final String REQUEST_UPDATE_CONVERSATION = "com.jndv.meet.cj.Request_update_conversation";

    public static final String CHAT_SYNC_MESSAGE_INTERFACE = "com.pview.cj.chat_sync_message_interface";

    public static final String FINISH_APPLICATION = "com.pview.cj.finish_application";


    /**
     * broadcast for new crowd notification. if current logged in user created, this broadcast will be sent<br>
     * key : crowd : crowd id
     */
    public static final String BROADCAST_NEW_CROWD_NOTIFICATION = "com.pview.jni.broadcast.new_crowd_notification";

    /**
     * Broadcast for user update the comment name
     * key: no
     */
    public static final String BROADCAST_USER_COMMENT_NAME_NOTIFICATION = "com.pview.broadcast.user_comment_name_notification" + BASE;

    /**
     * Broadcast for new conference. This is only for conference is created by self
     * extra key: newGid : group id
     * we can get conference object from GlobalHolder
     */
    public static final String BROADCAST_NEW_CONFERENCE_NOTIFICATION = "com.pview.cj.jni.broadcast.new_conference_notification";

    /**
     *
     */
    public static final String BROADCAST_REQUEST_UPDATE_CONTACTS_GROUP = "com.pview.cj.broadcast.update_contacts_group";

    /**
     * Broadcast for all type group was deleted
     * key: destGroupId
     */
    public static final String BROADCAST_GROUP_DELETED_NOTIFICATION = "com.pview.broadcast.group_deleted_notification" + BASE;

    /**
     * Broadcast for contact group updated
     * key: userId
     * key: srcGroupId
     * key: destGroupId
     */
    public static final String BROADCAST_CONTACT_GROUP_UPDATED_NOTIFICATION = "com.pview.cj.broadcast.contact_group_notification";

    /**
     * Broadcast for user quit discussion board
     * key: gid
     */
    public static final String BROADCAST_DISCUSSION_DELETED_NOTIFICATION = "com.pview.cj.broadcast.discussion_deleted_notification";

    /**
     * Broadcast for user joined conference, to inform that quit P2P conversation
     * 广播为用户参加会议，通知退出P2P会话
     * key: confid conference if
     */
    public static final String BROADCAST_JOINED_CONFERENCE_NOTIFICATION = "com.pview.cj.broadcast.joined_conference_notification";

    /**
     * Broadcast for current user is in waitting for other person agree to become friend
     */
    public static final String BROADCAST_ADD_OTHER_FRIEND_WAITING_NOTIFICATION = "com.pview.cj.broadcast.add_other_friend_waiting_notification";

    /**
     * Broadcast for CrowdFileActivity notify ConversationP2PTEXTActivity
     */
    public static final String BROADCAST_CROWD_FILE_ACTIVITY_SEND_NOTIFICATION = "com.pview.cj.broadcast.add_other_friend_waiting_notification";

    /**
     * Broadcast for MessageAuthenticationActivity notify ConversationsTabFragment
     */
    public static final String BROADCAST_AUTHENTIC_TO_CONVERSATIONS_TAB_FRAGMENT_NOTIFICATION = "com.pview.cj.broadcast.authentic_to_conversations_tab_fragment_notification";

    // -------------------------------PublicIntent PublicIntent PublicIntent -----------------------

    public static  int DEFAULT = 0;

    //警航常量
    public static final String FLIGHT_USER_SIP = "flight_user_sip";// sip登录账号
    public static final String FLIGHT_USER_SIP_PASS_SAVE = "flight_user_sip_pass_save";// sip登录密码
    public static final String FLIGHT_USER_ACCOUNT = "flight_user_account";// 飞手账号
    public static final String FLIGHT_USER_PASSWORD = "flight_user_password";// 飞手账号

}

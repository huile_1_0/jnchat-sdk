package com.jndv.jndvchatlibrary.ui.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCOrganizationNewFragment;

/**
 * Author: wangguodong
 * Date: 2022/4/23
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCOrganzationListActivity extends JCbaseFragmentActivity {
//    private int nid;
    private String name;
    private String zoningCode;

    public static void start(Context context, String zoningCode, String name){
        Intent intent = new Intent(context, JCOrganzationListActivity.class);
        intent.putExtra("zoningCode", zoningCode);
        intent.putExtra("name", name);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        nid = getIntent().getIntExtra("nid", 0);
        name = getIntent().getStringExtra("name");
        zoningCode = getIntent().getStringExtra("zoningCode");
//        Log.e("TAG", "onCreate: ========================nid="+nid);
        Log.e("TAG", "onCreate: ========================name="+name);
//        initFragment(nid, name);
        initFragmentToActivity(zoningCode, name);
//        initAddFragment(nid, name);
        showMenuClick(false);
    }

    @Override
    protected void onMenuClick() {

    }

    private void initFragmentToActivity(String innid, String name){
        Bundle bundle = new Bundle();
        bundle.putString("zoningCode", innid);
        Log.e("TAG", "initFragmentToActivity: ========================innid="+innid);
        JCOrganizationNewFragment jcOrganizationFragment = new JCOrganizationNewFragment();
        jcOrganizationFragment.setArguments(bundle);
        jcOrganizationFragment.setmOnClickOrganzationListener(new JCOrganizationNewFragment.OnClickOrganzationListener() {
            @Override
//            public void onClick(int nid, String name) {
            public void onClick(String zoningCode, String name) {
                Log.e("TAG", "initFragmentToActivity: =onClick=======================zoningCode="+zoningCode);
                JCOrganzationListActivity.start(JCOrganzationListActivity.this, zoningCode, name);
            }
        });
        showFragment(jcOrganizationFragment);
        setCenterTitle(name);
    }
    private void initFragment(String zoningCode, String name){
        Bundle bundle = new Bundle();
        bundle.putString("zoningCode", zoningCode);
        JCOrganizationNewFragment jcOrganizationFragment = new JCOrganizationNewFragment();
        jcOrganizationFragment.setArguments(bundle);
        jcOrganizationFragment.setmOnClickOrganzationListener(new JCOrganizationNewFragment.OnClickOrganzationListener() {
            @Override
            public void onClick(String zoningCode, String name) {
                initFragment(zoningCode, name);
            }
        });
        showFragment(jcOrganizationFragment);
        setCenterTitle(name);
    }

    private void initAddFragment(String zoningCode, String name){
        Bundle bundle = new Bundle();
        bundle.putString("zoningCode", zoningCode);
        JCOrganizationNewFragment jcOrganizationFragment = new JCOrganizationNewFragment();
        jcOrganizationFragment.setArguments(bundle);
        jcOrganizationFragment.setmOnClickOrganzationListener(new JCOrganizationNewFragment.OnClickOrganzationListener() {
            @Override
            public void onClick(String zoningCode, String name) {
                initAddFragment(zoningCode, name);
            }
        });
        addFragment(jcOrganizationFragment);
        setCenterTitle(name);
    }

}

package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
 * Author: wangguodong
 * Date: 2023/1/13
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCMsgGpsSendTimeContentBean {

    private String time ;

    public String getTime() {
        return time;
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.utils;

import android.graphics.Bitmap;

import androidx.collection.LruCache;

public class BitmapLruCache<K> extends LruCache<K, Bitmap> {

	public BitmapLruCache(int maxSize) {
		super(maxSize);
	}
	
	@Override
	protected void entryRemoved(boolean evicted, K key, Bitmap oldValue, Bitmap newValue) {
		super.entryRemoved(evicted, key, oldValue, newValue);
		if(oldValue != null && !oldValue.isRecycled()){
			oldValue.recycle();
			oldValue = null;
		}
	}
}

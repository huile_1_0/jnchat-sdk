package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcUpdateUserDetailBinding;
import com.jndv.jndvchatlibrary.http.api.JCImagesUpApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCUpdateUserDetailViewModel;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.jndv.jndvchatlibrary.utils.JCmyImagePicker;

import com.qingmei2.rximagepicker.core.RxImagePicker;
import com.qingmei2.rximagepicker.entity.Result;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link JCUpdateUserDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JCUpdateUserDetailFragment extends JCbaseFragment {

    private String userId;
    private FragmentJcUpdateUserDetailBinding detailBinding;
    private JCUpdateUserDetailViewModel viewModel;
    private FragmentActivity activity;
    String headUrl;
    String userName;

    public JCUpdateUserDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getString(JCEntityUtils.USER_ID, JNBasePreferenceSaves.getUserSipId());
        } else {
            userId = JNBasePreferenceSaves.getUserSipId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel =
                new ViewModelProvider(this).get(JCUpdateUserDetailViewModel.class);
        detailBinding = FragmentJcUpdateUserDetailBinding.inflate(inflater, container, false);
        activity = getActivity();
        initView();
        initData();
        return detailBinding.getRoot();
    }

    private void initView() {
        viewModel = new JCUpdateUserDetailViewModel();
        viewModel.init(activity);

        setJcFragmentSelect(new JCFragmentSelect() {
            @Override
            public void onSelecte(int type, Object... objects) {
                if (type == SELECTE_TYPE_PIC_CROP) {
//                    Bitmap bitmap = (Bitmap) objects[1];
                    JCFilesUpUtils.upImage((Uri) objects[0], "", new OnHttpListener<JCImagesUpApi.Bean>() {
                        @Override
                        public void onSucceed(JCImagesUpApi.Bean result) {
                            try {
//                                剪切成功，调用上传接口
                                headUrl = JNBaseUtilManager.getFilesUrl(result.getUrlArr().get(0).getTailUrl());
                                viewModel.updateUserInfo(headUrl);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(Exception e) {
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    JCChatManager.showToast("图片上传失败！");
                                }
                            });
                        }
                    });
                }
            }
        });

        initGender();

        detailBinding.tvSave.setOnClickListener(view -> {
            userName = detailBinding.etName.getText().toString();
            String phoneNum = detailBinding.etPhoneNum.getText().toString();
            int genderPosition = detailBinding.spinnerGender.getSelectedItemPosition();
            String paramSex = "";
            if(genderPosition==0){
                paramSex = "1";
            }else if(genderPosition==1){
                paramSex = "2";
            }else{
                paramSex = "9";
            }
            String age = detailBinding.etAge.getText().toString();
//            String password = detailBinding.etPassword.getText().toString();
            if(TextUtils.isEmpty(userName)){
                JCChatManager.showToast("姓名不能为空");
                return;
            }
//            if(TextUtils.isEmpty(password)){
//                JCChatManager.showToast("密码不能为空");
//                return;
//            }
//            if(TextUtils.isEmpty(phoneNum)){
//                JCChatManager.showToast("电话不能为空");
//                return;
//            }
//            if(TextUtils.isEmpty(age)){
//                JCChatManager.showToast("年龄不能为空");
//                return;
//            }
            // TODO: 2022/9/27 等待接口添加参数后修改
            viewModel.updateTotalUserInfo(headUrl,userName,phoneNum,paramSex,age);
        });

        detailBinding.ivHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxPermissions rxPermissions = new RxPermissions(getActivity());
                rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                        , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
                )
                        .subscribe(new Consumer<Boolean>() {
                            @SuppressLint("CheckResult")
                            @Override
                            public void accept(Boolean aBoolean) {
                                if (aBoolean) {
                                    //申请的权限全部允许
                                    RxImagePicker
                                            .create(JCmyImagePicker.class)
                                            .openGallery(getActivity())
                                            .subscribe(new Consumer<Result>() {
                                                @Override
                                                public void accept(Result result) {
                                                    Uri uri = result.getUri();
                                                    JNLogUtil.e("uri", uri.toString());
                                                    startCropActivity(uri, 1, 1, 200, 200);
                                                }
                                            });
                                } else {
                                    //只要有一个权限被拒绝，就会执行
                                    JCChatManager.showToast("未授权权限，部分功能不能使用");
                                }
                            }
                        });
            }
        });
    }

    private void initGender() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.user_gender, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        detailBinding.spinnerGender.setAdapter(adapter);
        detailBinding.spinnerGender.setSelection(2);
        detailBinding.spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("CrowdUser", position + "=++++++");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initData() {
        viewModel.getTotalUpdateData().observeForever(new Observer<JCRespondBean>() {
            @Override
            public void onChanged(JCRespondBean jcRespondBean) {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, jcRespondBean.getCode())) {
                    JCChatManager.showToast("操作成功！");
                    JCglideUtils.loadCircleImage(getActivity(), headUrl, detailBinding.ivHead);
                    JNBasePreferenceSaves.saveUserHead(headUrl);
                    JNBasePreferenceSaves.saveUserName(userName);

                } else {
                    JCChatManager.showToast("操作失败！");
                }
            }
        });

        viewModel.getHeadUpdateData().observeForever(new Observer<JCRespondBean>() {
            @Override
            public void onChanged(JCRespondBean jcRespondBean) {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, jcRespondBean.getCode())) {
                    JCglideUtils.loadCircleImage(getActivity(), headUrl, detailBinding.ivHead);
                    JNBasePreferenceSaves.saveUserHead(headUrl);
                } else {
                    JCChatManager.showToast("操作失败！");
                }
            }
        });

        viewModel.getUserDetail("")
                .getUserData().observe(activity, data -> {
            if (data != null) {
                detailBinding.etName.setText(data.getNickname());
                JCglideUtils.loadOriginHeadImage(getActivity(), data.getHeadIcon(), detailBinding.ivHead);
                headUrl = data.getHeadIcon();
                detailBinding.tvAccount.setText(data.getAccount());
                detailBinding.etPhoneNum.setText(data.getMobile());

                // TODO: 2022/9/27 等接口添加这些参数后修改
                try {
//                    detailBinding.etPassword.setText(data.getPassword());
                    detailBinding.etAge.setText(data.getAge());
                    switch (data.getSex()){
                        case "1":
                            detailBinding.spinnerGender.setSelection(0);
                            break;
                        case "2":
                            detailBinding.spinnerGender.setSelection(1);
                            break;
                        default:
                            detailBinding.spinnerGender.setSelection(2);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public static JCUpdateUserDetailFragment newInstance(String userId) {
        JCUpdateUserDetailFragment fragment = new JCUpdateUserDetailFragment();
        Bundle args = new Bundle();
        args.putString(JCEntityUtils.USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

}

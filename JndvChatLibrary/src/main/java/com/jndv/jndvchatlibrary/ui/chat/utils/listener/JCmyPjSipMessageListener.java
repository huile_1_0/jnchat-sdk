package com.jndv.jndvchatlibrary.ui.chat.utils.listener;

import android.text.TextUtils;
import android.util.Log;

import com.ehome.manager.JNPjSip;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.ehome.sipservice.SipServiceCommand;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.tencent.mmkv.MMKV;

/**
 * Author: wangguodong
 * Date: 2022/3/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: PJSIP全局监听工具类,这里要使用全局的工具类，方便一些公共的消息处理。
 */
public class JCmyPjSipMessageListener implements JNPjSip.PjSipMessageListener{
    String TAG="JCmyPjSipMessageListener";

    @Override
    public void onInstantMessage(String msg, String from_uri, String to_uri) {
        Log.d("JCmyPjSipMessage","JCmyPjSipMessageListener...onInstantMessage..."+ msg);
        try {
            JCimMessageBean jCimMessageBean = new Gson().fromJson(msg, JCimMessageBean.class);
            String loginMsgUuid= MMKV.defaultMMKV().decodeString("loginMsgId");
            String msgUuid=jCimMessageBean.getMsgID();
            if (
                    TextUtils.equals(JCSessionType.CHAT_SYSTEM, jCimMessageBean.getSessionType())
                            && TextUtils.equals(JCmessageType.LOGIN_SUCCESS, jCimMessageBean.getMsgType())
            ){
                if (
                        TextUtils.equals(JNBaseUtilManager.deviceType, jCimMessageBean.getDeviceType())
                                && !TextUtils.isEmpty(loginMsgUuid)
                                && !TextUtils.equals(loginMsgUuid,msgUuid)
                ){
                    JCChatManager.loginOutOther();
                }
                return;
            }

            Log.d("PjSipMessageListener","msgType=="+jCimMessageBean.getMsgType());
            if (TextUtils.equals(JCSessionType.SESSION_CREATE_MEETING, jCimMessageBean.getMsgType())){
                JCmessageListenerManager.onReceiveCreateMeetingMsg("meeting_session", jCimMessageBean);
                return;
            }
            if (TextUtils.equals(JCSessionType.SESSION_MEETING, jCimMessageBean.getSessionType())){
                return;
            }

            jCimMessageBean.setStatus(""+JCmessageStatusType.sendSuccessceN);
            jCimMessageBean.setReceiveTime(""+System.currentTimeMillis());
            jCimMessageBean.setSaveUserId(JNBasePreferenceSaves.getUserSipId());

            String ip = jCimMessageBean.getToRealm();
            Log.e(TAG, "onInstantMessage: ======SipID====" +JNSpUtils.getString(JCChatManager.mContext,JNPjSipConstants.PJSIP_NUMBER));
            Log.e(TAG, "onInstantMessage: ======HOST====" +JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_HOST));
            if (TextUtils.equals(jCimMessageBean.getToSipID(), JNSpUtils.getString(JCChatManager.mContext,JNPjSipConstants.PJSIP_NUMBER))
//            TODO  这里由于sip服务发送过来的消息是公网的ip，我们由于有VPN使用的是内网ip所以匹配不上
//            && TextUtils.equals(ip.substring(0, ip.indexOf(":")), JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_HOST))
            ){
//            && TextUtils.equals(jCimMessageBean.getToRealm().substring(0, jCimMessageBean.getToRealm().indexOf(":")), JCSpUtils.getString(JCchatManager.mContext,JCPjSipConstants.PJSIP_HOST))){
                if (TextUtils.equals(""+JCSessionType.CHAT_PERSION, jCimMessageBean.getSessionType())
                    || TextUtils.equals(""+JCSessionType.CHAT_SYSTEM, jCimMessageBean.getSessionType())
//                    || TextUtils.equals(""+JCSessionType.CHAT_SYSTEM_NOTIFY, jCimMessageBean.getSessionType())//这里暂时没问题，不过看后台数据这个水平，估计后期需要
                    ){
                    jCimMessageBean.setSessionId(jCimMessageBean.getFromSipID());
                    jCimMessageBean.setSessionRealm(jCimMessageBean.getFromRealm());
                    jCimMessageBean.setSessionRealmJava(jCimMessageBean.getFromRealmJava());
                }
//                192.168.1.5:5060   发送的消息
                Log.e("JCchatFragment", "JCmyPjSipMessageListener===onInstantMessage++ip=" + ip);
                String key = jCimMessageBean.getSessionId()+"_"+jCimMessageBean.getSessionType()+"_"+jCimMessageBean.getSessionRealm();
                Log.e("JCchatFragment", "JCmyPjSipMessageListener===onInstantMessage++key=" + key);
                JCmessageListenerManager.onReceiveMessage(key, jCimMessageBean);
            }else {
                Log.e("JCchatFragment", "JCmyPjSipMessageListener===不是给我发送的消息=sipid==sipurl=对不上=");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onInstantMessageStatus(String msg, String to_uri,int state) {
//        state=202-发送成功；408-发送超时；
        Log.d("message", "JNPjSip==JCmyPjSipMessageListener===onInstantMessageStatus++" + "===" + msg + "====" + to_uri+"=====state=="+state);
//        String userID = JCSpUtils.getString(JCchatManager.mContext,"myAccount", JCPjSipConstants.PJSIP_NUMBER_DEFAULT);
//        Log.e("JCchatFragment", "JCmyPjSipMessageListener===onInstantMessage++userID=" + userID);
//        sip:1042@192.168.1.5
        try {
            JCimMessageBean jCimMessageBean = new Gson().fromJson(msg, JCimMessageBean.class);
            if (TextUtils.equals(JCSessionType.SESSION_MEETING, jCimMessageBean.getSessionType())){
                return;
            }
            int stateType = (state/100==2? JCmessageStatusType.sendSuccessceS:JCmessageStatusType.sendFail);
            jCimMessageBean.setStatus(""+stateType);
            String ip = to_uri.substring(to_uri.indexOf("@")+1);
            Log.e("JCchatFragment", "JCmyPjSipMessageListener===onInstantMessageStatus++ip=" + ip);
            JCmessageListenerManager.onSendMessageState(jCimMessageBean.getSessionId()+"_"+jCimMessageBean.getSessionType()+"_"+ip, jCimMessageBean, stateType);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

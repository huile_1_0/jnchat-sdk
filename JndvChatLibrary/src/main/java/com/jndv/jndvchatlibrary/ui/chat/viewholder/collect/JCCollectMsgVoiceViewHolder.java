package com.jndv.jndvchatlibrary.ui.chat.viewholder.collect;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.http.api.JCGetCollectMessageApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgVoiceContentBean;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;

/**
 * Author: wangguodong
 * Date: 2022/6/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 收藏消息，viewHolder实体
 */
public class JCCollectMsgVoiceViewHolder extends JCCollectMsgBaseViewHolder{
    private TextView tvMessage;
    private ImageView ivLeftVoice;
    private ImageView ivRightVoice;
    public JCCollectMsgVoiceViewHolder(Context context, View itemView) {
        super(context, itemView);
    }

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_voice;
    }

    @Override
    protected void showContentView(JCGetCollectMessageApi.ColloctBean colloctBean) {
        tvMessage = findViewById(R.id.tv_msg_long);
        ivLeftVoice = findViewById(R.id.iv_left_voice);
        ivRightVoice = findViewById(R.id.iv_right_voice);
        ivLeftVoice.setVisibility(View.VISIBLE);
        ivRightVoice.setVisibility(View.GONE);
        try {
            JCbaseIMMessage message = new Gson().fromJson(colloctBean.getBody(), JCimMessageBean.class);
            JCMsgVoiceContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(message.getContent()), JCMsgVoiceContentBean.class);
            tvMessage.setText(bean.getTime());
        }catch (Exception e){
            JNLogUtil.e("==JCCollectMsgTextViewHolder==showContentView==", e);
//            tvMessage.setText("未识别的消息");
        }
    }

}

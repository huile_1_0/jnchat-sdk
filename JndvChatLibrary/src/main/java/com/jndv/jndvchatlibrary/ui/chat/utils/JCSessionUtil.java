package com.jndv.jndvchatlibrary.ui.chat.utils;

import android.text.TextUtils;

import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.chat.JCSessionType;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/5/6
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCSessionUtil {
    public static boolean isEquals(JCSessionListBean newJCSessionListBean, JCSessionListBean jcSessionListBean){
        if (newJCSessionListBean.getSession_id() == jcSessionListBean.getSession_id()){
            return true;
        }
        return false;
    }
    public static boolean isEquals(String sessionId, String sessionType, String sessionAddressSip, JCSessionListBean jcSessionListBean){
        if (TextUtils.equals(sessionId, ""+jcSessionListBean.getOther_id())
                && TextUtils.equals(sessionType, ""+jcSessionListBean.getType())
                && TextUtils.equals(sessionAddressSip, jcSessionListBean.getSession_party_domain_addr())){
            return true;
        }
        return false;
    }
    public static boolean isEquals(String sId, JCSessionListBean jcSessionListBean){
        if (TextUtils.equals(sId, ""+jcSessionListBean.getSession_id())){
            return true;
        }
        return false;
    }

    public static JCSessionListBean getJCSessionListBean(int sId){
        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
        return builder
//                .equal(JCSessionListBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                .equal(JCSessionListBean_.session_id, sId).build().findFirst();
    }

    public static JCSessionListBean getJCSessionListBean(String otherId, String type, String sessionAddressSip){
        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
        JNLogUtil.e("==JCSessionUtil==getJCSessionListBean===otherId="+otherId
                +"==user_id="+ JNBasePreferenceSaves.getUserSipId()
                +"==type="+type
                +"==session_party_domain_addr="+sessionAddressSip
        );
        if (null==otherId)otherId="";
        return builder.equal(JCSessionListBean_.other_id, otherId, QueryBuilder.StringOrder.CASE_SENSITIVE)
                .equal(JCSessionListBean_.user_id, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                .equal(JCSessionListBean_.type, type,QueryBuilder.StringOrder.CASE_SENSITIVE)
                .equal(JCSessionListBean_.session_party_domain_addr, sessionAddressSip,QueryBuilder.StringOrder.CASE_SENSITIVE)
                .build().findFirst();
    }

    public static boolean isGroupStateOk(int sessionId){
        return isGroupStateOk(sessionId, true);
    }
    public static boolean isGroupStateOk(int sessionId, boolean isToast){
        try {
            JCSessionListBean jcSessionListBean = JCSessionUtil.getJCSessionListBean(sessionId);
            if (null==jcSessionListBean)return false;
            if (jcSessionListBean.getType() == JCSessionType.CHAT_GROUP){
                if (1==jcSessionListBean.getStatus()){
                    if (isToast)JCChatManager.showToast("群聊已解散!");
                    return false;
                }else if (2==jcSessionListBean.getStatus()){
                    if (isToast)JCChatManager.showToast("已退出群聊!");
                    return false;
                }else if (3==jcSessionListBean.getStatus()){
                    if (isToast)JCChatManager.showToast("被踢出群聊!");
                    return false;
                }
            }
        }catch (Exception e){
            JNLogUtil.e("==JCSessionUtil==isGroupStateOk==",e);
        }
        return true;
    }

}

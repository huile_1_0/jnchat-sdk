package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.http.api.JNGetUserDetails;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.databinding.FragmentJcusersearchaddBinding;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCAddFriendApi;
import com.jndv.jndvchatlibrary.http.api.JCDeleteMessageApi;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jnbaseutils.chat.JCFriendBean;
import com.jndv.jnbaseutils.chat.JCFriendBean_;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCCreateSessionUtils;
import com.jndv.jndvchatlibrary.utils.JCFriendUtil;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserDetailBean;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/5/4
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 用户信息页，主要用于扫码加好友，搜索加好友
 */
public class JCUserSearchAddFragment extends JCbaseFragment {
    private FragmentJcusersearchaddBinding binding;
    private JCUserSearchAddViewModel jcUserSearchAddViewModel;
    private String sessionId = "";
    private String sipId = "0";
    private String sipAddress = "0";
    private String javaAddress = "0";
    private String name;
    private String head;
    private int type = 0 ;//0=普通；1=搜索
    public static final String INTYPE = "intype";
    public static final String INSIPID = "siPid";
    public static final String INSIPADDRESS = "siPADDRESSS";
    public static final String INJAVAADDRESS = "javaADDRESSS";
    public static final String CODE_INNAME = "CODE_INNAME" ;
    public static final String CODE_INHEAD = "CODE_INHEAD" ;
    LoadingPopupView loadingPopupView;
    JCFriendBean jcFriendBean ;
    JCSessionListBean messageBean=null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        jcUserSearchAddViewModel =
                new ViewModelProvider(this).get(JCUserSearchAddViewModel.class);
        binding = FragmentJcusersearchaddBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        loadingPopupView = new XPopup.Builder(getContext()).asLoading("正在加载中");
        jcUserSearchAddViewModel.init(this);
        init(root);

        return root;
    }

    private void init(View view) {
        Bundle bundle = getArguments();
        if (bundle != null){
            type = bundle.getInt(INTYPE);
            sessionId = bundle.getString(JCEntityUtils.SESSION_ID);
            name = bundle.getString(CODE_INNAME);
            head = bundle.getString(CODE_INHEAD);
            sipId = bundle.getString(INSIPID);
            sipAddress = bundle.getString(INSIPADDRESS);
            javaAddress = bundle.getString(INJAVAADDRESS);
//            Log.e("TAG", "init: ===============01=sessionId="+sessionId );
        }
        if (0!=type){
            binding.cvSearch.setVisibility(View.VISIBLE);
            binding.llUser.setVisibility(View.GONE);
            jcUserSearchAddViewModel.getChatManageModel().observeForever(new Observer<>() {
                @Override
                public void onChanged(JNGetUserDetails.UserDetailBean jcUserDetailBean) {
                    Log.e("0330", "onChanged: ===========" + new Gson().toJson(jcUserDetailBean));
                    if(jcUserDetailBean==null){
                        binding.llUser.setVisibility(View.GONE);
                    }else {
                        binding.llUser.setVisibility(View.VISIBLE);
                        name = jcUserDetailBean.getNickname();
                        head = jcUserDetailBean.getHeadIcon();
                        sipId = jcUserDetailBean.getImNum();
                        binding.llUser.setVisibility(View.VISIBLE);

//                        sipAddress = jcUserDetailBean.getSipIp();
//                        String sipport = jcUserDetailBean.getImPort();

//                        if (null!=sipport&&!TextUtils.isEmpty(sipport)){
//                            sipAddress = sipAddress + ":" +sipport;
//                        }
                        sipAddress = JNBasePreferenceSaves.getSipAddress();
                        javaAddress = JNBasePreferenceSaves.getJavaAddress();
                        Log.e("0330", "onChanged: =======sipAddress==" + sipAddress);
                        Log.e("0330", "onChanged: =======javaAddress==" + javaAddress);
//                    javaAddress = jcUserDetailBean.getAccount();
                        initInfo();
                    }
                }
            });
        }else {
            initInfo();
        }

        binding.tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //创建会话
                String str = binding.etSearch.getText().toString();

                if (!str.contains("@")){
                    str = str + "@" + JNBasePreferenceSaves.getJavaAddress();
                }
                String[] s = str.split("@");
                if (s.length>1){
                    javaAddress = s[1];
                    jcUserSearchAddViewModel.getUser(s[0],s[1]);
                }else {
                    JCChatManager.showToast("请输入正确的用户账号！");
                }
            }
        });
        binding.cvCreateSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //创建会话
                try {
                    Log.e("0330", "onClick: ==creatSession==sipAddress=="+sipAddress);
                    JCCreateSessionUtils.creatSession(getActivity(), sipId, sipAddress, javaAddress
                            , "" + JCSessionType.CHAT_PERSION, name, head, true);
                }catch (Exception e){e.printStackTrace();}
            }
        });

        binding.rlQcord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(JCUserQRcodeFragment.CODE_INTYPE, 1);
                bundle.putString(JCUserQRcodeFragment.CODE_INNAME, name);
                bundle.putString(JCUserQRcodeFragment.CODE_INHEAD, head);
                bundle.putString(JCUserQRcodeFragment.CODE_INSIPID, sipId);
                bundle.putString(JCUserQRcodeFragment.CODE_INSIPADDRESS, sipAddress);
                bundle.putString(JCUserQRcodeFragment.CODE_INJAVAADDRESS, javaAddress);
                JCUserQRcodeFragment jcUserQRcodeFragment = new JCUserQRcodeFragment();
                JCbaseFragmentActivity.start(getActivity(),jcUserQRcodeFragment, "个人二维码", bundle);
            }
        });

    }

    private void initInfo(){
        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
        if (null==sessionId&&TextUtils.isEmpty(sessionId)){
            messageBean = builder
                    .equal(JCSessionListBean_.other_id, sipId, QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.user_id, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.session_party_domain_addr, null==sipAddress?"":sipAddress,QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();

            if (null==messageBean){
                binding.cvCreateSession.setVisibility(View.VISIBLE);
                initFriend();
            }else {
                initSessionInfo();
                initFriend(messageBean);
            }
        }else {
            messageBean = builder.equal(JCSessionListBean_.session_id, Long.parseLong(sessionId)).build().findFirst();
            Log.e("TAG", "init: ===============02=sessionId="+sessionId );
            if (null!=messageBean){
                initSessionInfo();
                initFriend(messageBean);
            }
        }

    }

    private void initSessionInfo(){
        if (null!=messageBean){
            binding.rlMsgMdr.setVisibility(View.VISIBLE);
            binding.rlMsgZd.setVisibility(View.VISIBLE);
            binding.vLine1.setVisibility(View.VISIBLE);
            binding.vLine2.setVisibility(View.VISIBLE);
            if (1==messageBean.getIs_top()){
                binding.switchZd.setChecked(true);
            }
            if (1==messageBean.getIs_no_disturb()){
                binding.switchMdr.setChecked(true);
            }
            binding.switchZd.setClickable(false);
            binding.switchMdr.setClickable(false);
        }
    }

    private void initFriend(JCSessionListBean messageBean){
        sipId = ""+messageBean.getOther_id();
        sipAddress = messageBean.getSession_party_domain_addr();
        name = getName(messageBean);
        head = messageBean.getOther_pic();
        javaAddress = messageBean.getSession_party_domain_addr_user();
        initFriend();
    }
    private void initFriend(){
        JCglideUtils.loadCircleImage(getActivity(), head, binding.ivUserHead);
        binding.tvUserName.setText(name);
        try {
            Box<JCFriendBean> imMsgBox = JCobjectBox.get().boxFor(JCFriendBean.class);
            QueryBuilder<JCFriendBean> builder = imMsgBox.query();
            jcFriendBean = builder
                    .equal(JCFriendBean_.friendId, sipId,QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCFriendBean_.domainAddr, null==sipAddress?"":sipAddress,QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCFriendBean_.saveUid, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();
            if (null!=jcFriendBean){
                binding.cvDeleteFriend.setVisibility(View.VISIBLE);
                binding.cvAddFriend.setVisibility(View.GONE);
            }else {
                binding.cvAddFriend.setVisibility(View.VISIBLE);
                binding.cvDeleteFriend.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        binding.cvAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JCFriendUtil.addFrend(getActivity(), sipId, null==sipAddress?"":sipAddress, "", name, head, null==javaAddress?"":javaAddress
                        , new OnHttpListener<JCAddFriendApi.Bean>() {
                            @Override
                            public void onSucceed(JCAddFriendApi.Bean result) {
                                try {
                                    if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                        Toast.makeText(getActivity(), "添加成功！", Toast.LENGTH_SHORT).show();
                                        JCFriendBean jcFriendBean = new JCFriendBean();
                                        JCFriendUtil.setInfo(result.getData(), jcFriendBean);
                                        jcFriendBean.setSaveUid(JNBasePreferenceSaves.getUserSipId());
                                        JCobjectBox.get().boxFor(JCFriendBean.class).put(jcFriendBean);
                                        binding.cvDeleteFriend.setVisibility(View.VISIBLE);
                                        binding.cvAddFriend.setVisibility(View.GONE);
                                    }else if (TextUtils.equals("1001000002", result.getCode())){
                                        Toast.makeText(getActivity(), "已成为好友！", Toast.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(getActivity(), "添加失败！", Toast.LENGTH_SHORT).show();
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFail(Exception e) {
                                Toast.makeText(getActivity(), "添加失败,稍后再试！", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        binding.cvDeleteFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JCFriendUtil.deletFrend(getActivity(), sipId, null==sipAddress?"":sipAddress
//                JCFriendUtil.deletFrend(getActivity(), "" + messageBean.getOther_id(), messageBean.getSession_party_domain_addr()
                        , new OnHttpListener<JCDeleteMessageApi.Bean>() {
                            @Override
                            public void onSucceed(JCDeleteMessageApi.Bean result) {
                                try {
                                    if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                        Toast.makeText(getActivity(), "删除成功！", Toast.LENGTH_SHORT).show();
                                        Box<JCFriendBean> imMsgBox = JCobjectBox.get().boxFor(JCFriendBean.class);
                                        QueryBuilder<JCFriendBean> builder = imMsgBox.query();
                                        jcFriendBean = builder
                                                .equal(JCFriendBean_.friendId, sipId,QueryBuilder.StringOrder.CASE_SENSITIVE)
                                                .equal(JCFriendBean_.domainAddr, sipAddress,QueryBuilder.StringOrder.CASE_SENSITIVE)
                                                .equal(JCFriendBean_.saveUid, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                                                .build().findFirst();

                                        if (null!=jcFriendBean)JCobjectBox.get().boxFor(JCFriendBean.class).remove(jcFriendBean);
                                        binding.cvDeleteFriend.setVisibility(View.GONE);
                                        binding.cvAddFriend.setVisibility(View.VISIBLE);
//                                    }else if (1001000002==result.getCode()){
//                                        Toast.makeText(getActivity(), "已成为好友！", Toast.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(getActivity(), "删除失败！", Toast.LENGTH_SHORT).show();
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFail(Exception e) {
                                Toast.makeText(getActivity(), "删除失败,稍后再试！", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

    }

    public static String getName(JCSessionListBean dataBean){
        String name = ""+dataBean.getOther_id();
        try {
            if (!TextUtils.isEmpty(dataBean.getRemarks())){
                name = dataBean.getRemarks();
            }else if (!TextUtils.isEmpty(dataBean.getOther_name())){
                name = dataBean.getOther_name();
            }else {
                String message = dataBean.getFirst_content();
                JCimMessageBean jCimMessageBean = new Gson().fromJson(message, JCimMessageBean.class);
                if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), jCimMessageBean.getToSipID())&&!TextUtils.isEmpty(jCimMessageBean.getFromName())){
                    name = jCimMessageBean.getFromName();
//            }else if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), jCimMessageBean.getFromID())&&!TextUtils.isEmpty(jCimMessageBean.getto)){
//                name = jCimMessageBean.getFromName();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return name;
    }

}

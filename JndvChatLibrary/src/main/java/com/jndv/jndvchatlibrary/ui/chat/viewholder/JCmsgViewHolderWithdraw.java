package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.text.TextUtils;
import android.widget.TextView;

import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jnbaseutils.chat.JCSessionType;


import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/14
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 撤回消息类型
 */
public class JCmsgViewHolderWithdraw extends JCmsgViewHolderBase {

    private TextView tvMessage;

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_withdraw;
    }

    @Override
    protected void inflateContentView() {
        tvMessage = findViewById(R.id.tv_msg_text);
    }

    @Override
    protected void bindContentView() {
        String text = "一条消息被撤回！";
        if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), message.getFromSipID())){
            //自己发的消息
            text = "你撤回了一条消息！";
        }else {
            if (TextUtils.equals(message.getSessionType(),""+ JCSessionType.CHAT_PERSION)){
                text = "对方撤回了一条消息！";
            }else {
                text = message.getFromName()+"撤回了一条消息！";
            }
        }
        tvMessage.setText(text);
    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return true;
    }

}

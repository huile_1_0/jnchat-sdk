package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.http.api.JNGetDeptSons;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipP2PActivity;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipVideoActivity;
import com.jndv.jndvchatlibrary.databinding.FragmentJcorganizationnewBinding;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCChatActivity;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.ui.bean.Node;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCCreateSessionUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jnbaseutils.ui.JNSimpleDividerDecoration;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Author: wangguodong
 * Date: 2022/4/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 组织机构及人员列表fragment
 */
public class JCOrganizationNewFragment extends JCbaseFragment implements OnRefreshLoadMoreListener {
    private FragmentJcorganizationnewBinding binding;
    private JCOrganizationListViewModel jcOrganizationListViewModel;
    private CommonAdapter adapter ;
    private List<Node> datas = new ArrayList<>();
    private String zoningCode ;
    public interface OnClickOrganzationListener{
        void onClick(String zongCode, String name);
//        void onClick(int nid, String name);
    }
    public OnClickOrganzationListener mOnClickOrganzationListener;

    public void setmOnClickOrganzationListener(OnClickOrganzationListener mOnClickOrganzationListener) {
        this.mOnClickOrganzationListener = mOnClickOrganzationListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        jcOrganizationListViewModel =
                new ViewModelProvider(this).get(JCOrganizationListViewModel.class);
        binding = FragmentJcorganizationnewBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);
//        EventBus.getDefault().register(this);
        return root;
    }

    private void init(View view) {
        jcOrganizationListViewModel.init(this);
        Bundle bundle = getArguments();
        if (bundle != null){
            zoningCode = bundle.getString("zoningCode");
        }
        initRecycleView();
        initViewData();
    }

    private void initViewData(){
        jcOrganizationListViewModel.getOrganizationList().observeForever(new Observer<JCOrganizationListViewModel.JCOrganzationListModelItem>() {
            @Override
            public void onChanged(JCOrganizationListViewModel.JCOrganzationListModelItem jcOrganzationListModelItem) {
                binding.listRefresh.finishRefresh(200);
                if (jcOrganzationListModelItem.getType()==0){
                    List<Node> list = new ArrayList<>();
                    if (null!=jcOrganzationListModelItem&&null!=jcOrganzationListModelItem.getData()
                            ){
                        if (null!=jcOrganzationListModelItem.getData().getData() && jcOrganzationListModelItem.getData().getData().size()>0){
                            for (int i = 0; i < jcOrganzationListModelItem.getData().getData().size(); i++) {
                                Node node = new Node();
                                node.setType(TextUtils.equals("4", jcOrganzationListModelItem.getData().getData().get(i).getOrgType())?0:1);
                                node.setLevel(0);
                                node.setZoningCode(jcOrganzationListModelItem.getData().getData().get(i).getZoningCode());
                                node.setContent(new Gson().toJson(jcOrganzationListModelItem.getData().getData().get(i)));
                                list.add(node);
                            }
                        }

//                        if (-1==jcOrganzationListModelItem.getNid()){
                            datas.clear();
                            datas.addAll(list);
                            adapter.notifyDataSetChanged();
//                        }
                    }
                }
            }
        });
        binding.listRefresh.autoRefresh();
    }

    @Override
    public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {
        binding.listRefresh.finishLoadMore(1000);
    }

    @Override
    public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
        jcOrganizationListViewModel.getOrganzationListData(zoningCode);
    }

    private String getImage(String url){
        if (null!=url&&url.startsWith("http")){
            return url;
        }else {
            return JNBaseUtilManager.filesServerUrl +url;
        }
    }

    private void initRecycleView(){
        binding.listRefresh.setEnableLoadMore(false);
        binding.listRefresh.setOnRefreshLoadMoreListener(this);
        binding.recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        JNSimpleDividerDecoration divider = new JNSimpleDividerDecoration(getActivity());
        divider.setPadding(30,20);
        binding.recycleView.addItemDecoration(divider);
        adapter = new CommonAdapter<Node>(getActivity(), R.layout.jc_item_organixation_new, datas) {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            protected void convert(ViewHolder holder, Node node, int position) {
                if (0==node.getType()){
                    try {
                        JNGetDeptSons.DataBean userBean = new Gson().fromJson(node.getContent(), JNGetDeptSons.DataBean.class);
                        Log.e("0402", "convert: ====node====" + new Gson().toJson(node));
                        JCglideUtils.loadCircleAvatar(getActivity(), getImage(userBean.getHeadIcon()), holder.getView(R.id.iv_user_head));
                        Log.e("0402", "convert: ====01====" );
                        ((TextView)holder.getView(R.id.tv_user_name)).setText(getName(userBean));
                        Log.e("0402", "convert: ====02====" );
                        holder.getView(R.id.ll_friend).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle = new Bundle();
                                bundle.putString(JCUserSearchAddFragment.INSIPID, userBean.getImNum());
//                                bundle.putString(JCUserSearchAddFragment.INSIPADDRESS, userBean.getDeptUserDomainAddr());
                                bundle.putString(JCUserSearchAddFragment.INJAVAADDRESS, JNBasePreferenceSaves.getJavaAddress());
                                bundle.putString(JCUserSearchAddFragment.CODE_INNAME, userBean.getName());
                                bundle.putString(JCUserSearchAddFragment.CODE_INHEAD, getImage(userBean.getHeadIcon()));
                                JCUserSearchAddFragment contentFragment = new JCUserSearchAddFragment();
                                JCbaseFragmentActivity.start(getActivity(), contentFragment, "用户信息", bundle);
                            }
                        });
                        (holder.getView(R.id.rl_chat_voice)).setVisibility(View.VISIBLE);
                        (holder.getView(R.id.rl_chat_video)).setVisibility(View.VISIBLE);
                        (holder.getView(R.id.rl_chat_msg)).setVisibility(View.VISIBLE);
                        holder.getView(R.id.rl_chat_voice).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), userBean.getImNum())
//                                        && TextUtils.equals(userBean.getDeptUserDomainAddr(), JNBasePreferenceSaves.getSipAddress())
                                ){
                                    JCChatManager.showToast("不能给自己拨打通话！");
                                    return;
                                }
                                RxPermissions rxPermissions=new RxPermissions(getActivity());
                                rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                                        ,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO)
                                        .subscribe(new Consumer<Boolean>() {
                                            @SuppressLint("CheckResult")
                                            @Override
                                            public void accept(Boolean aBoolean) {
                                                if (aBoolean) {
                                                    //申请的权限全部允许
                                                    String mySipNumber = JNBasePreferenceSaves.getUserSipId();
                                                    String friendSipNumber = userBean.getImNum();
                                                    if (TextUtils.isEmpty(mySipNumber)) {
                                                        Toast.makeText(getActivity(), "请先配置sip账号！", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        if (mySipNumber.equals(friendSipNumber)) {
                                                            Toast.makeText(getActivity(), "无法和自己通话！", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Intent intent;
                                                            intent = new Intent(getActivity(), JCPjSipP2PActivity.class);
                                                            intent.putExtra("tag", "outing");
                                                            intent.putExtra("number", friendSipNumber.replace(" ", ""));
                                                            getActivity().startActivity(intent);
                                                        }
                                                    }
                                                }
                                            }
                                        });
                            }
                        });
                        holder.getView(R.id.rl_chat_video).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), userBean.getImNum())
//                                        && TextUtils.equals(userBean.getDeptUserDomainAddr(), JNBasePreferenceSaves.getSipAddress())
                                ){
                                    JCChatManager.showToast("不能给自己拨打通话！");
                                    return;
                                }
                                RxPermissions rxPermissions=new RxPermissions(getActivity());
                                rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                                        ,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO)
                                        .subscribe(new Consumer<Boolean>() {
                                            @SuppressLint("CheckResult")
                                            @Override
                                            public void accept(Boolean aBoolean) {
                                                if (aBoolean) {
                                                    String mySipNumber = JNBasePreferenceSaves.getUserSipId();
                                                    String friendSipNumber = userBean.getImNum();
                                                    if (TextUtils.isEmpty(mySipNumber)) {
                                                        Toast.makeText(getActivity(), "请先配置sip账号！", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        if (mySipNumber.equals(friendSipNumber)) {
                                                            Toast.makeText(getActivity(), "无法和自己通话！", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Intent intent =null;
                                                            intent = new Intent(getActivity(), JCPjSipVideoActivity.class);
                                                            intent.putExtra("tag", "outing");
                                                            intent.putExtra("number", friendSipNumber.replace(" ", ""));
                                                            getActivity().startActivity(intent);
                                                        }
                                                    }

                                                }
                                            }
                                        });
                            }
                        });
                        holder.getView(R.id.rl_chat_msg).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), userBean.getImNum())
//                                        && TextUtils.equals(userBean.getDeptUserDomainAddr(), JNBasePreferenceSaves.getSipAddress())
                                ){
                                    JCChatManager.showToast("不能给自己发消息！");
                                    return;
                                }
                                String friendNum = userBean.getImNum();
                                JCSessionListBean messageBean = JCSessionUtil.getJCSessionListBean(
                                        friendNum, JCSessionType.CHAT_PERSION, JNBasePreferenceSaves.getSipAddress());
                                if (null==messageBean){
                                    new XPopup.Builder(getActivity()).asConfirm("提醒", "还没聊过天，是否现在就去？",
                                            new OnConfirmListener() {
                                                @Override
                                                public void onConfirm() {
                                                    try {
                                                        JCCreateSessionUtils.creatSession(getActivity(), friendNum
                                                                , JNBasePreferenceSaves.getSipAddress(), JNBasePreferenceSaves.getJavaAddress()
                                                                , "" + JCSessionType.CHAT_PERSION, userBean.getName(), "", true);
                                                    }catch (Exception e){e.printStackTrace();}
                                                }
                                            }).show();
                                }else {//去聊天或者去人员详情
                                    JCChatActivity.start(getActivity(), messageBean);
                                }
                            }
                        });
                        Log.e("0402", "convert: ====03====" );
                    }catch (Exception e){e.printStackTrace();}
                }else {
                    try {
                        JNGetDeptSons.DataBean departBean = new Gson().fromJson(node.getContent(), JNGetDeptSons.DataBean.class);
                        ((TextView)holder.getView(R.id.tv_user_name)).setText(departBean.getName());
                        (holder.getView(R.id.rl_chat_voice)).setVisibility(View.GONE);
                        (holder.getView(R.id.rl_chat_video)).setVisibility(View.GONE);
                        (holder.getView(R.id.rl_chat_msg)).setVisibility(View.GONE);
                        JCglideUtils.loadCircleImage(getActivity(), R.mipmap.jc_organzation_depart_circle, holder.getView(R.id.iv_user_head));
                        holder.getView(R.id.ll_friend).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (null!=mOnClickOrganzationListener){
                                    mOnClickOrganzationListener.onClick(node.getZoningCode(), departBean.getName());
                                }
                            }
                        });
                    }catch (Exception e){e.printStackTrace();}
                }
            }
        };
        binding.recycleView.setAdapter(adapter);

    }

    public String getName(JNGetDeptSons.DataBean userBean){
        String name = "";
        if (null!=userBean){
            if (null!=userBean.getName()&&!TextUtils.isEmpty(userBean.getName())){
                name = userBean.getName();
            }else if (null!=userBean.getImNum()&&!TextUtils.isEmpty(userBean.getImNum())){
                name = userBean.getImNum();
            }else if (null!=userBean.getMobile()&&!TextUtils.isEmpty(userBean.getMobile())){
                name = userBean.getMobile();
            }
        }
        return name;
    }

}

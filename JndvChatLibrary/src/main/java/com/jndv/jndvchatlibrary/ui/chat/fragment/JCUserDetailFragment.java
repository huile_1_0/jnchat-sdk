package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.google.gson.Gson;
import com.google.zxing.client.android.utils.ZXingUtils;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.utils.CalculateUtils;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.databinding.FragmentJcUserDetailBinding;
import com.jndv.jndvchatlibrary.http.api.JCImagesUpApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCUserQRcodeBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserDetailBean;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCUserDetailViewModel;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jndvchatlibrary.utils.JCQRcodeBeanBase;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.jndv.jndvchatlibrary.utils.JCmyImagePicker;
import com.qingmei2.rximagepicker.core.RxImagePicker;
import com.qingmei2.rximagepicker.entity.Result;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.tencent.mmkv.MMKV;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link JCUserDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JCUserDetailFragment extends JCbaseFragment {

    private String userId;
    private String crowdId;
    private int type;
    private String invite_permission;
    private String group_domain_addr;
    private String domain_addr;
    private FragmentJcUserDetailBinding detailBinding;
    private JCUserDetailViewModel viewModel;
    private FragmentActivity activity;
    String headUrl ;
    OptionsPickerView pvPermissinOptions;
    boolean isMySelf = false;

    public JCUserDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getString(JCEntityUtils.USER_ID, JNBasePreferenceSaves.getUserSipId());
            crowdId = getArguments().getString(JCEntityUtils.CROWD_ID);
            type = getArguments().getInt(JCEntityUtils.IS_FROM_CROWD, 0);
            group_domain_addr = getArguments().getString(JCEntityUtils.GROUP_DOMAIN_ADDR);
            domain_addr = getArguments().getString(JCEntityUtils.DOMAIN_ADDR);
            invite_permission = getArguments().getString(JCEntityUtils.INVITE_PERMISSION);
        } else {
            userId = JNBasePreferenceSaves.getUserSipId();
        }
        Log.d("zhang","invite_permission==="+invite_permission);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel =
                new ViewModelProvider(this).get(JCUserDetailViewModel.class);
        detailBinding = FragmentJcUserDetailBinding.inflate(inflater, container, false);
        activity = getActivity();
        initView();
        initData();
        return detailBinding.getRoot();
    }

    private void initView() {
        viewModel = new JCUserDetailViewModel();
        viewModel.init(activity);
        boolean isSelf = (JNBasePreferenceSaves.getUserSipId().equals(userId));

        if (!isSelf) {
            if (type == 1) {//好友进来
                detailBinding.detailTvDelete.setVisibility(View.VISIBLE);
                detailBinding.detailTvDelete.setText("删除好友");
            } else if (type == 2) {//群组过来
                detailBinding.detailTvDelete.setVisibility(View.VISIBLE);
                detailBinding.detailTvDelete.setText("踢出群聊");
                initPermission();
            }
        }

        setJcFragmentSelect(new JCFragmentSelect() {
            @Override
            public void onSelecte(int type, Object... objects) {
                if (type == SELECTE_TYPE_PIC_CROP){
//                    Bitmap bitmap = (Bitmap) objects[1];
                    JCFilesUpUtils.upImage((Uri) objects[0], "", new OnHttpListener<JCImagesUpApi.Bean>() {
                        @Override
                        public void onSucceed(JCImagesUpApi.Bean result) {
                            try {
//                                剪切成功，调用上传接口
                                headUrl = JNBaseUtilManager.getFilesUrl(result.getUrlArr().get(0).getTailUrl());
                                viewModel.updateUserInfo(headUrl);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFail(Exception e) {
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    JCChatManager.showToast("图片上传失败！");
                                }
                            });
                        }
                    });
                }
            }
        });

        detailBinding.userInfoContainer.detailIvQr.setOnClickListener(v -> {
            JCUserQRcodeFragment jcUserQRcodeFragment = new JCUserQRcodeFragment();
            JCbaseFragmentActivity.start(getActivity(), jcUserQRcodeFragment, "个人二维码");
        });
        detailBinding.userInfoContainer.detailIvHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMySelf){
                    RxPermissions rxPermissions = new RxPermissions(getActivity());
                    rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                                    , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
                            )
                            .subscribe(new Consumer<Boolean>() {
                                @SuppressLint("CheckResult")
                                @Override
                                public void accept(Boolean aBoolean) {
                                    if (aBoolean) {
                                        //申请的权限全部允许
                                        RxImagePicker
                                                .create(JCmyImagePicker.class)
                                                .openGallery(getActivity())
                                                .subscribe(new Consumer<Result>() {
                                                    @Override
                                                    public void accept(Result result) {
                                                        Uri uri = result.getUri();
                                                        JNLogUtil.e("uri", uri.toString());
                                                        startCropActivity(uri, 1, 1, 200, 200);
                                                    }
                                                });
                                    } else {
                                        //只要有一个权限被拒绝，就会执行
                                        JCChatManager.showToast("未授权权限，部分功能不能使用");
                                    }
                                }
                            });
                }
            }
        });

        detailBinding.detailLlPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvPermissinOptions.show();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void initData() {
        viewModel.getUpdateData().observeForever(new Observer<JCRespondBean>() {
            @Override
            public void onChanged(JCRespondBean jcRespondBean) {
                try {
                    if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, jcRespondBean.getCode())){
                        JCChatManager.showToast("操作成功！");
                        JCglideUtils.loadCircleImage(getActivity(), headUrl, detailBinding.userInfoContainer.detailIvHead);
                        JNBasePreferenceSaves.saveUserHead(headUrl);
                    }else {
                        JCChatManager.showToast("操作失败！");
                    }
                }catch (Exception e){
                    JNLogUtil.e("=getUpdateData=onChanged=", e);
                }
            }
        });
        viewModel.getUserDetail(userId)
                .getUserData().observe(activity, (data) -> {
            if (data != null) {
                try {
                    String uuid = MMKV.defaultMMKV().getString(JNBaseConstans.MMKV_KEY_USER_UUID, "");

                    isMySelf = TextUtils.equals(uuid, data.getUuid());
                    detailBinding.userInfoContainer.detailTvName.setText(data.getNickname());
                    JCglideUtils.loadCircleImage(getActivity(), data.getHeadIcon(), detailBinding.userInfoContainer.detailIvHead);

//                JCglideUtils.loadImageToView(activity, data.getAvatarUrl(), detailBinding.userInfoContainer.detailIvHead);
                    detailBinding.detailTvIm.setText(data.getImNum());
                    detailBinding.userInfoContainer.detailTvAccount.setText(data.getAccount());
                    detailBinding.detailTvPhone.setText(data.getMobile());

                detailBinding.detailTvSex.setText(("1".equals(data.getSex()) ? "男" : "女"));
                detailBinding.detailTvIdCard.setText(data.getIdCardNum());
                detailBinding.detailTvAge.setText(CalculateUtils.countAge(data.getIdCardNum())+"");

                    JCUserQRcodeBean bean = new JCUserQRcodeBean(data.getNickname(), data.getHeadIcon(), data.getImNum(), JNBasePreferenceSaves.getSipAddress(), JNBasePreferenceSaves.getJavaAddress());
                    JCQRcodeBeanBase jcqRcodeBeanBase = new JCQRcodeBeanBase(JCQRcodeBeanBase.QRCODE_TYPE_USER, new Gson().toJson(bean));
                    Bitmap qrBitmap= ZXingUtils.createQRCodeImage(new Gson().toJson(jcqRcodeBeanBase));
                    JCglideUtils.loadImage(getActivity(), qrBitmap, detailBinding.userInfoContainer.detailIvQr);
                }catch (Exception e){
                    JNLogUtil.e("=getUpdateData=onChanged=", e);
                }
            }
        });

        detailBinding.detailTvDelete.setOnClickListener(v -> {
            if (type == 1) {
                // viewModel.deleteFrend()
            } else if (type == 2) {
                viewModel.kicksMember(crowdId, group_domain_addr, userId, domain_addr).getRespondData().observe(activity, new Observer<JCRespondBean>() {
                    @Override
                    public void onChanged(JCRespondBean jcRespondBean) {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, jcRespondBean.getCode())) {
                            activity.finish();
                        } else {
//                            showToast(jcRespondBean.getMsg());
                        }
                    }
                });

            }

        });
    }


    public static JCUserDetailFragment newInstance(String userId, String crowdId, String domain_addr, String group_domain_addr, int type,String invite_permission) {
        JCUserDetailFragment fragment = new JCUserDetailFragment();
        Bundle args = new Bundle();
        args.putString(JCEntityUtils.USER_ID, userId);
        args.putString(JCEntityUtils.CROWD_ID, crowdId);
        args.putString(JCEntityUtils.INVITE_PERMISSION, invite_permission);
        args.putInt(JCEntityUtils.IS_FROM_CROWD, type);
        args.putString(JCEntityUtils.DOMAIN_ADDR, domain_addr);
        args.putString(JCEntityUtils.GROUP_DOMAIN_ADDR, group_domain_addr);
        fragment.setArguments(args);
        return fragment;
    }

    private void initPermission(){
        detailBinding.detailLlPermission.setVisibility(View.VISIBLE);
        detailBinding.tvDetailPermission.setText(TextUtils.equals("0",invite_permission)?"关闭":"开启");
        List<String> list=new ArrayList<>();
        list.add("关闭");
        list.add("开启");
        pvPermissinOptions = new OptionsPickerBuilder(getActivity(), new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
                setPermission(options1);
                detailBinding.tvDetailPermission.setText(list.get(options1));
            }
        }).build();
        pvPermissinOptions.setPicker(list);
    }

    private void setPermission(int pos){
        viewModel.invitePerUpdate(crowdId,group_domain_addr,domain_addr,userId,String.valueOf(pos));
    }
}

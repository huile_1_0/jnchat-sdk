package com.jndv.jndvchatlibrary.ui.chat.fragment.popup;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.jndv.jndvchatlibrary.R;
import com.lxj.xpopup.core.CenterPopupView;

import org.jetbrains.annotations.NotNull;

/**
 * Author: wangguodong
 * Date: 2020/9/29
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 网络设置弹窗
 */
public class TipCenterPopup extends CenterPopupView {

    private Context mContext ;
    private TextView tv_close ;
    private TextView tv_title ;
    private TextView tv_content ;
    private TextView tv_clean ;
    private TextView tv_ok ;

    String title ;
    String content ;
    String cleanStr ;
    String okStr ;
    OnClickListener cleanListenner;
    OnClickListener okListenner;

    public TipCenterPopup setTitle(String title) {
        this.title = title;
        initView();
        return this;
    }

    public TipCenterPopup setContent(String content) {
        this.content = content;
        initView();
        return this;
    }

    public TipCenterPopup setCleanListenner(String cleanStr, OnClickListener cleanListenner) {
        this.cleanStr = cleanStr;
        this.cleanListenner = cleanListenner;
        initView();
        return this;
    }

    public TipCenterPopup setOkListenner(String okStr, OnClickListener okListenner) {
        this.okStr = okStr;
        this.okListenner = okListenner;
        initView();
        return this;
    }

    public TipCenterPopup(@NonNull Context context) {
        super(context);
        mContext = context;
    }

    public TipCenterPopup(@NonNull @NotNull Context context, Context mContext, String title
            , String content, String cleanStr, String okStr, OnClickListener cleanListenner
            , OnClickListener okListenner) {
        super(context);
        this.mContext = mContext;
        this.title = title;
        this.content = content;
        this.cleanStr = cleanStr;
        this.okStr = okStr;
        this.cleanListenner = cleanListenner;
        this.okListenner = okListenner;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.jc_popup_tip_center;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        findView();
        initView();
        initViewClick();
    }

    private void findView(){
        tv_close = findViewById(R.id.tv_close);
        tv_title = findViewById(R.id.tv_title);
        tv_content = findViewById(R.id.tv_content);
        tv_clean = findViewById(R.id.tv_clean);
        tv_ok = findViewById(R.id.tv_ok);
    }

    private void initView(){
        try {
            if (TextUtils.isEmpty(title)){
                tv_title.setVisibility(GONE);
            }else {
                tv_title.setText(title);
                tv_title.setVisibility(VISIBLE);
            }
            if (TextUtils.isEmpty(content)){
                tv_content.setVisibility(GONE);
            }else {
                tv_content.setText(content);
                tv_content.setVisibility(VISIBLE);
            }
            if (TextUtils.isEmpty(cleanStr)){
                if (null==cleanListenner)tv_clean.setVisibility(GONE);
                else tv_clean.setVisibility(VISIBLE);
            }else {
                tv_clean.setText(cleanStr);
                tv_clean.setVisibility(VISIBLE);
            }
            if (TextUtils.isEmpty(okStr)){
                if (null==okListenner)tv_ok.setVisibility(GONE);
                else tv_ok.setVisibility(VISIBLE);
            }else {
                tv_ok.setText(okStr);
                tv_ok.setVisibility(VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initViewClick(){

        tv_clean.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null==cleanListenner)dismiss();
                else cleanListenner.onClick(view);
            }
        });
        tv_ok.setOnClickListener(new OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                if (null!=okListenner) okListenner.onClick(view);
            }
        });
        tv_close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }
}

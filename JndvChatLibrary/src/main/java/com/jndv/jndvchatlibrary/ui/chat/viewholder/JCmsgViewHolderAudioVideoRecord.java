package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgAudioVideoRecordContentBean;

import java.util.ArrayList;
import java.util.List;

public class JCmsgViewHolderAudioVideoRecord extends JCmsgViewHolderBase {

    private TextView tvMessage;

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_text;
    }

    @Override
    protected void inflateContentView() {
        tvMessage = findViewById(R.id.tv_msg_text);

    }

    @Override
    protected void bindContentView() {
        try {
        Log.e("bindContentView",message.getContent());
        JNLogUtil.e("======Content=="+base64ToString(message.getContent()));
        Log.d("zhang","混合混合content=="+base64ToString(message.getContent()));

            JCMsgAudioVideoRecordContentBean bean = new Gson().fromJson(base64ToString(message.getContent()), JCMsgAudioVideoRecordContentBean.class);
//            tvMessage.setTextColor(Color.parseColor(bean.getFontColor()));
//            tvMessage.setTextSize(Float.parseFloat(""+bean.getFontSize()));
//            JNLogUtil.loge("======SendTime=="+message.getSendTime());
//            JNLogUtil.loge("======ReceiveTime=="+message.getReceiveTime());
            if(bean.getConversionTime().equals("00:00:00")){
                tvMessage.setText("音视频未接通");
            }else{
                tvMessage.setText("聊天时长："+bean.getConversionTime());
            }

//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            tvMessage.setText(bean.getText()+"\n"+format.format(new Date(Long.parseLong(message.getSendTime())))
//                    +"\n"+format.format(new Date(Long.parseLong(message.getReceiveTime()))));
        }catch (Exception e){
            e.printStackTrace();
            JNLogUtil.e("======getMsgType=="+message.getMsgType());
            tvMessage.setText("未识别的消息："+message.getContent());
        }

    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_copy), 2));
//        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_transmit), 2));
//        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_collect), 2));
//        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_withdraw), 1));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_delete), 2));
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return false;
    }

}

package com.jndv.jndvchatlibrary.ui.chat.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.AbsoluteSizeSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.bigkoo.pickerview.view.TimePickerView;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.ehome.sipservice.SipServiceCommand;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.http.api.JNGetDeptSons;
import com.jndv.jnbaseutils.ui.bean.Node;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ChatContant;
import com.jndv.jndvchatlibrary.bean.UserBean;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jndvchatlibrary.utils.ActivityStack;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditTTSContentActivity extends JCbaseFragmentActivity implements View.OnClickListener {

    TextView tvCallNum;
    TextView tvNotifyType;
    TextView tvEndTime;
    TimePickerView pvCustomTime;
    OptionsPickerView pvCallNumOptions;
    OptionsPickerView pvNotifyTypeOptions;
    int maxCallNum=1;
    int notifyType;
    long timeStamp;
    EditText etContent;
    EditText et1;
    EditText et2;
    EditText et3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ttscontent);
        ActivityStack.getInstance().addActivity(this);
        initView();
        initCallNumPicker();
        initNotifyTypePicker();
        initCustomTimePicker();
    }

    private void initView() {
        setCenterTitle("编辑消息");
        TextView tvSend=findViewById(R.id.tv_send);
        tvSend.setOnClickListener(this);
        RelativeLayout rlCallNum=findViewById(R.id.rl_call_num);
        rlCallNum.setOnClickListener(this);
        RelativeLayout rlNotifyType=findViewById(R.id.rl_notify_type);
        rlNotifyType.setOnClickListener(this);
        RelativeLayout rlEndTime=findViewById(R.id.rl_end_time);
        rlEndTime.setOnClickListener(this);

        tvCallNum=findViewById(R.id.tv_call_num);
        tvNotifyType=findViewById(R.id.tv_notify_type);
        tvEndTime=findViewById(R.id.tv_end_time);

        etContent=findViewById(R.id.et_content);
        et1=findViewById(R.id.et1);
        et2=findViewById(R.id.et2);
        et3=findViewById(R.id.et3);
        setHintTextSize(etContent,"请输入消息内容",14);
        setHintTextSize(et1,"请输入",14);
        setHintTextSize(et2,"请输入",14);
        setHintTextSize(et3,"请输入",14);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.rl_call_num){
            hideSoftKeyBoard();
            pvCallNumOptions.show();
        }else if(v.getId()==R.id.rl_notify_type){
            hideSoftKeyBoard();
            pvNotifyTypeOptions.show();
        }else if(v.getId()==R.id.rl_end_time){
            pvCustomTime.show();
        }else if(v.getId()==R.id.tv_send){
            sendMsg();
        }
    }

    private void initCallNumPicker() {
        List<String> list=new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        pvCallNumOptions = new OptionsPickerBuilder(EditTTSContentActivity.this, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
                tvCallNum.setText(list.get(options1));
                maxCallNum=options1+1;
            }
        }).build();
        pvCallNumOptions.setPicker(list);
    }

    private void initNotifyTypePicker() {
        List<String> list=new ArrayList<>();
        list.add("上班时间");
        list.add("24小时");
        pvNotifyTypeOptions = new OptionsPickerBuilder(EditTTSContentActivity.this, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
                tvNotifyType.setText(list.get(options1));
                notifyType=options1;
            }
        }).build();
        pvNotifyTypeOptions.setPicker(list);
    }

    private void initCustomTimePicker() {
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        Calendar startDate = Calendar.getInstance();
        startDate.set(2014, 1, 23);
        Calendar endDate = Calendar.getInstance();
        endDate.set(2050, 3, 31);
        //时间选择器 ，自定义布局
        pvCustomTime = new TimePickerBuilder(this, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                //选中事件回调
                timeStamp=date.getTime();
                Log.d("zhang","timeStamp=="+timeStamp);
                tvEndTime.setText(getTime(date));
            }
        }).setDate(selectedDate)
           .setRangDate(startDate, endDate)
           .setLayoutRes(R.layout.pickerview_custom_time, new CustomListener() {

                    @Override
                    public void customLayout(View v) {
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
                        ImageView ivCancel = (ImageView) v.findViewById(R.id.iv_cancel);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.returnData();
                                pvCustomTime.dismiss();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.dismiss();
                            }
                        });
                    }
                })
                .setContentTextSize(18)
                .setType(new boolean[]{true, true, true, true, true, false})
                .setLabel("年", "月", "日", "时", "分", "秒")
                .setLineSpacingMultiplier(1.2f)
                .setTextXOffset(0, 0, 0, 40, 0, -40)
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .setDividerColor(0xFF24AD9D)
                .build();
    }

    private String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return format.format(date);
    }

    private void sendMsg(){
        Map<String,String> commentMap=new HashMap<>();
        commentMap.put("1",et1.getText().toString());
        commentMap.put("2",et2.getText().toString());
        commentMap.put("3",et3.getText().toString());

        List<UserBean> userList=new ArrayList<>();
        if(ChatContant.TTS_PERSON_LIST.size()>0){
            for(Node node: ChatContant.TTS_PERSON_LIST){
                JNGetDeptSons.DataBean bean = new Gson().fromJson(node.getContent(), JNGetDeptSons.DataBean.class);

                UserBean userBean=new UserBean();
                userBean.mobile=bean.getMobile();
                userBean.name=bean.getName();
                userBean.uid=bean.getImNum();
                userList.add(userBean);
            }
        }

        String text=etContent.getText().toString()+"。回复1代表同意,回复2代表拒绝,回复3代表忽略,按#确认。";

        Map<String,Object> contentMap=new HashMap<>();
        contentMap.put("calltype",notifyType);
        contentMap.put("comment",commentMap);
        contentMap.put("endtime",timeStamp);
        contentMap.put("maxcall",maxCallNum);
        contentMap.put("text",text);
        contentMap.put("users",userList);

        String content=Base64.encodeToString(new Gson().toJson(contentMap).getBytes(), Base64.NO_WRAP);
        JCimMessageBean bean = JCchatFactory.creatTTSMessage(content);
        String userID = JNSpUtils.getString(this, "myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
        Log.d("SipService","msg==tts="+new Gson().toJson(bean));
        SipServiceCommand.sendMessageToBuddy(this, new Gson().toJson(bean), userID
                , "sip:112233@" + JNBasePreferenceSaves.getSipAddress(), "text/p2pmsgack+json", false);
        ActivityStack.getInstance().finishAllActivity();
    }

    /**
     * 设置 hint字体大小
     * @param editText 输入控件
     * @param hintText hint的文本内容
     * @param textSize hint的文本的文字大小（以dp为单位设置即可）
     */
    public static void setHintTextSize(EditText editText, String hintText, int textSize) {
        // 新建一个可以添加属性的文本对象
        SpannableString ss = new SpannableString(hintText);
        // 新建一个属性对象,设置文字的大小
        AbsoluteSizeSpan ass = new AbsoluteSizeSpan(textSize, true);
        // 附加属性到文本
        ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        // 设置hint
        editText.setHint(new SpannedString(ss)); // 一定要进行转换,否则属性会消失
    }

    private void hideSoftKeyBoard() {
        if (etContent != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etContent.getWindowToken(), 0);
        }
    }
}
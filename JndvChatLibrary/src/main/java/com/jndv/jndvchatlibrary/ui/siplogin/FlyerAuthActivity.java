package com.jndv.jndvchatlibrary.ui.siplogin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.fragment.app.FragmentActivity;

import com.ehome.manager.JNPjSip;
import com.ehome.manager.utils.JNLogUtil;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.databinding.ActivityFlyerAuthBinding;
import com.jndv.jndvchatlibrary.eventbus.JCCodeEvent;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jndvchatlibrary.ui.chat.bean.flycontent.FlightParamBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.flycontent.FlyerAuthBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.flyreceive.DroneVerifyDataBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCMessageListener;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCmessageListenerManager;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JFmyPjSipMessageListener;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;
import com.jndv.jndvchatlibrary.ui.crowd.utils.MD5Util;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;

import com.wega.library.loadingDialog.LoadingDialog;

import org.greenrobot.eventbus.EventBus;

/**
 * 登录页
 */
public class FlyerAuthActivity extends FragmentActivity {

    private ActivityFlyerAuthBinding binding;
    private Activity activity;
    private LoadingDialog loadingDialog = null;
    private JNPjSip pjSip;
    private static final String TAG = "FlyerAuthActivity";
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            cancelLoadding();
            JCChatManager.showToast("登录失败！");
        }
    };


    public static void start(Context context) {
        Intent intent = new Intent(context, FlyerAuthActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        viewFullScreen();
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(this);
        activity = FlyerAuthActivity.this;
        binding = ActivityFlyerAuthBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initConfig();
        initView();
    }

    private void initView() {
        Drawable leftDrawable = binding.loginUsername.getCompoundDrawables()[0];
        if (leftDrawable != null) {
            leftDrawable.setBounds(0, 0, 40, 40);
            binding.loginUsername.setCompoundDrawables(leftDrawable, binding.loginUsername.getCompoundDrawables()[1]
                    , binding.loginUsername.getCompoundDrawables()[2], binding.loginUsername.getCompoundDrawables()[3]);
        }
        Drawable leftDrawable2 = binding.loginPassword.getCompoundDrawables()[0];
        if (leftDrawable2 != null) {
            leftDrawable2.setBounds(0, 0, 40, 40);
            binding.loginPassword.setCompoundDrawables(leftDrawable2, binding.loginPassword.getCompoundDrawables()[1]
                    , binding.loginPassword.getCompoundDrawables()[2], binding.loginPassword.getCompoundDrawables()[3]);
        }

        binding.loginUsername.setText(JNBasePreferenceSaves.getString(GlobalConstant.FLIGHT_USER_ACCOUNT));
        binding.loginPassword.setText(JNBasePreferenceSaves.getString(GlobalConstant.FLIGHT_USER_PASSWORD));

        binding.loginBtnLogin.setOnClickListener(v -> {
            showLoadding();
            handler.postDelayed(runnable, 10000);
            String name = binding.loginUsername.getText().toString();
            String pass = binding.loginPassword.getText().toString();
//            JNBasePreferenceSaves.saveString("userSipPassSave", pass);
            binding.loginUsername.setText("");
            binding.loginPassword.setText("");

            GlobalConstant.sFlightAccountName = name;
            GlobalConstant.sFlightPassword = MD5Util.encrypt(pass);
            JNLogUtil.d("==LoginActivity==initView==time==01==" + System.currentTimeMillis());
            flyerLogin(GlobalConstant.sFlightAccountName, GlobalConstant.sFlightPassword);

        });
    }

    private void flyerLogin(String account,String pwd) {
        FlyerAuthBean flyerAuthBean = new FlyerAuthBean();
        flyerAuthBean.getData().setSip(account);
        flyerAuthBean.getData().setPassword(pwd);
        Log.d(TAG, "flyerLogin content: "+new Gson().toJson(flyerAuthBean));
        String contentInBase64 = JNContentEncryptUtils.encodeContent(new Gson().toJson(flyerAuthBean));
        JCimMessageBean jCimMessageBean = JCchatFactory.creatDroneIMMessage(contentInBase64, JCSessionType.SESSION_DRONE_VERIFY);
        JCMessageUtils.sendFlightMessage(jCimMessageBean,activity );
    }

    private void initConfig() {
        pjSip = JNPjSip.getInstance(this);
        pjSip.addMessageListeners(new JFmyPjSipMessageListener());
        JCmessageListenerManager.addMessageListener("droneFlyerAuth" + "_" + JCSessionType.SESSION_DRONE + "_" + JNBasePreferenceSaves.getSipAddress(), new JCMessageListener() {
            @Override
            public void onReceiveMessage(JCbaseIMMessage message, int isLook) {
                Log.e("DemoMainActivity", "onReceiveMessage: " + message.getContent());
                String content = JNContentEncryptUtils.decryptContent(message.getContent());
                Log.e("DemoMainActivity", "receiveMsgType: "+message.getMsgType());
                Log.e("DemoMainActivity", "receiveContent: "+content);
                try {
                    switch (message.getMsgType()){
                        case JCSessionType.SESSION_DRONE_VERIFY:
                            Log.d("DemoMainActivity", "newcontent SESSION_DRONE_VERIFY: "+content);
                            DroneVerifyDataBean droneVerifyDataBean = new Gson().fromJson(content,DroneVerifyDataBean.class);
                            GlobalConstant.mFhname = droneVerifyDataBean.getText().getNickname();
                            GlobalConstant.mFhzonecode = droneVerifyDataBean.getText().getZoningCode();
                            JNBasePreferenceSaves.saveUserAccount(droneVerifyDataBean.getText().getAccount());
                            JNBasePreferenceSaves.saveUserName(GlobalConstant.mFhname);
                            JNBasePreferenceSaves.saveZoningCode(GlobalConstant.mFhzonecode);

                            if(!TextUtils.isEmpty(GlobalConstant.mFhname)&&!TextUtils.isEmpty(GlobalConstant.mFhzonecode)) {
                                GlobalConstant.mFhsipstate = true;
                                EventBus.getDefault().post(new JCCodeEvent(JNEventBusType.CODE_SIP_Drone_login_flyer,""));
                            }
                            handler.removeCallbacks(runnable);
                            cancelSuccessLoadding();
                            JCChatManager.showToast("飞手登录成功！");
                            finish();
                            break;
                        default:
                            break;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onSendMessageState(JCbaseIMMessage message, int state) {

            }
        });
    }

    /**
     * 设置全屏
     * before adding content
     */
    protected void viewFullScreen() {
        // 隐藏标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onDestroy() {
        cancelLoadding();
        super.onDestroy();
    }


    /**
     * 显示加载框
     */
    public void showLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.loading();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==showLoadding==", e);
        }
    }

    /**
     * 直接取消加载框
     */
    public void cancelLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.cancel();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==cancelLoadding==", e);
        }
    }

    /**
     * 显示加载成功后取消加载框
     */
    public void cancelSuccessLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.loadSuccess();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==cancelSuccessLoadding==", e);
        }
    }

    /**
     * 显示加载失败后取消加载框
     */
    public void cancelFailLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.loadFail();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==cancelFailLoadding==", e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pjSip.sipBroadcastUnRegister();
        Log.e(TAG, "onPause: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        pjSip.sipBroadcastRegister();
        Log.e(TAG, "onResume: ");
    }

    private String createFlightData(String uuid,long timestamp,String state,int stateflag) {

        FlightParamBean flightParamBean = new FlightParamBean();
        flightParamBean.getData().setUuid(uuid);
        flightParamBean.getData().setRegno("123456");//暂时写死
        flightParamBean.getData().setTime(timestamp);
        flightParamBean.getHeader().setTimestamp(timestamp);


        flightParamBean.getData().setCurraction("10");
        //无人机验证参数
        flightParamBean.getData().setPlansipstate(GlobalConstant.mPlansipstate);
        if(!TextUtils.isEmpty(GlobalConstant.sSipNum)) {
            flightParamBean.getData().setPlansip(Long.parseLong(GlobalConstant.sSipNum));
        }
        if(!TextUtils.isEmpty(GlobalConstant.mPlanzonecode)) {
            flightParamBean.getData().setPlanzonecode(GlobalConstant.mPlanzonecode);
        }
        //飞手验证参数
        if(!TextUtils.isEmpty(GlobalConstant.sFlightAccountName)) {
            flightParamBean.getData().setFhsip(Long.parseLong(GlobalConstant.sFlightAccountName));
        }
        if(!TextUtils.isEmpty(GlobalConstant.mFhname)) {
            flightParamBean.getData().setFhname(GlobalConstant.mFhname);
        }
        if(!TextUtils.isEmpty(GlobalConstant.mFhzonecode)){
            flightParamBean.getData().setFhzonecode(GlobalConstant.mFhzonecode);
        }
        flightParamBean.getData().setFhsipstate(GlobalConstant.mFhsipstate);

        if(!TextUtils.isEmpty(state)&&stateflag!=0){
            flightParamBean.getData().setState(state);
            flightParamBean.getData().setStateflag(stateflag);
        }

        return new Gson().toJson(flightParamBean);
    }
}

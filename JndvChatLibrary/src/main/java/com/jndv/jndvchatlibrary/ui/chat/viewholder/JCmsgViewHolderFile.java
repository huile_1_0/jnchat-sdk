package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.hjq.http.listener.OnDownloadListener;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNFileUtils;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgFileContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCUriToPathUtils;
import com.jndv.jndvchatlibrary.utils.JCFileUtils;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;
import com.jndv.jndvchatlibrary.utils.JCOpenFileUtil;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
//import com.wgd.wgdfilepickerlib.utils.FileUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/3/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 图片消息
 */
public class JCmsgViewHolderFile extends JCmsgViewHolderBase {

    private ImageView ivMessage;
    private TextView tvName, tvSize;

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_file;
    }

    @Override
    protected void inflateContentView() {
        ivMessage = findViewById(R.id.iv_image);
        tvName = findViewById(R.id.tv_name);
        tvSize = findViewById(R.id.tv_size);
    }

    @Override
    protected void bindContentView() {
        Log.e("bindContentView",message.getContent());
        Log.e("netdata", "bindContentView: ========001======");
        try {
//            JCMsgImgContentBean bean = new Gson().fromJson(message.getContent(), JCMsgImgContentBean.class);
            String decodeBase64 = new String(Base64.decode(message.getContent(),Base64.NO_WRAP));
            JNLogUtil.e("==decodeBase64="+decodeBase64);
            JCMsgFileContentBean bean = new Gson().fromJson(decodeBase64, JCMsgFileContentBean.class);
            JNLogUtil.e("JCmsgViewHolderFile","====getName=="+bean.getName());
            JNLogUtil.e("JCmsgViewHolderFile","====getSize=="+bean.getSize());
            JNLogUtil.e("JCmsgViewHolderFile","====getPath=="+bean.getPath());
            String name = bean.getName();
            if (null==bean.getName()||TextUtils.isEmpty(bean.getName())){
                try {
                    name = bean.getPath().substring(bean.getPath().lastIndexOf("/")+1);
                }catch (Exception e){
                    JNLogUtil.e("JCmsgViewHolderFile","==bindContentView==name==", e);
                }
            }
            tvName.setText(name);
            String size = "";
            try {
                size = bean.getSize();
            }catch (Exception e){
                JNLogUtil.e("==JCmsgViewHolderFile==bindContentView==size==", e);
            }

            tvSize.setText(bean.getSize());
//            tvSize.setText(FileUtils.getReadableFileSize(size));
            if (null==size||size.isEmpty()){
                tvSize.setVisibility(View.GONE);
            }else {
                tvSize.setVisibility(View.VISIBLE);
            }
            if(bean.getFileType()!=null){
                String title = bean.getFileType();
//                String title = bean.getFileType().getTitle();
                if (title.equals("IMG")) {
                    JCglideUtils.loadImage(context, bean.getUrl(), ivMessage);
                } else {
//                    ivMessage.setImageResource(bean.getFileType().getIconStyle());
                }
            }else {
//                ivMessage.setImageResource(com.wgd.wgdfilepickerlib.R.mipmap.file_picker_def);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
//        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_copy), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_transmit), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_collect), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_withdraw), 1));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_delete), 2));
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected void onItemClick() {
        try {
            String decodeBase64 = new String(Base64.decode(message.getContent(),Base64.NO_WRAP));
            JNLogUtil.e("==decodeBase64="+decodeBase64);
            JCMsgFileContentBean bean = new Gson().fromJson(decodeBase64, JCMsgFileContentBean.class);
            String path = bean.getPath();
            if (isReceivedMessage()){
                try {
                    Log.e("downloadFile", "onComplete: ==0011=" +path);
                    File file = new File(path);
                    Log.e("downloadFile", "onComplete: ==0012=" );
                    if (file.exists()){
                        Log.e("downloadFile", "onComplete: ==0013=" );
                        JCOpenFileUtil.openFile(context, file,(JCimMessageBean)message);
                        return;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                changeShow(true);
                download(bean.getUrl());
            }else {
                try {
                    Log.e("downloadFile", "onComplete: ==0014=" +path);
                    File file = new File(path);
                    Log.e("downloadFile", "onComplete: ==0015=" );
                    if (file.exists()){
                        Log.e("downloadFile", "onComplete: ==0016=" );
                        JCOpenFileUtil.openFile(context, file,(JCimMessageBean)message);
                    }else {
                        JCChatManager.showToast("文件已经不存在！");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void download(String url){
        try {
//            JNFileUtils.downloadFile((FragmentActivity) context, url, "", new OnDownloadListener() {
            JCFileUtils.downloadFile((FragmentActivity) context, url, "", new OnDownloadListener() {
                @Override
                public void onStart(File file) {
                }
                @Override
                public void onProgress(File file, int progress) {
                }
                @Override
                public void onComplete(File file) {
//                    JCChatManager.showToast("文件下载成功："+file.getPath());
                    Log.e("downloadFile", "onComplete: ===" );
                    changeShow(false);
                    String filePath = file.getPath();
//                    String filePath = file.getAbsolutePath();
                    Log.e("downloadFile", "onComplete: ==0021=" +filePath);
//                    if (filePath.contains("content:")){
//                        filePath = JCFileUtils.getFilePathByUri(context, Uri.fromFile(file));
//                    }
//                    JCUriToPathUtils.getRealPathFromUri(uri);
                    Log.e("downloadFile", "onComplete: ==0022=" +JCUriToPathUtils.getRealPathFromUri(Uri.fromFile(file)));
                    String decodeBase64 = new String(Base64.decode(message.getContent(),Base64.NO_WRAP));
//                    JNLogUtil.e("==decodeBase64="+decodeBase64);
                    JCMsgFileContentBean bean = new Gson().fromJson(decodeBase64, JCMsgFileContentBean.class);
                    bean.setPath(filePath);
                    Log.e("downloadFile", "onComplete: ==JCMsgFileContentBean==" +new Gson().toJson(bean));
                    String content = JNContentEncryptUtils.encodeContent(new Gson().toJson(bean));
                    JCimMessageBean jCimMessageBean = (JCimMessageBean) message;
                    jCimMessageBean.setContent(content);
                    JCMessageUtils.saveMessage(jCimMessageBean);
                    EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>
                            (JNEventBusType.CODE_MESSAGE_UPDATE, "",jCimMessageBean));
                    Log.e("TAG", "getTypeIntent: =init:==JCOpenFileUtil=======getName==" + file.getName());

                    JCOpenFileUtil.openFile(context, file, jCimMessageBean);
//                cancelLoadding();
                }
                @Override
                public void onError(File file, Exception e) {
                    JCChatManager.showToast("文件下载失败！");
                    changeShow(false);
//                cancelLoadding();
                }
                @Override
                public void onEnd(File file) {
                    changeShow(false);
//                cancelLoadding();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}

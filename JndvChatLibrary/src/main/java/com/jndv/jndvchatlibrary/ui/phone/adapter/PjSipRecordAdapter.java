package com.jndv.jndvchatlibrary.ui.phone.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.chat.JCSipCallRecordBean;

import java.util.List;


public class PjSipRecordAdapter extends RecyclerView.Adapter<PjSipRecordAdapter.ViewHolder> {
    private static final String TAG = "PjSipRecordAdapter";
    private Context mContext;
    private List<JCSipCallRecordBean> dBsipRecordList;
    private CallListClickListener callListClickListener;

    public PjSipRecordAdapter(Context mContext, List<JCSipCallRecordBean> dBsipRecordList, CallListClickListener callListClickListener) {
        this.mContext = mContext;
        this.dBsipRecordList = dBsipRecordList;
        this.callListClickListener = callListClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder = null;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pjsiprecord, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    public void setDatas(List<JCSipCallRecordBean> list){
        dBsipRecordList.clear();
        dBsipRecordList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.callName.setText(dBsipRecordList.get(position).getName() == null ? "未知" : dBsipRecordList.get(position).getName());
        viewHolder.callNumber.setText(dBsipRecordList.get(position).getNumber());
        viewHolder.callTime.setText(dBsipRecordList.get(position).getTime());
        if (!dBsipRecordList.get(position).isConnected()) {
            viewHolder.ivIsout.setImageResource(R.drawable.call_missed);
        } else if (dBsipRecordList.get(position).isOuting()) {
            viewHolder.ivIsout.setImageResource(R.drawable.call_outing);
        } else {
            viewHolder.ivIsout.setImageResource(R.drawable.call_incoming);
        }
        viewHolder.rlChatVoice.setOnClickListener(view -> {
            callListClickListener.onCallListVoiceItemClick(dBsipRecordList.get(position).getNumber());
        });
        viewHolder.rlChatVideo.setOnClickListener(view -> {
            callListClickListener.onCallListVideoItemClick(dBsipRecordList.get(position).getNumber());
        });
        viewHolder.rlChatMsg.setOnClickListener(view -> {
            callListClickListener.onCallListMsgItemClick(dBsipRecordList.get(position).getNumber());
        });
    }

    @Override
    public int getItemCount() {
        Log.e(TAG, "getItemCount: " + dBsipRecordList.size());
        return dBsipRecordList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivIsout;
        TextView callName;
        TextView callNumber;
        TextView callTime;
        ImageView callDetail;
        ImageView iv_chat_voice;
        ImageView iv_chat_video;
        ImageView iv_chat_msg;
        RelativeLayout rlChatVoice;
        RelativeLayout rlChatVideo;
        RelativeLayout rlChatMsg;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.ivIsout = itemView.findViewById(R.id.iv_isout);
            this.callName = itemView.findViewById(R.id.call_name);
            this.callNumber = itemView.findViewById(R.id.call_number);
            this.callTime = itemView.findViewById(R.id.call_time);
            this.callDetail = itemView.findViewById(R.id.call_detail);
            this.iv_chat_voice = itemView.findViewById(R.id.iv_chat_voice);
            this.iv_chat_video = itemView.findViewById(R.id.iv_chat_video);
            this.iv_chat_msg = itemView.findViewById(R.id.iv_chat_msg);
            rlChatMsg=itemView.findViewById(R.id.rl_chat_msg);
            rlChatVideo=itemView.findViewById(R.id.rl_chat_video);
            rlChatVoice=itemView.findViewById(R.id.rl_chat_voice);
        }
    }

    public interface CallListClickListener{
        void onCallListVoiceItemClick(String friendNum);
        void onCallListVideoItemClick(String friendNum);
        void onCallListMsgItemClick(String friendNum);

    }
}

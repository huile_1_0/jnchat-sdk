package com.jndv.jndvchatlibrary.ui.crowd.utils;

import android.graphics.Bitmap;

public class GlobalHolder {
//    private static final String TAG = "GlobalHolder--";
//    private static GlobalHolder holder; // 单例模式
//    public long mCurrentUserId; // 当前用户ID
//    private User mCurrentUser;  // 当前用户
//    private final Object mUserLock = new Object();
//
//    private List<BoUserInfoGroup> datas = new ArrayList<>();
//    private LongSparseArray<BoUserInfoGroup> mUserHolder = new LongSparseArray<>();
//    private List<String> dataBaseTableCacheName = new ArrayList<>();
//
//    public Map<String, FileDownLoadBean> globleFileProgress = new HashMap<String, FileDownLoadBean>();
//
//    private LongSparseArray<User> mUHolder = new LongSparseArray<>();
//    private Map<String, User> mUHolderKeyIsName = new HashMap<>();
//    private LongSparseArray<Group> mGroupHolder = new LongSparseArray<>();
//    private final GlobalState mState = new GlobalState();
//    private LongSparseArray<List<UserDeviceConfig>> mUserDeviceList = new LongSparseArray<>();
//    private List<Group> mContactsGroup = new ArrayList<>();
//    private List<Group> mOrgGroup = new ArrayList<Group>();
//    private List<TreePoint> voiceDevOrgList = new ArrayList<>();
//
//    private List<Group> mConfGroup = new ArrayList<Group>();
//    private LongSparseArray<Group> mCrowdVideoGroup = new LongSparseArray<>();
//    private volatile boolean p2pAVNeedStickyBroadcast = false;
//    // 站岗巡查相关
//    private boolean isUploadGPS = false; // 是否上传GPS
//    private boolean isStartPatrol = true; // 是否开始巡查
//    private boolean isVisibility = false; // 任务按钮是否显示
//    private PatrolRoute currentPatrolRoute;
//
//    private boolean isTask;
//    private boolean isFreePatrol;
//    private boolean isRequest;
//    private boolean isGetGPS;
//
//
//    public List<CrowdBean> getCrowdBeanList() {
//
//        return crowdBeanList;
//    }
//
//    public CrowdBean getCrowdBean(long crwodId) {
//        CrowdBean crowdBean = null;
//        for (int i = 0; i < crowdBeanList.size(); i++) {
//            if (crowdBeanList.get(i).getId() == crwodId) {
//                crowdBean = crowdBeanList.get(i);
//                break;
//            }
//        }
//        return crowdBean;
//    }
//
//    public List<User> getCrowdBeanUserList(long crwodId) {
//
//        for (int i = 0; i < crowdBeanList.size(); i++) {
//            if (crowdBeanList.get(i).getId() == crwodId) {
//                return crowdBeanList.get(i).getUserList();
//            }
//        }
//        return null;
//    }
//
//    public void updateCrowdUser(long userId) {
//        List<User> temp;
//        for (int i = 0; i < crowdBeanList.size(); i++) {
//            temp = crowdBeanList.get(i).getUserList();
//            for (int j = 0; j < temp.size(); j++) {
//                if (userId == temp.get(j).getmUserId()) {
//                    temp.get(j).setNickName(getUser(userId).getNickName());
//                }
//            }
//        }
//    }
//
//
//    /**
//     * 查看是否是群主
//     *
//     * @return true  是
//     */
//    public boolean checkIsCrowdHost(long crowdId, long userId) {
//        if (getCrowdBean(crowdId) == null) {
//            return false;
//        }
//        return getCrowdBean(crowdId).getCreatoruserid() == userId;
//    }
//
//    public boolean removeCrowdBean(long gid) {
//
//        for (int i = 0; i < crowdBeanList.size(); i++) {
//            CrowdBean g = crowdBeanList.get(i);
//            if (g.getId() == gid) {
//                crowdBeanList.remove(g);
//                return true;
//            }
//        }
//
//        return false;
//    }
//
//    public void addCrowdBeanList(CrowdBean bean) {
//        for (int i = 0; i < crowdBeanList.size(); i++) {
//            if (crowdBeanList.get(i).getId() == bean.getId()) {
//                return;
//            }
//        }
//        crowdBeanList.add(bean);
//    }
//
//    public void addCrowdVideoGroup(long key, Group g) {
//        mCrowdVideoGroup.put(key, g);
//    }
//
//
//
//  /*  public  List<User> getCrowdVideoList(int groupType, long gId){
//        Group group = getGroupById(groupType, gId);
//        return group.getUsers();
//    }
//*/
//
//    public void setCurrentUserId(long currentUserId) {
//        this.mCurrentUserId = currentUserId;
//    }
//
//    private List<CrowdBean> crowdBeanList = new ArrayList<>();
//    private List<Group> mCrowdGroup = new ArrayList<Group>();
//
//    public List<User> getFriends() {
//        return friends;
//    }
//
//    public void addFriend(User friend) {
//       /* for (User u : friends) {
//            if (u.getmUserId() == friend.getmUserId()) {
//                return;
//            }
//        }*/
//
//        if (!friends.contains(friend)) {
//            friends.add(friend);
//        }
//
//    }
//
//    public void removeFriend(long id) {
//        for (int i = 0; i < friends.size(); i++) {
//            if (friends.get(i).getmUserId() == id) {
//                friends.remove(i);
//            }
//        }
//    }
//
//    private List<User> friends = new ArrayList<>();
//    // JNIServer 发来的数据
//    List<PviewGroup> initialGroupData;
//
//    private GlobalHolder() {
//    }
//
//    public static GlobalHolder getInstance() {
//        if (holder == null) {
//            synchronized (GlobalHolder.class) {
//                if (holder == null) {
//                    holder = new GlobalHolder();
//                }
//            }
//        }
//        return holder;
//    }
//
//    public Group getGroupById(long gId) {
//        return mGroupHolder.get(gId);
//    }
//
//    public Group getGroupById(int groupType, long gId) {
//
//        if (groupType == GroupType.GROUP_TYPE_CROWD_VIDEO) {
//            return mCrowdVideoGroup.get(gId);
//        }
//        return mGroupHolder.get(gId);
//    }
//
//    public List<Group> getmContactsGroup() {
//        return mContactsGroup;
//    }
//
//    public void setmContactsGroup(List<Group> mContactsGroup) {
//        this.mContactsGroup = mContactsGroup;
//    }
//
//    public User getExistUser(long userID) {
//        Long key = Long.valueOf(userID);
//        synchronized (key) {
//            User tmp = mUHolder.get(key);
//            if (tmp == null) {
//                return null;
//            } else {
//                return tmp;
//            }
//        }
//    }
//
//    public User getCurrentUser() {
//
//        return mCurrentUser == null ? new User(getCurrentUserId()) : mCurrentUser;
//    }
//
//    public void setCurrentUser(User currentUser) {
//        this.mCurrentUser = currentUser;
//    }
//
//    public LongSparseArray<BoUserInfoGroup> getmUserHolder() {
//        return mUserHolder;
//    }
//
//    public long getCurrentUserId() {
//        if (mCurrentUser == null) {
//            return SpUtil.getLong(JndvApplication.getInstance().getBaseContext(), ChatEntity.CURRENT_USER_ID, 0);
//        } else {
//            return mCurrentUser.getmUserId();
//        }
//    }
//
//    public List<BoUserInfoGroup> getDatas() {
//        //SpUtil.setString(JndvApplication.getInstance().getBaseContext(),ChatEntity.USER_LIST,new Gson().toJson(datas));
//        if (datas == null) {
//            datas = new ArrayList<>();
//        }
//
//        return datas;
//    }
//
//    public void addData(BoUserInfoGroup userInfoGroup) {
//        if (datas == null) {
//            datas = new ArrayList<>();
//        }
//        PviewImRequest.invokeNative(PviewImRequest.NATIVE_GET_USER_INFO, userInfoGroup.mId);
//        for (int i = 0; i < datas.size(); i++) {
//            if (datas.get(i).mId == userInfoGroup.mId) {
//                return;
//            }
//        }
//        datas.add(userInfoGroup);
//
//    }
//
//    public void setDatas(List<BoUserInfoGroup> datas) {
//        this.datas = datas;
//    }
//
//    public void dataTypeSwitch() {
//        getDatas();
//        if (mUserHolder == null) {
//            mUserHolder = new LongSparseArray<>();
//        }
//        for (int i = 0; i < datas.size(); i++) {
//            BoUserInfoGroup userInfo = datas.get(i);
//            mUserHolder.put(userInfo.mId, userInfo);
//        }
//
//        LogUtil.e("转换后的数据：" + mUserHolder.size());
//    }
//
//    public void updateGroupList(int gType, List<PviewGroup> list) {
//        for (PviewGroup vg : list) {
//            Group cache = mGroupHolder.get(vg.id);
//            if (cache != null) {
//                continue;
//            }
//
//            if (vg.getName() == null)
//                LogUtil.e("parse the group name is wroing...the group is :" + vg.id);
//
//            Group g = null;
//            if (gType == GroupType.GROUP_TYPE_CROWD) {
//                boolean flag = true;
//                for (Group group : mCrowdGroup) {
//                    if (group.getmGId() == vg.id) {
//                        flag = false;
//                    }
//                }
//
//                if (flag) {
//                    User owner = GlobalHolder.getInstance().getUser(vg.owner.mId);
//                    g = new CrowdGroup(vg.id, vg.getName(), owner);
//
//                    ((CrowdGroup) g).setBrief(vg.getBrief());
//                    ((CrowdGroup) g).setAnnouncement(vg.getAnnounce());
//                    ((CrowdGroup) g).setAuthType(CrowdGroup.AuthType.fromInt(vg.authType));
//                    ((CrowdGroup) g).setCreateDate(new Date(GlobalConfig.getGlobalServerTime()));
//                    mCrowdGroup.add(g);
//                }
//            } else if (gType == GroupType.GROUP_TYPE_CONFERENCE) {
//                User owner = GlobalHolder.getInstance().getUser(vg.owner.mId);
//                User chairMan = GlobalHolder.getInstance().getUser(vg.chairMan.mId);
//                g = new ConferenceGroup(vg.id, vg.getName(), owner, vg.createTime, chairMan);
//                mConfGroup.add(g);
//            } else if (gType == GroupType.GROUP_TYPE_DEPARTMENT) {
//
//                g = new OrgGroup(vg.id, vg.getName());
////                saveDBOrganization(0 + "", g.getmGId() + "", g.getName());
//                if (mOrgGroup != null) {
//                    mOrgGroup.add(g);
//                }
//            } else if (gType == GroupType.GROUP_TYPE_CONTACT) {
//                g = new ContactGroup(vg.id, vg.getName());
//                if (vg.isDefault) {
//                    ((ContactGroup) g).setDefault(true);
//                    g.setName(GlobalConfig.Resource.CONTACT_DEFAULT_GROUP_NAME);
//                }
//                LogUtil.e("mContactsGroup", g.getName());
//                mContactsGroup.add(g); // FIXME: 2019/4/10 bugly曾在登录时报空
//            } else if (gType == GroupType.GROUP_TYPE_DISCUSSION) {
//                User owner = GlobalHolder.getInstance().getUser(vg.owner.mId);
//                g = new DiscussionGroup(vg.id, vg.getName(), owner, new Date(GlobalConfig.getGlobalServerTime()));
////                mDiscussionBoardGroup.add(g);
//            } else {
//                throw new RuntimeException(" Can not support this type");
//            }
//
//            mGroupHolder.put(g.getmGId(), g);
//
//            populateGroup(gType, g, vg.childs);
//        }
//    }
//
//    //增添
//    private void populateGroup(int groupType, Group parent, Set<PviewGroup> list) {
//        for (PviewGroup vg : list) {
//            Group cache = mGroupHolder.get(Long.valueOf(vg.id));
//
//            Group g = null;
//            if (cache != null) {
//                g = cache;
//                // Update new name
//                cache.setName(vg.getName());
//            } else {
//                /*if (groupType == ChatEntity.GROUP_TYPE_CONTACT) {
//                    g = new ContactGroup(vg.id, vg.getName());
//                } else {
//                    throw new RuntimeException(" Can not support this type");
//                }*/
//
//                if (groupType == GroupType.GROUP_TYPE_CROWD) {
//                    User owner = GlobalHolder.getInstance().getUser(vg.owner.mId);
//                    g = new CrowdGroup(vg.id, vg.getName(), owner);
//                } else if (groupType == GroupType.GROUP_TYPE_CONFERENCE) {
//                    User owner = GlobalHolder.getInstance().getUser(vg.owner.mId);
//                    User chairMan = GlobalHolder.getInstance().getUser(vg.chairMan.mId);
//                    g = new ConferenceGroup(vg.id, vg.getName(), owner, vg.createTime, chairMan);
//                } else if (groupType == GroupType.GROUP_TYPE_DEPARTMENT) {
//                    g = new OrgGroup(vg.id, vg.getName());
////                    Log.e("20210508测试", "populateGroup: pid " + parent.getmGId() + " " + g.toString());
////                    saveDBOrganization(parent.getmGId() + "", g.getmGId() + "", g.getName());
//                } else if (groupType == GroupType.GROUP_TYPE_CONTACT) {
//                    g = new ContactGroup(vg.id, vg.getName());
//                } else {
//                    throw new RuntimeException(" Can not support this type");
//                }
//            }
//
//            parent.addGroupToGroup(g);
//            mGroupHolder.put(g.getmGId(), g);
//
//            populateGroup(groupType, g, vg.childs);
//        }
//    }
//
//    //本地保存组织机构
//    private void saveDBOrganization(String pid, String mid, String name) {
//        if (!name.equals("山东省")) {
//            DBOrganization dbOrganization = new DBOrganization();
//            dbOrganization.setName(name);
//            dbOrganization.setMid(mid);
//            dbOrganization.setPid(pid);
//            List<DBOrganization> list = DBOrganization.find(DBOrganization.class, "name = ? and mid = ?", name, mid);
////        Log.e("20210508测试", "saveDBOrganization: 查询结果："+list.size() );
//            if (list.size() == 0) {
//                long a = dbOrganization.save();
////            Log.e("20210508测试", "saveDBOrganization: " + a);
//            } else {
//                for (DBOrganization dBOrg : list) {
//                    dBOrg.setPid(pid);
//                    dBOrg.setMid(mid);
//                    dBOrg.setName(name);
//                    long b = dBOrg.save();
////                Log.e("20210508测试", "saveDBOrganization: 更新  "+b );
//                }
//            }
//        }
//    }
//
//    public List<Group> getGroup(int groupType) {
//       /* switch (groupType) {
//
//            case ChatEntity.GROUP_TYPE_CONTACT:
//                // SpUtil.setString(JndvApplication.getInstance().getBaseContext(),ChatEntity.GROUP_LIST,new Gson().toJson(mContactsGroup));
//                return mContactsGroup;
//            case PviewGlobalConstants.GROUP_TYPE_CROWD:
//                List<Group> ct = new CopyOnWriteArrayList<Group>();
//                ct.addAll(this.mCrowdGroup);
//                return ct;
//            default:
//                throw new RuntimeException("Unkonw type");
//        }*/
//
//        switch (groupType) {
//            case GroupType.GROUP_TYPE_DEPARTMENT:
//                return this.mOrgGroup;
//            case GroupType.GROUP_TYPE_CONTACT:
//                return mContactsGroup;
//            case GroupType.GROUP_TYPE_CROWD:
//                List<Group> ct = new CopyOnWriteArrayList<Group>();
//                ct.addAll(this.mCrowdGroup);
//                return ct;
//            case GroupType.GROUP_TYPE_CONFERENCE:
//                /*List<Group> confL = new ArrayList<Group>();
//                confL.addAll(this.mConfGroup);
//                Collections.sort(confL);
//                List<Group> sortConfL = new CopyOnWriteArrayList<Group>(confL);
//                return sortConfL;*/
//                return mConfGroup;
//            case GroupType.GROUP_TYPE_DISCUSSION:
//                //return mDiscussionBoardGroup;
//                break;
//
//            default:
//                throw new RuntimeException("Unkonw type");
//        }
//
//        return null;
//
//    }
//
////
////    public BoUserInfoGroup getUser(long userID) {
////
////        return mUserHolder.get(userID);
////
////    }
//
//    public BoUserInfoGroup getPUser(long userID) {
//
//        return mUserHolder.get(userID);
//
//    }
//
//    public boolean isFriend(long nUserId) {
//
//        List<User> friends;
//        if (GlobalHolder.getInstance().getFriends() != null && GlobalHolder.getInstance().getFriends().size() > 0) {
//            friends = GlobalHolder.getInstance().getFriends();
//        } else {
//            friends = new Gson().fromJson(SpUtil.getString(JndvApplication.getInstance().getBaseContext(), ChatEntity.FRIEND_LIST), new TypeToken<List<User>>() {
//            }.getType());
//        }
//        if (friends != null) {
//            for (int i = 0; i < friends.size(); i++) {
//                if (nUserId == friends.get(i).getmUserId()) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//
//    /**
//     * 判断是否为好友
//     */
//    public boolean isFriend(User user) { // FIXME: 2019/1/12 感觉多余，因为数据都是服务器获取来的，再便利是否是好友，感觉是无用功
//        LogUtil.e("PGlobalHolder isFriend() 用户数据数量：" + mUserHolder.size());
//        if (user == null) {
//            LogUtil.e("PGlobalHolder isFriend() ---> 获取用户为null , 请检查会话用户是否存在！");
//            return false;
//        }
//        long userID = user.getmUserId();
//        for (int i = 0; i < mUserHolder.size(); i++) {
//            if (userID == mUserHolder.keyAt(i)) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean isP2pAVNeedStickyBroadcast() {
//        return p2pAVNeedStickyBroadcast;
//    }
//
//    public void setP2pAVNeedStickyBroadcast(boolean p2pAVNeedStickyBroadcast) {
//        this.p2pAVNeedStickyBroadcast = p2pAVNeedStickyBroadcast;
//    }
//
//    public GlobalState getGlobalState() {
//        return new GlobalState(mState);
//    }
//
//    public User putOrUpdateUser(BoUserInfoShort boUserInfoShort) {
////        Log.e(TAG, "putOrUpdateUser: 1");
//        if (boUserInfoShort == null || boUserInfoShort.mId <= 0) {
//            return null;
//        }
//
//        User user;
//        synchronized (mUserLock) {
//            boolean isContained = true;
//            user = mUHolder.get(boUserInfoShort.mId);
//            if (user == null) {
//                isContained = false;
//                user = new User(boUserInfoShort.mId);
//            }
//
//            user.setFromService(true);
//
//            if (boUserInfoShort.mAccount != null) {
//                user.setAccount(boUserInfoShort.mAccount);
//            }
//            if (boUserInfoShort.mNickName != null) {
//                user.setNickName(boUserInfoShort.mNickName);
//            }
//            if (boUserInfoShort.mCommentName != null) {
//                user.setCommentName(boUserInfoShort.mCommentName);
//            }
//
//            if (boUserInfoShort.mUeType != null) {
//                try {
//                    int deviceType = Integer.valueOf(boUserInfoShort.mUeType);
//                    user.setDeviceType(User.DeviceType.fromInt(deviceType));
//                } catch (NumberFormatException e) {
//                    LogUtil.e("CLASS = GlobalHolder MOTHERD = putOrUpdateUser(BoUserInfoShort boUserInfoShort) mUeType 转整数失败 ");
//                }
//            }
//
//            if (boUserInfoShort.mAccountType != null) {
//                try {
//                    int accountType = Integer.valueOf(boUserInfoShort.mAccountType);
//
//                    if (accountType == GlobalConstant.ACCOUNT_TYPE_NON_REGISTERED) {
//                        user.setRapidInitiation(true);
//                    } else {
//                        user.setRapidInitiation(false);
//                    }
//                } catch (NumberFormatException e) {
//                    LogUtil.e("CLASS = GlobalHolder MOTHERD = putOrUpdateUser(BoUserInfoShort boUserInfoShort) mAccountType 转整数失败 ");
//                }
//            }
//
//            if (!isContained) {
//                mUHolder.put(user.getmUserId(), user);
//                mUHolderKeyIsName.put(user.getAccount(), user);
//            }
//        }
//        return user;
//    }
//
//    private int jsq = 0;
//
//    public User putOrUpdateUser(BoUserInfoBase boUserBaseInfo) {
////        Log.e(TAG, "putOrUpdateUser: 2");
//        jsq++;
//        if (boUserBaseInfo == null || boUserBaseInfo.mId <= 0) {
//            return null;
//        }
//
//        User user = null;
//        synchronized (mUserLock) {
//            boolean isContained = true;
//            user = mUHolder.get(Long.valueOf(boUserBaseInfo.mId));//在getUser方法里面mUHolder.put
//            if (user == null) {
//                isContained = false;
//                user = new User(boUserBaseInfo.mId);
//            }
//
//            user.setFromService(true);
//
//            if (boUserBaseInfo.mAccount != null) {
//                user.setAccount(boUserBaseInfo.mAccount);
//            }
//
//            if (boUserBaseInfo.mNickName != null) {
//                if (jsq <= 2)
////                    Log.e(TAG, "putOrUpdateUser2--: " + boUserBaseInfo.mNickName);
//                    user.setNickName(boUserBaseInfo.mNickName);
//            }
//            if (boUserBaseInfo.mCommentName != null) {
//                user.setCommentName(boUserBaseInfo.mCommentName);
//            }
//            if (boUserBaseInfo.mSign != null) {
//                user.setSignature(boUserBaseInfo.mSign);
//            }
//
//            if (boUserBaseInfo.mAuthtype != null) {
//                try {
//                    int authtype = Integer.valueOf(boUserBaseInfo.mAuthtype);
//                    user.setAuthtype(authtype);
//
//                } catch (NumberFormatException e) {
//                    LogUtil.e("CLASS = GlobalHolder MOTHERD = putOrUpdateUser(BoUserInfoGroup boGroupUserInfo) mAuthtype 转整数失败 ");
//                }
//            }
//
//            if (boUserBaseInfo.mSex != null) {
//                user.setSex(boUserBaseInfo.mSex);
//            }
//            if (boUserBaseInfo.mStringBirthday != null) {
//                user.setmStringBirthday(boUserBaseInfo.mStringBirthday);
//            }
//            if (boUserBaseInfo.mMobile != null) {
//                user.setMobile(boUserBaseInfo.mMobile);
//            }
//            if (boUserBaseInfo.mTelephone != null) {
//                user.setTelephone(boUserBaseInfo.mTelephone);
//            }
//            if (boUserBaseInfo.mEmail != null) {
//                user.setEmail(boUserBaseInfo.mEmail);
//            }
//            if (boUserBaseInfo.mFax != null) {
//                user.setFax(boUserBaseInfo.mFax);
//            }
//            if (boUserBaseInfo.mJob != null) {
//                user.setJob(boUserBaseInfo.mJob);
//            }
//
//            if (boUserBaseInfo.mAddress != null) {
//                user.setAddress(boUserBaseInfo.mAddress);
//            }
//
//            if (boUserBaseInfo.mBirthday != null) {
//                user.setBirthday(boUserBaseInfo.mBirthday);
//            }
//            if (boUserBaseInfo.avatarlocation != null) {
//                user.setmAvatarLocation(boUserBaseInfo.avatarlocation);
//            }
//            if (!isContained) {
//                mUHolder.put(user.getmUserId(), user);
//                mUHolderKeyIsName.put(user.getAccount(), user);
//            }
//
//        }
//
//        return user;
//    }
//
//    public User putOrUpdateUser(BoUserInfoGroup boGroupUserInfo) {
////        Log.e(TAG, "putOrUpdateUser: 3");
//        if (boGroupUserInfo == null || boGroupUserInfo.mId <= 0) {
//            return null;
//        }
//
//        User user = null;
//        synchronized (mUserLock) {
//            boolean isContained = true;
//            user = mUHolder.get(Long.valueOf(boGroupUserInfo.mId));
//            if (user == null) {
//                isContained = false;
//                user = new User(boGroupUserInfo.mId);
//                user.isContain = false;
//            } else {
//                user.isContain = true;
//            }
//
//            user.setFromService(true);
//
//            if (boGroupUserInfo.mAccount != null) {
//                user.setAccount(boGroupUserInfo.mAccount);
//            }
//            if (boGroupUserInfo.mAccountType != null) {
//                int mAccountType = Integer.valueOf(boGroupUserInfo.mAccountType);
//                user.setAccountType(mAccountType);
//                if (mAccountType == PviewGlobalConstants.ACCOUNT_TYPE_PHONE_FRIEND) {
//                    user.updateStatus(User.Status.ONLINE);
//                }
//            }
//            if (boGroupUserInfo.mAvatarLocation != null) {
//                user.setmAvatarLocation(boGroupUserInfo.mAvatarLocation);
//            }
//            if (boGroupUserInfo.mNickName != null) {
////                if (boGroupUserInfo.mNickName.equals("区总网格长"))
////                    Log.e(TAG, "putOrUpdateUser3--: " + boGroupUserInfo.mNickName);
//                user.setNickName(boGroupUserInfo.mNickName);
//            }
//            if (boGroupUserInfo.mCommentName != null) {
//                user.setCommentName(boGroupUserInfo.mCommentName);
//            }
//            if (boGroupUserInfo.mSign != null) {
//                user.setSignature(boGroupUserInfo.mSign);
//            }
//
//            if (boGroupUserInfo.mAuthtype != null) {
//                try {
//                    int authtype = Integer.valueOf(boGroupUserInfo.mAuthtype);
//                    user.setAuthtype(authtype);
//                } catch (NumberFormatException e) {
//                    LogUtil.e("CLASS = GlobalHolder MOTHERD = putOrUpdateUser(BoUserInfoGroup boGroupUserInfo) mAuthtype 转整数失败 ");
//                }
//            }
//
//            if (boGroupUserInfo.mSex != null) {
//                user.setSex(boGroupUserInfo.mSex);
//            }
//            if (boGroupUserInfo.mStringBirthday != null) {
//                user.setmStringBirthday(boGroupUserInfo.mStringBirthday);
//            }
//            if (boGroupUserInfo.mMobile != null) {
//                user.setMobile(boGroupUserInfo.mMobile);
//            }
//            if (boGroupUserInfo.mTelephone != null) {
//                user.setTelephone(boGroupUserInfo.mTelephone);
//            }
//            if (boGroupUserInfo.mEmail != null) {
//                user.setEmail(boGroupUserInfo.mEmail);
//            }
//            if (boGroupUserInfo.mFax != null) {
//                user.setFax(boGroupUserInfo.mFax);
//            }
//            if (boGroupUserInfo.mJob != null) {
//                user.setJob(boGroupUserInfo.mJob);
//            }
//
//            if (boGroupUserInfo.mAddress != null) {
//                user.setAddress(boGroupUserInfo.mAddress);
//            }
//
//            if (boGroupUserInfo.mBirthday != null) {
//                user.setBirthday(boGroupUserInfo.mBirthday);
//            }
//
//            if (boGroupUserInfo.imnumber != null) {
//                user.setImnumber(boGroupUserInfo.imnumber);
//            }
//
//            if (boGroupUserInfo.officenumber != null) {
//                user.setOfficenumber(boGroupUserInfo.officenumber);
//            }
//
//            if (boGroupUserInfo.djjnumber != null) {
//                user.setOfficenumber(boGroupUserInfo.djjnumber);
//            }
//
//            if (!isContained) {
//                mUHolder.put(user.getmUserId(), user);
//                mUHolderKeyIsName.put(user.getAccount(), user);
//            }
//        }
//        return user;
//    }
//
//    /**
//     * Add user collections to group collections
//     *
//     * @param gList
//     * @param uList
//     * @param belongGID
//     */
//    public void addUserToGroup(List<Group> gList, List<User> uList, long belongGID) {
//        for (Group g : gList) {
//            if (belongGID == g.getmGId()) {
//                g.addUserToGroup(uList);
//                return;
//            }
//            addUserToGroup(g.getChildGroup(), uList, belongGID);
//        }
//    }
//
//    public void removeGroupUser(long gid, long uid) {
//        Group g = this.findGroupById(gid);
//        if (g != null) {
//            g.removeUserFromGroup(uid);
//        } else {
//            LogUtil.e("GlobalHolder removeGroupUser",
//                    " Remove user failed ! get group is null " + " group id is : " + gid + " user id is : " + uid);
//        }
//    }
//
//    /**
//     * Add user collections to group collections
//     *
//     * @param uList
//     * @param belongGID
//     */
//    public void addUserToGroup(List<User> uList, long belongGID) {
//        Group g = findGroupById(belongGID);
//        if (g == null) {
//            LogUtil.e("Doesn't receive group<" + belongGID + "> information yet!");
//            return;
//        }
//        g.addUserToGroup(uList);
//    }
//
//    public void addUserToGroup(User u, long belongGID) {
//        Group g = findGroupById(belongGID);
//        if (g == null) {
//            LogUtil.e("Doesn't receive group<" + belongGID + "> information yet!");
//            return;
//        }
//        g.addUserToGroup(u);
//    }
//
//    public Group findGroupById(long gid) {
//        return mGroupHolder.get(Long.valueOf(gid));
//    }
//
//    public void setMessageShowTime(Context mContext, int groupType, long groupID, long remoteUserID, VMessage msg) {
//        VMessage lastMsg = ChatMessageProvider.getNewestShowTimeMessage(groupType, groupID, remoteUserID);
//        if (lastMsg == null) {
//            msg.setShowTime(true);
//        } else {
//            long lastDateLong = lastMsg.getmDateLong();
//            if (msg.getmDateLong() - lastDateLong > 60000) {
//                msg.setShowTime(true);
//            } else {
//                msg.setShowTime(false);
//            }
//        }
//    }
//
//    public User getUser(long userID) {
//        if (userID <= 0) {
//            return null;
//        }
//        Long key = Long.valueOf(userID);
//        synchronized (key) {
//            User tmp = mUHolder.get(key);
//            if (tmp == null) {
//                tmp = new User(userID);
//                mUHolder.put(key, tmp);
//                if (GlobalHolder.getInstance().getGlobalState().isGroupLoaded()) {
//                    // if receive this callback , the dirty change false;
//                    LogUtil.i("user is null , invoke!");
//                    PviewImRequest.invokeNative(PviewImRequest.NATIVE_GET_USER_INFO, userID);
//                }
//            }
//            return tmp;
//        }
//    }
//
//    public User getUserByAccount(String account) {
//        if (TextUtils.isEmpty(account)) {
//            return null;
//        }
//
//        User tmp = mUHolderKeyIsName.get(account);
//        if (tmp == null) {
//            Log.e("getUserByAccount: ", "tmp is null");
//        }
//        return tmp;
//    }
//
//    public void setChatState(boolean flag, long remoteID) {
//        synchronized (mState) {
//            int st = this.mState.getState();
//            if (flag) {
//                st |= GlobalState.STATE_CHAT_INTERFACE_OPEN;
//            } else {
//                st &= (~GlobalState.STATE_CHAT_INTERFACE_OPEN);
//            }
//            this.mState.setState(st);
//            this.mState.setRemoteChatUid(remoteID);
//        }
//    }
//
//    public void updateUserDevice(long id, List<UserDeviceConfig> udcList) {
//        Long key = Long.valueOf(id);
//        List<UserDeviceConfig> list = mUserDeviceList.get(key);
//        if (list != null) {
//            list.clear();
//        } else {
//            list = new ArrayList<>();
//            mUserDeviceList.put(key, list);
//        }
//
//        list.addAll(udcList);
//    }
//
//    public UserDeviceConfig getUserDefaultDevice(long uid) {
//        List<UserDeviceConfig> list = mUserDeviceList.get(Long.valueOf(uid));
//        if (list == null) {
//            return null;
//        }
//        for (UserDeviceConfig udc : list) {
//            if (udc.isDefault()) {
//                return udc;
//            }
//        }
//
//        if (list.size() > 0) {
//            LogUtil.e("Not found default device, use first device !");
//            return list.iterator().next();
//        }
//        return null;
//    }
//
//    public boolean isOfflineLoaded() {
//        synchronized (mState) {
//            return mState.isOfflineLoaded();
//        }
//    }
//
//    public void setServerConnection(boolean connected) {
//        synchronized (mState) {
//            int st = this.mState.getState();
//            if (connected) {
//                st |= GlobalState.STATE_SERVER_CONNECTED;
//            } else {
//                st &= (~GlobalState.STATE_SERVER_CONNECTED);
//            }
//            this.mState.setState(st);
//        }
//    }
//
//    public boolean isChatInterfaceOpen(long remoteID) {
//        synchronized (mState) {
//            if (mState.isChatInterfaceOpen()) {
//                if (this.mState.getRemoteChatUid() == remoteID)
//                    return true;
//                else
//                    return false;
//            } else {
//                return false;
//            }
//        }
//    }
//
//    public boolean isVoiceConnected() {
//        synchronized (mState) {
//            return this.mState.isVoiceConnected();
//        }
//    }
//
//    public boolean isInAudioCall() {
//        synchronized (mState) {
//            return this.mState.isInAudioCall();
//        }
//    }
//
//    public boolean isInVideoCall() {
//        synchronized (mState) {
//            return mState.isInVideoCall();
//        }
//    }
//
//    public boolean isInMeeting() {
//        synchronized (mState) {
//            return mState.isInMeeting();
//        }
//    }
//
//    public boolean isServerConnected() {
//        synchronized (mState) {
//            return mState.isConnectedServer();
//        }
//    }
//
//    public void setAudioState(boolean flag, long uid) {
//        synchronized (mState) {
//            int state = mState.getState();
//            if (flag) {
//                state |= GlobalState.STATE_IN_AUDIO_CONVERSATION;
//            } else {
//                state &= (~GlobalState.STATE_IN_AUDIO_CONVERSATION);
//            }
//            mState.setState(state);
//            mState.setUid(uid);
//            setVoiceConnectedState(flag);
//        }
//    }
//
//    /**
//     * 设置视频状态
//     */
//    public void setVideoState(boolean flag, long uid) {
//        synchronized (mState) {
//            int state = mState.getState();
//            if (flag) {
//                state |= GlobalState.STATE_IN_VIDEO_CONVERSATION;
//            } else {
//                state &= (~GlobalState.STATE_IN_VIDEO_CONVERSATION);
//            }
//            mState.setState(state);
//            mState.setUid(uid);
//        }
//    }
//
//    /**
//     * 设置会议状态
//     */
//    public void setMeetingState(boolean flag, long gid) {
//        synchronized (mState) {
//            int st = this.mState.getState();
//            if (flag) {
//                st |= GlobalState.STATE_IN_MEETING_CONVERSATION;
//            } else {
//                st &= (~GlobalState.STATE_IN_MEETING_CONVERSATION);
//            }
//            this.mState.setState(st);
//            this.mState.setGid(gid);
//        }
//    }
//
//    /**
//     * 设置语音连接状态
//     */
//    public void setVoiceConnectedState(boolean flag) {
//        synchronized (mState) {
//            int state = mState.getState();
//            if (flag) {
//                state |= GlobalState.STATE_IN_VOICE_CONNECTED;
//            } else {
//                state &= (~GlobalState.STATE_IN_VOICE_CONNECTED);
//            }
//            mState.setState(state);
//        }
//    }
//
//    public void setOfflineLoaded(boolean isLoad) {
//        synchronized (mState) {
//            int st = this.mState.getState();
//            if (isLoad) {
//                st |= GlobalState.STATE_SERVER_OFFLINE_MESSAGE_LOADED;
//            } else {
//                st &= (~GlobalState.STATE_SERVER_OFFLINE_MESSAGE_LOADED);
//            }
//            this.mState.setState(st);
//        }
//    }
//
//    public void setGroupLoaded() {
//        synchronized (mState) {
//            synchronized (mState) {
//                int st = mState.getState();
//                mState.setState(st | GlobalState.STATE_SERVER_GROUPS_LOADED);
//            }
//        }
//    }
//
//    public void addGroupToList(int groupType, Group g) {
//        if (groupType == GroupType.GROUP_TYPE_DEPARTMENT) {
//
//        } else if (groupType == GroupType.GROUP_TYPE_CONFERENCE) {
//            this.mConfGroup.add(g);
//        } else if (groupType == GroupType.GROUP_TYPE_CROWD) {
//            this.mCrowdGroup.add(g);
//        } else if (groupType == GroupType.GROUP_TYPE_CONTACT) {
//            this.mContactsGroup.add(g);
//        } else if (groupType == GroupType.GROUP_TYPE_DISCUSSION) {
//            this.mDiscussionBoardGroup.add(g);
//        }
//        mGroupHolder.put(g.getmGId(), g);
//    }
//
//    public void addVoiceDevToList(List<VoiceDevice> voiceDevices) {
//        for (VoiceDevice voiceDevice : voiceDevices) {
//            voiceDevOrgList.add(new TreePoint(voiceDevice.getMid(), voiceDevice.getLinenumber(), "12" + voiceDevice.getGroupid(), "1", 1, "dev"));
//            Log.e(TAG, "addOrgToList: 音频设备列表--" + voiceDevice.getMid() + " " + voiceDevice.getLinenumber() + " " + "12" + voiceDevice.getGroupid());
//        }
//    }
//
//    public void addOrgToList(List<XmlOrgBean> orgBeans) {
//        for (XmlOrgBean orgBean : orgBeans) {
//            if (!orgBean.getName().equals("山东省"))
//                voiceDevOrgList.add(new TreePoint(orgBean.getId(), orgBean.getName(), orgBean.getPid(), "0", 1, "org"));
//            Log.e(TAG, "addOrgToList: 组织结构--" + orgBean.getId() + " " + orgBean.getName() + " " + orgBean.getPid());
//        }
//    }
//
//    //音频设备列表组织机构
//    public List<TreePoint> getOrgVoiceDevs() {
//        return voiceDevOrgList;
//    }
//
//    public void clearAll() {
//        mUserHolder.clear();
////        mUserHolder = null;
//        mGroupHolder.clear();
//        //mGroupHolder = null;
//
//        mOrgGroup.clear();
//        // mOrgGroup = null;
//
//        mContactsGroup.clear();
//        // mContactsGroup = null;
//        mCrowdGroup.clear();
//        // mCrowdGroup = null;
//
//        friends.clear();
//        datas.clear();
//        mUserDeviceList.clear();
//        // mUserDeviceList = null;
//
//        dataBaseTableCacheName.clear();
//        // dataBaseTableCacheName = null;
//        globleFileProgress.clear();
//        globleFileProgress = null;
//        holder = null;
//    }
//
//    /**
//     * 站岗巡查界面设置是否上传GPS
//     *
//     * @param bool
//     * @return isUploadGPS
//     */
//    public boolean setIsUploadingGPS(boolean bool) {
//        isUploadGPS = bool;
//        return isUploadGPS;
//    }
//
//    /**
//     * 站岗巡查界面获取是否上传GPS
//     *
//     * @return isUploadGPS
//     */
//    public boolean getIsUploadingGPS() {
//        return isUploadGPS;
//    }
//
//    /**
//     * 站岗巡查界面设置是否开始巡查
//     *
//     * @param boo
//     * @return isStartPatrol
//     */
//    public boolean setIsStartPatrol(boolean boo) {
//        isStartPatrol = boo;
//        return isStartPatrol;
//    }
//
//    /**
//     * 站岗巡查界面获取是否开始巡查
//     *
//     * @return isStartPatrol
//     */
//    public boolean getIsStartPatrol() {
//        return isStartPatrol;
//    }
//
//    /**
//     * 站岗巡查界面设置“任务”按钮是否显示
//     *
//     * @param boo
//     * @return isVisibility
//     */
//    public boolean setTaskButtonVisibility(boolean boo) {
//        isVisibility = boo;
//        return isVisibility;
//
//    }
//
//    /**
//     * 站岗巡查界面“任务”按钮是否显示
//     *
//     * @return isVisibility
//     */
//    public boolean getTaskButtonVisibility() {
//        return isVisibility;
//    }
//
//    /**
//     * 设置当前的巡查路线
//     *
//     * @param patrolRoute
//     */
//    public void setCurrentRoute(PatrolRoute patrolRoute) {
//        currentPatrolRoute = patrolRoute;
//    }
//
//    /**
//     * 得到当前的巡查路线
//     *
//     * @return currentPatrolRoute
//     */
//    public PatrolRoute getCurrentPatrolRoute() {
//        return currentPatrolRoute;
//    }
//
//    /**
//     * 设置是否是自由巡查
//     */
//    public void setFreePatrol(boolean bb) {
//        isFreePatrol = bb;
//    }
//
//    /**
//     * 得到是否是自由巡查
//     */
//    public boolean getFreePatrol() {
//        return isFreePatrol;
//
//    }
//
//    /**
//     * 设置是否是在请求ASAgent
//     *
//     * @param bo
//     */
//    public void setIsRequest(boolean bo) {
//        isRequest = bo;
//    }
//
//    /**
//     * 得到是否是在请求ASAgent
//     *
//     * @return
//     */
//    public boolean getIsRequest() {
//        return isRequest;
//    }
//
//    /**
//     * 设置是否获取到经纬度
//     *
//     * @param boo
//     */
//    public void setIsGetGPS(boolean boo) {
//        isGetGPS = boo;
//    }
//
//    /**
//     * 得到是否获取到经纬度
//     *
//     * @return
//     */
//    public boolean getIsGetGPS() {
//        return isGetGPS;
//    }
//
//
//    public List<UserDeviceConfig> getAttendeeDevice(long uid) {
//        List<UserDeviceConfig> list = mUserDeviceList.get(Long.valueOf(uid));
//        if (list == null) {
//            return null;
//        }
//
//        return new ArrayList<UserDeviceConfig>(list);
//    }
//
//
//    private User currentUser; // 当前用户
//
//
//    public LruCache<Long, Bitmap> mAvatarBmHolder = new BitmapLruCache<>(GlobalConfig.getLruCacheMaxSize());
//
//
//    //会议相关
//
//    private List<Group> mDiscussionBoardGroup = new ArrayList<Group>();
//
//    // ---------------------------------------------------------------------------------------------
//    // 保存文档的ID，暂时用来区分该文档是图片文档还是白板
//    public Map<String, Integer> mShareDocIds = new HashMap<String, Integer>();
//
//    public LongSparseArray<Group> getmGroupHolder() {
//        return mGroupHolder;
//    }
//
//    //会有可能有问题，setDataBaseTableCacheName没有被调用的地方，得到的应该是null
//    public List<String> getDataBaseTableCacheName() {
//        return dataBaseTableCacheName;
//    }
//
//    public void setDataBaseTableCacheName(List<String> dataBaseTableCacheName) {
//        this.dataBaseTableCacheName = dataBaseTableCacheName;
//    }
//
//    public boolean changeGlobleTransFileMember(final int transType, final Context mContext, boolean isAdd, Long key,
//                                               String tag) {
//        key = GlobalHolder.getInstance().getCurrentUserId();
//        Map<Long, Integer> transingCollection = getFileTypeColl(transType);
//        Integer transing = transingCollection.get(key);
//        String typeString = null;
//        if (transType == PviewGlobalConstants.FILE_TRANS_SENDING)
//            typeString = mContext.getResources().getString(R.string.application_global_holder_send_or_upload);
//        else
//            typeString = mContext.getResources().getString(R.string.application_global_holder_download);
//        if (transing == null) {
//            if (isAdd) {
//                LogUtil.e("TRANSING_FILE_SIZE",
//                        tag + " --> ID为- " + key + " -的用户或群 , " + "传输类型 : " + typeString + " 正在传输文件加1 , 当前数量为1");
//                transing = 1;
//                transingCollection.put(key, transing);
//            }
//            return true;
//        } else {
//            if (isAdd) {
//                if (transing >= GlobalConfig.MAX_TRANS_FILE_SIZE) {
//                    new Thread(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            Looper.prepare();
//                            // if(transType ==
//                            // PviewGlobalConstants.FILE_TRANS_SENDING)
//                            // Toast.makeText(mContext,
//                            // "发送文件个数已达上限，当前正在传输的文件数量已达5个",
//                            // Toast.LENGTH_LONG).show();
//                            // else
//                            // Toast.makeText(mContext,
//                            // "下载文件个数已达上限，当前正在下载的文件数量已达5个",
//                            // Toast.LENGTH_LONG).show();
//                            Toast.makeText(mContext, R.string.application_global_holder_limit_number, Toast.LENGTH_LONG)
//                                    .show();
//                            Looper.loop();
//                        }
//                    }).start();
//                    return false;
//                } else {
//                    transing = transing + 1;
//                    LogUtil.e("TRANSING_FILE_SIZE", tag + " --> ID为- " + key + " -的用户或群 , " + "传输类型 : " + typeString
//                            + " 正在传输文件加1 , 当前数量为: " + transing);
//                    transingCollection.put(key, transing);
//                    return true;
//                }
//            } else {
//                if (transing == 0)
//                    return false;
//                transing = transing - 1;
//                LogUtil.e("TRANSING_FILE_SIZE", tag + " --> ID为- " + key + " -的用户或群 , " + "传输类型 : " + typeString
//                        + " 正在传输文件减1 , 当前数量为: " + transing);
//                transingCollection.put(key, transing);
//                return true;
//            }
//        }
//
//    }
//
//    private Map<Long, Integer> getFileTypeColl(int transType) {
//        return GlobalConfig.mTransingFiles;
////         if(transType == PviewGlobalConstants.FILE_TRANS_SENDING)
////         return GlobalConfig.mTransingFiles;
////         else
////         return GlobalConfig.mDownLoadingFiles;
//    }
//
//    public boolean removeGroup(int gType, long gid) {
//        List<Group> list = null;
//        if (gType == GroupType.GROUP_TYPE_CONFERENCE) {
//            list = mConfGroup;
//        } else if (gType == GroupType.GROUP_TYPE_CONTACT) {
//            list = mContactsGroup;
//        } else if (gType == GroupType.GROUP_TYPE_CROWD) {
//            list = mCrowdGroup;
//        } else if (gType == GroupType.GROUP_TYPE_DEPARTMENT) {
//            list = mOrgGroup;
//        } else if (gType == GroupType.GROUP_TYPE_DISCUSSION) {
//            list = mDiscussionBoardGroup;
//        }
//        mGroupHolder.remove(Long.valueOf(gid));
//        if (list != null) {
//            for (int i = 0; i < list.size(); i++) {
//                Group g = list.get(i);
//                if (g.getmGId() == gid) {
//                    list.remove(g);
//                    return true;
//                }
//            }
//        }
//        return false;
//    }

//    public Bitmap getUserAvatar(long id) {
//        Long key = Long.valueOf(id);
//        return mAvatarBmHolder.get(key);
//    }
}

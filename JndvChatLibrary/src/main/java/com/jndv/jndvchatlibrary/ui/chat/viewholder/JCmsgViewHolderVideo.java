package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.IntentConstant;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.VideoPlayActivity;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgVideoContentBean;
import com.jndv.jndvchatlibrary.utils.JCFileUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 小视频信息展示
 * @Author SunQinzheng
 * @Time 2022/6/22 上午 11:25
 **/
public class JCmsgViewHolderVideo extends JCmsgViewHolderBase {
    int w = 240;
    int h = 240 ;
    private TextView tvMessage;
    private ImageView ivBackground;
    private JCMsgVideoContentBean bean;

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_video;
    }

    @Override
    protected void inflateContentView() {
        tvMessage = findViewById(R.id.tv_msg_long);
        ivBackground = findViewById(R.id.iv_video_background);
    }

    @Override
    protected void bindContentView() {
        Log.e("bindContentView", message.getContent());
        JNLogUtil.e("======Content==" + base64ToString(message.getContent()));
        Log.d("zhang","视频content=="+base64ToString(message.getContent()));
        try {
            bean = new Gson().fromJson(base64ToString(message.getContent()), JCMsgVideoContentBean.class);
            JNLogUtil.e("======SendTime==" + message.getSendTime());
            JNLogUtil.e("======ReceiveTime==" + message.getReceiveTime());
            tvMessage.setText(bean.getTime());
            JCThreadManager.getIO().execute(new Runnable() {
                @Override
                public void run() {
                    if (isReceivedMessage()){
                        Bitmap urlThumbnail = JCFileUtils.getNetVideoBitmap(bean.getUrl());
                        if (urlThumbnail == null) {
                            Bitmap pathThumbnail = JCFileUtils.getLocalVideoBitmap(bean.getPath());
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    if (pathThumbnail != null) {
                                        initWH(pathThumbnail.getWidth(), pathThumbnail.getHeight());
                                        ivBackground.setImageBitmap(pathThumbnail);
                                    }else {
                                        initWH(240, 240);
                                        ivBackground.setBackgroundResource(R.drawable.jc_loading_error);
                                    }
                                }
                            });
                        } else {
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    initWH(urlThumbnail.getWidth(), urlThumbnail.getHeight());
                                    ivBackground.setImageBitmap(urlThumbnail);
                                }
                            });
                        }
                    }else {
                        Bitmap pathThumbnail = JCFileUtils.getLocalVideoBitmap(bean.getPath());
                        if (pathThumbnail != null) {
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    initWH(pathThumbnail.getWidth(), pathThumbnail.getHeight());
                                    ivBackground.setImageBitmap(pathThumbnail);
                                }
                            });
                        }else {
                            Bitmap urlThumbnail = JCFileUtils.getNetVideoBitmap(bean.getUrl());
                            JCThreadManager.onMainHandler(new Runnable() {
                                @Override
                                public void run() {
                                    if (null!=urlThumbnail){
                                        initWH(urlThumbnail.getWidth(), urlThumbnail.getHeight());
                                        ivBackground.setImageBitmap(urlThumbnail);
                                    }else {
                                        initWH(240, 240);
                                        ivBackground.setBackgroundResource(R.drawable.jc_loading_error);
                                    }
                                }
                            });
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            JNLogUtil.e("======getMsgType==" + message.getMsgType());
            tvMessage.setText("未识别的消息：" + message.getContent());
        }
    }

    private void initWH(int width, int height){
        if (width>0&&height>0){
            if (width>240){
                h = 240*height/width;
            }
            if (h<240){
                h=240;
                w = 240*width/height;
            }
        }
        setLayoutParams(w,h,ivBackground);
        EventBus.getDefault().post(new JNCodeEvent<>(JNEventBusType.CODE_MESSAGE_CHANGE_UI,""));
    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_transmit), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_collect), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_withdraw), 1));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_delete), 2));
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected void onItemClick() {
        Log.e("MediaPlayerUtil", bean.toString());
        try {
            Intent intent = new Intent(context, VideoPlayActivity.class);
//            intent.putExtra("path", bean.getUrl());
            intent.putExtra(IntentConstant.WEB_URL, bean.getUrl());
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.jndv.jndvchatlibrary.ui.chat.fragment;


import android.text.TextUtils;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.http.api.JCGetOrganizationMonitorApi;

/**
 * Author: wangguodong
 * Date: 2022/4/20
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 组织机构及监控列表
 */
public class JCOrganizationMonitorViewModel extends ViewModel {

    private Fragment fragment;

    private MutableLiveData<JCGetOrganizationMonitorItem> organizationList ;

    public JCOrganizationMonitorViewModel() {
        organizationList = new MutableLiveData<>();
    }

    public void init(Fragment fragment){
        this.fragment = fragment ;
    }

    public MutableLiveData<JCGetOrganizationMonitorItem> getOrganizationList() {
        return organizationList;
    }

    public void getOrganzationListData(String parentid, int nid) {
        EasyHttp.get(fragment)
                .api(new JCGetOrganizationMonitorApi()
                        .setZoningCode(parentid)
                )
                .request(new OnHttpListener<JCGetOrganizationMonitorApi.Bean>() {
                    @Override
                    public void onSucceed(JCGetOrganizationMonitorApi.Bean result) {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            JCGetOrganizationMonitorItem jcOrganzationListModelItem = new JCGetOrganizationMonitorItem();
                            jcOrganzationListModelItem.setType(0);
                            jcOrganzationListModelItem.setData(result.getData());
                            jcOrganzationListModelItem.setNid(nid);
                            organizationList.setValue(jcOrganzationListModelItem);
                        }else {
                            JCGetOrganizationMonitorItem jcOrganzationListModelItem = new JCGetOrganizationMonitorItem();
                            jcOrganzationListModelItem.setType(1);
                            organizationList.setValue(jcOrganzationListModelItem);
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        JCGetOrganizationMonitorItem jcOrganzationListModelItem = new JCGetOrganizationMonitorItem();
                        jcOrganzationListModelItem.setType(1);
                        organizationList.setValue(jcOrganzationListModelItem);
                    }
                });
    }

public class JCGetOrganizationMonitorItem {

    private JCGetOrganizationMonitorApi.Bean.DataBean data;//新的数据列表
    private int type;//0=成功；1=失败
    private int nid;//-1=首次获取；

    public JCGetOrganizationMonitorApi.Bean.DataBean getData() {
        return data;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setData(JCGetOrganizationMonitorApi.Bean.DataBean data) {
        this.data = data;
    }

    public int getNid() {
        return nid;
    }

    public void setNid(int nid) {
        this.nid = nid;
    }
}
}

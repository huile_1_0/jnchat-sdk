package com.jndv.jndvchatlibrary.ui.chat.fragment.actions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jndv.jnbaseutils.chat.listUi.JCbaseActions;
import com.jndv.jndvchatlibrary.R;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/16
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 底部加号菜单grid布局使用的adapter
 */
public class JCactionsGridviewAdapter extends BaseAdapter {

    private Context context;

    private List<JCbaseActions> baseActions;

    public JCactionsGridviewAdapter(Context context, List<JCbaseActions> baseActions) {
        this.context = context;
        this.baseActions = baseActions;
    }

    @Override
    public int getCount() {
        return baseActions.size();
    }

    @Override
    public Object getItem(int position) {
        return baseActions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemlayout;
        if (convertView == null) {
            itemlayout = LayoutInflater.from(context).inflate(R.layout.jc_item_actions, null);
        } else {
            itemlayout = convertView;
        }

        JCbaseActions viewHolder = baseActions.get(position);
        ((ImageView) itemlayout.findViewById(R.id.imageView)).setBackgroundResource(viewHolder.getIconResId());
        ((TextView) itemlayout.findViewById(R.id.textView)).setText(context.getString(viewHolder.getTitleId()));
        return itemlayout;
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProvider;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.ehome.manager.utils.JNLogUtil;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.ActivityCrowdCreateBinding;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCCreateGroupUsersBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCNumbersData;
import com.jndv.jndvchatlibrary.ui.crowd.ui.SelectListAdapter;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCCrowdCreateViewModel;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建群页面
 */
public class JCCrowdCreateActivity extends JCBaseHeadActivity{

    private static final int CREATE_GROUP_MESSAGE = 4;
    private static final int UPDATE_CROWD_RESPONSE = 5;
    private static final int END_CREATE_OPERATOR = 9;

    private static final int SEND_SERVER_TYPE_INVITE = 10;
    private static final int SEND_SERVER_TYPE_CREATE = 11;

    private SearchView crowd_create_sv;
    private ListView mGroupListView;
    private ProgressDialog mCreateWaitingDialog;
    private JCCrowdCreateViewModel jcCrowdCreateViewModel;

    //    private CrowdGroup crowd; // 群组
    private List<JCNumbersData> mList = new ArrayList<>();
    ; // 好友列表
    private List<JCNumbersData> tempList = new ArrayList<>(); // 选中的临时列表


    /**
     * ListView适配器
     */
    private SelectListAdapter selectListAdapter;

    private ActivityCrowdCreateBinding createBinding;

    OptionsPickerView pvVerifyOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createBinding = ActivityCrowdCreateBinding.inflate(getLayoutInflater(), (ViewGroup) findViewById(R.id.base_content), true);
//        setContentView(R.layout.activity_crowd_create);
        initView();
        initData();
        initVerifyPicker();
    }

    private void initData() {
        jcCrowdCreateViewModel = new ViewModelProvider(this).get(JCCrowdCreateViewModel.class);
        jcCrowdCreateViewModel.inidata(this);

        jcCrowdCreateViewModel.getAddressBook();
        jcCrowdCreateViewModel.getAddressBookData().observe(this, bean -> {
            try {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, bean.getCode()) && bean.getData() != null && bean.getData().size() > 0) {
                    mList.clear();
                    mList.addAll(bean.getData());
                    selectListAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        jcCrowdCreateViewModel.getCrowdCreateMutableLiveData().observe(this, bean -> {
            try {
                JNLogUtil.e("==createGroup==bbbb==");
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, bean.getCode())) {
                    EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_NET,"创建群组"));
                    Toast.makeText(JCCrowdCreateActivity.this, "创建群组成功", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(JCCrowdCreateActivity.this, "创建群组失败", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    private void initView() {
        // title
//        getSupportActionBar().hide();

//        findViewById(R.id.back).setOnClickListener(v -> finish());
        setCenterTitle("创建群组");
        // 选择列表
        selectListAdapter = new SelectListAdapter(this);
        selectListAdapter.setmList(mList);
        createBinding.crowdCreateLv.setAdapter(selectListAdapter);
        createBinding.crowdCreateLv.setTextFilterEnabled(true);
        createBinding.crowdCreateLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mList.get(position).isGroupChatSelectState()) {
                    mList.get(position).setGroupChatSelectState(false);
                    tempList.remove(mList.get(position));
                } else {
                    mList.get(position).setGroupChatSelectState(true);
                    tempList.add(mList.get(position));
                }
                selectListAdapter.notifyDataSetChanged();

            }
        });

        String hint = getResources().getString(R.string.crowd_create_name_input_hint);
        SpannableStringBuilder style = new SpannableStringBuilder(hint);
        style.setSpan(new ForegroundColorSpan(Color.rgb(194, 194, 194)), 0, // common_gray_color_c2
                hint.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        createBinding.crowdCreateName.setHint(style);

        TextView group_chat_create_tv_start = findViewById(R.id.right);
        group_chat_create_tv_start.setText("确定");
        group_chat_create_tv_start.setVisibility(View.VISIBLE);
        group_chat_create_tv_start.setOnClickListener(new View.OnClickListener() { // 确定
            @Override
            public void onClick(View v) {
                createGroup();
            }
        });
        createBinding.rlVerifyType.setOnClickListener(v -> {
            pvVerifyOptions.show();
        });
    }

    private void initVerifyPicker() {
        List<String> list=new ArrayList<>();
        list.add("允许任何人");
        list.add("需要验证");
        pvVerifyOptions = new OptionsPickerBuilder(this, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
                createBinding.tvVerifyType.setText(list.get(options1));
                pos=options1;
            }
        }).build();
        pvVerifyOptions.setPicker(list);
    }

    //设置群权限，验证方式
    int pos=0;

    private void createGroup() {
        String title = createBinding.crowdCreateName.getText().toString().trim();
        if (title.trim().isEmpty()) {
            Toast.makeText(mContext, "群组名称不能为空!", Toast.LENGTH_SHORT).show();
            createBinding.crowdCreateName.requestFocus();
            return;
        }

        List<JCCreateGroupUsersBean> list = new ArrayList<>();
        list.add(new JCCreateGroupUsersBean(JNBasePreferenceSaves.getUserSipId(), JNBasePreferenceSaves.getSipAddress()
                , JNBasePreferenceSaves.getJavaAddress(), JNBasePreferenceSaves.getUserName(),JNBasePreferenceSaves.getUserHead()));
        for (int i = 0; i < tempList.size(); i++) {
            list.add(new JCCreateGroupUsersBean(tempList.get(i)));
        }
        jcCrowdCreateViewModel.createGroup(title, "", "10", "0", new Gson().toJson(list),String.valueOf(pos));

    }
}

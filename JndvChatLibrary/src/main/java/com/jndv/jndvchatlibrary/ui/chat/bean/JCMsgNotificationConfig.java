package com.jndv.jndvchatlibrary.ui.chat.bean;

import android.app.PendingIntent;
import android.content.Intent;

/**
 * Author: wangguodong
 * Date: 2022/5/5
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 消息通知消息配置信息
 */
public class JCMsgNotificationConfig {
    private int iconId;
    private String title;
//    private String content;
    private PendingIntent pendingIntent;
    private Intent intent;

    public JCMsgNotificationConfig(int iconId, String title, PendingIntent pendingIntent, Intent intent) {
        this.iconId = iconId;
        this.title = title;
//        this.content = content;
        this.pendingIntent = pendingIntent;
        this.intent = intent;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }

    public PendingIntent getPendingIntent() {
        return pendingIntent;
    }

    public void setPendingIntent(PendingIntent pendingIntent) {
        this.pendingIntent = pendingIntent;
    }
}

package com.jndv.jndvchatlibrary.ui.chat.fragment.actions;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.Toast;

import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.listUi.JCbaseActions;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipP2PActivity;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgAudioVideoRecordContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;

import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.functions.Consumer;

/**
 * Author: wangguodong
 * Date: 2022/2/16
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 相册-聊天页面加号菜单中菜单项
 */
public class JCVoiceCallAction extends JCbaseActions {

    public JCVoiceCallAction(int nameId, int iconId) {
        super(nameId, iconId);
    }

    public JCVoiceCallAction() {
        super(R.string.chat_action_title_voice_call, R.drawable.jc_chat_action_call_a);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onClick() {
        RxPermissions rxPermissions=new RxPermissions(getContainer().activity);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                ,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO)
                .subscribe(new Consumer<Boolean>() {
            @SuppressLint("CheckResult")
            @Override
            public void accept(Boolean aBoolean) {
                if (aBoolean) {
                    //申请的权限全部允许
                    String mySipNumber = JNBasePreferenceSaves.getUserSipId();
                    String friendSipNumber = getOtherId();
                    if (TextUtils.isEmpty(mySipNumber)) {
                        Toast.makeText(getActivity(), "请先配置sip账号！", Toast.LENGTH_SHORT).show();
                    } else {
                        if (mySipNumber.equals(friendSipNumber)) {
                            Toast.makeText(getActivity(), "无法和自己通话！", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent;
                            intent = new Intent(getActivity(), JCPjSipP2PActivity.class);
                            intent.putExtra("tag", "outing");
                            intent.putExtra("number", friendSipNumber.replace(" ", ""));
                            getActivity().startActivity(intent);
                            JCPjSipP2PActivity.setJCFragmentSelect(new JCbaseFragment.JCFragmentSelect() {
                                @Override
                                public void onSelecte(int type, Object... objects) {
                                    try {
                                        if (null!=getContainer() && null!=getContainer().activity
                                                && null!=getContainer().jCsessionChatInfoBean && null!=getContainer().sessionType
                                                && null!=getContainer().sessionType){
                                            JCMsgAudioVideoRecordContentBean audioVideoBean = (JCMsgAudioVideoRecordContentBean)objects[0];
                                            String audioVideoMsg = new Gson().toJson(audioVideoBean);
                                            JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                                                    audioVideoMsg, "" + JCmessageType.AUDIO_VIDEO_RECORD, getOtherId()
                                                    , JNSpUtils.getString(getContainer().activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
                                                    , JNBasePreferenceSaves.getSipAddress(), getContainer().jCsessionChatInfoBean.getSessionChatId()
                                                    , getContainer().jCsessionChatInfoBean.getSessionaddress()
                                                    , "" + getContainer().sessionType, JNBasePreferenceSaves.getJavaAddress()
                                                    , getContainer().jCsessionChatInfoBean.getSessionaddressJava());

                                            JNLogUtil.e("====CODE_ON_CLOSE_AUDIO_VIDEO======");
                                            sendMessage(jCimMessageBean);
                                        }
                                    }catch (Exception e){
                                        JNLogUtil.e("============", e);
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void codeEvent(JNCodeEvent codeEvent) {
//        switch (codeEvent.getCode()) {
//            case JNEventBusType.CODE_ON_CLOSE_AUDIO_VIDEO:
//                try {
//                    if (null!=getContainer() && null!=getContainer().activity
//                            && null!=getContainer().jCsessionChatInfoBean && null!=getContainer().sessionType
//                            && null!=getContainer().sessionType){
//                        JCMsgAudioVideoRecordContentBean audioVideoBean = (JCMsgAudioVideoRecordContentBean)codeEvent.getData();
//                        String audioVideoMsg = new Gson().toJson(audioVideoBean);
//                        JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
//                                audioVideoMsg, "" + JCmessageType.AUDIO_VIDEO_RECORD, getOtherId()
//                                , JNSpUtils.getString(getContainer().activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
//                                , JNBasePreferenceSaves.getSipAddress(), getContainer().jCsessionChatInfoBean.getSessionChatId()
//                                , getContainer().jCsessionChatInfoBean.getSessionaddress()
//                                , "" + getContainer().sessionType, JNBasePreferenceSaves.getJavaAddress()
//                                , getContainer().jCsessionChatInfoBean.getSessionaddressJava());
//
//                        JNLogUtil.e("====CODE_ON_CLOSE_AUDIO_VIDEO======");
//                        sendMessage(jCimMessageBean);
//                    }
//                }catch (Exception e){
//                    JNLogUtil.e("============", e);
//                }
//                break;
//
//        }
//    }

}

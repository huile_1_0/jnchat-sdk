package com.jndv.jndvchatlibrary.ui.chat.fragment.actions;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.ViewPager;

import com.jndv.jnbaseutils.chat.listUi.JCbaseActions;
import com.jndv.jndvchatlibrary.R;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/16
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 底部加号菜单管理工具类
 */
public class JCactionsManager {

    // 初始化更多布局adapter
    public static void init(View view, List<JCbaseActions> actions) {
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        final ViewGroup indicator = (ViewGroup) view.findViewById(R.id.actions_page_indicator);

        JCactionsPagerAdapter adapter = new JCactionsPagerAdapter(viewPager, actions);
//        JCactionsPagerAdapter adapter = new JCactionsPagerAdapter(viewPager, JCchatFactory.getActions());
        viewPager.setAdapter(adapter);
        initPageListener(indicator, adapter.getCount(), viewPager);
    }

    // 初始化更多布局PageListener
    private static void initPageListener(final ViewGroup indicator, final int count, final ViewPager viewPager) {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                setIndicator(indicator, count, position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setIndicator(indicator, count, 0);
    }

    /**
     * 设置页码
     */
    private static void setIndicator(ViewGroup indicator, int total, int current) {
        if (total <= 1) {
            indicator.removeAllViews();
        } else {
            indicator.removeAllViews();
            for (int i = 0; i < total; i++) {
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        15,15);
                if (0!=i)lp.setMargins(10, 0, 0, 0);
                else lp.setMargins(0, 0, 0, 0);
                ImageView imgCur = new ImageView(indicator.getContext());
                imgCur.setId(i);
                imgCur.setLayoutParams(lp);
                // 判断当前页码来更新
                if (i == current) {
                    imgCur.setBackgroundResource(R.drawable.jc_actions_page_selected);
                } else {
                    imgCur.setBackgroundResource(R.drawable.jc_actions_page_unselected);
                }

                indicator.addView(imgCur);
            }
        }
    }
}

package com.jndv.jndvchatlibrary.ui.chat.base;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;

import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jnbaseutils.chat.listUi.JCPopupList;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jnbaseutils.chat.listUi.JCbaseViewHolder;
import com.jndv.jnbaseutils.chat.listUi.JCbaseViewHolderManager;
import com.jndv.jnbaseutils.db.JNObjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jnbaseutils.utils.JNGlideUtils;
import com.jndv.jndvchatlibrary.ChatContant;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.http.api.JCCollectMessageApi;
import com.jndv.jndvchatlibrary.http.api.JCDeleteMessageApi;
import com.jndv.jndvchatlibrary.http.api.JCWithdrawMessageApi;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCTransferActivity;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgTextContentBean;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/2/14
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 聊天消息的viewholder基类
 */
public abstract class JCmsgViewHolderBase extends
        JCbaseViewHolderManager<JCbaseViewHolder, JCbaseIMMessage> {

    // basic
    protected View view;
    protected Context context;
    // data
    protected JCbaseIMMessage message;

    //view
    protected TextView timeTextView;
    protected TextView nameTextView;
    protected TextView readTextView;
    protected FrameLayout contentContainer;
    protected FrameLayout fragme_layout_name;
    protected ImageView headLeft;
    protected ImageView headRight;
    protected ImageView errorButton;
    protected LinearLayout message_item_body;
    protected ProgressBar progressBar, progressBarRight;

    protected int animaitionPos = -1;
    protected int position = -1;

    private boolean isShowBubble = true;//是否显示气泡背景

    public JCmsgViewHolderBase() {
        super();
    }

    /// -- 以下接口可由子类覆盖或实现
    // 返回具体消息类型内容展示区域的layout res id
    abstract protected int getContentResId();

    // 在该接口中根据layout对各控件成员变量赋值
    abstract protected void inflateContentView();

    // 在该接口操作BaseViewHolder中的数据，进行事件绑定，可选
    protected void bindHolder(JCbaseViewHolder holder) {

    }

    // 将消息数据项与内容的view进行绑定
    abstract protected void bindContentView();

    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        return new ArrayList<>();
    }
    
    // 内容区域点击事件响应处理。
    protected void onItemClick() {
    }

    // 内容区域长按事件响应处理。该接口的优先级比adapter中有长按事件的处理监听高，当该接口返回为true时，adapter的长按事件监听不会被调用到。
    protected boolean onItemLongClick() {
        return false;
    }

    // 当是接收到的消息时，内容区域背景的drawable id
    protected int leftBackground() {
        return R.drawable.jc_message_item_left_selector;
    }

    // 当是发送出去的消息时，内容区域背景的drawable id
    protected int rightBackground() {
        return R.drawable.jc_message_item_right_selector;
    }

    // 消息在中间显示时，内容区域背景的drawable id
    protected int centerBackground() {
        return R.drawable.jc_message_item_center_bg;
    }

    // 返回该消息是不是居中显示
    protected boolean isMiddleItem() {
        return false;
    }

    // 是否显示头像，默认为显示
    protected boolean isShowHeadImage() {
        return true;
    }

    // 是否显示气泡背景，默认为显示
    protected boolean isShowBubble() {
        return isShowBubble;
    }

    protected void setShowBubble(boolean showBubble) {
        isShowBubble = showBubble;
    }
    //听筒播放声音消息
    protected void playVoiceMessage(boolean useReceiver){

    }



    /// -- 以下接口可由子类调用
//    protected final MsgAdapter getMsgAdapter() {
//        return (MsgAdapter) adapter;
//    }

    // 设置控件的长宽
    protected void setLayoutParams(int width, int height, View... views) {
        for (View view : views) {
            ViewGroup.LayoutParams maskParams = view.getLayoutParams();
            maskParams.width = width;
            maskParams.height = height;
            view.setLayoutParams(maskParams);
        }
    }

    // 设置FrameLayout子控件的gravity参数
    protected final void setGravity(View view, int gravity) {
        try {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
            params.gravity = gravity;
            view.setLayoutParams(params);
        }catch (Exception e){
            JNLogUtil.e("==JCmsgViewHolderBase==setGravity===", e);
        }
    }

    // 设置FrameLayout子控件的gravity参数
    protected final void setContentGravity(View view, int gravity) {
        try {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
            params.gravity = gravity;
            params.rightMargin = 1;
            params.leftMargin = 1 ;
            view.setLayoutParams(params);
//            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
//            if (gravity==Gravity.RIGHT)params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT );
//            else if (gravity==Gravity.LEFT)params.addRule(RelativeLayout.ALIGN_PARENT_LEFT );
//            params.rightMargin = 1;
//            view.setLayoutParams(params);
        }catch (Exception e){
            JNLogUtil.e("==JCmsgViewHolderBase==setContentGravity===", e);
        }
    }

    // 根据layout id查找对应的控件
    protected <T extends View> T findViewById(int id) {
        return (T) view.findViewById(id);
    }

    // 判断消息方向，是否是接收到的消息
    protected boolean isReceivedMessage() {
//        if (null!=message && null!=message.getToSipID())
//            return message.getToSipID().equals(null== JNBasePreferenceSaves.getUserSipId()?"":JNBasePreferenceSaves.getUserSipId());//
        if (null != message && null != message.getFromSipID())
            return !message.getFromSipID().equals(null == JNBasePreferenceSaves.getUserSipId() ? "" : JNBasePreferenceSaves.getUserSipId());//
        else return false;
    }

    // 获取消息状态
    protected int getMessageState() {
        int state = JCmessageStatusType.sendIng;
        if (null != message && null != message.getStatus()) {
            try {
                state = Integer.parseInt(message.getStatus());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return state;
    }

    protected String base64ToString(String base64) {
        return new String(Base64.decode(base64, Base64.NO_WRAP));
    }


    @Override
    public void convert(JCbaseViewHolder holder, JCbaseIMMessage data, int position, boolean isScrolling) {
        view = holder.getConvertView();
        context = holder.getContext();
        message = data;
        this.position = position;

        inflate();
        refresh();
    }

    protected final void inflate() {
        timeTextView = findViewById(R.id.txt_time);
        readTextView = findViewById(R.id.tv_msg_item_read);
        fragme_layout_name = findViewById(R.id.fragme_layout_name);
        headLeft = findViewById(R.id.item_msg_head_left);
        headRight = findViewById(R.id.item_msg_head_right);
        message_item_body = findViewById(R.id.message_item_body);
        progressBar = findViewById(R.id.message_item_progress);
        progressBarRight = findViewById(R.id.message_item_progress_right);
        nameTextView = findViewById(R.id.txt_allname);
        contentContainer = findViewById(R.id.fragme_layout);
        errorButton = findViewById(R.id.message_item_alert);
//        nameContainer = findViewById(R.id.message_item_name_layout);
//        readReceiptTextView = findViewById(R.id.textViewAlreadyRead);

        // 这里只要inflate出来后加入一次即可
        if (contentContainer.getChildCount() != 0) {
            Log.i("-----", "inflate: ---------" + getContentResId());
            try {
                if (Integer.parseInt(contentContainer.getChildAt(0).getTag().toString()) != getContentResId()) {
                    contentContainer.removeAllViews();
                    setView();
                }
            } catch (Exception e) {
                e.printStackTrace();
                setView();
            }
        } else {
            setView();
        }
        inflateContentView();
    }

    public void changeShow(boolean show){
        try {
            if (show){
                progressBarRight.setVisibility(View.VISIBLE);
            }else {
                progressBarRight.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setView() {
        try {
            View mview = View.inflate(view.getContext(), getContentResId(), null);
            mview.setTag(getContentResId());
            contentContainer.addView(mview);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected final void refresh() {
        setHeadImageView();
        setNameTextView();
        setTimeTextView();
//        setStatus();
        setOnClickListener();
        setLongClickListener();

        setReadReceipt();
//        setContent();
        bindContentView();
        setMessageState();

        setContent();//由于需要设置背景气泡显示状态，所以这个方法一定要在bindContentView之后

    }

    @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
    private void setReadReceipt() {
        if (isMiddleItem()) {
            readTextView.setVisibility(View.GONE);
        } else if (TextUtils.equals(message.getSessionType(), "" + JCSessionType.CHAT_PERSION)) {
            if (isReceivedMessage()) readTextView.setVisibility(View.GONE);
            else {
                if (0 != message.getRead_num()) {
                    readTextView.setText("");
                    readTextView.setBackground(context.getResources().getDrawable(R.drawable.ic_read_yes));
                } else {
                    readTextView.setText("  ");
                    readTextView.setBackground(context.getResources().getDrawable(R.drawable.jc_txt_read_round));
                }
                readTextView.setVisibility(View.VISIBLE);
            }
        } else if (-1 == message.getRead_num()) {//全部已读
            if (isReceivedMessage()) readTextView.setVisibility(View.GONE);
            else {
                readTextView.setText("");
                readTextView.setBackground(context.getResources().getDrawable(R.drawable.ic_read_yes));
                readTextView.setVisibility(View.VISIBLE);
            }
        } else {
            if (isReceivedMessage()) readTextView.setVisibility(View.GONE);
            else {
                readTextView.setText("" + message.getRead_num());
                readTextView.setBackground(context.getResources().getDrawable(R.drawable.jc_txt_read_round));
                readTextView.setVisibility(View.VISIBLE);
            }
        }
    }

    /*------------------------------------------设置外围信息，时间、头像等----------------------------------------*/

    private void setHeadImageView() {
        ImageView show = isReceivedMessage() ? headLeft : headRight;
        ImageView hide = isReceivedMessage() ? headRight : headLeft;
        hide.setVisibility(View.GONE);
        if (!isShowHeadImage()) {
            show.setVisibility(View.GONE);
            return;
        }
        if (isMiddleItem()) {
            show.setVisibility(View.GONE);
        } else {
            show.setVisibility(View.VISIBLE);

            JNGlideUtils.loadCircleImage(context, isReceivedMessage() ? message.getFromHead() : JNBasePreferenceSaves.getUserHead(), show);
        }
    }

    public void setNameTextView() {
        if (isMiddleItem()) {
            fragme_layout_name.setVisibility(View.GONE);
        } else {
            fragme_layout_name.setVisibility(View.VISIBLE);
            if (isReceivedMessage()) {
                setGravity(nameTextView, Gravity.LEFT);
            } else {
                setGravity(nameTextView, Gravity.RIGHT);
            }
        }
        //这里需要设置用户昵称
//        nameTextView.setText("默认昵称");
        if(!TextUtils.isEmpty(message.getFromName())){
            nameTextView.setText(message.getFromName());
        }else {
//            String decodeContent = new String(Base64.decode(message.getContent(),Base64.NO_WRAP));
//            JCMsgTextContentBean jcMsgTextContentBean = new Gson().fromJson(decodeContent, JCMsgTextContentBean.class);
            nameTextView.setText(message.getFromSipID());
        }

    }

    @SuppressLint("RtlHardcoded")
    protected void setContent() {
        if (!isShowBubble()) {
            contentContainer.setBackgroundResource(R.drawable.ic_loading_tm);
        }

//        LinearLayout bodyContainer = (LinearLayout) view.findViewById(R.id.message_item_body);
//
//        // 调整container的位置
//        int index = isReceivedMessage() ? 0 : 3;
//        if (bodyContainer.getChildAt(index) != contentContainer) {
//            bodyContainer.removeView(contentContainer);
//            bodyContainer.addView(contentContainer, index);
//        }

        if (isMiddleItem()){
            setGravity(message_item_body, Gravity.CENTER);
            if (isShowBubble()) contentContainer.setBackgroundResource(centerBackground());
//            else contentContainer.setBackground(null);
        } else {
            if (isReceivedMessage()){
                Log.e("netdata", "bindContentView: ========005==接收====");
                if (isShowBubble()) contentContainer.setBackgroundResource(leftBackground());
//                else contentContainer.setBackground(null);
                setGravity(message_item_body, Gravity.LEFT);
                setContentGravity(contentContainer, Gravity.LEFT);
            } else {
                Log.e("netdata", "bindContentView: ========005==发送====");
                if (isShowBubble()) contentContainer.setBackgroundResource(rightBackground());
//                else contentContainer.setBackground(null);
                setGravity(message_item_body, Gravity.RIGHT);
                setContentGravity(contentContainer, Gravity.RIGHT);
            }
        }
    }

    public void setOnClickListener() {
        if (null != contentContainer)
            contentContainer.setOnClickListener(v -> {
               Log.e("setOnClickListener",position+"++++++");
                onItemClick();
            });
        if (null != readTextView) {
            readTextView.setOnClickListener(v -> {
                if (TextUtils.equals(message.getSessionType(), "" + JCSessionType.CHAT_GROUP)) {
                    JNLogUtil.e("=======message.getRead_num()==" + message.getRead_num());
                    if (-1 != message.getRead_num() && -2 != message.getRead_num())
                        EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>
                                (JNEventBusType.CODE_MESSAGE_READ_USER_LIST, "", message));
                }
            });
        }
        if (null != errorButton) {
            errorButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JCMessageUtils.reSendMessage((JCimMessageBean) message, context);
                }
            });
        }
    }

    public void setLongClickListener() {
        if (null != contentContainer)
            contentContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    List<JCPopupMenuBean> jcPopupMenuBeanList = getPopupMenuItemList();
                    List<JCPopupMenuBean> popupMenuItemList = new ArrayList<>();

                    if (null == jcPopupMenuBeanList || jcPopupMenuBeanList.size() <= 0) {
                        return false;
                    }
                    for (int i = 0; i < jcPopupMenuBeanList.size(); i++) {
                        if (2 == jcPopupMenuBeanList.get(i).getType()) {
                            popupMenuItemList.add(jcPopupMenuBeanList.get(i));
                        } else if (0 == jcPopupMenuBeanList.get(i).getType() && isReceivedMessage()) {
                            popupMenuItemList.add(jcPopupMenuBeanList.get(i));
                        } else if (1 == jcPopupMenuBeanList.get(i).getType() && !isReceivedMessage()) {
                            popupMenuItemList.add(jcPopupMenuBeanList.get(i));
                        }
                    }
                    int[] location = new int[2];
                    contentContainer.getLocationOnScreen(location);
                    JCPopupList popupList = new JCPopupList(contentContainer.getContext());
                    popupList.showPopupListWindow(contentContainer, position, location[0] + contentContainer.getWidth() / 2,
                            location[1], popupMenuItemList, new JCPopupList.PopupListListener() {
                                @Override
                                public boolean showPopupList(View adapterView, View contextView, int contextPosition) {
                                    return true;
                                }

                                @Override
                                public void onPopupListClick(View contextView, int contextPosition, int position) {
//                                    JCPopupMenuBean bean = popupMenuItemList.get(position);
//                                    if (0==bean.getClickType()){
//
//                                    }else {
//
//                                    }
                                    String beanName = popupMenuItemList.get(position).getName();
                                    if (beanName.equals(context.getString(R.string.message_menu_copy))) {
                                        try {
                                            JCMsgTextContentBean bean = new Gson().fromJson(base64ToString(message.getContent()), JCMsgTextContentBean.class);
                                            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                            ClipData clip = ClipData.newPlainText("jndvChat", bean.getText());
                                            clipboard.setPrimaryClip(clip);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else if (beanName.equals(context.getString(R.string.message_menu_transmit))) {
//                                        EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_MESSAGE_TRANSMIT
//                                                , "", message));
                                        /*JCMsgTransmitFragment transmitFragment = new JCMsgTransmitFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable(JCMsgTransmitFragment.CONTENT, (JCimMessageBean)message);
                                        JCbaseFragmentActivity.start(context, transmitFragment, "转发", bundle);*/
                                        ChatContant.TRANSFER_PERSON_LIST.clear();
                                        Intent intent=new Intent(context, JCTransferActivity.class);
                                        intent.putExtra("content",(JCimMessageBean)message);
                                        context.startActivity(intent);

                                    } else if (beanName.equals(context.getString(R.string.message_menu_collect))) {
                                        collectMessage();
                                    } else if (beanName.equals(context.getString(R.string.message_menu_withdraw))) {
                                        if (System.currentTimeMillis() - Long.parseLong(message.getSendTime()) > 2 * 60 * 1000) {
                                            JCChatManager.showToast("已超过2分钟，不能撤回!");
                                        } else withdrawMessage();
                                    } else if (beanName.equals(context.getString(R.string.message_menu_delete))) {
                                        deleteMessage();
                                    } else if (beanName.equals(context.getString(R.string.message_menu_audio_receiver))) {
                                        playVoiceMessage(true);
                                    }

                                }
                            });
                    return true;
                }
            });
    }


    @SuppressLint("SimpleDateFormat")
    private void setTimeTextView() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat format1 = new SimpleDateFormat("MM-dd HH:mm");
        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        long time = 0;
        JNLogUtil.e("=======setTimeTextView===0001==");
        try {
            JNLogUtil.e("=======setTimeTextView===0002==");
            time = Long.parseLong(message.getReceiveTime());
//            time = Long.parseLong(message.getSendTime());
        } catch (Exception e) {
            e.printStackTrace();
            time = calendar.getTime().getTime();
            JNLogUtil.e("=======setTimeTextView===0003==");
        }
        String t1 = "";
        JNLogUtil.e("=======setTimeTextView===0004==time=="+time);
        Calendar d2 = Calendar.getInstance();
        d2.setTime(new Date(time));
        JNLogUtil.e("viewholder", "setTimeTextView: ====d2==" + d2.get(Calendar.DAY_OF_MONTH));
        JNLogUtil.e("viewholder", "setTimeTextView: ====d3==" + (calendar.get(Calendar.DAY_OF_MONTH) - 1));

        if (d2.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)){
            if (d2.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)){
                if (d2.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH) - 1) {//昨天
                    t1 = "昨天" + format2.format(d2.getTime());
                } else if (d2.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {//今天
                    t1 = format2.format(d2.getTime());
                }else {
                    t1 = format1.format(d2.getTime());
                }
            }else {
                t1 = format1.format(d2.getTime());
            }
        }else {
            t1 = format.format(d2.getTime());
        }

        JNLogUtil.e("=======setTimeTextView===0005==t1=="+t1);
        timeTextView.setText(t1);
    }

    private void setMessageState() {
        progressBar.setVisibility(View.GONE);
        errorButton.setVisibility(View.GONE);
        switch (getMessageState()) {
            case JCmessageStatusType.sendIng:
                try {
                    long time = 0;
                    try {
                        time = Long.parseLong(message.getSendTime());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (System.currentTimeMillis() - time > 20 * 1000) {
                        Box<JCimMessageBean> imMsgBox = JNObjectBox.getBoxStore().boxFor(JCimMessageBean.class);
                        QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
                        JCimMessageBean localMessage = builder
                                .equal(JCimMessageBean_.msgID, message.getMsgID(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                                .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                                .build().findFirst();
                        assert localMessage != null;
                        localMessage.setStatus("" + JCmessageStatusType.sendFail);
                        EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>
                                (JNEventBusType.CODE_MESSAGE_UPDATE, "", localMessage));
                        imMsgBox.put(localMessage);
                        errorButton.setVisibility(View.VISIBLE);
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case JCmessageStatusType.sendSuccessceS:

                break;
            case JCmessageStatusType.sendSuccessceN:

                break;
            case JCmessageStatusType.sendSuccessceY:

                break;
            case JCmessageStatusType.sendFail:
                errorButton.setVisibility(View.VISIBLE);
                break;
            case JCmessageStatusType.sendBack:

                break;
            case JCmessageStatusType.sendDelete:

                break;
        }
    }

    /**
     * 消息相关接口
     * 收藏消息
     */
    public void collectMessage() {
        EasyHttp.post((LifecycleOwner) context)
                .api(new JCCollectMessageApi())
                .json(new Gson().toJson(new JCCollectMessageApi()
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                        .setType("" + 0)
                        .setMsgBody(new Gson().toJson((JCimMessageBean) message))))
                .request(new OnHttpListener<JCCollectMessageApi.Bean>() {
                    @Override
                    public void onSucceed(JCCollectMessageApi.Bean result) {
                        try {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                                Toast.makeText(context, "收藏成功！", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "收藏失败！", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
//                        netFail();
                        Toast.makeText(context, "收藏失败！", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /**
     * 消息相关接口
     * 撤回消息
     */
    public void withdrawMessage() {
        JCWithdrawMessageApi jcWithdrawMessageApi = new JCWithdrawMessageApi()
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setUserId(JNBasePreferenceSaves.getUserSipId())
                .setMsgUuid(message.getMsgID());

        EasyHttp.post((LifecycleOwner) context)
                .api(jcWithdrawMessageApi)
//                .json(new Gson().toJson(jcWithdrawMessageApi))
                .request(new OnHttpListener<JCCollectMessageApi.Bean>() {
                    @Override
                    public void onSucceed(JCCollectMessageApi.Bean result) {
                        try {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                                JCMessageUtils.withdrawMessage(message.getMsgID());
                                EventBus.getDefault().post(new JNCodeEvent<String>(JNEventBusType.CODE_MESSAGE_WITHDRAW, message.getMsgID(), message.getMsgID()));
                                Toast.makeText(context, "撤回成功！", Toast.LENGTH_SHORT).show();
                            } else if (TextUtils.equals("1001002000", result.getCode())) {
                                Toast.makeText(context, "消息不存在！", Toast.LENGTH_SHORT).show();
                            } else {
                                String msg = "撤回失败！";
                                if (null!=result.getMsg()&&!TextUtils.isEmpty(result.getMsg()))msg = result.getMsg();
                                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
//                        netFail();
                        Toast.makeText(context, "撤回失败！", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /**
     * 消息相关接口
     * 删除消息
     */
    public void deleteMessage() {
        EasyHttp.post((LifecycleOwner) context)
                .api(new JCDeleteMessageApi()
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setUuid(message.getMsgID())
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                )
                .request(new OnHttpListener<JCDeleteMessageApi.Bean>() {
                    @Override
                    public void onSucceed(JCDeleteMessageApi.Bean result) {
                        try {
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                                EventBus.getDefault().post(new JNCodeEvent<String>(JNEventBusType.CODE_MESSAGE_DELETE, message.getSessionId(), message.getMsgID()));
                                Toast.makeText(context, "删除成功！", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "删除失败！", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
//                        netFail();
                        Toast.makeText(context, "删除失败！", Toast.LENGTH_SHORT).show();
                    }
                });
    }


}

package com.jndv.jndvchatlibrary.ui.chat.bean.content;

public class JCIntercomApplyBean {

    private String extend;
    private String fromName;//邀请人
    private String groupnum;//当前群组号码
    private String groupname;//频道名称
//    groupnum  string类型   当前群组号码
//    groupname string类型   频道名称


    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public String getGroupnum() {
        return groupnum;
    }

    public void setGroupnum(String groupnum) {
        this.groupnum = groupnum;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }
}

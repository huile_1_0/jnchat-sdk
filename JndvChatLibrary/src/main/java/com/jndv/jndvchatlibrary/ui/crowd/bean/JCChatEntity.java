package com.jndv.jndvchatlibrary.ui.crowd.bean;

public class JCChatEntity {
    private static final String BASE="grid";

    public static final String JNI_BROADCAST_CATEGORY = "com.pview.jni.broadcast"+BASE;
    public static final String JNI_BROADCAST_CONNECT_STATE_NOTIFICATION = "com.pview.jni.broadcast.connect_state_notification"+BASE;

    //文件传输错误的广播
    public static final String JNI_BROADCAST_FILE_STATUS_ERROR_NOTIFICATION = "com.pview.jni.broadcast.file_stauts_error_notification"+BASE;
    //文件传输进度广播
    public static final String JNI_BROADCAST_FILE_PROGRESS_NOTIFICATION = "com.pview.jni.broadcast.file_progress_notification"+BASE;
    //文件传输完成的广播
    public static final String JNI_BROADCAST_FILE_END_NOTIFICATION = "com.pview.jni.broadcast.file_end_notification"+BASE;
    //图片消息接收完成的广播
    public static final String JNI_BROADCAST_IMAGE_MSG_END_NOTIFICATION = "com.pview.jni.broadcast.image_msg_end_notification"+BASE;

    public static final String JNI_BROADCAST_GROUP_NOTIFICATION = "com.pview.jni.broadcast.group_geted"+BASE;
    public static final String JNI_BROADCAST_USER_STATUS_NOTIFICATION = "com.pview.jni.broadcast.user_stauts_notification"+BASE;
    public static final String JNI_BROADCAST_GROUP_UPDATED = "com.pview.jni.broadcast.group_updated"+BASE;

    public static final String JNI_BROADCAST_NEW_MESSAGE = "com.pview.jni.broadcast.new.message"+BASE;
    public static final String JNI_BROADCAST_MESSAGE_SENT_RESULT = "com.pview.jni.broadcast.message_sent_result"+BASE;
    public static final String JNI_BROADCAST_NEW_CONF_MESSAGE = "com.pview.jni.broadcast.new.conf.message"+BASE;
    public static final String JNI_BROADCAST_GROUP_USER_REMOVED = "com.pview.jni.broadcast.group_user_removed"+BASE;
    public static final String JNI_BROADCAST_GROUP_USER_ADDED = "com.pview.jni.broadcast.group_user_added"+BASE;

    public static final String JNI_BROADCAST_GROUPS_LOADED = "com.pview.jni.broadcast.groups_loaded"+BASE;
    public static final String JNI_BROADCAST_OFFLINE_MESSAGE_END = "com.pview.jni.broadcast.offline_message_end"+BASE;
    public static final String JNI_BROADCAST_USER_LOG_OUT_NOTIFICATION = "com.pview.jni.broadcast.user_log_out_notification"+BASE;

    public static final String JNI_BROADCAST_KICED_CROWD = "com.pview.jni.broadcast.kick_crowd"+BASE;

    public static final String JNI_BROADCAST_USER_UPDATE_BASE_INFO = "com.pview.jni.broadcast.user_update_base_info"+BASE;
    public static final String JNI_BROADCAST_GROUP_USER_UPDATED_NOTIFICATION = "com.pview.jni.broadcast.group_user_updated"+BASE;
    public static final String JNI_BROADCAST_GROUP_JOIN_FAILED = "com.pview.jni.broadcast.group_join_failed"+BASE;
    public static final String JNI_BROADCAST_CONFERENCE_INVATITION = "com.pview.jni.broadcast.conference_invatition_new"+BASE;
    public static final String JNI_BROADCAST_CONFERENCE_REMOVED = "com.pview.jni.broadcast.conference_removed"+BASE;
    public static final String JNI_BROADCAST_CONFERENCE_REMOVED_SIP_CALL = "com.pview.jni.broadcast.conference_removed_sip_call"+BASE;
    public static final String JNI_BROADCAST_CONFERENCE_CONF_SYNC_OPEN_VIDEO = "com.pview.jni.broadcast.conference_confSyncOpenVideo"+BASE;
    public static final String JNI_BROADCAST_CONFERENCE_CONF_SYNC_CLOSE_VIDEO = "com.pview.jni.broadcast.conference_confSyncCloseVideo"+BASE;
    public static final String JNI_BROADCAST_ON_CHANGE_SYNC_CONF_OPEN_VIDEO_POS = "com.pview.jni.broadcast.conference_vodOpenVideo"+BASE;
    public static final String JNI_BROADCAST_VIDEO_CALL_CLOSED = "com.pview.jni.broadcast.video_call_closed"+BASE;
    public static final String JNI_BROADCAST_CONTACTS_AUTHENTICATION = "com.pview.jni.broadcast.friend_authentication"+BASE;
    public static final String JNI_BROADCAST_NEW_QUALIFICATION_MESSAGE = "com.pview.jni.broadcast.new.qualification_message"+BASE;
    public static final String JNI_BROADCAST_CONFERENCE_CONF_SYNC_CLOSE_VIDEO_TO_MOBILE = "com.pview.jni.broadcast.conference_OnConfSyncCloseVideoToMobile"+BASE;
    public static final String JNI_BROADCAST_CONFERENCE_CONF_SYNC_OPEN_VIDEO_TO_MOBILE = "com.pview.jni.broadcast.conference_OnConfSyncOpenVideoToMobile"+BASE;

    public static final String JNI_BROADCAST_ON_REMOTE_USER_VIDEO_DEVICE = "com.pview.jni.broadcast.OnRemoteUserVideoDevice"+BASE;
    public static final String JNI_BROADCAST_ON_CONF_MEMBER_ENTER_OR_EXIT = "com.pview.jni.broadcast.OnConfMemberEnterOrExit"+BASE;
    public static final String JNI_BROADCAST_ON_CONF_CHAIR_CHANGED = "com.pview.jni.broadcast.OnConfChairChanged"+BASE;
    public static final String JNI_BROADCAST_ON_NOTIFY_CHAIR = "com.pview.jni.broadcast.OnNotifyChair"+BASE;
    public static final String JNI_BROADCAST_ON_KICK_CONF_CALLBACK = "com.pview.jni.broadcast.OnKickConfCallback"+BASE;
    public static final String JNI_BROADCAST_ON_GRANT_PERMISSION_CALLBACK = "com.pview.jni.broadcast.OnGrantPermissionCallback"+BASE;
    public static final String JNI_BROADCAST_ON_ENTER_CONF_CALLBACK = "com.pview.jni.broadcast.OnEnterConfCallback"+BASE;

    // Current user kicked by crowd master key crowd : crowdId
    // Crowd invitation with key crowd
    public static final String JNI_BROADCAST_CROWD_INVATITION = "com.pview.jni.broadcast.crowd_invatition"+BASE;
    // Broadcast for joined new discussion key gid
    public static final String BROADCAST_CROWD_NEW_UPLOAD_FILE_NOTIFICATION = "com.pview.jni.broadcast.new.upload_crowd_file_message"+BASE;

    public static final String ACTION_ACTIVITY_MAIN = "com.pview.jni.activity_main";
    public static final String ACTION_ACTIVITY_LOGIN = "com.pview.jni.activity_login";

    public static final int JNI_GROUP_LOADED = 63;
    public static final int JNI_OFFLINE_LOADED = 64;

    public static final int JNI_RECEIVED_MESSAGE = 91;
    public static final int JNI_RECEIVED_MESSAGE_BINARY_DATA = 93;

    public static final int JNI_UPDATE_USER_INFO = 24;
    public static final int JNI_UPDATE_USER_STATE = 25;
    public static final int LOGIN_OUT_EXIT = 26;
    public static final int LOGIN_SUCCEED = 27;
    public static final int LOGIN_FAILURE = 28;
    public static final int GROUP_LOADED_SUCCEED = 29;

    public static final int JNI_GROUP_NOTIFY = 35; // 后来加入
    public static final int JNI_GROUP_USER_INFO_NOTIFICATION = 60; // 处理OnGetGroupUserInfoCallback回调的Handler
    public static final int JNI_GROUP_VOICE_DEVICES_INFO_NOTIFICATION = 110; // 获取音频设备列表
    public static final int JNI_CONFERENCE_INVITATION = 61;
    public static final int JNI_RECEIVED_VIDEO_INVITATION = 92;

    public static final String DEFAULT_CATEGORY = "com.pview";


    public static final String BROADCAST_GROUP_DELETED_NOTIFICATION = "com.pview.broadcast.group_deleted_notification"+BASE;
    public static final String BROADCAST_CROWD_FILE_ACTIVITY_SEND_NOTIFICATION = "com.pview.broadcast.add_other_friend_waiting_notification"+BASE;
    public static final String BROADCAST_USER_COMMENT_NAME_NOTIFICATION = "com.pview.broadcast.user_comment_name_notification"+BASE;

    public static final String BROADCAST_LOGIN_OUT= "com.pview.broadcast.login.out"+BASE;
    public static final String BROADCAST_ASAGENT_CAMERA_LIST_LOAD_FINISH= "asagent_cameralist_load_finish";

    //海康设备红色按键发送广播
    public static final String BROADCAST_PPTCONN="com.hikvision.keyevent.PPTConn";
    public static final String BROASCAST_PPTDISCONN="com.hikvision.keyevent.PPTDisConn";

    public static final int VOICE_DIALOG_FLAG_RECORDING = 1;
    public static final int VOICE_DIALOG_FLAG_CANCEL = 2;
    public static final int VOICE_DIALOG_FLAG_WARING_FOR_TIME_TOO_SHORT = 3;
    public static final int BATCH_COUNT = 100;

    public static final int START_LOAD_MESSAGE = 1;
    //开始加载网络聊天记录
    public static final int START_LOAD_NET_MESSAGE = 11;
    public static final int LOAD_MESSAGE = 2;
    //加载网络聊天记录
    public static final int LOAD_NET_MESSAGE = 10;
    public static final int END_LOAD_MESSAGE = 3;
    public static final int SEND_MESSAGE = 4;
    public static final int PLAY_NEXT_UNREAD_MESSAGE = 7;
    public static final int ADAPTER_NOTIFY = 9;
    public static final int FILE_STATUS_LISTENER = 20;
    public static final int RECORD_STATUS_LISTENER = 21;
    public static final int RECORD_MIC_LEVEL = 22;

    /**
     * for activity result
     */
    public static final int TAKE_PHOTO_CODE = 101;
    public static final int TAKE_VEDIO_CODE = 104;
    public static final int TAKE_ERRO = 105;
    public static final int TAKE_REQUESTCODE = 99;
    public static final int SELECT_PICTURE_CODE = 100;
    public static final int RECEIVE_SELECTED_FILE = 1000;

    public static final int KEY_CANCELLED_LISTNER = 1;
    public static final int KEY_FILE_TRANS_STATUS_NOTIFICATION_LISTNER = 2;
    public static final int KEY_VIDEO_CONNECTED = 3;
    public static final int KEY_P2P_CALL_RESPONSE = 4;
    public static final int KEY_P2P_RECORD_CALL_RESPONSE = 5;
    public static final int KEY_P2P_RECORD_MIC_CALL_RESPONSE = 6;

    public static final int NATIVE_INVITE = 0x001;
    public static final int NATIVE_ACCEPT = 0x002;
    public static final int NATIVE_REFUSE = 0x003;
    public static final int NATIVE_CANNEL = 0x004;
    public static final int NATIVE_CLOSE = 0x005;
    public static final int NATIVE_MUTE = 0x006;
    public static final int NATIVE_RECORD_START = 0x007;
    public static final int NATIVE_RECORD_STOP = 0x008;

    /**
     * 未读标识，适用于各种场景。
     */
    public static final int READ_STATE_UNREAD = 0;

    /**
     * 已读标识，适用于各种场景。
     */
    public static final int READ_STATE_READ = 1;

    /**
     * 账户类型：未注册用户，用于快速入会
     */
    public static final int ACCOUNT_TYPE_NON_REGISTERED = 2;

    /**
     * 账户类型：电话联系人(仅PC能用，移动端屏蔽)
     */
    public static final int ACCOUNT_TYPE_PHONE_FRIEND = 3;

    /**
     * 用户状态:在线
     */
    public static final int USER_STATUS_ONLINE = 1;

    /**
     * 用户状态:离线
     */
    public static final int USER_STATUS_OFFLINE = 0;

    /**
     * 用户状态:离开
     */
    public static final int USER_STATUS_LEAVING = 2;

    /**
     * 用户状态:繁忙
     */
    public static final int USER_STATUS_BUSY = 3;

    /**
     * 用户状态:请勿打扰
     */
    public static final int USER_STATUS_DO_NOT_DISTURB = 4;

    /**
     * 用户状态:隐身
     */
    public static final int USER_STATUS_HIDDEN = 5;

    /**
     * 会议进入时错误信息：该会议已被删除
     */
    public static final int CONF_ERROR_CONFOVER = 204;

    /**
     * 会议进入时错误信息：与服务器断开连接
     */
    public static final int CONF_ERROR_SERVERDISCONNECT = 206;

    /**
     * 会议中语音激励状态：未激活
     */
    public static final int CONF_VOICE_ACTIVATION_NO = 0;

    /**
     * 会议中语音激励状态：已激活
     */
    public static final int CONF_VOICE_ACTIVATION_YES = 1;

    /**
     * 会议中文档的类型：正常文档
     */
    public static final int DOC_TYPE_NORMAL = 3;

    /**
     * 会议中文档的类型：白板
     */
    public static final int DOC_TYPE_BLACK = 4;

    /**
     * 文件的发送类型：在线发送
     */
    public static final int FILE_TYPE_ONLINE = 1;

    /**
     * 文件的发送类型：离线发送
     */
    public static final int FILE_TYPE_OFFLINE = 2;

    /**
     * 文件的发送类型：离线发送，未加密
     */
    public static final int FILE_ENCRYPT_TYPE = 1;

    /**
     * 文件传输中状态：正在发送
     */
    public static final int FILE_TRANS_SENDING = 10;

    /**
     * 文件传输中状态：正在下载
     */
    public static final int FILE_TRANS_DOWNLOADING = 11;

    /**
     * 文件传输中状态：传输错误
     */
    public static final int FILE_TRANS_ERROR = 13;

    /**
     * 点对点语音留言状态：开始录音
     */
    public static final int RECORD_TYPE_START = 0x0001;

    /**
     * 点对点语音留言状态：结束录音
     */
    public static final int RECORD_TYPE_STOP = 0x0002;

    public static final int MESSAGE_SHOW_TIME = 14;

    public static final int MESSAGE_NOT_SHOW_TIME = 15;

    public static final int UNKOWN = 0;
    public static final int EVIDEODEVTYPE_VIDEO = 1;
    public static final int EVIDEODEVTYPE_CAMERA = 2;
    public static final int EVIDEODEVTYPE_FILE = 3;
    public static final int EVIDEODEVTYPE_VIDEOMIXER = 4;

    public static final int CONF_CAMERA_MASS_LOW = 1;
    public static final int CONF_CAMERA_MASS_MIDDLE = 2;
    public static final int CONF_CAMERA_MASS_HIGH = 3;

    public static final int TYPE_ORG = 1;
    public static final int TYPE_CONF = 4;


    public static final int CONF = 1;

    public static final int ANDROID = 2;


    public static final int TYPE_SEND = 1;
    public static final int TYPE_DOWNLOAD = 2;
    /**
     * eventbus相关code
     */

    public static final int CODE_ACTIVITY_MAIN = 100;
    public static final int CODE_ACTIVITY_CONTACT_DETAIL = 101;
    public static final int CODE_ACTIVITY_LOGIN = 102;
    public static final int CODE_ACTIVITY_P2P = 103;
    public static final int CODE_FRAGMENT_CONTACT_DETAIL = 1001;
    public static final int CODE_FRAGMENT_SIGNIN = 1002;

    public static final int CODE_ORGANIZATION_FILTER = 1010;



    /**
     * sp相关
     */
    public static final String USER_LIST = "user_list";
    public static final String GROUP_LIST = "group_list";
    public static final String CROWD_LIST = "crowd_list";
    public static final String CROWD_LIST_WITH_USER = "crowd_list_with_user";
    public static final String GROUP_AND_USER_LIST = "group_and_user_list";

    public static final String CURRENT_USER_ID = "current_user_id";
    public static final String DB_TABLE_LIST = "db_table_list";
    public static final String HAS_LOGIN = "has_login";
    public static final String LOGIN_OUT_TYPE = "login_out_type";
    public static final String CURRENT_USER_NAME = "current_user_name";
    public static final String CURRENT_PASSWORD = "current_password";
    public static final String CURRENT_IP_WITH_PORT = "current_ip_with_port";
    public static final String HAS_USER_DATA = "has_user_data";
    public static final String FRIEND_LIST = "friend_list";
    public static final String PJSIP_NUMBER = "pjsip_number";
    public static final String PJSIP_PSWD = "pjsip_pswd";
    public static final String PJSIP_HOST = "pjsip_host";
    public static final String PJSIP_REALM = "pjsip_realm";
    public static final String PJSIP_PORT = "pjsip_port";

    public static final String JNI_ACTIVITY_CATEGROY = "com.pview";


    public static final int REUSLT_ACCOUNT_CHECK = 1;
    public static final int RESULT_PHONE_SEND = 2;
    public static final int RESULT_CODE_CHECK = 3;
    public static final int RESULT_PASSWORD_RESET=4;

    public static final int UPDATE_USER_INFO = 5;
    public static final int UPDATE_USER_INFO_DONE = 6;
    public static final int UPDATE_USER_HEADIMG = 7;
    public static final int DELETE_CONTACT_USER = 8;
    public static final int GET_IP_SERVER = 9;
    public static final int CHECK_USER_INFO = 10;

    public static final int RESULT_ERROR = 11;
    //聊天记录token
    public static final int CHATRECORDTOKEN = 12;
    //处理聊天记录
    public static final int CHATRECORD = 13;
    //请求聊天记录
    public static final int GETCHATRECORD = 14;
    //社会治理网格化组织机构数据
    public static final int COMMUNGRIDDING = 16;
    //社会治理网格化页面初始化数据
    public static final int COMMUNGRIDDING_INITIALIZE = 17;
    //社会治理网格化页面事件统计信息
    public static final int COMMUNGRIDDING_EVENT = 18;
    //社会治理网格化页面采集统计信息
    public static final int COMMUNGRIDDING_GATHER= 19;
    //判断个人信息是否完整
    public static final int JUDGEUSERINFO=23;

    public static final String GROUP_NOTIFY="group_notify";
    public static final String NEW_MESSAGE_UNREAD="new_message_unread";

    public static final String LAST_LOGIN_TIME="last_login_time";
    public static final String USER_LOGIN_STATUS="user_login_status";//保存用户在线状态


    public static final int REQUEST_ENTER_CONF = 0x0002;
    public static final int REQUEST_ENTER_CONF_RESPONSE = 0x0003;


}

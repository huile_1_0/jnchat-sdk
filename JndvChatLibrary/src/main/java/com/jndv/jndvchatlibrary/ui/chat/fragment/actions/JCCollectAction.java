package com.jndv.jndvchatlibrary.ui.chat.fragment.actions;

import android.annotation.SuppressLint;

import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.listUi.JCbaseActions;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCCollectMsgFragment;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;


/**
 * Author: wangguodong
 * Date: 2022/2/16
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 收藏-聊天页面加号菜单中菜单项
 */
public class JCCollectAction extends JCbaseActions {
    public JCCollectAction(int nameId, int iconId) {
        super(nameId, iconId);
    }

    public JCCollectAction() {
        super(R.string.action_collect, R.drawable.jc_chat_action_collect);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onClick(){
        JCbaseFragmentActivity.start(JCChatManager.mContext
                , JCCollectMsgFragment.newInstance(), "我的收藏",null, new JCbaseFragment.JCFragmentSelect() {
                    @Override
                    public void onSelecte(int type, Object... objects) {
                        if (type==JCbaseFragment.SELECTE_TYPE_MSG){
                            String contentBase64 = ((JCimMessageBean) objects[0]).getContent();
                            contentBase64 = JNContentEncryptUtils.decryptContent(contentBase64);
                            String msgType = ((JCimMessageBean) objects[0]).getMsgType();
                            JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                                    contentBase64,msgType, getOtherId(), JNBasePreferenceSaves.getUserSipId()
                                    , JNBasePreferenceSaves.getSipAddress(), getOtherId(), getOtherSipAddress()
                                    , ""+getContainer().sessionType, JNBasePreferenceSaves.getJavaAddress(),getOtherJavaAddress());
                            sendMessage(jCimMessageBean);
                        }
                    }
                }, true);
    }
}

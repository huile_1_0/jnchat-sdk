package com.jndv.jndvchatlibrary.ui.crowd.bean;

public class JCServerInfo {

    /**
     * domainid : 2
     * id : 361
     * servertype : MQTT  IMS(sip使用)  MNAS(图片服务使用)
     * serverip : http://bdpapi.qa.jndv.org
     * port : null
     * picaddress : null
     * servername : null
     * urlparams : null
     * remark : 大数据接口API
     */

    private int domainid;
    private int id;
    private String servertype;
    private String serverip;
    private String port;
    private Object picaddress;
    private Object servername;
    private Object urlparams;
    private String remark;

    public int getDomainid() {
        return domainid;
    }

    public void setDomainid(int domainid) {
        this.domainid = domainid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServertype() {
        return servertype;
    }

    public void setServertype(String servertype) {
        this.servertype = servertype;
    }

    public String getServerip() {
        return serverip;
    }

    public void setServerip(String serverip) {
        this.serverip = serverip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Object getPicaddress() {
        return picaddress;
    }

    public void setPicaddress(Object picaddress) {
        this.picaddress = picaddress;
    }

    public Object getServername() {
        return servername;
    }

    public void setServername(Object servername) {
        this.servername = servername;
    }

    public Object getUrlparams() {
        return urlparams;
    }

    public void setUrlparams(Object urlparams) {
        this.urlparams = urlparams;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

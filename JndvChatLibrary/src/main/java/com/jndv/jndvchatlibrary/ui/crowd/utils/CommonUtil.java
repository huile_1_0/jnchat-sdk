package com.jndv.jndvchatlibrary.ui.crowd.utils;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.phone.interfaces.KeyBoardDialogListener;
import com.jndv.jndvchatlibrary.ui.phone.view.DialerKeyView;
import com.jndv.jndvchatlibrary.ui.phone.view.DigitsEditText;
import com.zhy.autolayout.AutoLinearLayout;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sun on 2019/2/17 12:18.
 */
public class CommonUtil {
    private static AlertDialog dialog;
    private static PopupWindow popupWindow;
    public static boolean isDestroy(Activity activity) {
        if (activity == null || activity.isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && activity.isDestroyed())) {
            return true;
        } else {
            return false;
        }
    }

    public static void showKeyBoardDialog(Context context,View parentView, KeyBoardDialogListener listener) {
        int px = DensityUtils.dip2px(context,58);
        if(dialog!=null){
            popupWindow.showAtLocation(parentView,Gravity.BOTTOM,0,px);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_keyboard, null);
            DigitsEditText digits = view.findViewById(R.id.digits);
            ImageButton deleteButton = view.findViewById(R.id.deleteButton);
            TextView tvText1 = view.findViewById(R.id.tv_text1);
            TextView tvText2 = view.findViewById(R.id.tv_text2);
            AutoLinearLayout dv1 = view.findViewById(R.id.dv_1);
            DialerKeyView dv2 = view.findViewById(R.id.dv_2);
            DialerKeyView dv3 = view.findViewById(R.id.dv_3);
            DialerKeyView dv4 = view.findViewById(R.id.dv_4);
            DialerKeyView dv5 = view.findViewById(R.id.dv_5);
            DialerKeyView dv6 = view.findViewById(R.id.dv_6);
            DialerKeyView dv7 = view.findViewById(R.id.dv_7);
            DialerKeyView dv8 = view.findViewById(R.id.dv_8);
            DialerKeyView dv9 = view.findViewById(R.id.dv_9);
            DialerKeyView dvXing = view.findViewById(R.id.dv_xing);
            DialerKeyView dv0 = view.findViewById(R.id.dv_0);
            DialerKeyView dvJing = view.findViewById(R.id.dv_jing);
            ImageButton ibCall = view.findViewById(R.id.ib_call);
            ImageButton ibVideo = view.findViewById(R.id.ib_video);
//            Window window = dialog.getWindow();
//            window.setWindowAnimations(R.style.AnimBottom);

            listener.getDigitsView(digits);//把此控件传递到外部

            digits.setOnClickListener(view1 -> {
                listener.itemClick(digits, digits.getText().toString());
            });
            deleteButton.setOnClickListener(view1 -> {
                listener.itemClick(deleteButton, "");
            });
            ibCall.setOnClickListener(view1 -> {
                listener.itemClick(ibCall, "");
                try {
                    if (null!=dialog)dialog.dismiss();
                }catch (Exception e){
                    JNLogUtil.e("==============", e);
                }

            });

            ibVideo.setOnClickListener(view1 -> {
                listener.itemClick(ibVideo, "");
                try {
                    if (null!=dialog)dialog.dismiss();
                }catch (Exception e){
                    JNLogUtil.e("==============", e);
                }
            });

            dv1.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv1, motionEvent);
                return false;
            });
            dv2.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv2, motionEvent);
                return false;
            });
            dv3.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv3, motionEvent);
                return false;
            });
            dv4.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv4, motionEvent);
                return false;
            });
            dv5.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv5, motionEvent);
                return false;
            });
            dv6.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv6, motionEvent);
                return false;
            });
            dv7.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv7, motionEvent);
                return false;
            });
            dv8.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv8, motionEvent);
                return false;
            });
            dv9.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv9, motionEvent);
                return false;
            });
            dvXing.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dvXing, motionEvent);
                return false;
            });
            dv0.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dv0, motionEvent);
                return false;
            });
            dvJing.setOnTouchListener((view1, motionEvent) -> {
                listener.itemTouch(dvJing, motionEvent);
                return false;
            });

            dvXing.setOnLongClickListener(view1 -> {
                listener.itemLongClick(dvXing);
                return true;
            });
            dv0.setOnLongClickListener(view1 -> {
                listener.itemLongClick(dv0);
                return true;
            });
            dvJing.setOnLongClickListener(view1 -> {
                listener.itemLongClick(dvJing);
                return true;
            });
            deleteButton.setOnLongClickListener(view1 -> {
                listener.itemLongClick(deleteButton);
                return true;
            });
            digits.setOnLongClickListener(view1 -> {
                listener.itemLongClick(digits);
                return false;
            });

            popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT,true);
            popupWindow.setOutsideTouchable(true);
            popupWindow.setAnimationStyle(R.style.AnimBottom);
            popupWindow.showAtLocation(parentView,Gravity.BOTTOM,0,px);
        }
    }

    public static void hideKeyBoardDialog(){
        try {
            if (null!=dialog)dialog.dismiss();
        }catch (Exception e){
            JNLogUtil.e("==============", e);
        }
        try {
            popupWindow.dismiss();
        }catch (Exception e){
            JNLogUtil.e("==============", e);
        }
    }

    public static void showWebDialog(Context context, String title, String message, DialogListener listener) {
        //增加验证消息弹出框
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        dialog = builder.create();

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_web, null);
        TextView titleTv = view.findViewById(R.id.dialog_title);
        TextView messageTv = view.findViewById(R.id.dialog_content);
        LinearLayout bottomRoot = view.findViewById(R.id.imq_l5);
        bottomRoot.setVisibility(View.VISIBLE);
        Button confirm = view.findViewById(R.id.dialog_confirm);
        Button cancle = view.findViewById(R.id.dialog_cannel);
        titleTv.setText(title);
        messageTv.setText(message);

        messageTv.setOnClickListener(view1 -> {
            dialog.dismiss();
            listener.confirmCallBack("");
        });
        confirm.setOnClickListener(view1 -> {
            dialog.dismiss();
            listener.confirmCallBack("");
        });

        cancle.setOnClickListener(view1 -> {
            dialog.dismiss();
        });

        dialog.setView(view);
        try {
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 5000);

//        Window window = dialog.getWindow();
//        window.getDecorView().setPadding(200, 200, 200, 200);
//        window.setGravity(Gravity.CENTER);
//        WindowManager.LayoutParams lp = window.getAttributes();
//        lp.width = WindowManager.LayoutParams.FILL_PARENT;
//        lp.height = WindowManager.LayoutParams.FILL_PARENT;
//        window.setAttributes(lp);

    }

    public interface DialogListener {

        void confirmCallBack(String input);

        void cancelCallBack();
    }

    public static void showToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }
}

package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jnbaseutils.chat.MeetingInviteMsg;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.eventbus.RefreshEvent;
import com.jndv.jnbaseutils.ui.base.JNBaseFragment;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.MQTTManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcchatlistBinding;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCCreatSessionApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCChatActivity;
import com.jndv.jndvchatlibrary.ui.chat.activity.TTSListActivity;
import com.jndv.jndvchatlibrary.ui.chat.base.JCchatListAdapter;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCsessionListModelItem;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMeetingBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgNotifyContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgTextContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgWebNoticeContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCCreateSessionUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCNotifyType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCMessageListener;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCmessageListenerManager;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGetGroupAllApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCGroupAllBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.utils.JCFriendUtil;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.yanzhenjie.recyclerview.OnItemClickListener;
import com.yanzhenjie.recyclerview.OnItemMenuClickListener;
import com.yanzhenjie.recyclerview.SwipeMenu;
import com.yanzhenjie.recyclerview.SwipeMenuBridge;
import com.yanzhenjie.recyclerview.SwipeMenuCreator;
import com.yanzhenjie.recyclerview.SwipeMenuItem;
import com.yanzhenjie.recyclerview.SwipeRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * 会话列表fragment
 */
public class JCchatListFragment extends JCbaseFragment implements OnRefreshLoadMoreListener {

    private static final String TAG = "JCchatListFragment";
    private JCchatListViewModel jCchatListViewModel;
    private FragmentJcchatlistBinding binding;
    private RecyclerView.LayoutManager mLayoutManager;
    private JCchatListAdapter adapter;
    private List<JCSessionListBean> dataBeanList;
    List<JCSessionListBean> dataBeanListAll = new ArrayList<>();
    private int page = 1;
    String mMsgId = "-1";

    String fragmentId = "";
    JCMessageListener mJCMessageListenerAll ;
    MQTTManager.MqttMsgListener mMqttMsgListenerAll;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentId = UUID.randomUUID().toString();
        init();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        jCchatListViewModel =
                new ViewModelProvider(this).get(JCchatListViewModel.class);
        binding = FragmentJcchatlistBinding.inflate(inflater, container, false);
        EventBus.getDefault().register(this);
        JNLogUtil.d("JCmessageListenerManager", "002==002==creatSession: ");

        return binding.getRoot();
    }

    public void changeTopMenu(boolean show){
        if (show){
            binding.llTopMenu.setVisibility(View.VISIBLE);
        }else {
            binding.llTopMenu.setVisibility(View.GONE);
        }
    }

    private void init() {
        jCchatListViewModel.init(this);
        toolInitObj();
        observeData();

        JCMessageListener mJCMessageListener = new JCMessageListener() {
            @Override
            public void onReceiveMessage(JCbaseIMMessage message, int isLook) {
                Log.d("PjSipMessageListener", "JCchatListFragment...msg==" + message.getContent());
                for (JCSessionListBean bean : dataBeanList) {
                    if (TextUtils.equals(bean.getType(), JCSessionType.MEETING)) {
                        MeetingInviteMsg.MEETING_INVITE_MSG = message.getContent();
                        bean.setFirst_content(message.getContent());
                        break;
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onSendMessageState(JCbaseIMMessage message, int state) {

            }
        };
        JCmessageListenerManager.addMessageListener("meeting_session", mJCMessageListener);
                JNLogUtil.d("JCmessageListenerManager", "002==001==creatSession: "+fragmentId);
        mJCMessageListenerAll = new JCMessageListener() {

            @Override
            public void onReceiveMessage(JCbaseIMMessage message, int isLook) {
                Log.d(TAG, "==dataBeanList.size()==creatSession==0=01=" + new Gson().toJson(message));
                Log.d(TAG, "==dataBeanList.size()==creatSession==0=010=" + dataBeanList.size());
                Log.d(TAG, "==dataBeanList.size()==creatSession==0=011=" + dataBeanListAll.size());
                initReceiveMessage(message, isLook);
            }

            @Override
            public void onSendMessageState(JCbaseIMMessage message, int state) {
                Log.e(TAG, "-----------onSendMessageState------------");
            }
        };
        JCmessageListenerManager.addMessageListenerAll(mJCMessageListenerAll);
        mMqttMsgListenerAll = new MQTTManager.MqttMsgListener() {
            @Override
            public void onReceiveMessage(JCbaseIMMessage message, int isLook) {
                String msgContent = new String(Base64.decode(message.getContent(),Base64.NO_WRAP));
                JCMsgWebNoticeContentBean contentInfo = new Gson().fromJson(msgContent,JCMsgWebNoticeContentBean.class);
                Log.e(TAG, "onReceiveMessage: ====confirmCallBack==========" + new Gson().toJson(contentInfo));
                EventBus.getDefault().post(new JNCodeEvent<>(JNEventBusType.CODE_POP_UP_NOTIFY_MSG, "",contentInfo));

//                if (TextUtils.equals(JCmessageType.NOTIFY, message.getMsgType())) {
//                    JCMessageUtils.initNotifyMsg(message);
//                }
                Log.d(TAG, "==dataBeanList.size()==creatSession==0=02=" + dataBeanList.size());
                initReceiveMessage(message, isLook);
            }
        };
        MQTTManager.getInstance().addMqttMsgListenerListAll(mMqttMsgListenerAll);
    }

    private void initReceiveMessage(JCbaseIMMessage message, int isLook){
        if (TextUtils.equals(message.getSessionType(), JCSessionType.SESSION_CLUSTER_INTERCOM)){
            if (TextUtils.equals(message.getMsgType(), JCmessageType.MSG_INTERCOM_308)){
                for (int j = 0; j < dataBeanList.size(); j++) {
                    JCSessionListBean intercomBean = dataBeanList.get(j);
                    if (TextUtils.equals(intercomBean.getType(), JCSessionType.SESSION_CLUSTER_INTERCOM)) {
                        int num = 0 ;
                        try {
                            num = intercomBean.getUnread_number();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        intercomBean.setUnread_number(num+1);
                        intercomBean.setFirst_content(new Gson().toJson(message));
                    }
                    dataBeanList.set(j, intercomBean);
                    adapter.notifyItemChanged(j);
                    JCobjectBox.get().boxFor(JCSessionListBean.class).put(intercomBean);
                }
            }
            //                    这里对讲模块的消息暂时不处理
            return;
        }
        String sessionId = message.getSessionId();
        Log.d(TAG, "msgId===" + message.getMsgID());
        //过滤重复消息
        if (TextUtils.equals(mMsgId, message.getMsgID())) {
            return;
        }
        mMsgId = message.getMsgID();

        Log.e(TAG, "-----------onReceiveMessage------------" + isLook);
        Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
        QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
        //0:未查看消息
        if (0 == isLook) {
            try {
//                        获取本地消息
                if (!TextUtils.equals(JCmessageType.NOTIFY, message.getMsgType())) {
                    JCimMessageBean jCimMessageBean = (JCimMessageBean) message;
                    JCimMessageBean messageBean = builder.equal(JCimMessageBean_.msgID
                                    , message.getMsgID(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .build().findFirst();
                    if (null != messageBean) {
                        jCimMessageBean.setId(messageBean.getId());
                    } else {
                        jCimMessageBean.setId(0);
                    }
                    Log.e(TAG, "onReceiveMessage: ==================getId==" + jCimMessageBean.getId());
                    if (TextUtils.isEmpty(jCimMessageBean.getFromName())) {
                        //消息外层from_name=null,里层from_name!=null
                        String decodeContent = new String(Base64.decode(jCimMessageBean.getContent(), Base64.NO_WRAP));
                        JCMsgTextContentBean jcMsgTextContentBean = new Gson().fromJson(decodeContent, JCMsgTextContentBean.class);
                        String fromName = jcMsgTextContentBean.getFromName();
                        jCimMessageBean.setFromName(fromName);
                    }
                    JCobjectBox.get().boxFor(JCimMessageBean.class).put(jCimMessageBean);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //是通知消息，且是单聊通知
        if (TextUtils.equals(JCmessageType.NOTIFY, message.getMsgType())) {
            JCMsgNotifyContentBean bean = new Gson().fromJson(new String(Base64.decode(message.getContent(), Base64.NO_WRAP)), JCMsgNotifyContentBean.class);
            if (TextUtils.equals(bean.getType(), "" + JCNotifyType.NOTIFY_SingleNotice)) {
                sessionId = "0";
            } else if (TextUtils.equals(bean.getType(), "" + JCNotifyType.NOTIFY_CreateGroupSucced)) {
                JNLogUtil.e("==init==addMessageListenerAll==NOTIFY==NOTIFY_CreateGroupSucced=======");
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_NET, "创建群组"));
                return;
            } else if (TextUtils.equals(bean.getType(), "" + JCNotifyType.NOTIFY_NewGroupMemberAdd)) {
                JNLogUtil.e("==init==addMessageListenerAll==NOTIFY==NOTIFY_NewGroupMemberAdd=======");
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_NET, "加入群组"));
                return;
            }
        }
        try {

            Log.d(TAG, "==dataBeanList.size()==creatSession==1==" + dataBeanList.size());
            for (int i = 0; i < dataBeanList.size(); i++) {
                JCSessionListBean dataBean = dataBeanList.get(i);
                JNLogUtil.e(TAG, "======00==creatSession==dataBeanList==index==" + i + "==sessionId==" + sessionId + "++++" + dataBean.getOther_id() + "++++" + dataBean.getOther_name());
                JNLogUtil.e(TAG, "======00==creatSession==getFromRealm==" + message.getFromRealm() + "++domain_addr++" + dataBean.getSession_party_domain_addr());
                if (TextUtils.equals(sessionId, dataBean.getOther_id())){
                    JNLogUtil.e(TAG, "===newSession===00==creatSession==dataBeanList==" + sessionId + "++++" + dataBean.getOther_id() + "++++" + dataBean.getOther_name());
                    JNLogUtil.e(TAG, "===newSession===00==creatSession==getFromRealm==" + message.getFromRealm() + "++domain_addr++" + dataBean.getSession_party_domain_addr());
                }
                if (TextUtils.equals(sessionId, dataBean.getOther_id())
                        && TextUtils.equals(message.getFromRealm(),dataBean.getSession_party_domain_addr())) {
                    boolean isRefresh = true;
                    JNLogUtil.e(TAG, "======01==creatSession==MsgType==" + message.getMsgType());

                    if (TextUtils.equals(JCmessageType.NOTIFY, message.getMsgType())) {
                        JCMsgNotifyContentBean bean = new Gson().fromJson(new String(Base64.decode(message.getContent(), Base64.NO_WRAP)), JCMsgNotifyContentBean.class);
                        JNLogUtil.e(TAG, "======02==creatSession==Type==" + bean.getType());
                        if (TextUtils.equals(bean.getType(), "" + JCNotifyType.NOTIFY_msgWithdraw)) {
                            JCimMessageBean jCimMessageBean = new Gson().fromJson(dataBean.getFirst_content(), JCimMessageBean.class);
                            JNLogUtil.e(TAG, "======03=creatSession==jCimMessageBean=MsgID==" + jCimMessageBean.getMsgID());
                            JNLogUtil.e(TAG, "======03=creatSession==bean.getText()==" + bean.getText());
                            if (TextUtils.equals(jCimMessageBean.getMsgID(), bean.getText())) {
                                jCimMessageBean.setMsgType(JCmessageType.WITHDRAW);
                                message = jCimMessageBean;
                            } else {
                                isRefresh = false;
                            }
                        }
                    }
                    if (isRefresh) {
                        dataBean.setFirst_content(new Gson().toJson(message));

                        if (0 == isLook) {
                            JNLogUtil.e(TAG, "==Withdraw==creatSession==01======");
                            if (TextUtils.equals(JCmessageType.WITHDRAW, message.getMsgType())) {
                                JNLogUtil.e(TAG, "==Withdraw==creatSession==02======");
//                                        如果是撤回消息，这里未读数量应该不需要改动
                            } else {
                                dataBean.setUnread_number(dataBean.getUnread_number() + 1);
                            }
                        }
                        if (!TextUtils.equals(sessionId, "0")) {
                            long sendtime = System.currentTimeMillis();
                            try {
                                sendtime = Long.parseLong(message.getSendTime());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            dataBean.setLast_time(sendtime);
                        }

                        Box<JCSessionListBean> sessionBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                        if (dataBean.getId() <= 0) {
                            QueryBuilder<JCSessionListBean> sessionbuilder = sessionBox.query();
                            JCSessionListBean messageBean = sessionbuilder.equal(JCSessionListBean_.session_id
                                    , dataBean.getSession_id()).build().findFirst();
                            if (null != messageBean) {
                                dataBean.setId(messageBean.getId());
                            }
                        }
                        sessionBox.put(dataBean);
                        if (1 == dataBean.getIs_top()) {
                            dataBeanList.remove(i);
                            dataBeanList.add(0, dataBean);
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        } else
                            for (int j = 0; j < dataBeanList.size(); j++) {
                                JNLogUtil.e("==JCMessageListener==creatSession==Is_top==j==" + j);
                                JNLogUtil.e("==JCMessageListener==creatSession==Is_top==top==" + dataBeanList.get(j).getIs_top());
                                if (1 != dataBeanList.get(j).getIs_top()) {
                                    dataBeanList.remove(i);
                                    dataBeanList.add(j, dataBean);
                                    if (adapter != null) {
                                        adapter.notifyDataSetChanged();
                                    }
                                    return;
                                }
                            }
                    }
                    return;
                }
            }

            JNLogUtil.e(TAG, "onReceiveMessage: =====creatSession===JCSessionListBean======003=======");
            if (TextUtils.equals(message.getSessionType(),JCSessionType.CHAT_PERSION)
                    || TextUtils.equals(message.getSessionType(),JCSessionType.CHAT_SYSTEM))
                creatSession(message, isLook);
            else {
                JNLogUtil.e(TAG, "onReceiveMessage: =====creatSession===JCGetGroupAllApi======003=======");
                JCbaseIMMessage imMessage = message;
                EasyHttp.get(JCchatListFragment.this)
                        .api(new JCGetGroupAllApi()
                                .setDomainAddr(JNBasePreferenceSaves.getSipAddress())
                                .setGroupId(message.getSessionId())
                                .setUserId(JNBasePreferenceSaves.getUserSipId())
                                .setGroupDomainaddr(message.getSessionRealm())
                        )
                        .request(new OnHttpListener<JCRespondBean<JCGroupAllBean>>() {
                            @Override
                            public void onSucceed(JCRespondBean<JCGroupAllBean> result) {
                                JCThreadManager.onMainHandler(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            JNLogUtil.e(TAG, "onReceiveMessage: ==creatSession==getCgName==" + result.getData().getGroup_info().getCgName());
                                            JNLogUtil.e(TAG, "onReceiveMessage: ==creatSession==getCgPic==" + result.getData().getGroup_info().getCgPic());
                                            creatSession(imMessage, isLook, imMessage.getSessionId()
                                                    , imMessage.getSessionRealm(), imMessage.getSessionRealmJava()
                                                    , imMessage.getSessionType()
                                                    , result.getData().getGroup_info().getCgName()
                                                    , result.getData().getGroup_info().getCgPic());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                            }

                            @Override
                            public void onFail(Exception e) {
                                e.printStackTrace();
                                Log.e(TAG, "onFail: " + e.getMessage());
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void creatSession(JCbaseIMMessage message, int isLook) {
        creatSession(message, isLook, message.getSessionId()
                , message.getFromRealm(), message.getFromRealmJava(), message.getSessionType()
                , message.getFromName(), message.getFromHead());
    }

    private void creatSession(JCbaseIMMessage message, int isLook, String sessionPartyId
            , String sessionPartyDomainAddr, String sessionPartyDomainAddrUser, String type
            , String sessionPartyName, String sessionPartyPic) {
        Log.d("JCCreateSessionUtils", "creatSession...111");
        JCCreateSessionUtils.creatSession(getActivity(), sessionPartyId
                , sessionPartyDomainAddr, sessionPartyDomainAddrUser, type
                , sessionPartyName, sessionPartyPic, new OnHttpListener<JCCreatSessionApi.Bean>() {
                    @Override
                    public void onSucceed(JCCreatSessionApi.Bean result) {
                        try {
//                            这里应该是想办法获取到新创建的会话信息
                            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                                JCChatManager.showToast("创建成功！");
                                JNLogUtil.e("==session==creatSession==01==");
                                JCThreadManager.onMainHandler(new Runnable() {
                                    @Override
                                    public void run() {
                                        JNLogUtil.e("==session==creatSession==02==");
                                        if (null != result.getData()) {
                                            JNLogUtil.e("==session==creatSession==03==");
                                            initSessionList(result.getData(), message, isLook);
                                        }
                                    }
                                });
                            } else {
                                JCChatManager.showToast("创建失败！");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        JCChatManager.showToast("创建失败！");
                    }
                });
    }

    private void initSessionList(JCSessionListBean sessionListBean, JCbaseIMMessage message, int isLook) {
        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        JNLogUtil.e("==session==creatSession==04==");
        sessionListBean.setFirst_content(new Gson().toJson(message));
        long sendtime = System.currentTimeMillis();
        try {
            sendtime = Long.parseLong(message.getSendTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        JNLogUtil.e("==session==creatSession==05==");
        sessionListBean.setLast_time(sendtime);
        if (0 == isLook) sessionListBean.setUnread_number(sessionListBean.getUnread_number() + 1);
        sessionListBean.setId(0);
        JNLogUtil.e("==session==creatSession==06==");
        imMsgBox.put(sessionListBean);
        JNLogUtil.e("==session==creatSession==07==");
        if (dataBeanList.size() == 0) {
            JNLogUtil.e("==session==creatSession==08==");
            dataBeanList.add(sessionListBean);
            adapter.notifyDataSetChanged();
        } else {
            JNLogUtil.e("==session==creatSession==09==");
            if (1 == sessionListBean.getIs_top()) {
                JNLogUtil.e("==session==creatSession==10==");
//                dataBeanList.remove(j);
                dataBeanList.add(0, sessionListBean);
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            } else {
                JNLogUtil.e("==session==creatSession==11==");
                for (int j = 0; j < dataBeanList.size(); j++) {
                    if (1 != dataBeanList.get(j).getIs_top()) {
                        dataBeanList.add(j, sessionListBean);
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }
                        return;
                    }
                }
                dataBeanList.add(sessionListBean);
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void observeData() {
        jCchatListViewModel.getSessionList().observeForever(new Observer<JCsessionListModelItem>() {
            @Override
            public void onChanged(JCsessionListModelItem jCsessionListModelItem) {
                try {
                    if (null != jCsessionListModelItem && jCsessionListModelItem.getType() == 0) {
                        if (1 == jCsessionListModelItem.getPage()) {
                            dataBeanListAll.clear();
                        }
                        dataBeanListAll.addAll(jCsessionListModelItem.getDatas());
                        if (null == jCsessionListModelItem.getDatas() || jCsessionListModelItem.getDatas().size() < JCChatManager.pageSizeSession) {
                            dataBeanListAll = jCchatListViewModel.sortData(dataBeanListAll);
                            dataBeanList.clear();
                            dataBeanList.addAll(dataBeanListAll);
                            adapter.notifyDataSetChanged();
                            binding.listRefresh.finishRefresh(1000);
                            binding.listRefresh.finishLoadMore(1000);
                            binding.listRefresh.setEnableLoadMore(false);
                        } else {
//                            binding.listRefresh.setEnableLoadMore(true);
                        }
                    } else {
                        binding.listRefresh.finishRefresh(1000);
                        binding.listRefresh.finishLoadMore(1000);
                    }
                } catch (Exception e) {
                    JNLogUtil.e(TAG, "Exception e: ", e);
                    try {
                        binding.listRefresh.finishRefresh(1000);
                        binding.listRefresh.finishLoadMore(1000);
                    } catch (Exception e1) {
                        JNLogUtil.e(TAG, "Exception e1: ", e1);
                    }
                }
            }
        });
        jCchatListViewModel.getOperateeData().observeForever(new Observer<JCchatListViewModel.OperateItem>() {
            @Override
            public void onChanged(JCchatListViewModel.OperateItem operateItem) {
                JNLogUtil.e("==getOperateeData==onChanged==01==");
                if (null != operateItem) {
                    JNLogUtil.e("==getOperateeData==onChanged==02==" + operateItem.getState());
                    if (0 == operateItem.getState()) {
                        if (0 == operateItem.getType()) {
//                            去删除列表中数据
                            delete(operateItem.getMsg());
                        } else if (1 == operateItem.getType()) {
                            JNLogUtil.e("==getOperateeData==onChanged==03==" + operateItem.getMsg());
//                            置顶
//                            goUp(operateItem.getMsg());
                            try {
                                String[] s = operateItem.getMsg().split(",");
                                String sid = s[0];
                                int top = 0;
                                int nodisturbing = 0;
                                try {
                                    top = Integer.parseInt(s[1]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    nodisturbing = Integer.parseInt(s[2]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                JNLogUtil.e("==getOperateeData==onChanged==04==");
                                JCSessionListBean messageBean = new JCSessionListBean();
                                messageBean.setSession_id(Integer.parseInt(sid));
                                messageBean.setIs_top(top);
                                messageBean.setIs_no_disturb(nodisturbing);
                                updateTopInfo(messageBean);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * 这里使用粘性事件接收
     *
     * @param codeEvent
     */
    @SuppressLint("SuspiciousIndentation")
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void codeEvent(JNCodeEvent codeEvent) {
        Log.d(TAG,"codeEvent..."+codeEvent.getCode());
        switch (codeEvent.getCode()) {
            case JNEventBusType
                    .CODE_SESSION_LIST_UPDATE_MSG://消息更新
                String sid = codeEvent.getMessage();
                JNLogUtil.e("=JCmessageListenerManager==CODE_SESSION_LIST_UPDATE_MSG===sid==" + sid);
                try {
                    for (int i = 0; i < dataBeanList.size(); i++) {
                        JCSessionListBean dataBean = dataBeanList.get(i);
                        if (TextUtils.equals(sid, "" + dataBean.getSession_id())) {
                            String content = "";
                            if (null != codeEvent.getData())
                                content = new Gson().toJson((JCimMessageBean) codeEvent.getData());
                            JNLogUtil.e("======content==" + content);
                            dataBean.setFirst_content(content);
                            long sendtime = System.currentTimeMillis();
                            try {
                                if (null != codeEvent.getData())
                                    sendtime = Long.parseLong(((JCimMessageBean) codeEvent.getData()).getSendTime());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (null != codeEvent.getData()) dataBean.setLast_time(sendtime);
                            Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                            if (dataBean.getId() <= 0) {
                                QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
                                JCSessionListBean messageBean = builder.equal(JCSessionListBean_.session_id, dataBean.getSession_id()).build().findFirst();
                                if (null != messageBean) {
                                    dataBean.setId(messageBean.getId());
                                }
                            }
                            imMsgBox.put(dataBean);
                            if (1 == dataBean.getIs_top()) {
                                dataBeanList.remove(i);
                                dataBeanList.add(0, dataBean);
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }
                            } else
                                for (int j = 0; j < dataBeanList.size(); j++) {
                                    if (1 != dataBeanList.get(j).getIs_top()) {
                                        dataBeanList.remove(i);
                                        dataBeanList.add(j, dataBean);
                                        if (adapter != null) {
                                            adapter.notifyDataSetChanged();
                                        }
                                        return;
                                    }
                                }
                            return;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case JNEventBusType
                    .CODE_SESSION_LIST_UPDATE_NONUM://消息更新
                String sid2 = codeEvent.getMessage();
                JNLogUtil.e(TAG, "==UPDATE_NONUM=================sid2==" + sid2);
                try {
                    for (int i = 0; i < dataBeanList.size(); i++) {
                        JCSessionListBean dataBean = dataBeanList.get(i);
                        if (TextUtils.equals(sid2, "" + dataBean.getSession_id())) {
                            dataBean.setUnread_number(0);
                            Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                            Log.d(TAG, "dataBean.getStatus(): " + dataBean.getStatus());
                            if (dataBean.getId() <= 0) {
                                QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
                                JCSessionListBean messageBean = builder.equal(JCSessionListBean_.session_id, dataBean.getSession_id()).build().findFirst();
                                if (null != messageBean) {
                                    dataBean.setId(messageBean.getId());
                                }
                            }
                            imMsgBox.put(dataBean);
                            if (adapter != null) {
                                adapter.notifyItemChanged(i);
                            }
                            return;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case JNEventBusType
                    .CODE_SESSION_LIST_UPDATE_INFO://
                updateTopInfo((JCSessionListBean) codeEvent.getData());
                break;
            case JNEventBusType.CODE_SESSION_LIST_UPDATE_NET://
                try {
                    JCFriendUtil.initGetFriendData(this);
                    binding.listRefresh.autoRefresh();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case JNEventBusType.CODE_SIP_CONNECT_OK://
                break;
        }
    }

    /**
     * 对应场景:从通知列表退出后，需要更新下通知消息的未读数量为0
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefreshEvent(RefreshEvent event){
        Log.d("zhang","onRefreshEvent...");
        getChatListData();
    }

    private void updateTopInfo(JCSessionListBean newJCSessionListBean) {
        JNLogUtil.e("==getOperateeData==onChanged==05==");
        try {
            boolean isTop = false;
            for (int i = 0; i < dataBeanList.size(); i++) {
                JCSessionListBean dataBean = dataBeanList.get(i);
                if (JCSessionUtil.isEquals(dataBean, newJCSessionListBean)) {
                    if (dataBean.getIs_top() != newJCSessionListBean.getIs_top()) {
                        isTop = true;
                    }
                    dataBean.setIs_top(newJCSessionListBean.getIs_top());
                    dataBean.setIs_no_disturb(newJCSessionListBean.getIs_no_disturb());
                    dataBean.setStatus(newJCSessionListBean.getStatus());
                    dataBean.setFirst_time(newJCSessionListBean.getFirst_time());
                    Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                    if (dataBean.getId() <= 0) {
                        if (newJCSessionListBean.getId() <= 0) {
                            QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
                            JCSessionListBean messageBean = builder.equal(JCSessionListBean_.session_id, dataBean.getSession_id()).build().findFirst();
                            if (null != messageBean) {
                                dataBean.setId(messageBean.getId());
                            }
                        } else {
                            dataBean.setId(newJCSessionListBean.getId());
                        }
                    }
                    imMsgBox.put(dataBean);
                    Log.e(TAG, "codeEvent: =================isTop=" + isTop);
                    if (adapter != null) {
                        if (!isTop) {
                            adapter.notifyItemChanged(i);
                        }
                    }
                    break;
                }
            }
            if (adapter != null) {
                if (isTop) {
                    dataBeanList = jCchatListViewModel.sortData(dataBeanList);
                    adapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新本地数据库的通知消息,未读消息数量有以下两种操作:
     * 1.+1递增
     * 2.置为0
     * @param isReset true:代表清空置为0  false:代表递增+1
     * @param type 0:代表对讲
     */
//    private void updateSessionUnreadNumb(boolean isReset, int type){
//        Log.d(TAG,"updateSessionUnreadNumb...");
//        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
//        QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
//        if (0==type){
//            JCSessionListBean messageBean=builder.equal(JCSessionListBean_.session_id, 0).build().findFirst();
//            messageBean.setUnread_number(isReset?0:messageBean.getUnread_number()+1);
//        }
//
//        imMsgBox.put(messageBean);
//    }
    /**
     * 更新本地数据库的通知消息,未读消息数量有以下两种操作:
     * 1.+1递增
     * 2.置为0
     * @param isReset true:代表清空置为0  false:代表递增+1
     */
    private void updateNotifyMsg(boolean isReset){
        Log.d(TAG,"updateNotifyMsg...");
        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
        JCSessionListBean messageBean=builder.equal(JCSessionListBean_.session_id, 0).build().findFirst();
        messageBean.setUnread_number(isReset?0:messageBean.getUnread_number()+1);
        imMsgBox.put(messageBean);
    }

    /**
     * 初始化对象
     */
    private void toolInitObj() {
        dataBeanList = new ArrayList<>();

        binding.recycleView.setSwipeMenuCreator(swipeMenuCreator);
        binding.recycleView.setOnItemMenuClickListener(mMenuItemClickListener);
        binding.recycleView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int adapterPosition) {
                if (JCSessionType.CHAT_SYSTEM_NOTIFY.equals(dataBeanList.get(adapterPosition).getType())) {
                    updateNotifyMsg(true);
                    JCbaseFragmentActivity.start(getContext(), new JCMsgNotifyListFragment(), "通知列表");
                } else if (JCSessionType.CHAT_TTS.equals(dataBeanList.get(adapterPosition).getType())) {
                    Intent intent = new Intent(getActivity(), TTSListActivity.class);
                    startActivity(intent);
                } else if (JCSessionType.SESSION_CLUSTER_INTERCOM.equals(dataBeanList.get(adapterPosition).getType())) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("type", 1);
                    JNBaseUtilManager.startActivityIntercom(getActivity(), "频道邀请列表", bundle, new JNBaseFragment.FragmentSelect() {
                        @Override
                        public void onSelecte(int type, Object... objects) {

                        }
                    }, true);
                    JCSessionListBean jcSessionListBean = dataBeanList.get(adapterPosition);
                    jcSessionListBean.setUnread_number(0);
                    dataBeanList.set(adapterPosition, jcSessionListBean);
                    adapter.notifyItemChanged(adapterPosition);
                    JCobjectBox.get().boxFor(JCSessionListBean.class).put(jcSessionListBean);
                } else if (JCSessionType.MEETING.equals(dataBeanList.get(adapterPosition).getType())) {
                    if (TextUtils.isEmpty(dataBeanList.get(adapterPosition).getFirst_content())) {
                        JCChatManager.showToast("暂无会议消息");
                        return;
                    }
                    String decodeContent = new String(Base64.decode(dataBeanList.get(adapterPosition).getFirst_content(), Base64.NO_WRAP));
                    JCMeetingBean jcMeetingBean = new Gson().fromJson(decodeContent, JCMeetingBean.class);
                    JNBaseUtilManager.jumpToMeetingMsgDetailActivity(getActivity(), jcMeetingBean.extend, jcMeetingBean.meetCode);
                } else if (null == JCChatManager.getmAllOperateListener()) {
                    Log.e(TAG, "onItemClick: =====" + new Gson().toJson(dataBeanList.get(adapterPosition)));
                    JCChatActivity.start(getActivity(), dataBeanList.get(adapterPosition));
                } else {
                    boolean operate = JCChatManager.getmAllOperateListener().chatListOnClickItem(dataBeanList.get(adapterPosition));
                    if (!operate)
                        JCChatActivity.start(getActivity(), dataBeanList.get(adapterPosition));
                }

            }
        });

        mLayoutManager = createLayoutManager();
        binding.recycleView.setLayoutManager(mLayoutManager);
        binding.listRefresh.setOnRefreshLoadMoreListener(this);
        adapter = new JCchatListAdapter(getContext(), dataBeanList);
        binding.recycleView.setAdapter(adapter);

        binding.llMeet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JNBaseUtilManager.startActivityMeet(getActivity(), "会议",null, true);
            }
        });
        binding.llIntercom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: ==startActivityIntercom==");
                JNBaseUtilManager.startActivityIntercom(getActivity(), "对讲", null,null, true);
            }
        });
        binding.llTts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TTSListActivity.class);
                startActivity(intent);
            }
        });

        binding.llIvs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JNBaseUtilManager.startActivityMonitor(getActivity(), "监控", null, false);
            }
        });

    }

    /**
     * 菜单创建器，在Item要创建菜单的时候调用。
     */
    private SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
        @Override
        public void onCreateMenu(SwipeMenu swipeLeftMenu, SwipeMenu swipeRightMenu, int position) {
            int width = getResources().getDimensionPixelSize(R.dimen.dp_70);
//            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            // 1. MATCH_PARENT 自适应高度，保持和Item一样高;
            // 2. 指定具体的高，比如80;
            // 3. WRAP_CONTENT，自身高度，不推荐;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            String topText = "置顶";
            try {
                if (1 == dataBeanList.get(position).getIs_top()) {
                    topText = "取消置顶";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            SwipeMenuItem addItem = new SwipeMenuItem(getContext()).setBackground(R.drawable.selector_green)
                    .setText(topText)
                    .setTextColor(Color.WHITE)
                    .setWidth(width)
                    .setHeight(height);
            swipeRightMenu.addMenuItem(addItem); // 添加菜单到右侧。
            // 添加右侧的，如果不添加，则右侧不会出现菜单。
            SwipeMenuItem deleteItem = new SwipeMenuItem(getContext()).setBackground(R.drawable.selector_red)
//                    .setImage(R.drawable.ic_action_delete)
                    .setText("删除")
                    .setTextColor(Color.WHITE)
                    .setWidth(width)
                    .setHeight(height);
            swipeRightMenu.addMenuItem(deleteItem);// 添加菜单到右侧。
        }
    };

    /**
     * RecyclerView的Item的Menu点击监听。
     */
    private OnItemMenuClickListener mMenuItemClickListener = new OnItemMenuClickListener() {
        @Override
        public void onItemClick(SwipeMenuBridge menuBridge, int position) {
            menuBridge.closeMenu();

            int direction = menuBridge.getDirection(); // 左侧还是右侧菜单。
            int menuPosition = menuBridge.getPosition(); // 菜单在RecyclerView的Item中的Position。

            if (direction == SwipeRecyclerView.RIGHT_DIRECTION) {
                if (menuPosition == 1) {
                    jCchatListViewModel.deleteSession("" + dataBeanList.get(position).getSession_id());
                    Log.e(TAG, "onItemClick: 删除：" + position);
                } else if (menuPosition == 0) {
                    int top = 1;
                    if (1 == dataBeanList.get(position).getIs_top()) {
                        top = 0;
                    }
                    jCchatListViewModel.updateSession("" + dataBeanList.get(position).getSession_id(), top
                            , dataBeanList.get(position).getIs_no_disturb());
                    Log.e(TAG, "onItemClick: 置顶：" + position);
                }

            } else if (direction == SwipeRecyclerView.LEFT_DIRECTION) {
                Toast.makeText(getContext(), "list第" + position + "; 左侧菜单第" + menuPosition, Toast.LENGTH_SHORT)
                        .show();
            }
        }
    };
    //删除
    private void delete(String sid) {
        if (dataBeanList != null) {
            if (dataBeanList.size() > 0) {
                for (int i = 0; i < dataBeanList.size(); i++) {
                    if (TextUtils.equals(sid, "" + dataBeanList.get(i).getSession_id())) {
                        adapter.notifyItemRangeRemoved(i, 1);
                        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                        JCSessionListBean dataBean = dataBeanList.get(i);
                        if (dataBean.getId() <= 0) {
                            QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
                            JCSessionListBean messageBean = builder.equal(JCSessionListBean_.session_id, dataBean.getSession_id()).build().findFirst();
                            if (null != messageBean) {
                                dataBean.setId(messageBean.getId());
                            }
                        }
                        imMsgBox.remove(dataBean);
                        dataBeanList.remove(i);
                    }
                }

            }
        }
    }
    private void getChatListData() {
        jCchatListViewModel.getChatListData(page);
    }
    private RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(getContext());
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            binding = null;
            EventBus.getDefault().unregister(this);
            JCmessageListenerManager.removeMessageListenerAll(mJCMessageListenerAll);
        }catch (Exception e){}
        try {
            MQTTManager.getInstance().removeMqttMsgListenerListAll(mMqttMsgListenerAll);
        }catch (Exception e){}
//        JNLogUtil.d("JCmessageListenerManager", "002==002=onDestroyView=creatSession: "+fragmentId);

    }

    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        page++;
        getChatListData();
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        page = 1;
        getChatListData();
    }
}

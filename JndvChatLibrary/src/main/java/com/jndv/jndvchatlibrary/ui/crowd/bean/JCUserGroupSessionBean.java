package com.jndv.jndvchatlibrary.ui.crowd.bean;

/**
 * @Description
 * @Author SunQinzheng
 * @Time 2022/5/10 下午 1:52
 **/
public class JCUserGroupSessionBean {
    /**
     * id	number	会话id
     * session_party_id	number	会话对象(群聊i)
     * session_party_name	String	会话对象名称
     * session_party_pic	String	会话对象头像
     * session_party_domain_addr	String	会话对象所处域地址(sip)
     * session_party_domain_addr_user	String	会话对象所处域地址(java)
     * remarks	String	备注
     */

    private int id;
    private String session_party_pic;
    private String session_party_domain_addr;
    private String session_party_domain_addr_user;
    private String remark;
    private String session_party_name;
    private String session_party_id;
    /*
    * (0—-正常，1—-已解散(群聊)，2—-已退出(群聊)，3—-被踢出(群聊)，-1—-会话已删除，4—-非群成员并已删除会话)
    * */
    private int status;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSession_party_pic() {
        return session_party_pic;
    }

    public void setSession_party_pic(String session_party_pic) {
        this.session_party_pic = session_party_pic;
    }

    public String getSession_party_domain_addr() {
        return session_party_domain_addr;
    }

    public void setSession_party_domain_addr(String session_party_domain_addr) {
        this.session_party_domain_addr = session_party_domain_addr;
    }

    public String getSession_party_domain_addr_user() {
        return session_party_domain_addr_user;
    }

    public void setSession_party_domain_addr_user(String session_party_domain_addr_user) {
        this.session_party_domain_addr_user = session_party_domain_addr_user;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSession_party_name() {
        return session_party_name;
    }

    public void setSession_party_name(String session_party_name) {
        this.session_party_name = session_party_name;
    }

    public String getSession_party_id() {
        return session_party_id;
    }

    public void setSession_party_id(String session_party_id) {
        this.session_party_id = session_party_id;
    }
}

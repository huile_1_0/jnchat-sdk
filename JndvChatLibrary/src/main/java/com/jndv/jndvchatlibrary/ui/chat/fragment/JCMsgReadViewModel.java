package com.jndv.jndvchatlibrary.ui.chat.fragment;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.http.api.JCMessageReadUserApi;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCReadUserBean;


import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/11
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 聊天页面中，数据管理工具，获取本地、网络消息列表操作实现
 */
public class JCMsgReadViewModel extends ViewModel {

    private MutableLiveData<List<JCReadUserBean>> reads;
    private MutableLiveData<List<JCReadUserBean>> unreads;

    private String msgId;
    private Fragment fragment;

    public JCMsgReadViewModel() {
        reads = new MutableLiveData<>();
        unreads = new MutableLiveData<>();
    }

    public void init(String msgId, Fragment fragment){
        this.msgId = msgId ;
        this.fragment = fragment ;
    }
    public LiveData<List<JCReadUserBean>> getReadDataChange(){
        return reads;
    }
    public LiveData<List<JCReadUserBean>> getUnReadDataChange(){
        return unreads;
    }

    public void getMessageReadUser(){
        List<JCReadUserBean> jcReadUserBeans = new ArrayList<>();
        reads.setValue(jcReadUserBeans);
        unreads.setValue(jcReadUserBeans);
        EasyHttp.get(fragment)
                .api(new JCMessageReadUserApi()
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setGroupMsgUuid(msgId)
                )
                .request(new OnHttpListener<JCMessageReadUserApi.Bean>() {
                    @Override
                    public void onSucceed(JCMessageReadUserApi.Bean result) {
                        try {
                            if (null!=result && null!=result.getData()){
                                reads.setValue(result.getData().getRead());
                                unreads.setValue(result.getData().getUnread());
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFail(Exception e) {
//                        netFail();
                        e.printStackTrace();
                    }
                });
    }

}
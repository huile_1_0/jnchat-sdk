package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgCardContentBean;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCChatManageFragment;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/3/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 默认消息展示holder，兼容未定义的消息类型，显示：未知类型消息
 */
public class JCmsgViewHolderNameCard extends JCmsgViewHolderBase {

    private TextView tvName;
    private ImageView ivHead;

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_name_card;
    }

    @Override
    protected void inflateContentView() {
        ivHead = findViewById(R.id.iv_user_head);
        tvName = findViewById(R.id.tv_user_name);
    }

    @Override
    protected void bindContentView() {
        Log.e("bindContentView",message.getContent());
        JNLogUtil.e("======Content=="+base64ToString(message.getContent()));
        try {
            JCMsgCardContentBean bean = new Gson().fromJson(base64ToString(message.getContent()), JCMsgCardContentBean.class);
            JNLogUtil.e("======SendTime=="+message.getSendTime());
            JNLogUtil.e("======ReceiveTime=="+message.getReceiveTime());
            tvName.setText(bean.getName());
//            JCglideUtils.loadImage(context, bean.getHead(), ivHead);
            JCglideUtils.loadCircleImage(context, bean.getHead(), ivHead);
        }catch (Exception e){
            e.printStackTrace();
            JNLogUtil.e("======getMsgType=="+message.getMsgType());
            tvName.setText("未识别的消息："+message.getContent());
        }
    }

    @Override
    protected void onItemClick() {
        try {
            JCMsgCardContentBean bean = new Gson().fromJson(base64ToString(message.getContent()), JCMsgCardContentBean.class);
            JNLogUtil.e("======SendTime=="+message.getSendTime());
            JNLogUtil.e("======ReceiveTime=="+message.getReceiveTime());
            Bundle bundle = new Bundle();
            bundle.putString(JCChatManageFragment.INSIPID, bean.getuID());
            bundle.putString(JCChatManageFragment.CODE_INNAME, bean.getName());
            bundle.putString(JCChatManageFragment.CODE_INHEAD, bean.getHead());
            bundle.putString(JCChatManageFragment.INSIPADDRESS, bean.getDomainAddr());
            bundle.putString(JCChatManageFragment.INJAVAADDRESS, bean.getDomainAddrJava());
            JCChatManageFragment contentFragment = new JCChatManageFragment();
            JCbaseFragmentActivity.start(context, contentFragment, "用户信息", bundle);
        }catch (Exception e){
            JNLogUtil.e("====onItemClick==getMsgType=="+message.getMsgType(), e);
        }
    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
//        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_copy), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_transmit), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_collect), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_withdraw), 1));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_delete), 2));
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

}

package com.jndv.jndvchatlibrary.ui.chat.bean.content;

//import com.wgd.wgdfilepickerlib.bean.FileType;

/**
 * @ProjectName: JndvChat
 * @Package: com.jndv.jndvchatlibrary.ui.chat.bean.content
 * @ClassName: JCMsgFileContentBean
 * @Description: 文件消息内容类
 * @Author: SunQinzheng
 * @CreateDate: 2022/2/19 上午 11:25
 */
public class JCMsgFileContentBean {
    /**
     * "extend":"",//扩展字段（暂无意义）
     * "url":"",//文件地址
     * "size":"",//文件大小
     */
    private String name;
    private String url;
    private String path;
    private String size;
    private String  fileType;
//    private FileType fileType;

    public JCMsgFileContentBean(String name, String path, String size, String fileType) {
//    public JCMsgFileContentBean(String name, String path, String size, FileType fileType) {
        this.name = name;
        this.path = path;
        this.size = size;
        this.fileType = fileType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

//    public FileType getFileType() {
//        return fileType;
//    }
//
//    public void setFileType(FileType fileType) {
//        this.fileType = fileType;
//    }
    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}

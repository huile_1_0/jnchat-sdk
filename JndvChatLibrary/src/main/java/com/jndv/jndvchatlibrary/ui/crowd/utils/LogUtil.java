package com.jndv.jndvchatlibrary.ui.crowd.utils;

import android.util.Log;

/**
 * 日志工具类（对log的简单封装）
 */
public class LogUtil {

    private static final String TAG = "0723";
    private static boolean isDebug = true;

    // callback log tag
    public static final String JNI_CALLBACK = "JNI_CALLBACK";
    public static final String JNI_REQUEST = "JNI_REQUEST";
    public static final String JNISERVICE_CALLBACK = "JNISERVICE_CALLBACK";
    public static final String SERVICE_CALLBACK = "SERVICE_CALLBACK";
    public static final String UI_MESSAGE = "UI_MESSAGE";
    public static final String UI_BROADCAST = "UI_BROADCAST";
    public static final String UI_V2AV = "V2AV";

    // 下面是传入自定义tag的函数
    public static void v(String msg) {
        if (isDebug && msg != null)
            Log.v(TAG, msg);
    }

    public static void d(String msg) {
        if (isDebug && msg != null)
            Log.d(TAG, msg);
    }

    public static void i(String msg) {
        if (isDebug && msg != null)
            Log.i(TAG, msg);
    }

    public static void w(String msg) {
        if (isDebug && msg != null)
            Log.w(TAG, msg);
    }

    public static void e(String msg) {
        if (isDebug && msg != null)
            Log.e(TAG, msg);
    }

    // 下面是传入自定义tag的函数
    public static void v(String tag, String msg) {
        if (isDebug)
            Log.v(tag, msg);
    }

    public static void d(String tag, String msg) {
        if (isDebug && tag != null && msg != null)
            Log.d(tag, msg);
    }

    public static void i(String tag, String msg) {
        if (isDebug && tag != null && msg != null)
            Log.i(tag, msg);
    }

    public static void w(String tag, String msg) {
        if (isDebug && tag != null && msg != null)
            Log.w(tag, msg);
    }

    public static void e(String tag, String msg) {
        if (isDebug && tag != null && msg != null)
            Log.e(tag, msg);
    }

    public static void jniCall(String methodName, String content) {
        LogUtil.i(JNI_CALLBACK, "METHOD = " + methodName + " --> " + content);
        Log.d(JNI_CALLBACK, "---methodName:" + methodName);
        Log.d(JNI_CALLBACK, "---content:" + content);
    }

}

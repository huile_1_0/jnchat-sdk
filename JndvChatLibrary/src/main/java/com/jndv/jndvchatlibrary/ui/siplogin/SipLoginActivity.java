package com.jndv.jndvchatlibrary.ui.siplogin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.fragment.app.FragmentActivity;

import com.ehome.manager.JNPjSip;
import com.ehome.manager.utils.JNLogUtil;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.databinding.ActivitySipLoginBinding;
import com.jndv.jndvchatlibrary.eventbus.JCCodeEvent;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jndvchatlibrary.ui.chat.bean.flycontent.FlightAuthBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.flycontent.FlightParamBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.flyreceive.DroneFlightDataBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCMessageListener;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCmessageListenerManager;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JFmyPjSipMessageListener;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;
import com.jndv.jndvchatlibrary.ui.crowd.utils.MD5Util;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;

import com.wega.library.loadingDialog.LoadingDialog;

import org.greenrobot.eventbus.EventBus;

/**
 * 登录页
 */
public class SipLoginActivity extends FragmentActivity {

    private static final String TAG = "SipLoginActivity";
    private ActivitySipLoginBinding binding;
    private Activity activity;
    private LoadingDialog loadingDialog = null;
    private JNPjSip pjSip;
    private int loginState = 0 ;//0=未登录；1=sip注册成功；2=sip注册成功并验证成功
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            cancelLoadding();
            if (0==loginState) JCChatManager.showToast("登录失败！");
            else if (1==loginState) JCChatManager.showToast("注册成功但登录验证失败！");
        }
    };

    public static void start(Context context) {
        Intent intent = new Intent(context, SipLoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        viewFullScreen();
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(this);
        activity = SipLoginActivity.this;
        binding = ActivitySipLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initConfig();
        initView();
    }

    private void initView() {
        Drawable leftDrawable = binding.loginUsername.getCompoundDrawables()[0];
        if (leftDrawable != null) {
            leftDrawable.setBounds(0, 0, 40, 40);
            binding.loginUsername.setCompoundDrawables(leftDrawable, binding.loginUsername.getCompoundDrawables()[1]
                    , binding.loginUsername.getCompoundDrawables()[2], binding.loginUsername.getCompoundDrawables()[3]);
        }
        Drawable leftDrawable2 = binding.loginPassword.getCompoundDrawables()[0];
        if (leftDrawable2 != null) {
            leftDrawable2.setBounds(0, 0, 40, 40);
            binding.loginPassword.setCompoundDrawables(leftDrawable2, binding.loginPassword.getCompoundDrawables()[1]
                    , binding.loginPassword.getCompoundDrawables()[2], binding.loginPassword.getCompoundDrawables()[3]);
        }

        binding.loginUsername.setText(JNBasePreferenceSaves.getString(GlobalConstant.FLIGHT_USER_SIP));
        binding.loginPassword.setText(JNBasePreferenceSaves.getString(GlobalConstant.FLIGHT_USER_SIP_PASS_SAVE));

        binding.loginBtnLogin.setOnClickListener(v -> {
            showLoadding();
            handler.postDelayed(runnable, 10000);
            String name = binding.loginUsername.getText().toString();
            String pass = binding.loginPassword.getText().toString();
            JNBasePreferenceSaves.saveString("userSipPassSave", pass);
            binding.loginUsername.setText("");
            binding.loginPassword.setText("");

            GlobalConstant.sSipNum = name;
            GlobalConstant.sSipPassword = MD5Util.encrypt(pass);
//            暂时不加密sip密码
//            GlobalConstant.sSipPassword = pass;
            JNLogUtil.d("==LoginActivity==initView==time==01==" + System.currentTimeMillis());
            if(TextUtils.isEmpty(GlobalConstant.sSipPassword)){
                GlobalConstant.sSipPassword = "1234";
            }
            sipRegister(GlobalConstant.sSipNum, GlobalConstant.sSipPassword);
            JNSpUtils.setString(this, JNPjSipConstants.PJSIP_NUMBER, GlobalConstant.sSipNum);
            JNSpUtils.setString(this, JNPjSipConstants.PJSIP_PSWD, GlobalConstant.sSipPassword);


        });
    }

    private void sipRegister(String sipNum,String pwd) {
        pjSip.addPjSipListeners(new JNPjSip.PjSipListener() {
            @Override
            public void onRegistration(boolean isSuccessed) {
                Log.d("sip", "onRegistration: " + isSuccessed);
                loginState = 1 ;
                FlightAuthBean flightAuthBean = new FlightAuthBean();
                flightAuthBean.getData().setSip(sipNum);
                flightAuthBean.getData().setPassword(pwd);
//                flightAuthBean.getData().setPassword(MD5Util.encrypt(pwd));
                flightAuthBean.getData().setRegno("12345");
                String flightAuthContent = new Gson().toJson(flightAuthBean);
                JCimMessageBean jCimMessageBean = JCchatFactory.creatDroneIMMessage(JNContentEncryptUtils.encodeContent(flightAuthContent), JCSessionType.SESSION_DRONE_FLIGHT);
                JCMessageUtils.sendFlightMessage(jCimMessageBean, activity);
                Log.d("sip", "sendFlightMessage: " + flightAuthContent);
            }

            @Override
            public void onIncomingCall(String accountID, int callID, String displayName, String remoteUri, boolean isVideo,boolean isMeeting, String msgHead) {
                Log.d("sip", "onIncomingCall: accountID:" + accountID + " callID:" + callID + " displayName:" + displayName + " remoteUri:" + remoteUri + " isVideo:" + isVideo);
//                goInActivity("incoming", displayName, isVideo);
            }

            @Override
            public void onOutgoingCall(String accountID, int callID, String number) {
                Log.d("sip", "onOutgoingCall: ");
            }

            @Override
            public void onConnectState(int state) {
                Log.d("sip", "onConnectState: ");
            }
        });

        pjSip.sipRegister(sipNum,pwd);

    }

    private void initConfig() {
        pjSip = JNPjSip.getInstance(this);
        pjSip.addMessageListeners(new JFmyPjSipMessageListener());
        JCmessageListenerManager.addMessageListener("droneSipLogin" + "_" + JCSessionType.SESSION_DRONE + "_" + JNBasePreferenceSaves.getSipAddress(), new JCMessageListener() {
            @Override
            public void onReceiveMessage(JCbaseIMMessage message, int isLook) {
                Log.e("DemoMainActivity", "onReceiveMessage: " + message.getContent());
                String content = JNContentEncryptUtils.decryptContent(message.getContent());
                Log.e("DemoMainActivity", "receiveMsgType: "+message.getMsgType());
                Log.e("DemoMainActivity", "receiveContent: "+content);
                try {
                    switch (message.getMsgType()){
                        case JCSessionType.SESSION_DRONE_FLIGHT:
                            Log.d("DemoMainActivity", "newcontent SESSION_DRONE_FLIGHT: "+content);
                            DroneFlightDataBean droneFlightDataBean = new Gson().fromJson(content,DroneFlightDataBean.class);
                            GlobalConstant.mPlanzonecode = droneFlightDataBean.getText().getZoningCode();
                            JNBasePreferenceSaves.saveString("28181deviceId", droneFlightDataBean.getText().getDeviceId());
                            JNBasePreferenceSaves.saveString("28181plane_video", droneFlightDataBean.getText().getPlane_video());
                            JNBasePreferenceSaves.saveString("28181nickname", droneFlightDataBean.getText().getNickname());
                            JNBasePreferenceSaves.saveString("28181ZoningCode", droneFlightDataBean.getText().getZoningCode());
                            if(!TextUtils.isEmpty(GlobalConstant.mPlanzonecode)){
                                GlobalConstant.mPlansipstate = true;
                                EventBus.getDefault().post(new JCCodeEvent(JNEventBusType.CODE_SIP_Drone_login_plane,""));
                            }
                            loginState = 2 ;
                            handler.removeCallbacks(runnable);
                            cancelSuccessLoadding();
                            JCChatManager.showToast("飞机登录成功！");
                            finish();
                            break;
                        default:
                            break;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onSendMessageState(JCbaseIMMessage message, int state) {

            }
        });
    }

    /**
     * 设置全屏
     * before adding content
     */
    protected void viewFullScreen() {
        // 隐藏标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onDestroy() {
        cancelLoadding();
        super.onDestroy();
    }


    /**
     * 显示加载框
     */
    public void showLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.loading();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==showLoadding==", e);
        }
    }

    /**
     * 直接取消加载框
     */
    public void cancelLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.cancel();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==cancelLoadding==", e);
        }
    }

    /**
     * 显示加载成功后取消加载框
     */
    public void cancelSuccessLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.loadSuccess();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==cancelSuccessLoadding==", e);
        }
    }

    /**
     * 显示加载失败后取消加载框
     */
    public void cancelFailLoadding() {
        try {
            if (null != loadingDialog) loadingDialog.loadFail();
        } catch (Exception e) {
            JNLogUtil.e("==JCChatManager==cancelFailLoadding==", e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pjSip.sipBroadcastUnRegister();
        Log.e(TAG, "onPause: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        pjSip.sipBroadcastRegister();
        Log.e(TAG, "onResume: ");
    }

    private String createFlightData(String uuid,long timestamp,String state,int stateflag) {

        FlightParamBean flightParamBean = new FlightParamBean();
        flightParamBean.getData().setUuid(uuid);
        flightParamBean.getData().setRegno("123456");//暂时写死
        flightParamBean.getData().setTime(timestamp);
        flightParamBean.getHeader().setTimestamp(timestamp);

        flightParamBean.getData().setCurraction("10");
        //无人机验证参数
        flightParamBean.getData().setPlansipstate(GlobalConstant.mPlansipstate);
        if(!TextUtils.isEmpty(GlobalConstant.sSipNum)) {
            flightParamBean.getData().setPlansip(Long.parseLong(GlobalConstant.sSipNum));
        }
        if(!TextUtils.isEmpty(GlobalConstant.mPlanzonecode)) {
            flightParamBean.getData().setPlanzonecode(GlobalConstant.mPlanzonecode);
        }
        //飞手验证参数
        if(!TextUtils.isEmpty(GlobalConstant.sFlightAccountName)) {
            flightParamBean.getData().setFhsip(Long.parseLong(GlobalConstant.sFlightAccountName));
        }
        if(!TextUtils.isEmpty(GlobalConstant.mFhname)) {
            flightParamBean.getData().setFhname(GlobalConstant.mFhname);
        }
        if(!TextUtils.isEmpty(GlobalConstant.mFhzonecode)){
            flightParamBean.getData().setFhzonecode(GlobalConstant.mFhzonecode);
        }
        flightParamBean.getData().setFhsipstate(GlobalConstant.mFhsipstate);

        if(!TextUtils.isEmpty(state)&&stateflag!=0){
            flightParamBean.getData().setState(state);
            flightParamBean.getData().setStateflag(stateflag);
        }

        return new Gson().toJson(flightParamBean);
    }

}

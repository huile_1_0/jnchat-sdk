package com.jndv.jndvchatlibrary.ui.crowd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCCreateGroupUsersBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCNumbersData;
import com.jndv.jndvchatlibrary.ui.crowd.ui.SelectListAdapter;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCCrowdNumbersInviteViewModel;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * 群成员邀请页面
 */
public class JCCrowdNumbersInviteActivity extends JCBaseHeadActivity {

    private JCCrowdNumbersInviteViewModel jcViewModel;
    //view
    private ListView numbersRecyclerView;

    /**
     * ListView适配器
     */
    private SelectListAdapter selectListAdapter;


    private String crowdId;//群id
    private String group_domain_addr;
    private String group_domain_addr_user;
    //    private CrowdGroup crowd; // 群组
    private List<JCNumbersData> mList = new ArrayList<>();
    ; // 好友列表
    private List<JCNumbersData> tempList = new ArrayList<>(); // 选中的临时列表

    private List<String> hasInviteList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_jc_crowd_numbers_invite);
        initIntent();
        initView();
        initData();

    }

    private void initIntent() {
        Intent intent = getIntent();
        crowdId = intent.getStringExtra(JCEntityUtils.CROWD_ID);
        group_domain_addr = intent.getStringExtra(JCEntityUtils.GROUP_DOMAIN_ADDR);
        group_domain_addr_user = intent.getStringExtra(JCEntityUtils.GROUP_DOMAIN_ADDR_USER);

        String[] numbersStr = intent.getStringArrayExtra(JCEntityUtils.GROUP_NUMBERS);
        assert numbersStr != null;
        hasInviteList.addAll(Arrays.asList(numbersStr));


    }


    private void initData() {
        jcViewModel = new ViewModelProvider(this).get(JCCrowdNumbersInviteViewModel.class);
        jcViewModel.init(this);

        //获取好友列表
        jcViewModel.getAddressBook();

        jcViewModel.getAddressBookData().observe(this, bean -> {
            try {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, bean.getCode()) && bean.getData() != null && bean.getData().size() > 0) {

                    mList.clear();
                    mList.addAll(bean.getData());
                    resetData();
                    selectListAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        jcViewModel.getInviteRespondData().observe(this, bean -> {
            if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, bean.getCode())) {
                showToast("邀请成功！");
                finish();
            } else {
                showToast(bean.getMsg());
            }
        });
    }

    private void initView() {
        setTitleRight(getString(R.string.determine), null);
        setCenterTitle(getString(R.string.invite_numbers));
        numbersRecyclerView = (ListView) getId(R.id.crowd_invite_lv);
        selectListAdapter = new SelectListAdapter(this);
        selectListAdapter.setmList(mList);
        numbersRecyclerView.setAdapter(selectListAdapter);
        numbersRecyclerView.setTextFilterEnabled(true);
        numbersRecyclerView.setOnItemClickListener((parent, view, position, id) -> {
            if (!mList.get(position).isHasInvite()) {
                Log.e("onRightClickss",mList.get(position).isGroupChatSelectState()+"++++");
                if (mList.get(position).isGroupChatSelectState()) {
                    mList.get(position).setGroupChatSelectState(false);
                    tempList.remove(mList.get(position));
                } else {
                    mList.get(position).setGroupChatSelectState(true);
                    tempList.add(mList.get(position));
                }
                selectListAdapter.notifyDataSetChanged();
            }
            Log.e("onRightClick",tempList.size()+"++++");
        });

    }

    @Override
    protected void onRightClick() {
        super.onRightClick();

        if (tempList.size()==0){
            showToast("未选择好友");
            return;
        }
        List<JCCreateGroupUsersBean> list = new ArrayList<>();
        for (int i = 0; i < tempList.size(); i++) {
            list.add(new JCCreateGroupUsersBean(tempList.get(i)));
        }
        String json = new Gson().toJson(list);
        jcViewModel.inviteFriend(json, crowdId,group_domain_addr,group_domain_addr_user);
    }


    private void resetData() {
        for (int i = 0; i < mList.size(); i++) {
            mList.get(i).setHasInvite(hasInviteList.contains(mList.get(i).getFriendId()));
            mList.get(i).setGroupChatSelectState(hasInviteList.contains(mList.get(i).getFriendId()));
        }
    }

}

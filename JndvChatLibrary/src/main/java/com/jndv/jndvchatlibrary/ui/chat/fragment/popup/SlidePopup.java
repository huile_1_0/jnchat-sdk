package com.jndv.jndvchatlibrary.ui.chat.fragment.popup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.zxing.Result;
import com.jndv.jnbaseutils.IntentConstant;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.dowload.DownLoadImageService;
import com.jndv.jnbaseutils.dowload.ImageDownLoadCallBack;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCTransferActivity;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;
import com.jndv.jndvchatlibrary.utils.JSharedPreferences;
import com.jndv.jndvchatlibrary.utils.ZxingQrCodeUtil;
import com.jndv.jndvchatlibrary.web.InfoWebActivity;
import com.lxj.xpopup.core.BottomPopupView;

import java.io.File;

public class SlidePopup extends BottomPopupView implements View.OnClickListener {

    String TAG="SlidePopup";
    Context context;
    String path;
    private JCimMessageBean messageBean ;
    Bitmap bitmap;
    String imgSavePath="/内部存储/Pictures/image/";

    Handler handler=new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            dismiss();
            if(msg.what==1){
                Toast.makeText(context, "保存成功:可在"+imgSavePath+"文件夹中查看", Toast.LENGTH_SHORT).show();
            }else if(msg.what==2){
                Toast.makeText(context, "保存失败", Toast.LENGTH_SHORT).show();
            }
        }
    };

    public SlidePopup(Context context,String path,JCimMessageBean messageBean,Bitmap bitmap) {
        super(context);
        this.context=context;
        this.path=path;
        this.messageBean=messageBean;
        this.bitmap=bitmap;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_slide;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        initView();
    }
    private void initView() {
        RelativeLayout rlTransfer=findViewById(R.id.rl_transfer);
        RelativeLayout rlSave=findViewById(R.id.rl_save);
        RelativeLayout rlJump=findViewById(R.id.rl_jump);
        rlTransfer.setOnClickListener(this);
        rlSave.setOnClickListener(this);
        rlJump.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.rl_transfer){
            transfer();
        }else if(v.getId()==R.id.rl_save){
            save();
        }else if(v.getId()==R.id.rl_jump){
            jump();
        }
    }

    /**
     * 转发
     */
    private void transfer() {
        dismiss();
        Intent intent=new Intent(context, JCTransferActivity.class);
        intent.putExtra("content",messageBean);
        context.startActivity(intent);
    }

    /**
     * 保存图片
     */
    private void save() {
        DownLoadImageService service = new DownLoadImageService(context, path,
                new ImageDownLoadCallBack() {

                    @Override
                    public void onDownLoadSuccess(File file) {
                    }
                    @Override
                    public void onDownLoadSuccess(Bitmap bitmap) {
                        // 在这里执行图片保存方法
                        Log.d(TAG,"onDownLoadSuccess");
                        handler.sendEmptyMessage(1);
                    }

                    @Override
                    public void onDownLoadFailed() {
                        // 图片保存失败
                        Log.d(TAG,"onDownLoadFailed");
                        handler.sendEmptyMessage(2);
                    }
                });
        //启动图片下载线程
        new Thread(service).start();
    }

    /**
     * 识别跳转
     */
    private void jump() {
        dismiss();
        Result result = ZxingQrCodeUtil.getInstance().returnQrCode(bitmap);
        if(result==null){
            Toast.makeText(context,"未识别到二维码", Toast.LENGTH_SHORT).show();
            return;
        }

        String qrcodeStr=result.getText();
        Log.d("zhang","qrcodeStr=="+result.getText());
        if(qrcodeStr.contains("Admin_New")){
            try {
                String url=getRealAddress(qrcodeStr);
                Intent intent=new Intent(context, InfoWebActivity.class);
                intent.putExtra("WEB_URL",url);
                intent.putExtra("WEB_TITLE","详情页面");
                //这里可以认为和扫码页面一样，设置为true
                intent.putExtra(IntentConstant.INTENT_IS_FROM_CAMERA,true);
//            context.startActivity(intent);
                InfoWebActivity.startActivity((Activity) context,intent);

            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            Toast.makeText(context,"请扫描正确的二维码",Toast.LENGTH_SHORT).show();
        }
    }

    private String getRealAddress(String address) {
        String zoningcode= JNBasePreferenceSaves.getZoningCode();
        String userName=JSharedPreferences.getConfigStrValue(context, "appAccount");
        String domainName = GlobalConstant.getDomainBMS();
        domainName = domainName.startsWith("http") ? domainName : ("http://" + domainName);
        address=domainName+address+"&zoningcode="+zoningcode+"&userId="+userName+"&role_code=1";
        Log.d("zhang","address=="+address);
        return address;
    }
}

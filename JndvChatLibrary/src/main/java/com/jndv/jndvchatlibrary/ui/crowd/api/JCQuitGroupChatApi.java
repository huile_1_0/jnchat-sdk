package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;

public class JCQuitGroupChatApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/group/quitGroupChat";
    }

    private String domainAddr;
    private String userId;
    private String groupId;
    private String groupDomainAddr;

    public JCQuitGroupChatApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCQuitGroupChatApi setGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    public JCQuitGroupChatApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public JCQuitGroupChatApi setGroupDomainaddr(String groupDomainAddr) {
        this.groupDomainAddr = groupDomainAddr;
        return this;
    }

    public final static class Bean {

        private String code;
        private String msg;
        private Data data;

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public Data getData() {
            return data;
        }

        public class Data {

        }


    }
}

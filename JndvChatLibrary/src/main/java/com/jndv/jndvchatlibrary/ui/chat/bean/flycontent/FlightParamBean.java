package com.jndv.jndvchatlibrary.ui.chat.bean.flycontent;

public class FlightParamBean {
    private Header header = new Header();
    private Data data = new Data();

    public void setHeader(Header header) {
        this.header = header;
    }

    public Header getHeader() {
        return header;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public class Header {

        private int msg_id;//  消息编号 无人机飞行参数
        private int msg_no;//  消息序号
        private int res;//  消息来源
        private int des;//  消息目标
        private long timestamp;//  时间戳， 64位整数，单位： ms

        public void setMsg_id(int msg_id) {
            this.msg_id = msg_id;
        }

        public int getMsg_id() {
            return msg_id;
        }

        public void setMsg_no(int msg_no) {
            this.msg_no = msg_no;
        }

        public int getMsg_no() {
            return msg_no;
        }

        public void setRes(int res) {
            this.res = res;
        }

        public int getRes() {
            return res;
        }

        public void setDes(int des) {
            this.des = des;
        }

        public int getDes() {
            return des;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        public long getTimestamp() {
            return timestamp;
        }

    }

    public class Data {
        private String uuid;
        private String taskid;
        private String regno;// 民航局无人机实名 登记编号 必填
        private long time;//时间戳 必填
        private int alt;//海拔高度(高度) 必填
        private long lng;//经度 必填
        private long lat;//纬度 必填
        private float spd;//地速 必填
        private long ht;
        private float head;
        private float pit;
        private float roll;
        private int bat;//电池容量百分比 必填
        private int vol;
        private int fuel;//燃油剩余量百分比, 或燃油数量升 必填
        private String act;
        private int mode;//飞行模式 必填
        private int arm;//马达是否启动 必填
        private int air;//是否起飞到空中 必填
        private float absp;
        private float temp;

        private int cam;
        private String fcsn;
        private String imsi;
        private String imei;
        private String phone;
        private int pow;
        private String ver;
        private float hcc;
        private float vcc;
        private float tcc;
        private float ts;
        private long plansip;
        private boolean plansipstate;
        private String planzonecode;
        private long fhsip;
        private String fhname;
        private boolean fhsipstate;
        private String fhzonecode;
        private long jysip;
        private String jyname;
        private boolean jysipstate;
        private String jyzonecode;
        private String tasktitle;
        private String curr;//当前航点 到达航点后此字段有值必填
        /*8、任务开始
        9、正在按航线飞行
        10、到达XX航点
        11、航点动作（悬停）
        12、航点动作（拍照）
        13、航点动作（开始录像）
        14、航点动作（停止录像）
        15、航点动作（飞行器偏航角调整）
        16、航点动作（云台俯仰角调整）
        17、航点动作（相机变焦）
        18、航点动作（云台偏航角调整）
        19、离开航点
        20、任务结束*/
        private String curraction;//航点动作 航点动作尽量回传，如无法传递，至少保证到达航电、离开航点动作的参数传值

        private String pic;//拍照必填

        private String state;//飞行状态

        private int stateflag;//飞行状态值

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public int getStateflag() {
            return stateflag;
        }

        public void setStateflag(int stateflag) {
            this.stateflag = stateflag;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getTaskid() {
            return taskid;
        }

        public void setTaskid(String taskid) {
            this.taskid = taskid;
        }

        public int getCam() {
            return cam;
        }

        public void setCam(int cam) {
            this.cam = cam;
        }

        public String getFcsn() {
            return fcsn;
        }

        public void setFcsn(String fcsn) {
            this.fcsn = fcsn;
        }

        public String getImsi() {
            return imsi;
        }

        public void setImsi(String imsi) {
            this.imsi = imsi;
        }

        public String getImei() {
            return imei;
        }

        public void setImei(String imei) {
            this.imei = imei;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getPow() {
            return pow;
        }

        public void setPow(int pow) {
            this.pow = pow;
        }

        public String getVer() {
            return ver;
        }

        public void setVer(String ver) {
            this.ver = ver;
        }

        public float getHcc() {
            return hcc;
        }

        public void setHcc(float hcc) {
            this.hcc = hcc;
        }

        public float getVcc() {
            return vcc;
        }

        public void setVcc(float vcc) {
            this.vcc = vcc;
        }

        public float getTcc() {
            return tcc;
        }

        public void setTcc(float tcc) {
            this.tcc = tcc;
        }

        public float getTs() {
            return ts;
        }

        public void setTs(float ts) {
            this.ts = ts;
        }

        public long getPlansip() {
            return plansip;
        }

        public void setPlansip(long plansip) {
            this.plansip = plansip;
        }

        public boolean getPlansipstate() {
            return plansipstate;
        }

        public void setPlansipstate(boolean plansipstate) {
            this.plansipstate = plansipstate;
        }

        public String getPlanzonecode() {
            return planzonecode;
        }

        public void setPlanzonecode(String planzonecode) {
            this.planzonecode = planzonecode;
        }

        public long getFhsip() {
            return fhsip;
        }

        public void setFhsip(long fhsip) {
            this.fhsip = fhsip;
        }

        public String getFhname() {
            return fhname;
        }

        public void setFhname(String fhname) {
            this.fhname = fhname;
        }

        public boolean getFhsipstate() {
            return fhsipstate;
        }

        public void setFhsipstate(boolean fhsipstate) {
            this.fhsipstate = fhsipstate;
        }

        public String getFhzonecode() {
            return fhzonecode;
        }

        public void setFhzonecode(String fhzonecode) {
            this.fhzonecode = fhzonecode;
        }

        public long getJysip() {
            return jysip;
        }

        public void setJysip(long jysip) {
            this.jysip = jysip;
        }

        public String getJyname() {
            return jyname;
        }

        public void setJyname(String jyname) {
            this.jyname = jyname;
        }

        public boolean getJysipstate() {
            return jysipstate;
        }

        public void setJysipstate(boolean jysipstate) {
            this.jysipstate = jysipstate;
        }

        public String getJyzonecode() {
            return jyzonecode;
        }

        public void setJyzonecode(String jyzonecode) {
            this.jyzonecode = jyzonecode;
        }

        public String getTasktitle() {
            return tasktitle;
        }

        public void setTasktitle(String tasktitle) {
            this.tasktitle = tasktitle;
        }

        public String getCurr() {
            return curr;
        }

        public void setCurr(String curr) {
            this.curr = curr;
        }

        public String getCurraction() {
            return curraction;
        }

        public void setCurraction(String curraction) {
            this.curraction = curraction;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public void setRegno(String regno) {
            this.regno = regno;
        }

        public String getRegno() {
            return regno;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public long getTime() {
            return time;
        }

        public void setAlt(int alt) {
            this.alt = alt;
        }

        public int getAlt() {
            return alt;
        }

        public void setLng(long lng) {
            this.lng = lng;
        }

        public long getLng() {
            return lng;
        }

        public void setLat(long lat) {
            this.lat = lat;
        }

        public long getLat() {
            return lat;
        }

        public void setSpd(float spd) {
            this.spd = spd;
        }

        public float getSpd() {
            return spd;
        }

        public void setHt(long ht) {
            this.ht = ht;
        }

        public long getHt() {
            return ht;
        }

        public void setHead(float head) {
            this.head = head;
        }

        public float getHead() {
            return head;
        }

        public void setPit(float pit) {
            this.pit = pit;
        }

        public float getPit() {
            return pit;
        }

        public void setRoll(float roll) {
            this.roll = roll;
        }

        public float getRoll() {
            return roll;
        }

        public void setBat(int bat) {
            this.bat = bat;
        }

        public int getBat() {
            return bat;
        }

        public void setVol(int vol) {
            this.vol = vol;
        }

        public int getVol() {
            return vol;
        }

        public void setFuel(int fuel) {
            this.fuel = fuel;
        }

        public int getFuel() {
            return fuel;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getAct() {
            return act;
        }

        public void setMode(int mode) {
            this.mode = mode;
        }

        public int getMode() {
            return mode;
        }

        public void setArm(int arm) {
            this.arm = arm;
        }

        public int getArm() {
            return arm;
        }

        public void setAir(int air) {
            this.air = air;
        }

        public int getAir() {
            return air;
        }

        public void setAbsp(float absp) {
            this.absp = absp;
        }

        public float getAbsp() {
            return absp;
        }

        public void setTemp(float temp) {
            this.temp = temp;
        }

        public float getTemp() {
            return temp;
        }


    }
}

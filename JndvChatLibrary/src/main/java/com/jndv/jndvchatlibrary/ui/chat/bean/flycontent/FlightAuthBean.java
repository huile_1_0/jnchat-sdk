package com.jndv.jndvchatlibrary.ui.chat.bean.flycontent;

public class FlightAuthBean {
    private Header header;
    private Data data = new Data();
    public void setHeader(Header header) {
        this.header = header;
    }
    public Header getHeader() {
        return header;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public Data getData() {
        return data;
    }

    public class Header {

        private int msg_id ;
        private int msg_no;
        private int res;
        private int des;
        private long timestamp;
        public void setMsg_id (int msg_id ) {
            this.msg_id  = msg_id ;
        }
        public int getMsg_id () {
            return msg_id ;
        }

        public void setMsg_no(int msg_no) {
            this.msg_no = msg_no;
        }
        public int getMsg_no() {
            return msg_no;
        }

        public void setRes(int res) {
            this.res = res;
        }
        public int getRes() {
            return res;
        }

        public void setDes(int des) {
            this.des = des;
        }
        public int getDes() {
            return des;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }
        public long getTimestamp() {
            return timestamp;
        }

    }

    public class Data {

        private String regno;
        private String sip;
        private String password;
        public void setRegno(String regno) {
            this.regno = regno;
        }
        public String getRegno() {
            return regno;
        }

        public void setSip(String sip) {
            this.sip = sip;
        }
        public String getSip() {
            return sip;
        }

        public void setPassword(String password) {
            this.password = password;
        }
        public String getPassword() {
            return password;
        }

    }
}

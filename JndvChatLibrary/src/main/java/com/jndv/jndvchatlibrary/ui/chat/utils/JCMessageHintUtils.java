package com.jndv.jndvchatlibrary.ui.chat.utils;

import android.app.Service;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

import com.jndv.jndvchatlibrary.JCChatManager;

/**
 * Author: wangguodong
 * Date: 2022/5/5
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 消息提示的工具类
 */
public class JCMessageHintUtils {
    static MediaPlayer mMediaPlayer = new MediaPlayer();
//    static SoundPool soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 100);
    public static void playMsgHint(){
        playRing();
        phoneVibrates();
    }

    /**
     * 手机震动
     */
    public static void phoneVibrates() {
        Vibrator vib = (Vibrator) JCChatManager.getApplication().getSystemService(Service.VIBRATOR_SERVICE);
        vib.vibrate(new long[]{150, 250, 150, 250}, -1);
    }

    /**
     * 播放铃声
     */
    public static void playRing() {
        try {
            mMediaPlayer.reset();
            //用于获取手机 默认提示音（RingtoneManager.TYPE_NOTIFICATION） 的Uri
//            Uri ringUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            Uri ringUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mMediaPlayer.setDataSource(JCChatManager.getApplication(), ringUri);

            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mMediaPlayer.setLooping(false);
            mMediaPlayer.prepare();
            mMediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void playSound(int sound, int loop) {
//
//        AudioManager mgr = (AudioManager)JCchatManager.getApplication().getSystemService(Context.AUDIO_SERVICE);
//
//        float streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
//
//        float streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
//
//        float volume = streamVolumeCurrent/streamVolumeMax;
//
//        soundPool.play(soundPoolMap.get(sound), volume, volume, 1, 0, 1f);
//
////参数：1、Map中取值 2、当前音量 3、最大音量 4、优先级 5、重播次数 6、播放速度
//    }


}

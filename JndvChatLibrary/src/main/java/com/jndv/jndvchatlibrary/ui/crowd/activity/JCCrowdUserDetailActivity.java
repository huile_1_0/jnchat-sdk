package com.jndv.jndvchatlibrary.ui.crowd.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCCrowdUserDetailFragment;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;

public class JCCrowdUserDetailActivity extends JCbaseFragmentActivity {
    private String userId;
    private String domainAddr;
    private String groupDomainAddr;
    private String crowdId;
    private int type;
    private String invite_permission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initIntent();
        initView();
    }

    public static void start(Context context, String userId, String crowdId,String domainAddr,String groupDomainAddr, int type,String invite_permission) {
        Intent intent = new Intent(context, JCCrowdUserDetailActivity.class);
        intent.putExtra(JCEntityUtils.USER_ID, userId);
        intent.putExtra(JCEntityUtils.CROWD_ID, crowdId);
        intent.putExtra(JCEntityUtils.DOMAIN_ADDR, domainAddr);
        intent.putExtra(JCEntityUtils.GROUP_DOMAIN_ADDR, groupDomainAddr);
        intent.putExtra(JCEntityUtils.IS_FROM_CROWD, type);
        intent.putExtra(JCEntityUtils.INVITE_PERMISSION,invite_permission);
        context.startActivity(intent);
    }

    private void initIntent() {
        Intent intent = getIntent();
        userId = intent.getStringExtra(JCEntityUtils.USER_ID);
        domainAddr = intent.getStringExtra(JCEntityUtils.DOMAIN_ADDR);
        invite_permission = intent.getStringExtra(JCEntityUtils.INVITE_PERMISSION);
        groupDomainAddr = intent.getStringExtra(JCEntityUtils.GROUP_DOMAIN_ADDR);
        crowdId = intent.getStringExtra(JCEntityUtils.CROWD_ID);
        type = intent.getIntExtra(JCEntityUtils.IS_FROM_CROWD, 0);

        JCCrowdUserDetailFragment fragment = JCCrowdUserDetailFragment.newInstance(userId, crowdId,domainAddr,groupDomainAddr, type,invite_permission);
        showFragment(fragment);

    }

    private void initView() {
//        setCenterTitle(getString(R.string.crowd_user_info));
        setCenterTitle("");
    }
}

package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.databinding.FragmentJcchatmanageBinding;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jndvchatlibrary.http.api.JCAddFriendApi;
import com.jndv.jndvchatlibrary.http.api.JCDeleteMessageApi;
import com.jndv.jndvchatlibrary.http.api.JCFriendRelationShipApi;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jnbaseutils.chat.JCFriendBean;
import com.jndv.jnbaseutils.chat.JCFriendBean_;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCFriendRelationShipBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.utils.JCFriendUtil;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;

import org.greenrobot.eventbus.EventBus;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/5/4
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 点对点私聊个人详情页，管理页
 *
 * 问题:显示添加好友还是删除好友，不能根据本地数据库里的状态判断。因为如果这时候两人已经是好友，再卸载app,数据库就清空了。
 *      稳妥起见，只有好友关系从接口获取，这样尽量少改代码.
 */
public class JCChatManageFragment  extends JCbaseFragment {
    String TAG="JCChatManageFragment";
    private FragmentJcchatmanageBinding binding;
    private JCChatManageViewModel jcChatManageViewModel;
    private String sessionId = "";
    private String sipId = "0";
    private String sipAddress = "0";
    private String javaAddress = "0";
    private String name;
    private String head;
    public static final String INSIPID = "siPid";
    public static final String INSIPADDRESS = "siPADDRESSS";
    public static final String INJAVAADDRESS = "javaADDRESSS";
    public static final String CODE_INNAME = "CODE_INNAME" ;
    public static final String CODE_INHEAD = "CODE_INHEAD" ;
    LoadingPopupView loadingPopupView;
    JCFriendBean jcFriendBean ;
    JCSessionListBean seaaionBean=null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        jcChatManageViewModel =
                new ViewModelProvider(this).get(JCChatManageViewModel.class);
        binding = FragmentJcchatmanageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        loadingPopupView = new XPopup.Builder(getContext()).asLoading("正在加载中");
        jcChatManageViewModel.init(this);
        init();
        getFriendStatus();
        return root;
    }

    private void init() {
        Bundle bundle = getArguments();
        if (bundle != null){
            sessionId = bundle.getString(JCEntityUtils.SESSION_ID);
            name = bundle.getString(CODE_INNAME);
            head = bundle.getString(CODE_INHEAD);
            sipId = bundle.getString(INSIPID);
            sipAddress = bundle.getString(INSIPADDRESS);
            javaAddress = bundle.getString(INJAVAADDRESS);
            Log.e("TAG", "init: ===============01=sessionId="+sessionId );
        }
        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
        if (TextUtils.isEmpty(sessionId)){
            seaaionBean = builder
                    .equal(JCSessionListBean_.other_id, sipId, QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.user_id, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.session_party_domain_addr, sipAddress,QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();
            if (null==seaaionBean){
                binding.cvCreateSession.setVisibility(View.VISIBLE);
                initFriend();
            }else {
                if (1==seaaionBean.getIs_top()){
                    binding.switchZd.setChecked(true);
                }
                if (1==seaaionBean.getIs_no_disturb()){
                    binding.switchMdr.setChecked(true);
                }
                initFriend(seaaionBean);
            }

        }else {
            seaaionBean = builder.equal(JCSessionListBean_.session_id, Long.parseLong(sessionId)).build().findFirst();
            Log.e("TAG", "init: ===============02=sessionId="+sessionId );
            if (null!=seaaionBean){
                if (1==seaaionBean.getIs_top()){
                    binding.switchZd.setChecked(true);
                }
                if (1==seaaionBean.getIs_no_disturb()){
                    binding.switchMdr.setChecked(true);
                }
                initFriend(seaaionBean);
            }
        }

        binding.cvCreateSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //创建会话

            }
        });
        binding.llFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //点击进入用户详情

            }
        });
        binding.switchMdr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //去开启免打扰
                if (isChecked&&null!=seaaionBean&&0==seaaionBean.getIs_no_disturb()){
                    loadingPopupView.show();
                    jcChatManageViewModel.updateSession(sessionId, seaaionBean.getIs_top(), 1);
                }else {
                    if (!isChecked&&null!=seaaionBean&&1==seaaionBean.getIs_no_disturb()){
                        loadingPopupView.show();
                        jcChatManageViewModel.updateSession(sessionId, seaaionBean.getIs_top(), 0);
                    }
                }
            }
        });
        binding.switchZd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //去置顶聊天
                if (isChecked&&null!=seaaionBean&&0==seaaionBean.getIs_top()){
                    loadingPopupView.show();
                    jcChatManageViewModel.updateSession(sessionId, 1, seaaionBean.getIs_no_disturb());
                }else {
                    if (!isChecked&&null!=seaaionBean&&1==seaaionBean.getIs_top()){
                        loadingPopupView.show();
                        jcChatManageViewModel.updateSession(sessionId, 0, seaaionBean.getIs_no_disturb());
                    }
                }
            }
        });

        binding.rlQcord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(JCUserQRcodeFragment.CODE_INTYPE, 1);
                bundle.putString(JCUserQRcodeFragment.CODE_INNAME, name);
                bundle.putString(JCUserQRcodeFragment.CODE_INHEAD, head);
                bundle.putString(JCUserQRcodeFragment.CODE_INSIPID, sipId);
                bundle.putString(JCUserQRcodeFragment.CODE_INSIPADDRESS, sipAddress);
                bundle.putString(JCUserQRcodeFragment.CODE_INJAVAADDRESS, javaAddress);
                JCUserQRcodeFragment jcUserQRcodeFragment = new JCUserQRcodeFragment();
                JCbaseFragmentActivity.start(getActivity(),jcUserQRcodeFragment, "个人二维码", bundle);
            }
        });

        jcChatManageViewModel.getChatManageModel().observeForever(new Observer<JCChatManageViewModel.JCChatManageModelItem>() {
            @Override
            public void onChanged(JCChatManageViewModel.JCChatManageModelItem jcChatManageModelItem) {
                if (null!=seaaionBean){
                    if (0==jcChatManageModelItem.getTop()){
                        seaaionBean.setIs_top(0);
                    }else {
                        seaaionBean.setIs_top(1);
                    }
                    if (0==jcChatManageModelItem.getNodisturb()){
                        seaaionBean.setIs_no_disturb(0);
                    }else {
                        seaaionBean.setIs_no_disturb(1);
                    }
                    EventBus.getDefault().post(new JNCodeEvent<JCSessionListBean>(JNEventBusType.CODE_SESSION_LIST_UPDATE_INFO,"", seaaionBean));

                    Toast.makeText(getActivity(), "设置成功！", Toast.LENGTH_SHORT).show();
                }
                    loadingPopupView.dismiss();
            }
        });

    }

    private void initFriend(JCSessionListBean messageBean){
        sipId = ""+messageBean.getOther_id();
        sipAddress = messageBean.getSession_party_domain_addr();
        name = getName(messageBean);
        head = messageBean.getOther_pic();
        javaAddress = messageBean.getSession_party_domain_addr_user();
        initFriend();
    }
    private void initFriend(){
        JCglideUtils.loadCircleImage(getActivity(), head, binding.ivUserHead);
        binding.tvUserName.setText(name);

        binding.cvAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JCFriendUtil.addFrend(getActivity(), sipId, sipAddress, "", name, head, javaAddress
                        , new OnHttpListener<JCAddFriendApi.Bean>() {
                            @Override
                            public void onSucceed(JCAddFriendApi.Bean result) {
                                try {
                                    if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                        Toast.makeText(getActivity(), "添加成功！", Toast.LENGTH_SHORT).show();
                                        JCFriendBean jcFriendBean = new JCFriendBean();
                                        JCFriendUtil.setInfo(result.getData(), jcFriendBean);
                                        jcFriendBean.setSaveUid(JNBasePreferenceSaves.getUserSipId());
                                        JCobjectBox.get().boxFor(JCFriendBean.class).put(jcFriendBean);
                                        binding.cvDeleteFriend.setVisibility(View.VISIBLE);
                                        binding.cvAddFriend.setVisibility(View.GONE);
                                    }else if (TextUtils.equals("1001000002", result.getCode())){
                                        Toast.makeText(getActivity(), "已成为好友！", Toast.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(getActivity(), "添加失败！", Toast.LENGTH_SHORT).show();
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFail(Exception e) {
                                Toast.makeText(getActivity(), "添加失败,稍后再试！", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        binding.cvDeleteFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JCFriendUtil.deletFrend(getActivity(), sipId, sipAddress
//                JCFriendUtil.deletFrend(getActivity(), "" + messageBean.getOther_id(), messageBean.getSession_party_domain_addr()
                        , new OnHttpListener<JCDeleteMessageApi.Bean>() {
                            @Override
                            public void onSucceed(JCDeleteMessageApi.Bean result) {
                                try {
                                    if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                                        Toast.makeText(getActivity(), "删除成功！", Toast.LENGTH_SHORT).show();
                                        Box<JCFriendBean> imMsgBox = JCobjectBox.get().boxFor(JCFriendBean.class);
                                        QueryBuilder<JCFriendBean> builder = imMsgBox.query();
                                        jcFriendBean = builder
                                                .equal(JCFriendBean_.friendId, sipId,QueryBuilder.StringOrder.CASE_SENSITIVE)
                                                .equal(JCFriendBean_.domainAddr, sipAddress,QueryBuilder.StringOrder.CASE_SENSITIVE)
                                                .equal(JCFriendBean_.saveUid, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                                                .build().findFirst();

                                        if (null!=jcFriendBean)JCobjectBox.get().boxFor(JCFriendBean.class).remove(jcFriendBean);
//                                        binding.cvDeleteFriend.setVisibility(View.GONE);
//                                        binding.cvAddFriend.setVisibility(View.VISIBLE);
                                        getActivity().finish();
//                                    }else if (1001000002==result.getCode()){
//                                        Toast.makeText(getActivity(), "已成为好友！", Toast.LENGTH_SHORT).show();
                                    }else if (TextUtils.equals("1001000003", result.getCode())){
                                        Toast.makeText(getActivity(), "删除成功！", Toast.LENGTH_SHORT).show();
                                        if (null!=jcFriendBean)JCobjectBox.get().boxFor(JCFriendBean.class).remove(jcFriendBean);
                                        getActivity().finish();
                                    }else {
                                        Toast.makeText(getActivity(), "删除失败！", Toast.LENGTH_SHORT).show();
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFail(Exception e) {
                                Toast.makeText(getActivity(), "删除失败,稍后再试！", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), sipId) && TextUtils.equals(sipAddress, JNBasePreferenceSaves.getSipAddress())){
            binding.rlMsgMdr.setVisibility(View.GONE);
            binding.rlMsgZd.setVisibility(View.GONE);
        }
    }

    public static String getName(JCSessionListBean dataBean){
        String name = ""+dataBean.getOther_id();
        try {
            if (!TextUtils.isEmpty(dataBean.getRemarks())){
                name = dataBean.getRemarks();
            }else if (!TextUtils.isEmpty(dataBean.getOther_name())){
                name = dataBean.getOther_name();
            }else {
                String message = dataBean.getFirst_content();
                JCimMessageBean jCimMessageBean = new Gson().fromJson(message, JCimMessageBean.class);
                if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), jCimMessageBean.getToSipID())&&!TextUtils.isEmpty(jCimMessageBean.getFromName())){
                    name = jCimMessageBean.getFromName();
//            }else if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), jCimMessageBean.getFromID())&&!TextUtils.isEmpty(jCimMessageBean.getto)){
//                name = jCimMessageBean.getFromName();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return name;
    }

    /**
     * 获取两个人是否好友关系
     */
    public void getFriendStatus(){
        Log.d(TAG,"对方sip号=="+sipId);
        JCFriendRelationShipApi api=new JCFriendRelationShipApi().setOperationUserId(JNBasePreferenceSaves.getUserSipId())
                                                                .setFriendDomainAddr(sipAddress)
                                                                .setFriendId(sipId);
        EasyHttp.get(this)
                .api(api)
                .request(new OnHttpListener<JCRespondBean<JCFriendRelationShipBean>>() {
                    @Override
                    public void onSucceed(JCRespondBean<JCFriendRelationShipBean> result) {
                        if(TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            binding.cvAddFriend.setVisibility(View.GONE);
                            binding.cvDeleteFriend.setVisibility(View.VISIBLE);
                        }else {
                            binding.cvAddFriend.setVisibility(View.VISIBLE);
                            binding.cvDeleteFriend.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        binding.cvAddFriend.setVisibility(View.VISIBLE);
                        binding.cvDeleteFriend.setVisibility(View.GONE);
                    }
                });
    }

}

package com.jndv.jndvchatlibrary.ui.crowd.viewmodel;

import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGetGroupAllApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGetUserGroupSessionApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCGroupAllBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCGroupNumbersBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserGroupSessionBean;


import java.util.List;

public class JCCrowdListModel extends ViewModel {

    private static final String TAG = "JCCrowdListModel";
    private MutableLiveData<List<JCUserGroupSessionBean>> userGroupSessionAllData= new MutableLiveData<>();

    private ComponentActivity activity;

    public JCCrowdListModel(ComponentActivity activity) {
        this.activity =activity;
    }

    public MutableLiveData<List<JCUserGroupSessionBean>> getUserGroupSessionData() {
        return userGroupSessionAllData;
    }

    public void requestUserGroupSessionAllData(){
        EasyHttp.get(activity)
                .api(new JCGetUserGroupSessionApi(
                        JNBasePreferenceSaves.getUserSipId(),JNBasePreferenceSaves.getSipAddress(),JNBasePreferenceSaves.getJavaAddress())

                )
                .request(new OnHttpListener<JCRespondBean<List<JCUserGroupSessionBean>>>() {
                    @Override
                    public void onSucceed(JCRespondBean<List<JCUserGroupSessionBean>> result) {
                        userGroupSessionAllData.postValue(result.getData());
                    }
                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: "+e.getMessage());
                    }
                });
    }
}
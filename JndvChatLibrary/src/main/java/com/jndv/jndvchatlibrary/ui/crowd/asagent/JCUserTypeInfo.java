package com.jndv.jndvchatlibrary.ui.crowd.asagent;

public class JCUserTypeInfo implements Comparable<JCUserTypeInfo> {
	
	public String strID;
	public String strTypeName;
	public String strSort;
	public String strTypeImgUrl;
	public String strRemark;
	@Override
	public String toString() {
		return "UserTypeInfo [strID=" + strID + ", strTypeName=" + strTypeName + ", strSort=" + strSort
				+ ", strTypeImgUrl=" + strTypeImgUrl + ", strRemark=" + strRemark + "]";
	}
	@Override
	public int compareTo(JCUserTypeInfo another) {
		int i = Integer.parseInt(strSort) - Integer.parseInt(another.strSort);
		return i;
	}
}

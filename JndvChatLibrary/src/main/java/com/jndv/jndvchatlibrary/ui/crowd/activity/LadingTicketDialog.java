package com.jndv.jndvchatlibrary.ui.crowd.activity;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.google.zxing.client.android.utils.ZXingUtils;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCTransferActivity;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgImgContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCUriToPathUtils;
import com.jndv.jndvchatlibrary.ui.crowd.utils.SystemUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;


public class LadingTicketDialog {

    private Dialog mDialog;
    private TextView tv_confirm,tv_invite_num,tv_cancel;
    private ImageView iv_address_qr;
    private Context mContext;
    private String address,link;

    public LadingTicketDialog(Context context, Bitmap bitmap, String address,String link ,String message) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.crowd_qr_share_dialog, null);
        mContext = context;
        this.address = address;
        this.link = link;
        mDialog = new Dialog(context, R.style.dialogDim);
        mDialog.setContentView(view);
        mDialog.setCanceledOnTouchOutside(true);
        final Window win = mDialog.getWindow();
        win.setWindowAnimations(R.style.dialogAnim);
        tv_confirm = (TextView) view.findViewById(R.id.tv_confirm);
        tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_invite_num = (TextView) view.findViewById(R.id.tv_invite_num);
        iv_address_qr = (ImageView) view.findViewById(R.id.iv_qr);
        tv_invite_num.setText(address);

        JCglideUtils.loadImage(context,bitmap,iv_address_qr);
        iv_address_qr.setOnLongClickListener(v -> {

//                String imgurl = webView.getHitTestResult().getExtra();

            final String[] items = { "保存图片" };
            AlertDialog.Builder listDialog =
                    new AlertDialog.Builder(context);
            listDialog.setTitle("");
            listDialog.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == 0) {
                        //保存图片
                        saveQrcodeToGallery();
//                            saveImage(bitmap, new ImageCallback() {
//                                @Override
//                                public void onFinish(String savePath) {
//                                    if (!TextUtils.isEmpty(savePath)) {
//                                        ToastUtil.showToast(context.getString(R.string.person_qr_save_success)+savePath);
//                                    }
//                                }
//                            });
                    }
                }
            });
            listDialog.show();
            return false;
        });
        /*mDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_BACK){
                    return true;
                }
                return false;
            }
        });*/

//        view.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });
        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dismiss();
                saveQrcodeToGallery();
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dismiss();
                Uri uri = saveQrcodeToGallery();
                //
//                doShare();
                copy(uri);
            }
        });

    }

    /**
     * 复制数据
     * @param uri
     */
    private void copy(Uri uri) {
//        if (TextUtils.isEmpty(content)) {
//            Toast.makeText(mContext,"复制内容为空",Toast.LENGTH_SHORT).show();
//            return;
//        }
//        ClipboardManager cm = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
//        ClipData mClipData = ClipData.newPlainText("Label", content);
//        cm.setPrimaryClip(mClipData);
//        Toast.makeText(mContext,"复制成功",Toast.LENGTH_SHORT).show();
        int whd = 100 ;
        int whh = 100 ;
        String type = "jpg";
        try {
            String[] wh = JCglideUtils.getBitmapWHFromUri(mContext, uri);
            type = wh[2];
            try {
                whd = Integer.parseInt(wh[0]);
            }catch (Exception e){e.printStackTrace();}

            try {
                whh = Integer.parseInt(wh[1]);
            }catch (Exception e){e.printStackTrace();}
        }catch (Exception e){
            e.printStackTrace();
        }

        JCMsgImgContentBean msgEntity = new JCMsgImgContentBean(JCUriToPathUtils.getRealPathFromUri(uri),whd,whh, type);
        String contentText = new Gson().toJson(msgEntity);
        String contentBase64 = Base64.encodeToString(contentText.getBytes(),Base64.NO_WRAP);

        Log.e("0410", "copy: " + uri.toString());

        Intent intent=new Intent(mContext, JCTransferActivity.class);
        intent.putExtra("contentText",contentBase64);
//        intent.putExtra("uri",JCUriToPathUtils.getRealPathFromUri(uri));
        intent.putExtra("imageUri", uri);
        mContext.startActivity(intent);
        dismiss();
    }

//    public void setNegativeListener(View.OnClickListener listener){
//        negative.setOnClickListener(listener);
//    }



    public void show() {
        mDialog.show();
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    /* 保存图片方法 Start */
    public static interface SaveImageCallback {
        void onFinish(String savePath);
    }
//    private void saveImage(Bitmap bitmap, ImageCallback callback) {
//        //申请保存权限
//        ((BaseActivity) mContext).requestRuntimePermisssions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, new PermissionListener() {
//            @Override
//            public void onGranted() {
////                showLoading();
//                List<String> filePath = FileUtils.saveBitmapForFileName(bitmap);
//                if (null != callback) {
//                    callback.onFinish("");
//                }
//                try {
//                    MediaStore.Images.Media.insertImage(mContext.getContentResolver(), bitmap, filePath.get(0), null);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
////                 最后通知图库更新
//                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    Uri contentUri = FileProvider.getUriForFile(mContext, BuildConfig.APP_APPLICATION_ID + ".fileprovider", new File(filePath.get(2)));
//                    intent.setData(contentUri);
//                } else {
//                    Uri uri2 = Uri.fromFile(new File(filePath.get(2)));
//                    intent.setData(uri2);
//                }
//                mContext.sendBroadcast(intent);
//
//            }
//
//            @Override
//            public void onDenied(List<String> deniedList) {
//            }
//        });
//    }
    /* 保存图片方法 End */

    public static interface ImageCallback {
        void onFinish(String savePath);
    }

    private Uri saveQrcodeToGallery() {
        //创建视图
        View qrcodeView = LayoutInflater.from(mContext).inflate(R.layout.get_ticket_share_qr, null, false);
        Bitmap bm= ZXingUtils.createQRCodeImage(link);
        ((ImageView)qrcodeView.findViewById(R.id.iv_qr)).setImageDrawable(new BitmapDrawable(bm));
        ((TextView)qrcodeView.findViewById(R.id.tv_invite_num)).setText(address);

        //计算视图大小
        DisplayMetrics displayMetrics = SystemUtils.getWindowDisplayMetrics(mContext);
        final int width = displayMetrics.widthPixels;
        final int height = displayMetrics.heightPixels - SystemUtils.getStatusBarHeight(mContext) - SystemUtils.getActionBarHeight(mContext) - mContext.getResources().getDimensionPixelSize(R.dimen.default_bottom_bar_height);

        //将视图生成图片
        Bitmap image = SystemUtils.generateImageFromView(qrcodeView, width, height);

        //将图片保存到系统相册
        Uri uri = SystemUtils.saveImageToGallery(mContext, image);
        image.recycle();
        if (null!=uri) {
            Toast.makeText(mContext, "已保存到系统相册！", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "保存到系统相册失败！", Toast.LENGTH_SHORT).show();
        }
        return uri;
    }

    /**
     * ContentProvider 的方式也可能失败
     * 使用ContentUri分享的方式 是可以分享到 部分应用获取资源失败的情况，比如 朋友圈
     *
     * @param context
     * @param filePath
     * @return
     */
//    public static Uri getImageContentUri(Context context, String filePath) {
////        String filePath = imageFile.getAbsolutePath();
//        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                new String[]{MediaStore.Images.Media._ID}, MediaStore.Images.Media.DATA + "=? ",
//                new String[]{filePath}, null);
//        Uri uri = null;
//
//        if (cursor != null) {
//            if (cursor.moveToFirst()) {
//                int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
//                Uri baseUri = Uri.parse("content://media/external/images/media");
//                uri = Uri.withAppendedPath(baseUri, "" + id);
//            }
//
//            cursor.close();
//        }
//
//        if (uri == null) {
//            ContentValues values = new ContentValues();
//            values.put(MediaStore.Images.Media.DATA, filePath);
//            uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//        }
//
//        return uri;
//    }
}

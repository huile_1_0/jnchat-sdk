package com.jndv.jndvchatlibrary.ui.chat.bean;

import java.util.List;

/**
 * @Description 入群邀请实体类
 * @Author SunQinzheng
 * @Time 2022/7/14 下午 5:18
 **/
public class JCInvitationApprovalBean {


    /**
     * list : [{"id":13,"groupId":9938,"userId":1041,"groupDomainAddr":"imapi.jndv.com:5080","groupDomainAddrUser":"imapi.jndv.com","addTime":1656663112000,"joinDomainAddr":"imapi.jndv.com:5080","joinUserid":1049,"approvalStatus":2,"modTime":null,"groupInfo":"{\"addTime\":\"2022-06-13T09:36:03\",\"cgName\":\"群聊20220526\",\"cgPic\":\"\",\"disableSendMsg\":\"0\",\"groupIds\":\"\",\"groupType\":0,\"id\":9938,\"isProhibition\":0,\"isReasonsForClosure\":\"\",\"modPicTime\":\"2022-06-13T09:36:03\",\"number\":10,\"qrPhoto\":\"\",\"renzheng\":0}"}]
     * total : 1
     */

    private int total;
    private List<ListBean> list;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 13
         * groupId : 9938
         * userId : 1041
         * groupDomainAddr : imapi.jndv.com:5080
         * groupDomainAddrUser : imapi.jndv.com
         * addTime : 1656663112000
         * joinDomainAddr : imapi.jndv.com:5080
         * joinUserid : 1049
         * approvalStatus : 2
         * modTime : null
         * groupInfo : {"addTime":"2022-06-13T09:36:03","cgName":"群聊20220526","cgPic":"","disableSendMsg":"0","groupIds":"","groupType":0,"id":9938,"isProhibition":0,"isReasonsForClosure":"","modPicTime":"2022-06-13T09:36:03","number":10,"qrPhoto":"","renzheng":0}
         */

        private int id;
        private String groupId;
        private int userId;
        private String groupDomainAddr;
        private String groupDomainAddrUser;
        private String addTime;
        private String joinDomainAddr;
        private String joinUserid;
        private String joinUserName;
        private int approvalStatus;
        private Object modTime;
        private String groupInfo;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getGroupDomainAddr() {
            return groupDomainAddr;
        }

        public void setGroupDomainAddr(String groupDomainAddr) {
            this.groupDomainAddr = groupDomainAddr;
        }

        public String getJoinUserName() {
            return joinUserName;
        }

        public void setJoinUserName(String joinUserName) {
            this.joinUserName = joinUserName;
        }

        public String getGroupDomainAddrUser() {
            return groupDomainAddrUser;
        }

        public void setGroupDomainAddrUser(String groupDomainAddrUser) {
            this.groupDomainAddrUser = groupDomainAddrUser;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getJoinDomainAddr() {
            return joinDomainAddr;
        }

        public void setJoinDomainAddr(String joinDomainAddr) {
            this.joinDomainAddr = joinDomainAddr;
        }

        public String getJoinUserid() {
            return joinUserid;
        }

        public void setJoinUserid(String joinUserid) {
            this.joinUserid = joinUserid;
        }

        public int getApprovalStatus() {
            return approvalStatus;
        }

        public void setApprovalStatus(int approvalStatus) {
            this.approvalStatus = approvalStatus;
        }

        public Object getModTime() {
            return modTime;
        }

        public void setModTime(Object modTime) {
            this.modTime = modTime;
        }

        public String getGroupInfo() {
            return groupInfo;
        }

        public void setGroupInfo(String groupInfo) {
            this.groupInfo = groupInfo;
        }
    }

    public class GroupInfo{

        /**
         * addTime : 2022-06-13T09:36:03
         * cgName : 群聊20220526
         * cgPic :
         * disableSendMsg : 0
         * groupIds :
         * groupType : 0
         * id : 9938
         * isProhibition : 0
         * isReasonsForClosure :
         * modPicTime : 2022-06-13T09:36:03
         * number : 10
         * qrPhoto :
         * renzheng : 0
         */

        private String addTime;
        private String cgName;
        private String cgPic;
        private String disableSendMsg;
        private String groupIds;
        private int groupType;
        private int id;
        private int isProhibition;
        private String isReasonsForClosure;
        private String modPicTime;
        private int number;
        private String qrPhoto;
        private int renzheng;

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getCgName() {
            return cgName;
        }

        public void setCgName(String cgName) {
            this.cgName = cgName;
        }

        public String getCgPic() {
            return cgPic;
        }

        public void setCgPic(String cgPic) {
            this.cgPic = cgPic;
        }

        public String getDisableSendMsg() {
            return disableSendMsg;
        }

        public void setDisableSendMsg(String disableSendMsg) {
            this.disableSendMsg = disableSendMsg;
        }

        public String getGroupIds() {
            return groupIds;
        }

        public void setGroupIds(String groupIds) {
            this.groupIds = groupIds;
        }

        public int getGroupType() {
            return groupType;
        }

        public void setGroupType(int groupType) {
            this.groupType = groupType;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsProhibition() {
            return isProhibition;
        }

        public void setIsProhibition(int isProhibition) {
            this.isProhibition = isProhibition;
        }

        public String getIsReasonsForClosure() {
            return isReasonsForClosure;
        }

        public void setIsReasonsForClosure(String isReasonsForClosure) {
            this.isReasonsForClosure = isReasonsForClosure;
        }

        public String getModPicTime() {
            return modPicTime;
        }

        public void setModPicTime(String modPicTime) {
            this.modPicTime = modPicTime;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public String getQrPhoto() {
            return qrPhoto;
        }

        public void setQrPhoto(String qrPhoto) {
            this.qrPhoto = qrPhoto;
        }

        public int getRenzheng() {
            return renzheng;
        }

        public void setRenzheng(int renzheng) {
            this.renzheng = renzheng;
        }
    }
}

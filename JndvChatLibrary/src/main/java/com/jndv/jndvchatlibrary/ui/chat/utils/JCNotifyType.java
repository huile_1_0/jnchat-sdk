package com.jndv.jndvchatlibrary.ui.chat.utils;

/**
 * Author: wangguodong
 * Date: 2022/5/27
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 通知消息的内容类型
 */
public class JCNotifyType {
//    public static final int WITHDRAW = 1 ;//撤回一条消息
    /**
     * 通知消息_消息已撤回
     */
    public static final int NOTIFY_msgWithdraw = 10901 ;//通知消息_消息已撤回

    /**
     * 通知消息_群消息修改
     */
    public static final int NOTIFY_UpdateGroupInfo = 10902 ;//通知消息_群消息修改

    /**
     * 通知消息_群公告发布
     */
    public static final int NOTIFY_CreateGroupNotice = 10903 ;//通知消息_群公告发布

    /**
     * 通知消息_踢出群成员
     */
    public static final int NOTIFY_GroupKicksMember = 10904 ;//通知消息_踢出群成员

    /**
     * 通知消息_群聊解散
     */
    public static final int NOTIFY_DisbandGroup = 10905 ;//通知消息_群聊解散

    /**
     * 通知消息_群成员退出群聊
     */
    public static final int NOTIFY_QuitGroupChat = 10906 ;//通知消息_群成员退出群聊

    /**
     * 通知消息_单聊消息通知
     */
    public static final int NOTIFY_SingleNotice = 10907 ;//通知消息_单聊消息通知

    /**
     * 通知消息_入群申请审核通知
     */
    public static final int NOTIFY_JoinGroupAudit = 10908 ;//通知消息_入群申请审核通知

    /**
     * 通知消息_创群成功通知
     */
    public static final int NOTIFY_CreateGroupSucced = 10909 ;//通知消息_创建群聊成功

    /**
     * 通知消息_新成员加入群聊通知
     */
    public static final int NOTIFY_NewGroupMemberAdd = 10910 ;

}

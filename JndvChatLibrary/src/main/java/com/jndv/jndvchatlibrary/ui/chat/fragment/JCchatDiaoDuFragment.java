package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.ehome.sipservice.SipServiceCommand;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jnbaseutils.chat.listUi.JCcontainer;
import com.jndv.jnbaseutils.chat.listUi.JCsessionChatInfoBean;
import com.jndv.jnbaseutils.chat.listUi.JCviewPanelInterface;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.ui.base.JNBaseFragment;
import com.jndv.jnbaseutils.ui.base.JNBaseFragmentActivity;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.utils.MediaPlayerUtil;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.MQTTManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcchatBinding;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCmsgModelItem;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgNotifyContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCNotifyType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCMessageListener;
import com.jndv.jndvchatlibrary.ui.chat.utils.listener.JCmessageListenerManager;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/2/11
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 调度通知消息页面Fragment
 */
public class JCchatDiaoDuFragment extends JNBaseFragment implements JCviewPanelInterface {
    private JCchatDiaoDuViewModel jCchatViewModel;
    private FragmentJcchatBinding binding;
    private String otherId;//对方ID，用户id或者群组id
    private String sessionaddress;//会话SIP域地址
    private String sessionaddressJava;//会话JAVA域地址
    private long readLastTime;//最新已读消息发送时间
    private String sessionType;//会话类型
    private int sessionId = 0;//会话id
    protected JCmessageListPanel messageListPanel;
    private String lastMsgId="-1";

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        jCchatViewModel =
                new ViewModelProvider(this).get(JCchatDiaoDuViewModel.class);

        binding = FragmentJcchatBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);
        EventBus.getDefault().register(this);
        return root;
    }

    private void init(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            otherId = bundle.getString(JCEntityUtils.SESSION_OTHER_ID);
            sessionType = bundle.getString(JCEntityUtils.SESSION_TYPE);
            sessionId = bundle.getInt(JCEntityUtils.SESSION_ID);
            sessionaddress = bundle.getString(JCEntityUtils.SESSION_ADDRESS);
            sessionaddressJava = bundle.getString(JCEntityUtils.SESSION_ADDRESS_JAVA);
            readLastTime = bundle.getLong(JCEntityUtils.SESSION_READ_LAST_TIME);
            jCchatViewModel.init(otherId, "" + sessionId, "" + sessionType, sessionaddress, this);
        }
        if (null == otherId) otherId = "";

        Log.e("JCchatFragment", "init: ====otherId=" + otherId);
        Log.e("JCchatFragment", "init: ====sessionType=" + sessionType);
        Log.e("JCchatFragment", "init: ====sId=" + sessionId);
        Log.e("JCchatFragment", "init: ====sessionaddress=" + sessionaddress);

//        隐藏底部输入模块及悬浮显示模块
        view.findViewById(R.id.messageToolBox).setVisibility(View.GONE);

        JCsessionChatInfoBean bean = new JCsessionChatInfoBean(otherId, sessionaddress, sessionaddressJava);
        JCcontainer container = new JCcontainer(getActivity(), "" + sessionId, sessionType, this
                , bean, 0);
        initData();
        if (messageListPanel == null) {
            messageListPanel = new JCmessageListPanel(container, view);
        } else {
//            messageListPanel.update(container,view);
        }

        String addreass = "";
        try {
            addreass = sessionaddress;
        } catch (Exception e) {
            e.printStackTrace();
        }
        String key = otherId + "_" + sessionType + "_" + addreass;
        JNLogUtil.e("JCchatFragment", "========addMessageListener=======key=" + key);
        MQTTManager.getInstance().addMqttMsgListenerList(key, new MQTTManager.MqttMsgListener() {
            @Override
            public void onReceiveMessage(JCbaseIMMessage message, int isLook) {
                JNLogUtil.e("JCchatFragment", "==onReceiveMessage==MQTTManager==01===key=" + key);
                initReceiveMessage(message);
            }
        });
    }

    private void initReceiveMessage(JCbaseIMMessage message){
        JNLogUtil.e("JCchatFragment", "==onReceiveMessage==getMsgID=" + message.getMsgID());
        if(message.getMsgID().equals(lastMsgId)){
            return;
        }
        lastMsgId=message.getMsgID();
        try {
            readLastTime = Long.parseLong(message.getSendTime());
        } catch (Exception e) {

        }
        try {
            JCimMessageBean jCimMessageBean = (JCimMessageBean) message;
            JNLogUtil.e("JCchatFragment", "==onReceiveMessage==getId=" + jCimMessageBean.getId());
            JNLogUtil.e("JCchatFragment", "==onReceiveMessage====jCimMessageBean.getMsgType()==" + jCimMessageBean.getMsgType());
            if (TextUtils.equals("" + JCmessageType.READACK, jCimMessageBean.getMsgType())) {
                //这里是已读回执，需要更新已读数量
                jCchatViewModel.updateDataRead(message);
            } else if (TextUtils.equals("" + JCmessageType.NOTIFY, message.getMsgType())) {
                jCchatViewModel.updateData(JCMessageUtils.getNotifyMsg(message));
                JCMsgNotifyContentBean bean = new Gson().fromJson(new String(Base64.decode(message.getContent(), Base64.NO_WRAP)), JCMsgNotifyContentBean.class);

                if (TextUtils.equals(bean.getType(), JCNotifyType.NOTIFY_SingleNotice + "")) {
                    jCchatViewModel.addData(message);
                    jCimMessageBean.setId(0);
                    JCobjectBox.get().boxFor(JCimMessageBean.class).put(jCimMessageBean);
                }
            } else {
                jCchatViewModel.addData(message);
                jCimMessageBean.setId(0);
                JCobjectBox.get().boxFor(JCimMessageBean.class).put(jCimMessageBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onDestroy() {
        try {
            JCSessionListBean sessionListBean = null;
            if (0 != sessionId) {
                sessionListBean = JCSessionUtil.getJCSessionListBean(sessionId);
            } else {
                sessionListBean = JCSessionUtil.getJCSessionListBean(otherId, sessionType, sessionaddress);
            }
            if (null != sessionListBean) {
                sessionListBean.setFirst_time("" + readLastTime);
                Box<JCSessionListBean> sessionBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                sessionBox.put(sessionListBean);
                QueryBuilder<JCSessionListBean> builder = sessionBox.query();
                JCSessionListBean dataBean = builder.equal(JCSessionListBean_.session_id
                        , sessionId).build().findFirst();
                EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_INFO, "", dataBean));
            }
        } catch (Exception e) {

        }
        super.onDestroy();
    }

    /**
     * 设置数据监听
     */
    private void initData(){
        if (null!=jCchatViewModel)jCchatViewModel.getDataChange().observeForever( new Observer<JCmsgModelItem>() {
            @Override
            public void onChanged(JCmsgModelItem jCmsgModelItem) {
                Log.e("http", "initData==onChanged: ==========" + new Gson().toJson(jCmsgModelItem));
                if (null!=jCmsgModelItem) {
                    if (null!=messageListPanel)messageListPanel.initData(jCmsgModelItem);
                }
            }
        });
    }

    @Override
    public void makeMethodCall(String type, Object... objects) {
        Log.e("JCchatFragment", "makeMethodCall: ====type==" + type);
        if (TextUtils.equals(type, "getMessageData")){
            long time = System.currentTimeMillis();
            jCchatViewModel.getOldMessage(time);
        }else if (TextUtils.equals(type, "getNewMessage")){
            long time = System.currentTimeMillis();
            int addType = 1 ;
            try {
                time = (long) objects[0];
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                addType = (int) objects[1];
            }catch (Exception e){
                e.printStackTrace();
            }
            jCchatViewModel.getNewMessage(time, addType);
        }else if (TextUtils.equals(type, "getOldMessage")){
            long time = System.currentTimeMillis();
            try {
                time = (long) objects[0];
            }catch (Exception e){
                e.printStackTrace();
            }
            jCchatViewModel.getOldMessage(time);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void codeEvent(JNCodeEvent codeEvent) {
        switch (codeEvent.getCode()) {
            case JNEventBusType
                    .CODE_MESSAGE_WITHDRAW://消息撤销
                messageListPanel.withdrawMessage((String) codeEvent.getData());
                break;
            case JNEventBusType.CODE_MESSAGE_CHANGE_UI://消息撤销
                messageListPanel.gotoBottomIs(300);
                break;
            case JNEventBusType
                    .CODE_MESSAGE_DELETE://消息删除
                try {
                    JNLogUtil.e("===CODE_MESSAGE_DELETE===getData==" + (String) codeEvent.getData());
                    JNLogUtil.e("===CODE_MESSAGE_DELETE===getMessage==" + codeEvent.getMessage());
                    JNLogUtil.e("===CODE_MESSAGE_DELETE===otherId==" + otherId);
                    messageListPanel.deleteMessage((String) codeEvent.getData());
                    JNLogUtil.e("===CODE_MESSAGE_DELETE===go==remove==");
                    Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                    QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
                    JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, codeEvent.getData().toString(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .build().findFirst();
                    if (null != localMessage) {
                        boolean isremove = imMsgBox.remove(localMessage);
                        JNLogUtil.e("===CODE_MESSAGE_DELETE===isremove==" + isremove);
                    }

                }catch (Exception e){
                    JNLogUtil.e("====", e);
                }

                break;
            case JNEventBusType
                    .CODE_MESSAGE_READ_USER_LIST://跳转消息已读未读人员列表
//                if (null!=jcActivityFragmentInterface){
                try {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(JCMsgReadFragment.MESSAGEITEM, (JCimMessageBean) codeEvent.getData());
                    JCMsgReadFragment contentFragment = new JCMsgReadFragment();
//                contentFragment.setArguments(bundle);
                    JNBaseFragmentActivity.start(getActivity(), contentFragment, "已读人员", bundle);
//                    JCbaseFragmentActivity.start(getActivity(), contentFragment, "已读人员", bundle);
                }catch (Exception e ){
                    JNLogUtil.e("====", e);
                }

//                }
                break;

            case JNEventBusType.CODE_MESSAGE_UPDATE:
                try {
                    Log.e("downloadFile", "onComplete: ==0010=" +codeEvent.getMessage());
//                    Log.e("codeEvent", codeEvent.getMessage());
                    JCimMessageBean bean = (JCimMessageBean) codeEvent.getData();
                    messageListPanel.updateData(bean);
                }catch (Exception e){
                    JNLogUtil.e("====", e);
                }

                break;
            case JNEventBusType.CODE_MESSAGE_TRANSMIT://
//                Log.e("codeEvent", codeEvent.getMessage());
//                transmitMessage((JCimMessageBean) codeEvent.getData());
                try {
                    JCimMessageBean messageBean = (JCimMessageBean) codeEvent.getData();
                    if (TextUtils.equals(sessionaddress, messageBean.getSessionRealm())
                            && TextUtils.equals(otherId, messageBean.getToSipID())){
                        messageListPanel.addData(messageBean);
                    }
                }catch (Exception e){
                    JNLogUtil.e("====", e);
                }

                break;

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        String key = otherId + "_" + sessionType + "_" + sessionaddress;
        JCmessageListenerManager.removeMessageListener(key);
        MQTTManager.getInstance().removeMqttMsgListenerList(key);
        EventBus.getDefault().unregister(this);
        MediaPlayerUtil.release();
        MediaPlayerUtil.getInstances().getAnimationMap().clear();
    }

    @Override
    public boolean sendMessage(JCbaseIMMessage msg) {
        if (JCSessionUtil.isGroupStateOk(sessionId)) {
            try {
                messageListPanel.gotoBottom();
                ((JCimMessageBean) msg).setContent(Base64.encodeToString(msg.getContent().getBytes(), Base64.NO_WRAP));
                JNLogUtil.e("sendMessage: ======================sendMessage=======" + "sip:" + otherId + "@" + sessionaddress);
                String userID = JNSpUtils.getString(getActivity(), "myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
                if (TextUtils.equals("" + JCmessageType.READACK, msg.getMsgType())) {
                    ((JCimMessageBean) msg).setId(0);
                    if (sessionType.equals(JCSessionType.CHAT_GROUP)) {

                        Log.d("SipService","msg==chat=01=");
                        SipServiceCommand.sendMessageToBuddy(getActivity(), new Gson().toJson(msg), userID
                                , "sip:" + otherId + "@" + sessionaddress, "text/groupmsgack+json", false);
                    } else {
                        Log.d("SipService","msg==chat=02=");
                        SipServiceCommand.sendMessageToBuddy(getActivity(), new Gson().toJson(msg), userID
                                , "sip:" + otherId + "@" + sessionaddress, "text/p2pmsgack+json", false);
                    }
                } else {
                    Box<JCimMessageBean> beanBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                    JCimMessageBean bean = ((JCimMessageBean) msg);
                    beanBox.put(bean);
                    messageListPanel.addData(bean);
                    ((JCimMessageBean) msg).setId(0);
                    Log.d("SipService","msg==chat=03=");
                    SipServiceCommand.sendMessageToBuddy(getActivity(), new Gson().toJson(msg), userID
                            , "sip:" + otherId + "@" + sessionaddress, JCSessionType.getTypeSIP(sessionType), false);
                }
//                messageListPanel.gotoBottom();
            } catch (Exception e) {
                JNLogUtil.e("==JCChatFragment==sendFileMessage==", e);
            }
        }
        return false;
    }

    @Override
    public boolean sendFileMessage(JCbaseIMMessage msg, int type) {
        if (JCSessionUtil.isGroupStateOk(sessionId)) {
            try {
                messageListPanel.gotoBottom();
                Box<JCimMessageBean> beanBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                if (0 == type) {
                    beanBox.put((JCimMessageBean) msg);
                    messageListPanel.addData(msg);
                } else {
                    String userID = JNSpUtils.getString(getActivity(), "myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
                    JCimMessageBean bean = beanBox.query().equal(JCimMessageBean_.msgID, msg.getMsgID(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                            .build().findFirst();
                    bean.setContent(msg.getContent());
                    beanBox.put(bean);
                    messageListPanel.updateData(bean);
                    ((JCimMessageBean) msg).setId(0);
                    Log.d("SipService","msg==chat=04=");
                    SipServiceCommand.sendMessageToBuddy(getActivity(), new Gson().toJson(msg), userID
                            , "sip:" + otherId + "@" + sessionaddress, JCSessionType.getTypeSIP(sessionType), false);
                }
            } catch (Exception e) {
                JNLogUtil.e("==JCChatFragment==sendFileMessage==", e);
            }
        }
        return false;
    }

    @Override
    public boolean updateMessageState(String msgID, int state) {
        try {
            JNLogUtil.d("==JCFileAction==updateMessageState==msgID==" + msgID + "==state==" + state);
            JNLogUtil.d("==JCChatFragment==updateMessageState==msgID==" + msgID + "==state==" + state);
            Box<JCimMessageBean> box = JCobjectBox.get().boxFor(JCimMessageBean.class);
            JCimMessageBean bean = box.query().equal(JCimMessageBean_.msgID, msgID, QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();
            bean.setStatus("" + state);
            box.put(bean);
            messageListPanel.updateData(bean);
        } catch (Exception e) {
            JNLogUtil.e("==JCFileAction==updateMessageState==", e);
            JNLogUtil.e("==JCChatFragment==updateMessageState==", e);
        }

        return false;
    }

    @Override
    public void addDragBtmView() {

    }

    @Override
    public void hindDragBtmView() {

    }

    @Override
    public void initDragViewData(long time) {

    }
}


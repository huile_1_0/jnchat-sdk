package com.jndv.jndvchatlibrary.ui.phone.ui;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;


public class MyFragmentAdapter extends FragmentPagerAdapter {
    private List<Fragment> dataSource;

    public MyFragmentAdapter(FragmentManager fm, List<Fragment> dataSource) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.dataSource = dataSource;
    }

    @Override
    public Fragment getItem(int position) {
        return dataSource.get(position);
    }

    @Override
    public int getCount() {
        if (dataSource != null) {
            return dataSource.size();
        }
        return 0;
    }
}

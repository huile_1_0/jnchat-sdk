package com.jndv.jndvchatlibrary.ui.chat.bean;

/**
 * Author: wangguodong
 * Date: 2022/5/12
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 用户个人二维码信息
 */
public class JCUserQRcodeBean {
    private String username ;
    private String userhead ;
    private String sipId ;
    private String sipAddress ;
    private String javaAddress ;

    public JCUserQRcodeBean(String username, String userhead
            , String sipId, String sipAddress
            , String javaAddress) {
        this.username = username;
        this.userhead = userhead;
        this.sipId = sipId;
        this.sipAddress = sipAddress;
        this.javaAddress = javaAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserhead() {
        return userhead;
    }

    public void setUserhead(String userhead) {
        this.userhead = userhead;
    }

    public String getSipId() {
        return sipId;
    }

    public void setSipId(String sipId) {
        this.sipId = sipId;
    }

    public String getSipAddress() {
        return sipAddress;
    }

    public void setSipAddress(String sipAddress) {
        this.sipAddress = sipAddress;
    }

    public String getJavaAddress() {
        return javaAddress;
    }

    public void setJavaAddress(String javaAddress) {
        this.javaAddress = javaAddress;
    }

}

package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
* @Title: JCMsgTextContentBean.java
* @Description: 文字消息内容类
* @author SunQinzheng
* @date 2022/2/19 上午 11:07
*/

public class JCMsgTextContentBean {
    /**
     * "extend":"",//扩展字段（暂无意义）
     *"text":"",//文字文本
     * "fontSize":0,//文字字体大小
     * "fontColor":0,//文字字体颜色
     * "fontType":0,//文字字体类型
     * "reply":0,//是否回复别的消息
     * "replyContent":{//被回复消息的内容
     * }
     */
    private String text;
    private int fontSize;
    private String fontColor;
    private int fontType;
    private int reply;
    private String replyContent;
    private String fromName;//TODO 这里为什么加这个？？外层数据中已经存在的字段，不知道什么时候被谁在这里加了新的，确认无用后删除

    public JCMsgTextContentBean() {
    }

    public JCMsgTextContentBean(String text) {
        this.text = text;
        this.fontSize=16;
        this.fontColor="#222222";
        this.fontType=0;
        this.reply=0;
        this.replyContent="";
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public int getFontType() {
        return fontType;
    }

    public void setFontType(int fontType) {
        this.fontType = fontType;
    }

    public int getReply() {
        return reply;
    }

    public void setReply(int reply) {
        this.reply = reply;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    @Override
    public String toString() {
        return "JCMsgTextContentBean{" +
                "text='" + text + '\'' +
                ", fontSize=" + fontSize +
                ", fontColor='" + fontColor + '\'' +
                ", fontType=" + fontType +
                ", reply=" + reply +
                ", replyContent='" + replyContent + '\'' +
                '}';
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.viewmodel;

import android.text.TextUtils;
import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNGetUserDetails;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipVideoActivity;
import com.jndv.jndvchatlibrary.http.api.JCDelFriendApi;
import com.jndv.jndvchatlibrary.http.api.JCGetUserDetailsApi;
import com.jndv.jndvchatlibrary.http.api.JCUpdateUserHeadApi;
import com.jndv.jndvchatlibrary.http.api.JCUpdateUserInfoApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCAdministratorKicksMemberApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGroupMemberInvitePerUpdateApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserDetailBean;


public class JCUserDetailViewModel extends ViewModel {

    private static final String TAG = "JCUserDetailViewModel";
    private MutableLiveData<JNGetUserDetails.UserDetailBean> userData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean> respondData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean> updateData = new MutableLiveData<>();
    private String domainAddr = "192.168.2.205:8000";
    private ComponentActivity activity;

    public JCUserDetailViewModel() {
    }

    public void init(ComponentActivity activity) {
        this.activity = activity;
    }

    public MutableLiveData<JNGetUserDetails.UserDetailBean> getUserData() {
        return userData;
    }

    public MutableLiveData<JCRespondBean> getRespondData() {
        return respondData;
    }
    public MutableLiveData<JCRespondBean> getUpdateData() {
        return updateData;
    }


    public JCUserDetailViewModel getUserDetail(String userId) {
        JNBaseHttpUtils.getInstance().GetUserDetailMore(
                EasyHttp.post(activity), userId, new OnHttpListener<JNGetUserDetails.Bean>() {
                    @Override
                    public void onSucceed(JNGetUserDetails.Bean result) {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            userData.postValue(result.getData());
                        }
                    }

                    @Override
                    public void onFail(Exception e) {

                    }
                });
        return this;
    }

    public JCUserDetailViewModel updateUserInfo(String pic) {
        JCUpdateUserHeadApi api = new JCUpdateUserHeadApi()
                .setUserId(JNBasePreferenceSaves.getUserSipId())
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setPicName(JNBasePreferenceSaves.getUserName())
                .setPicUrl(pic);

        EasyHttp.put(activity)
                .api(api)
                .json(new Gson().toJson(api)).request(new OnHttpListener<JCRespondBean<String>>() {
                    @Override
                    public void onSucceed(JCRespondBean<String> result) {
                        updateData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        JCChatManager.showToast("操作失败！");
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });

        return this;
    }

    public JCUserDetailViewModel deleteFrend(int frendId, String friendDomainAddr) {

        JCDelFriendApi api = new JCDelFriendApi().setOperationUserId(String.valueOf(JNBasePreferenceSaves.getUserSipId()))
                .setDomainAddr(domainAddr).setFriendId(String.valueOf(frendId)).setFriendDomainAddr(friendDomainAddr);

        EasyHttp.delete(activity)
                .api(api
                )
                .request(new OnHttpListener<JCRespondBean>() {
                    @Override
                    public void onSucceed(JCRespondBean result) {
                        respondData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });

        return this;
    }

    /*
    踢出群聊
     */
    public JCUserDetailViewModel kicksMember(String groupId, String groupDomainAddr, String frendId, String friendDomainAddr) {

        JCAdministratorKicksMemberApi api = new JCAdministratorKicksMemberApi(
                String.valueOf(JNBasePreferenceSaves.getUserSipId()),
                JNBasePreferenceSaves.getSipAddress(),
                groupId,
                groupDomainAddr,
                frendId,
                friendDomainAddr
        );
        EasyHttp.put(activity)
                .api(api)
                .json(new Gson().toJson(api))
                .request(new OnHttpListener<JCRespondBean>() {
                    @Override
                    public void onSucceed(JCRespondBean result) {
                        respondData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });

        return this;
    }

    /*
       更新用户邀请权限
        */
    public JCUserDetailViewModel invitePerUpdate(String groupId, String groupDomainAddr, String processedDomainAddr, String processedUsersId, String invitePermission) {
        JCGroupMemberInvitePerUpdateApi api = new JCGroupMemberInvitePerUpdateApi(
                JNBasePreferenceSaves.getUserSipId(),
                JNBasePreferenceSaves.getSipAddress(),
                groupId,
                groupDomainAddr,
                processedDomainAddr,
                processedUsersId,
                invitePermission
        );
        EasyHttp.put(activity)
                .api(api)
                .json(new Gson().toJson(api))
                .request(new OnHttpListener<JCRespondBean>() {
                    @Override
                    public void onSucceed(JCRespondBean result) {
//                        respondData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });

        return this;
    }


}
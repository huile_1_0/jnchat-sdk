package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCmsgModelItem;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCMessageReadUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jnbaseutils.chat.listUi.JCcontainer;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;
import com.jndv.jnbaseutils.chat.JCSessionType;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/2/11
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 聊天页面中，消息列表展示组件
 */
public class JCmessageListPanel implements OnRefreshLoadMoreListener {
    private JCcontainer container;
    private View rootView;
    private RecyclerView messageListView;
//    private JCRecyclerView messageListView;
    private SmartRefreshLayout listRefresh;
    private ImageView image_chat_bg ;
    private List<JCbaseIMMessage> items = new ArrayList<>();
    private JCmessageAdapter adapter;
    private int mFirstVisiblePosition = 0 ;
    private int mLastVisiblePosition = 0 ;
    private boolean isBottom = true; //显示位置是否是最底部，即最新一条消息

    public JCmessageListPanel(JCcontainer container, View rootView) {
        this.container = container;
        this.rootView = rootView;
        init();
    }

    /**
     * 初始化
     */
    private void init(){
        messageListView = rootView.findViewById(R.id.recycle_view);
        listRefresh = rootView.findViewById(R.id.list_refresh);
//        setChatBg();//设置聊天背景
        messageListView.requestDisallowInterceptTouchEvent(true);
        messageListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                JNLogUtil.e("==onScrollStateChanged=====01====="+newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE || newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    // DES: 找出当前可视Item位置
                    JNLogUtil.e("==onScrollStateChanged=====02====="+newState);
//                    JCchatManager.showToast("发送已读回执：RecyclerView.SCROLL_STATE_IDLE" );
                    updateMsgReadState();
                }
                if (newState != RecyclerView.SCROLL_STATE_IDLE) {//界面滑动时需要将底部菜单及键盘都收起
//                    container.jcviewPanelInterface.shouldCollapseInputPanel();
                }
            }
        });
        adapter = new JCmessageAdapter(messageListView, items);
        initRefreshRecyclerView();
        container.jcviewPanelInterface.makeMethodCall("getMessageData");
    }

    public void setRefreshEnable(boolean b){
        try {
            listRefresh.setEnableRefresh(b);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void setLoadMoreEnable(boolean b){
        try {
            listRefresh.setEnableLoadMore(b);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 初始化列表刷新控件
     */
    private void initRefreshRecyclerView() {
        setLoadMoreEnable(false);
        messageListView.setLayoutManager(new LinearLayoutManager(container.activity));
        listRefresh.setOnRefreshLoadMoreListener(this);
        messageListView.setAdapter(adapter);
    }

    /**
     * 设置数据监听
     */
    public void initData(JCmsgModelItem jCmsgModelItem){
//        container.jCchatViewModel.getDataChange().observeForever( new Observer<JCmsgModelItem>() {
//            @Override
//            public void onChanged(JCmsgModelItem jCmsgModelItem) {
                if (null!=jCmsgModelItem){
                    if (items.size() <= 0 && null!=jCmsgModelItem.getDatas()&&jCmsgModelItem.getDatas().size() > 0) {
                        items.addAll(jCmsgModelItem.getDatas());
                        adapter.notifyDataSetChanged();
                        stopRefresh(true);
                        gotoBottom(300);
                    }else switch (jCmsgModelItem.getType()){
                        case 0://0=增(item=null时会使用datas数据重置列表数据)；
                            if (null!=jCmsgModelItem.getItem()){
                                Log.e("Bottom", "onChanged: ======？？====isBottom="+isBottom);
                                addData(jCmsgModelItem.getItem());
                                Log.e("Bottom", "onChanged: ==========isBottom="+isBottom);
                            }else {
                                if (jCmsgModelItem.getDatas().size()>= JCChatManager.pageSize){
                                    items.clear();
                                    setLoadMoreEnable(true);
                                }
                                items.addAll(jCmsgModelItem.getDatas());
                                adapter.notifyDataSetChanged();
                                gotoBottom(300);
                            }
                            break;
                        case 1://1=删；
                            if (jCmsgModelItem.getIndex()<items.size()){
                                items.remove(jCmsgModelItem.getIndex());
                                adapter.notifyItemRemoved(jCmsgModelItem.getIndex());
                                adapter.notifyItemRangeRemoved(jCmsgModelItem.getIndex(),items.size()-1);
                            }
                            break;
                        case 2://2=改；
                                JNLogUtil.e("JCchatFragment", "==JCMessageUtils==getDataChange=002=="+jCmsgModelItem.getItem().getMsgID());
                            if (-1==jCmsgModelItem.getIndex()){
                                for (int i = 0; i < items.size(); i++) {
                                JNLogUtil.e("JCchatFragment", "JCMessageUtils==getDataChange===002=="+items.get(i).getMsgID());
                                    if (TextUtils.equals(items.get(i).getMsgID(), jCmsgModelItem.getItem().getMsgID())){
                                        items.set(i,jCmsgModelItem.getItem());
                                        adapter.notifyItemChanged(i);
                                        break;
                                    }
                                }
                            }else if (jCmsgModelItem.getIndex()<items.size()){
                                items.set(jCmsgModelItem.getIndex(),jCmsgModelItem.getItem());
                                adapter.notifyItemChanged(jCmsgModelItem.getIndex());
                                }
                            break;
                        case 3://3=增（datas增加到顶部add(0,datas)）；
                            try {
                                Log.e("TAG", "onChanged: ====observeForever====3====001=========");
//                                if (null!=jCmsgModelItem.getDatas()&&jCmsgModelItem.getDatas().size()>0){
//                                    items.addAll(0, jCmsgModelItem.getDatas());
//                                    adapter.notifyItemRangeInserted(0, jCmsgModelItem.getDatas().size());
//                                    messageListView.scrollToPosition(jCmsgModelItem.getDatas().size()+(mLastVisiblePosition-mFirstVisiblePosition));
//                                }
                                if (null!=jCmsgModelItem.getDatas()&&jCmsgModelItem.getDatas().size()>0){
                                     List<JCimMessageBean> datas = new ArrayList<>();
                                     try {
                                         container.jcviewPanelInterface.initDragViewData(Long.parseLong(jCmsgModelItem.getDatas().get(0).getSendTime()));
                                     }catch (Exception e){
                                         JNLogUtil.e("==getDataChange==3==time=", e);
                                     }
                                    Log.e("TAG", "onChanged: ====observeForever====3====002=========");
                                    for (int i = 0; i < jCmsgModelItem.getDatas().size(); i++) {
                                        Log.e("TAG", "onChanged: ====observeForever====3====003=========");
                                        boolean isHave = false;
                                        if (items.size()>0)for (int j = 0; j < items.size(); j++) {
                                            try {
                                                if (TextUtils.equals(items.get(j).getMsgID(), jCmsgModelItem.getDatas().get(i).getMsgID())){
                                                    items.set(j,jCmsgModelItem.getDatas().get(i));
                                                    adapter.notifyItemChanged(j);
                                                    isHave = true;
                                                    break;
                                                }
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }
                                        if (!isHave&&null!=jCmsgModelItem.getDatas().get(i))datas.add(jCmsgModelItem.getDatas().get(i));
                                    }
                                    Log.e("TAG", "onChanged: ====observeForever====3====004=========");

                                    if (datas.size()>0){
                                        Log.e("TAG", "onChanged: ====observeForever====3====005========="+datas.size());

                                        items.addAll(0, datas);
                                        adapter.notifyItemRangeInserted(0, datas.size());
                                        messageListView.scrollToPosition(datas.size()+(mLastVisiblePosition-mFirstVisiblePosition));
                                    }
                                }else {
                                    try {
                                        container.jcviewPanelInterface.initDragViewData(-1);
                                    }catch (Exception e){
                                        JNLogUtil.e("==getDataChange==3==time=", e);
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            stopRefresh(true);
                            break;
                        case 4://4=增(增到底部)；
                            try {
//                                items.addAll(jCmsgModelItem.getDatas());
//                                adapter.notifyItemRangeInserted(items.size()-1, jCmsgModelItem.getDatas().size());
                                if (null!=jCmsgModelItem.getDatas()&&jCmsgModelItem.getDatas().size()>0){
                                    if (jCmsgModelItem.getDatas().size()< JCChatManager.pageSize)setLoadMoreEnable(false);
                                    List<JCimMessageBean> datas = new ArrayList<>();
                                    for (int i = 0; i < jCmsgModelItem.getDatas().size(); i++) {
                                        boolean isHave = false;
                                        for (int j = 0; j < items.size(); j++) {
                                            if (TextUtils.equals(items.get(j).getMsgID(), jCmsgModelItem.getDatas().get(i).getMsgID())){
                                                items.set(j,jCmsgModelItem.getDatas().get(i));
                                                adapter.notifyItemChanged(j);
                                                isHave = true;
                                                break;
                                            }
                                        }
                                        if (!isHave)datas.add(jCmsgModelItem.getDatas().get(i));
                                    }
                                    items.addAll(items.size(), datas);
                                    adapter.notifyItemRangeInserted(items.size(), datas.size());
//                                    messageListView.scrollToPosition(datas.size()+(mLastVisiblePosition-mFirstVisiblePosition));
                                }else {
                                    setLoadMoreEnable(false);
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            stopRefresh(true);
                            break;
                        case 5://5=加载失败
                            stopRefresh(false);
//                            Toast.makeText(container.activity, "加载失败！", Toast.LENGTH_SHORT).show();
                            break;
                        case 6://6=改（改已读状态）；
//                            if (-1==jCmsgModelItem.getIndex()){
                            JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll===01===14============");
                            try {
                                JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll===02===14============");
//                                循环消息列表
                                if (items.size()>0)for (int i = 0; i < items.size(); i++) {
//                                    存在消息
                                    JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll===03===14============");
                                    if (TextUtils.equals(items.get(i).getMsgID(), jCmsgModelItem.getItem().getContent())){
                                        JCimMessageBean messageBean = (JCimMessageBean) items.get(i);
//                                        readNum==-1时是群聊已经全部已读；私聊时直接+1就行
                                        JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======14============");
                                        if (messageBean.getRead_num()!=-1 && container.sessionType==JCSessionType.CHAT_GROUP){
                                            if (messageBean.getReadNumAll() <=0){
                                                JCMessageReadUtils.updateGroupMsgReadAll(container.activity, messageBean, new JCMessageReadUtils.JCMessageReadListenner() {
                                                    @Override
                                                    public void onUpdateMessage(JCbaseIMMessage message) {
                                                        JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======13============");
                                                        messageBean.setReadNumAll(message.getReadNumAll());
                                                        int read = messageBean.getRead_num()+1;
                                                        if (read>=messageBean.getReadNumAll()){
                                                            messageBean.setReadNum("-1");
                                                        }else {
                                                            messageBean.setReadNum(""+read);
                                                        }
                                                        updateData(messageBean);
                                                    }
                                                });
                                            }else {
                                                JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======15============");
                                                int read = messageBean.getRead_num()+1;
                                                if (read>=messageBean.getReadNumAll()){
                                                    messageBean.setReadNum("-1");
                                                }else {
                                                    messageBean.setReadNum(""+read);
                                                }
                                            }
                                        }else {
                                            int read = messageBean.getRead_num()+1;
                                            messageBean.setReadNum(""+read);
                                        }
                                        JNLogUtil.e("JCMessageReadUtils","==updateGroupMsgReadAll======16============");
                                        items.set(i,messageBean);
                                        adapter.notifyItemChanged(i);
                                        break;
                                    }
                                }
                            }catch (Exception e){
                                JNLogUtil.e("消息列表===6==",e);
                            }

//                            }else if (jCmsgModelItem.getIndex()<items.size()){
//                                items.set(jCmsgModelItem.getIndex(),jCmsgModelItem.getItem());
//                                adapter.notifyItemChanged(jCmsgModelItem.getIndex());
//                            }
                            break;
                        case 7://7=获取离线消息，首次进入，底部加载禁用
                            items.clear();
                            items.addAll(jCmsgModelItem.getDatas());
                            adapter.notifyDataSetChanged();
                            gotoBottom(300);
                            break;
                        case 8://8=获取未读消息最早一页
                            items.clear();
                            items.addAll(jCmsgModelItem.getDatas());
                            adapter.notifyDataSetChanged();
                            messageListView.smoothScrollToPosition(0);
                            setLoadMoreEnable(true);
//                            gotoBottom();
                            break;
                    }
                    updateMsgReadState();
                }
//            }
//        });

    }

    /**
     * 停止刷新操作
     */
    private void stopRefresh(boolean b){
        listRefresh.finishRefresh();
        listRefresh.finishLoadMore();
    }

    /**
     * 获取当前页面显示消息数量
     * 去发送已读消息
     */
    private void updateMsgReadState(){
        if (JCSessionUtil.isGroupStateOk(Integer.parseInt(container.sessionId), false)){
            RecyclerView.LayoutManager layoutManager = messageListView.getLayoutManager();
            if (layoutManager instanceof LinearLayoutManager) {
                LinearLayoutManager linearManager = (LinearLayoutManager) layoutManager;
                int firstPos = linearManager.findFirstVisibleItemPosition();
                int lastPos = linearManager.findLastVisibleItemPosition();
                if (mFirstVisiblePosition!=firstPos || mLastVisiblePosition!=lastPos){
                    mFirstVisiblePosition = linearManager.findFirstVisibleItemPosition();
                    mLastVisiblePosition = linearManager.findLastVisibleItemPosition();
                    JNLogUtil.e("Bottom", "updateMsgReadState: ==========First="+mFirstVisiblePosition);
                    JNLogUtil.e("Bottom", "updateMsgReadState: ==========Last="+mLastVisiblePosition);
                    JNLogUtil.e("Bottom", "updateMsgReadState: ==========size="+(items.size()));
                    if(mLastVisiblePosition<0 || mLastVisiblePosition>=items.size()-1){
                        isBottom = true;
                        container.jcviewPanelInterface.hindDragBtmView();
                    }else {
                        isBottom = false;
                    }
                    sendMsgRead();
                }
            }
        }
    }

    /**
     * 判断当前页面显示消息条数
     * 发送已读消息
     */
    private void sendMsgRead(){
        JNLogUtil.e("==sendMsgRead=====01====="+mFirstVisiblePosition);
        JNLogUtil.e("==sendMsgRead=====02====="+mLastVisiblePosition);
//        JCchatManager.showToast("发送已读回执：mFirstVisiblePosition="+mFirstVisiblePosition+"==mLastVisiblePosition=="+mLastVisiblePosition );
        if (mFirstVisiblePosition<0)mFirstVisiblePosition=0;
        if (mLastVisiblePosition - mFirstVisiblePosition >0){
            JNLogUtil.e("==sendMsgRead=====03=====");
            for (int i = mFirstVisiblePosition; i <= mLastVisiblePosition ; i++) {
                JNLogUtil.e("==sendMsgRead=====04=====" + i);
                try {
                    JNLogUtil.e("=sendMsgRead=发送已读回执：i="+i+"==items.get(i).getRead_num()=="+items.get(i).getRead_num() );
                    JNLogUtil.e("=sendMsgRead=发送已读回执：JNBasePreferenceSaves.getUserSipId()="+JNBasePreferenceSaves.getUserSipId()+"==items.get(i).getFromSipID()=="+items.get(i).getFromSipID() );
                    if (null!=items.get(i)&&0==items.get(i).getRead_num() && !TextUtils.equals(items.get(i).getFromSipID(), JNBasePreferenceSaves.getUserSipId())){
                        JCimMessageBean jCimMessageBean  = (JCimMessageBean) items.get(i);
                        JNLogUtil.e("==sendMsgRead=====05=====" + i);
                        if (!jCimMessageBean.getFromSipID().equals(null== JNBasePreferenceSaves.getUserSipId()?"":JNBasePreferenceSaves.getUserSipId())){
                            JNLogUtil.e("==sendMsgRead=====06=====" + i);
                            jCimMessageBean.setRead_num(items.get(i).getRead_num()+1);

                            QueryBuilder<JCimMessageBean> builder = JCobjectBox.get().boxFor(JCimMessageBean.class).query();
                            JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, jCimMessageBean.getMsgID(),QueryBuilder.StringOrder.CASE_SENSITIVE)
//                        .equal(JCimMessageBean_.sessionType, sessionType,QueryBuilder.StringOrder.CASE_SENSITIVE)
                                    .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                                    .build().findFirst();
                            if (null!=localMessage){
                                jCimMessageBean.setId(localMessage.getId());
                                //更新数据库中这条消息的已读状态
                                JCobjectBox.get().boxFor(JCimMessageBean.class).put(jCimMessageBean);
                            }

                            //去发送已读消息
//                        String userID = JCSpUtils.getString(container.activity,"myAccount", JCPjSipConstants.PJSIP_NUMBER_DEFAULT);

                            JCimMessageBean readMessageBean = JCchatFactory.creatIMMessage(
                                    jCimMessageBean.getMsgID(),""+ JCmessageType.READACK, container.jCsessionChatInfoBean.getSessionChatId()
                                    , JNSpUtils.getString(container.activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
                                    , JNBasePreferenceSaves.getSipAddress(), jCimMessageBean.getFromSipID(), jCimMessageBean.getFromRealm()
                                    , ""+container.sessionType, JNBasePreferenceSaves.getJavaAddress(),jCimMessageBean.getToRealmJava()
                                    , jCimMessageBean.getSessionRealm(), jCimMessageBean.getSessionRealmJava());
                            JNLogUtil.e("==sendMsgRead=====07=====" + jCimMessageBean.getMsgID());
                            JNLogUtil.e("==sendMsgRead=====08=====" + new Gson().toJson(readMessageBean));
//                            JCchatManager.showToast("发送已读回执：" + jCimMessageBean.getMsgID());
//                        container.jcviewPanelInterface.sendMessage(readMessageBean);
//                            JCChatManager.showToast("==SEND=="+readMessageBean.getFromSipID()+"==to=="+readMessageBean.getToSipID());
                            JCMessageUtils.sendACKMessage(readMessageBean, container.activity);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    JNLogUtil.e("发送已读回执：sendMsgRead 时Exception=",e);
//                    JCchatManager.showToast("发送已读回执：sendMsgRead时Exception="+e.getMessage() );
                }
            }
        }
    }

    /**
     * 撤回消息
     * @param msgId
     */
    public void withdrawMessage(String msgId){
        try {
            if (null!=items && items.size()>0){
                JCimMessageBean jCimMessageBean = null;
                for (int i = items.size()-1; i >= 0; i--) {
                    if(TextUtils.equals(msgId, items.get(i).getMsgID())){
                        jCimMessageBean = (JCimMessageBean) items.get(i);
                        jCimMessageBean.setMsgType(""+JCmessageType.WITHDRAW);
                        items.set(i, jCimMessageBean);
                        adapter.notifyItemChanged(i);
                    }
                }
                if (null!=jCimMessageBean){
                    JNLogUtil.e("===withdrawMessage==message=msgId=="+jCimMessageBean.getMsgID());
                    JCSessionListBean sessionListBean = JCSessionUtil.getJCSessionListBean(jCimMessageBean.getSessionId(),jCimMessageBean.getSessionType(),jCimMessageBean.getToRealm());
                    JCimMessageBean sessionMsg = new Gson().fromJson(sessionListBean.getFirst_content(),JCimMessageBean.class);
                    JNLogUtil.e("===withdrawMessage==session=getFirst_content=="+sessionListBean.getFirst_content());
                    if (null!=sessionMsg.getContent()){
                        String decodeContent = new String(Base64.decode(sessionMsg.getContent(),Base64.NO_WRAP));
                        JNLogUtil.e("===withdrawMessage==session=decodeContent=="+decodeContent);
                    }
                    JNLogUtil.e("===withdrawMessage==sessionMsg=msgId=="+sessionMsg.getMsgID());
                    jCimMessageBean.setMsgType(""+JCmessageType.WITHDRAW);
                    if(TextUtils.equals(sessionMsg.getMsgID(), jCimMessageBean.getMsgID())){
                        EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_SESSION_LIST_UPDATE_MSG
                                , ""+sessionListBean.getSession_id(), jCimMessageBean));
                    }
                }
            }
        }catch (Exception e){
            JNLogUtil.e("==JCmessageListPanel==withdrawMessage==",e);
        }
    }

    /**
     * 删除消息
     * @param msgId
     */
    public void deleteMessage(String msgId){
        JNLogUtil.e("===deleteMessage===msgId=="+msgId);
        try {
            if (null!=items && items.size()>0){
                JCbaseIMMessage message =null;
                JCbaseIMMessage messageOld =null;
                for (int i = items.size()-1; i >= 0; i--) {
                    if(TextUtils.equals(msgId, items.get(i).getMsgID())){
                        message = items.get(i);
                        if (i>0){
                            messageOld = items.get(i-1);
                        }
                        items.remove(i);
                        adapter.notifyItemRemoved(i);
                    }
                }
                JNLogUtil.e("===deleteMessage==02=msgId=="+msgId);
                if (null!=message){
                    JNLogUtil.e("===deleteMessage==message=msgId=="+message.getMsgID());
                    String address = message.getFromRealm();
                    if (TextUtils.equals(message.getFromSipID(), JNBasePreferenceSaves.getUserSipId())){
                        address = message.getToRealm();
                    }
                    JCSessionListBean sessionListBean = JCSessionUtil.getJCSessionListBean(message.getSessionId(),message.getSessionType(),address);
                    JCimMessageBean sessionMsg = new Gson().fromJson(sessionListBean.getFirst_content(),JCimMessageBean.class);
                    JNLogUtil.e("===deleteMessage==session=getFirst_content=="+sessionListBean.getFirst_content());
                    if (null!=sessionMsg.getContent()) {
                        String decodeContent = new String(Base64.decode(sessionMsg.getContent(),Base64.NO_WRAP));
                        JNLogUtil.e("===deleteMessage==session=decodeContent=="+decodeContent);
                    }
                    JNLogUtil.e("===deleteMessage==sessionMsg=msgId=="+sessionMsg.getMsgID());
                    if(TextUtils.equals(sessionMsg.getMsgID(), message.getMsgID())){
                        if (null==messageOld){
                            EventBus.getDefault().post(new JNCodeEvent(JNEventBusType.CODE_SESSION_LIST_UPDATE_NET,"删除消息"));
                        }else {
                            EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_SESSION_LIST_UPDATE_MSG
                                    , ""+sessionListBean.getSession_id(), messageOld));
                        }
                    }
                }
            }
        }catch (Exception e){
            JNLogUtil.e("==JCmessageListPanel==deleteMessage==",e);
        }
    }

    public void gotoBottomIs(long time){
        if (isBottom)gotoBottom(time);
    }
    /**
     * 列表滚动到最后一条数据
     */
    public void gotoBottom(){
        gotoBottom(0);
    }
    public void gotoBottom(long time){
        try {
            isBottom = true;
            JCThreadManager.onMainHandler(new Runnable() {
                @Override
                public void run() {
                    if (items.size()>0)messageListView.smoothScrollToPosition(items.size());
                }
            }, time);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 添加一条消息到最底部
     * 220325调整
     * @param message
     */
    public void addData(JCbaseIMMessage message){
        items.add(message);
        adapter.notifyItemInserted(items.size());
//        gotoBottom();
        if (isBottom){
            gotoBottom();
        }else {
            //N条新消息功能
//            if (!TextUtils.equals(message.getFromSipID(), JNBasePreferenceSaves.getUserSipId())
//                    && !TextUtils.equals(message.getFromRealm(), JNBasePreferenceSaves.getSipAddress()))
            container.jcviewPanelInterface.addDragBtmView();
        }
    }

    /**
     * 更新某一条消息
     * 所有信息
     * @param message
     */
    public void updateData(JCbaseIMMessage message){
        try {
            if (items.size()>0){
                for (int i = 0; i < items.size(); i++) {
                    if (TextUtils.equals(message.getMsgID(), items.get(i).getMsgID())){
                        items.set(i, message);
                        adapter.notifyItemChanged(i);
                        return;
                    }
                }
            }
        }catch (Exception e){
            JNLogUtil.e("====JCmessageListPanel==updateData==", e);
        }
    }

    @Override
    public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {
        long time = System.currentTimeMillis();
        try {
            time = Long.parseLong(items.get(items.size()-1).getSendTime());
        }catch (Exception e){
            e.printStackTrace();
        }
        container.jcviewPanelInterface.makeMethodCall("getNewMessage", time, 1);
    }

    @Override
    public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
        long time = System.currentTimeMillis();
        try {
            time = Long.parseLong(items.get(0).getSendTime())-10;
        }catch (Exception e){
            e.printStackTrace();
        }
        container.jcviewPanelInterface.makeMethodCall("getOldMessage", time);
    }
}

package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.text.TextUtils;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCSessionListApi;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCSessionListSelecteBean;
import com.jndv.jnbaseutils.chat.JCSessionType;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/4/20
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCMsgTransmitViewModel extends ViewModel {
    private Fragment fragment;

    private MutableLiveData<JCsessionSelecteModelItem> sessionList = new MutableLiveData<>();;

    public JCMsgTransmitViewModel(Fragment fragment) {
        sessionList = new MutableLiveData<>();
        this.fragment = fragment ;
    }

    public MutableLiveData<JCsessionSelecteModelItem> getSessionList() {
        return sessionList;
    }

    public void getChatListData() {
        QueryBuilder<JCSessionListBean> builder = JCobjectBox.get().boxFor(JCSessionListBean.class).query();
        List<JCSessionListBean> dataLocal = builder
                .equal(JCSessionListBean_.type, JCSessionType.CHAT_PERSION,QueryBuilder.StringOrder.CASE_SENSITIVE)
                .equal(JCSessionListBean_.type, JCSessionType.CHAT_GROUP,QueryBuilder.StringOrder.CASE_SENSITIVE)
                .orderDesc(JCSessionListBean_.last_time)//降序
                .build().find();
        if (null==dataLocal||dataLocal.size()<=0){
            getChatListData(1);
        }else {
            JCsessionSelecteModelItem jCsessionListModelItem = new JCsessionSelecteModelItem();
            jCsessionListModelItem.setPage(1);
            jCsessionListModelItem.setType(0);
            jCsessionListModelItem.setDatas(initData(dataLocal));
            sessionList.setValue(jCsessionListModelItem);
        }

    }
    private void getChatListData(int page) {
        EasyHttp.get(fragment)
                .api(new JCSessionListApi()
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                        .setPageNo(page)
                        .setPageSize(JCChatManager.pageSizeSession)
                )
                .request(new OnHttpListener<JCSessionListApi.Bean>() {
                    @Override
                    public void onSucceed(JCSessionListApi.Bean result) {
                        if (result==null||result.getData()==null){
                            JCsessionSelecteModelItem jCsessionListModelItem = new JCsessionSelecteModelItem();
                            jCsessionListModelItem.setPage(page);
                            jCsessionListModelItem.setType(1);
                            sessionList.setValue(jCsessionListModelItem);
                            return;
                        }
                        if (!TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            JCsessionSelecteModelItem jCsessionListModelItem = new JCsessionSelecteModelItem();
                            jCsessionListModelItem.setPage(page);
                            jCsessionListModelItem.setType(1);
                            sessionList.setValue(jCsessionListModelItem);
                        }else {
                            List<JCSessionListBean> data = new ArrayList<>();
                            Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
//                            if (page==1)data.add(getSession());
                            for (int i = 0; i < result.getData().size(); i++) {
                                QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
                                JCSessionListBean dataBean = result.getData().get(i);
                                JCSessionListBean messageBean = builder.equal(JCSessionListBean_.session_id
                                        , dataBean.getSession_id()).build().findFirst();
                                if (null!=messageBean){
                                    dataBean.setId(messageBean.getId());
                                }
                                imMsgBox.put(dataBean);
                                data.add(dataBean);
                            }
                            JCsessionSelecteModelItem jCsessionListModelItem = new JCsessionSelecteModelItem();
                            jCsessionListModelItem.setPage(page);
                            jCsessionListModelItem.setType(0);
                            data = sortData(data);

                            jCsessionListModelItem.setDatas(initData(data));
                            sessionList.setValue(jCsessionListModelItem);
                            if (result.getData().size()>=JCChatManager.pageSizeSession){
                                getChatListData(page+1);
                            }
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        JCsessionSelecteModelItem jCsessionListModelItem = new JCsessionSelecteModelItem();
                        jCsessionListModelItem.setPage(page);
                        jCsessionListModelItem.setType(1);
                        sessionList.setValue(jCsessionListModelItem);
                    }
                });
    }

    private List<JCSessionListSelecteBean> initData(List<JCSessionListBean> mList) {
        List<JCSessionListSelecteBean> list = new ArrayList<>();
        if (null!=mList && mList.size()>0){
            for (int i = 0; i < mList.size(); i++) {
                JCSessionListSelecteBean bean = new JCSessionListSelecteBean(mList.get(i), false);
                list.add(bean);
            }
        }
        return list;
    }

    private List<JCSessionListBean> sortData(List<JCSessionListBean> mList) {
        try {
            Collections.sort(mList, new Comparator<JCSessionListBean>() {
                @Override
                public int compare(JCSessionListBean o1, JCSessionListBean o2) {
                    if (o1.getLast_time() < o2.getLast_time()){
                        return 1;
                    }
                    return -1;
                }
            });
            List<JCSessionListBean> list1 = new ArrayList();
            List<JCSessionListBean> list2 = new ArrayList();
            for (int i = 0; i < mList.size(); i++) {
                if (1==mList.get(i).getIs_top()){
                    list1.add(mList.get(i));
                }else {
                    list2.add(mList.get(i));
                }
            }
            mList.clear();
            mList.addAll(list1);
            mList.addAll(list2);
        }catch (Exception e){
            e.printStackTrace();
        }
        return mList;
    }

    public class JCsessionSelecteModelItem {

        private List<JCSessionListSelecteBean> datas ;//新的数据列表
        private int page;//
        private int type;//0=成功；1=失败

        public List<JCSessionListSelecteBean> getDatas() {
            return datas;
        }

        public void setDatas(List<JCSessionListSelecteBean> datas) {
            this.datas = datas;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }
    }

}

package com.jndv.jndvchatlibrary.ui.chat.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;

import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.crowd.utils.DensityUtils;
import com.mingyuechunqiu.recordermanager.data.bean.RecorderOption;
import com.mingyuechunqiu.recordermanager.feature.record.IRecorderManager;
import com.mingyuechunqiu.recordermanager.feature.record.RecorderManagerProvider;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.util.UUID;

import io.reactivex.functions.Consumer;

/**
 * 录音按钮
 * Created by xuan on 2016/6/8.
 */
public class JCrecordButton extends AppCompatButton {
    private final int Volume_What_100 = 100;
    private final int Time_What_101 = 101;
    private final int CancelRecordWhat_102 = 102;
    private String mFilePath = "";
    private String mFileName = "";
    /**
     * 文件路径
     **/
    private String mFile;
    private OnFinishedRecordListener finishedListener;
    /**
     * 最短录音时间
     **/
    private int MIN_INTERVAL_TIME = 1000;
    /**
     * 最长录音时间
     **/
    private int MAX_INTERVAL_TIME = 1000 * 60;
    private long mStartTime;
    private Dialog mDialog;
    private ImageView mImageView;
    private TextView mTitleTv, mTimeTv;
//    private MediaRecorder mRecorder;
    private IRecorderManager mRecorderManager;
    private ObtainDecibelThread mthread;
    private Handler mVolumeHandler;
    private int CANCLE_LENGTH = -200;// 默认上滑取消距离
    private boolean ismaxtimefinish ; //是否是超出时间结束的
    /**
     * 文件名前缀
     */
    private String mPrefix = "jndvchat";
    /***
     * 时间太短提示语
     */
    private String tooShortToastMessage = "";

    private Context mContext;
    private boolean isPermissionOk = false ;
    private boolean isGoNext = false ;

    public JCrecordButton(Context context) {
        super(context);
        init(context);
    }

    public JCrecordButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public JCrecordButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @SuppressLint("CheckResult")
    private void permissionStartRecord(){
        try {
            RxPermissions rxPermissions=new RxPermissions((Activity) mContext);
            rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ,Manifest.permission.READ_EXTERNAL_STORAGE
                    ,Manifest.permission.RECORD_AUDIO
            )
                    .subscribe(new Consumer<Boolean>() {
                        @SuppressLint("CheckResult")
                        @Override
                        public void accept(Boolean aBoolean) {
                            if (aBoolean){
                                //申请的权限全部允许
                                isPermissionOk = true ;
                                if (isGoNext)initDialogAndStartRecord();
                            }else{
                                //只要有一个权限被拒绝，就会执行
//                    Toast.makeText(MainActivity.this, "未授权权限，部分功能不能使用", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 保存路径，为空取默认值
     *
     * @param path
     */
    public void setSavePath(String path) {
        if (!TextUtils.isEmpty(path)) {
            mFilePath = path;
        }
    }

    /**
     * 设置保存的名字
     *
     * @param pName
     */
    public void setSaveName(String pName) {
        if (!TextUtils.isEmpty(pName)) {
            mFileName = pName;
        }
    }

    /**
     * 设置文件名前缀，文件命名：前缀+UUID
     *
     * @param pPrefix
     */
    public void setPrefix(String pPrefix) {
        if (!TextUtils.isEmpty(pPrefix)) {
            mPrefix = pPrefix;
        }
    }

    /**
     * 设置默认路径
     *
     * @return
     */
    private String getDefaultPath() {
        return getContext().getExternalFilesDir("audio").getAbsolutePath();
    }

    /***
     * 设置默认名字
     *
     * @return
     */
    private String getDefaultName() {
//        return mPrefix + "_" + UUID.randomUUID().toString() + ".aac";//这里综合考虑还是用这个吧
        return mPrefix + "_" + UUID.randomUUID().toString() + ".mp3";//音频上传似乎不支持aac!!!
    }

    /****
     * 设置最大时间
     *
     * @param time 单位毫秒
     */
    public void setMaxIntervalTime(int time) {
        MAX_INTERVAL_TIME = time;
    }

    /**
     * 设置最短录音时间
     *
     * @param time 时间毫秒
     */
    public void setMinIntervalTime(int time) {
        MIN_INTERVAL_TIME = time;
    }


    public void setTooShortToastMessage(String pTooShortToastMessage) {
        tooShortToastMessage = pTooShortToastMessage;
    }

    /**
     * 录音完成的回调
     *
     * @param listener
     */
    public void setOnFinishedRecordListener(OnFinishedRecordListener listener) {
        finishedListener = listener;
    }

    private void init(Context context) {
        mContext = context;
        mVolumeHandler = new ShowVolumeHandler();
    }

    int startY = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                startY = (int) event.getY();
                isGoNext = true;
                permissionStartRecord();
                break;
            case MotionEvent.ACTION_UP:
                if (isPermissionOk){
                    int endY = (int) event.getY();
                    if (startY < 0)
                        return true;
                    if (endY - startY < CANCLE_LENGTH) {
                        cancelRecord();
                    } else {
                        if (!ismaxtimefinish){
                            finishRecord();
                        }
                    }
                    ismaxtimefinish = false ;
                }
                isGoNext = false;
                break;
            case MotionEvent.ACTION_MOVE:
                if (isPermissionOk){
                    int tempNowY = (int) event.getY();
                    if (startY < 0)
                        return true;
                    if (tempNowY - startY < CANCLE_LENGTH) {
                        mTitleTv.setText(mContext.getString(R.string.zeffect_recordbutton_releasing_finger_to_cancal_send));
                    } else {
                        mTitleTv.setText(mContext.getString(R.string.zeffect_recordbutton_finger_up_to_cancal_send));
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
//                cancelRecord();
//                isGoNext = false;
                //适配部分小米手机
                handleTouchAction(event);
                break;
        }

        return true;
    }

    private void handleTouchAction(MotionEvent event){
        if (isPermissionOk){
            int endY = (int) event.getY();
            if (startY < 0)
                return;
            if (endY - startY < CANCLE_LENGTH) {
                cancelRecord();
            } else {
                if (!ismaxtimefinish){
                    finishRecord();
                }
            }
            ismaxtimefinish = false ;
        }
        isGoNext = false;
    }

    private void initDialogAndStartRecord() {
        CANCLE_LENGTH = -(this.getMeasuredHeight() / 2);
        //
        String tempFilePath = "";
        if (TextUtils.isEmpty(mFilePath)) {
            tempFilePath = getDefaultPath();
        } else {
            tempFilePath = mFilePath;
        }
        String tempFileName = "";
        if (TextUtils.isEmpty(mFileName)) {
            tempFileName = getDefaultName();
        } else {
            tempFileName = mFileName;
        }
        mFile = tempFilePath + "/" + tempFileName;
        mRecorderManager = RecorderManagerProvider.newInstance();
        mDialog = new Dialog(getContext(), R.style.custom_dialog);
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.jc_recordbutton_alert_dialog, null);
        mImageView = (ImageView) contentView.findViewById(R.id.zeffect_recordbutton_dialog_imageview);
        mTimeTv = (TextView) contentView.findViewById(R.id.zeffect_recordbutton_dialog_time_tv);
        mTitleTv = (TextView) contentView.findViewById(R.id.zeffect_recordbutton_dialog_title_tv);
        mDialog.setContentView(contentView, new LinearLayout.LayoutParams(DensityUtils.dip2px(getContext(),200), DensityUtils.dip2px(getContext(),200)));
        mDialog.setOnDismissListener(onDismiss);
        mDialog.setCancelable(false);
        startRecording();
        mStartTime = System.currentTimeMillis();
        mDialog.show();
    }

    private void finishRecord() {
        stopRecording();
        mDialog.dismiss();
        long intervalTime = System.currentTimeMillis() - mStartTime;
        if (intervalTime < MIN_INTERVAL_TIME) {
            if (TextUtils.isEmpty(tooShortToastMessage)) {
                Toast.makeText(getContext(), getContext().getResources().getString(R.string.recordbutton_time_too_short), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), tooShortToastMessage, Toast.LENGTH_SHORT).show();
            }
            File file = new File(mFile);
            if (file.exists())
                file.delete();
            return;
        }
        if (finishedListener != null)
            finishedListener.onFinishedRecord(mFile);
    }

    private void cancelRecord() {
        stopRecording();
        mDialog.dismiss();
        File file = new File(mFile);
        if (file.exists())
            file.delete();
    }

    private void startRecording() {
        mRecorderManager.recordAudio(new RecorderOption.Builder()
                .setAudioSource(MediaRecorder.AudioSource.MIC)
                .setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
//                .setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
                .setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
                .setAudioSamplingRate(44100)
                .setBitRate(96000)
//                .setMaxDuration(6000)
                .setFilePath(mFile)
                .build());
        mthread = new ObtainDecibelThread();
        mthread.start();
    }

    private void stopRecording() {
        if (mthread != null) {
            mthread.exit();
            mthread = null;
        }
        mRecorderManager.release();
    }

    private class ObtainDecibelThread extends Thread {
        private volatile boolean running = true;
        public void exit() {
            running = false;
        }
        @Override
        public void run() {
            while (running) {
                try {
//                    TODO 这里要考虑电量消耗
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (mRecorderManager == null || !running) {
                    break;
                }
                if (System.currentTimeMillis() - mStartTime >= MAX_INTERVAL_TIME) {
                    // 如果超过最长录音时间
                    mVolumeHandler.sendEmptyMessage(CancelRecordWhat_102);
                    ismaxtimefinish = true ;
                }else {
                    //发送时间
                    mVolumeHandler.sendEmptyMessage(Time_What_101);
                    //
                    int x = mRecorderManager.getMediaRecorder().getMaxAmplitude();
                    if (x != 0) {
                        int f = (int) (20 * Math.log(x) / Math.log(10));
                        Message msg = new Message();
                        msg.obj = f;
                        msg.what = Volume_What_100;
                        mVolumeHandler.sendMessage(msg);
                    }
                }
            }
        }

    }

    private DialogInterface.OnDismissListener onDismiss = new DialogInterface.OnDismissListener() {

        @Override
        public void onDismiss(DialogInterface dialog) {
            stopRecording();
        }
    };

    class ShowVolumeHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Volume_What_100:
                    int tempVolumeMax = (int) msg.obj;
                    setLevel(tempVolumeMax);
                    break;
                case Time_What_101:
                    long nowTime = System.currentTimeMillis();
                    int time = ((int) (nowTime - mStartTime) / 1000);
                    int second = time % 60;
                    int mil = time / 60;
                    if (mil < 10) {
                        if (second < 10)
                            mTimeTv.setText("0" + mil + ":0" + second);
                        else
                            mTimeTv.setText("0" + mil + ":" + second);
                    } else if (mil >= 10 && mil < 60) {
                        if (second < 10)
                            mTimeTv.setText(mil + ":0" + second);
                        else
                            mTimeTv.setText(mil + ":" + second);
                    }
                    break;
                case CancelRecordWhat_102:
                    finishRecord();
                    break;
            }
        }
    }

    private void setLevel(int level) {
        if (mImageView != null)
            mImageView.getDrawable().setLevel(4000 + 6000 * level / 90);
    }

    public interface OnFinishedRecordListener {
        void onFinishedRecord(String audioPath);
//
//        /**
//         * 手指上滑，准备取消录音
//         **/
//        void readCancel();
//
//        /**
//         * 手指回退，准备继续录音
//         **/
//        void noCancel();
//
//        /***
//         * 手指按下回调
//         */
//        void onActionDown();
//
//        /***
//         * 手指抬起回调
//         */
//        void onActionUp();
//
//        /***
//         * 手指移动回调
//         */
//        void onActionMove();

    }
}

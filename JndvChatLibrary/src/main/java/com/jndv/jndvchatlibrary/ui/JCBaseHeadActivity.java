package com.jndv.jndvchatlibrary.ui;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;

import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.wega.library.loadingDialog.LoadingDialog;
import com.zhy.autolayout.AutoLinearLayout;
import com.zhy.autolayout.AutoRelativeLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * 简单返回的头部
 */

public abstract class JCBaseHeadActivity extends FragmentActivity {
    protected TextView titleTv;
    protected TextView titleRight;
    protected ImageView titleBack;
    protected ImageView iv_loading;
    protected View v_line_top_10;
    protected LinearLayout ll_title_all;
    protected ImageView menu;
    protected AutoRelativeLayout top_chat_root;
    protected AutoLinearLayout back;

    private LoadingDialog loadingDialog = null;
    private boolean isShowTitle = true;
    /**
     * 显示加载框
     */
    public void showLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.loading();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==showLoadding==",e);
        }
    }

    /**
     * 直接取消加载框
     */
    public void cancelLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.cancel();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==cancelLoadding==",e);
        }
    }

    /**
     * 显示加载成功后取消加载框
     */
    public void cancelSuccessLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.loadSuccess();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==cancelSuccessLoadding==",e);
        }
    }

    /**
     * 显示加载失败后取消加载框
     */
    public void cancelFailLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.loadFail();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==cancelFailLoadding==",e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.jc_basehead_activity);
        initBaseView();
        loadingDialog = new LoadingDialog(this);
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        initLoadingImg();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (iv_loading.getVisibility() == View.VISIBLE){
                iv_loading.setBackgroundResource(com.jndv.jnbaseutils.R.drawable.ic_loading_tm);
                iv_loading.clearAnimation();
                iv_loading.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initLoadingImg(){
        try {
            JNLogUtil.e("==111==onPrepareOptionsMenu====DisConnect=00=isIsConnect="+ !JNBaseUtilManager.isIsConnect());
            if (!JNBaseUtilManager.isIsConnect()){
                if (JNBaseUtilManager.getTitleState() == JNBaseConstans.TITLE_STATE_LIGHT)
                    iv_loading.setBackgroundResource(com.jndv.jnbaseutils.R.drawable.jn_loading_black);
                else
                    iv_loading.setBackgroundResource(com.jndv.jnbaseutils.R.drawable.jn_loading_16);

                if (iv_loading.getVisibility() == View.GONE){
                    Animation operatingAnim = AnimationUtils.loadAnimation(this, com.jndv.jnbaseutils.R.anim.jn_anim_loading);
                    operatingAnim.setInterpolator(new LinearInterpolator());
                    iv_loading.startAnimation(operatingAnim);
                    iv_loading.setVisibility(View.VISIBLE);
                }
            }else {
                if (iv_loading.getVisibility() == View.VISIBLE){
                    iv_loading.setBackgroundResource(com.jndv.jnbaseutils.R.drawable.ic_loading_tm);
                    iv_loading.clearAnimation();
                    iv_loading.setVisibility(View.GONE);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View.inflate(this, layoutResID, (ViewGroup) findViewById(R.id.base_content));
    }
    @Override
    public void setContentView(View view) {
        ((ViewGroup)findViewById(R.id.base_content)).addView(view);
    }

    public void goneTopLine(){
        try {
            v_line_top_10.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void codeEvent(JNCodeEvent codeEvent) {
        switch (codeEvent.getCode()) {
            case JNEventBusType
                    .CODE_SIP_LOGIN_OUT://异地登录
                finish();
                break;
            case JNEventBusType.CODE_SIP_CONNECT_OK://
            case JNEventBusType.CODE_SIP_CONNECT_NO://
            case JNEventBusType.CODE_VPN_CONNECT_OK://
            case JNEventBusType.CODE_VPN_CONNECT_NO://
                initLoadingImg();
                break;
        }
    }

    public boolean isShowTitle() {
        return isShowTitle;
    }

    public void setShowTitle(boolean showTitle) {
        isShowTitle = showTitle;
    }

    /**
     * 调用后 才能得到titleTv否则为空
     */
    private void initBaseView() {
        ll_title_all = (LinearLayout) findViewById(R.id.ll_title_all);
        back = (AutoLinearLayout) findViewById(R.id.back);
        top_chat_root = (AutoRelativeLayout) findViewById(R.id.top_chat_root);
        titleTv = (TextView) findViewById(R.id.title_content);
        titleBack = (ImageView) findViewById(R.id.iv_back);
        iv_loading = (ImageView) findViewById(com.jndv.jnbaseutils.R.id.iv_loading);
        v_line_top_10 = findViewById(R.id.v_line_top_10);

        menu = (ImageView) findViewById(R.id.menu);
        BaseTitleClick baseTitleClick = new BaseTitleClick();
        back.setOnClickListener(baseTitleClick);
        titleRight = (TextView) findViewById(R.id.right);
        if(titleRight!=null){
            titleRight.setEnabled(true);
            titleRight.setOnClickListener(baseTitleClick);
        }
        titleTv.setOnClickListener(baseTitleClick);
        menu.setOnClickListener(baseTitleClick);
        if (isShowTitle()){
            ll_title_all.setVisibility(View.VISIBLE);
            initTitle();
        }else {
            ll_title_all.setVisibility(View.GONE);
        }

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void initTitle() {
        top_chat_root.setBackgroundColor(getResources().getColor(JCChatManager.getTitleColor()));
        titleTv.setTextColor(getResources().getColor(JCChatManager.getTitleTVColor()));
        titleRight.setTextColor(getResources().getColor(JCChatManager.getTitleTVColor()));
        titleBack.setBackground(getResources().getDrawable(JCChatManager.getTitleIVBack()));
        menu.setBackground(getResources().getDrawable(JCChatManager.getTitleIVMenu()));
    }
    /**
     * 显示标题栏
     */
    public void showTitle() {
        setShowTitle(true);
        if (ll_title_all != null) {
            ll_title_all.setVisibility(View.VISIBLE);
        }
    }
    /**
     * 隐藏标题栏
     */
    public void hindTitle() {
        setShowTitle(false);
        if (ll_title_all != null) {
            ll_title_all.setVisibility(View.GONE);
        }
    }

    /**
     * 设置中间标题
     *
     * @param titleText
     */
    public void setCenterTitle(String titleText) {
        if (titleText != null) {
            if (titleTv != null) {
                titleTv.setText(titleText);
            }
        }
    }

    /**
     * @param text
     * @param drawableRes 设置右侧的按钮，可显示文字或图片
     */
    public void setTitleRight(String text, Drawable drawableRes) {
        if (titleRight == null) {
            return;
        }
        if (text == null && drawableRes == null) {
            titleRight.setVisibility(View.GONE);
        } else {
            titleRight.setVisibility(View.VISIBLE);
        }
        if (text != null) {
            titleRight.setText(text);
            //titleRight.setBackgroundResource(R.color.colorTransparents);
        }
        if (drawableRes != null) {
            titleRight.setBackgroundDrawable(drawableRes);
            titleRight.setText("");
        }

    }

    /**
     * 标题按钮的点击事件
     */
    private class BaseTitleClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.iv_back) {
                onBackClick();
            } else if (id == R.id.back) {
                onBackClick();
            } else if (id == R.id.right) {
                onRightClick();
            }else if (id ==R.id.title_content){
                onTitleClick();
            }else if (id ==R.id.menu){
                onMenuClick();
            }
        }
    }

    /**
     * 显示menue按钮，
     */
    protected void showMenuClick(boolean b) {
        if (b)menu.setVisibility(View.VISIBLE);
        else menu.setVisibility(View.GONE);
    }
    /**
     * menue点击事件，
     */
    protected void onMenuClick() {
    }

    /**
     * 标题Title，
     */
    protected void onTitleClick() {
    }

    /**
     * 标题中右边的部分，
     */
    protected void onRightClick() {
    }

    /**
     * 返回按钮的点击事件
     */
    public void onBackClick() {
        finish();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
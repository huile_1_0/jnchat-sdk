package com.jndv.jndvchatlibrary.ui.phone.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipP2PActivity;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipVideoActivity;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.ui.base.BaseFragment;
import com.jndv.jnbaseutils.chat.JCSipCallRecordBean;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCChatActivity;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCCreateSessionUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jndvchatlibrary.ui.phone.adapter.PjSipRecordAdapter;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.Collections;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;
import io.reactivex.functions.Consumer;


public final class AllCallFragment extends BaseFragment implements PjSipRecordAdapter.CallListClickListener {
    private static final String TAG = "AllCallFragment";
    RecyclerView recyclerRecoder;
    TextView allEmptyTv;
    private List<JCSipCallRecordBean> dBsipRecordList;
    private PjSipRecordAdapter adapter;
    private LinearLayoutManager layoutManager;
    private static final int UPDATE = 1;

    public static AllCallFragment newInstance() {
        return new AllCallFragment();
    }

    @Override
    public void addBroadcast(IntentFilter filter) {

    }

    @Override
    public void receiveBroadcast(Intent intent) {

    }



    @Override
    public void receiveMessage(Message msg) {
        switch (msg.what) {
            case UPDATE:
                Log.e(TAG, "receiveMessage: " + msg.obj);
                if (!dBsipRecordList.isEmpty()) {
                    dBsipRecordList.clear();
                }

                Box<JCSipCallRecordBean> callRecordBeanBox = JCobjectBox.get().boxFor(JCSipCallRecordBean.class);
                QueryBuilder<JCSipCallRecordBean> builder = callRecordBeanBox.query();
                List<JCSipCallRecordBean> recordList = builder.build().find();

                dBsipRecordList.addAll(recordList);
                Collections.reverse(dBsipRecordList);
                if (dBsipRecordList.isEmpty()){
                    allEmptyTv.setVisibility(View.VISIBLE);
                }else {
                    allEmptyTv.setVisibility(View.GONE);
                }
                adapter.notifyDataSetChanged();
                break;
        }

    }

    @Override
    protected int setLayoutResourceID() {
        return R.layout.fragment_call;
    }

    @Override
    protected void initView(View view) {
        allEmptyTv = view.findViewById(R.id.all_empty_tv);
        recyclerRecoder = view.findViewById(R.id.recycler_recoder);

        Box<JCSipCallRecordBean> callRecordBeanBox = JCobjectBox.get().boxFor(JCSipCallRecordBean.class);
        QueryBuilder<JCSipCallRecordBean> builder = callRecordBeanBox.query();
        dBsipRecordList = builder.build().find();

        Collections.reverse(dBsipRecordList);
        Log.e(TAG, "initView: " + dBsipRecordList.size());
        if (dBsipRecordList.isEmpty()){
            allEmptyTv.setVisibility(View.VISIBLE);
        }else {
            allEmptyTv.setVisibility(View.GONE);
        }
        layoutManager = new LinearLayoutManager(getmContext());
        recyclerRecoder.setLayoutManager(layoutManager);
        adapter = new PjSipRecordAdapter(getmContext(), dBsipRecordList,this);
        recyclerRecoder.setAdapter(adapter);
    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ");
        Message.obtain(mHandler, UPDATE, UPDATE).sendToTarget();
    }

    @Override
    public void onCallListVoiceItemClick(String friendNum) {
        RxPermissions rxPermissions=new RxPermissions(getActivity());
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                ,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO)
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean) {
                            //申请的权限全部允许
                            String mySipNumber = JNBasePreferenceSaves.getUserSipId();
                            String friendSipNumber = friendNum;
                            if (TextUtils.isEmpty(mySipNumber)) {
                                Toast.makeText(getActivity(), "请先配置sip账号！", Toast.LENGTH_SHORT).show();
                            } else {
                                if (mySipNumber.equals(friendSipNumber)) {
                                    Toast.makeText(getActivity(), "无法和自己通话！", Toast.LENGTH_SHORT).show();
                                } else {
                                    if(JCChatManager.isConnect()){
                                        Intent intent = new Intent(getActivity(), JCPjSipP2PActivity.class);
                                        intent.putExtra("tag", "outing");
                                        intent.putExtra("number", friendSipNumber.replace(" ", ""));
                                        getActivity().startActivity(intent);
                                    }else {
                                        Toast.makeText(getmContext(), "sip注册失败，请稍后拨打", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void onCallListVideoItemClick(String friendNum) {
        RxPermissions rxPermissions=new RxPermissions(getActivity());
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                ,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO)
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean) {
                            String mySipNumber = JNBasePreferenceSaves.getUserSipId();
                            String friendSipNumber = friendNum;
                            if (TextUtils.isEmpty(mySipNumber)) {
                                Toast.makeText(getActivity(), "请先配置sip账号！", Toast.LENGTH_SHORT).show();
                            } else {
                                if (mySipNumber.equals(friendSipNumber)) {
                                    Toast.makeText(getActivity(), "无法和自己通话！", Toast.LENGTH_SHORT).show();
                                } else {
                                    if(JCChatManager.isConnect()){
                                        Intent intent = new Intent(getActivity(), JCPjSipVideoActivity.class);
                                        intent.putExtra("tag", "outing");
                                        intent.putExtra("number", friendSipNumber.replace(" ", ""));
                                        getActivity().startActivity(intent);
                                    }else {
                                        Toast.makeText(getmContext(), "sip注册失败，请稍后拨打", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                        }
                    }
                });
    }

    @Override
    public void onCallListMsgItemClick(String friendNum) {
        JCSessionListBean messageBean = JCSessionUtil.getJCSessionListBean(
                friendNum, JCSessionType.CHAT_PERSION, JNBasePreferenceSaves.getSipAddress());
        if (null==messageBean){
            new XPopup.Builder(getActivity()).asConfirm("提醒", "还没聊过天，是否现在就去？",
                    new OnConfirmListener() {
                        @Override
                        public void onConfirm() {
                            try {
                                JCCreateSessionUtils.creatSession(getActivity(), friendNum
                                        , JNBasePreferenceSaves.getSipAddress(), JNBasePreferenceSaves.getJavaAddress()
                                        , "" + JCSessionType.CHAT_PERSION, friendNum, "", true);
                            }catch (Exception e){e.printStackTrace();}
                        }
                    }).show();
        }else {//去聊天或者去人员详情
            JCChatActivity.start(getActivity(), messageBean);
        }
    }
}

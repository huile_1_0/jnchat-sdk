package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.text.TextUtils;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.http.api.JCGroupInvitationApprovalApi;
import com.jndv.jndvchatlibrary.http.api.JCGroupInvitationApprovalUpdateApi;
import com.jndv.jndvchatlibrary.http.api.JCJoinGroupAuditApi;
import com.jndv.jndvchatlibrary.http.api.JCJoinGroupAuditUpdateApi;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCInvitationApprovalBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCJoinGroupAuditBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;


import java.util.List;

/**
 * 消息通知相关
 */
public class JCMsgNotifyViewModel extends ViewModel {
    private Fragment fragment;

    private MutableLiveData<List<JCInvitationApprovalBean.ListBean>> approvalList = new MutableLiveData<>();
    ;
    private MutableLiveData<List<JCJoinGroupAuditBean.ListBean>> joinList = new MutableLiveData<>();
    private MutableLiveData<String> approvalResult = new MutableLiveData<>();
    private MutableLiveData<String> joinResult = new MutableLiveData<>();
    ;

    public JCMsgNotifyViewModel(Fragment fragment) {
        this.fragment = fragment;

    }

    public MutableLiveData<List<JCInvitationApprovalBean.ListBean>> getApprovalList() {
        return approvalList;
    }

    public MutableLiveData<List<JCJoinGroupAuditBean.ListBean>> getJoinList() {
        return joinList;
    }

    public MutableLiveData<String> getApprovalResult() {
        return approvalResult;

    }

    public MutableLiveData<String> getJoinResult() {
        return joinResult;
    }

    /**
     * 分页获取入群邀请审核记录
     *
     * @param page
     */
    public void getNotifyListData(int approvalStatus, int page) {
//        String host = JCSpUtils.getString(fragment.getContext(), JCPjSipConstants.PJSIP_HOST, JCPjSipConstants.PJSIP_HOST_DEFAULT);
//        String port = JCSpUtils.getString(fragment.getContext(), JCPjSipConstants.PJSIP_PORT, JCPjSipConstants.PJSIP_PORT_DEFAULT);

        JCGroupInvitationApprovalApi api = new JCGroupInvitationApprovalApi(
                JNBasePreferenceSaves.getSipAddress(),
                JNBasePreferenceSaves.getUserSipId(),
                String.valueOf(approvalStatus),
                page, JCChatManager.pageSize);

        EasyHttp.get(fragment)
//                .api(new Gson().toJson(api))
                .api(api)
                .request(new OnHttpListener<JCRespondBean<JCInvitationApprovalBean>>() {
                    @Override
                    public void onSucceed(JCRespondBean<JCInvitationApprovalBean> result) {
                        if (result != null && TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                            JCInvitationApprovalBean data = result.getData();
                            approvalList.postValue(data.getList());
                        } else {
                            approvalList.postValue(null);
                        }

                    }

                    @Override
                    public void onFail(Exception e) {
                        approvalList.postValue(null);
                    }
                });
    }

    /**
     * 分页获取新用户入群审核记录
     * 该请求发送的请求域名地址为群聊所在域地址，非操作人(特指非同一域的管理员)所在域地址
     *
     * @param auditStatus
     * @param groupDomainAddr
     * @param groupDomainAddrUser
     * @param page
     */
    public void getJoinAuditListData(int auditStatus, String groupId, String groupDomainAddr, String groupDomainAddrUser, int page) {

        JCJoinGroupAuditApi api = new JCJoinGroupAuditApi(
                JNBasePreferenceSaves.getSipAddress(),
                JNBasePreferenceSaves.getJavaAddress(),
                JNBasePreferenceSaves.getUserSipId(),
                groupId,
                String.valueOf(auditStatus),
                groupDomainAddr, groupDomainAddrUser,
                page, JCChatManager.pageSize);
        EasyHttp.get(fragment)
                .api(api)
                .request(new OnHttpListener<JCRespondBean<JCJoinGroupAuditBean>>() {
                    @Override
                    public void onSucceed(JCRespondBean<JCJoinGroupAuditBean> result) {
                        if (result != null && TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                            JCJoinGroupAuditBean data = result.getData();
                            joinList.postValue(data.getList());
                        } else {
                            approvalList.postValue(null);
                        }

                    }

                    @Override
                    public void onFail(Exception e) {
                        joinList.postValue(null);
                    }
                });
    }


    /**
     * 用户审核是否接收邀请加入群聊。
     *
     * @param approvalStatus
     * @param id
     */
    public void updateGroupInvitationApproval(int approvalStatus, int id) {

        JCGroupInvitationApprovalUpdateApi api = new JCGroupInvitationApprovalUpdateApi(
                JNBasePreferenceSaves.getUserSipId(),
                JNBasePreferenceSaves.getSipAddress(),
                JNBasePreferenceSaves.getJavaAddress(),
                String.valueOf(approvalStatus),
                String.valueOf(id));
        EasyHttp.put(fragment)
                .api(api)
                .json(new Gson().toJson(api))
                .request(new OnHttpListener<JCRespondBean<String>>() {
                    @Override
                    public void onSucceed(JCRespondBean<String> result) {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                            approvalResult.postValue("0");
                        } else {
                            approvalResult.postValue(result.getMsg());
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        approvalResult.postValue(null);
                    }
                });
    }

    /**
     * 群主或管理员审核是否同意用户加入群聊。
     * 该请求发送的请求域名地址为群聊所在域地址，非操作人(特指非同一域的管理员)所在域地址
     *
     * @param groupId
     * @param auditStatus
     * @param id
     */
    public void updateJoinGroupAudit(String groupId, int auditStatus, int id) {
        JCJoinGroupAuditUpdateApi api = new JCJoinGroupAuditUpdateApi(
                JNBasePreferenceSaves.getUserSipId(),
                JNBasePreferenceSaves.getSipAddress(),
                JNBasePreferenceSaves.getJavaAddress(),
                groupId,
                String.valueOf(auditStatus),
                String.valueOf(id));
        EasyHttp.put(fragment)
                .api(api)
                .json(new Gson().toJson(api))
                .request(new OnHttpListener<JCRespondBean<String>>() {
                    @Override
                    public void onSucceed(JCRespondBean<String> result) {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())) {
                            joinResult.postValue("200");
                        } else {
                            joinResult.postValue(null);
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        joinResult.postValue(null);
                    }
                });
    }
}

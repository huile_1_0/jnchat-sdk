package com.jndv.jndvchatlibrary.ui.crowd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.ActivityJcCrowdNumbersBinding;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCUserDetailActivity;
import com.jndv.jnbaseutils.ui.JNSimpleDividerDecoration;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCGroupNumbersBean;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCCrowdNumbersModel;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * 群成员页面
 */
public class JCCrowdMembersActivity extends JCBaseHeadActivity {

    private ActivityJcCrowdNumbersBinding binding;
    JCCrowdNumbersModel model;
    private CommonAdapter adapter;
    private List<JCGroupNumbersBean> datas = new ArrayList<>();

    private String crowdId;
    private String groupDomainAddr;
    private int type;//2:群主

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityJcCrowdNumbersBinding.inflate(getLayoutInflater(), (ViewGroup) findViewById(R.id.base_content), true);
        model=new JCCrowdNumbersModel(this);
        initIntent();
        initView();
        initData();
    }

    private void initIntent() {
        Intent intent = getIntent();
        crowdId = intent.getStringExtra(JCEntityUtils.CROWD_ID);
        groupDomainAddr = intent.getStringExtra(JCEntityUtils.GROUP_DOMAIN_ADDR);
        type=intent.getIntExtra(JCEntityUtils.IS_FROM_CROWD,0);
    }

    private void initView() {
        setCenterTitle("群成员");
        binding.rvNumbers.setLayoutManager(new LinearLayoutManager(this));
        JNSimpleDividerDecoration divider = new JNSimpleDividerDecoration(this, getResources().getColor(R.color.gray_line));
        divider.setPadding(30, 20);
//        divider.setColor();
        binding.rvNumbers.addItemDecoration(divider);

        adapter = new CommonAdapter<JCGroupNumbersBean>(this, R.layout.jc_item_friend, datas) {
            @Override
            protected void convert(ViewHolder holder, JCGroupNumbersBean adapterDataBean, int position) {
                Log.e("initData",adapterDataBean.toString());
                JCglideUtils.loadCircleImage(mContext, adapterDataBean.getAvatar(), holder.getView(R.id.iv_user_head));
                ((TextView) holder.getView(R.id.tv_user_name)).setText(getName(adapterDataBean));

                holder.setOnClickListener(holder.itemView.getId(), v -> {
                    JCUserDetailActivity.start(mContext,adapterDataBean.getUser_id(),crowdId,adapterDataBean.getDomain_addr(),groupDomainAddr,type,adapterDataBean.getInvite_permission());
                });
            }
        };
        binding.rvNumbers.setAdapter(adapter);
    }

    private String getName(JCGroupNumbersBean adapterDataBean){
        String name = adapterDataBean.getNickname();
        if (null==name|| TextUtils.isEmpty(name)){
            name = adapterDataBean.getUser_id();
        }
        return name;
    }

    private void initData() {
        model.getGroupAllData().observe(this, jcGroupNumbersBeans -> {
            datas.clear();
            datas.addAll(jcGroupNumbersBeans);

            Log.e("initData",datas.size()+"+++");
            adapter.notifyDataSetChanged();
        });
        model.requestGroupAllData(crowdId,groupDomainAddr);
    }
}
package com.jndv.jndvchatlibrary.ui.chat.utils.listener;

import android.text.TextUtils;
import android.util.Log;

import com.ehome.manager.JNPjSip;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jnbaseutils.chat.JCSessionType;

/**
 * Author: wangguodong
 * Date: 2022/3/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: PJSIP全局监听工具类,这里要使用全局的工具类，方便一些公共的消息处理。
 */
public class JFmyPjSipMessageListener implements JNPjSip.PjSipMessageListener{

    @Override
    public void onInstantMessage(String msg, String from_uri, String to_uri) {
        JNLogUtil.e("DemoMainActivity", "sendMsgRead====msg: "+ msg);
        JNLogUtil.e("DemoMainActivity", "JCmyPjSipMessageListener===onInstantMessage++" + from_uri + "===" + msg + "====" + to_uri);
        try {
            if (TextUtils.equals("kickoff", msg)){
                String tosip = to_uri.substring(to_uri.indexOf(":")+1,to_uri.indexOf("@"));
                JNLogUtil.e("DemoMainActivity", "tosip: "+ tosip);
                JNLogUtil.e("DemoMainActivity", "JNBasePreferenceSaves.getUserSipId(): "+ JNBasePreferenceSaves.getUserSipId());
                if (TextUtils.equals(tosip, JNBasePreferenceSaves.getUserSipId()))
                    JCChatManager.loginOutOther();
                return;
            }
            JCimMessageBean jCimMessageBean = new Gson().fromJson(msg, JCimMessageBean.class);
            jCimMessageBean.setStatus(""+JCmessageStatusType.sendSuccessceN);
            jCimMessageBean.setReceiveTime(""+System.currentTimeMillis());
            jCimMessageBean.setSaveUserId(JNBasePreferenceSaves.getUserSipId());

            String ip = jCimMessageBean.getToRealm();
            String sipNumSaved = JNSpUtils.getString(JCChatManager.mContext,JNPjSipConstants.PJSIP_NUMBER);
            String sipAddressInData = ip.substring(0, ip.indexOf(":"));
            String sipAddressSaved = JNSpUtils.getString(JCChatManager.mContext, JNPjSipConstants.PJSIP_HOST,JNPjSipConstants.PJSIP_HOST_DEFAULT);
            if (TextUtils.equals(jCimMessageBean.getToSipID(), sipNumSaved)
//            TODO  这里由于sip服务发送过来的消息是公网的ip，我们由于有VPN使用的是内网ip所以匹配不上

//                    && TextUtils.equals(sipAddressInData, sipAddressSaved)
            ){
//            && TextUtils.equals(jCimMessageBean.getToRealm().substring(0, jCimMessageBean.getToRealm().indexOf(":")), JCSpUtils.getString(JCchatManager.mContext,JCPjSipConstants.PJSIP_HOST))){
                if (TextUtils.equals(""+JCSessionType.CHAT_PERSION, jCimMessageBean.getSessionType())){
                    jCimMessageBean.setSessionId(jCimMessageBean.getFromSipID());
                    jCimMessageBean.setSessionRealm(jCimMessageBean.getFromRealm());
                    jCimMessageBean.setSessionRealmJava(jCimMessageBean.getFromRealmJava());
                }
//                192.168.1.5:5060   发送的消息
                Log.e("DemoMainActivity", "JCmyPjSipMessageListener===onInstantMessage++ip=" + ip);
                String key = "drone"+"_"+jCimMessageBean.getSessionType()+"_"+jCimMessageBean.getSessionRealm();
                Log.e("DemoMainActivity", "JCmyPjSipMessageListener===onInstantMessage++key=" + key);
                JCmessageListenerManager.onReceiveFlightMessage(key, jCimMessageBean);
            }else {
                Log.e("DemoMainActivity", "JCmyPjSipMessageListener===不是给我发送的消息=sipid==sipurl=对不上=");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onInstantMessageStatus(String msg, String to_uri,int state) {
//        state=202-发送成功；408-发送超时；
        Log.e("DemoMainActivity", "JCmyPjSipMessageListener===onInstantMessageStatus++" + "===" + msg + "====" + to_uri);
//        String userID = JCSpUtils.getString(JCchatManager.mContext,"myAccount", JCPjSipConstants.PJSIP_NUMBER_DEFAULT);
//        Log.e("JCchatFragment", "JCmyPjSipMessageListener===onInstantMessage++userID=" + userID);
//        sip:1042@192.168.1.5

//        try {
//            JCimMessageBean jCimMessageBean = new Gson().fromJson(msg, JCimMessageBean.class);
//            int stateType = (state/100==2? JCmessageStatusType.sendSuccessceS:JCmessageStatusType.sendFail);
//            jCimMessageBean.setStatus(""+stateType);
//            String ip = to_uri.substring(to_uri.indexOf("@")+1);
//            Log.e("DemoMainActivity", "JCmyPjSipMessageListener===onInstantMessageStatus++ip=" + ip);
//            JCmessageListenerManager.onSendMessageState(jCimMessageBean.getSessionId()+"_"+jCimMessageBean.getSessionType()+"_"+ip, jCimMessageBean, stateType);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
    }

}

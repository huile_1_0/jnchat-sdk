package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
 * @ProjectName: JndvChat
 * @Package: com.jndv.jndvchatlibrary.ui.chat.bean
 * @ClassName: JCMsgLocationContentBean
 * @Description: 位置消息内容类
 * @Author: SunQinzheng
 * @CreateDate: 2022/2/19 上午 11:19
 */
public class JCMsgLocationContentBean {
    /**
     * "extend":"",//扩展字段（暂无意义）
     * "url":"",//位置图片地址
     * 	"longitude":"",//经度
     * "latitude":"",//纬度
     * "address":"",//详细地址
     */

    private String url;
    private String longitude;
    private String latitude;
    private String address;


    public JCMsgLocationContentBean(String url, String longitude, String latitude, String address) {
        this.url = url;
        this.longitude = longitude;
        this.latitude = latitude;
        this.address = address;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

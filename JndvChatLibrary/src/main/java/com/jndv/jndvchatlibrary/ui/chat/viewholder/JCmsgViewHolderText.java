package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.graphics.Color;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgTextContentBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/3/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 默认消息展示holder，兼容未定义的消息类型，显示：未知类型消息
 */
public class JCmsgViewHolderText extends JCmsgViewHolderBase {

    private TextView tvMessage;

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_text;
    }

    @Override
    protected void inflateContentView() {
        tvMessage = findViewById(R.id.tv_msg_text);
    }

    @Override
    protected void bindContentView() {
        Log.e("bindContentView",message.getContent());
        JNLogUtil.e("======Content=="+base64ToString(message.getContent()));
        try {
            JCMsgTextContentBean bean = new Gson().fromJson(base64ToString(message.getContent()), JCMsgTextContentBean.class);
            tvMessage.setTextColor(Color.parseColor(bean.getFontColor()));
//            tvMessage.setTextSize(Float.parseFloat(""+bean.getFontSize()));
            JNLogUtil.e("======SendTime=="+message.getSendTime());
            JNLogUtil.e("======ReceiveTime=="+message.getReceiveTime());
            String content ="";
            try {
                content = bean.getText().trim();
                content = content.replace("\r","");
                content = content.replace("\r\n","\n");
            }catch (Exception e){
                e.printStackTrace();
            }

            tvMessage.setText(content);
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            tvMessage.setText(bean.getText()+"\n"+format.format(new Date(Long.parseLong(message.getSendTime())))
//                    +"\n"+format.format(new Date(Long.parseLong(message.getReceiveTime()))));
        }catch (Exception e){
            e.printStackTrace();
            JNLogUtil.e("======getMsgType=="+message.getMsgType());
            tvMessage.setText("未识别的消息："+message.getContent());
        }

    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_copy), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_transmit), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_collect), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_withdraw), 1));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_delete), 2));
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return false;
    }

}

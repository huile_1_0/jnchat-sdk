package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
 * @ProjectName: JndvChat
 * @Package: com.jndv.jndvchatlibrary.ui.chat.bean
 * @ClassName: JCMsgVideoContentBean
 * @Description: 视频消息内容类
 * @Author: SunQinzheng
 * @CreateDate: 2022/6/28 上午 15:00
 */
 public class JCMsgVideoContentBean {
    /**
     * "extend":"",//扩展字段（暂无意义）
     * "url":"",//视频文件地址
     * "time":"",//视频时长
     * "path"://视频本地地址
     */

    private String time;
    private String url;
    private String path;

    public JCMsgVideoContentBean(String time, String path) {
        this.time = time;
        this.path = path;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "JCMsgVideoContentBean{" +
                "time='" + time + '\'' +
                ", url='" + url + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}

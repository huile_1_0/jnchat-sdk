package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

public class JCGroupMemberInvitePerUpdateApi implements IRequestApi {
    /**
     domainAddr	是	string	(操作人)用户域地址
     userId	是	string	(操作人)用户SIP id
     groupId	是	number	群聊id
     groupDomainAddr	是	string	群聊所在域地址
     processedDomainAddr	是	string	被操作的群成员所在SIP域地址
     processedUsersId	是	string	被操作的群成员SIP id
     invitePermission	是	number	被处理的群成员成员邀请新用户权限(0—-关闭，1—-开启)
     */


    @Override
    public String getApi() {
        return "/groupMember/groupMemberInvitePerUpdate";
    }


    private String userId;
    private String domainAddr;
    private String groupId;
    private String groupDomainAddr;
    private String processedDomainAddr;
    private String processedUsersId;
    private String invitePermission;

    public JCGroupMemberInvitePerUpdateApi(String userId, String domainAddr, String groupId, String groupDomainAddr, String processedDomainAddr, String processedUsersId, String invitePermission) {
        this.userId = userId;
        this.domainAddr = domainAddr;
        this.groupId = groupId;
        this.groupDomainAddr = groupDomainAddr;
        this.processedDomainAddr = processedDomainAddr;
        this.processedUsersId = processedUsersId;
        this.invitePermission = invitePermission;
    }
}

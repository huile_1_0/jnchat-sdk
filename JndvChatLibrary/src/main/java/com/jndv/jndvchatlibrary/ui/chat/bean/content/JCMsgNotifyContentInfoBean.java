package com.jndv.jndvchatlibrary.ui.chat.bean.content;//package com.jndv.jndvchatlibrary.ui.chat.bean.content;
//
//public class JCMsgNotifyContentInfoBean {
//    private String text;
//    private int type;
//    private SipParameterJson sipParameterJson;
//    public void setText(String text) {
//        this.text = text;
//    }
//    public String getText() {
//        return text;
//    }
//
//    public void setType(int type) {
//        this.type = type;
//    }
//    public int getType() {
//        return type;
//    }
//
//    public void setSipParameterJson(SipParameterJson sipParameterJson) {
//        this.sipParameterJson = sipParameterJson;
//    }
//    public SipParameterJson getSipParameterJson() {
//        return sipParameterJson;
//    }
//
//    public class SipParameterJson {
//
//        private int userSipId;
//        private String lon;
//        private String remark;
//        private String type;
//        private String urlMobile;
//        private String userContact;
//        private String userName;
//        private String distributeMethod;
//        private String displayMode;//展示模式：Popup=弹框
//        private String url;
//        private String distributeTarget;
//        private String iD;
//        private String lat;
//        private String seq;
//        private String info;
//        public void setUserSipId(int userSipId) {
//            this.userSipId = userSipId;
//        }
//        public int getUserSipId() {
//            return userSipId;
//        }
//
//        public void setLon(String lon) {
//            this.lon = lon;
//        }
//        public String getLon() {
//            return lon;
//        }
//
//        public void setRemark(String remark) {
//            this.remark = remark;
//        }
//        public String getRemark() {
//            return remark;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//        public String getType() {
//            return type;
//        }
//
//        public void setUrlMobile(String urlMobile) {
//            this.urlMobile = urlMobile;
//        }
//        public String getUrlMobile() {
//            return urlMobile;
//        }
//
//        public void setUserContact(String userContact) {
//            this.userContact = userContact;
//        }
//        public String getUserContact() {
//            return userContact;
//        }
//
//        public void setUserName(String userName) {
//            this.userName = userName;
//        }
//        public String getUserName() {
//            return userName;
//        }
//
//        public void setDistributeMethod(String distributeMethod) {
//            this.distributeMethod = distributeMethod;
//        }
//        public String getDistributeMethod() {
//            return distributeMethod;
//        }
//
//        public void setDisplayMode(String displayMode) {
//            this.displayMode = displayMode;
//        }
//        public String getDisplayMode() {
//            return displayMode;
//        }
//
//        public void setUrl(String url) {
//            this.url = url;
//        }
//        public String getUrl() {
//            return url;
//        }
//
//        public void setDistributeTarget(String distributeTarget) {
//            this.distributeTarget = distributeTarget;
//        }
//        public String getDistributeTarget() {
//            return distributeTarget;
//        }
//
//        public void setID(String iD) {
//            this.iD = iD;
//        }
//        public String getID() {
//            return iD;
//        }
//
//        public void setLat(String lat) {
//            this.lat = lat;
//        }
//        public String getLat() {
//            return lat;
//        }
//
//        public void setSeq(String seq) {
//            this.seq = seq;
//        }
//        public String getSeq() {
//            return seq;
//        }
//
//        public void setInfo(String info) {
//            this.info = info;
//        }
//        public String getInfo() {
//            return info;
//        }
//
//    }
//}

package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcCrowdUserDetailBinding;
import com.jndv.jndvchatlibrary.http.api.JCImagesUpApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCUpdateUserDetailActivity;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCUserQRcodeBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCUserDetailViewModel;
import com.jndv.jndvchatlibrary.utils.FunctionUtils;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link JCCrowdUserDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JCCrowdUserDetailFragment extends JCbaseFragment {

    private String userId;
    private String crowdId;
    private int type;
    private String invite_permission;
    private String group_domain_addr;
    private String domain_addr;
    private FragmentJcCrowdUserDetailBinding detailBinding;
    private JCUserDetailViewModel viewModel;
    private FragmentActivity activity;
    String headUrl ;

    public JCCrowdUserDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getString(JCEntityUtils.USER_ID, JNBasePreferenceSaves.getUserSipId());
            crowdId = getArguments().getString(JCEntityUtils.CROWD_ID);
            type = getArguments().getInt(JCEntityUtils.IS_FROM_CROWD, 0);
            group_domain_addr = getArguments().getString(JCEntityUtils.GROUP_DOMAIN_ADDR);
            domain_addr = getArguments().getString(JCEntityUtils.DOMAIN_ADDR);
            invite_permission = getArguments().getString(JCEntityUtils.INVITE_PERMISSION);
        } else {
            userId = JNBasePreferenceSaves.getUserSipId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel =
                new ViewModelProvider(this).get(JCUserDetailViewModel.class);
        detailBinding = FragmentJcCrowdUserDetailBinding.inflate(inflater, container, false);
        activity = getActivity();
        initView();
        initData();
        return detailBinding.getRoot();
    }

    private void initView() {
        viewModel = new JCUserDetailViewModel();
        viewModel.init(activity);
        boolean isSelf = (JNBasePreferenceSaves.getUserSipId().equals(userId));

        if (isSelf) {
            detailBinding.detailTvEdit.setVisibility(View.VISIBLE);
        } else {
            if (type == 1) {//好友进来
//                detailBinding.detailTvDelete.setVisibility(View.VISIBLE);
//                detailBinding.detailTvDelete.setText("删除好友");
            } else if (type == 2) {//群组过来
                detailBinding.detailTvDelete.setVisibility(View.VISIBLE);
                detailBinding.detailTvDelete.setText("踢出群聊");
                initPermission();

            }
        }

        detailBinding.detailTvEdit.setOnClickListener(view -> {
            JCUpdateUserDetailActivity.start(getContext(),userId);
        });
    }

    private void initData() {
        viewModel.getUpdateData().observeForever(new Observer<JCRespondBean>() {
            @Override
            public void onChanged(JCRespondBean jcRespondBean) {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, jcRespondBean.getCode())){
                    JCChatManager.showToast("操作成功！");
                    JCglideUtils.loadCircleImage(getActivity(), headUrl, detailBinding.detailIvHead);
                    JNBasePreferenceSaves.saveUserHead(headUrl);
                }else {
                    JCChatManager.showToast("操作失败！");
                }
            }
        });
        viewModel.getUserDetail(userId)
                .getUserData().observe(activity, data -> {
            if (data != null) {
                detailBinding.detailTvName.setText(data.getNickname());
                if(data.getNickname()!=null){
                    detailBinding.detailTvNickname.setText("昵称："+data.getNickname());
                }else {
                    detailBinding.detailTvNickname.setText("昵称：");
                }
                JCglideUtils.loadOriginHeadImage(getActivity(), data.getHeadIcon(), detailBinding.detailIvHead);

//                JCglideUtils.loadImageToView(activity, data.getAvatarUrl(), detailBinding.userInfoContainer.detailIvHead);
                detailBinding.tvImnumText.setText(data.getImNum());
                if(data.getAccount()!=null) {
                    detailBinding.detailTvAccount.setText("账号："+data.getAccount());
                }else{
                    detailBinding.detailTvAccount.setText("账号：");
                }
                detailBinding.tvPhonenumText.setText(data.getMobile());
                detailBinding.tvGenderText.setText("1".equals(data.getSex()) ? "男" : "女");
                detailBinding.tvIdCard.setText(data.getIdCardNum());
                detailBinding.tvAgeText.setText(FunctionUtils.countAge(data.getIdCardNum()));
            }
        });

        detailBinding.detailTvDelete.setOnClickListener(v -> {
            if (type == 1) {
                // viewModel.deleteFrend()
            } else if (type == 2) {
                viewModel.kicksMember(crowdId, group_domain_addr, userId, domain_addr).getRespondData().observe(activity, new Observer<JCRespondBean>() {
                    @Override
                    public void onChanged(JCRespondBean jcRespondBean) {
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, jcRespondBean.getCode())) {
                            activity.finish();
                        } else {
//                            showToast(jcRespondBean.getMsg());
                        }
                    }
                });

            }

        });


    }


    public static JCCrowdUserDetailFragment newInstance(String userId, String crowdId, String domain_addr, String group_domain_addr, int type,String invite_permission) {
        JCCrowdUserDetailFragment fragment = new JCCrowdUserDetailFragment();
        Bundle args = new Bundle();
        args.putString(JCEntityUtils.USER_ID, userId);
        args.putString(JCEntityUtils.CROWD_ID, crowdId);
        args.putString(JCEntityUtils.INVITE_PERMISSION, invite_permission);
        args.putInt(JCEntityUtils.IS_FROM_CROWD, type);
        args.putString(JCEntityUtils.DOMAIN_ADDR, domain_addr);
        args.putString(JCEntityUtils.GROUP_DOMAIN_ADDR, group_domain_addr);
        fragment.setArguments(args);
        return fragment;
    }

    private void initPermission() {
        //群主可以设置成员的邀请权限
        detailBinding.detailLlPermission.setVisibility(View.VISIBLE);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.crowd_permission, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        detailBinding.detailTvPermission.setAdapter(adapter);
        detailBinding.detailTvPermission.setSelection(Integer.parseInt(invite_permission));
        detailBinding.detailTvPermission.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("CrowdUser",position+"=++++++");
                setPermission(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    //TODO 设置邀请权限
    private void setPermission(int pos){
        viewModel.invitePerUpdate(crowdId,group_domain_addr,domain_addr,userId,String.valueOf(pos));
    }
}

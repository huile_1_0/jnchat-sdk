package com.jndv.jndvchatlibrary.ui.chat.viewholder.collect;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.http.api.JCGetCollectMessageApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgFileContentBean;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
//import com.wgd.wgdfilepickerlib.utils.FileUtils;

/**
 * Author: wangguodong
 * Date: 2022/6/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 收藏消息，viewHolder实体
 */
public class JCCollectMsgFileViewHolder extends JCCollectMsgBaseViewHolder{
    private TextView tvName;
    private TextView tvSize;
    private ImageView iv_image;
    public JCCollectMsgFileViewHolder(Context context, View itemView) {
        super(context, itemView);
    }

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_file_collect;
    }

    @Override
    protected void showContentView(JCGetCollectMessageApi.ColloctBean colloctBean) {
        JNLogUtil.e("===JCCollectMsgTextViewHolder===showContentView====");
        tvName = findViewById(R.id.tv_name);
        tvSize = findViewById(R.id.tv_size);
        iv_image = findViewById(R.id.iv_image);
        try {
            JCbaseIMMessage message = new Gson().fromJson(colloctBean.getBody(), JCimMessageBean.class);
            String decodeBase64 = JNContentEncryptUtils.decryptContent(message.getContent());
            JNLogUtil.e("==decodeBase64="+decodeBase64);
            JCMsgFileContentBean bean = new Gson().fromJson(decodeBase64, JCMsgFileContentBean.class);
            JNLogUtil.e("JCmsgViewHolderFile","====getName=="+bean.getName());
            JNLogUtil.e("JCmsgViewHolderFile","====getSize=="+bean.getSize());
            JNLogUtil.e("JCmsgViewHolderFile","====getPath=="+bean.getPath());
            String name = bean.getName();
            if (null==bean.getName()|| TextUtils.isEmpty(bean.getName())){
                try {
                    name = bean.getPath().substring(bean.getPath().lastIndexOf("/")+1);
                }catch (Exception e){
                    JNLogUtil.e("JCmsgViewHolderFile","==bindContentView==name==", e);
                }
            }
            tvName.setText(name);
            long size = 0;
            try {
                size = Long.parseLong(bean.getSize());
            }catch (Exception e){
                JNLogUtil.e("==JCmsgViewHolderFile==bindContentView==size==", e);
            }

            tvSize.setText(bean.getSize());
//            tvSize.setText(FileUtils.getReadableFileSize(size));
            if (0==size){
                tvSize.setVisibility(View.GONE);
            }else {
                tvSize.setVisibility(View.VISIBLE);
            }
            if(bean.getFileType()!=null){
                String title = bean.getFileType();
//                String title = bean.getFileType().getTitle();
                if (title.equals("IMG")) {
                    JCglideUtils.loadImage(mContext, bean.getUrl(), iv_image);
                } else {
//                    iv_image.setImageResource(bean.getFileType().getIconStyle());
                }
            }else {
//                iv_image.setImageResource(com.wgd.wgdfilepickerlib.R.mipmap.file_picker_def);
            }
        }catch (Exception e){
            JNLogUtil.e("==JCCollectMsgTextViewHolder==showContentView==", e);
            tvName.setText("未识别的消息");
        }
    }
}

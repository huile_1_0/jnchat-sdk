package com.jndv.jndvchatlibrary.ui.chat.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.ehome.sipservice.SipServiceCommand;
import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.ChatContant;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.adapter.JCOrganzationPersonAdapter;
import com.jndv.jndvchatlibrary.bean.OrganizationPersonBean;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCGetOrganizationApi;
import com.jndv.jndvchatlibrary.http.api.JCImagesUpApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgImgContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCCreateSessionUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCconstants;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.ui.location.adapter.DefaultItemDecoration;
import com.jndv.jndvchatlibrary.utils.ActivityStack;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import okhttp3.Call;

public class JCTransferSelectPersonActivity extends JCbaseFragmentActivity {

    private static final String TAG = "JCTransferSelectPersonActivity";

    List<OrganizationPersonBean> opbeanList=new ArrayList<>();
    JCOrganzationPersonAdapter adapter;
    OrganizationPersonBean selectOpbean;
    TextView tvNum;
    private JCimMessageBean messageBean ;
    //用来区分跳转页面的来源，非空代表从webview页面跳转过来
    String contentText;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ttsselect_person);
        ActivityStack.getInstance().addActivity(this);
        messageBean= (JCimMessageBean) getIntent().getSerializableExtra("content");
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvNum.setText(ChatContant.TRANSFER_PERSON_LIST.size()+"");
    }

    private void initView() {
        String uri=getIntent().getStringExtra("uri");
        if(!TextUtils.isEmpty(uri)){
            imageUri=Uri.parse(uri);
        }
        contentText=getIntent().getStringExtra("contentText");
        selectOpbean= (OrganizationPersonBean) getIntent().getSerializableExtra("data");
        tvNum=findViewById(R.id.tv_num);
        RecyclerView rv=findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter=new JCOrganzationPersonAdapter(opbeanList);
        rv.addItemDecoration(new DefaultItemDecoration(this,R.dimen.dimen_1,R.color.gray));
        rv.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if(TextUtils.isEmpty(opbeanList.get(position).deptname))
                    return;

                Intent intent = new Intent(JCTransferSelectPersonActivity.this, JCTransferSelectPersonActivity.class);
                intent.putExtra("data", opbeanList.get(position));
                intent.putExtra("content",messageBean);
                intent.putExtra("contentText",contentText);
                if(!TextUtils.isEmpty(uri)){
                    intent.putExtra("uri",imageUri.toString());
                }
                startActivity(intent);
            }
        });
        adapter.setOnSelectCountListener(count -> {
            tvNum.setText(ChatContant.TRANSFER_PERSON_LIST.size()+"");
        });

        TextView tvNext=findViewById(R.id.tv_next);
        tvNext.setText("发送");
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ChatContant.TRANSFER_PERSON_LIST.size()==0){
                    JCThreadManager.showMainToast("请选择要发送的对象！");
                    return;
                }
                for(OrganizationPersonBean personBean: ChatContant.TRANSFER_PERSON_LIST){
                    createSession(personBean);
                }

                for(OrganizationPersonBean personBean: ChatContant.TRANSFER_PERSON_LIST){
                    transmit(personBean);
                }
            }
        });
        if(selectOpbean==null){
            setCenterTitle("组织机构");
//            getData(JNBasePreferenceSaves.getOrganzationId());
        }else {
            setCenterTitle(selectOpbean.deptname);
            getData(selectOpbean.id+"");
        }
    }

    private void createSession(OrganizationPersonBean personBean) {
        String friendNum = personBean.IM_Number;
        if (null==friendNum)friendNum="";
        JCSessionListBean messageBean = JCSessionUtil.getJCSessionListBean(
                friendNum, JCSessionType.CHAT_PERSION, JNBasePreferenceSaves.getSipAddress());
        if (null==messageBean){
            JCCreateSessionUtils.creatSession2(this, friendNum
                    , JNBasePreferenceSaves.getSipAddress(), JNBasePreferenceSaves.getJavaAddress()
                    , "" + JCSessionType.CHAT_PERSION, friendNum, "");
        }
    }

    private void getData(String parentId) {
        String url= JCconstants.HOST_URL+"/department/getDeptInfo";
        OkHttpUtils
                .get()
                .url(url)
                .addParams("parentid",parentId)
                .build()
                .execute(new StringCallback()
                {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(String response, int id) {
                        opbeanList.clear();
                        JCGetOrganizationApi.Bean bean=new Gson().fromJson(response,JCGetOrganizationApi.Bean.class);
                        if(!TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, bean.getCode())){
                            JCChatManager.showToast("数据异常");
                            return;
                        }
                        if(bean.getData()==null)
                            return;
                        Log.e(TAG, "onResponse: ===" + new Gson().toJson(bean.getData()));
                        List<JCGetOrganizationApi.Bean.DepartBean> departList = bean.getData().getDeparts();
                        if(departList!=null && departList.size()>0){
                            for(JCGetOrganizationApi.Bean.DepartBean departBean:departList){
                                OrganizationPersonBean opbean=new OrganizationPersonBean();
                                opbean.deptname=departBean.getDeptname();
                                opbean.id=departBean.getId();
                                opbean.itemType=1;
                                opbeanList.add(opbean);
                            }
                        }
                        List<JCGetOrganizationApi.Bean.UserBean> userList = bean.getData().getUsers();
                        if(userList!=null && userList.size()>0){
                            for(JCGetOrganizationApi.Bean.UserBean userBean:userList){
                                OrganizationPersonBean opbean=new OrganizationPersonBean();
                                opbean.nickName=userBean.getNickName();
                                opbean.IM_Number=userBean.getIM_Number();
                                opbean.otherId=userBean.getIM_Number();
                                opbean.mobile=userBean.getMobile();
                                opbean.itemType=2;
                                opbean.id=userBean.getId();
                                opbeanList.add(opbean);
                            }
                        }
                        adapter.setNewData(opbeanList);
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        JCChatManager.showToast("数据异常");
                    }
                });
    }

    private void transmit(OrganizationPersonBean bean) {
        if(TextUtils.isEmpty(contentText)){
            JCimMessageBean msg = JCchatFactory.creatIMMessage(messageBean.getContent(),
                    messageBean.getMsgType(), bean.otherId, JNBasePreferenceSaves.getUserSipId()
                    , JNBasePreferenceSaves.getSipAddress(), bean.otherId, JNBasePreferenceSaves.getSipAddress()
                    , bean.type, JNBasePreferenceSaves.getJavaAddress(),JNBasePreferenceSaves.getJavaAddress());
            if(bean.type.equals("1")){
                sendMsg(msg,bean);
            }else {
                if (JCSessionUtil.isGroupStateOk(bean.sessionId)){
                    sendMsg(msg,bean);
                }
            }
        }else {
            sendImgText(bean);
        }

        ActivityStack.getInstance().finishAllActivity();
    }

    private void sendMsg(JCimMessageBean msg,OrganizationPersonBean bean){
        String userID = JNSpUtils.getString(this, "myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
        Log.e("sendMessage",new Gson().toJson(msg));
        Box<JCimMessageBean> beanBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
        beanBox.put(msg);
        Log.d("SipService","msg==trans=select=");
        EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_MESSAGE_TRANSMIT, "", msg));
        SipServiceCommand.sendMessageToBuddy(this, new Gson().toJson(msg), userID
                , "sip:" + bean.otherId + "@" + JNBasePreferenceSaves.getSipAddress()
                , JCSessionType.getTypeSIP(bean.type), false);
    }

    /**
     *发送图片文本
     * @param
     */
    private void sendImgText(OrganizationPersonBean bean) {
        JCimMessageBean msg = JCchatFactory.creatIMMessage(contentText,
                JCmessageType.IMG, bean.otherId, JNBasePreferenceSaves.getUserSipId()
                , JNBasePreferenceSaves.getSipAddress(), bean.otherId, JNBasePreferenceSaves.getSipAddress()
                , bean.type, JNBasePreferenceSaves.getJavaAddress(),JNBasePreferenceSaves.getJavaAddress());

        JCFilesUpUtils.upImage(imageUri, "jpg", new OnHttpListener<JCImagesUpApi.Bean>() {
            @Override
            public void onSucceed(JCImagesUpApi.Bean result) {
                try {
//                                                        Box<JCimMessageBean > box = JCobjectBox.get().boxFor(JCimMessageBean.class);
                    if (TextUtils.equals("success", result.getMsg())){
                        JCMsgImgContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(msg.getContent(),Base64.NO_WRAP)), JCMsgImgContentBean.class);
                        msgEntity.setUrl(JNBaseUtilManager.getFilesUrl(result.getUrlArr().get(0).getTailUrl()));
//                                                            msgEntity.setUri(null);
                        String msgEntityText = new Gson().toJson(msgEntity);
                        msg.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
                        JCThreadManager.onMainHandler(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("zhang","发送图片消息");
                                sendMsg(msg,bean);
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(Exception e) {

            }
        });
    }
}

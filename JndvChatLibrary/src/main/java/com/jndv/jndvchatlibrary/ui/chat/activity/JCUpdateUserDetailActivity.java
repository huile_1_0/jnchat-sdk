package com.jndv.jndvchatlibrary.ui.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCUpdateUserDetailFragment;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;

public class JCUpdateUserDetailActivity extends JCbaseFragmentActivity {
    private String userId;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initIntent();
        initView();
    }

    public static void start(Context context, String userId) {
        Intent intent = new Intent(context, JCUpdateUserDetailActivity.class);
        intent.putExtra(JCEntityUtils.USER_ID, userId);
        context.startActivity(intent);
    }

    private void initIntent() {
        Intent intent = getIntent();
        userId = intent.getStringExtra(JCEntityUtils.USER_ID);
        JCUpdateUserDetailFragment fragment = JCUpdateUserDetailFragment.newInstance(userId);
        showFragment(fragment);

    }

    private void initView() {
        setCenterTitle("个人资料");
    }
}

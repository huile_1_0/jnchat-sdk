package com.jndv.jndvchatlibrary.ui.chat.utils.listener;

import com.jndv.jnbaseutils.chat.JCbaseIMMessage;

/**
 * Author: wangguodong
 * Date: 2022/3/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 消息接收监听
 */
public interface JCMessageListener {
//    isLook=0不是；1=是
    void onReceiveMessage(JCbaseIMMessage message, int isLook);
    void onSendMessageState(JCbaseIMMessage message, int state);
}

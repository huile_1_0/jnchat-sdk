package com.jndv.jndvchatlibrary.ui.chat.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.bean.TTSMsgBean;
import com.jndv.jndvchatlibrary.bean.TTSMsgReplyBean;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCconstants;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.MediaType;

public class TTSDetailActivity extends JCbaseFragmentActivity {

    TTSMsgBean.DataBean.TTSDataBean dataBean;
    EditText etReply;
    //0:未处理 1:已处理
    int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ttsdetail);
        initView();
    }

    private void initView() {
        setCenterTitle("消息详情");
        result=getIntent().getIntExtra("is_result",0);
        dataBean= (TTSMsgBean.DataBean.TTSDataBean) getIntent().getSerializableExtra("data");
        TextView tvContent=findViewById(R.id.tv_content);
        tvContent.setText(dataBean.getVoice_content());
        if(result==1){
            LinearLayout llReply=findViewById(R.id.ll_reply);
            llReply.setVisibility(View.GONE);
        }else {
            etReply=findViewById(R.id.et_reply);
            findViewById(R.id.btn_confirm).setOnClickListener(v -> {
                if(TextUtils.isEmpty(etReply.getText().toString())){
                    Toast.makeText(TTSDetailActivity.this,"回复内容不能为空!",Toast.LENGTH_SHORT).show();
                    return;
                }
                sendConfirm();
            });
        }
    }

    private void sendConfirm() {
        showLoadding();
        String url= JCconstants.HOST_URL+"/tts-voice-record/create";
        Map<String,String> map=new HashMap<>();
        map.put("ttsMsgId",dataBean.getMsg_id());
        map.put("callee", JNBasePreferenceSaves.getUserSipId());
        map.put("result",etReply.getText().toString());
        map.put("resultExplain", "");
        map.put("ackType", "0");
        OkHttpUtils.postString()
                .url(url)
                .content(new Gson().toJson(map))
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .build()
                .execute(new StringCallback()
                {
                    @Override
                    public void onResponse(String response, int id) {
                        cancelLoadding();
                        TTSMsgReplyBean bean=new Gson().fromJson(response,TTSMsgReplyBean.class);
                        if(bean.code==200){
                            JCChatManager.showToast("发送回复成功");
                            TTSDetailActivity.this.finish();
                        }else {
                            JCChatManager.showToast("发送回复失败");
                        }
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        cancelLoadding();
                        JCChatManager.showToast("发送回复失败");
                    }
                });
    }
}
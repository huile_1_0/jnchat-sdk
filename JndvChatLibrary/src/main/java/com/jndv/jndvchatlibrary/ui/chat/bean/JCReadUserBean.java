package com.jndv.jndvchatlibrary.ui.chat.bean;

/**
 * Author: wangguodong
 * Date: 2022/2/23
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 群消息已读未读人员实体类
 */
public class JCReadUserBean {

    private String id ;
    private String nickname ;
    private String avatar ;
    private String readTime ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getReadTime() {
        return readTime;
    }

    public void setReadTime(String readTime) {
        this.readTime = readTime;
    }
}

package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.text.TextUtils;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.MeetingInviteMsg;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCDeleteSessionApi;
import com.jndv.jndvchatlibrary.http.api.JCSessionListApi;
import com.jndv.jndvchatlibrary.http.api.JCUpdateSessionStateApi;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCsessionListModelItem;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/4/20
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCchatListViewModel extends ViewModel {
    LoadingPopupView loadingPopupView = null;
    private Fragment fragment;

    private MutableLiveData<JCsessionListModelItem> sessionList = new MutableLiveData<>();;
    private MutableLiveData<OperateItem> operateeData =new MutableLiveData<>();;

    public JCchatListViewModel() {
        sessionList = new MutableLiveData<>();
        operateeData = new MutableLiveData<>();
    }

    public void init(Fragment fragment){
        this.fragment = fragment ;
    }

    public MutableLiveData<JCsessionListModelItem> getSessionList() {
        return sessionList;
    }

    public MutableLiveData<OperateItem> getOperateeData() {
        return operateeData;
    }

    Map<Integer,Integer> unreadSessionMap;

    public void getChatListData(int page) {
        if (1==page){
            unreadSessionMap=new HashMap<>();
            Box<JCSessionListBean> sessionBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
            QueryBuilder<JCSessionListBean> builder = sessionBox.query();
            List<JCSessionListBean> jcSessionListBeans = builder.build().find();
            for(JCSessionListBean bean:jcSessionListBeans){
                if (null==bean) continue;
                unreadSessionMap.put(bean.getSession_id(),bean.getUnread_number());
            }
            //这里不能全部删除掉，因为在通讯录->我的群组-已退群组里还要查询本地数据库(JCSessionListBean里还要有已退群聊的数据)
//            sessionBox.removeAll();
        }
        Log.d("ping","getChatListData...start");
        EasyHttp.get(fragment)
                .api(new JCSessionListApi()
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                        .setPageNo(page)
                        .setPageSize(JCChatManager.pageSizeSession)
                )
                .request(new OnHttpListener<JCSessionListApi.Bean>() {
                    @Override
                    public void onSucceed(JCSessionListApi.Bean result) {
                        Log.d("ping","getChatListData...end");
                        if (result==null||result.getData()==null){
                            JCsessionListModelItem jCsessionListModelItem = new JCsessionListModelItem();
                            jCsessionListModelItem.setPage(page);
                            jCsessionListModelItem.setType(1);
                            sessionList.setValue(jCsessionListModelItem);
                            return;
                        }
                        if (!TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            JCsessionListModelItem jCsessionListModelItem = new JCsessionListModelItem();
                            jCsessionListModelItem.setPage(page);
                            jCsessionListModelItem.setType(1);
                            sessionList.setValue(jCsessionListModelItem);
                        }else {
                            List<JCSessionListBean> data = new ArrayList<>();
                            Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
                            if (page==1){
                                data.add(getSession());
                                data.add(createTTSSession());
                                data.add(createMeetingSession());
                                data.add(getIntercomSession());
                            }
                            for (int i = 0; i < result.getData().size(); i++) {
                                QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
                                JCSessionListBean dataBean = result.getData().get(i);
                                if (null==dataBean)continue;
                                JCSessionListBean messageBean = builder.equal(JCSessionListBean_.session_id
                                        , dataBean.getSession_id()).build().findFirst();
                                if (null!=messageBean){
                                    dataBean.setId(messageBean.getId());
                                    dataBean.setCreate_time(messageBean.getCreate_time());
                                    dataBean.setLast_time(messageBean.getLast_time());
                                }
//                               这里现在有类型转换问题，就先捕获吧
                                try {
                                    if(unreadSessionMap!=null && unreadSessionMap.get(dataBean.getSession_id())!=null){
                                        dataBean.setUnread_number(unreadSessionMap.get(dataBean.getSession_id()));
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                //过滤掉other_id是自己sipId的消息
                                if(!TextUtils.equals(dataBean.getOther_id(),JNBasePreferenceSaves.getUserSipId())){
                                    imMsgBox.put(dataBean);
                                    data.add(dataBean);
                                }
                            }
                            JCsessionListModelItem jCsessionListModelItem = new JCsessionListModelItem();
                            jCsessionListModelItem.setPage(page);
                            jCsessionListModelItem.setType(0);
//                            jCsessionListModelItem.setDatas(sortData(data));
                            jCsessionListModelItem.setDatas(data);
                            sessionList.setValue(jCsessionListModelItem);
                            if (result.getData().size()>=JCChatManager.pageSizeSession){
                                getChatListData(page+1);
                            }
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        JCsessionListModelItem jCsessionListModelItem = new JCsessionListModelItem();
                        jCsessionListModelItem.setPage(page);
                        jCsessionListModelItem.setType(1);
                        sessionList.setValue(jCsessionListModelItem);
                    }
                });
    }

    private JCSessionListBean createTTSSession(){
        JCSessionListBean bean = new JCSessionListBean();
        bean.setType(JCSessionType.CHAT_TTS);
        bean.setOther_name("TTS消息");
        return bean;
    }

    private JCSessionListBean createMeetingSession(){
        JCSessionListBean bean = new JCSessionListBean();
        bean.setType(JCSessionType.MEETING);
        bean.setOther_name("会议消息");
        bean.setFirst_content(MeetingInviteMsg.MEETING_INVITE_MSG);
        return bean;
    }

    /**
     * 注意通知消息的other_id设为"0"，和session_id值相同
     * @return
     */
    private JCSessionListBean getSession() {
        JNLogUtil.e("==getSession==01=========");
        Box<JCSessionListBean> sessionBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        QueryBuilder<JCSessionListBean> sessionbuilder = sessionBox.query();
        JCSessionListBean sessionListBean = sessionbuilder.equal(JCSessionListBean_.session_id, 0)
                .equal(JCSessionListBean_.user_id, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                .build().findFirst();
        JNLogUtil.e("==getSession==02=========");
        if (null==sessionListBean){
            JNLogUtil.e("==getSession==03=========");
            JCSessionListBean bean = new JCSessionListBean();
            bean.setType(JCSessionType.CHAT_SYSTEM_NOTIFY);
            bean.setSession_id(0);
            bean.setOther_id("0");
            bean.setUser_id(JNBasePreferenceSaves.getUserSipId());
            bean.setSession_party_domain_addr(JNBasePreferenceSaves.getSipAddress());
            bean.setOther_name("通知消息");
            bean.setLast_time(0);
            JNLogUtil.e("==getSession==04=========");
            long id = sessionBox.put(bean);
            JNLogUtil.e("==getSession==05========id="+id);
            bean.setId(id);
            sessionListBean = bean;
        }else {
            sessionListBean.setOther_id("0");
            sessionBox.put(sessionListBean);
        }
        JNLogUtil.e("==getSession==06=========");
        return sessionListBean;
    }

    /**
     * 获取对讲通知会话
     * @return
     */
    private JCSessionListBean getIntercomSession() {
        JNLogUtil.e("==getSession==01=========");
        Box<JCSessionListBean> sessionBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        QueryBuilder<JCSessionListBean> sessionbuilder = sessionBox.query();
        JCSessionListBean sessionListBean = sessionbuilder.equal(JCSessionListBean_.type, JCSessionType.SESSION_CLUSTER_INTERCOM, QueryBuilder.StringOrder.CASE_SENSITIVE)
                .equal(JCSessionListBean_.user_id, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                .build().findFirst();
        JNLogUtil.e("==getSession==02=========");
        if (null==sessionListBean){
            JNLogUtil.e("==getSession==03=========");
            JCSessionListBean bean = new JCSessionListBean();
            bean.setType(JCSessionType.SESSION_CLUSTER_INTERCOM);
            bean.setSession_id(-1);
            bean.setOther_id("-1");
            bean.setUser_id(JNBasePreferenceSaves.getUserSipId());
            bean.setSession_party_domain_addr(JNBasePreferenceSaves.getSipAddress());
            bean.setOther_name("对讲系统消息");
            bean.setLast_time(0);
            JNLogUtil.e("==getSession==04=========");
            long id = sessionBox.put(bean);
            JNLogUtil.e("==getSession==05========id="+id);
            bean.setId(id);
            sessionListBean = bean;
        }else {
            sessionListBean.setOther_id("-1");
            sessionBox.put(sessionListBean);
        }
        JNLogUtil.e("==getSession==06=========");
        return sessionListBean;
    }

    public List<JCSessionListBean> sortData(List<JCSessionListBean> mList) {
        try {
            Collections.sort(mList, new Comparator<JCSessionListBean>() {
                @Override
                public int compare(JCSessionListBean o1, JCSessionListBean o2) {
                    if (o1.getLast_time() < o2.getLast_time()){
                        return 1;
                    }
                    return -1;
                }
            });
            List<JCSessionListBean> list1 = new ArrayList();
            List<JCSessionListBean> list2 = new ArrayList();
            for (int i = 0; i < mList.size(); i++) {
                if (1==mList.get(i).getIs_top()){
                    list1.add(mList.get(i));
                }else {
                    list2.add(mList.get(i));
                }
            }
            mList.clear();
            mList.addAll(list1);
            mList.addAll(list2);
        }catch (Exception e){
            e.printStackTrace();
        }
        return mList;
    }

    public void deleteSession(String sid) {
//        String host = JCSpUtils.getString(fragment.getContext(), JCPjSipConstants.PJSIP_HOST, JCPjSipConstants.PJSIP_HOST_DEFAULT);
//        String port = JCSpUtils.getString(fragment.getContext(), JCPjSipConstants.PJSIP_PORT, JCPjSipConstants.PJSIP_PORT_DEFAULT);
        loadingPopupView = new XPopup.Builder(fragment.getActivity()).asLoading("操作中...");
        loadingPopupView.show();
        EasyHttp.delete(fragment)
                .api(new JCDeleteSessionApi()
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setOperationUserId(JNBasePreferenceSaves.getUserSipId())
                        .setConversationId(sid)
                )
                .request(new OnHttpListener<JCDeleteSessionApi.Bean>() {
                    @Override
                    public void onSucceed(JCDeleteSessionApi.Bean result) {
                        Log.e("chatlist", "onSucceed: " + result.toString());
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            OperateItem operateItem = new OperateItem();
                            operateItem.setType(0);
                            operateItem.setState(0);
                            operateItem.setMsg(sid);
                            operateeData.setValue(operateItem);
                        }else {
                            OperateItem operateItem = new OperateItem();
                            operateItem.setType(0);
                            operateItem.setState(1);
                            operateeData.setValue(operateItem);
                        }
                        if (null!=loadingPopupView)loadingPopupView.dismiss();
                    }
                    @Override
                    public void onFail(Exception e) {
                        OperateItem operateItem = new OperateItem();
                        operateItem.setType(0);
                        operateItem.setState(1);
                        operateeData.setValue(operateItem);
                        if (null!=loadingPopupView)loadingPopupView.dismiss();
                    }
                });
    }


    public void updateSession(String sid, int top, int nodisturb) {
        loadingPopupView = new XPopup.Builder(fragment.getActivity()).asLoading("操作中...");
        loadingPopupView.show();

        JCUpdateSessionStateApi api = new JCUpdateSessionStateApi()
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setSessionId(sid)
                .setTop(top)
                .setNoDisturb(nodisturb);
        EasyHttp.put(fragment)
                .api(api)
                .json(new Gson().toJson(api))
                .request(new OnHttpListener<JCUpdateSessionStateApi.Bean>() {
                    @Override
                    public void onSucceed(JCUpdateSessionStateApi.Bean result) {
                        Log.e("chatlist", "onSucceed: " + result.toString());
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            OperateItem operateItem = new OperateItem();
                            operateItem.setType(1);
                            operateItem.setState(0);
                            operateItem.setMsg(sid+","+top+","+nodisturb);
                            operateeData.setValue(operateItem);
                        }else {
                            OperateItem operateItem = new OperateItem();
                            operateItem.setType(1);
                            operateItem.setState(1);
                            operateeData.setValue(operateItem);
                        }
                        if (null!=loadingPopupView)loadingPopupView.dismiss();
                    }

                    @Override
                    public void onFail(Exception e) {
                        OperateItem operateItem = new OperateItem();
                        operateItem.setType(1);
                        operateItem.setState(1);
                        operateeData.setValue(operateItem);
                        if (null!=loadingPopupView)loadingPopupView.dismiss();
                    }
                });
    }

    public static final class OperateItem{
        private int type;//操作类型；0=删除;1=更新会话设置

        private int state;//操作状态；0=成功；1=失败
        private String msg;//说明

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}

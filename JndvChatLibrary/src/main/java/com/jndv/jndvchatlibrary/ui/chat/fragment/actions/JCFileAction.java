package com.jndv.jndvchatlibrary.ui.chat.fragment.actions;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.listUi.JCbaseActions;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.http.api.JCFilesUpApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgFileContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;

import com.wgd.choosefilelib.ChooseFile;
import com.wgd.choosefilelib.ChooseFileInfo;
import com.wgd.choosefilelib.ChooseFileUIConfig;
import com.wgd.choosefilelib.IFileChooseListener;
import com.wgd.choosefilelib.IFileTypeFilter;

import java.util.ArrayList;
import java.util.List;
//import com.wgd.wgdfilepickerlib.BaseFragment;
//import com.wgd.wgdfilepickerlib.FilePickerActivity;
//import com.wgd.wgdfilepickerlib.bean.FileEntity;


/**
 * @ProjectName: JndvChat
 * @Package: com.jndv.jndvchatlibrary.ui.chat.fragment.actions
 * @ClassName: JCCardAction
 * @Description: 发送文件按钮
 */
public class JCFileAction extends JCbaseActions {
    public JCFileAction() {
        super(R.string.action_file, R.drawable.jc_chat_action_file);
    }

    @Override
    public void onClick() {
        JNLogUtil.e("JCFileAction","++++");
        ChooseFile.create((FragmentActivity) getActivity())
                .setUIConfig(new ChooseFileUIConfig.Builder().build())
                .setTypeFilter(new IFileTypeFilter() {
                    @Override
                    public List<ChooseFileInfo> doFilter(List<ChooseFileInfo> list) {
                        Log.e("TAG", "doFilter: =JCFileAction=ChooseFile===doFilter=筛选===" );
                        List<ChooseFileInfo> nlist = new ArrayList<>();
                        if (null!=list&&list.size()>0){
                            for (ChooseFileInfo mChooseFileInfo:list ) {
                                Log.e("TAG", "doFilter: ==ChooseFile=======" + mChooseFileInfo.fileType);
                                if (!TextUtils.equals(ChooseFile.FILE_TYPE_Unknown, mChooseFileInfo.fileType)){
                                    nlist.add(mChooseFileInfo);
                                }
                            }
                        }
                        return nlist;
                    }
                })
                .forResult(new IFileChooseListener() {
                    @Override
                    public void doChoose(@Nullable ChooseFileInfo chooseFileInfo) {
                        Log.e("0531", "doChoose: ===ChooseFile===========" + chooseFileInfo.filePath);
                        JCMsgFileContentBean msgEntity = new JCMsgFileContentBean(chooseFileInfo.fileName, chooseFileInfo.filePath, chooseFileInfo.fileSize, chooseFileInfo.fileType);
                        String contentText = new Gson().toJson(msgEntity);
                        String contentBase64 = Base64.encodeToString(contentText.getBytes(),Base64.NO_WRAP);

                        JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                                contentBase64,""+ JCmessageType.FILE, getOtherId(), JNSpUtils.getString(getContainer().activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
                                , JNBasePreferenceSaves.getSipAddress(), getContainer().jCsessionChatInfoBean.getSessionChatId(), getContainer().jCsessionChatInfoBean.getSessionaddress()
                                , ""+getContainer().sessionType, JNBasePreferenceSaves.getJavaAddress(),getContainer().jCsessionChatInfoBean.getSessionaddressJava());

                        getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,0);

                        JCFilesUpUtils.upFilesAll(chooseFileInfo.filePath, new OnHttpListener<JCFilesUpApi.Bean>() {
                            @Override
                            public void onSucceed(JCFilesUpApi.Bean result) {
                                try {
                                    if (null!=result){
                                        if (null!=result.getUrlArr()&&result.getUrlArr().size()>0) {
                                            if (TextUtils.equals("success",result.getUrlArr().get(0).getUpload())){
                                                JCMsgFileContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP)), JCMsgFileContentBean.class);
                                                msgEntity.setUrl(JNBaseUtilManager.getFilesUrl(result.getUrlArr().get(0).getTailUrl()));
//                                                            msgEntity.setUri(null);
                                                String msgEntityText = new Gson().toJson(msgEntity);
                                                jCimMessageBean.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
                                                JCThreadManager.onMainHandler(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,1);
                                                    }
                                                });
                                            }else {
                                                JCChatManager.showToast("文件上传失败！");
                                                JCThreadManager.onMainHandler(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(), JCmessageStatusType.sendFail);
                                                    }
                                                });
                                            }
                                        }
                                    }else {
                                        JCChatManager.showToast("文件上传失败！");
                                        JCThreadManager.onMainHandler(new Runnable() {
                                            @Override
                                            public void run() {
                                                getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(), JCmessageStatusType.sendFail);
                                            }
                                        });
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFail(Exception e) {
                                JCChatManager.showToast("文件上传失败！");
                                JNLogUtil.e("JCFileAction","==upFilesAll==onFail==01==");
                                JCThreadManager.onMainHandler(new Runnable() {
                                    @Override
                                    public void run() {
                                        getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(),JCmessageStatusType.sendFail);
                                    }
                                });
                            }
                        });

                    }
                });
//        FilePickerActivity.start(getActivity(), "文件选择", new BaseFragment.FragmentSelect() {
//            @Override
//            public void onSelecte(int type, List<FileEntity> files, Object... objects) {
//                if (type == BaseFragment.SELECTE_FILE_RESULT && null!=files&&files.size()>0){
//                    for (int i = 0; i < files.size(); i++) {
//                        FileEntity fileEntity = files.get(i);
//                        JNLogUtil.e("JCFileAction","====getName=="+fileEntity.getName());
//                        JNLogUtil.e("JCFileAction","====getPath=="+fileEntity.getPath());
//                        JNLogUtil.e("JCFileAction","====getSize=="+fileEntity.getSize());
//                        JCMsgFileContentBean msgEntity = new JCMsgFileContentBean(fileEntity.getName(), fileEntity.getPath(), fileEntity.getSize(), fileEntity.getFileType());
//                        String contentText = new Gson().toJson(msgEntity);
//                        String contentBase64 = Base64.encodeToString(contentText.getBytes(),Base64.NO_WRAP);
//
//                        JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
//                                contentBase64,""+ JCmessageType.FILE, getOtherId(), JNSpUtils.getString(getContainer().activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
//                                , JNBasePreferenceSaves.getSipAddress(), getContainer().jCsessionChatInfoBean.getSessionChatId(), getContainer().jCsessionChatInfoBean.getSessionaddress()
//                                , ""+getContainer().sessionType, JNBasePreferenceSaves.getJavaAddress(),getContainer().jCsessionChatInfoBean.getSessionaddressJava());
//
//                        getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,0);
//
//                        JCFilesUpUtils.upFilesAll(fileEntity.getPath(), new OnHttpListener<JCFilesUpApi.Bean>() {
//                            @Override
//                            public void onSucceed(JCFilesUpApi.Bean result) {
//                                try {
//                                    if (null!=result){
//                                        if (null!=result.getUrlArr()&&result.getUrlArr().size()>0) {
//                                            if (TextUtils.equals("success",result.getUrlArr().get(0).getUpload())){
//                                                JCMsgFileContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP)), JCMsgFileContentBean.class);
//                                                msgEntity.setUrl(JNBaseUtilManager.getFilesUrl(result.getUrlArr().get(0).getUrl()));
////                                                            msgEntity.setUri(null);
//                                                String msgEntityText = new Gson().toJson(msgEntity);
//                                                jCimMessageBean.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
//                                                JCThreadManager.onMainHandler(new Runnable() {
//                                                    @Override
//                                                    public void run() {
//                                                        getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,1);
//                                                    }
//                                                });
//                                            }else {
//                                                JCThreadManager.onMainHandler(new Runnable() {
//                                                    @Override
//                                                    public void run() {
//                                                        getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(), JCmessageStatusType.sendFail);
//                                                    }
//                                                });
//                                            }
//                                        }
//                                    }else {
//                                        JCThreadManager.onMainHandler(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(), JCmessageStatusType.sendFail);
//                                            }
//                                        });
//                                    }
//                                }catch (Exception e){
//                                    e.printStackTrace();
//                                }
//                            }
//
//                            @Override
//                            public void onFail(Exception e) {
//                                JNLogUtil.e("JCFileAction","==upFilesAll==onFail==01==");
//                                JCThreadManager.onMainHandler(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(),JCmessageStatusType.sendFail);
//                                    }
//                                });
//                            }
//                        });
//
//                    }
//                }
//            }
//        });
    }

    /**
     *
     * @param fileEntity
     */
//    private void upFiles(FileEntity fileEntity){
//        if (TextUtils.equals("IMG",fileEntity.getFileType().getTitle())){
//
//        }else if (TextUtils.equals("AUDIO",fileEntity.getFileType().getTitle())){
//
//        }else if (TextUtils.equals("VIDEO",fileEntity.getFileType().getTitle())){
//
//        }else {
//
//        }
//    }

}

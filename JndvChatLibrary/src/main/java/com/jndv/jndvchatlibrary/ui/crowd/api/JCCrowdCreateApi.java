package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;


public class JCCrowdCreateApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/group/createGroup";
    }


    private String domainAddr;

    private String domainAddrUser;

    private String operationUserId  ;
    private String name;
    private String pic;
    private String number;
    private String groupType;
    private String groupNumbers;
    private String renzheng;


    public JCCrowdCreateApi(String domainAddr, String domainAddrUser, String operationUserId, String name, String pic, String number, String groupType, String groupNumbers,String renzheng) {
        this.domainAddr = domainAddr;
        this.domainAddrUser = domainAddrUser;
        this.operationUserId = operationUserId;
        this.name = name;
        this.pic = pic;
        this.number = number;
        this.groupType = groupType;
        this.groupNumbers = groupNumbers;
        this.renzheng=renzheng;
    }

    public JCCrowdCreateApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCCrowdCreateApi setOperationUserId(String operationUserId) {
        this.operationUserId = operationUserId;
        return this;
    }

    public JCCrowdCreateApi setName(String name) {
        this.name = name;
        return this;
    }

    public JCCrowdCreateApi setPic(String pic) {
        this.pic = pic;
        return this;
    }

    public JCCrowdCreateApi setNumber(String number) {
        this.number = number;
        return this;
    }

    public JCCrowdCreateApi setGroupType(String groupType) {
        this.groupType = groupType;
        return this;
    }

    public JCCrowdCreateApi setGroupNumbers(String groupNumbers) {
        this.groupNumbers = groupNumbers;
        return this;
    }

    public JCCrowdCreateApi setDomainAddrUser(String domainAddrUser) {
        this.domainAddrUser = domainAddrUser;
        return this;
    }
}

package com.jndv.jndvchatlibrary.ui.menu;

import android.content.Context;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import com.jndv.jndvchatlibrary.R;
import com.lxj.xpopup.core.AttachPopupView;

public class MenuPopup extends AttachPopupView {
    Context mContext ;
    CardView cv_pop_menu ;

    TextView mian_addfriend ;
    TextView main_createcrowd ;
    TextView main_meet_more ;
    TextView tvCreateTTS;
    OnClickListener onClickListener ;
    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public MenuPopup(@NonNull Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_right_menu;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        initView();
    }

    private void initView(){
        cv_pop_menu = findViewById(R.id.cv_pop_menu);

        mian_addfriend = findViewById(R.id.mian_addfriend);
        main_createcrowd = findViewById(R.id.main_createcrowd);
        main_meet_more = findViewById(R.id.main_meet_more);
        tvCreateTTS=findViewById(R.id.tv_create_tts);

        if (null!=onClickListener){
            mian_addfriend.setOnClickListener(onClickListener);
            main_createcrowd.setOnClickListener(onClickListener);
            main_meet_more.setOnClickListener(onClickListener);
            tvCreateTTS.setOnClickListener(onClickListener);
        }

    }
//
//    OnClickListener onClickListener =

}

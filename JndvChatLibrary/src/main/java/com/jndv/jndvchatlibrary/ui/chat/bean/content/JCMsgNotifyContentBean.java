package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
 * Author: wangguodong
 * Date: 2022/5/27
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 通知消息内容
 */
public class JCMsgNotifyContentBean {
    /*"type":"",//通知消息的通知类型
"extend":"",//扩展字段（暂无意义）
"text":"",//信息*/
    private String type;
    private String extend;
    private String text;

    public JCMsgNotifyContentBean() {

    }
    public JCMsgNotifyContentBean(String type, String extend, String text) {
        this.type = type;
        this.extend = extend;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "JCMsgNotifyContentBean{" +
                "type='" + type + '\'' +
                ", extend='" + extend + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}

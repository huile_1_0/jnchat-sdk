package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgNotifyContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCNotifyType;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/14
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 通知消息类型
 */
public class JCmsgViewHolderNotifyCenter extends JCmsgViewHolderBase {

    private TextView tvMessage;
    JCMsgNotifyContentBean bean;

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_withdraw;
    }

    @Override
    protected void inflateContentView() {
        tvMessage = findViewById(R.id.tv_msg_text);
    }

    @Override
    protected void bindContentView() {

        Log.e("NotifyCenter","-------------");
        String text = "未知消息类型！";
        try {
            bean = new Gson().fromJson(new String(
                    Base64.decode(message.getContent(), Base64.NO_WRAP)), JCMsgNotifyContentBean.class);
            text = bean.getText();
        }catch (Exception e){
            JNLogUtil.e("NotifyCenter","==JCmsgViewHolderNotifyCenter==bindContentView==content解析==",e);
        }
        tvMessage.setText(text);

        Log.e("NotifyCenter","-------------"+text);
    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return true;
    }


    @Override
    protected void onItemClick() {
        super.onItemClick();
        //单聊的通知消息
        if (TextUtils.equals(bean.getType(),JCNotifyType.NOTIFY_SingleNotice+"")){

        }
    }
}

package com.jndv.jndvchatlibrary.ui.chat.viewholder.collect;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.http.api.JCGetCollectMessageApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jndvchatlibrary.utils.JCDateUtils;


/**
 * Author: wangguodong
 * Date: 2022/6/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 收藏消息列表使用的viewholder基类
 */
public abstract class JCCollectMsgBaseViewHolder {

    private View itemView;
    protected Context mContext;
    private TextView tv_time ;
    private FrameLayout fragme_layout ;

    abstract protected int getContentResId();
    abstract protected void showContentView(JCGetCollectMessageApi.ColloctBean colloctBean);

    public JCCollectMsgBaseViewHolder(Context context, View itemView) {
        JNLogUtil.e("==JCCollectMsgFragment==CommonAdapter==convert==003");
        this.itemView = itemView;
        mContext = context;
        tv_time = itemView.findViewById(R.id.tv_time);
        fragme_layout = itemView.findViewById(R.id.fragme_layout);
    }

    protected boolean isReceivedMessage(JCbaseIMMessage message) {
//        if (null!=message && null!=message.getToSipID())
//            return message.getToSipID().equals(null== JNBasePreferenceSaves.getUserSipId()?"":JNBasePreferenceSaves.getUserSipId());//
        if (null != message && null != message.getFromSipID())
            return !message.getFromSipID().equals(null == JNBasePreferenceSaves.getUserSipId() ? "" : JNBasePreferenceSaves.getUserSipId());//
        else return false;
    }

    public void initContent(JCGetCollectMessageApi.ColloctBean colloctBean){
        JNLogUtil.e("===JCCollectMsgBaseViewHolder===initContent==");
        if (fragme_layout.getChildCount() != 0) {
            Log.i("-----", "inflate: ---------"+getContentResId());
            try {
                if (Integer.parseInt(fragme_layout.getChildAt(0).getTag().toString()) != getContentResId()){
                    fragme_layout.removeAllViews();
                    setView();
                }
            }catch (Exception e){
                e.printStackTrace();
                setView();
            }
        }else {
            setView();
        }
        initTime(colloctBean);
        showContentView(colloctBean);
    }
    // 根据layout id查找对应的控件
    protected <T extends View> T findViewById(int id) {
        return (T) itemView.findViewById(id);
    }

    // 设置控件的长宽
    protected void setLayoutParams(int width, int height, View... views) {
        for (View view : views) {
            ViewGroup.LayoutParams maskParams = view.getLayoutParams();
            maskParams.width = width;
            maskParams.height = height;
            view.setLayoutParams(maskParams);
        }
    }

    public void setViewClick(View.OnClickListener onClickListener){
        if (null!=itemView)itemView.setOnClickListener(onClickListener);
    }

    private void setView(){
        try {
            JNLogUtil.e("===JCCollectMsgBaseViewHolder===initContent==002==");
            View mview = View.inflate(itemView.getContext(), getContentResId(), null);
            mview.setTag(getContentResId());
            fragme_layout.addView(mview);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initTime(JCGetCollectMessageApi.ColloctBean colloctBean){
        try {
            JNLogUtil.e("===JCCollectMsgBaseViewHolder===initTime====");
            long time = System.currentTimeMillis();
            try {
                time = Long.parseLong(colloctBean.getCreateTime()+"000");
            }catch (Exception e){
                JNLogUtil.e("==JCCollectMsgBaseBean==initContent==time==", e);
            }
            tv_time.setText(JCDateUtils.getDateFormat(time, "yyyy-MM-dd HH:mm:ss"));

        }catch (Exception e){
            JNLogUtil.e("==JCCollectMsgBaseBean==initContent====", e);
        }
    }

}

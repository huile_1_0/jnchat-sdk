package com.jndv.jndvchatlibrary.ui.chat.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.OptIn;
import androidx.media3.common.MediaItem;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.Player;
import androidx.media3.common.util.UnstableApi;
import androidx.media3.datasource.DefaultDataSourceFactory;
import androidx.media3.exoplayer.ExoPlayer;
import androidx.media3.exoplayer.rtsp.RtspMediaSource;
import androidx.media3.exoplayer.source.MediaSource;
import androidx.media3.exoplayer.source.ProgressiveMediaSource;
import androidx.media3.ui.PlayerView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.player.NetState;
import com.jndv.jndvchatlibrary.ui.player.Strings;
import com.jndv.jndvchatlibrary.ui.player.NetStateUtil;
import com.jndv.jndvchatlibrary.ui.player.ProgressTextView;
import com.jndv.jndvchatlibrary.ui.player.VerticalSeekBar;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by liubohua on 2017/2/16.
 */
public class TextureVodActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "TextureVodActivity";

    public static final int UPDATE_SEEKBAR = 0;
    public static final int HIDDEN_SEEKBAR = 1;

    private String bufferTime;
    private String bufferSize;

    private Context mContext;

//    KSYTextureView mVideoView = null;

    ExoPlayer player;
    PlayerView playerView;

    private Handler mHandler;

    private VerticalSeekBar mAudioSeekbar;
    private ProgressTextView mProgressTextView;

    private RelativeLayout mPlayerPanel;
    private ImageView mPlayerStartBtn;
    private SeekBar mPlayerSeekbar;
    private TextView mPlayerPosition;

    private RelativeLayout topPanel;
    private ImageView reload;
    private ImageView mPlayerVolume;
    private ImageView mPlayerRotate;
    private ImageView mPlayerScreen;
    private ImageView mPlayerScale;

    private boolean mPlayerPanelShow = true;
    private boolean mPause = false;

    private boolean showAudioBar = false;

    private long mStartTime = 0;
    private long mPauseStartTime = 0;
    private long mPausedTime = 0;

    private int mVideoWidth = 0;
    private int mVideoHeight = 0;
    private int mVideoScaleIndex = 0;

    private int rotateNum = 0;

    private String mDataSource;
    //
    private float centerPointX;
    private float centerPointY;
    private float lastMoveX = -1;
    private float lastMoveY = -1;
    private float movedDeltaX;
    private float movedDeltaY;
    private float totalRatio;
    private float deltaRatio;
    private double lastSpan;
    private boolean mTouching;

    private void InitView(){
//        if (mVideoView == null)
//            return;
//        mVideoWidth = mVideoView.getVideoWidth();
//        mVideoHeight = mVideoView.getVideoHeight();
        // Set Video Scaling Mode
//        mVideoView.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
//        mVideoView.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);
        mPlayerScale.setImageResource(R.drawable.scale_fit);
        //start player
//        mVideoView.start();

        //set progress
        setVideoProgress(0);

        mStartTime = System.currentTimeMillis();
        initPlayer();
    }

    @OptIn(markerClass = UnstableApi.class) private void initPlayer(){
        player = new ExoPlayer.Builder(this).build();
        String path = mDataSource;
//        String path = "rtsp://admin:jndv12345@192.168.1.37";
//    String path = "rtsp://172.20.96.189:554/rtp/37130000012008800000_37130202121326010072";
//        MediaSource mediaSource =
//                new RtspMediaSource.Factory().setForceUseRtpTcp(true).createMediaSource(MediaItem.fromUri(path));
//                new RtspMediaSource.Factory().createMediaSource(MediaItem.fromUri(path_url));
//        ExoPlayer player = new ExoPlayer.Builder(this).build();
        ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultDataSourceFactory(this)).createMediaSource(MediaItem.fromUri(path));

        player.setMediaSource(mediaSource);

        playerView = findViewById(R.id.player_view);
        playerView.setPlayer(player);
        player.addListener(new Player.Listener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                Player.Listener.super.onPlayerStateChanged(playWhenReady, playbackState);
                Log.e(TAG, "onPlayerStateChanged: ========" +playbackState);
//                Player.COMMAND_CHANGE_MEDIA_ITEMS
                player.play();
            }

            @Override
            public void onIsPlayingChanged(boolean isPlaying) {
                Player.Listener.super.onIsPlayingChanged(isPlaying);
                Log.e(TAG, "onIsPlayingChanged: ==isPlaying=="+isPlaying);
                if (true){
//                    cancelLoadding();
                }
            }

            @Override
            public void onIsLoadingChanged(boolean isLoading) {
                Player.Listener.super.onIsLoadingChanged(isLoading);
                Log.e(TAG, "onIsLoadingChanged: ==isLoading=="+isLoading);
            }

            @Override
            public void onPlayerError(PlaybackException error) {
                Player.Listener.super.onPlayerError(error);
                Log.e(TAG, "onPlayerError: ==error=="+new Gson().toJson(error));
            }

            @Override
            public void onPlaybackStateChanged(int playbackState) {
                Player.Listener.super.onPlaybackStateChanged(playbackState);
                Log.e(TAG, "onPlaybackStateChanged: ==playbackState=="+playbackState);
            }

            @Override
            public void onPlayerErrorChanged(@Nullable PlaybackException error) {
                Player.Listener.super.onPlayerErrorChanged(error);
                Log.e(TAG, "onPlayerErrorChanged: ==error=="+new Gson().toJson(error));
            }

            @Override
            public void onPlayWhenReadyChanged(boolean playWhenReady, int reason) {
                Player.Listener.super.onPlayWhenReadyChanged(playWhenReady, reason);
                Log.e(TAG, "onPlayWhenReadyChanged: ==playWhenReady=="+playWhenReady);
            }
        });
        player.prepare();
    }

//
//    private IMediaPlayer.OnPreparedListener mOnPreparedListener = new IMediaPlayer.OnPreparedListener() {
//        @Override
//        public void onPrepared(IMediaPlayer mp) {
//            InitView();
//        }
//    };
//
//    private IMediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener = new IMediaPlayer.OnBufferingUpdateListener() {
//        @Override
//        public void onBufferingUpdate(IMediaPlayer mp, int percent) {
//            if (mVideoView != null) {
//                long duration = mVideoView.getDuration();
//                long progress = duration * percent / 100;
////                JNLogUtil.e(TAG, "====onBufferingUpdate==duration=="+duration);
////                JNLogUtil.e(TAG, "====onBufferingUpdate==percent=="+percent);
////                JNLogUtil.e(TAG, "====onBufferingUpdate==progress=="+progress);
//                mPlayerSeekbar.setSecondaryProgress((int) progress);
//            }
//        }
//    };
//
//    private IMediaPlayer.OnVideoSizeChangedListener mOnVideoSizeChangeListener = new IMediaPlayer.OnVideoSizeChangedListener() {
//        @Override
//        public void onVideoSizeChanged(IMediaPlayer mp, int width, int height, int sarNum, int sarDen) {
//            if (mVideoWidth > 0 && mVideoHeight > 0) {
//                if (width != mVideoWidth || height != mVideoHeight) {
//                    mVideoWidth = mp.getVideoWidth();
//                    mVideoHeight = mp.getVideoHeight();
//
//                    if (mVideoView != null)
//                        mVideoView.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
//                }
//            }
//        }
//    };
//
//    private IMediaPlayer.OnSeekCompleteListener mOnSeekCompletedListener = new IMediaPlayer.OnSeekCompleteListener() {
//        @Override
//        public void onSeekComplete(IMediaPlayer mp) {
//            JNLogUtil.e(TAG, "onSeekComplete...............");
//        }
//    };
//
//    private IMediaPlayer.OnCompletionListener mOnCompletionListener = new IMediaPlayer.OnCompletionListener() {
//        @Override
//        public void onCompletion(IMediaPlayer mp) {
//            if (null!=mPlayerSeekbar)mPlayerSeekbar.setSecondaryProgress(100);
////            Toast.makeText(mContext, "OnCompletionListener, play complete.", Toast.LENGTH_LONG).show();
//            mPause = true;
//            if (null!=mPlayerStartBtn)mPlayerStartBtn.setBackgroundResource(R.drawable.ksy_pause_btn);
//            videoPlayCompletion();
//            videoPlayEnd();
//        }
//    };
//
//    private IMediaPlayer.OnErrorListener mOnErrorListener = new IMediaPlayer.OnErrorListener() {
//        @Override
//        public boolean onError(IMediaPlayer mp, int what, int extra) {
//            switch (what) {
////                case KSYVideoView.MEDIA_ERROR_UNKNOWN:
//                // Log.e(TAG, "OnErrorListener, Error Unknown:" + what + ",extra:" + extra);
//                //  break;
//                default:
//                    JNLogUtil.e(TAG, "OnErrorListener, Error:" + what + ",extra:" + extra);
//            }
//
//            videoPlayEnd();
//
//            return false;
//        }
//    };
//
//    public IMediaPlayer.OnInfoListener mOnInfoListener = new IMediaPlayer.OnInfoListener() {
//        @Override
//        public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i1) {
//            switch (i) {
//                case KSYMediaPlayer.MEDIA_INFO_BUFFERING_START:
//                    JNLogUtil.d(TAG, "Buffering Start.");
//                    break;
//                case KSYMediaPlayer.MEDIA_INFO_BUFFERING_END:
//                    JNLogUtil.d(TAG, "Buffering End.");
//                    break;
//                case KSYMediaPlayer.MEDIA_INFO_AUDIO_RENDERING_START:
////                    Toast.makeText(mContext, "Audio Rendering Start", Toast.LENGTH_SHORT).show();
//                    break;
//                case KSYMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
////                    Toast.makeText(mContext, "Video Rendering Start", Toast.LENGTH_SHORT).show();
//                    break;
//                case KSYMediaPlayer.MEDIA_INFO_RELOADED:
//                    InitView();
////                    Toast.makeText(mContext, "Succeed to reload video.", Toast.LENGTH_SHORT).show();
//                    JNLogUtil.d(TAG, "Succeed to reload video.");
//                    return false;
//            }
//            return false;
//        }
//    };

//    private IMediaPlayer.OnMessageListener mOnMessageListener = new IMediaPlayer.OnMessageListener() {
//        @Override
//        public void onMessage(IMediaPlayer iMediaPlayer, String s, String s1, double v) {
//
//        }
//    };

    private SeekBar.OnSeekBarChangeListener audioSeekbarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            JNLogUtil.e(TAG, "==onProgressChanged==i==" + i);
            JNLogUtil.e(TAG, "==onProgressChanged==b==" + b);
            mProgressTextView.setProgress(i, i + "%");
//            mVideoView.setVolume((float) i / 100, (float) i / 100);

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this.getApplicationContext();
        setContentView(R.layout.activity_vod);

        mPlayerPanel = (RelativeLayout) findViewById(R.id.player_panel);
        mPlayerStartBtn = (ImageView) findViewById(R.id.player_start);
        mPlayerSeekbar = (SeekBar) findViewById(R.id.player_seekbar);
        mPlayerVolume = (ImageView) findViewById(R.id.player_volume);
        mPlayerRotate = (ImageView) findViewById(R.id.player_rotate);
        mPlayerScreen = (ImageView) findViewById(R.id.player_screen);
        mPlayerScale = (ImageView) findViewById(R.id.player_scale);
        mPlayerPosition = (TextView) findViewById(R.id.player_time);
        topPanel = (RelativeLayout) findViewById(R.id.rightPanel_player);
        reload = (ImageView) findViewById(R.id.player_reload);
        mPlayerPanel.setVisibility(View.VISIBLE);
        mAudioSeekbar = (VerticalSeekBar) findViewById(R.id.player_audio_seekbar);
        mProgressTextView = (ProgressTextView) findViewById(R.id.ptv_open_percentage);
        mAudioSeekbar.setProgress(100);
        mAudioSeekbar.setOnSeekBarChangeListener(audioSeekbarListener);

        reload.setOnClickListener(this);
        mPlayerVolume.setOnClickListener(this);
        mPlayerRotate.setOnClickListener(this);
        mPlayerScreen.setOnClickListener(this);
        mPlayerScale.setOnClickListener(mVideoScaleButton);

        mPlayerStartBtn.setOnClickListener(mStartBtnListener);
        mPlayerSeekbar.setOnSeekBarChangeListener(mSeekBarListener);
        mPlayerSeekbar.setEnabled(true);

        mPlayerSeekbar.bringToFront();

//        mVideoView = (KSYTextureView) findViewById(R.id.texture_view);
//        mVideoView.setOnTouchListener(mTouchListener);
//        mVideoView.setKeepScreenOn(true);
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case UPDATE_SEEKBAR:
                        setVideoProgress(0);
                        break;
                    case HIDDEN_SEEKBAR:
                        mPlayerPanelShow = false;
                        if (showAudioBar) {
                            hideAudioBar();
                        }
//                        mPlayerPanel.setVisibility(View.GONE);
//                        topPanel.setVisibility(View.GONE);
                        break;
                }
            }
        };

        mDataSource = getIntent().getStringExtra("path");
        JNLogUtil.e(TAG,"==video==path=="+mDataSource);
//        mVideoView.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
//        mVideoView.setOnCompletionListener(mOnCompletionListener);
//        mVideoView.setOnPreparedListener(mOnPreparedListener);
//        mVideoView.setOnInfoListener(mOnInfoListener);
//        mVideoView.setOnVideoSizeChangedListener(mOnVideoSizeChangeListener);
//        mVideoView.setOnErrorListener(mOnErrorListener);
//        mVideoView.setOnSeekCompleteListener(mOnSeekCompletedListener);
////        mVideoView.setOnMessageListener(mOnMessageListener);
//        mVideoView.setScreenOnWhilePlaying(true);
//        mVideoView.setTimeout(5, 30);

        bufferTime = "2";
        bufferSize = "15";

        if (!TextUtils.isEmpty(bufferTime)) {
//            mVideoView.setBufferTimeMax(Integer.parseInt(bufferTime));
            Log.e(TAG, "palyer buffertime :" + bufferTime);
        }

        if (!TextUtils.isEmpty(bufferSize)) {
//            mVideoView.setBufferSize(Integer.parseInt(bufferSize));
            Log.e(TAG, "palyer buffersize :" + bufferSize);
        }

        //硬解264&265
        Log.e(TAG, "Hardware !!!!!!!!");
//        mVideoView.setDecodeMode(KSYMediaPlayer.KSYDecodeMode.KSY_DECODE_MODE_AUTO);

        try {
//            mVideoView.setDataSource(mDataSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        mVideoView.prepareAsync();
        initPlayer();
    }

    private NetStateUtil.NetChangeListener netChangeListener = new NetStateUtil.NetChangeListener() {
        @Override
        public void onNetStateChange(int netWorkState) {
            switch (netWorkState) {
                case NetState.NETWORK_MOBILE:
                    break;
                case NetState.NETWORK_WIFI:
                    break;
                case NetState.NETWORK_NONE:
                    Toast.makeText(TextureVodActivity.this, "没有监测到网络,请检查网络连接", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }
    };

    private View.OnClickListener mVideoScaleButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int mode = mVideoScaleIndex % 2;
            mVideoScaleIndex++;
            if (mHandler != null) {
                mHandler.removeMessages(HIDDEN_SEEKBAR);
                Message msg = new Message();
                msg.what = HIDDEN_SEEKBAR;
                mHandler.sendMessageDelayed(msg, 3000);
            }
//            if (mVideoView != null) {
//                if (mode == 1) {
//                    mVideoView.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
//                    mPlayerScale.setImageResource(R.drawable.scale);
//                } else {
//                    mVideoView.setVideoScalingMode(KSYMediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);
//                    mPlayerScale.setImageResource(R.drawable.scale_fit);
//                }
//            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mVideoView = null;
        closeAll();
        NetStateUtil.unregisterNetState(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            player.pause();
        }catch (Exception e){
            e.printStackTrace();
        }

//        if (mVideoView != null) {
//            mVideoView.runInBackground(true);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (mVideoView != null) {
//            mVideoView.runInForeground();
//        }
        try {
            player.play();
        }catch (Exception e){
            e.printStackTrace();
        }

        NetStateUtil.registerNetState(this, netChangeListener);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            videoPlayEnd();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public int getChangingConfigurations() {
        return super.getChangingConfigurations();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // Maybe we could support gesture detect
    private void dealTouchEvent(View view, MotionEvent event) {
        mPlayerPanelShow = !mPlayerPanelShow;

        if (mPlayerPanelShow) {
            mPlayerPanel.setVisibility(View.VISIBLE);
            topPanel.setVisibility(View.VISIBLE);
//            Toast.makeText(mContext, "可双指缩放画面,单指移动画面",Toast.LENGTH_SHORT).show();
            Message msg = new Message();
            msg.what = HIDDEN_SEEKBAR;
            if (mHandler != null)
                mHandler.sendMessageDelayed(msg, 3000);
        } else {
//            mPlayerPanel.setVisibility(View.GONE);
//            topPanel.setVisibility(View.GONE);
            if (mHandler != null)
                mHandler.removeMessages(HIDDEN_SEEKBAR);
        }
    }

    public int setVideoProgress(int currentProgress) {

//        if (mVideoView == null)
//            return -1;

//        JNLogUtil.e(TAG,"===setVideoProgress==CurrentPosition=="+mVideoView.getCurrentPosition());

        long time = currentProgress > 0 ? currentProgress : player.getCurrentPosition();
        long length = player.getDuration();
        if (-1==currentProgress){
            mPlayerSeekbar.setMax((int) length);
            mPlayerSeekbar.setProgress((int) length);
            if (time >= 0) {
                String progress = Strings.millisToString(length) + "/" + Strings.millisToString(length);
                mPlayerPosition.setText(progress);
            }
            return -1;
        }
        // Update all view elements
        mPlayerSeekbar.setMax((int) length);
        mPlayerSeekbar.setProgress((int) time);

        if (time >= 0) {
            String progress = Strings.millisToString(time) + "/" + Strings.millisToString(length);
            mPlayerPosition.setText(progress);
        }

        Message msg = new Message();
        msg.what = UPDATE_SEEKBAR;

        if (mHandler != null)
            mHandler.sendMessageDelayed(msg, 100);
        return (int) time;
    }

    private void closeAll() {
//        if (mVideoView != null) {
//            mVideoView.release();
//            mVideoView = null;
//        }
        mHandler.removeCallbacksAndMessages(null);
        mHandler = null;
        if (player != null) {
            player.release();
        }
//        finish();
    }

    private void videoPlayCompletion() {
        try {
            mHandler.removeMessages(UPDATE_SEEKBAR);
        }catch (Exception e){
            JNLogUtil.e("=======", e);
        }
        setVideoProgress(-1);
    }
    private void videoPlayEnd() {
//        if (mVideoView != null) {
//            mVideoView.release();
//            mVideoView = null;
//        }
//        mHandler.removeCallbacksAndMessages(null);
//        mHandler = null;
//        finish();
        if (player != null) {
            player.release();
        }
    }

    private View.OnClickListener mStartBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPause = !mPause;
            if (null!=mHandler){
                mHandler.removeMessages(HIDDEN_SEEKBAR);
                Message msg = new Message();
                msg.what = HIDDEN_SEEKBAR;
                mHandler.sendMessageDelayed(msg, 3000);
            }

            if (mPause) {
                mPlayerStartBtn.setBackgroundResource(R.drawable.ksy_pause_btn);
//                mVideoView.pause();
                if (player != null) {
                    player.pause();
                }
                mPauseStartTime = System.currentTimeMillis();
            } else {
                mPlayerStartBtn.setBackgroundResource(R.drawable.ksy_playing_btn);
//                mVideoView.start();
                if (player != null) {
                    player.play();
                }
                setVideoProgress(0);
                mPausedTime += System.currentTimeMillis() - mPauseStartTime;
                mPauseStartTime = 0;
            }
        }
    };

    private int mVideoProgress = 0;
    private SeekBar.OnSeekBarChangeListener mSeekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            JNLogUtil.e(TAG, "==onProgressChanged==progress==" + progress);
            JNLogUtil.e(TAG, "==onProgressChanged==fromUser==" + fromUser);
            if (fromUser) {
                mVideoProgress = progress;
                if (mHandler != null){
                    mHandler.removeMessages(HIDDEN_SEEKBAR);
                    Message msg = new Message();
                    msg.what = HIDDEN_SEEKBAR;
                    mHandler.sendMessageDelayed(msg, 3000);
                }
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
//            if (mVideoView != null)
//                mVideoView.seekTo(mVideoProgress);
            if (player != null) {
                player.seekTo(mVideoProgress);
            }
            setVideoProgress(mVideoProgress);
        }
    };

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    mTouching = false;
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    mTouching = true;
                    if (event.getPointerCount() == 2) {
                        lastSpan = getCurrentSpan(event);
                        centerPointX = getFocusX(event);
                        centerPointY = getFocusY(event);
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (event.getPointerCount() == 1) {
                        float posX = event.getX();
                        float posY = event.getY();
                        if (lastMoveX == -1 && lastMoveX == -1) {
                            lastMoveX = posX;
                            lastMoveY = posY;
                        }
                        movedDeltaX = posX - lastMoveX;
                        movedDeltaY = posY - lastMoveY;

                        if (Math.abs(movedDeltaX) > 5 || Math.abs(movedDeltaY) > 5) {
//                            if (mVideoView != null) {
//                                mVideoView.moveVideo(movedDeltaX, movedDeltaY);
//                            }
                            mTouching = true;
                        }
                        lastMoveX = posX;
                        lastMoveY = posY;
                    } else if (event.getPointerCount() == 2) {
                        double spans = getCurrentSpan(event);
                        if (spans > 5)
                        {
                            deltaRatio = (float) (spans / lastSpan);
//                            totalRatio = mVideoView.getVideoScaleRatio() * deltaRatio;
                            /*
                            //限定缩放边界,如果视频的宽度小于屏幕的宽度则停止播放
                            if ((rotateNum / 90) %2 != 0){
                                if (totalRatio * mVideoWidth <= mVideoView.getHeight())
                                    break;
                            }
                            else {
                                if (totalRatio * mVideoWidth <= mVideoView.getWidth())
                                    break;
                            }
                            */
//                            if(mVideoView != null){
//                                mVideoView.setVideoScaleRatio(totalRatio, centerPointX, centerPointY);
//                            }
                            lastSpan = spans;
                        }
                    }
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    if (event.getPointerCount() == 2) {
                        lastMoveX = -1;
                        lastMoveY = -1;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    lastMoveX = -1;
                    lastMoveY = -1;

                    if (!mTouching){
                        dealTouchEvent(v, event);
                    }
                    break;
                default:
                    break;
            }
            return true;
        }
    };

    private double getCurrentSpan(MotionEvent event) {
        float disX = Math.abs(event.getX(0) - event.getX(1));
        float disY = Math.abs(event.getY(0) - event.getY(1));
        return Math.sqrt(disX * disX + disY * disY);
    }

    private float getFocusX(MotionEvent event){
        float xPoint0 = event.getX(0);
        float xPoint1 = event.getX(1);
        return (xPoint0 + xPoint1) / 2;
    }

    private float getFocusY(MotionEvent event) {
        float yPoint0 = event.getY(0);
        float yPoint1 = event.getY(1);
        return (yPoint0 + yPoint1) / 2;
    }


    @Override
    public void onClick(View view) {
        if (mHandler != null) {
            mHandler.removeMessages(HIDDEN_SEEKBAR);
            Message msg = new Message();
            msg.what = HIDDEN_SEEKBAR;
            mHandler.sendMessageDelayed(msg, 3000);
        }

        int id = view.getId();
        if (id == R.id.player_volume) {
            JNLogUtil.e("==volume====01====");
            if (!showAudioBar) {
                JNLogUtil.e("==volume====02====");
                showAudioBar();
            } else {
                JNLogUtil.e("==volume====03====");
                hideAudioBar();
            }
        } else if (id == R.id.player_reload) {
//            String mVideoUrl2 = "rtmp://live.hkstv.hk.lxdns.com/live/hks";
//            // 播放新的视频
//            mVideoView.reload(mVideoUrl2, true);
//            MediaSource mediaSource =
//                    new RtspMediaSource.Factory().setForceUseRtpTcp(true).createMediaSource(MediaItem.fromUri(path));
////                new RtspMediaSource.Factory().createMediaSource(MediaItem.fromUri(path_url));
//            player.setMediaSource(mediaSource);

        } else if (id == R.id.player_rotate) {
            rotateNum += 90;
//            mVideoView.setRotateDegree(rotateNum % 360);
            playerView.setRotation(rotateNum % 360);
        } else if (id == R.id.player_screen) {
//            Bitmap bitmap = mVideoView.getScreenShot();
            Bitmap bitmap = playerView.getDrawingCache();
            savebitmap(bitmap);
            if (bitmap != null) {
                Toast.makeText(TextureVodActivity.this, "截图成功", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void hideAudioBar() {
        mAudioSeekbar.setVisibility(View.INVISIBLE);
        mProgressTextView.setVisibility(View.INVISIBLE);
        showAudioBar = false;
    }

    private void showAudioBar() {
        JNLogUtil.e("==volume====04====");
        mAudioSeekbar.setVisibility(View.VISIBLE);
        mProgressTextView.setVisibility(View.VISIBLE);
        JNLogUtil.e("==volume====05====");
        int Progress = mAudioSeekbar.getProgress();
        mProgressTextView.setProgress(Progress, Progress + "%");
        showAudioBar = true;
    }

    public void savebitmap(Bitmap bitmap) {
        File appDir = new File(Environment.getExternalStorageDirectory(), "com.ksy.recordlib.demo.demo");
        if (!appDir.exists()) {
            appDir.mkdir();
        }

        if (bitmap == null)
            return;

        String fileName = System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

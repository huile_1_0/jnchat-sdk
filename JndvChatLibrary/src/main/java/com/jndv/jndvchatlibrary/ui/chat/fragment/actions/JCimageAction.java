package com.jndv.jndvchatlibrary.ui.chat.fragment.actions;

import android.Manifest;
import android.annotation.SuppressLint;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jnbaseutils.chat.listUi.JCbaseActions;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.utils.JNFileUtils;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;

import com.jndv.jndvchatlibrary.http.api.JCImagesUpApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgImgContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgVideoContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCUriToPathUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
//import com.qingmei2.rximagepicker.core.RxImagePicker;
//import com.qingmei2.rximagepicker.entity.Result;
//import com.qingmei2.rximagepicker_extension.MimeType;
//import com.qingmei2.rximagepicker_extension_wechat.WechatConfigrationBuilder;
import com.jndv.jndvchatlibrary.utils.JCzhihuImagePicker;
import com.qingmei2.rximagepicker.core.RxImagePicker;
import com.qingmei2.rximagepicker.entity.Result;
import com.qingmei2.rximagepicker_extension.MimeType;
import com.qingmei2.rximagepicker_extension_zhihu.ZhihuConfigurationBuilder;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.functions.Consumer;

/**
 * Author: wangguodong
 * Date: 2022/2/16
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 相册-聊天页面加号菜单中菜单项
 */
public class JCimageAction extends JCbaseActions {

    public JCimageAction(int nameId, int iconId) {
        super(nameId, iconId);
    }

    public JCimageAction() {
        super(R.string.action_image, R.drawable.jc_chat_action_album);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onClick() {
        RxPermissions rxPermissions = new RxPermissions(getActivity());
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
        )
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean) {
                            //申请的权限全部允许
                            RxImagePicker
                                    .create(JCzhihuImagePicker.class)
                                    .openGalleryAsNormal(getContainer().activity,
                                            new ZhihuConfigurationBuilder(MimeType.INSTANCE.ofAll()
                                                    , false)
                                            .maxSelectable(9)
                                            .spanCount(4)
                                            .countable(false)
                                            .build())             //打开微信相册选取图片
                                    .subscribe(new Consumer<Result>() {
                                        @Override
                                        public void accept(Result result) {
                                            Uri uri = result.getUri();
                                            Log.e("uri", uri.toString());
                                            String typef = JNFileUtils.getMimeType(getContainer().activity, uri);
                                            Log.e("0428", "accept: ===========type="+typef);//image/jpeg   video/mp4
                                            if (typef.contains("video")){
                                                sendVideo("", JCUriToPathUtils.getRealPathFromUri(uri));
                                            }else {
                                                selecteImage(uri);
                                            }
                                        }
                                    });
                        } else {
                            //只要有一个权限被拒绝，就会执行
                            JCChatManager.showToast("未授权权限，部分功能不能使用");
                        }
                    }
                });
    }

    private void selecteImage(Uri uri){
        int whd = 100 ;
        int whh = 100 ;
        String type = "jpg";
        try {
            String[] wh = JCglideUtils.getBitmapWHFromUri(getActivity(), uri);
            type = wh[2];
            try {
                whd = Integer.parseInt(wh[0]);
            }catch (Exception e){e.printStackTrace();}

            try {
                whh = Integer.parseInt(wh[1]);
            }catch (Exception e){e.printStackTrace();}
        }catch (Exception e){
            e.printStackTrace();
        }

        JCMsgImgContentBean msgEntity = new JCMsgImgContentBean(JCUriToPathUtils.getRealPathFromUri(uri),whd,whh, type);
        String contentText = new Gson().toJson(msgEntity);
        String contentBase64 = Base64.encodeToString(contentText.getBytes(),Base64.NO_WRAP);

        JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                contentBase64,""+ JCmessageType.IMG, getOtherId(), JNSpUtils.getString(getContainer().activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
                , JNBasePreferenceSaves.getSipAddress(), getContainer().jCsessionChatInfoBean.getSessionChatId(), getContainer().jCsessionChatInfoBean.getSessionaddress()
                , ""+getContainer().sessionType, JNBasePreferenceSaves.getJavaAddress(),getContainer().jCsessionChatInfoBean.getSessionaddressJava());

        getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,0);

        JCFilesUpUtils.upImage(uri, type, new OnHttpListener<JCImagesUpApi.Bean>() {
            @Override
            public void onSucceed(JCImagesUpApi.Bean result) {
                try {
//                                                        Box<JCimMessageBean > box = JCobjectBox.get().boxFor(JCimMessageBean.class);
                    if (TextUtils.equals("success", result.getMsg())){
                        JCMsgImgContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(jCimMessageBean.getContent(),Base64.NO_WRAP)), JCMsgImgContentBean.class);
                        msgEntity.setUrl(JNBaseUtilManager.filesServerUrl+result.getUrlArr().get(0).getTailUrl());
//                                                            msgEntity.setUri(null);
                        String msgEntityText = new Gson().toJson(msgEntity);
                        jCimMessageBean.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
                        JCThreadManager.onMainHandler(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("zhang","发送图片消息");
                                getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,1);
                            }
                        });
                    }else {
                        JCThreadManager.onMainHandler(new Runnable() {
                            @Override
                            public void run() {
                                getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(), JCmessageStatusType.sendFail);
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(Exception e) {
                JCThreadManager.onMainHandler(new Runnable() {
                    @Override
                    public void run() {
                        getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(),JCmessageStatusType.sendFail);
                    }
                });
            }
        });
    }


    protected void sendVideo( String time ,  String filePath) {
        JCMsgVideoContentBean videoContentBean = new JCMsgVideoContentBean(time, filePath);
        String contentText = new Gson().toJson(videoContentBean);
        String contentBase64 = Base64.encodeToString(contentText.getBytes(),Base64.NO_WRAP);
//
        JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                contentBase64,""+ JCmessageType.VIDEO, getContainer().jCsessionChatInfoBean.getSessionChatId(), JNSpUtils.getString(getContainer().activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
                , JNBasePreferenceSaves.getSipAddress(), getContainer().jCsessionChatInfoBean.getSessionChatId(), getContainer().jCsessionChatInfoBean.getSessionaddress()
                , ""+getContainer().sessionType, JNBasePreferenceSaves.getJavaAddress(),getContainer().jCsessionChatInfoBean.getSessionaddressJava());

        getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,0);

        Log.e("sendVideo",filePath);
        Log.e("sendVideo",time+"++++");

        JCFilesUpUtils.upCommonFile(filePath,JCmessageType.VIDEO, new OnHttpListener<String>() {
            @Override
            public void onSucceed(String url) {
                JNLogUtil.e("====JCvideoAction===upCommonFile==001======url=="+url);
                try {
                    if (TextUtils.isEmpty(url)){
                        JCThreadManager.onMainHandler(() -> getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(), JCmessageStatusType.sendFail));
                    }else {
                        JNLogUtil.e("====JCvideoAction===upCommonFile==002========"+JNBaseUtilManager.getFilesUrl(url));
                        videoContentBean.setUrl(JNBaseUtilManager.getFilesUrl(url));
                        String msgEntityText = new Gson().toJson(videoContentBean);
                        jCimMessageBean.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
                        JCThreadManager.onMainHandler(() -> getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,1));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(Exception e) {
                JCThreadManager.onMainHandler(() -> getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(),JCmessageStatusType.sendFail));
            }
        });
    }

}

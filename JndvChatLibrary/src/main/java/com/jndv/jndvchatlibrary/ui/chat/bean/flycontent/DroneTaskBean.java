package com.jndv.jndvchatlibrary.ui.chat.bean.flycontent;

public class DroneTaskBean {
    private Data data = new Data();
    private Header header = new Header();

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Header getHeader() {
        return header;
    }

    public class Filter {

        private long plane_sip;

        public void setPlane_sip(long plane_sip) {
            this.plane_sip = plane_sip;
        }

        public long getPlane_sip() {
            return plane_sip;
        }

    }

    public class Data {

        private Filter filter = new Filter();
        private int limit;
        private int page;

        public void setFilter(Filter filter) {
            this.filter = filter;
        }

        public Filter getFilter() {
            return filter;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public int getLimit() {
            return limit;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPage() {
            return page;
        }

    }


    public class Header {

        private int res;
        private int des;
        private int msg_no;
        private long msg_id;
        private long timestamp;

        public void setRes(int res) {
            this.res = res;
        }

        public int getRes() {
            return res;
        }

        public void setDes(int des) {
            this.des = des;
        }

        public int getDes() {
            return des;
        }

        public void setMsg_no(int msg_no) {
            this.msg_no = msg_no;
        }

        public int getMsg_no() {
            return msg_no;
        }

        public void setMsg_id(long msg_id) {
            this.msg_id = msg_id;
        }

        public long getMsg_id() {
            return msg_id;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        public long getTimestamp() {
            return timestamp;
        }

    }
}

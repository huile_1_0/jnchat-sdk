package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

public class JCAdministratorKicksMemberApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/group/administratorKicksMember";
    }

    /**
     * domainAddr	是	string	(操作人)用户域地址
     * userId	是	string	(操作人)用户SIP id
     * groupId	是	number	群聊id
     * groupDomainAddr	是	string	群聊所在域地址
     * kickedDomainAddr	是	string	被踢的用户域地址
     * kickedUserId	是	string	被踢的用户SIP id
     */

    private String userId;
    private String domainAddr;
    private String groupId;
    private String groupDomainAddr;
    private String kickedUserId;
    private String kickedDomainAddr;


    public JCAdministratorKicksMemberApi(String userId, String domainAddr, String groupId, String groupDomainAddr, String kickedUserId, String kickedDomainAddr) {
        this.userId = userId;
        this.domainAddr = domainAddr;
        this.groupId = groupId;
        this.groupDomainAddr = groupDomainAddr;
        this.kickedUserId = kickedUserId;
        this.kickedDomainAddr = kickedDomainAddr;
    }
}

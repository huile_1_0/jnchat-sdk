package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gjiazhe.wavesidebar.WaveSideBar;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcfriendlistBinding;
import com.jndv.jndvchatlibrary.http.api.JCMyFriendApi;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCChatActivity;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCOrganzationListActivity;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgCardContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCCreateSessionUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jnbaseutils.ui.JNSimpleDividerDecoration;
import com.jndv.jndvchatlibrary.ui.crowd.activity.JCCrowdListActivity;
import com.jndv.jndvchatlibrary.utils.JCFriendUtil;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import net.sourceforge.pinyin4j.PinyinHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/4/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 好友列表房fragment
 */
public class JCFriendListFragment extends JCbaseFragment {
    public final static String KEY_IS_SELECTE = "key_is_selecte" ;
    private FragmentJcfriendlistBinding binding;
    private JCFriendListViewModel JCFriendListViewModel;
    private CommonAdapter adapter ;
    private List<AdapterDataBean> datas = new ArrayList<>();
    private boolean isSelecte = false;
    private final static String[] DEFAULT_INDEX_ITEMS = {"#","A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        JCFriendListViewModel =
                new ViewModelProvider(this).get(JCFriendListViewModel.class);
        binding = FragmentJcfriendlistBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init(root);
//        EventBus.getDefault().register(this);
        return root;
    }

    public void setSelecte(){
        isSelecte = true;
        binding.llMonitors.setVisibility(View.GONE);
        binding.llGroup.setVisibility(View.GONE);
        binding.llOrganization.setVisibility(View.GONE);
        binding.llOrganization2.setVisibility(View.GONE);
    }

    @SuppressLint("SuspiciousIndentation")
    private void init(View view) {
        if (JCChatManager.showMonitor){
            binding.llMonitors.setVisibility(View.VISIBLE);
            binding.vLineMonitor.setVisibility(View.VISIBLE);
        }
        Bundle bundle = getArguments();
        if (bundle != null){
            if (bundle.getBoolean(KEY_IS_SELECTE, false)) setSelecte();
        }
        JCFriendListViewModel.init(this);
        initRecycleView();
        binding.wavesidebar.setIndexItems(DEFAULT_INDEX_ITEMS);
        DisplayMetrics dm = getResources().getDisplayMetrics();         //实例化显示指标对象
        int displayWidth = dm.widthPixels;      //得到屏幕宽度
        int displayHeight = dm.heightPixels;    //得到屏幕高度
        JNLogUtil.e("==JCFriendListFragment=====displayWidth=="+displayWidth);//720
        JNLogUtil.e("==JCFriendListFragment=====displayHeight=="+displayHeight);//1520
        if (displayHeight>=1600)binding.wavesidebar.setTextSize(26);
        else if (displayHeight>=1400)binding.wavesidebar.setTextSize(23);//20
        else if (displayHeight>=1200)binding.wavesidebar.setTextSize(20);//20
        else if (displayHeight>=1000)binding.wavesidebar.setTextSize(17);//20
        else binding.wavesidebar.setTextSize(12);
//        binding.wavesidebar.setTextSize(12);
        //设置右边字母滑动时的事件
        binding.wavesidebar.setOnSelectIndexItemListener(new WaveSideBar.OnSelectIndexItemListener() {
            @Override
            public void onSelectIndexItem(String index) {
                //获得滑动位置的字母在adapter里的位置
                JNLogUtil.e("=========index="+index);
                for (int i = 0; i < datas.size(); i++) {
                    JNLogUtil.e("=========datas.get(i).getName()="+datas.get(i).getName());
                    if (TextUtils.equals(index.toLowerCase(), datas.get(i).getName().toLowerCase())){
                        JNLogUtil.e("=========true==");
//                    //跳转到对应的位置
                        binding.rvFriend.scrollToPosition(i);
//                        binding.rvFriend.smoothScrollToPosition(i);
//                        LinearLayoutManager mLayoutManager =
//                                (LinearLayoutManager) binding.rvFriend.getLayoutManager();
//                        if (null!=mLayoutManager)mLayoutManager.scrollToPositionWithOffset(i, 0);
                        return;
                    }
                }
            }
        });
        initViewData();
    }

    private void initViewData(){
        JCFriendListViewModel.getFriendList().observeForever(new Observer<JCFriendListViewModel.JCFriendListModelItem>() {
            @Override
            public void onChanged(JCFriendListViewModel.JCFriendListModelItem jcFriendListModelItem) {
                if (jcFriendListModelItem.getType()==0){
                    List<AdapterDataBean> list = new ArrayList<>();
                    if (null!=jcFriendListModelItem.getDatas()&&jcFriendListModelItem.getDatas().size()>0){
                        for (int i = 0; i < jcFriendListModelItem.getDatas().size(); i++) {
                            JCMyFriendApi.Bean.DataBean dataBean = jcFriendListModelItem.getDatas().get(i);
                            AdapterDataBean adapterDataBean = new AdapterDataBean(0,getPinYinFristHeadChar(getName(dataBean))
                                    , jcFriendListModelItem.getDatas().get(i));
                            list.add(adapterDataBean);
                            JCFriendUtil.initLocalFriend(dataBean);
                        }
                        Collections.sort(list, new LetterComparator());
                        datas.addAll(list);
                        adapter.notifyDataSetChanged();
                        if (null==datas||datas.size()<=0){
                            binding.llNull.setVisibility(View.VISIBLE);
                        }else {
                            binding.llNull.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });
        JCFriendListViewModel.getFriendListData();
    }

//    private void initLocalFriend(JCMyFriendApi.Bean.DataBean dataBean){
//        try {
//            Box<JCFriendBean> imMsgBox = JCobjectBox.get().boxFor(JCFriendBean.class);
//            QueryBuilder<JCFriendBean> builder = imMsgBox.query();
//            JCFriendBean jcFriendBean = builder.equal(JCFriendBean_.fid, dataBean.getId()).build().findFirst();
//            if (null!=jcFriendBean){
//                jcFriendBean.setInfo(dataBean);
//                jcFriendBean.setSaveUid(JNBasePreferenceSaves.getUserSipId());
//                imMsgBox.put(jcFriendBean);
//            }else {
//                JCFriendBean friendBean = new JCFriendBean();
//                friendBean.setInfo(dataBean);
//                friendBean.setSaveUid(JNBasePreferenceSaves.getUserSipId());
//                imMsgBox.put(friendBean);
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//    }

    private String getName(JCMyFriendApi.Bean.DataBean dataBean){
        String name = "";
        if (null!=dataBean){
            if (null!=dataBean.getRemark() && !TextUtils.isEmpty(dataBean.getRemark())){
                name = dataBean.getRemark();
            }else if (null!=dataBean.getFriendName() && !TextUtils.isEmpty(dataBean.getFriendName())){
                name = dataBean.getFriendName();
            }else if (null!=dataBean.getFriendId() && !TextUtils.isEmpty(dataBean.getFriendId())){
                name = dataBean.getFriendId();
            }
        }
        return name;
    }

    private void initRecycleView(){
        binding.rvFriend.setLayoutManager(new LinearLayoutManager(getActivity()));
        JNSimpleDividerDecoration divider = new JNSimpleDividerDecoration(getActivity());
        divider.setPadding(25,30);
        binding.rvFriend.addItemDecoration(divider);
        adapter = new CommonAdapter<AdapterDataBean>(getActivity(), R.layout.jc_item_friend, datas) {
            @Override
            protected void convert(ViewHolder holder, AdapterDataBean adapterDataBean, int position) {
                JCglideUtils.loadCircleImage(getActivity(), adapterDataBean.getDataBean().getFriendPic(), holder.getView(R.id.iv_user_head));
                ((TextView)holder.getView(R.id.tv_user_name)).setText(getName(adapterDataBean.getDataBean()));
                ((TextView)holder.getView(R.id.tv_name)).setText(adapterDataBean.getName());
                if (0==position || !TextUtils.equals(datas.get(position-1).getName(), adapterDataBean.getName())){
//                    holder.getView(R.id.tv_name).setVisibility(View.VISIBLE);
                    holder.getView(R.id.tv_name).setVisibility(View.GONE);
                }else {
                    holder.getView(R.id.tv_name).setVisibility(View.GONE);
                }
                    holder.getView(R.id.ll_friend).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if (isSelecte){
                                    JCMsgCardContentBean jcMsgCardContentBean = new JCMsgCardContentBean(
                                            adapterDataBean.getDataBean().getFriendName(), adapterDataBean.getDataBean().getFriendPic()
                                            , adapterDataBean.getDataBean().getFriendId(), adapterDataBean.getDataBean().getDomainAddr()
                                            , adapterDataBean.getDataBean().getDomainAddrUser());
                                    if (null!=jcFragmentSelect)jcFragmentSelect.onSelecte(JCbaseFragment.SELECTE_TYPE_FRIEND, jcMsgCardContentBean);
                                    getActivity().finish();
                                }else {
                                    JCSessionListBean messageBean = JCSessionUtil.getJCSessionListBean(
                                            adapterDataBean.getDataBean().getFriendId(), JCSessionType.CHAT_PERSION, adapterDataBean.getDataBean().getDomainAddr());
                                    Log.e("TAG", "onClick: =========001=========");
                                    if (null!=messageBean){
                                        Log.e("TAG", "onClick: =========002=========");
                                        JCChatActivity.start(getActivity(), messageBean);
                                    }else {
                                        Log.e("TAG", "onClick: =========003=========");
                                        JCCreateSessionUtils.creatSession(getActivity(), adapterDataBean.getDataBean().getFriendId()
                                                , adapterDataBean.getDataBean().getDomainAddr(), adapterDataBean.getDataBean().getDomainAddrUser(), "" + JCSessionType.CHAT_PERSION
                                                , adapterDataBean.getDataBean().getFriendName(), adapterDataBean.getDataBean().getFriendPic(), true);
                                    }
                                }

                            }catch (Exception e){
                                JNLogUtil.e("==JCFriendListFragment==CommonAdapter==onClick==isSelecte==="+isSelecte, e);
                                if (isSelecte){

                                }else {
                                    JCCreateSessionUtils.creatSession(getActivity(), adapterDataBean.getDataBean().getFriendId()
                                            , adapterDataBean.getDataBean().getDomainAddr(), adapterDataBean.getDataBean().getDomainAddrUser(), "" + JCSessionType.CHAT_PERSION
                                            , adapterDataBean.getDataBean().getFriendName(), adapterDataBean.getDataBean().getFriendPic(), true);
                                }
                            }
                        }
                    });
            }
        };
        binding.rvFriend.setAdapter(adapter);
        binding.llOrganization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int nid = 0 ;
//                try {
//                    nid = Integer.parseInt(JNBasePreferenceSaves.getOrganzationId());
//                }catch (Exception e){e.printStackTrace();}
//                Bundle bundle = new Bundle();
//                bundle.putInt("nid", nid);
//                JCOrganizationFragment jcOrganizationFragment = new JCOrganizationFragment();
//                jcOrganizationFragment.setArguments(bundle);
//                JCbaseFragmentActivity.start(getActivity(), jcOrganizationFragment, "组织结构");
            }
        });
        binding.llOrganization2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String zoningCode = JNBasePreferenceSaves.getZoningCode();
                JCOrganzationListActivity.start(getActivity(), zoningCode, "组织结构");
            }
        });
        binding.llGroup.setOnClickListener(v -> {
            JCCrowdListActivity.start(getActivity());
        });
        binding.llMonitors.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
//            bundle.putString("nid", JNBasePreferenceSaves.getZoningCode()); 371312001D004
            bundle.putString("zoningCode", JNBasePreferenceSaves.getZoningCode());
            JCbaseFragmentActivity.start(getActivity(), new JCOrganizationMonitorFragment(), "监控列表", bundle);
        });
        binding.llGPSStart.setOnClickListener(v -> {
//            isStartGps = !isStartGps;
//            if (isStartGps){
//                JCGpsManager.getInstance().startSendGps();
//                (binding.tvGps).setText("去关闭GPS");
//            }else {
//                JCGpsManager.getInstance().stopSendGps();
//                (binding.tvGps).setText("去开始GPS");
//            }
        });
    }

    private boolean isStartGps = false;
    /**
     * 得到中文首字母
     *
     * @param str
     * @return
     */
    public static String getPinYinFristHeadChar(String str) {
        String convert = "";
        if(str.length()>=1){
            char word = str.charAt(0);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert += pinyinArray[0].charAt(0);
            } else {
                convert += word;
            }
        }
        return convert;
    }


    public class AdapterDataBean{
        private int type ;
        private String name;
        private JCMyFriendApi.Bean.DataBean dataBean;

        public AdapterDataBean(int type, String name, JCMyFriendApi.Bean.DataBean dataBean) {
            this.type = type;
            this.name = name;
            this.dataBean = dataBean;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public JCMyFriendApi.Bean.DataBean getDataBean() {
            return dataBean;
        }

        public void setDataBean(JCMyFriendApi.Bean.DataBean dataBean) {
            this.dataBean = dataBean;
        }
    }

    public class LetterComparator implements Comparator<AdapterDataBean> {

        @Override
        public int compare(AdapterDataBean l, AdapterDataBean r) {
            if (l == null || r == null) {
                return 0;
            }
            String lhsSortLetters = l.getName().toUpperCase();
            String rhsSortLetters = r.getName().toUpperCase();
            return lhsSortLetters.compareTo(rhsSortLetters);
        }
    }
}

package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.graphics.drawable.AnimationDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgVoiceContentBean;
import com.jndv.jnbaseutils.utils.MediaPlayerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 语音信息展示
 * @Author SunQinzheng
 * @Time 2022/6/22 上午 11:25
 **/
public class JCmsgViewHolderVoice extends JCmsgViewHolderBase {

    private TextView tvMessage;
    private ImageView ivLeftVoice;
    private ImageView ivRightVoice;
    AnimationDrawable animaition;
    private boolean isReceived;//左边

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_voice;
    }

    @Override
    protected void inflateContentView() {
        tvMessage = findViewById(R.id.tv_msg_long);
        ivLeftVoice = findViewById(R.id.iv_left_voice);
        ivRightVoice = findViewById(R.id.iv_right_voice);
    }

    @Override
    protected void bindContentView() {
        isReceived = isReceivedMessage();
        if (isReceived) {
            ivLeftVoice.setVisibility(View.VISIBLE);
            ivRightVoice.setVisibility(View.GONE);
            animaition = (AnimationDrawable) ivLeftVoice.getBackground();
        } else {
            ivLeftVoice.setVisibility(View.GONE);
            ivRightVoice.setVisibility(View.VISIBLE);
            animaition = (AnimationDrawable) ivRightVoice.getBackground();
        }


        Log.e("bindContentView", message.getContent());
        JNLogUtil.e("======Content==" + base64ToString(message.getContent()));
        Log.d("zhang","声音content=="+ base64ToString(message.getContent()));


        try {
            JCMsgVoiceContentBean bean = new Gson().fromJson(base64ToString(message.getContent()), JCMsgVoiceContentBean.class);

            JNLogUtil.e("======SendTime==" + message.getSendTime());
            JNLogUtil.e("======ReceiveTime==" + message.getReceiveTime());
            tvMessage.setText(bean.getTime());
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            tvMessage.setText(bean.getText()+"\n"+format.format(new Date(Long.parseLong(message.getSendTime())))
//                    +"\n"+format.format(new Date(Long.parseLong(message.getReceiveTime()))));
        } catch (Exception e) {
            e.printStackTrace();
            JNLogUtil.e("======getMsgType==" + message.getMsgType());
            tvMessage.setText("未识别的消息：" + message.getContent());
        }
        MediaPlayerUtil.getInstances().getAnimationMap().put(position, animaition);

        JNLogUtil.e("======Content==" + position + "========" + MediaPlayerUtil.getInstances().getAnimationMap().size() + "++++++");
    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_audio_receiver), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_transmit), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_collect), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_withdraw), 1));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_delete), 2));
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return false;
    }

    @Override
    protected void onItemClick() {
        playVoice(false);
    }
    @Override
    protected void playVoiceMessage(boolean useReceiver){
        playVoice(useReceiver);
    }

    private void playVoice(boolean useReceiver){
        //        super.onItemClick();
        JCMsgVoiceContentBean bean = new Gson().fromJson(base64ToString(message.getContent()), JCMsgVoiceContentBean.class);
//            JCMsgImgContentBean bean = new Gson().fromJson(message.getContent(), JCMsgImgContentBean.class);

        animaition.stop();
        //停止所有播放动画
        for (AnimationDrawable drawable : MediaPlayerUtil.getInstances().getAnimationMap().values()) {
            drawable.stop();
        }
        if (position == MediaPlayerUtil.playPos) {//点击的和上一个一致
            if (MediaPlayerUtil.isPlaying()){//如果正在播放，则暂停
                MediaPlayerUtil.pause();
            }else if (MediaPlayerUtil.isIsPause()){//如果是暂停的状态，则继续播放
                MediaPlayerUtil.resume();
                animaition.start();
            }
            if (!MediaPlayerUtil.isPlaying() && !MediaPlayerUtil.isIsPause()) {//不在播放状态也不在暂停状态，即语音未播放或播放完成，则重新加载播放
                animaition.start();
                MediaPlayerUtil.playSound(position, useReceiver, TextUtils.isEmpty(bean.getUrl()) ? bean.getPath() : bean.getUrl(), mp -> {
                    animaition.stop();
                    animaition.selectDrawable(0);
                },(mp, what, extra) -> {
                    animaition.stop();
                    animaition.selectDrawable(0);
                    JCChatManager.showToast("播放失败！");
                    return false;
                });
            }

        } else {//点击另一条语音，则重新加载语音，开始播放
            animaition.start();
            MediaPlayerUtil.playSound(position,useReceiver, TextUtils.isEmpty(bean.getUrl()) ? bean.getPath() : bean.getUrl(), mp -> {
                animaition.stop();
                animaition.selectDrawable(0);
            },(mp, what, extra) -> {
                animaition.stop();
                animaition.selectDrawable(0);
                JCChatManager.showToast("播放失败！");
                return false;
            });


        }



   /*
        if (MediaPlayerUtil.isPlaying()) {
            MediaPlayerUtil.pause();

        }else {
            if (MediaPlayerUtil.isIsPause()){
                MediaPlayerUtil.resume();
            }else {

            }


        }

*/
    }
}

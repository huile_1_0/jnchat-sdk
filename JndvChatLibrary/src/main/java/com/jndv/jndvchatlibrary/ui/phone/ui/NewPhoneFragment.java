package com.jndv.jndvchatlibrary.ui.phone.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Message;
import android.provider.Settings;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.DialerKeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ehome.manager.utils.JNLogUtil;
import com.google.android.material.tabs.TabLayout;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.ui.base.JNBaseFragment;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipP2PActivity;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipVideoActivity;
import com.jndv.jndvchatlibrary.databinding.FragmentNewPhoneBinding;
import com.jndv.jndvchatlibrary.ui.base.BaseFragment;
import com.jndv.jndvchatlibrary.ui.crowd.utils.CommonUtil;
import com.jndv.jndvchatlibrary.ui.phone.interfaces.KeyBoardDialogListener;
import com.jndv.jndvchatlibrary.ui.phone.view.DigitsEditText;

import com.tbruyelle.rxpermissions2.RxPermissions;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import io.reactivex.functions.Consumer;


public final class NewPhoneFragment extends JNBaseFragment implements View.OnClickListener,TabLayout.OnTabSelectedListener {
//public final class NewPhoneFragment extends BaseFragment implements View.OnClickListener,TabLayout.OnTabSelectedListener {
    private static final String TAG = "NewPhoneFragment";

    private ViewPager viewpager;
    private TabLayout tl_tablayout;
    private ImageView floatKeyboard;
    private MyAdapter adapter;

    private DigitsEditText mDigits;
    private static final char PAUSE = ',';//延长暂停时间2秒
    private static final char WAIT = ';';//延长等待时间

    /**
     * Stream type used to play the DTMF tones off call, and mapped to the volume control keys (流类型用于播放DTMF音调打电话,和映射到音量控制键)
     */
    private static final int DIAL_TONE_STREAM_TYPE = AudioManager.STREAM_DTMF;

    /**
     * The DTMF tone volume relative to other sounds in the stream (DTMF语气体积相对于其他的声音流)
     */
    private static final int TONE_RELATIVE_VOLUME = 80;

    /**
     * The length of DTMF tones in milliseconds(DTMF音调以毫秒为单位的长度)
     */
    private static final int TONE_LENGTH_INFINITE = 300;


    // determines if we want to playback local DTMF tones.(确定如果我们想播放本地DTMF音调。)
    private boolean mDTMFToneEnabled;

    /**
     * Set of dialpad keys that are currently being pressed
     */
    private final HashSet<View> mPressedDialpadKeys = new HashSet<View>(12);

    private ToneGenerator mToneGenerator; //音频发生器
    private final Object mToneGeneratorLock = new Object(); //Lock

    FragmentNewPhoneBinding binding;

    private List<Fragment> fragmentList;

    public static NewPhoneFragment newInstance() {
        return new NewPhoneFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentNewPhoneBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        initView(root);
        return root;
    }

    public void initView(View view) {
        viewpager = view.findViewById(R.id.viewpager);
        tl_tablayout = view.findViewById(R.id.tl_tablayout);
        floatKeyboard = view.findViewById(R.id.float_keyboard);
        floatKeyboard.setOnClickListener(this);
        initTabLayout();
        init();
        initTab();
    }

    private void initTabLayout() {
        tl_tablayout.addOnTabSelectedListener(this);
    }

    private void init() {
        fragmentList = new ArrayList<>();
        fragmentList.add(AllCallFragment.newInstance());
        fragmentList.add(MissedCallFragment.newInstance());
        //创建自定的适配器对象
        adapter = new MyAdapter(getChildFragmentManager(), fragmentList);
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tl_tablayout));

    }

    private void initTab() {
        tl_tablayout.addTab(tl_tablayout.newTab().setText("所有电话"));
        tl_tablayout.addTab(tl_tablayout.newTab().setText("未接电话"));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        synchronized (mToneGeneratorLock) {
            if (mToneGenerator == null) {
                try {
                    mToneGenerator = new ToneGenerator(DIAL_TONE_STREAM_TYPE, TONE_RELATIVE_VOLUME);
                } catch (RuntimeException e) {
                    Log.w(TAG, "Exception caught while creating local tone generator: " + e);
                    mToneGenerator = null;
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        final ContentResolver contentResolver = getActivity().getContentResolver();
        mDTMFToneEnabled = Settings.System.getInt(contentResolver,
                Settings.System.DTMF_TONE_WHEN_DIALING, 1) == 1;

        mPressedDialpadKeys.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPressedDialpadKeys.clear();
    }

    @Override
    public void onStop() {
        super.onStop();
        synchronized (mToneGeneratorLock) {
            if (mToneGenerator != null) {
                mToneGenerator.release();
                mToneGenerator = null;
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.float_keyboard) {
            showKeyBoard();
        }
    }

    //拨号键盘dialog
    public void hideKeyBoard() {
        CommonUtil.hideKeyBoardDialog();
    }
    public void showKeyBoard() {
        CommonUtil.showKeyBoardDialog(getActivity(), binding.getRoot(), new KeyBoardDialogListener() {
            @Override
            public void getDigitsView(DigitsEditText digits) {
                Log.e(TAG, "getDigitsView: ");
                mDigits = digits;
                initListener();
            }

            @Override
            public void itemClick(View view, String number) {
                int id = view.getId();
                if (id == R.id.digits) {
                    if (mDigits.length() != 0) {
                        mDigits.setCursorVisible(true);
                    }
                } else if (id == R.id.deleteButton) {
                    keyPressed(KeyEvent.KEYCODE_DEL);
                } else if (id == R.id.ib_call) {
                    if(!TextUtils.isEmpty(mDigits.getText().toString())) {
                        Toast.makeText(getActivity(), mDigits.getText().toString(), Toast.LENGTH_SHORT).show();
                        Editable digits = mDigits.getText();
//                pjSip.onCall(mDigits.getText().toString());

                        if (digits != null) {
                            Log.e(TAG, "itemClick: " + digits.toString());
                            if(JCChatManager.isConnect()){
                                goVoiceActivity();
                                digits.clear();
                            }else {
                                Toast.makeText(getActivity(), "sip注册失败，请稍后拨打", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), "请输入号码", Toast.LENGTH_SHORT).show();
                    }
                }else if (id == R.id.ib_video) {
                    if(!TextUtils.isEmpty(mDigits.getText().toString())) {
                        Toast.makeText(getActivity(), mDigits.getText().toString(), Toast.LENGTH_SHORT).show();
                        Editable digits = mDigits.getText();
                        if (digits != null) {
                            if(JCChatManager.isConnect()){
                                Log.e(TAG, "itemClick: " + digits.toString());
                                goVideoActivity();
                                digits.clear();
                            }else {
                                Toast.makeText(getActivity(), "sip注册失败，请稍后拨打", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), "请输入号码", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void itemLongClick(View v) {
                int id = v.getId();
                if (id == R.id.digits) {
                    if (mDigits.length() != 0) {
                        mDigits.setCursorVisible(true);
                    }
                } else if (id == R.id.deleteButton) {
                    Editable digits = mDigits.getText();
                    if (digits != null) {
                        digits.clear();
                    }
//                        return true;

                    // Remove tentative input ('0') done by onTouch().
                    removePreviousDigitIfPossible();

                    keyPressed(KeyEvent.KEYCODE_PLUS);

                    // Stop tone immediately
                    stopTone();
                    mPressedDialpadKeys.remove(v);
//                        return true;

                    // Remove tentative input ('*') done by onTouch().
                    removePreviousDigitIfPossible();

                    updateDialString(PAUSE);

                    // Stop tone immediately
                    stopTone();
                    mPressedDialpadKeys.remove(v);
//                        return true;

                    // Remove tentative input ('#') done by onTouch().
                    removePreviousDigitIfPossible();

                    updateDialString(WAIT);

                    // Stop tone immediately
                    stopTone();
                    mPressedDialpadKeys.remove(v);
//                        return true;
                } else if (id == R.id.dv_0) {// Remove tentative input ('0') done by onTouch().
                    removePreviousDigitIfPossible();

                    keyPressed(KeyEvent.KEYCODE_PLUS);

                    // Stop tone immediately
                    stopTone();
                    mPressedDialpadKeys.remove(v);
//                        return true;

                    // Remove tentative input ('*') done by onTouch().
                    removePreviousDigitIfPossible();

                    updateDialString(PAUSE);

                    // Stop tone immediately
                    stopTone();
                    mPressedDialpadKeys.remove(v);
//                        return true;

                    // Remove tentative input ('#') done by onTouch().
                    removePreviousDigitIfPossible();

                    updateDialString(WAIT);

                    // Stop tone immediately
                    stopTone();
                    mPressedDialpadKeys.remove(v);
//                        return true;
                } else if (id == R.id.dv_xing) {// Remove tentative input ('*') done by onTouch().
                    removePreviousDigitIfPossible();

                    updateDialString(PAUSE);

                    // Stop tone immediately
                    stopTone();
                    mPressedDialpadKeys.remove(v);
//                        return true;

                    // Remove tentative input ('#') done by onTouch().
                    removePreviousDigitIfPossible();

                    updateDialString(WAIT);

                    // Stop tone immediately
                    stopTone();
                    mPressedDialpadKeys.remove(v);
//                        return true;
                } else if (id == R.id.dv_jing) {// Remove tentative input ('#') done by onTouch().
                    removePreviousDigitIfPossible();

                    updateDialString(WAIT);

                    // Stop tone immediately
                    stopTone();
                    mPressedDialpadKeys.remove(v);
//                        return true;
                }
            }

            @Override
            public void itemTouch(View v, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        int id = v.getId();
                        if (id == R.id.dv_1) {
                            Log.e(TAG, "itemTouch: 1");
                            keyPressed(KeyEvent.KEYCODE_1);
                        } else if (id == R.id.dv_2) {
                            keyPressed(KeyEvent.KEYCODE_2);
                        } else if (id == R.id.dv_3) {
                            keyPressed(KeyEvent.KEYCODE_3);
                        } else if (id == R.id.dv_4) {
                            keyPressed(KeyEvent.KEYCODE_4);
                        } else if (id == R.id.dv_5) {
                            keyPressed(KeyEvent.KEYCODE_5);
                        } else if (id == R.id.dv_6) {
                            keyPressed(KeyEvent.KEYCODE_6);
                        } else if (id == R.id.dv_7) {
                            keyPressed(KeyEvent.KEYCODE_7);
                        } else if (id == R.id.dv_8) {
                            keyPressed(KeyEvent.KEYCODE_8);
                        } else if (id == R.id.dv_9) {
                            keyPressed(KeyEvent.KEYCODE_9);
                        } else if (id == R.id.dv_0) {
                            keyPressed(KeyEvent.KEYCODE_0);
                        } else if (id == R.id.dv_xing) {
                            keyPressed(KeyEvent.KEYCODE_STAR);
                        } else if (id == R.id.dv_jing) {
                            keyPressed(KeyEvent.KEYCODE_POUND);
                        }
                        mPressedDialpadKeys.add(v);

                    }
                    break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        mPressedDialpadKeys.remove(v);
                        if (mPressedDialpadKeys.isEmpty()) {
                            stopTone();
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }
    //=========================自定义键盘相关 start==============================


    public void goVoiceActivity() {
        if(getActivity()==null){
            return;
        }
        RxPermissions rxPermissions=new RxPermissions(getActivity());
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                ,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO)
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean) {
                            //申请的权限全部允许
                            String number = JNBasePreferenceSaves.getUserSipId();
                            if (TextUtils.isEmpty(number)) {
                                Toast.makeText(getActivity(), "请先配置sip账号！", Toast.LENGTH_SHORT).show();
                            } else {
                                if (number.equals(mDigits.getText().toString())) {
                                    Toast.makeText(getActivity(), "无法和自己通话！", Toast.LENGTH_SHORT).show();
                                } else {
                                    Intent intent = new Intent(getActivity(), JCPjSipP2PActivity.class);
                                    intent.putExtra("tag", "outing");
                                    intent.putExtra("number", mDigits.getText().toString().replace(" ", ""));
//                                    intent.putExtra("mode","phone");
                                    startActivity(intent);
                                }
                            }
                        }
                    }
                });
    }

    public void goVideoActivity() {
        RxPermissions rxPermissions=new RxPermissions(getActivity());
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                ,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO)
                .subscribe(new Consumer<Boolean>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void accept(Boolean aBoolean) {
                        if (aBoolean) {
                            String mySipNumber = JNBasePreferenceSaves.getUserSipId();
                            String friendSipNumber = mDigits.getText().toString().replace(" ", "");
                            if (TextUtils.isEmpty(mySipNumber)) {
                                Toast.makeText(getActivity(), "请先配置sip账号！", Toast.LENGTH_SHORT).show();
                            } else {
                                if (mySipNumber.equals(friendSipNumber)) {
                                    Toast.makeText(getActivity(), "无法和自己通话！", Toast.LENGTH_SHORT).show();
                                } else {
                                    JNLogUtil.e("==============friendSipNumber=="+friendSipNumber);
                                    Intent intent =null;
                                    intent = new Intent(getActivity(), JCPjSipVideoActivity.class);
                                    intent.putExtra("tag", "outing");
                                    intent.putExtra("number", friendSipNumber);
                                    getActivity().startActivity(intent);
                                }
                            }

                        }
                    }
                });
    }

    private void initListener() {
        mDigits.setKeyListener(new DialerKeyListener() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                final String converted = PhoneNumberUtils.convertKeypadLettersToDigits(
                        replaceUnicodeDigits(source.toString()));
                // PhoneNumberUtils.replaceUnicodeDigits performs a character for character replacement,
                // so we can assume that start and end positions should remain unchanged.
                CharSequence result = super.filter(converted, start, end, dest, dstart, dend);
                if (result == null) {
                    if (source.equals(converted)) {
                        // There was no conversion or filtering performed. Just return null according to
                        // the behavior of DialerKeyListener.
                        return null;
                    } else {
                        // filter returns null if the charsequence is to be returned unchanged/unfiltered.
                        // But in this case we do want to return a modified character string (even if
                        // none of the characters in the modified string are filtered). So if
                        // result == null we return the unfiltered but converted numeric string instead.
                        return converted.subSequence(start, end);
                    }
                }
                return result;
            }

            /**
             * Replaces all unicode(e.g. Arabic, Persian) digits with their decimal digit equivalents.
             *
             * @param number the number to perform the replacement on.
             * @return the replaced number.
             */
            public String replaceUnicodeDigits(String number) {
                StringBuilder normalizedDigits = new StringBuilder(number.length());
                for (char c : number.toCharArray()) {
                    int digit = Character.digit(c, 10);
                    if (digit != -1) {
                        normalizedDigits.append(digit);
                    } else {
                        normalizedDigits.append(c);
                    }
                }
                return normalizedDigits.toString();
            }
        });
//        mDigits.setOnLongClickListener(this);
        mDigits.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

//        dv1.setOnTouchListener(this);
//        dv2.setOnTouchListener(this);
//        dv3.setOnTouchListener(this);
//        dv4.setOnTouchListener(this);
//        dv5.setOnTouchListener(this);
//        dv6.setOnTouchListener(this);
//        dv7.setOnTouchListener(this);
//        dv8.setOnTouchListener(this);
//        dv9.setOnTouchListener(this);
//        dvXing.setOnTouchListener(this);
//        dv0.setOnTouchListener(this);
//        dvJing.setOnTouchListener(this);
//        dvXing.setOnLongClickListener(this);
//        dv0.setOnLongClickListener(this);
//        dvJing.setOnLongClickListener(this);
//        deleteButton.setOnLongClickListener(this);
    }

    private void keyPressed(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
                playTone(ToneGenerator.TONE_DTMF_1, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_2:
                playTone(ToneGenerator.TONE_DTMF_2, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_3:
                playTone(ToneGenerator.TONE_DTMF_3, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_4:
                playTone(ToneGenerator.TONE_DTMF_4, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_5:
                playTone(ToneGenerator.TONE_DTMF_5, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_6:
                playTone(ToneGenerator.TONE_DTMF_6, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_7:
                playTone(ToneGenerator.TONE_DTMF_7, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_8:
                playTone(ToneGenerator.TONE_DTMF_8, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_9:
                playTone(ToneGenerator.TONE_DTMF_9, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_0:
                playTone(ToneGenerator.TONE_DTMF_0, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_POUND:
                playTone(ToneGenerator.TONE_DTMF_P, TONE_LENGTH_INFINITE);
                break;
            case KeyEvent.KEYCODE_STAR:
                playTone(ToneGenerator.TONE_DTMF_S, TONE_LENGTH_INFINITE);
                break;
            default:
                break;
        }

        // 振动 mHaptic.vibrate();
        KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
        mDigits.onKeyDown(keyCode, event);

        // 如果光标在文本的最后我们隐藏它。
        final int length = mDigits.length();
        if (length == mDigits.getSelectionStart() && length == mDigits.getSelectionEnd()) {
            mDigits.setCursorVisible(false);
        }
    }

    /**
     * Play the specified tone for the specified milliseconds
     * <p>
     * The tone is played locally, using the audio stream for phone calls.
     * Tones are played only if the "Audible touch tones" user preference
     * is checked, and are NOT played if the device is in silent mode.
     * <p>
     * The tone length can be -1, meaning "keep playing the tone." If the caller does so, it should
     * call stopTone() afterward.
     *
     * @param tone       a tone code from {@link ToneGenerator}
     * @param durationMs tone length.
     */
    private void playTone(int tone, int durationMs) {
        // if local tone playback is disabled, just return.
        if (!mDTMFToneEnabled) {
            return;
        }

        // Also do nothing if the phone is in silent mode.
        // We need to re-check the ringer mode for *every* playTone()
        // call, rather than keeping a local flag that's updated in
        // onResume(), since it's possible to toggle silent mode without
        // leaving the current activity (via the ENDCALL-longpress menu.)
        AudioManager audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        int ringerMode = audioManager.getRingerMode();
        if ((ringerMode == AudioManager.RINGER_MODE_SILENT)
                || (ringerMode == AudioManager.RINGER_MODE_VIBRATE)) {
            return;
        }

        synchronized (mToneGeneratorLock) {
            if (mToneGenerator == null) {
                Log.w(TAG, "playTone: mToneGenerator == null, tone: " + tone);
                return;
            }

            // Start the new tone (will stop any playing tone)
            mToneGenerator.startTone(tone, durationMs);
        }
    }

    /**
     * Stop the tone if it is played.
     */
    private void stopTone() {
        // if local tone playback is disabled, just return.
        if (!mDTMFToneEnabled) {
            return;
        }
        synchronized (mToneGeneratorLock) {
            if (mToneGenerator == null) {
                Log.w(TAG, "stopTone: mToneGenerator == null");
                return;
            }
            mToneGenerator.stopTone();
        }
    }

    /**
     * Remove the digit just before the current position. This can be used if we want to replace
     * the previous digit or cancel previously entered character.
     */
    private void removePreviousDigitIfPossible() {
        final int currentPosition = mDigits.getSelectionStart();
        if (currentPosition > 0) {
            mDigits.setSelection(currentPosition);
            mDigits.getText().delete(currentPosition - 1, currentPosition);
        }
    }

    /**
     * Updates the dial string (mDigits) after inserting a Pause character (,)
     * or Wait character (;).
     */
    private void updateDialString(char newDigit) {
        if (newDigit != WAIT && newDigit != PAUSE) {
            throw new IllegalArgumentException(
                    "Not expected for anything other than PAUSE & WAIT");
        }

        int selectionStart;
        int selectionEnd;

        // SpannableStringBuilder editable_text = new SpannableStringBuilder(mDigits.getText());
        int anchor = mDigits.getSelectionStart();
        int point = mDigits.getSelectionEnd();

        selectionStart = Math.min(anchor, point);
        selectionEnd = Math.max(anchor, point);

        if (selectionStart == -1) {
            selectionStart = selectionEnd = mDigits.length();
        }

        Editable digits = mDigits.getText();

        if (canAddDigit(digits, selectionStart, selectionEnd, newDigit)) {
            digits.replace(selectionStart, selectionEnd, Character.toString(newDigit));

            if (selectionStart != selectionEnd) {
                // Unselect: back to a regular cursor, just pass the character inserted.
                mDigits.setSelection(selectionStart + 1);
            }
        }
    }

    /**
     * Returns true of the newDigit parameter can be added at the current selection
     * point, otherwise returns false.
     * Only prevents input of WAIT and PAUSE digits at an unsupported position.
     * Fails early if start == -1 or start is larger than end.
     */
    @VisibleForTesting
    /* package */ static boolean canAddDigit(CharSequence digits, int start, int end,
                                             char newDigit) {
        if (newDigit != WAIT && newDigit != PAUSE) {
            throw new IllegalArgumentException(
                    "Should not be called for anything other than PAUSE & WAIT");
        }

        // False if no selection, or selection is reversed (end < start)
        if (start == -1 || end < start) {
            return false;
        }

        // unsupported selection-out-of-bounds state
        if (start > digits.length() || end > digits.length()) return false;

        // Special digit cannot be the first digit
        if (start == 0) return false;

        if (newDigit == WAIT) {
            // preceding char is ';' (WAIT)
            if (digits.charAt(start - 1) == WAIT) return false;

            // next char is ';' (WAIT)
            if ((digits.length() > end) && (digits.charAt(end) == WAIT)) return false;
        }

        return true;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewpager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
    //=========================自定义键盘相关 end==============================

    /**
     * 定义FragmentPager适配器类
     */
    class MyAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;//fragment管理员
        private FragmentManager fm;//fragment数组

        /**
         * 构造函数
         *
         * @param fm           fragment管理员对象
         * @param fragmentList fragment数组
         */
        public MyAdapter(FragmentManager fm, List<Fragment> fragmentList) {
            super(fm);
            this.fragments = fragmentList;
            this.fm = fm;
        }


        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }

}

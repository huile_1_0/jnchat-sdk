package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

public class JCGetUserGroupSessionApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/session/getUserGroupSession";
    }

    private String userId;
    private String domainAddr;
    private String domainaddrUser;

    public JCGetUserGroupSessionApi(String userId, String domainAddr, String domainaddrUser) {
        this.userId = userId;
        this.domainAddr = domainAddr;
        this.domainaddrUser = domainaddrUser;
    }
}

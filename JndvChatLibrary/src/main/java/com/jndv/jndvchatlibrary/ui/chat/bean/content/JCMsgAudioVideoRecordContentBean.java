package com.jndv.jndvchatlibrary.ui.chat.bean.content;

public class JCMsgAudioVideoRecordContentBean {
    private String conversionTime;
    private boolean isconnected;

    public String getConversionTime() {
        return conversionTime;
    }

    public void setConversionTime(String conversionTime) {
        this.conversionTime = conversionTime;
    }

    public boolean getIsconnected() {
        return isconnected;
    }

    public void setIsconnected(boolean isconnected) {
        this.isconnected = isconnected;
    }
}

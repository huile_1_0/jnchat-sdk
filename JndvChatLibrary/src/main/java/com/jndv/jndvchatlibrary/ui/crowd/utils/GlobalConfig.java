package com.jndv.jndvchatlibrary.ui.crowd.utils;

import android.content.Context;
import android.os.Environment;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.SparseArray;



import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalConfig {

    public static final String IS_FIRST_USE = "is_first_use";
    public static final String IS_LOGIN = "is_login";

    public static final String KEY_LOGGED_IN = "LoggedIn";

    public static int GLOBAL_DPI = DisplayMetrics.DENSITY_XHIGH;

    public static boolean isLogined = false;

    public static final String DATA_SAVE_FILE_NAME = "v2tech";

    public static final String DEFAULT_CONFIG_FILE = "v2platform.cfg";

    public static final String DEFAULT_CONFIG_LOG_FILE = "log_options.xml";

    public static int GLOBAL_VERSION_CODE = 1;

    public static String GLOBAL_VERSION_NAME = "1.3.0.1";

    public static int MESSAGE_RECV_BINARY_TYPE_TEXT = 0;

    /**
     * Android 4.1 版本代号 Build.VERSION_CODES 不包含该版本号
     */
    public static int JELLY_BEAN = 16;
    /**
     * 当前登录设备的密度的水平
     */
    public static int GLOBAL_DENSITY_LEVEL = DisplayMetrics.DENSITY_XHIGH;

    /**
     * 当前登录设备的密度的值
     */
    public static float GLOBAL_DENSITY_VALUE;

    /**
     * xxxhdip的数值
     */
    public static int DENSITY_XXXHIGH = 640;

    /**
     * xxhdip的数值
     */
    public static int DENSITY_XXHIGH = 480;
    /**
     * 屏幕英寸数
     */
    public static double SCREEN_INCHES = 0;

    /**
     * 屏幕宽度
     */
    public static int SCREEN_WIDTH = 0;

    /**
     * 屏幕高度
     */
    public static int SCREEN_HEIGHT = 0;

    /**
     * Bitmap所支持的最大值
     */
    public static int BITMAP_MAX_SIZE = 4096;

    /**
     * 线程池最大线程数
     */
    public static int THREAD_POOL_MAX_SIZE = 10;

    /**
     * 表明当前设备是否是Pad
     */
    public static boolean PROGRAM_IS_PAD = false;

    /**
     * 用来记录聊天界面是否已经被打开
     */
    public static boolean CHAT_INTERFACE_OPEN = false;

    /**
     * 登陆的时候获取的服务器时间
     */
    public static long LONGIN_SERVER_TIME = 0;

    /**
     * 登陆的时候记录的本地时间
     */
    public static long LONGIN_LOCAL_TIME = 0;
    /**
     * 默认的手机存储路径
     */
    public static String DEFAULT_GLOBLE_PATH = Environment.getDataDirectory().getAbsolutePath();

    /**
     * 默认的sd卡存储路径
     */
    public static String SDCARD_GLOBLE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();

    /**
     * 登陆用户的ID
     */
//    public static String LOGIN_USER_ID = SpUtil.getLong(JndvApplication.getInstance().getBaseContext(), ChatEntity.CURRENT_USER_ID, 0) + "";
    public static String LOGIN_USER_ID = "0";

    /**
     * 传输文件的同时最大限制
     */
    public static final int MAX_TRANS_FILE_SIZE = 5;

   /* *//**
     * 点对点视频，本地采集的宽度
     *//*
    public static int P2P_LOCAL_VIDEO_WIDTH = 176;

    *//**
     * 点对点视频，本地采集的高度
     *//*
    public static int P2P_LOCAL_VIDEO_HEIGHT = 144;*/

    public static  int MAIN_FRAGMENT_FLAG=2;//主页面标识符,1:老版本，2：新版本

    /**
     * Get from Server , For distinguish different server
     */
    public static String SERVER_DATABASE_ID = "";//"ffd54725f9d8438740be872a21a40972";

    public static HashMap<String, String> allChinese = new HashMap<String, String>();

    public static Map<Long, Integer> mTransingFiles = new HashMap<Long, Integer>();

    // public static Map<Long, Integer> mDownLoadingFiles = new HashMap<Long,
    // Integer>();

    public static List<Long> NeedToUpdateUser = new ArrayList<Long>();



    private static SparseArray<EmojiWraper> EMOJI_ARRAY = new SparseArray<>();



    public static String getEmojiStr(int id) {
        EmojiWraper wrapper = EMOJI_ARRAY.get(id);
        if (wrapper != null) {
            return wrapper.emojiStr;
        }
        return null;
    }

    public static String getGlobalRootPath() {
        boolean sdExist = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
        if (!sdExist) {// 如果不存在,
            // --data/data/com.pview
            return DEFAULT_GLOBLE_PATH + File.separator + DATA_SAVE_FILE_NAME;
        } else {
            // --mnt/sdcard
            return SDCARD_GLOBLE_PATH + File.separator + DATA_SAVE_FILE_NAME;
        }
    }

    public static String getGlobalPath() {
        boolean sdExist = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
        if (!sdExist) {// 如果不存在,
            // --data/data/com.pview
            return DEFAULT_GLOBLE_PATH + File.separator + DATA_SAVE_FILE_NAME + File.separator + SERVER_DATABASE_ID;
        } else {
            // --mnt/sdcard
            return SDCARD_GLOBLE_PATH + File.separator + DATA_SAVE_FILE_NAME + File.separator + SERVER_DATABASE_ID;
        }
    }

    public static String getGlobalUserAvatarPath() {
        return getGlobalPath() + File.separator + "Users" + File.separator + LOGIN_USER_ID + File.separator + "avatars";
    }

    public static String getGlobalPicsPath() {
        return getGlobalPath() + File.separator + "Users" + File.separator + LOGIN_USER_ID + File.separator + "Images";
    }

    public static String getGlobalAudioPath() {
        return getGlobalPath() + File.separator + "Users" + File.separator + LOGIN_USER_ID + File.separator + "audios";
    }

    public static String getGlobalFilePath() {
        return getGlobalPath() + File.separator + "Users" + File.separator + LOGIN_USER_ID + File.separator + "files";
    }

    public static String getGlobalDataBasePath() {
        LogUtil.e("databasepath:" + getGlobalPath() + File.separator + "Users" + File.separator + LOGIN_USER_ID);
        return getGlobalPath() + File.separator + "Users" + File.separator + LOGIN_USER_ID;
    }

    public static long getGlobalServerTime() {
        return (SystemClock.elapsedRealtime() - LONGIN_LOCAL_TIME
                + LONGIN_SERVER_TIME * 1000);
    }

    public static void recordLoginTime(long serverTime) {
        LONGIN_LOCAL_TIME = SystemClock.elapsedRealtime();
        LONGIN_SERVER_TIME = serverTime;
    }

    public static int getLruCacheMaxSize() {
        int maxMemory = (int) Runtime.getRuntime().maxMemory();
        return maxMemory / 8;
    }

    static class EmojiWraper {
        String emojiStr;
        int id;

        public EmojiWraper(String emojiStr, int id) {
            super();
            this.emojiStr = emojiStr;
            this.id = id;
        }

    }

    public static class Resource {
        public static String CONTACT_DEFAULT_GROUP_NAME = "";

        public static String MIX_VIDEO_DEFAULT_NAME = "";
    }
}

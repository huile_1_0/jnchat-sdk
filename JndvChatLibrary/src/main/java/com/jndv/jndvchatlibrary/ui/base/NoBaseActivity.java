package com.jndv.jndvchatlibrary.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipP2PActivity;
import com.jndv.jndvchatlibrary.chatSIP.JCPjSipVideoActivity;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.wega.library.loadingDialog.LoadingDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Author: wangguodong
 * Date: 2022/7/6
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class NoBaseActivity extends AppCompatActivity {

    private LoadingDialog loadingDialog = null;
    /**
     * 显示加载框
     */
    public void showLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.loading();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==showLoadding==",e);
        }
    }

    /**
     * 直接取消加载框
     */
    public void cancelLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.cancel();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==cancelLoadding==",e);
        }
    }

    /**
     * 显示加载成功后取消加载框
     */
    public void cancelSuccessLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.loadSuccess();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==cancelSuccessLoadding==",e);
        }
    }

    /**
     * 显示加载失败后取消加载框
     */
    public void cancelFailLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.loadFail();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==cancelFailLoadding==",e);
        }
    }


    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        loadingDialog = new LoadingDialog(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void codeEvent(JNCodeEvent codeEvent) {
        switch (codeEvent.getCode()) {
            case JNEventBusType
                    .CODE_SIP_LOGIN_OUT://异地登录
                finish();
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
//            if (this instanceof JCPjSipP2PActivity) {
////                JNBaseUtilManager.setIsCalling(false);
//                ((JCPjSipP2PActivity) this).getPjSip().stopRingtoneCommand();
//            } else if (this instanceof JCPjSipVideoActivity) {
////                JNBaseUtilManager.setIsCalling(false);
//                ((JCPjSipVideoActivity) this).getPjSip().stopRingtoneCommand();
//            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        try {
            if(this instanceof JCPjSipP2PActivity){
                JNBaseUtilManager.setIsCalling(false);
                JNLogUtil.e("=stopRingtoneCommand==NoBaseActivity==onDestroy==JCPjSipP2PActivity==");
                ((JCPjSipP2PActivity)this).getPjSip().stopRingtoneCommand();
            }else if(this instanceof JCPjSipVideoActivity){
                JNBaseUtilManager.setIsCalling(false);
                JNLogUtil.e("=stopRingtoneCommand==NoBaseActivity==onDestroy==JCPjSipP2PActivity==");
//                ((JCPjSipVideoActivity)this).getPjSip().stopRingtoneCommand();
            }
            EventBus.getDefault().unregister(this);
        } catch (Exception e) {
            JNLogUtil.e("==JCBaseActivity==onDestroy==", e);
        }

        super.onDestroy();
    }
}

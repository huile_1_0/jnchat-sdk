package com.jndv.jndvchatlibrary.ui.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ChatContant;
import com.jndv.jndvchatlibrary.adapter.TTSOrganzationPersonAdapter;
import com.jndv.jndvchatlibrary.bean.OrganizationPersonBean;
import com.jndv.jndvchatlibrary.http.api.JCGetOrganizationApi;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCconstants;
import com.jndv.jndvchatlibrary.ui.location.adapter.DefaultItemDecoration;
import com.jndv.jndvchatlibrary.utils.ActivityStack;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

public class TTSOrganizationPersonActivity extends JCbaseFragmentActivity {

    List<OrganizationPersonBean> opbeanList=new ArrayList<>();
    TTSOrganzationPersonAdapter adapter;
    OrganizationPersonBean selectOpbean;
    TextView tvNum;

    public static void start(Context context, OrganizationPersonBean data){
        Intent intent = new Intent(context, TTSOrganizationPersonActivity.class);
        intent.putExtra("data", data);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ttsselect_person);
        ActivityStack.getInstance().addActivity(this);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvNum.setText(ChatContant.TTS_PERSON_LIST.size()+"");
    }

    private void initView() {
        selectOpbean= (OrganizationPersonBean) getIntent().getSerializableExtra("data");
        tvNum=findViewById(R.id.tv_num);
        RecyclerView rv=findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter=new TTSOrganzationPersonAdapter(opbeanList);
        rv.addItemDecoration(new DefaultItemDecoration(this,R.dimen.dimen_1,R.color.gray));
        rv.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if(TextUtils.isEmpty(opbeanList.get(position).deptname))
                    return;
                TTSOrganizationPersonActivity.start(TTSOrganizationPersonActivity.this, opbeanList.get(position));
            }
        });
        adapter.setOnSelectCountListener(count -> {
            tvNum.setText(ChatContant.TTS_PERSON_LIST.size()+"");
        });

        TextView tvNext=findViewById(R.id.tv_next);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TTSOrganizationPersonActivity.this,EditTTSContentActivity.class);
                startActivity(intent);
            }
        });
        if(selectOpbean==null){
            setCenterTitle("组织机构");
//            getData(JNBasePreferenceSaves.getOrganzationId());
        }else {
            setCenterTitle(selectOpbean.deptname);
            getData(selectOpbean.id+"");
        }
    }

    private void getData(String parentId) {
        String url= JCconstants.HOST_URL+"/department/getDeptInfo";
        OkHttpUtils
                .get()
                .url(url)
                .addParams("parentid",parentId)
                .build()
                .execute(new StringCallback()
                {
                    @Override
                    public void onResponse(String response, int id) {
                        opbeanList.clear();
                        JCGetOrganizationApi.Bean bean=new Gson().fromJson(response,JCGetOrganizationApi.Bean.class);
                        if(!TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, bean.getCode())){
                            JCChatManager.showToast("数据异常");
                            return;
                        }
                        if(bean.getData()==null)
                            return;
                        List<JCGetOrganizationApi.Bean.DepartBean> departList = bean.getData().getDeparts();
                        if(departList!=null && departList.size()>0){
                            for(JCGetOrganizationApi.Bean.DepartBean departBean:departList){
                                OrganizationPersonBean opbean=new OrganizationPersonBean();
                                opbean.deptname=departBean.getDeptname();
                                opbean.id=departBean.getId();
                                opbean.itemType=1;
                                opbeanList.add(opbean);
                            }
                        }
                        List<JCGetOrganizationApi.Bean.UserBean> userList = bean.getData().getUsers();
                        if(userList!=null && userList.size()>0){
                            for(JCGetOrganizationApi.Bean.UserBean userBean:userList){
                                OrganizationPersonBean opbean=new OrganizationPersonBean();
                                opbean.nickName=userBean.getNickName();
                                opbean.IM_Number=userBean.getIM_Number();
                                opbean.mobile=userBean.getMobile();
                                opbean.itemType=2;
                                opbean.id=userBean.getId();
                                opbeanList.add(opbean);
                            }
                        }
                        adapter.setNewData(opbeanList);
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        JCChatManager.showToast("数据异常");
                    }
                });
    }
}
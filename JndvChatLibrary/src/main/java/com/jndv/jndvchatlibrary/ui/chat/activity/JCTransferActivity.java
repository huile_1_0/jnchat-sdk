package com.jndv.jndvchatlibrary.ui.chat.activity;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.ehome.sipservice.SipServiceCommand;
import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.IntentConstant;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.http.api.JNGetDeptSons;
import com.jndv.jnbaseutils.ui.JNOrganizationSelectFragment;
import com.jndv.jnbaseutils.ui.base.JNBaseFragment;
import com.jndv.jnbaseutils.ui.base.JNBaseFragmentActivity;
import com.jndv.jnbaseutils.ui.bean.Node;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.ChatContant;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.adapter.JCTransferAdapter;
import com.jndv.jndvchatlibrary.bean.OrganizationPersonBean;
import com.jndv.jndvchatlibrary.bean.SessionDataBean;
import com.jndv.jndvchatlibrary.bean.SessionResultBean;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCImagesUpApi;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgImgContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCconstants;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.ui.location.adapter.DefaultItemDecoration;
import com.jndv.jndvchatlibrary.utils.ActivityStack;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import okhttp3.Call;

public class JCTransferActivity extends JCbaseFragmentActivity {
    String TAG="JCTransferActivity";
    List<OrganizationPersonBean> personList=new ArrayList<>();
    JCTransferAdapter adapter;
    private JCimMessageBean messageBean ;
    TextView tvNum;
    //用来区分跳转页面的来源，非空代表从webview页面跳转过来
    String contentText;
    String zoningCode;
    String roleCode;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);
        ChatContant.TRANSFER_PERSON_LIST.clear();
        ActivityStack.getInstance().addActivity(this);
        setCenterTitle("转发");
        initView();
        getData();
    }

    private void initView() {
        String uri=getIntent().getStringExtra("uri");
        if(!TextUtils.isEmpty(uri)){
            imageUri=Uri.parse(uri);
        }else {
            imageUri = getIntent().getParcelableExtra("imageUri");
        }
        zoningCode=getIntent().getStringExtra(IntentConstant.ZONING_CODE);
        roleCode=getIntent().getStringExtra(IntentConstant.ROLE_CODE);
        contentText=getIntent().getStringExtra("contentText");
        messageBean= (JCimMessageBean) getIntent().getSerializableExtra("content");
        Log.d(TAG,"addr=="+JNBasePreferenceSaves.getSipAddress());
        tvNum=findViewById(R.id.tv_num);
        RecyclerView rv=findViewById(R.id.rv_user);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DefaultItemDecoration(this,R.dimen.dimen_1,R.color.gray));
        adapter=new JCTransferAdapter(R.layout.item_list_person,personList);
        rv.setAdapter(adapter);
        adapter.setOnSelectCountListener(new JCTransferAdapter.OnSelectCountListener() {
            @Override
            public void onSelectCount(int count) {
                tvNum.setText(ChatContant.TRANSFER_PERSON_LIST.size()+"");
            }
        });
        findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ChatContant.TRANSFER_PERSON_LIST.size()==0){
                    JCThreadManager.showMainToast("请选择要发送的对象！");
                    return;
                }
                for(OrganizationPersonBean personBean: ChatContant.TRANSFER_PERSON_LIST){
                    transmit(personBean);
                }
            }
        });

        findViewById(R.id.ll_organization).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(JCTransferActivity.this,JCTransferSelectPersonActivity.class);
//                intent.putExtra("content",messageBean);
//                intent.putExtra("contentText",contentText);
//                if(!TextUtils.isEmpty(uri)){
//                    intent.putExtra("uri",imageUri.toString());
//                }
//                startActivity(intent);

                Bundle bundle = new Bundle();
                bundle.putString("zoningCode", JNBasePreferenceSaves.getZoningCode());
                bundle.putInt("type", 2);
                if (null!=ChatContant.TRANSFER_PERSON_LIST&&ChatContant.TRANSFER_PERSON_LIST.size()>0){
                    for (int i = 0; i < ChatContant.TRANSFER_PERSON_LIST.size(); i++) {
                        OrganizationPersonBean organizationPersonBean = ChatContant.TRANSFER_PERSON_LIST.get(i);

                        JNGetDeptSons.DataBean userBean = new JNGetDeptSons.DataBean();
                        userBean.setImNum(organizationPersonBean.IM_Number);
                        Node node = new Node();
                        node.setType(TextUtils.equals(JCSessionType.CHAT_PERSION, organizationPersonBean.type)?0:2);
                        node.setLevel(0);
                        node.setContent(new Gson().toJson(userBean));
                        node.setSelecte(true);
                        JNBaseConstans.OrganizationSelecte.add(node);
                    }
                }

                JNOrganizationSelectFragment jnOrganizationSelectFragment = new JNOrganizationSelectFragment();
                JNBaseFragmentActivity.start(JCTransferActivity.this, jnOrganizationSelectFragment, "组织机构", bundle, new JNBaseFragment.FragmentSelect() {
                    @Override
                    public void onSelecte(int type, Object... objects) {
                        try {
                            List<Node> listSelect = (List<Node>) objects[0];
                            for (int i = 0; i < listSelect.size(); i++) {
                                Node node = listSelect.get(i);
                                JNGetDeptSons.DataBean userBean = new Gson().fromJson(node.getContent(), JNGetDeptSons.DataBean.class);
                                boolean have = false;
                                for (int j = 0; j < ChatContant.TRANSFER_PERSON_LIST.size(); j++) {
                                    OrganizationPersonBean organizationPersonBean = ChatContant.TRANSFER_PERSON_LIST.get(j);
                                    if (TextUtils.equals(node.getType() == 2?JCSessionType.CHAT_GROUP:JCSessionType.CHAT_PERSION, organizationPersonBean.type)
                                            &&TextUtils.equals(userBean.getImNum(), userBean.getImNum())){
                                        have = true;
                                    }
                                }
                                if (!have){
                                    OrganizationPersonBean organizationPersonBean = new OrganizationPersonBean();
                                    organizationPersonBean.IM_Number = userBean.getImNum();
                                    organizationPersonBean.otherId = userBean.getImNum();
                                    organizationPersonBean.type = JCSessionType.CHAT_PERSION;
                                    ChatContant.TRANSFER_PERSON_LIST.add(organizationPersonBean);
                                }
                            }
                            tvNum.setText(ChatContant.TRANSFER_PERSON_LIST.size()+"");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }, true);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvNum.setText(ChatContant.TRANSFER_PERSON_LIST.size()+"");
    }

    private void transmit(OrganizationPersonBean bean) {
        if(TextUtils.isEmpty(contentText)){
            JCimMessageBean msg = JCchatFactory.creatIMMessage(messageBean.getContent(),
                    messageBean.getMsgType(), bean.otherId, JNBasePreferenceSaves.getUserSipId()
                    , JNBasePreferenceSaves.getSipAddress(), bean.otherId, JNBasePreferenceSaves.getSipAddress()
                    , bean.type, JNBasePreferenceSaves.getJavaAddress(),JNBasePreferenceSaves.getJavaAddress());
            if(bean.type.equals("1")){
                sendMsg(msg,bean);
            }else {
                if (JCSessionUtil.isGroupStateOk(bean.sessionId)){
                    sendMsg(msg,bean);
                }
            }
        }else {
            Log.e(TAG, "transmit: ===01===");
            sendImgText(bean);
        }

        ActivityStack.getInstance().finishAllActivity();
    }

    /**
     *发送图片文本
     * @param
     */
    private void sendImgText(OrganizationPersonBean bean) {
        JCimMessageBean msg = JCchatFactory.creatIMMessage(contentText,
                JCmessageType.IMG, bean.otherId, JNBasePreferenceSaves.getUserSipId()
                , JNBasePreferenceSaves.getSipAddress(), bean.otherId, JNBasePreferenceSaves.getSipAddress()
                , bean.type, JNBasePreferenceSaves.getJavaAddress(),JNBasePreferenceSaves.getJavaAddress());

        Log.e(TAG, "transmit: ===02===");
        JCFilesUpUtils.upImage(imageUri, "jpg", new OnHttpListener<JCImagesUpApi.Bean>() {
            @Override
            public void onSucceed(JCImagesUpApi.Bean result) {
                try {
                    Log.e(TAG, "transmit: ===03===");
//                                                        Box<JCimMessageBean > box = JCobjectBox.get().boxFor(JCimMessageBean.class);
                    if (TextUtils.equals("success", result.getMsg())){
                        Log.e(TAG, "transmit: ===04==="+result.getUrlArr().get(0).getTailUrl());
                        JCMsgImgContentBean msgEntity = new Gson().fromJson(new String(Base64.decode(msg.getContent(),Base64.NO_WRAP)), JCMsgImgContentBean.class);
                        msgEntity.setUrl(JNBaseUtilManager.getFilesUrl(result.getUrlArr().get(0).getTailUrl()));
//                        msgEntity.setUrl(JCChatManager.getFilesServerUrl()+result.getUrlArr().get(0).getUrl());
//                                                            msgEntity.setUri(null);
                        String msgEntityText = new Gson().toJson(msgEntity);
                        msg.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
                        JCThreadManager.onMainHandler(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("zhang","发送图片消息");
                                Log.e(TAG, "transmit: ===04===");
                                messageBean = msg;
                                contentText = "";
                                sendMsg(msg,bean);
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(Exception e) {

            }
        });
    }

    private void sendMsg(JCimMessageBean msg,OrganizationPersonBean bean){
        String userID = JNSpUtils.getString(this, "myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
        Log.e("sendMessage",new Gson().toJson(msg));
        Box<JCimMessageBean> beanBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
        beanBox.put(msg);
        Log.d("SipService","msg==trans=activity=");
        Log.e(TAG, "transmit: ===05===");
        EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_MESSAGE_TRANSMIT, "", msg));
        SipServiceCommand.sendMessageToBuddy(this, new Gson().toJson(msg), userID
                , "sip:" + bean.otherId + "@" + JNBasePreferenceSaves.getSipAddress()
                , JCSessionType.getTypeSIP(bean.type), false);
    }

    private void getData(){
        String url= JCconstants.HOST_URL+"/session/getSessionList";
        OkHttpUtils
                .get()
                .url(url)
                .addParams("pageNo","1")
                .addParams("pageSize","200")
                .addParams("domainAddr", JNBasePreferenceSaves.getJavaAddress())
                .addParams("userId",JNBasePreferenceSaves.getUserSipId())
                .build()
                .execute(new StringCallback()
                {
                    @Override
                    public void onResponse(String response, int id) {
                        SessionResultBean bean=new Gson().fromJson(response, SessionResultBean.class);
                        if(!TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, bean.getCode())){
                            JCChatManager.showToast("数据异常");
                            return;
                        }
                        if(bean.getData()==null)
                            return;
                        for(SessionDataBean sessionBean:bean.getData()){
                            //单聊和群聊
                            if(sessionBean.getType().equals("1") || sessionBean.getType().equals("2")){
                                OrganizationPersonBean personBean=new OrganizationPersonBean();
                                personBean.IM_Number=sessionBean.getOther_id();
                                personBean.otherId=sessionBean.getOther_id();
                                personBean.otherName=sessionBean.getOther_name();
                                personBean.type=sessionBean.getType();
                                personBean.sessionId=sessionBean.getSession_id();
                                personList.add(personBean);
                            }
                        }
                        adapter.setNewData(personList);
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        JCChatManager.showToast("数据异常");
                    }
                });
    }
}

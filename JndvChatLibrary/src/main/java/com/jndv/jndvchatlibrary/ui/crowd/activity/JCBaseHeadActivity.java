package com.jndv.jndvchatlibrary.ui.crowd.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.RequiresApi;


import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.zhy.autolayout.AutoLinearLayout;
import com.zhy.autolayout.AutoRelativeLayout;

import java.util.Objects;

/**
 * 简单返回的头部
 */

public abstract class JCBaseHeadActivity extends JCBaseActivity {
    protected TextView titleTv;
    protected TextView titleRight;
    protected ImageView moreRight;
    protected AutoRelativeLayout top_chat_root;
    protected ImageView titleBack;
    protected AutoLinearLayout back;
    protected Context mContext;
    private boolean isShowTitle = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_basehead_layout);
        mContext=this;
        Objects.requireNonNull(getSupportActionBar()).hide();
        initSystemBar();
        initBaseView();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View.inflate(this, layoutResID, (ViewGroup) findViewById(R.id.base_content));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void initTitle() {
        top_chat_root.setBackgroundColor(getResources().getColor(JCChatManager.getTitleColor()));
        titleTv.setTextColor(getResources().getColor(JCChatManager.getTitleTVColor()));
        titleRight.setTextColor(getResources().getColor(JCChatManager.getTitleTVColor()));
        titleBack.setBackground(getResources().getDrawable(JCChatManager.getTitleIVBack()));
        moreRight.setBackground(getResources().getDrawable(JCChatManager.getTitleIVMenu()));
    }

    /**
     * 调用后 才能得到titleTv否则为空
     */
    private void initBaseView() {
        try {
            top_chat_root = (AutoRelativeLayout) findViewById(R.id.top_chat_root);
            titleTv = (TextView) findViewById(R.id.title_content);
            back = (AutoLinearLayout) findViewById(R.id.back);
            titleBack = (ImageView) findViewById(R.id.iv_back);
            titleRight = (TextView) findViewById(R.id.right);
            moreRight = (ImageView) findViewById(R.id.menu);
            titleRight.setEnabled(true);
            BaseTitleClick baseTitleClick = new BaseTitleClick();
            back.setOnClickListener(baseTitleClick);
            titleRight.setOnClickListener(baseTitleClick);
            titleTv.setOnClickListener(baseTitleClick);
            if (isShowTitle()){
                top_chat_root.setVisibility(View.VISIBLE);
                initTitle();
            }else {
                top_chat_root.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public boolean isShowTitle() {
        return isShowTitle;
    }

    public void setShowTitle(boolean showTitle) {
        isShowTitle = showTitle;
    }
    /**
     * 改变系统标题栏颜色
     */
    public void initSystemBar() {
        if (JCChatManager.getTitleState() == JNBaseConstans.TITLE_STATE_LIGHT){
            setStatusBar();
        }
    }
    protected void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));//设置状态栏颜色
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏图标和文字颜色为暗色
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏图标和文字颜色为暗色
        }
    }
    /**
     * 设置全屏
     * before adding content
     */
    protected void viewFullScreen() {
        // 隐藏标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
// 隐藏状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void receiveBroadcast(Intent intent) {

    }

    @Override
    public void receiveMessage(Message msg) {

    }

    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 设置中间标题
     *
     * @param titleText
     */
    public void setCenterTitle(String titleText) {
        if (titleText != null) {
            if (titleTv != null) {
                titleTv.setText(titleText);
            }
        }
    }

    /**
     * @param text
     * @param drawableRes 设置右侧的按钮，可显示文字或图片
     */
    public void setTitleRight(String text, String drawableRes) {
        if (titleRight == null||moreRight==null) {
            return;
        }

        if (text!=null){
            titleRight.setVisibility(View.VISIBLE);
            moreRight.setVisibility(View.GONE);
            titleRight.setText(text);
        }else if (drawableRes!=null){
            titleRight.setVisibility(View.GONE);
            moreRight.setVisibility(View.VISIBLE);
        }else {
            titleRight.setVisibility(View.GONE);
            moreRight.setVisibility(View.GONE);
        }
    }

    /**
     * 标题按钮的点击事件
     */
    private class BaseTitleClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.back) {
                onBackClick();
            } else if (id == R.id.right) {
                onRightClick();
            }else if (id ==R.id.title_content){
                onTitleClick();
            }
        }
    }

    /**
     * 标题Title，
     */
    protected void onTitleClick() {
    }

    /**
     * 标题中右边的部分，
     */
    protected void onRightClick() {
    }

    /**
     * 返回按钮的点击事件
     */
    public void onBackClick() {
        back();
    }


}
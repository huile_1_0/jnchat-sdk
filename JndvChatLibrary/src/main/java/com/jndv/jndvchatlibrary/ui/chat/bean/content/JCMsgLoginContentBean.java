package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
 * Author: wangguodong
 * Date: 2023/1/16
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCMsgLoginContentBean {
    private String account ;

    public JCMsgLoginContentBean(String account) {
        this.account = account;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}

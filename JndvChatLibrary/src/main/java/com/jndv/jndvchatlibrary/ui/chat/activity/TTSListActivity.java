package com.jndv.jndvchatlibrary.ui.chat.activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.adapter.TTSMsgAdapter;
import com.jndv.jndvchatlibrary.bean.TTSMsgBean;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCconstants;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

public class TTSListActivity extends JCbaseFragmentActivity implements View.OnClickListener {

    TextView tvAll;
    View viewAll;
    TextView tvMineSend;
    View viewMineSend;
    TextView tvFinish;
    View viewFinish;
    TextView tvUnfinish;
    View viewUnfinish;

    TTSMsgAdapter adapter;
    List<TTSMsgBean.DataBean.TTSDataBean> msgList=new ArrayList<>();
    int pageNo=1;
    SmartRefreshLayout refreshLayout;
    String ttsUrl;
    int tabIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ttslist);
        setCenterTitle("TTS消息");
        initView();
        changeTab(0);
    }

    private void initView() {
        RelativeLayout rlAll=findViewById(R.id.rl_all);
        tvAll=findViewById(R.id.tv_all);
        viewAll=findViewById(R.id.view_all);

        RelativeLayout rlMineSend=findViewById(R.id.rl_mine_send);
        tvMineSend=findViewById(R.id.tv_mine_send);
        viewMineSend=findViewById(R.id.view_mine_send);

        RelativeLayout rlFinish=findViewById(R.id.rl_finish);
        tvFinish=findViewById(R.id.tv_finish);
        viewFinish=findViewById(R.id.view_finish);

        RelativeLayout rlUnfinish=findViewById(R.id.rl_unfinish);
        tvUnfinish=findViewById(R.id.tv_unfinish);
        viewUnfinish=findViewById(R.id.view_unfinish);

        rlAll.setOnClickListener(this);
        rlMineSend.setOnClickListener(this);
        rlFinish.setOnClickListener(this);
        rlUnfinish.setOnClickListener(this);

        refreshLayout=findViewById(R.id.refreshLayout);
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                pageNo++;
                getData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {

            }
        });

        adapter=new TTSMsgAdapter(R.layout.item_tts_info,msgList);
        RecyclerView rv=findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent=new Intent(TTSListActivity.this, TTSDetailActivity.class);
                intent.putExtra("data",msgList.get(position));
                if(tabIndex==1 || tabIndex==2){
                    intent.putExtra("is_result",1);
                }else if(tabIndex==0){
                    intent.putExtra("is_result",msgList.get(position).getIs_result());
                }else {
                    intent.putExtra("is_result",0);
                }
                startActivity(intent);
            }
        });
    }

    private void changeTab(int index){
        msgList.clear();
        adapter.notifyDataSetChanged();
        tabIndex=index;
        switch (index){
            case 0:
                tvAll.setTextColor(getResources().getColor(R.color.text_blue));
                viewAll.setVisibility(View.VISIBLE);
                tvMineSend.setTextColor(getResources().getColor(R.color.text_gray2));
                viewMineSend.setVisibility(View.GONE);
                tvFinish.setTextColor(getResources().getColor(R.color.text_gray2));
                viewFinish.setVisibility(View.GONE);
                tvUnfinish.setTextColor(getResources().getColor(R.color.text_gray2));
                viewUnfinish.setVisibility(View.GONE);
                ttsUrl="/tts-voice-msg/getTtsMsgList";
                getData();
                break;
            case 1:
                tvMineSend.setTextColor(getResources().getColor(R.color.text_blue));
                viewMineSend.setVisibility(View.VISIBLE);
                tvAll.setTextColor(getResources().getColor(R.color.text_gray2));
                viewAll.setVisibility(View.GONE);
                tvFinish.setTextColor(getResources().getColor(R.color.text_gray2));
                viewFinish.setVisibility(View.GONE);
                tvUnfinish.setTextColor(getResources().getColor(R.color.text_gray2));
                viewUnfinish.setVisibility(View.GONE);
                ttsUrl="/tts-voice-msg/getTtsMsgSendList";
                getData();
                break;
            case 2:
                tvFinish.setTextColor(getResources().getColor(R.color.text_blue));
                viewFinish.setVisibility(View.VISIBLE);
                tvAll.setTextColor(getResources().getColor(R.color.text_gray2));
                viewAll.setVisibility(View.GONE);
                tvMineSend.setTextColor(getResources().getColor(R.color.text_gray2));
                viewMineSend.setVisibility(View.GONE);
                tvUnfinish.setTextColor(getResources().getColor(R.color.text_gray2));
                viewUnfinish.setVisibility(View.GONE);
                ttsUrl="/tts-voice-record/page";
                getData();
                break;
            case 3:
                tvUnfinish.setTextColor(getResources().getColor(R.color.text_blue));
                viewUnfinish.setVisibility(View.VISIBLE);
                tvAll.setTextColor(getResources().getColor(R.color.text_gray2));
                viewAll.setVisibility(View.GONE);
                tvMineSend.setTextColor(getResources().getColor(R.color.text_gray2));
                viewMineSend.setVisibility(View.GONE);
                tvFinish.setTextColor(getResources().getColor(R.color.text_gray2));
                viewFinish.setVisibility(View.GONE);
                ttsUrl="/tts-voice-queue/page";
                getData();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.rl_all){
            changeTab(0);
        }else if(v.getId()==R.id.rl_mine_send){
            changeTab(1);
        }else if(v.getId()==R.id.rl_finish){
            changeTab(2);
        }else if(v.getId()==R.id.rl_unfinish){
            changeTab(3);
        }
    }

    public void getData() {
        String url= JCconstants.HOST_URL+ttsUrl;
        OkHttpUtils
                .get()
                .url(url)
                .addParams("userId", JNBasePreferenceSaves.getUserSipId())
                .addParams("pageNo", pageNo+"")
                .addParams("pageSize", "10")
                .build()
                .execute(new StringCallback()
                {
                    @Override
                    public void onResponse(String response, int id) {
                        onNetworkResponse(response);
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }
                });
    }

    public void onNetworkResponse(String response) {
        TTSMsgBean ttsMsgBean=new Gson().fromJson(response,TTSMsgBean.class);
        if(!TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, ttsMsgBean.getCode()))
            return;
        if(ttsMsgBean.getData()==null)
            return;
        if(ttsMsgBean.getData().getList()==null || ttsMsgBean.getData().getList().size()==0){
            refreshLayout.finishLoadMore();
            Toast.makeText(this, "没有更多数据啦!", Toast.LENGTH_SHORT).show();
            return;
        }
        refreshLayout.finishLoadMore();
        msgList.addAll(ttsMsgBean.getData().getList());
        adapter.setNewData(msgList);
    }
}
package com.jndv.jndvchatlibrary.ui.crowd.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    /**
     * get specific format String date <br>
     * <ul>
     * today HH:mm:ss (时:分:秒)<br>
     * yesterday : yesterday (昨天) <br>
     * more before : yyyy-MM-dd HH:mm:ss <br>
     * </ul>
     *
     * @param longDate
     * @return
     */
    public static String getStringDate(long longDate) {
        SimpleDateFormat format = null;

        Date dates = new Date(longDate);
        Calendar cale = Calendar.getInstance();
        cale.setTime(dates);

        Date nowDates = new Date(System.currentTimeMillis());
        Calendar currentCale = Calendar.getInstance();
        currentCale.setTime(nowDates);

        int days = cale.get(Calendar.DAY_OF_MONTH);
        int currentCaleDays = currentCale.get(Calendar.DAY_OF_MONTH);

        int loginMonth = cale.get(Calendar.MONTH);
        int currentMonth = currentCale.get(Calendar.MONTH);

        if (currentCaleDays - days == 0)
            return getDateFormat(longDate, "HH:mm");

        if (currentCaleDays - 1 == days) {
            // need get context
            return "昨天" + getDateFormat(longDate, "HH:mm");
        }

        if (currentMonth == loginMonth) {
            if (currentCaleDays - days == 0) {
                return getDateFormat(longDate, "HH:mm");
            }

            if (currentCaleDays - 1 == days) {
                // need get context
                return "昨天" + getDateFormat(longDate, "HH:mm");
            }

        } else if (currentMonth - loginMonth == 1) {
            int maxDayNumber = cale.getActualMaximum(Calendar.DAY_OF_MONTH);
            if (days == maxDayNumber && currentCaleDays == 1) {
                return "昨天" + getDateFormat(longDate, "HH:mm");
            }
        }

        format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(longDate);
    }

    /**
     * get specific format String date <br>
     * <ul>
     * today HH:mm:ss (时:分)<br>
     * yesterday : yesterday (昨天) <br>
     * more before : yyyy-MM-dd HH:mm:ss <br>
     * </ul>
     *
     * @param longDate
     * @return
     */
    public static String getDateForChatMsg(long longDate) {
        SimpleDateFormat format = null;

        Date dates = new Date(longDate);
        Calendar cale = Calendar.getInstance();
        cale.setTime(dates);

        Date nowDates = new Date(System.currentTimeMillis());
        Calendar currentCale = Calendar.getInstance();
        currentCale.setTime(nowDates);

        int days = cale.get(Calendar.DAY_OF_MONTH);
        int currentCaleDays = currentCale.get(Calendar.DAY_OF_MONTH);

        int loginMonth = cale.get(Calendar.MONTH);
        int currentMonth = currentCale.get(Calendar.MONTH);

        if (currentMonth == loginMonth) {
            if (currentCaleDays - days == 0) {
                return getDateFormat(longDate, "HH:mm");
            }

            if (currentCaleDays - 1 == days) {
                // need get context
                return "昨天" + getDateFormat(longDate, "HH:mm");
            }

        } else if (currentMonth - loginMonth == 1) {
            int maxDayNumber = cale.getActualMaximum(Calendar.DAY_OF_MONTH);
            if (days == maxDayNumber && currentCaleDays == 1) {
                return "昨天" + getDateFormat(longDate, "HH:mm");
            }
        }

        format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(longDate);
    }

    /**
     * get the time format , like HH:mm:ss
     *
     * @param mTimeLine
     * @return
     */
    public static String getDateFormat(long mTimeLine, String format) {

        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date currentTime = new Date(mTimeLine);
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    /**
     * get standard date time , like 2014-09-01 14:20:22
     *
     * @return
     */
    public static String getStandardDate(Date date) {

        if (date == null)
            throw new RuntimeException("Given date object is null...");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date.getTime());
    }

    /**
     * get the time format , like HH:mm:ss ; mm:ss ; ss ...
     *
     * @param times
     * @return
     */
    public static String calculateTime(long times) {
        times = times / 1000;
        int hour = (int) times / 3600;
        int minute = (int) (times - (hour * 3600)) / 60;
        int second = (int) times - (hour * 3600 + minute * 60);
        if (minute <= 0 && hour <= 0) {
            return (second < 10 ? "0" + second : second) + "秒";
        } else if (hour <= 0) {
            return (minute < 10 ? "0" + minute : minute) + "分"
                    + (second < 10 ? "0" + second : second) + "秒";
        } else {
            if (minute <= 0 && second > 0) {
                return (hour < 10 ? "0" + hour : hour) + "时"
                        + (second < 10 ? "0" + second : second) + "秒";
            } else if (second <= 0 && minute > 0) {
                return (hour < 10 ? "0" + hour : hour) + "时"
                        + (minute < 10 ? "0" + minute : minute) + "分";
            } else if (second <= 0 && minute <= 0) {
                return (hour < 10 ? "0" + hour : hour) + "时";
            } else {
                return (hour < 10 ? "0" + hour : hour) + "时"
                        + (minute < 10 ? "0" + minute : minute) + "分"
                        + (second < 10 ? "0" + second : second) + "秒";
            }
        }
    }

    /**
     * Time format display when the p2p voiceCall..
     *
     * @return
     */
    public static String calculateFixedTime(long callTime) {
        int hour = (int) callTime / 3600;
        int minute = (int) (callTime - (hour * 3600)) / 60;
        int second = (int) callTime - (hour * 3600 + minute * 60);
        return (hour < 10 ? "0" + hour : hour) + ":"
                + (minute < 10 ? "0" + minute : minute) + ":"
                + (second < 10 ? "0" + second : second);
    }

    public static String secToTime(int time) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (time <= 0)
            return "00:00";
        else {
            minute = time / 60;
            if (minute < 60) {
                second = time % 60;
                timeStr = unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                if (hour > 99)
                    return "99:59:59";
                minute = minute % 60;
                second = time - hour * 3600 - minute * 60;
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
            }
        }
        return timeStr;
    }

    public static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10)
            retStr = "0" + Integer.toString(i);
        else
            retStr = "" + i;
        return retStr;
    }

    //获取当前日期时间
    public static String getNowYMDHSM(){
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateformat.format(System.currentTimeMillis());
    }

    public static String getNowYMD(){
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy/MM/dd");
        return dateformat.format(System.currentTimeMillis());
    }
}

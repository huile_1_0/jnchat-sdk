package com.jndv.jndvchatlibrary.ui.crowd.bean;

/**
 * 群成员信息
 */
public class JCNumbersData {
    private int id;
    private String userId;
    private String friendId;
    private String domainAddr;
    private String domainAddrUser;
    private String remark;
    private long createTime;
    private String friendName;
    private String friendPic;
    private boolean groupChatSelectState = false;
    private boolean hasInvite=false;


    public void setGroupChatSelectState(boolean groupChatSelectState){
        this.groupChatSelectState = groupChatSelectState;
    }
    public boolean isGroupChatSelectState(){
        return groupChatSelectState;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getFriendId() {
        return friendId;
    }
    public void setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
    }

    public String getDomainAddr() {
        return domainAddr;
    }

    public String getDomainAddrUser() {
        return domainAddrUser;
    }

    public void setDomainAddrUser(String domainAddrUser) {
        this.domainAddrUser = domainAddrUser;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getCreateTime() {
        return createTime;
    }
    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendName() {
        return friendName;
    }
    public void setFriendPic(String friendPic) {
        this.friendPic = friendPic;
    }

    public String getFriendPic() {
        return friendPic;
    }

    public boolean isHasInvite() {
        return hasInvite;
    }

    public void setHasInvite(boolean hasInvite) {
        this.hasInvite = hasInvite;
    }

    @Override
    public String toString() {
        return "JCNumbersData{" +
                "id=" + id +
                ", userId=" + userId +
                ", friendId=" + friendId +
                ", domainAddr='" + domainAddr + '\'' +
                ", domainAddrUser='" + domainAddrUser + '\'' +
                ", remark='" + remark + '\'' +
                ", createTime=" + createTime +
                ", friendName='" + friendName + '\'' +
                ", friendPic='" + friendPic + '\'' +
                ", groupChatSelectState=" + groupChatSelectState +
                ", hasInvite=" + hasInvite +
                '}';
    }
}

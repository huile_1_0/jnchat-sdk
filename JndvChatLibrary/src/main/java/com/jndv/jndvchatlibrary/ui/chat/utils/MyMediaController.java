package com.jndv.jndvchatlibrary.ui.chat.utils;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.MediaController;

/**
 * Author: wangguodong
 * Date: 2022/7/13
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 视频播放控制器
 */
public class MyMediaController extends MediaController {
    public MyMediaController(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyMediaController(Context context, boolean useFastForward) {
        super(context, useFastForward);
    }

    public MyMediaController(Context context) {
        super(context);
    }

    @Override
    public void show(int timeout) {
        super.show(0);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            super.hide();
            Activity a = (Activity)getContext();
            a.finish();

        }
        return true;
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        initView();
    }

    private void initView(){

    }
}

package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.eventbus.RefreshEvent;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJcmsgnotifylistBinding;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCInvitationApprovalBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCJoinGroupAuditBean;
import com.jndv.jndvchatlibrary.ui.chat.fragment.popup.TipCenterPopup;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 消息通知列表页面（进群邀请+入群审核）
 */
public class JCMsgNotifyListFragment extends JCbaseFragment implements OnRefreshLoadMoreListener {
    private FragmentJcmsgnotifylistBinding binding;
    private JCMsgNotifyViewModel viewModel;
    private int flag = 1;//1:进群邀请 2：入群审核
    private int page = 1;
    private String groupId;
    private String groupDomainAddr;
    private String groupDomainAddrUser;

    private CommonAdapter adapter;
    private List<JCInvitationApprovalBean.ListBean> approvalList = new ArrayList<>();
    private List<JCJoinGroupAuditBean.ListBean> joinList = new ArrayList<>();
    private Dialog mDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentJcmsgnotifylistBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        initIntent();
        init();
        return root;
    }

    private void initIntent() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            flag = bundle.getInt("flag", 1);
            groupId = bundle.getString("groupId");
            groupDomainAddr = bundle.getString("groupDomainAddr");
            groupDomainAddrUser = bundle.getString("groupDomainAddrUser");
        }
    }

    private void init() {
        binding.recycleView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.listRefresh.setOnRefreshLoadMoreListener(this);
        binding.listRefresh.autoRefresh();
        if (flag == 1) {
            adapter = new CommonAdapter<JCInvitationApprovalBean.ListBean>(getContext(), R.layout.notify_item, approvalList) {
                @SuppressLint("SetTextI18n")
                @Override
                protected void convert(ViewHolder holder, JCInvitationApprovalBean.ListBean adapterDataBean, int position) {
                    Log.e("initData", adapterDataBean.toString());
                    JCInvitationApprovalBean.GroupInfo groupInfo = new Gson().fromJson(adapterDataBean.getGroupInfo(), JCInvitationApprovalBean.GroupInfo.class);
                    ((TextView) holder.getView(R.id.notify_msg))
                            .setText( adapterDataBean.getJoinUserName()
                                    + getString(R.string.invitation_approval_str)
                                    + groupInfo.getCgName());
                    String status = "待审核";
                    int statusColor = R.drawable.selector_blue_r;
                    switch (adapterDataBean.getApprovalStatus()) {
                        case 1:
                            status = "已通过";
                            statusColor = R.drawable.selector_green_r;
                            break;
                        case 2:
                            status = "已拒绝";
                            statusColor = R.drawable.selector_red_r;
                            break;
                    }
                    ((TextView) holder.getView(R.id.notify_status)).setText(status);
                    holder.getView(R.id.notify_status).setBackgroundResource(statusColor);
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String format = sdf.format(new Date(Long.valueOf(adapterDataBean.getAddTime() + "000")));
                        ((TextView) holder.getView(R.id.notify_time)).setText(format);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    holder.setOnClickListener(R.id.notify_status, v -> {
                        if (adapterDataBean.getApprovalStatus() == 0) {
                            showDialog("", adapterDataBean.getId());
                        }
                    });
                }
            };
        } else {
            adapter = new CommonAdapter<JCJoinGroupAuditBean.ListBean>(getContext(), R.layout.notify_item, joinList) {
                @Override
                protected void convert(ViewHolder holder, JCJoinGroupAuditBean.ListBean adapterDataBean, int position) {
                    Log.e("initData", adapterDataBean.toString());
                    ((TextView) holder.getView(R.id.notify_msg)).setText(adapterDataBean.getJoinNickname()
                                    + getString(R.string.join_group_str));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String format = sdf.format(new Date(Long.valueOf(adapterDataBean.getAddTime() + "000")));
                    ((TextView) holder.getView(R.id.notify_time)).setText(format);

                    String status = "审核";
                    int statusColor = R.color.button_color_blue;
                    switch (adapterDataBean.getAuditStatus()) {
                        case 1:
                            status = "已通过";
                            statusColor = R.color.green_normal;
                            break;
                        case 2:
                            status = "已拒绝";
                            statusColor = R.color.red;
                            break;
                    }
                    ((TextView) holder.getView(R.id.notify_status)).setText(status);
                    holder.getView(R.id.notify_status).setBackgroundResource(statusColor);
                    holder.setOnClickListener(R.id.notify_status, v -> {
                        if (adapterDataBean.getAuditStatus() == 0) {
                            showDialog(adapterDataBean.getGroupId(), adapterDataBean.getId());
                        }
                    });
                }
            };
        }
        binding.recycleView.setAdapter(adapter);
        initData();

    }

    private void initData() {
        viewModel = new JCMsgNotifyViewModel(this);
        viewModel.getApprovalList().observeForever(list -> {
            if (list == null || list.size() == 0) {
                binding.listRefresh.finishLoadMore();
                binding.listRefresh.finishRefresh();
                showToast("暂无数据！");
                if(page==1){
                    binding.tvEmpty.setVisibility(View.VISIBLE);
                }else {
                    binding.tvEmpty.setVisibility(View.GONE);
                }
            } else {
                binding.tvEmpty.setVisibility(View.GONE);
                if (page == 1) {
                    approvalList.clear();
                    approvalList.addAll(list);
                    binding.listRefresh.finishRefresh(1000);
                } else {
                    approvalList.addAll(list);
                    binding.listRefresh.finishLoadMore(1000);
                }
                adapter.notifyDataSetChanged();
            }
        });
        viewModel.getJoinList().observeForever(list -> {
            if (list == null || list.size() == 0) {
                binding.listRefresh.finishLoadMore();
                binding.listRefresh.finishRefresh();
                showToast("暂无数据！");
                if(page==1){
                    binding.tvEmpty.setVisibility(View.VISIBLE);
                }else {
                    binding.tvEmpty.setVisibility(View.GONE);
                }
            } else {
                binding.tvEmpty.setVisibility(View.GONE);
                if (page == 1) {
                    joinList.clear();
                    joinList.addAll(list);
                    binding.listRefresh.finishRefresh(1000);
                } else {
                    joinList.addAll(list);
                    binding.listRefresh.finishLoadMore(1000);
                }
                adapter.notifyDataSetChanged();
            }
        });

        viewModel.getApprovalResult().observeForever(s -> {
            if(mDialog!=null){
                mDialog.dismiss();
            }
            if (s == null) {
                showToast("操作失败");
            }else if (TextUtils.equals("0", s)){
                getListData();
            } else {
                showToast(s);
            }
        });
        viewModel.getJoinResult().observeForever(s -> {
            if(mDialog!=null){
                mDialog.dismiss();
            }
            if (s == null) {
                showToast("操作失败");
            } else {
                getListData();
            }
        });
    }


    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        page++;
        getListData();
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        page = 1;
        getListData();
    }

    private void getListData() {
        if (flag == 1) {
            viewModel.getNotifyListData(3, page);
        } else if (flag == 2) {
            viewModel.getJoinAuditListData(3, groupId, groupDomainAddr, groupDomainAddrUser, page);
        }
    }


    private void showDialog(String groupId, int id) {

        TipCenterPopup tipCenterPopup = new TipCenterPopup(getActivity());
        tipCenterPopup.setTitle(getText(flag == 1 ? R.string.invitation_conform : R.string.join_conform).toString());
        tipCenterPopup.setCleanListenner("", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipCenterPopup.dismiss();
                updateStatus(groupId, 2, id);
            }
        });
        tipCenterPopup.setOkListenner("", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipCenterPopup.dismiss();
                updateStatus(groupId, 1, id);
            }
        });
        new XPopup.Builder(getActivity())
//                .hasStatusBar(false)
//                .hasShadowBg(true)
//                .atView(binding.topTitle.menu)
                .asCustom(tipCenterPopup).show();
//        mDialog = DialogManager
//                .getInstance()
//                .showNoTitleDialog(
//                        DialogManager.getInstance().new DialogInterface(
//                                getActivity(),
//                                null,
//                                getText(flag == 1 ? R.string.invitation_conform : R.string.join_conform),
//                                getText(R.string.approve),
//                                getText(R.string.disapprove)) {
//
//                            @Override
//                            public void confirmCallBack() {
//                                updateStatus(groupId, 1, id);
//                            }
//
//                            @Override
//                            public void cannelCallBack() {
//                                updateStatus(groupId, 2, id);
//                            }
//                        });
//
//            mDialog.show();
    }

    /**
     * 处理通知，通过/拒绝
     *
     * @param groupId
     * @param status
     * @param id
     */
    private void updateStatus(String groupId, int status, int id) {
        if (flag == 1) {
            viewModel.updateGroupInvitationApproval(status, id);
        } else if (flag == 2) {
            viewModel.updateJoinGroupAudit(groupId, status, id);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().post(new RefreshEvent());
    }
}

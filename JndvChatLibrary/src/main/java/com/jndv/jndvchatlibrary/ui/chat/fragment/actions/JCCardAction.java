package com.jndv.jndvchatlibrary.ui.chat.fragment.actions;

import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.listUi.JCbaseActions;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgCardContentBean;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCFriendListFragment;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;


/**
 * @ProjectName: JndvChat
 * @Package: com.jndv.jndvchatlibrary.ui.chat.fragment.actions
 * @ClassName: JCCardAction
 * @Description: 发送名片按钮
 * @Author: SunQinzheng
 * @CreateDate: 2022/2/19 下午 3:13
 */
public class JCCardAction extends JCbaseActions {
//    TextView mmm;
    public JCCardAction() {
        super(R.string.action_card, R.drawable.jc_chat_action_card);
    }

    @Override
    public void onClick() {
        Log.e("JCCardAction","++++");
//        mmm.setText("");
        Bundle bundle = new Bundle();
        bundle.putBoolean(JCFriendListFragment.KEY_IS_SELECTE, true);
        JCbaseFragmentActivity.start(JCChatManager.mContext
                , new JCFriendListFragment(), "选择好友",bundle, new JCbaseFragment.JCFragmentSelect() {
                    @Override
                    public void onSelecte(int type, Object... objects) {
                        if (type==JCbaseFragment.SELECTE_TYPE_FRIEND){
                            JCMsgCardContentBean jcMsgCardContentBean = (JCMsgCardContentBean) objects[0];

                            String contentBase64 = new Gson().toJson(jcMsgCardContentBean);
//                            contentBase64 = JCContentEncryptUtils.encodeContent(contentBase64);

                            String msgType = ""+JCmessageType.CARD;
                            JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                                    contentBase64,msgType, getOtherId(), JNBasePreferenceSaves.getUserSipId()
                                    , JNBasePreferenceSaves.getSipAddress(), getOtherId(), getOtherSipAddress()
                                    , ""+getSessionType(), JNBasePreferenceSaves.getJavaAddress(),getOtherJavaAddress());
                            sendMessage(jCimMessageBean);
                        }
                    }
                }, true);
    }
}

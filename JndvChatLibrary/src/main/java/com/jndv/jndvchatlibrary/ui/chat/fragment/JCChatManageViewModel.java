package com.jndv.jndvchatlibrary.ui.chat.fragment;


import android.text.TextUtils;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCUpdateSessionStateApi;


import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/4/20
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCChatManageViewModel extends ViewModel {

    private Fragment fragment;

    private MutableLiveData<JCChatManageModelItem> chatDetail ;

    public JCChatManageViewModel() {
        chatDetail = new MutableLiveData<>();
    }

    public void init(Fragment fragment){
        this.fragment = fragment ;
    }

    public MutableLiveData<JCChatManageModelItem> getChatManageModel() {
        return chatDetail;
    }

    public void updateSession(String sid, int top, int nodisturb) {
        JCUpdateSessionStateApi api = new JCUpdateSessionStateApi()
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setSessionId(sid)
                .setTop(top)
                .setNoDisturb(nodisturb);

        EasyHttp.put(fragment)
                .api(api)
                .json(new Gson().toJson(api))
                .request(new OnHttpListener<JCUpdateSessionStateApi.Bean>() {
                    @Override
                    public void onSucceed(JCUpdateSessionStateApi.Bean result) {
                        Log.e("JCChatManageViewModel", "onSucceed: " + result.toString());
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                            JCChatManageModelItem jcChatManageModelItem = new JCChatManageModelItem();
                            jcChatManageModelItem.setType(0);
                            jcChatManageModelItem.setSid(sid);
                            jcChatManageModelItem.setTop(top);
                            jcChatManageModelItem.setNodisturb(nodisturb);
                            chatDetail.setValue(jcChatManageModelItem);
                            updateData(sid,top);
                        }else {
                            JCChatManageModelItem jcChatManageModelItem = new JCChatManageModelItem();
                            jcChatManageModelItem.setType(1);
                            chatDetail.setValue(jcChatManageModelItem);
                        }
                    }

                    @Override
                    public void onFail(Exception e) {
                        JCChatManageModelItem jcChatManageModelItem = new JCChatManageModelItem();
                        jcChatManageModelItem.setType(1);
                        chatDetail.setValue(jcChatManageModelItem);
                    }
                });
    }

    private void updateData(String sessionId, int top) {
        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
        JCSessionListBean jcSessionListBean = builder.equal(JCSessionListBean_.session_id, Long.parseLong(sessionId)).build().findFirst();
        jcSessionListBean.setIs_top(top);
        imMsgBox.put(jcSessionListBean);
    }

    public class JCChatManageModelItem {

    private int type;//0=成功；1=失败
    private String sid;//
    private int top;//0=成功；1=失败
    private int nodisturb;//0=成功；1=失败

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getNodisturb() {
        return nodisturb;
    }

    public void setNodisturb(int nodisturb) {
        this.nodisturb = nodisturb;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
}

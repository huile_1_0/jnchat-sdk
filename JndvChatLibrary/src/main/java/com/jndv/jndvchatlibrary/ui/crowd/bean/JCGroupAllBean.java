package com.jndv.jndvchatlibrary.ui.crowd.bean;

import java.util.List;

/**
 * @Description
 * @Author SunQinzheng
 * @Time 2022/5/5 下午 1:04
 **/
public class JCGroupAllBean {
    private JCGroupInfoBean group_info;
    private List<JCGroupNumbersBean> group_numbers;
    private String identity_permission;

    //新增当前用户的邀请权限
    private String invite_permission;

    public JCGroupInfoBean getGroup_info() {
        return group_info;
    }

    public void setGroup_info(JCGroupInfoBean group_info) {
        this.group_info = group_info;
    }

    public List<JCGroupNumbersBean> getGroup_numbers() {
        return group_numbers;
    }

    public void setGroup_numbers(List<JCGroupNumbersBean> group_numbers) {
        this.group_numbers = group_numbers;
    }

    public String getIdentity_permission() {
        return identity_permission;
    }

    public void setIdentity_permission(String identity_permission) {
        this.identity_permission = identity_permission;
    }

    public String getInvite_permission() {
        return invite_permission;
    }

    public void setInvite_permission(String invite_permission) {
        this.invite_permission = invite_permission;
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;


public class JCCreatGroupNoticeApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/groupNotice/create";
    }


    private String domainAddr;
    private String userId;
    private String groupId;
    private String content;

    public JCCreatGroupNoticeApi(String domainAddr, String userId, String groupId, String content) {
        this.domainAddr = domainAddr;
        this.userId = userId;
        this.groupId = groupId;
        this.content = content;
    }
}

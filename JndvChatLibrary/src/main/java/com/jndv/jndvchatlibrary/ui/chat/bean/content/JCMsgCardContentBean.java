package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
 * @ProjectName: JndvChat
 * @Package: com.jndv.jndvchatlibrary.ui.chat.bean
 * @ClassName: JCMsgCardContentBean
 * @Description: 名片消息内容类
 * @Author: SunQinzheng
 * @CreateDate: 2022/2/19 上午 11:15
 */
public class JCMsgCardContentBean {

    private String name;
    private String head;
    private String uID;//好友sip号
    private String domainAddr;//好友sip域地址
    private String domainAddrJava;//好友java域地址

    public JCMsgCardContentBean(String name, String head, String uID, String domainAddr, String domainAddrJava) {
        this.name = name;
        this.head = head;
        this.uID = uID;
        this.domainAddr = domainAddr;
        this.domainAddrJava = domainAddrJava;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getuID() {
        return uID;
    }

    public void setuID(String uID) {
        this.uID = uID;
    }

    public String getDomainAddr() {
        return domainAddr;
    }

    public void setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
    }

    public String getDomainAddrJava() {
        return domainAddrJava;
    }

    public void setDomainAddrJava(String domainAddrJava) {
        this.domainAddrJava = domainAddrJava;
    }
}

package com.jndv.jndvchatlibrary.ui.chat.bean;

public class JCFriendRelationShipBean {

    /**
     * friendName : 金诺研发测试2
     * friendPic : 1
     * domainAddrUser : imapi.jndv.com
     * friendId : 1002
     * createTime : 2022-07-15T08:50:07
     * domainAddr : imapi.jndv.com:5080
     * remark :
     * id : 202
     * userId : 37130109013320109013
     */

    private String friendName;
    private String friendPic;
    private String domainAddrUser;
    private String friendId;
    private String createTime;
    private String domainAddr;
    private String remark;
    private int id;
    private String userId;

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendPic() {
        return friendPic;
    }

    public void setFriendPic(String friendPic) {
        this.friendPic = friendPic;
    }

    public String getDomainAddrUser() {
        return domainAddrUser;
    }

    public void setDomainAddrUser(String domainAddrUser) {
        this.domainAddrUser = domainAddrUser;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDomainAddr() {
        return domainAddr;
    }

    public void setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.entity;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class JCUserAvatarObject implements Parcelable {

	private long mUId;
	
	private String mAvatarPath;
	
	/**
	 * Never will be parcel
	 */
	private Bitmap bm;
	
	public JCUserAvatarObject(Parcel in) {
		if (in != null) {
			mUId = in.readLong();
			mAvatarPath = in.readString();
		}
	}
	
	
	
	public JCUserAvatarObject(long uId, String avatarPath) {
		super();
		this.mUId = uId;
		this.mAvatarPath = avatarPath;
	}



	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flag) {
		out.writeLong(mUId);
		out.writeString(mAvatarPath);
	}
	
	public static final Creator<JCUserAvatarObject> CREATOR = new Creator<JCUserAvatarObject>() {
	    public JCUserAvatarObject createFromParcel(Parcel in) {
	        return new JCUserAvatarObject(in);
	    }

	    public JCUserAvatarObject[] newArray(int size) {
	        return new JCUserAvatarObject[size];
	    }
	};

	public long getUId() {
		return mUId;
	}



	public void setUId(long mUId) {
		this.mUId = mUId;
	}



	public String getAvatarPath() {
		return mAvatarPath;
	}



	public void setAvatarPath(String mAvatarPath) {
		this.mAvatarPath = mAvatarPath;
	}



	public Bitmap getBm() {
		return bm;
	}



	public void setBm(Bitmap bm) {
		this.bm = bm;
	}


	@Override
	public String toString() {
		return "UserAvatarObject{" +
				"mUId=" + mUId +
				", mAvatarPath='" + mAvatarPath + '\'' +
				", bm=" + bm +
				'}';
	}
}

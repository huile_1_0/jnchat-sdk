package com.jndv.jndvchatlibrary.ui;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.OptIn;
import androidx.media3.common.MediaItem;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.Player;
import androidx.media3.common.Timeline;
import androidx.media3.common.util.UnstableApi;
import androidx.media3.datasource.DefaultDataSourceFactory;
import androidx.media3.exoplayer.ExoPlayer;
import androidx.media3.exoplayer.analytics.AnalyticsListener;
import androidx.media3.exoplayer.rtsp.RtspMediaSource;
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory;
import androidx.media3.exoplayer.source.LoopingMediaSource;
import androidx.media3.exoplayer.source.MediaSource;
import androidx.media3.exoplayer.source.ProgressiveMediaSource;
import androidx.media3.ui.PlayerView;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.IntentConstant;
import com.jndv.jnbaseutils.ui.base.JNBaseHeadActivity;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.ActivityVodDisplayBinding;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.player.Strings;

import java.io.IOException;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class VideoPlayActivity extends JNBaseHeadActivity {
//public class VideoPlayActivity extends JCBaseHeadActivity {

    private final String TAG = "VideoPlayActivity";
//    String url="https://192.168.1.24:8203//group1/M03/00/B9/wKgBGGTy2Y-ACLy1ACMUhVCDFxk483.Mp4";

    ActivityVodDisplayBinding binding;
    private boolean isPlaying = false;
    private boolean isPause = false;
    private Boolean comeBackFromRestart = false;
    private long quit_time;
    private int mVideoProgress = 0;
    private String url = "";

    ExoPlayer player;
//    PlayerView playerView;

    ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(3);

    private SeekBar.OnSeekBarChangeListener mSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            try {
                Log.e(TAG, "onStopTrackingTouch: ==========");
                mVideoProgress = binding.vodDisplaySeekbar.getProgress();
                player.seekTo(mVideoProgress);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    private void videoPlayEnd() {
        if (player != null) {
            player.release();
        }
        isPlaying = false;
        cancelLoadding();
    }

    @OptIn(markerClass = UnstableApi.class)
    private void initPlayer(){
        if (null==player)player = new ExoPlayer.Builder(this).build();
        String path = url;
        Log.e(TAG, "initPlayer: ====path=" + path);
//        MediaSource mediaSource = new DefaultMediaSourceFactory(this).createMediaSource(MediaItem.fromUri(path));
        ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultDataSourceFactory(this)).createMediaSource(MediaItem.fromUri(path));
        player.setMediaSource(mediaSource);

        binding.playerView.setPlayer(player);

        player.addListener(new Player.Listener() {
            @Override
            public void onTimelineChanged(Timeline timeline, int reason) {
                Player.Listener.super.onTimelineChanged(timeline, reason);
                Log.e(TAG, "onTimelineChanged: ======timeline==" +new Gson().toJson(timeline));
                Log.e(TAG, "onTimelineChanged: ======reason==" +reason);
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                Player.Listener.super.onPlayerStateChanged(playWhenReady, playbackState);
                Log.e(TAG, "onPlayerStateChanged: ========" +playbackState);
//                Player.COMMAND_CHANGE_MEDIA_ITEMS
                switch (playbackState) {
                    case ExoPlayer.STATE_ENDED://已完成
                        Log.i(TAG, "Playback ended!");
                        //Stop playback and return to start position
                        quit_time = 0 ;
                        isPause = true;
                        binding.vodDisplayPause.setImageResource(R.mipmap.start);
//                        binding.ksytextureView.pause();
                        binding.ivPlay.setVisibility(View.VISIBLE);
                        break;
                    case ExoPlayer.STATE_READY://准备好
//                        if (imgPreview!=null){
//                            imgPreview.setVisibility(View.GONE);
//                        }
//                        setVideoProgress(0);
                        long time = player.getCurrentPosition();
                        long length = player.getDuration();

                        Log.e(TAG, "setVideoProgress: ====002====time=="+time);
                        Log.e(TAG, "setVideoProgress: ====002====length=="+length);
                        binding.vodDisplaySeekbar.setMax((int) length);
                        binding.vodDisplaySeekbar.setProgress((int) time);
                        binding.vodDisplayTotalTime.setText(Strings.millisToString(length));
//                        initTimerTask();
                        if (!isPause){
                            if (comeBackFromRestart) {
                                player.seekTo(quit_time);
                                player.play();
                                if (!isPlaying) {
                                    player.pause();
                                }
//                isPlaying = true;
                                comeBackFromRestart = false;
                            } else {
                                player.play();
                                binding.ivPlay.setVisibility(View.GONE);
                            }
                        }
                        break;
                    case ExoPlayer.STATE_BUFFERING://正在缓冲
                        Log.i(TAG, "Playback buffering!");
                        break;
                    case ExoPlayer.STATE_IDLE://有限资源，没准备好
                        Log.i(TAG, "ExoPlayer idle!");
                        break;
                }
//                player.release();
//                player.play();
            }

            @Override
            public void onIsPlayingChanged(boolean isPlaying) {
                Player.Listener.super.onIsPlayingChanged(isPlaying);
                Log.e(TAG, "onIsPlayingChanged: ==isPlaying=="+isPlaying);
                if (isPlaying){
                    cancelLoadding();
                }
            }

            @Override
            public void onIsLoadingChanged(boolean isLoading) {
                Player.Listener.super.onIsLoadingChanged(isLoading);
                Log.e(TAG, "onIsLoadingChanged: ==isLoading=="+isLoading);
            }

            @Override
            public void onPlayerError(PlaybackException error) {
                Player.Listener.super.onPlayerError(error);
                Log.e(TAG, "onPlayerError: ==error=="+new Gson().toJson(error));
                videoPlayEnd();
            }

            @Override
            public void onPlaybackStateChanged(int playbackState) {
                Player.Listener.super.onPlaybackStateChanged(playbackState);
                Log.e(TAG, "onPlaybackStateChanged: ==playbackState=="+playbackState);
            }

            @Override
            public void onPlayerErrorChanged(@Nullable PlaybackException error) {
                Player.Listener.super.onPlayerErrorChanged(error);
                Log.e(TAG, "onPlayerErrorChanged: ==error=="+new Gson().toJson(error));
            }

            @Override
            public void onPlayWhenReadyChanged(boolean playWhenReady, int reason) {
                Player.Listener.super.onPlayWhenReadyChanged(playWhenReady, reason);
                Log.e(TAG, "onPlayWhenReadyChanged: ==playWhenReady=="+playWhenReady);
            }

            @Override
            public void onSeekForwardIncrementChanged(long seekForwardIncrementMs) {
                Player.Listener.super.onSeekForwardIncrementChanged(seekForwardIncrementMs);
                Log.e(TAG, "onSeekForwardIncrementChanged: ==seekForwardIncrementMs=="+seekForwardIncrementMs);
            }

            @Override
            public void onSeekBackIncrementChanged(long seekBackIncrementMs) {
                Player.Listener.super.onSeekBackIncrementChanged(seekBackIncrementMs);
                Log.e(TAG, "onSeekBackIncrementChanged: ==seekBackIncrementMs=="+seekBackIncrementMs);
            }
        });
        player.prepare();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityVodDisplayBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setCenterTitle("视频");

        url=getIntent().getStringExtra(IntentConstant.WEB_URL);
        Log.e(TAG, "onCreate: =======url="+url);
        binding.vodDisplaySeekbar.setOnSeekBarChangeListener(mSeekBarChangeListener);

        binding.vodDisplayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePause();
            }
        });

        binding.ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePause();
            }
        });

        startToPlay();

        goneTopLine();
        hindTitle();

    }

    private void changePause(){
        if (isPause){
            isPause = false;
            binding.vodDisplayPause.setImageResource(R.mipmap.stop_full_screen);
//            binding.playerView.start();
            if (player != null) {
                player.seekTo(quit_time);
                player.play();
            }
            binding.ivPlay.setVisibility(View.GONE);
            initTimerTask();
        }else {
            quit_time = player.getCurrentPosition();
            isPlaying = false;
            cancelLoadding();
            isPause = true;
            binding.vodDisplayPause.setImageResource(R.mipmap.start);
//            binding.playerView.pause();
            if (player != null) {
                Log.e(TAG, "changePause: ===0001=====");
                player.pause();
//                player.pause();
//                player.stop();
            }
            scheduledThreadPoolExecutor.shutdown();
            binding.ivPlay.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 初始化定时器
     */
    private void initTimerTask() {
        try {
//            scheduledThreadPoolExecutor.shutdown();
            scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(3);
        } catch (Exception e) {
            Log.e(TAG, "scheduledThreadPoolExecutor: ", e);
            e.printStackTrace();
        }

        try {
            scheduledThreadPoolExecutor.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    try {
                        setVideoProgress(0);
                    } catch (Exception e) {
                        Log.e(TAG, "scheduledThreadPoolExecutor01: ", e);
                        e.printStackTrace();
                    }
                }
            }, 300, 300, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            Log.e(TAG, "scheduledThreadPoolExecutor: ", e);
            e.printStackTrace();
        }

    }

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:

                    break;
                case MotionEvent.ACTION_POINTER_UP:
                case MotionEvent.ACTION_UP:
                    changePause();
                    break;
                default:
                    break;
            }
            return true;
        }
    };

    @SuppressLint("ClickableViewAccessibility")
    private void startToPlay() {
        showLoadding();

        binding.playerView.setOnTouchListener(mTouchListener);
        isPlaying = true;
        initPlayer();
        initTimerTask();
    }

    public void setVideoProgress(int currentProgress) {
        JCThreadManager.onMainHandler(new Runnable() {
            @Override
            public void run() {
                long time = currentProgress > 0 ? currentProgress :player.getCurrentPosition();
                long length = player.getDuration();

                Log.e(TAG, "setVideoProgress: ========time=="+time);
                Log.e(TAG, "setVideoProgress: ========length=="+length);
//                binding.vodDisplaySeekbar.setMax((int) length);
                binding.vodDisplaySeekbar.setProgress((int) time);
//                binding.vodDisplayTotalTime.setText(Strings.millisToString(length));
                if (time >= 0) {
//                    Log.e(TAG, "setVideoProgress: =====str===time=="+Strings.millisToString(time));
                    binding.vodDisplayCurrentTime.setText(Strings.millisToString(time));
                }

            }
        });

//        return (int) time;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isPause)changePause();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        try {
            comeBackFromRestart = true;
        }catch (Exception e){
            e.printStackTrace();
        }

//        startToPlay();
        changePause();
    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoPlayEnd();

        scheduledThreadPoolExecutor.shutdown();
    }

}

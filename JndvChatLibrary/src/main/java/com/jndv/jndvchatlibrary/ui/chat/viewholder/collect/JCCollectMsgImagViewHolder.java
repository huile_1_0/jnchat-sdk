package com.jndv.jndvchatlibrary.ui.chat.viewholder.collect;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.RequestBuilder;
import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.http.api.JCGetCollectMessageApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgImgContentBean;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

/**
 * Author: wangguodong
 * Date: 2022/6/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 收藏消息，viewHolder实体
 */
public class JCCollectMsgImagViewHolder extends JCCollectMsgBaseViewHolder{
    private ImageView ivMessage;
    public JCCollectMsgImagViewHolder(Context context, View itemView) {
        super(context, itemView);
    }

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_image;
    }

    @Override
    protected void showContentView(JCGetCollectMessageApi.ColloctBean colloctBean) {
        ivMessage = findViewById(R.id.iv_image);
        try {
            JCbaseIMMessage message = new Gson().fromJson(colloctBean.getBody(), JCimMessageBean.class);
//            JNLogUtil.loge("==decodeBase64="+decodeBase64);
            JCMsgImgContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(message.getContent()), JCMsgImgContentBean.class);
//            Log.e("netdata", "bindContentView: ========002======"+message.getContent());
            int w = 240;
            int h = 240 ;
            Log.e("netdata", "bindContentView: =============w="+w+"==h="+h);
            setLayoutParams(w,h,ivMessage);
            if (null!=bean.getPath() || !TextUtils.isEmpty(bean.getPath())){
                Log.e("netdata", "bindContentView: ========003======");
                try {
                    RequestBuilder<Drawable> requestBuilder = JCglideUtils.loadImageBuilder(mContext, bean.getUrl());
                    JCglideUtils.loadImage(mContext, bean.getPath(), ivMessage, requestBuilder);
                }catch (Exception e){
                    e.printStackTrace();
                    if (!TextUtils.isEmpty(bean.getUrl())){
                        JCglideUtils.loadImage(mContext, bean.getUrl(), ivMessage);
                    }
                }
            }else {
                Log.e("netdata", "bindContentView: ========004======");
                if (!TextUtils.isEmpty(bean.getUrl())){
                    JCglideUtils.loadImage(mContext, bean.getUrl(), ivMessage);
//                    JCglideUtils.loadImage(context, bean.getUrl(), ivMessage, w, height);
                }
            }
        }catch (Exception e){
            JNLogUtil.e("==JCCollectMsgTextViewHolder==showContentView==", e);
//            tvMessage.setText("未识别的消息");
        }
    }
}

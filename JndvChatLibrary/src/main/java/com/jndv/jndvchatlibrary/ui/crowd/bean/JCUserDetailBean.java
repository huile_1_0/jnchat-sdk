package com.jndv.jndvchatlibrary.ui.crowd.bean;

import java.util.List;

/**
 * @Description 用户详细信息
 * @Author SunQinzheng
 * @Time 2022/5/5 上午 10:03
 **/
public class JCUserDetailBean {
    //  avatarUrl	string	用户头像
    //  mobile	number	消息类型
    //  nickname	number	用户昵称
    //  id	number	用户id
    //  imNumber	string	IM内线号（唯一，与FreeSwitch绑定）
    //  account	number	登录账号
    //   imPort	string	IM内线端口）

    String avatarUrl;
    String mobile;
    String nickname;
    String userId;
    String imNumber;
    String account;
    String imPort;
    String deptid;
    String sipIp;
    String zoningCode;
    String MOBILERTSPIP;
    String MOBILERTSPPort;
    String password;
    String userAge;
    String sex;
    String idCard;
    List<JCServerInfo> serverInfoList;

    public List<JCServerInfo> getServerInfoList() {
        return serverInfoList;
    }

    public void setServerInfoList(List<JCServerInfo> serverInfoList) {
        this.serverInfoList = serverInfoList;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAge() {
        return userAge;
    }

    public void setAge(String userAge) {
        this.userAge = userAge;
    }

    public String getGender() {
        return sex;
    }

    public void setGender(String gender) {
        this.sex = gender;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImNumber() {
        return imNumber;
    }

    public void setImNumber(String imNumber) {
        this.imNumber = imNumber;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getImPort() {
        return imPort;
    }

    public void setImPort(String imPort) {
        this.imPort = imPort;
    }

    public String getDeptid() {
        return deptid;
    }

    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }

    public String getSipIp() {
        return sipIp;
    }

    public void setSipIp(String sipIp) {
        this.sipIp = sipIp;
    }

    public String getZoningCode() {
        return zoningCode;
    }

    public void setZoningCode(String zoningCode) {
        this.zoningCode = zoningCode;
    }

    public String getMOBILERTSPIP() {
        return MOBILERTSPIP;
    }

    public void setMOBILERTSPIP(String MOBILERTSPIP) {
        this.MOBILERTSPIP = MOBILERTSPIP;
    }

    public String getMOBILERTSPPort() {
        return MOBILERTSPPort;
    }

    public void setMOBILERTSPPort(String MOBILERTSPPort) {
        this.MOBILERTSPPort = MOBILERTSPPort;
    }

}

package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import static com.jndv.jndvchatlibrary.JCChatManager.mContext;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgWebNoticeContentBean;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jndvchatlibrary.ui.crowd.utils.GlobalConstant;
import com.jndv.jndvchatlibrary.web.InfoWebActivity;


import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/14
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 默认消息展示holder，兼容未定义的消息类型，显示：未知类型消息
 */
public class JCmsgViewHolderWebNotice extends JCmsgViewHolderBase {
    private static final String TAG = "JCmsgViewHolderWebNotic";
    private TextView tv_title, tv_brife, tv_time;

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_web_notice;
    }

    @Override
    protected void inflateContentView() {
        tv_title = findViewById(R.id.tv_title);
        tv_brife = findViewById(R.id.tv_brife);
        tv_time = findViewById(R.id.tv_time);
    }

    @Override
    protected void bindContentView() {
        JNLogUtil.e("==WebNotice==MsgID=="+message.getMsgID()+"==Content=="+ JNContentEncryptUtils.decryptContent(message.getContent()));
        try {
            JCMsgWebNoticeContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(message.getContent())
                    , JCMsgWebNoticeContentBean.class);
            JNLogUtil.e("======SendTime=="+message.getSendTime());
            JNLogUtil.e("======ReceiveTime=="+message.getReceiveTime());
            try {
                JCMsgWebNoticeContentBean.SipParameterJson mSipParameterJson = bean.getSipParameterJson();
                tv_title.setText(mSipParameterJson.getBigTypeTitle());
                tv_brife.setText(mSipParameterJson.getInfo());
                tv_time.setText(mSipParameterJson.getSeq());
            }catch (Exception e){
                e.printStackTrace();
                tv_title.setText(bean.getType());
                tv_brife.setText(bean.getText());
            }
//            tv_brife.setText(bean.getType());
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            tvMessage.setText(bean.getText()+"\n"+format.format(new Date(Long.parseLong(message.getSendTime())))
//                    +"\n"+format.format(new Date(Long.parseLong(message.getReceiveTime()))));
        }catch (Exception e){
            e.printStackTrace();
            JNLogUtil.e("======getMsgType=="+message.getMsgType());
            tv_title.setText("未识别的消息："+message.getContent());
        }
    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return true;
    }

    @Override
    protected int centerBackground() {
        return R.drawable.jc_white_primary_16;
    }

    @Override
    protected void onItemClick() {
        try {
            JCMsgWebNoticeContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(message.getContent())
                    , JCMsgWebNoticeContentBean.class);

            tv_title.setText(bean.getType());
            boolean operate = false;
            String url = bean.getSipParameterJson().getUrlMobile();

            if (null!=JCChatManager.getmAllOperateListener()){
                try {
                    url = URLDecoder.decode(url, "utf-8");
                }catch (Exception e){
                    e.printStackTrace();
                }
                url = getRealAddress(url);
                Log.e(TAG, "onItemClick: ===========url=="+url );
//                http://192.168.7.9/admin_old/src/BigData/html/ShijianView.html?id=580cc40f-19da-06b9-c1c7-aab44ad3df2e&userid=dgpcsyjq
                operate = JCChatManager.getmAllOperateListener().webShowActivity(url, bean.getType(), true);
            }
            if (!operate) {
                url = getRealAddress(url);
                Intent it = new Intent(mContext, InfoWebActivity.class);
                it.putExtra("WEB_TITLE", bean.getType());
                if (url.startsWith("http")){
                    it.putExtra("WEB_URL", url);
                }else {
                    it.putExtra("WEB_URL", JNBaseUtilManager.getLoginServerUrl()+url);
                }
                mContext.startActivity(it);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 获取真实地址
     */
    private String getRealAddress(String address) {
        if (TextUtils.isEmpty(address))
            return "";
        if (address.toLowerCase().endsWith("userid=")){
            address += JNBasePreferenceSaves.getUserAccount();
        }else {
            if (address.contains("?")){
                if (!address.toLowerCase().endsWith("?")){
                    if (!address.toLowerCase().endsWith("&")){
                        address += "&";
                    }
                }
            }else {
                address += "?";
            }
            if(!address.toLowerCase().contains("userid=")){
                address += "userid=";
                address += JNBasePreferenceSaves.getUserAccount();
            }
        }
        return address;
    }

}

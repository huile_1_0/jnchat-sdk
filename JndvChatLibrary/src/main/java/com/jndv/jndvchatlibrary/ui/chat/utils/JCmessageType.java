package com.jndv.jndvchatlibrary.ui.chat.utils;

/**
 * Author: sunqinzheng
 * Date: 2022/2/18
 * Description: 消息的类型管理工具类
 */
public class JCmessageType {
    public static final String TEXT = "1" ;//普通文字消息
    public static final String IMG = "2" ;//普通图片消息
    public static final String VOICE = "3" ;//语音消息
    public static final String CARD = "4" ;//名片消息
    public static final String LOCATION = "5" ;//位置消息
    public static final String FILE = "6" ;//文件消息
    public static final String READACK = "7" ;//消息已读回执
    public static final String WITHDRAW = "8" ;//撤回消息
    public static final String NOTIFY = "9" ;//通知消息
    public static final String VIDEO = "10" ;//视频消息
    public static final String AUDIO_VIDEO_RECORD = "11" ;//音视频通话记录
    public static final String LOGIN_SUCCESS = "sip_login_success" ;//登录成功消息
    public static final String GPS_TRACK_START = "gps_track_start" ;//位置跟踪 开始
    public static final String GPS_TRACK_END = "gps_track_end" ;//位置跟踪 结束

    public static final String SYSTEM_EVENT = "event" ;//web消息中心-事件
    public static final String SYSTEM_ALARM = "alarm" ;//web消息中心-告警
    public static final String SYSTEM_TASK = "task" ;//web消息中心-任务管理
    public static final String SYSTEM_NOTICE = "notice" ;//web消息中心-通知公告
    public static final String SYSTEM_DISPATCH = "dispatch" ;//web消息中心-调度

    public static final String MSG_INTERCOM_308 = "308" ;//对讲，308消息，普通频道邀请

    public static final String MSG_CALL_THREE_INVITE = "601" ;//三方通话，添加人到通话
    public static final String MSG_CALL_THREE_DELETE = "602" ;//三方通话，踢人
    public static final String MSG_CALL_THREE_USER_STATE = "603" ;//三方通话，成员状态变更
    public static final String MSG_CALL_THREE_STATE = "605" ;//三方通话，设备状态变更
    public static final String MSG_CALL_THREE_ADD = "606" ;//三方通话，预警主动加入

}

package com.jndv.jndvchatlibrary.ui.location;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.jndv.jndvchatlibrary.R;

/***
 * 定位滤波demo，实际定位场景中，可能会存在很多的位置抖动，此示例展示了一种对定位结果进行的平滑优化处理
 * 实际测试下，该平滑策略在市区步行场景下，有明显平滑效果，有效减少了部分抖动，开放算法逻辑，希望能够对开发者提供帮助
 * 注意：该示例场景仅用于对定位结果优化处理的演示，里边相关的策略或算法并不一定适用于您的使用场景，请注意！！！
 *
 * @author baidu
 *
 */
public class JCLocationExtendActivity extends Activity {

    private MapView mMapView = null;
    private BaiduMap mBaiduMap;
    public final static String MSG_LOCATION_LON = "msg_location_lon";
    public final static String MSG_LOCATION_LAT = "msg_location_lat";
    private String locationLat;
    private String locationLon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jc_location_extend_activity);
        locationLat = getIntent().getStringExtra(MSG_LOCATION_LAT);
        locationLon = getIntent().getStringExtra(MSG_LOCATION_LON);
        mMapView = (MapView) findViewById(R.id.bmapView);
        mBaiduMap = mMapView.getMap();
        mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
        mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomTo(15));

        if (!TextUtils.isEmpty(locationLat) && !TextUtils.isEmpty(locationLon)) {
            LatLng point = new LatLng(Double.parseDouble(locationLat), Double.parseDouble(locationLon));
            // 构建Marker图标
            BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(R.drawable.icon_openmap_mark); // 非推算结果
            // 构建MarkerOption，用于在地图上添加Marker
            OverlayOptions option = new MarkerOptions().position(point).icon(bitmap);
            // 在地图上添加Marker，并显示
            mBaiduMap.addOverlay(option);
            mBaiduMap.setMapStatus(MapStatusUpdateFactory.newLatLng(point));
        }

//        locService = JCChatManager.locationService;
//        LocationClientOption mOption = locService.getDefaultLocationClientOption();
//        mOption.setLocationMode(LocationClientOption.LocationMode.Battery_Saving);
//        mOption.setCoorType("bd09ll");
//        locService.setLocationOption(mOption);
//        locService.registerListener(listener);
//        locService.start();
    }


    /***
     * 接收定位结果消息，并显示在地图上
     */
//    private Handler locHander = new Handler() {
//
//        @Override
//        public void handleMessage(Message msg) {
//            // TODO Auto-generated method stub
//            super.handleMessage(msg);
//            try {
//                BDLocation location = msg.getData().getParcelable("loc");
//                int iscal = msg.getData().getInt("iscalculate");
//                if (location != null) {
//                    LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
//                    // 构建Marker图标
//                    BitmapDescriptor bitmap = null;
//                    if (iscal == 0) {
//                        bitmap = BitmapDescriptorFactory.fromResource(R.drawable.icon_openmap_mark); // 非推算结果
//                    } else {
//                        bitmap = BitmapDescriptorFactory.fromResource(R.drawable.icon_openmap_focuse_mark); // 推算结果
//                    }
//
//                    // 构建MarkerOption，用于在地图上添加Marker
//                    OverlayOptions option = new MarkerOptions().position(point).icon(bitmap);
//                    // 在地图上添加Marker，并显示
//                    mBaiduMap.addOverlay(option);
//                    mBaiduMap.setMapStatus(MapStatusUpdateFactory.newLatLng(point));
//                }
//            } catch (Exception e) {
//                // TODO: handle exception
//            }
//        }
//    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mMapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // 在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        // 在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }


}

package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

public class JCScanQRCodeIntoGroupApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/group/scanQRCodeIntoGroup";
    }

    private String domainAddr;
    private String domainAddrUser;
    private String userId;
    private String groupDomainAddr;
    private String groupDomainAddrUser;
    private String nickname;
    private String pic;
    private String groupId;

    public JCScanQRCodeIntoGroupApi(String domainAddr, String domainAddrUser, String userId, String groupDomainAddr, String groupDomainAddrUser, String nickname, String pic, String groupId) {
        this.domainAddr = domainAddr;
        this.domainAddrUser = domainAddrUser;
        this.userId = userId;
        this.groupDomainAddr = groupDomainAddr;
        this.groupDomainAddrUser = groupDomainAddrUser;
        this.nickname = nickname;
        this.pic = pic;
        this.groupId = groupId;
    }

    public String getDomainAddr() {
        return domainAddr;
    }

    public void setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
    }

    public String getDomainAddrUser() {
        return domainAddrUser;
    }

    public void setDomainAddrUser(String domainAddrUser) {
        this.domainAddrUser = domainAddrUser;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupDomainAddr() {
        return groupDomainAddr;
    }

    public void setGroupDomainAddr(String groupDomainAddr) {
        this.groupDomainAddr = groupDomainAddr;
    }

    public String getGroupDomainAddrUser() {
        return groupDomainAddrUser;
    }

    public void setGroupDomainAddrUser(String groupDomainAddrUser) {
        this.groupDomainAddrUser = groupDomainAddrUser;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "JCScanQRCodeIntoGroupApi{" +
                "domainAddr='" + domainAddr + '\'' +
                ", domainAddrUser='" + domainAddrUser + '\'' +
                ", userId=" + userId +
                ", groupDomainAddr='" + groupDomainAddr + '\'' +
                ", groupDomainAddrUser='" + groupDomainAddrUser + '\'' +
                ", groupId=" + groupId +
                '}';
    }
}

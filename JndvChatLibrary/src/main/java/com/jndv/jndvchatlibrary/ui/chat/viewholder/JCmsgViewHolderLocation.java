package com.jndv.jndvchatlibrary.ui.chat.viewholder;

import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.chat.base.JCmsgViewHolderBase;
import com.jndv.jnbaseutils.chat.listUi.JCPopupMenuBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgLocationContentBean;
import com.jndv.jndvchatlibrary.ui.location.JCLocationExtendActivity;

import java.util.ArrayList;
import java.util.List;

public class JCmsgViewHolderLocation extends JCmsgViewHolderBase {

    private ImageView ivMessage;
    private TextView tv_location_address;


    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_location;
    }

    @Override
    protected void inflateContentView() {
        ivMessage = findViewById(R.id.iv_image);
        tv_location_address = findViewById(R.id.tv_location_address);
    }

    @Override
    protected void bindContentView() {
        Log.e("bindContentView",message.getContent());
        JNLogUtil.e("======Content=="+base64ToString(message.getContent()));
        try {
            JCMsgLocationContentBean bean = new Gson().fromJson(base64ToString(message.getContent()), JCMsgLocationContentBean.class);
            tv_location_address.setText(bean.getAddress());
//            tvMessage.setTextColor(Color.parseColor(bean.getFontColor()));
//            tvMessage.setTextSize(Float.parseFloat(""+bean.getFontSize()));
//            JNLogUtil.loge("======SendTime=="+message.getSendTime());
//            JNLogUtil.loge("======ReceiveTime=="+message.getReceiveTime());
//                                    tvMessage.setText("经纬度1："+message.getContent());
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            tvMessage.setText(bean.getText()+"\n"+format.format(new Date(Long.parseLong(message.getSendTime())))
//                    +"\n"+format.format(new Date(Long.parseLong(message.getReceiveTime()))));
        }catch (Exception e){
            e.printStackTrace();
            JNLogUtil.e("======getMsgType=="+message.getMsgType());
//            tvMessage.setText("未识别的消息："+message.getContent());
        }

    }

    @Override
    protected List<JCPopupMenuBean> getPopupMenuItemList() {
        List<JCPopupMenuBean> list = new ArrayList<>();
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_copy), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_transmit), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_collect), 2));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_withdraw), 1));
        list.add(new JCPopupMenuBean(context.getString(R.string.message_menu_delete), 2));
        return list;
    }

    @Override
    protected boolean isMiddleItem() {
        return false;
    }

    @Override
    protected void onItemClick() {
        try {
            String decodeBase64 = new String(Base64.decode(message.getContent(),Base64.NO_WRAP));

            JCMsgLocationContentBean bean = new Gson().fromJson(decodeBase64, JCMsgLocationContentBean.class);
            Log.e("bindContentView",bean.toString());
//            JCMsgImgContentBean bean = new Gson().fromJson(message.getContent(), JCMsgImgContentBean.class);

            Intent intent = new Intent(context,JCLocationExtendActivity.class);
//            Intent intent = new Intent(context, JCLocationSelectActivity.class);
            intent.putExtra(JCLocationExtendActivity.MSG_LOCATION_LAT, bean.getLatitude());
            intent.putExtra(JCLocationExtendActivity.MSG_LOCATION_LON, bean.getLongitude());
            context.startActivity(intent);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

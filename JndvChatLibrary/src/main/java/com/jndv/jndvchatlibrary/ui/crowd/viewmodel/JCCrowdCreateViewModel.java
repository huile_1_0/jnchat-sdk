package com.jndv.jndvchatlibrary.ui.crowd.viewmodel;

import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCAddressBookApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCCrowdCreateApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCNumbersData;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;


import java.util.List;

public class JCCrowdCreateViewModel extends ViewModel {

    private static final String TAG = "";
    private MutableLiveData<String> mText;
    private MutableLiveData<JCRespondBean<Boolean>> crowdCreateMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean<List<JCNumbersData>>> addressBookData = new MutableLiveData<>();

    private ComponentActivity activity;

    public JCCrowdCreateViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is crowd fragment");
    }

    public MutableLiveData<JCRespondBean<Boolean>> getCrowdCreateMutableLiveData() {
        return crowdCreateMutableLiveData;
    }

    public MutableLiveData<JCRespondBean<List<JCNumbersData>>> getAddressBookData() {
        return addressBookData;
    }

    public void inidata(ComponentActivity activity) {
        this.activity = activity;
    }

    public void createGroup(String name, String pic, String number, String groupType, String groupNumbers,String renzheng) {
        groupNumbers = groupNumbers.replace("\n","");
        groupNumbers = groupNumbers.replace("\t","");
        groupNumbers = groupNumbers.replace("\r","");
        JCCrowdCreateApi createApi = new JCCrowdCreateApi(JNBasePreferenceSaves.getSipAddress()
                , JNBasePreferenceSaves.getJavaAddress(), JNBasePreferenceSaves.getUserSipId(), name
                , pic, number, groupType, groupNumbers,renzheng);

        String bbbb = new Gson().toJson(createApi);
        bbbb = bbbb.replace("\n","");
        bbbb = bbbb.replace("\t","");
        bbbb = bbbb.replace("\r","");
        JNLogUtil.e("==createGroup==bbbb=="+bbbb);

        EasyHttp.post(activity)
                .api(createApi).json(bbbb)
                .request(new OnHttpListener<JCRespondBean<Boolean>>() {
                    @Override
                    public void onSucceed(JCRespondBean<Boolean> result) {
                        crowdCreateMutableLiveData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });
    }

    public void getAddressBook() {
        EasyHttp.get(activity)
                .api(new JCAddressBookApi()
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                )
                .request(new OnHttpListener<JCRespondBean<List<JCNumbersData>>>() {
                    @Override
                    public void onSucceed(JCRespondBean<List<JCNumbersData>> result) {
                        addressBookData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });
    }

}
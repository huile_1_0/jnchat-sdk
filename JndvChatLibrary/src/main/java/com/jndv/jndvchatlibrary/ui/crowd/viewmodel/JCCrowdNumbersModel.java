package com.jndv.jndvchatlibrary.ui.crowd.viewmodel;

import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCDisbandGroupApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGetGroupAllApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCGetGroupNoticeApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCQuitGroupChatApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCGroupAllBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCGroupNumbersBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;


import java.util.List;

public class JCCrowdNumbersModel extends ViewModel {

    private static final String TAG = "";
    private MutableLiveData<List<JCGroupNumbersBean>> groupAllData= new MutableLiveData<>();


    private ComponentActivity activity;

    public JCCrowdNumbersModel(ComponentActivity activity) {
        this.activity =activity;
    }

    public MutableLiveData<List<JCGroupNumbersBean>> getGroupAllData() {
        return groupAllData;
    }

    public void requestGroupAllData(String groupId,String groupDomainAddr){
        EasyHttp.get(activity)
                    .api(new JCGetGroupAllApi()
                            .setDomainAddr(JNBasePreferenceSaves.getSipAddress())
                            .setGroupId(groupId)
                            .setUserId(JNBasePreferenceSaves.getUserSipId())
                            .setGroupDomainaddr(groupDomainAddr)
                )
                .request(new OnHttpListener<JCRespondBean<JCGroupAllBean>>() {
                    @Override
                    public void onSucceed(JCRespondBean<JCGroupAllBean> result) {
                        if(result!=null&&result.getData()!=null) {
                            groupAllData.postValue(result.getData().getGroup_numbers());
                        }
                    }
                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: "+e.getMessage());
                    }
                });
    }
}
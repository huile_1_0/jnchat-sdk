package com.jndv.jndvchatlibrary.ui.chat.fragment;


import android.text.TextUtils;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.http.api.JCMyFriendApi;
import com.jndv.jndvchatlibrary.utils.JCFriendUtil;
import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/4/20
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCFriendListViewModel extends ViewModel {

    private Fragment fragment;

    private MutableLiveData<JCFriendListModelItem> friendList ;

    public JCFriendListViewModel() {
        friendList = new MutableLiveData<>();
    }

    public void init(Fragment fragment){
        this.fragment = fragment ;
    }

    public MutableLiveData<JCFriendListModelItem> getFriendList() {
        return friendList;
    }

    public void getFriendListData() {
        Log.d("zhang","getFriendListData111...");
        JCFriendUtil.getFriendListData(fragment, new OnHttpListener<JCMyFriendApi.Bean>() {
            @Override
            public void onSucceed(JCMyFriendApi.Bean result) {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                    JCFriendListModelItem jcFriendListModelItem = new JCFriendListModelItem();
                    jcFriendListModelItem.setType(0);
                    jcFriendListModelItem.setDatas(result.getData());
                    friendList.setValue(jcFriendListModelItem);
                }else {
                    JCFriendListModelItem jcFriendListModelItem = new JCFriendListModelItem();
                    jcFriendListModelItem.setType(1);
                    friendList.setValue(jcFriendListModelItem);
                }
            }

            @Override
            public void onFail(Exception e) {
                JCFriendListModelItem jcFriendListModelItem = new JCFriendListModelItem();
                jcFriendListModelItem.setType(1);
                friendList.setValue(jcFriendListModelItem);
            }
        });

    }

public class JCFriendListModelItem {

    private List<JCMyFriendApi.Bean.DataBean> datas;//新的数据列表
    private int type;//0=成功；1=失败

    public List<JCMyFriendApi.Bean.DataBean> getDatas() {
        return datas;
    }

    public void setDatas(List<JCMyFriendApi.Bean.DataBean> datas) {
        this.datas = datas;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
}

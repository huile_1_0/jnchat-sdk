package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

/**
 * 更新认证设置
 *
 */
public class JCGroupRenZhengUpdateApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/group/groupRenZhengUpdate";
    }


    /**
     *
     * domainAddr	是	string	用户所在SIP域地址
     * groupDomainAddr	是	string	群聊所在域地址
     * userId	是	number	用户SIP id
     * groupId	是	number	群聊 id
     * domainAddrUser	是	string	用户所在java域地址
     * renzheng	是	number	群认证(默认0未开启，1.开启)开启群认证后，无法直接通过邀请、扫码入群，必须经过群主或管理员审核
     *
     */


    private String domainAddr;
    private String groupDomainAddr;
    private String userId;
    private String groupId;
    private String domainAddrUser;
    private String renzheng;


    public JCGroupRenZhengUpdateApi(String domainAddr, String groupDomainAddr, String userId, String groupId, String domainAddrUser, String renzheng) {
        this.domainAddr = domainAddr;
        this.groupDomainAddr = groupDomainAddr;
        this.userId = userId;
        this.groupId = groupId;
        this.domainAddrUser = domainAddrUser;
        this.renzheng = renzheng;
    }
}

package com.jndv.jndvchatlibrary.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.ehome.manager.JNPjSip;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.eventbus.StepEvent;
import com.jndv.jndvchatlibrary.ui.chat.activity.CropActivity;
import com.kevin.crop.UCrop;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.wega.library.loadingDialog.LoadingDialog;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Author: wangguodong
 * Date: 2022/2/15
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: fragment基类
 */
public class JCbaseFragment extends Fragment implements Serializable {
    private static final String TAG = "JCbaseFragment";
    //public class JCbaseFragment extends Fragment implements Serializable {
    RxPermissions rxPermissions = null;
    private int containerId;
    public JCFragmentSelect jcFragmentSelect;
    private LoadingDialog loadingDialog = null;

    // 拍照临时图片
    private String mTempPhotoPath;
    // 剪切后图像文件
    private Uri mDestinationUri ;

    public static final int SELECTE_TYPE_MSG = 0;//消息选择（收藏消息）；
    public static final int SELECTE_TYPE_FRIEND = 1;//好友选择（名片消息）；
    public static final int SELECTE_TYPE_PIC_CROP = 2;//图片剪切
    public static interface JCFragmentSelect {
        void onSelecte(int type, Object... objects);
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        initCrop();
        super.onCreate(savedInstanceState);
    }

    /**
     * 显示加载框
     */
    public void showLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.loading();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==showLoadding==",e);
        }
    }

    /**
     * 直接取消加载框
     */
    public void cancelLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.cancel();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==cancelLoadding==",e);
        }
    }

    /**
     * 显示加载成功后取消加载框
     */
    public void cancelSuccessLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.loadSuccess();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==cancelSuccessLoadding==",e);
        }
    }

    /**
     * 显示加载失败后取消加载框
     */
    public void cancelFailLoadding(){
        try {
            if (null!=loadingDialog)loadingDialog.loadFail();
        }catch (Exception e){
            JNLogUtil.e("==JCChatManager==cancelFailLoadding==",e);
        }
    }

    public JCFragmentSelect getJcFragmentSelect() {
        return jcFragmentSelect;
    }

    public void setJcFragmentSelect(JCFragmentSelect jcFragmentSelect) {
        this.jcFragmentSelect = jcFragmentSelect;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadingDialog = new LoadingDialog(getActivity());
    }

    public void initCameraPermission(){
        rxPermissions = new RxPermissions(getActivity());
        List<String> list = new ArrayList<>();
            list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            list.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            list.add(Manifest.permission.CAMERA);
            list.add(Manifest.permission.RECORD_AUDIO);
            list.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            list.add(Manifest.permission.ACCESS_FINE_LOCATION);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P){
            list.add(Manifest.permission.ACTIVITY_RECOGNITION);
        }
//            list.add(Manifest.permission.CHANGE_WIFI_STATE);
            JNLogUtil.e("==Permission===initCameraPermission======lacksPermission()===="+lacksPermission(list));
            if (!lacksPermission(list)){
                showPermissionDialog();
            }
//        }
    }

    /**
     * 判断是否缺少权限
     */
    public boolean lacksPermission( List<String> permissions) {
        for (int i = 0; i < permissions.size(); i++) {
            JNLogUtil.e("==Permission===initCameraPermission======checkSelfPermission()===="+ContextCompat.checkSelfPermission(getActivity(), permissions.get(i)));
            if (!rxPermissions.isGranted(permissions.get(i)))return false;
        }
        return true;
    }

    String[] permissionStrs;

    BasePopupView popupView = null;
    public void showPermissionDialog(){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P){
            permissionStrs=new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE
                    , Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.CAMERA
                    , Manifest.permission.RECORD_AUDIO
                    , Manifest.permission.ACCESS_COARSE_LOCATION
                    , Manifest.permission.ACCESS_FINE_LOCATION
                    , Manifest.permission.ACTIVITY_RECOGNITION};
        }else {
            permissionStrs=new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE
                    , Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.CAMERA
                    , Manifest.permission.RECORD_AUDIO
                    , Manifest.permission.ACCESS_COARSE_LOCATION
                    , Manifest.permission.ACCESS_FINE_LOCATION
            };
        }

        popupView = new XPopup.Builder(getContext())
                    .asConfirm("权限提醒", "为了能正常通话，需要您同意相关权限！",
                            "取消", "确定",
                            new OnConfirmListener() {
                                @Override
                                public void onConfirm() {
                                    if (null != popupView) popupView.dismiss();
//                                    JNBasePreferenceSaves.saveInt(JNBasePreferenceSaves.KEY_PERMISSION_DIALOG_CAMERA, 1);
                                    rxPermissions.request(permissionStrs)
                                            .subscribe(new Consumer<Boolean>() {
                                                @SuppressLint("CheckResult")
                                                @Override
                                                public void accept(Boolean aBoolean) {
                                                    if (!aBoolean) {
                                                        if (rxPermissions.isGranted(Manifest.permission.ACCESS_COARSE_LOCATION)
                                                            || rxPermissions.isGranted(Manifest.permission.ACCESS_FINE_LOCATION)){
                                                            JNBaseUtilManager.onStartMap(JCChatManager.getApplication());
                                                        }
                                                        JCChatManager.showToast("您有权限未同意，将影响部分功能的使用！");
                                                    }else {
                                                        JNBaseUtilManager.onStartMap(JCChatManager.getApplication());
                                                        EventBus.getDefault().post(new StepEvent());
                                                    }
                                                    boolean isOpen=checkGPSIsOpen();
                                                    Log.d("zhang","GPS开关状态=="+isOpen);
                                                    if(!isOpen){
                                                        openGPSSettings();
                                                    }
                                                }
                                            });
                                }
                            }, new OnCancelListener() {
                                @Override
                                public void onCancel() {
                                    if (null != popupView) popupView.dismiss();
                                }
                            }, true);
            if (null!=popupView)popupView.show();
        }

    public int getContainerId() {
        return containerId;
    }

    public void setContainerId(int containerId) {
        this.containerId = containerId;
    }

//    public JCActivityFragmentInterface getJcActivityFragmentInterface() {
//        return jcActivityFragmentInterface;
//    }
//
//    public void setJcActivityFragmentInterface(JCActivityFragmentInterface jcActivityFragmentInterface) {
//        this.jcActivityFragmentInterface = jcActivityFragmentInterface;
//    }

    protected void showToast(String message){
        Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
    }

    private static final int GPS_REQUEST_CODE = 10;

    /**
     * 检测GPS是否打开
     *
     * @return
     */
    private boolean checkGPSIsOpen() {
        boolean isOpen;
        LocationManager locationManager = (LocationManager) getActivity()
                .getSystemService(Context.LOCATION_SERVICE);
        isOpen = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return isOpen;
    }

    /**
     * 跳转GPS设置
     */
    private void openGPSSettings() {
            //没有打开则弹出对话框
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.notifyTitle)
                    .setMessage(R.string.gpsNotifyMsg)
                    // 拒绝, 退出应用
                    .setNegativeButton(R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                    finish();
                                }
                            })

                    .setPositiveButton(R.string.setting,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //跳转GPS设置界面
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivityForResult(intent, GPS_REQUEST_CODE);
                                }
                            })

                    .setCancelable(false)
                    .show();
    }

    private void initCrop(){
        mDestinationUri = Uri.fromFile(new File(getActivity().getCacheDir(), "cropImage.jpg"));
        mTempPhotoPath = Environment.getExternalStorageDirectory() + File.separator + "photo.jpg";
        JNLogUtil.d(TAG, "initCrop: =================mDestinationUri="+mDestinationUri);
        JNLogUtil.d(TAG, "initCrop: =================mTempPhotoPath="+mTempPhotoPath);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        JNLogUtil.d(TAG, "onActivityResult: =================requestCode="+requestCode);
        JNLogUtil.d(TAG, "onActivityResult: =================resultCode="+resultCode);
        onFragmentResult(requestCode, resultCode, data);
    }

    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        Log.d("zhang","onFragmentResult...resultCode=="+resultCode);
        if (resultCode == getActivity().RESULT_OK){
            switch (requestCode){
                case UCrop.REQUEST_CROP:    // 裁剪图片结果
                    handleCropResult(data);
                    break;
                case UCrop.RESULT_ERROR:    // 裁剪图片错误
                    handleCropError(data);
                    break;
            }
        }else {
            if(requestCode==GPS_REQUEST_CODE){
                Log.d("zhang","onGpsResult...");
                //打开获取位置信息页面后，这时进程会重启，表现sip断开，所以这时候需要重新注册sip
                JNPjSip.getInstance(getActivity()).sipRegister();
            }else {
                handleCropError(data);
            }
        }
    }

    /**
     * 处理剪切成功的返回值
     *
     * @param result
     */
    private void handleCropResult(Intent result) {
        try {
            deleteTempPhotoFile();
            final Uri resultUri = UCrop.getOutput(result);
            JNLogUtil.d(TAG, "handleCropResult: ===============resultUri="+resultUri);
            JNLogUtil.d(TAG, "handleCropResult: ===============mOnPictureSelectedListener="+jcFragmentSelect);
            if (null != resultUri && null != jcFragmentSelect) {
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultUri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                jcFragmentSelect.onSelecte(SELECTE_TYPE_PIC_CROP, resultUri, bitmap);
            } else {
                Toast.makeText(getActivity(), "无图片", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            JNLogUtil.e("===========", e);
        }

    }

    /**
     * 处理剪切失败的返回值
     *
     * @param result
     */
    private void handleCropError(Intent result) {
        deleteTempPhotoFile();
        if (null==result){
            Toast.makeText(getActivity(), "无法剪切选择图片!", Toast.LENGTH_SHORT).show();
        }else {
            try {
                final Throwable cropError = UCrop.getError(result);
                if (cropError != null) {
//            Log.e(TAG, "handleCropError: ", cropError);
                    Toast.makeText(getActivity(), cropError.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "无法剪切选择图片!", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                Toast.makeText(getActivity(), "无法剪切选择图片!", Toast.LENGTH_SHORT).show();
                JNLogUtil.e("=======", e);
            }
        }
    }
    /**
     * 删除拍照临时文件
     */
    private void deleteTempPhotoFile() {
        File tempFile = new File(mTempPhotoPath);
        if (tempFile.exists() && tempFile.isFile()) {
            tempFile.delete();
        }
    }

    /**
     * 裁剪图片方法实现
     *
     * @param uri
     */
    public void startCropActivity(Uri uri, int X, int Y, int MaxX, int MaxY) {
        try {
            mDestinationUri = Uri.fromFile(new File(getActivity().getCacheDir(), "cropImage"+System.currentTimeMillis()+".jpg"));
            UCrop.of(uri, mDestinationUri)
                    .withAspectRatio(X, Y)
                    .withMaxResultSize(MaxX, MaxY)
                    .withTargetActivity(CropActivity.class)
                    .start(getActivity(), this);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}

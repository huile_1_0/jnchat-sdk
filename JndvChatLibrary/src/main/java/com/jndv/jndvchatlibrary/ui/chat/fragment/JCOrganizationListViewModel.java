package com.jndv.jndvchatlibrary.ui.chat.fragment;


import android.text.TextUtils;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNGetDeptSons;
import com.jndv.jnbaseutils.utils.JNBaseConstans;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/4/20
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCOrganizationListViewModel extends ViewModel {

    private Fragment fragment;

    private MutableLiveData<JCOrganzationListModelItem> organizationList ;

    public JCOrganizationListViewModel() {
        organizationList = new MutableLiveData<>();
    }

    public void init(Fragment fragment){
        this.fragment = fragment ;
    }

    public MutableLiveData<JCOrganzationListModelItem> getOrganizationList() {
        return organizationList;
    }

    public void getOrganzationListData(String zoningCode) {
        JNBaseHttpUtils.getInstance().GetDeptSons(EasyHttp.post(fragment),"",zoningCode,null, new OnHttpListener<JNGetDeptSons.Bean>() {
            @Override
            public void onSucceed(JNGetDeptSons.Bean result) {
                if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                    JCOrganzationListModelItem jcOrganzationListModelItem = new JCOrganzationListModelItem();
                    jcOrganzationListModelItem.setType(0);
                    jcOrganzationListModelItem.setData(result);
//                    jcOrganzationListModelItem.setNid(nid);
                    organizationList.setValue(jcOrganzationListModelItem);
                }else {
                    JCOrganzationListModelItem jcOrganzationListModelItem = new JCOrganzationListModelItem();
                    jcOrganzationListModelItem.setType(1);
                    organizationList.setValue(jcOrganzationListModelItem);
                }
            }

            @Override
            public void onFail(Exception e) {
                JCOrganzationListModelItem jcOrganzationListModelItem = new JCOrganzationListModelItem();
                jcOrganzationListModelItem.setType(1);
                organizationList.setValue(jcOrganzationListModelItem);
            }
        });
    }

public class JCOrganzationListModelItem {

    private JNGetDeptSons.Bean data;//新的数据列表
    private int type;//0=成功；1=失败
//    private int nid;//-1=首次获取；

    public JNGetDeptSons.Bean getData() {
        return data;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setData(JNGetDeptSons.Bean data) {
        this.data = data;
    }

//    public int getNid() {
//        return nid;
//    }
//
//    public void setNid(int nid) {
//        this.nid = nid;
//    }
}
}

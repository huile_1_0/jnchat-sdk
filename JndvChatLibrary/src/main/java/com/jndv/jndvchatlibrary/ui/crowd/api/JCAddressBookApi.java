package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCNumbersData;

import java.util.List;

public class JCAddressBookApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/address-book/getAddressBook";
    }

    private String userId;

    public JCAddressBookApi setUserId(String userId) {
        this.userId = userId;
        return this;
    }

}

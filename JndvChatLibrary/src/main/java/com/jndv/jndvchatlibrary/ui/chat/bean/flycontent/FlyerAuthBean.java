package com.jndv.jndvchatlibrary.ui.chat.bean.flycontent;

public class FlyerAuthBean {
    private Header header;
    private Data data = new Data();
    public void setHeader(Header header) {
        this.header = header;
    }
    public Header getHeader() {
        return header;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public Data getData() {
        return data;
    }

    public class Header {

        private String msg_id ;
        private String msg_no;
        private String res;
        private String des;
        private String timestamp;
        public void setMsg_id (String msg_id ) {
            this.msg_id  = msg_id ;
        }
        public String getMsg_id () {
            return msg_id ;
        }

        public void setMsg_no(String msg_no) {
            this.msg_no = msg_no;
        }
        public String getMsg_no() {
            return msg_no;
        }

        public void setRes(String res) {
            this.res = res;
        }
        public String getRes() {
            return res;
        }

        public void setDes(String des) {
            this.des = des;
        }
        public String getDes() {
            return des;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
        public String getTimestamp() {
            return timestamp;
        }

    }

    public class Data {

        private String sip;
        private String password;
        public void setSip(String sip) {
            this.sip = sip;
        }
        public String getSip() {
            return sip;
        }

        public void setPassword(String password) {
            this.password = password;
        }
        public String getPassword() {
            return password;
        }

    }
}

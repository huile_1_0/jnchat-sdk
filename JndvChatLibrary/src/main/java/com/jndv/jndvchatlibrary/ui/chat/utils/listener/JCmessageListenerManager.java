package com.jndv.jndvchatlibrary.ui.chat.utils.listener;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgGpsSendTimeContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCMessageHintUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;
import com.jndv.jndvchatlibrary.utils.JCMessageUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/3/25
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 消息监听管理工具
 */
public class JCmessageListenerManager {

    /**
     * String 为sessionid+“_”+sessionType
     */
    static HashMap<String, JCMessageListener> map = new HashMap<>();

    /**
     * 添加某一会话的消息监听
     * key规则："对方id"+“_”+"会话类型"+"_"+"会话sip域地址"
     * @param key
     * @param listener
     */
    public static void addMessageListener(String key, JCMessageListener listener){
        map.put(key, listener);
    }
    public static void removeMessageListener(String key){
        map.remove(key);
    }
    public static HashMap<String, JCMessageListener> getMessageListener(){
        return map;
    }

    private static List<JCMessageListener> list = new ArrayList();
    public static void addMessageListenerAll(JCMessageListener listener){
        list.add(listener);
    }
    public static void removeMessageListenerAll(JCMessageListener listener){
        list.remove(listener);
    }
    public static void onReceiveMessageMy(JCbaseIMMessage message, int isLook){
        JNLogUtil.d("JCmessageListenerManager", "onReceiveMessageMy====creatSession: "+new Gson().toJson(message));
        try {
            if (!TextUtils.equals(""+ JCmessageType.READACK, message.getMsgType()))
                for (int i = 0; i < list.size(); i++) {
                    JNLogUtil.d("JCmessageListenerManager", "001====creatSession: ");
                    list.get(i).onReceiveMessage(message, isLook);
                }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 接收到消息
     * key规则："对方id"+“_”+"会话类型"+"_"+"会话sip域地址"
     * @param key
     * @param message
     */
    public static void onReceiveMessage(String key, JCbaseIMMessage message){
        JNLogUtil.d("JCmessageListenerManager", "onReceiveMessage111====msg: "+message.getContent());
        boolean isConsume = false;//是否消费，如果被消费那么就不再发送给接下来的监听（会话列表中总监听、聊天页面监听）
        JCSessionListBean sessionBean = null;
        try {
//            在这里进行已读回执的统一处理，防止错乱
            if (TextUtils.equals(""+ JCmessageType.READACK, message.getMsgType())) {
                JCMessageUtils.initReadMsgNum(message);
            }

            try {
                if (TextUtils.equals(""+ JCmessageType.GPS_TRACK_START, message.getMsgType())) {
                    int num = 1000;
                    try {
                        JCMsgGpsSendTimeContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(message.getContent())
                                , JCMsgGpsSendTimeContentBean.class);
                        num = Integer.parseInt(bean.getTime());
                    }catch (Exception e){
                        JNLogUtil.e("=GPS_TRACK_START==num==", e);
                    }
                    JNBaseUtilManager.setGpsFrequency(num);
                }
                if (TextUtils.equals(""+ JCmessageType.GPS_TRACK_END, message.getMsgType())) {
                    JNBaseUtilManager.recoverGpsFrequency();
                }
            }catch (Exception e){
                JNLogUtil.e("====", e);
            }
            if (TextUtils.equals(""+ JCmessageType.NOTIFY, message.getMsgType())) {
                isConsume = JCMessageUtils.initNotifyMsg(message);
            }
            String [] keystr = key.split("_");

            Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++keystr[0]=" +keystr[0]);
            Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++keystr[1]=" +keystr[1]);
            Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
            QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
            sessionBean = builder.equal(JCSessionListBean_.other_id, keystr[0], QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.user_id, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.session_party_domain_addr, keystr[2],QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.type, keystr[1],QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();
            Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++111=" );
            if (null==sessionBean||0==sessionBean.getIs_no_disturb()){
//                Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++112=" +sessionBean.getIs_no_disturb());
//                已读回执消息不进行提醒、对讲消息不进行消息提醒
                if (!isConsume &&!TextUtils.equals(""+ JCmessageType.READACK, message.getMsgType())
                        && !TextUtils.equals(JCSessionType.SESSION_CLUSTER_INTERCOM, message.getSessionType())) {
                    JNLogUtil.e("==JCmessageListenerManager==onReceiveMessage==showNotification==01==");
                    JCMessageHintUtils.playMsgHint();
                }
            }
        }catch (Exception e){
            Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++113=" +e.getMessage());
            e.printStackTrace();
        }
//1050_1_imapi.jndv.com:5080
        Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++key=" + key);
        //1、这里要收到消息就增加会话列表未读消息数量，聊天页面，收到消息也要更新会话列表
        // 2、区分是否需要增加会话列表未读消息数量，没有聊天页面监听消息时增加
        JCMessageListener listener = map.get(key);
        if (null!=listener){
            Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++01=" +isConsume);
            if (!isConsume)onReceiveMessageMy(message, 1);
            if (!isConsume){
                JNLogUtil.d("JCmessageListenerManager", "onReceiveMessage111====msg: "+message.getContent());
                listener.onReceiveMessage(message, 1);
            }
        }else {//没有消息监听页面，需要通知消息，
            Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++02=isConsume"+isConsume);
            if (!isConsume)onReceiveMessageMy(message, 0);
            if (null!=sessionBean&&0==sessionBean.getIs_no_disturb()){
                Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++02-1=" +sessionBean.getIs_no_disturb());
                if (!isConsume&&!TextUtils.equals(""+ JCmessageType.READACK, message.getMsgType())) {
                    JNLogUtil.e("==JCmessageListenerManager==onReceiveMessage==showNotification=");
//                    JCNotificationUtil.showNotification(message);
                }
            }
        }
    }

    public static void onReceiveCreateMeetingMsg(String key, JCbaseIMMessage message){
        JCMessageListener listener = map.get(key);
        listener.onReceiveMessage(message, 1);
    }

    /**
     * 接收到消息
     * key规则："对方id"+“_”+"会话类型"+"_"+"会话sip域地址"
     * @param key
     * @param message
     */
    public static void onReceiveFlightMessage(String key, JCbaseIMMessage message){
        Log.d("linyi","onReceiveFlightMessage");
        boolean isConsume = false;//是否消费，如果被消费那么就不再发送给接下来的监听（会话列表中总监听、聊天页面监听）
        JCSessionListBean sessionBean = null;
//        JCimMessageBean jCimMessageBean = (JCimMessageBean) message;
//        JNLogUtil.e("JCchatFragment", "==00000=getId=" + jCimMessageBean.getId());

        Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++key=" + key);
        for (JCMessageListener jCMessageListener : map.values()) {
            if (null!=jCMessageListener){
                Log.e("JCchatFragment", "JCmessageListenerManager===onReceiveMessage++01=" +isConsume);
                if (!isConsume)onReceiveMessageMy(message, 1);
                if (!isConsume)jCMessageListener.onReceiveMessage(message, 1);
            }
        }

    }

    public static void onSendMessageState(String key, JCbaseIMMessage message, int state){
        JNLogUtil.e("JCmessageListenerManager===onSendMessageState++key=" +key);
        try {
            if (TextUtils.equals("" + JCmessageType.READACK, message.getMsgType())) {
                JNLogUtil.e("===onSendMessageState====JCmessageType.READACK=======");
                return;
            }
            QueryBuilder<JCimMessageBean> builder = JCobjectBox.get().boxFor(JCimMessageBean.class).query();
            JCimMessageBean localMessage = builder.equal(JCimMessageBean_.msgID, message.getMsgID(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();
            JCSessionListBean sessionListBean = JCSessionUtil.getJCSessionListBean(message.getSessionId(),message.getSessionType(),message.getToRealm());

            JNLogUtil.e("JCmessageListenerManager==01==onSendMessageState++key=" +key);
            if (null != localMessage) {
                localMessage.setStatus("" + state);
                JCobjectBox.get().boxFor(JCimMessageBean.class).put(localMessage);

                JNLogUtil.e("JCmessageListenerManager==02==onSendMessageState++key=" +key);
                if (null!=sessionListBean)EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_SESSION_LIST_UPDATE_MSG,  ""+sessionListBean.getSession_id(), localMessage));
            } else {

                JNLogUtil.e("JCmessageListenerManager==03==onSendMessageState++key=" +key);
                if (null!=sessionListBean)EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_SESSION_LIST_UPDATE_MSG,  ""+sessionListBean.getSession_id(), message));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        JCMessageListener listener = map.get(key);
        if (null!=listener){
            JNLogUtil.e("JCmessageListenerManager===onSendMessageState++01=" );
            listener.onSendMessageState(message, state);
        }
    }

}

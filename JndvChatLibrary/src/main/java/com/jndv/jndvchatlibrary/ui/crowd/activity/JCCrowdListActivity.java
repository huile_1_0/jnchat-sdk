package com.jndv.jndvchatlibrary.ui.crowd.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.ActivityJcCrowdListBinding;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCChatActivity;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCCreateSessionUtils;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jnbaseutils.ui.JNSimpleDividerDecoration;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserGroupSessionBean;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCCrowdListModel;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;
import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * 群列表页面
 */
public class JCCrowdListActivity extends JCBaseHeadActivity {
    private JCCrowdListModel listModel;
    private ActivityJcCrowdListBinding listBinding;
    private CommonAdapter adapter;
    private List<JCUserGroupSessionBean> datas = new ArrayList<>();
    private List<JCUserGroupSessionBean> datasZC = new ArrayList<>();
    private List<JCUserGroupSessionBean> datasNX = new ArrayList<>();

    private int state = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listBinding = ActivityJcCrowdListBinding.inflate(getLayoutInflater(), findViewById(R.id.base_content), true);
        initView();
        initData();
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, JCCrowdListActivity.class);
        context.startActivity(intent);
    }

    private void initView() {
        setCenterTitle("群组");
        listBinding.rvCrowdList.setLayoutManager(new LinearLayoutManager(this));
        JNSimpleDividerDecoration divider = new JNSimpleDividerDecoration(this);
        divider.setPadding(30, 20);
        listBinding.rvCrowdList.addItemDecoration(divider);
        adapter = new CommonAdapter<JCUserGroupSessionBean>(this, R.layout.jc_item_friend, datas) {
            @Override
            protected void convert(ViewHolder holder, JCUserGroupSessionBean adapterDataBean, int position) {
                JCglideUtils.loadCircleImage(mContext, adapterDataBean.getSession_party_pic(), holder.getView(R.id.iv_user_head));
                ((TextView) holder.getView(R.id.tv_user_name)).setText(adapterDataBean.getSession_party_name());
                holder.setOnClickListener(holder.itemView.getId(), v -> {
                    try {
                        JCSessionListBean messageBean = JCSessionUtil.getJCSessionListBean(
                                adapterDataBean.getSession_party_id(), JCSessionType.CHAT_GROUP, adapterDataBean.getSession_party_domain_addr());
                        Log.e("TAG", "onClick: =========001=========");
                        if (null!=messageBean){
                            Log.e("zhang", "onClick: =========002=========");
                            messageBean.setStatus(adapterDataBean.getStatus());
                            JCChatActivity.start(JCCrowdListActivity.this, messageBean);
                        }else {
                            Log.e("zhang", "onClick: =========003=========");
                            JCCreateSessionUtils.creatSession(JCCrowdListActivity.this, ""+adapterDataBean.getSession_party_id()
                                    , adapterDataBean.getSession_party_domain_addr(), adapterDataBean.getSession_party_domain_addr_user()
                                    , "" + JCSessionType.CHAT_GROUP
                                    , adapterDataBean.getSession_party_name(), adapterDataBean.getSession_party_pic(), true);
                        }
                    }catch (Exception e){
                        JNLogUtil.e("=======",e);
                    }
                });
            }
        };
        listBinding.rvCrowdList.setAdapter(adapter);
        listModel = new JCCrowdListModel(this);
        listModel.getUserGroupSessionData().observe(this, this::initDataA);
        listBinding.tvZhengchang.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                state = 0;
                datas.clear();
                datas.addAll(datasZC);
                adapter.notifyDataSetChanged();
                listBinding.tvZhengchang.setTextColor(getResources().getColor(R.color.colorPrimaryChat));
                listBinding.tvExit.setTextColor(getResources().getColor(R.color.flag_text));
            }
        });
        listBinding.tvExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state = 1;
                datas.clear();
                datas.addAll(datasNX);
                adapter.notifyDataSetChanged();
                listBinding.tvZhengchang.setTextColor(getResources().getColor(R.color.flag_text));
                listBinding.tvExit.setTextColor(getResources().getColor(R.color.colorPrimaryChat));
            }
        });
    }

    private void initDataA(List<JCUserGroupSessionBean> jcUserGroupSessionBeans){
        datasZC.clear();
        datasNX.clear();
        if (null!=jcUserGroupSessionBeans&&jcUserGroupSessionBeans.size()>0){
            for (int i = 0; i < jcUserGroupSessionBeans.size(); i++) {
                if (jcUserGroupSessionBeans.get(i).getStatus() == 0){
                    datasZC.add(jcUserGroupSessionBeans.get(i));
                }
                if (jcUserGroupSessionBeans.get(i).getStatus() == 1
                        ||jcUserGroupSessionBeans.get(i).getStatus() == 2
                        ||jcUserGroupSessionBeans.get(i).getStatus() == 3
                ){
                    datasNX.add(jcUserGroupSessionBeans.get(i));
                }
            }
            if (state==0){
                datas.clear();
                datas.addAll(datasZC);
                adapter.notifyDataSetChanged();
            }else {
                datas.clear();
                datas.addAll(datasNX);
                adapter.notifyDataSetChanged();
            }
        }else {
            datasZC.clear();
            datas.clear();
            datasNX.clear();
            datas.addAll(jcUserGroupSessionBeans);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initData() {
        listModel.requestUserGroupSessionAllData();
    }
}

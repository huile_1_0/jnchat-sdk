package com.jndv.jndvchatlibrary.ui.crowd.ui;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.chat.JCSessionListBean;

import java.util.List;

public class TransmitSelectAdapter extends BaseAdapter {
    private Context mContext;
    private List<JCSessionListBean> mList;

    private boolean display;

    public TransmitSelectAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public List<JCSessionListBean> getmList() {
        return mList;
    }

    public void setmList(List<JCSessionListBean> mList) {
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.group_chat_create_list_item, parent, false);

            viewHolder.imageView = convertView.findViewById(R.id.group_chat_create_list_item_icon);
            viewHolder.textView = convertView.findViewById(R.id.group_chat_create_list_item_name);
            viewHolder.checkBox = convertView.findViewById(R.id.group_chat_create_list_item_cb);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.checkBox.setVisibility(display ? View.VISIBLE : View.GONE);
        viewHolder.checkBox.setChecked(!TextUtils.isEmpty(mList.get(position).getRemark()));
        if (mList.get(position).getOther_name().isEmpty()) {
            viewHolder.textView.setText(String.valueOf(mList.get(position).getOther_id()));
        } else {
            viewHolder.textView.setText(mList.get(position).getOther_name());
        }

        return convertView;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView textView;
        CheckBox checkBox;

    }
}


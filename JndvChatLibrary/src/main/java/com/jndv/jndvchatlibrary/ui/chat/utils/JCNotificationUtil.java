package com.jndv.jndvchatlibrary.ui.chat.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.activity.JCChatActivity;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCMsgNotificationConfig;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgTextContentBean;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCMsgNotifyListFragment;
import com.jndv.jnbaseutils.chat.JCSessionType;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.QueryBuilder;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Author: wangguodong
 * Date: 2022/5/5
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: Notification通知栏显示工具类
 */
public class JCNotificationUtil {

//    public static void showNotification(int iconId, String title, String content, PendingIntent pendingIntent){
//        Notification notification = new NotificationCompat.Builder(JCchatManager.getApplication())
//                /**设置通知左边的大图标**/
//                .setLargeIcon(BitmapFactory.decodeResource(JCchatManager.getApplication().getResources(), R.mipmap.ic_launcher))
//                /**设置通知右边的小图标**/
//                .setSmallIcon(R.mipmap.ic_launcher)
//                /**通知首次出现在通知栏，带上升动画效果的**/
//                .setTicker("有新消息来啦")
//                /**设置通知的标题**/
//                .setContentTitle("这是一个通知的标题")
//                /**设置通知的内容**/
//                .setContentText("这是一个通知的内容这是一个通知的内容")
//                /**通知产生的时间，会在通知信息里显示**/
//                .setWhen(System.currentTimeMillis())
//                /**设置该通知优先级**/
//                .setPriority(Notification.PRIORITY_DEFAULT)
//                /**设置这个标志当用户单击面板就可以让通知将自动取消**/
//                .setAutoCancel(true)
//                /**设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)**/
//                .setOngoing(false)
//                /**向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：**/
//                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
//                .setContentIntent(PendingIntent.getActivity(JCchatManager.getApplication(), 1
//                        , new Intent(JCchatManager.getApplication(), MainActivity.class), PendingIntent.FLAG_CANCEL_CURRENT))
//                .build();
//        NotificationManager notificationManager = (NotificationManager) JCchatManager.getApplication().getSystemService(NOTIFICATION_SERVICE);
//        /**发起通知**/
//        notificationManager.notify(0, notification);
//    }
private static String getMessage(JCbaseIMMessage jCbaseIMMessage){
    String msg = "";
    try {
//        String message = list.get(pot).getFirst_content();
//        JCimMessageBean jCimMessageBean = new Gson().fromJson(message, JCimMessageBean.class);
        if (TextUtils.equals(""+JCSessionType.CHAT_PERSION,jCbaseIMMessage.getSessionType()) ){
            //私聊
            msg = getMsgStr(jCbaseIMMessage);
        }else {
            //群聊
            msg = jCbaseIMMessage.getFromName()+"："+getMsgStr(jCbaseIMMessage);
        }
    }catch (Exception e){
        e.printStackTrace();
    }
    if (TextUtils.isEmpty(msg)){
        msg = "您有一条新消息！";
    }
    return msg;
}
    private static String getMsgStr(JCbaseIMMessage jCimMessageBean){
        String info = "";
        try {
            if (null!=jCimMessageBean){
                if (TextUtils.equals(jCimMessageBean.getMsgType(), ""+JCmessageType.TEXT)){
                    JCMsgTextContentBean jcMsgTextContentBean = new Gson().fromJson(jCimMessageBean.getContent(), JCMsgTextContentBean.class);
                    info = jcMsgTextContentBean.getText();
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), ""+JCmessageType.IMG)){
                    info = "[图片]";
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), ""+JCmessageType.AUDIO_VIDEO_RECORD)){
                    info = "[音视频通话记录]";
                }else if (TextUtils.equals(jCimMessageBean.getMsgType(), ""+JCmessageType.LOCATION)){
                    info = "[位置消息]";
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return info;
    }
    public static void showNotification(JCbaseIMMessage jCbaseIMMessage){
        JNLogUtil.e("==JCNotificationUtil==showNotification=="+jCbaseIMMessage.toString());
        String title = "";
        JCSessionListBean sessionBean = null;
        PendingIntent pendingIntent = null;
        int iconId = 0 ;
        try {
            JNLogUtil.e("==JCNotificationUtil==showNotification====001==");
            BoxStore boxStore = JCobjectBox.get();
            Box<JCSessionListBean> imMsgBox = boxStore.boxFor(JCSessionListBean.class);
            QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
            JNLogUtil.e("==JCNotificationUtil==showNotification====002==");
            sessionBean = builder.equal(JCSessionListBean_.other_id, jCbaseIMMessage.getSessionId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.user_id, JNBasePreferenceSaves.getUserSipId(), QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.session_party_domain_addr, jCbaseIMMessage.getFromRealm(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .equal(JCSessionListBean_.type, jCbaseIMMessage.getSessionType(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                    .build().findFirst();
            JNLogUtil.e("==JCNotificationUtil==showNotification====003==");
            //TODO 消息通知栏通知，在这里被截断了，等后期通知流程确定好了再处理吧
            JCChatManager.initMsgNotification(R.drawable.logo,"通知消息",pendingIntent);
        }catch (Exception e){
            JNLogUtil.e("==JCNotificationUtil==showNotification====004==", e);
        }
        JCMsgNotificationConfig jcMsgNotificationConfig = JCChatManager.getJcMsgNotificationConfig();
        JNLogUtil.e("==JCNotificationUtil==showNotification====005==");
        if (null!=jcMsgNotificationConfig){
            JNLogUtil.e("==JCNotificationUtil==showNotification====006==");
            if (TextUtils.isEmpty(title)){
                title = jcMsgNotificationConfig.getTitle();
            }
            if (TextUtils.isEmpty(title)){
                title = "新消息";
            }
            JNLogUtil.e("==JCNotificationUtil==showNotification====007==");
            if (null!=jcMsgNotificationConfig.getPendingIntent()){
                pendingIntent = jcMsgNotificationConfig.getPendingIntent();
                iconId = jcMsgNotificationConfig.getIconId();
            }else if (null!=jcMsgNotificationConfig.getIntent()){
                iconId = jcMsgNotificationConfig.getIconId();
                Intent intent = jcMsgNotificationConfig.getIntent();
                intent.putExtra(JCChatActivity.KEY_JCSESSIONLISTBEAN, sessionBean);
                pendingIntent = PendingIntent.getActivity(JCChatManager.getApplication(), 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            }else{
                iconId = jcMsgNotificationConfig.getIconId();
//                Intent intent = new Intent(JCChatManager.getApplication(), JCChatActivity.class);
//                intent.putExtra(JCChatActivity.KEY_JCSESSIONLISTBEAN, sessionBean);
//                pendingIntent = PendingIntent.getActivity(JCChatManager.getApplication(), 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);

                Intent intent = new Intent(JCChatManager.getApplication(), JCbaseFragmentActivity.class);
                intent.putExtra("mJCbaseFragment", new JCMsgNotifyListFragment());
                pendingIntent = PendingIntent.getActivity(JCChatManager.getApplication(), 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            }
            JNLogUtil.e("==JCNotificationUtil==showNotification====008==");
        }else {
            try {
                JNLogUtil.e("==JCNotificationUtil==showNotification====009==");
              // iconId = R.drawable.logo;
                Intent intent = new Intent(JCChatManager.getApplication(), JCChatActivity.class);
                intent.putExtra(JCChatActivity.KEY_JCSESSIONLISTBEAN, sessionBean);
                pendingIntent = PendingIntent.getActivity(JCChatManager.getApplication(), 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                JNLogUtil.e("==JCNotificationUtil==showNotification====010==");
//                return;
            }catch (Exception e){
                e.printStackTrace();
            }
            JNLogUtil.e("==JCNotificationUtil==showNotification====011==");
           JCChatManager.showToast("消息通知未进行配置！");
        }
        JNLogUtil.e("==JCNotificationUtil==showNotification==02==");
        showNotification(iconId, title, getMessage(jCbaseIMMessage),pendingIntent);
    }
    public static void showNotification(int iconId, String title, String content, PendingIntent pendingIntent){
        JNLogUtil.e("==JCNotificationUtil==showNotification==03==");
        Notification notification = new NotificationCompat.Builder(JCChatManager.getApplication())
                /**设置通知左边的大图标**/
                .setLargeIcon(BitmapFactory.decodeResource(JCChatManager.getApplication().getResources(), iconId))
                /**设置通知右边的小图标**/
                .setSmallIcon(iconId)
                /**通知首次出现在通知栏，带上升动画效果的**/
                .setTicker("有新消息来啦")
                /**设置通知的标题**/
                .setContentTitle(title)
                /**设置通知的内容**/
                .setContentText(content)
                /**通知产生的时间，会在通知信息里显示**/
                .setWhen(System.currentTimeMillis())
                /**设置该通知优先级**/
                .setPriority(Notification.PRIORITY_DEFAULT)
                /**设置这个标志当用户单击面板就可以让通知将自动取消**/
                .setAutoCancel(true)
                /**设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)**/
                .setOngoing(false)
                /**向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：**/
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setContentIntent(pendingIntent)
                .build();
        JNLogUtil.e("==JCNotificationUtil==showNotification==04==");
        NotificationManager notificationManager = (NotificationManager) JCChatManager.getApplication().getSystemService(NOTIFICATION_SERVICE);
        JNLogUtil.e("==JCNotificationUtil==showNotification==05==");
        /**发起通知**/
        notificationManager.notify(0, notification);
    }

}

package com.jndv.jndvchatlibrary.ui.crowd.entity;


import android.util.Log;


import com.jndv.jndvchatlibrary.ui.crowd.utils.DateUtil;
import com.jndv.jndvchatlibrary.ui.crowd.utils.LogUtil;
import com.jndv.jndvchatlibrary.ui.crowd.utils.PinYinUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Group information
 *
 * @author 28851274
 */
public abstract class JCGroup implements Comparable<JCGroup> {

    protected long mGId;
    protected int mGroupType;
    protected String mName;

    protected long mOwner;
    protected JCUser mOwnerJCUser;
    protected Date mCreateDate;
    protected JCGroup mParent;
    protected List<JCGroup> mChild;
    protected Set<JCUser> JCUsers;
    protected int level;
    private Object mLock = new Object();

    public JCGroup() {
    }

    /**
     * @param gId
     * @param groupType
     * @param name
     * @param owner
     * @param createDate
     */
    protected JCGroup(long gId, int groupType, String name, JCUser owner,
                      Date createDate) {

        this.mGId = gId;
        this.mGroupType = groupType;
        this.mName = name;
        this.mOwnerJCUser = owner;
        this.mCreateDate = createDate;
        JCUsers = new CopyOnWriteArraySet<JCUser>();
        mChild = new CopyOnWriteArrayList<JCGroup>();
        level = 1;

    }

    /**
     * @param gId
     * @param groupType
     * @param name
     * @param owner
     */
    protected JCGroup(long gId, int groupType, String name, JCUser owner) {
        this(gId, groupType, name, owner, new Date());

    }

    public long getmGId() {
        return mGId;
    }

    public void setGId(long mGId) {
        this.mGId = mGId;
    }

    public int getGroupType() {
        return mGroupType;
    }

    public void setGroupType(int mGroupType) {
        this.mGroupType = mGroupType;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public Date getCreateDate() {
        return mCreateDate;
    }

    public String getStrCreateDate() {
        if (mCreateDate != null) {
            return DateUtil.getStringDate(mCreateDate.getTime());
        } else {
            return null;
        }
    }

    public void setCreateDate(Date createDate) {
        this.mCreateDate = createDate;
    }

    public JCUser getOwnerUser() {
        return mOwnerJCUser;
    }

    public void setOwnerUser(JCUser mOwnerJCUser) {
        this.mOwnerJCUser = mOwnerJCUser;
    }

    public void setmOwner(long mOwner) {
        this.mOwner = mOwner;
    }

    public long getmOwner() {
        return mOwner;
    }

    public JCGroup getParent() {
        return mParent;
    }

    public void setmParent(JCGroup parent) {
        this.mParent = parent;
        level = this.getParent().getLevel() + 1;
    }

    public void addUserToGroup(Collection<JCUser> u) {
        if (u == null) {
            LogUtil.e(" Invalid user data");
            return;
        }
        synchronized (mLock) {
            this.JCUsers.addAll(u);
        }
    }

    public void addUserToGroup(JCUser u) {
        if (u == null) {
            LogUtil.e(" addUserToGroup --> Invalid user data");
            return;
        }
        synchronized (mLock) {
            this.JCUsers.add(u);
            u.addUserToGroup(this);
        }
    }

    public void removeUserFromGroup(JCUser u) {
        if (u == null) {
            LogUtil.e(" removeUserFromGroup --> Invalid user data");
            return;
        }
        synchronized (mLock) {
            this.JCUsers.remove(u);
            u.removeUserFromGroup(this);
        }
    }

    public void removeUserFromGroup(long uid) {
        synchronized (mLock) {
            // User object use id as identification
            JCUser tmpJCUser = new JCUser(uid);
            JCUsers.remove(tmpJCUser);
        }
    }

    /**
     * return copy collection
     *
     * @return
     */
    public List<JCUser> getJCUsers() {
        return new ArrayList<JCUser>(this.JCUsers);
    }

    public List<JCGroup> getChildGroup() {
        return this.mChild;
    }

    /**
     * Get sub group and user sum
     *
     * @return
     */
    public int getSubSize() {
        return this.mChild.size() + this.JCUsers.size();
    }

    public void addGroupToGroup(JCGroup g) {
        if (g == null) {
            LogUtil.e(" Invalid group data");
            return;
        }
        synchronized (mLock) {
            this.mChild.add(g);
            g.setmParent(this);
        }
    }

    /**
     * Find use in current group and childs group.<br>
     * If find and return first belongs group.
     *
     * @param u
     * @return
     */
    public JCGroup findUser(JCUser u) {
        if (u == null) {
            return null;
        }
        return internalSearchUser(null, u.getmUserId());
    }

    /**
     * Find use in current group and childs group.<br>
     * If find and return first belongs group.
     *
     * @param
     * @return
     */
    public JCGroup findUser(long userID) {
        return internalSearchUser(null, userID);
    }

    public JCGroup internalSearchUser(JCGroup g, long userID) {
        if (g == null) {
            g = this;
        }
        List<JCUser> list = this.getJCUsers();
        for (JCUser tu : list) {
            if (tu.getmUserId() == userID) {
                return g;
            }
        }
        List<JCGroup> subJCGroups = g.getChildGroup();
        for (int i = 0; i < subJCGroups.size(); i++) {
            JCGroup subG = subJCGroups.get(i);
            JCGroup gg = internalSearchUser(subG, userID);
            if (gg != null) {
                return gg;
            }
        }
        return null;

    }

    public List<JCUser> searchUser(String text) {
        List<JCUser> l = new ArrayList<JCUser>();
        JCGroup.searchUser(text, l, this);
        return l;
    }

    public static void searchUser(String text, List<JCUser> l, JCGroup g) {
        if (l == null || g == null) {
            return;
        }
        List<JCUser> list = g.getJCUsers();
        for (JCUser u : list) {
            if ((u != null && u.getDisplayName() != null && u.getDisplayName()
                    .contains(text)) || (u.getArra().equals(text))) {
                l.add(u);
            }
        }
        for (JCGroup subG : g.getChildGroup()) {
            searchUser(text, l, subG);
        }
    }

    public int getOnlineUserCount() {
        Set<JCUser> counter = new HashSet<JCUser>();
        this.populateUser(this, counter);

        int c = 0;
        for (JCUser u : counter) {
            if (u.getmStatus() == JCUser.Status.ONLINE
                    || u.getmStatus() == JCUser.Status.BUSY
                    || u.getmStatus() == JCUser.Status.DO_NOT_DISTURB
                    || u.getmStatus() == JCUser.Status.LEAVE) {
                c++;
            }
        }
        return c;
    }

    public Set<JCUser> getOnlineUserSet() {
        Set<JCUser> counter = new HashSet<JCUser>();
        this.populateUser(this, counter);
        return counter;
    }

    public int getUserCount() {
        Set<JCUser> counter = new HashSet<JCUser>();
        populateUser(this, counter);
        int count = counter.size();
        counter.clear();
        return count;
    }

    private void populateUser(JCGroup g, Set<JCUser> counter) {
        List<JCUser> lu = g.getJCUsers();
        for (int i = 0; i < lu.size(); i++) {
            counter.add(lu.get(i));
        }
        List<JCGroup> sGs = g.getChildGroup();
        for (int i = 0; i < sGs.size(); i++) {
            JCGroup subG = sGs.get(i);
            populateUser(subG, counter);
        }
    }

    public void addUserToGroup(List<JCUser> l) {
        synchronized (mLock) {
            for (JCUser u : l) {
                this.JCUsers.add(u);
                u.addUserToGroup(this);
            }
        }
    }

    public int getLevel() {
        return level;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (mGId ^ (mGId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JCGroup other = (JCGroup) obj;
        if (mGId != other.mGId)
            return false;
        return true;
    }

    @Override
    public int compareTo(JCGroup arg0) {

        Log.e("compareTo:--组排序", PinYinUtil.getPingYin(arg0.getName())+"---"+PinYinUtil.getPingYin(this.mName) );
        return PinYinUtil.getPingYin(this.mName).compareTo(PinYinUtil.getPingYin(arg0.getName()));
    }

    public abstract String toXml();



//    public void sort(){
//        Collections.sort(mChild, new Comparator<Group>() {
//            @Override
//            public int compare(Group o1, Group o2) {
//                Log.e("Comparator",o1.getName()+"=="+o2.getName());
//                return o1.getName().compareTo(o2.getName());
//            }
//        });
//    }
}

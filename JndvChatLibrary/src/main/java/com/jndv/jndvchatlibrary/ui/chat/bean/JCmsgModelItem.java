package com.jndv.jndvchatlibrary.ui.chat.bean;

import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/12
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 消息model中消息变化管理工具类，增删改查
 */
public class JCmsgModelItem {

    private List<JCimMessageBean> datas ;//处理后的消息列表
    private int type;//0=增(item=null时会使用datas数据重置列表数据)；1=删；2=改；
    // 3=增（datas增加到顶部add(0,datas)）；4=增(增到底部)；5=加载失败
    private int index;//变化的消息的位置
    private JCbaseIMMessage item;//新消息

    public List<JCimMessageBean> getDatas() {
        return datas;
    }

    public void setDatas(List<JCimMessageBean> datas) {
        this.datas = datas;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public JCbaseIMMessage getItem() {
        return item;
    }

    public void setItem(JCbaseIMMessage item) {
        this.item = item;
    }
}

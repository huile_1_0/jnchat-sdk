package com.jndv.jndvchatlibrary.ui.chat.fragment.actions;

import android.Manifest;
import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.listUi.JCbaseActions;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgVideoContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCFilesUpUtils;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;

import com.mingyuechunqiu.recordermanager.data.bean.RecordVideoOption;
import com.mingyuechunqiu.recordermanager.data.bean.RecordVideoRequestOption;
import com.mingyuechunqiu.recordermanager.data.bean.RecordVideoResultInfo;
import com.mingyuechunqiu.recordermanager.data.exception.RecorderManagerException;
import com.mingyuechunqiu.recordermanager.feature.record.RecorderManagerProvider;
import com.mingyuechunqiu.recordermanager.framework.RMRecordVideoResultCallback;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;

/**
 * Author: wangguodong
 * Date: 2022/2/16
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 相册-聊天页面加号菜单中菜单项
 */
public class JCvideoAction extends JCbaseActions {

    public JCvideoAction(int nameId, int iconId) {
        super(nameId, iconId);
    }

    public JCvideoAction() {
        super(R.string.chat_action_title_video, R.drawable.jc_chat_action_video);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onClick() {
        RxPermissions rxPermissions = new RxPermissions(getContainer().activity);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                , Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        //申请的权限全部允许
//                        String filepath = FilePathUtils.getSaveFilePath(getActivity());
//                        RecorderOption recorderOption = new RecorderOption.Builder().buildDefaultVideoBean(filepath);
//                        recorderOption.setVideoHeight(720);
//                        recorderOption.setVideoWidth(1280);
                        RecordVideoRequestOption option = new RecordVideoRequestOption.Builder()
                                .setRecordVideoOption(new RecordVideoOption.Builder()
                                        .setTimingHint("长按录制")
//                                        .setRecorderOption(recorderOption)
                                        .build())
                                .setMaxDuration(60)
                                .build();

                        RecorderManagerProvider.getRecordVideoRequester().startRecordVideo((FragmentActivity) getContainer().activity, option, new RMRecordVideoResultCallback() {
                            @Override
                            public void onFailure(@NotNull RecorderManagerException e) {

                            }

                            @Override
                            public void onResponseRecordVideoResult(@NonNull RecordVideoResultInfo info) {
                                Log.e("JCFilesUpUtils", "onActivityResult: " + info.getDuration() + " " + info.getFilePath());
                                //发送小视频
                                int time = info.getDuration()/1000;
                                if (time<1) {
                                    JCChatManager.showToast("时间不能少于1秒，请重试！");
                                }else {
                                    String timestr = time/100 + ":" + (time%100<10?("0"+time%100):time%100);
                                    sendVideo(timestr,info.getFilePath());
                                }

                            }
                        });
                    }
                });
    }

    protected void sendVideo( String time ,  String filePath) {
        JCMsgVideoContentBean videoContentBean = new JCMsgVideoContentBean(time, filePath);
        String contentText = new Gson().toJson(videoContentBean);
        String contentBase64 = Base64.encodeToString(contentText.getBytes(),Base64.NO_WRAP);
//
        JCimMessageBean jCimMessageBean = JCchatFactory.creatIMMessage(
                contentBase64,""+ JCmessageType.VIDEO, getContainer().jCsessionChatInfoBean.getSessionChatId(), JNSpUtils.getString(getContainer().activity, JNPjSipConstants.PJSIP_NUMBER, JNPjSipConstants.PJSIP_NUMBER_DEFAULT)
                , JNBasePreferenceSaves.getSipAddress(), getContainer().jCsessionChatInfoBean.getSessionChatId(), getContainer().jCsessionChatInfoBean.getSessionaddress()
                , ""+getContainer().sessionType, JNBasePreferenceSaves.getJavaAddress(),getContainer().jCsessionChatInfoBean.getSessionaddressJava());

        getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,0);

        Log.e("sendVideo",filePath);
        Log.e("sendVideo",time+"++++");

        JCFilesUpUtils.upCommonFile(filePath,JCmessageType.VIDEO, new OnHttpListener<String>() {
            @Override
            public void onSucceed(String url) {
                JNLogUtil.e("====JCvideoAction===upCommonFile==001======url=="+url);
                try {
                    if (TextUtils.isEmpty(url)){
                        JCThreadManager.onMainHandler(() -> getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(), JCmessageStatusType.sendFail));
                    }else {
                        JNLogUtil.e("====JCvideoAction===upCommonFile==002========"+JNBaseUtilManager.getFilesUrl(url));
                        videoContentBean.setUrl(JNBaseUtilManager.getFilesUrl(url));
                        String msgEntityText = new Gson().toJson(videoContentBean);
                        jCimMessageBean.setContent(Base64.encodeToString(msgEntityText.getBytes(),Base64.NO_WRAP));
                        JCThreadManager.onMainHandler(() -> getContainer().jcviewPanelInterface.sendFileMessage(jCimMessageBean,1));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(Exception e) {
                JCThreadManager.onMainHandler(() -> getContainer().jcviewPanelInterface.updateMessageState(jCimMessageBean.getMsgID(),JCmessageStatusType.sendFail));
            }
        });
    }

}

package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.text.TextUtils;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.http.api.JCGetCollectMessageApi;


import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/4/20
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCCollectMsgViewModel extends ViewModel {
    private Fragment fragment;

    private MutableLiveData<List<JCGetCollectMessageApi.ColloctBean>> collectMsgList = new MutableLiveData<>();;
//    private MutableLiveData<OperateItem> operateeData =new MutableLiveData<>();;

    public JCCollectMsgViewModel() {
        collectMsgList = new MutableLiveData<>();
//        operateeData = new MutableLiveData<>();
    }

    public void init(Fragment fragment){
        this.fragment = fragment ;
    }

    public MutableLiveData<List<JCGetCollectMessageApi.ColloctBean>> getCollectMsgList() {
        return collectMsgList;
    }

//    public MutableLiveData<OperateItem> getOperateeData() {
//        return operateeData;
//    }

    public void getCollectMsgListData() {
        EasyHttp.get(fragment)
                .api(new JCGetCollectMessageApi()
                        .setInterfaceName("/msgCollect/"+JNBasePreferenceSaves.getUserSipId()+"/list")
                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
//                        .setPageNo(page)
//                        .setPageSize(JCChatManager.pageSize)
                )
                .request(new OnHttpListener<JCGetCollectMessageApi.Bean>() {
                    @Override
                    public void onSucceed(JCGetCollectMessageApi.Bean result) {
                        if (result==null){
                            collectMsgList.setValue(null);
                            return;
                        }
                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode()))collectMsgList.setValue(result.getData());
                        else collectMsgList.setValue(null);
                    }

                    @Override
                    public void onFail(Exception e) {
                        collectMsgList.setValue(null);
                    }
                });
    }

//    public void deleteSession(String sid) {
//        loadingPopupView = new XPopup.Builder(fragment.getActivity()).asLoading("操作中...");
//        loadingPopupView.show();
//        EasyHttp.delete(fragment)
//                .api(new JCDeleteSessionApi()
//                        .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
//                        .setOperationUserId(JNBasePreferenceSaves.getUserSipId())
//                        .setConversationId(sid)
//                )
//                .request(new OnHttpListener<JCDeleteSessionApi.Bean>() {
//                    @Override
//                    public void onSucceed(JCDeleteSessionApi.Bean result) {
//                        Log.e("chatlist", "onSucceed: " + result.toString());
//                        if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
//                            OperateItem operateItem = new OperateItem();
//                            operateItem.setType(0);
//                            operateItem.setState(0);
//                            operateItem.setMsg(sid);
//                            operateeData.setValue(operateItem);
//                        }else {
//                            OperateItem operateItem = new OperateItem();
//                            operateItem.setType(0);
//                            operateItem.setState(1);
//                            operateeData.setValue(operateItem);
//                        }
//                        if (null!=loadingPopupView)loadingPopupView.dismiss();
//                    }
//                    @Override
//                    public void onFail(Exception e) {
//                        OperateItem operateItem = new OperateItem();
//                        operateItem.setType(0);
//                        operateItem.setState(1);
//                        operateeData.setValue(operateItem);
//                        if (null!=loadingPopupView)loadingPopupView.dismiss();
//                    }
//                });
//    }



    public static final class OperateItem{
        private int type;//操作类型；0=删除;1=更新会话设置

        private int state;//操作状态；0=成功；1=失败
        private String msg;//说明

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}

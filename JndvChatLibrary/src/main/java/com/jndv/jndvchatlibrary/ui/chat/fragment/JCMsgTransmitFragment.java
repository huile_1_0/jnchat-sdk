package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.ehome.sipservice.SipServiceCommand;
import com.google.gson.Gson;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.FragmentJCMsgTransmitBinding;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.thraed.JCThreadManager;
import com.jndv.jndvchatlibrary.ui.JCbaseFragment;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCSessionListSelecteBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCSessionUtil;
import com.jndv.jnbaseutils.ui.JNSimpleDividerDecoration;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

import com.wgd.baservadapterx.CommonAdapter;
import com.wgd.baservadapterx.base.ViewHolder;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;


public class JCMsgTransmitFragment extends JCbaseFragment {
    private FragmentJCMsgTransmitBinding binding;
    private JCimMessageBean messageBean ;
    public static final String CONTENT = "content";

    JCMsgTransmitViewModel viewModel;
    private CommonAdapter adapter ;
    private List<JCSessionListSelecteBean> datas = new ArrayList<>();
    private List<JCSessionListSelecteBean> selectes = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentJCMsgTransmitBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        init();
        return root;
    }

    private void init() {
        initView();
        Bundle bundle = getArguments();
        if (bundle != null) {
            messageBean = (JCimMessageBean) bundle.getSerializable(CONTENT);
        }

        viewModel = new JCMsgTransmitViewModel(this);
        viewModel.getChatListData();
        viewModel.getSessionList().observeForever(jCsessionListModelItem -> {
            if (null != jCsessionListModelItem && jCsessionListModelItem.getType() == 0) {
                if (null != jCsessionListModelItem.getDatas() && jCsessionListModelItem.getDatas().size()>0){
                    if (1==jCsessionListModelItem.getPage()){
                        datas.clear();
                    }
                    datas.addAll(jCsessionListModelItem.getDatas());
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void initView() {
        binding.rvUser.setLayoutManager(new LinearLayoutManager(getActivity()));
        JNSimpleDividerDecoration divider = new JNSimpleDividerDecoration(getActivity());
        divider.setPadding(30,20);
        binding.rvUser.addItemDecoration(divider);
        adapter = new CommonAdapter<JCSessionListSelecteBean>(getActivity(), R.layout.jc_item_session_selecte, datas) {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            protected void convert(ViewHolder holder, JCSessionListSelecteBean node, int position) {
                JCglideUtils.loadCircleImage(getActivity(), node.getJcSessionListBean().getOther_pic(), holder.getView(R.id.iv_user_head));
                ((TextView)holder.getView(R.id.tv_user_name)).setText(getName(node.getJcSessionListBean()));
                if (node.isSelected()){
                    ((CheckBox)holder.getView(R.id.cb_user)).setChecked(true);
                }else {
                    ((CheckBox)holder.getView(R.id.cb_user)).setChecked(false);
                }
                (holder.getView(R.id.ll_friend)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        node.setSelected(!node.isSelected());
                        initSelecte(node);
                        datas.set(position, node);
                        adapter.notifyItemChanged(position);
                    }
                });
            }
        };
        binding.rvUser.setAdapter(adapter);
        binding.tvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectes.size()<=0){
                    JCThreadManager.showMainToast("请选择要发送的对象！");
                }else {
                    for (JCSessionListSelecteBean nodeSelecte: selectes) {
                        transmit(nodeSelecte.getJcSessionListBean());
                    }
                    getActivity().finish();
                }
            }
        });
        binding.llSelecteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectes.size()==datas.size()){
                    selectes.clear();
                    for (int i = 0; i < datas.size(); i++) {
                        JCSessionListSelecteBean node = datas.get(i);
                        node.setSelected(false);
                        datas.set(i, node);
                    }
                    adapter.notifyDataSetChanged();
                    showSelecte();
                }else {
                    selectes.clear();
                    for (int i = 0; i < datas.size(); i++) {
                        JCSessionListSelecteBean node = datas.get(i);
                        node.setSelected(true);
                        datas.set(i, node);
                        selectes.add(node);
                    }
                    adapter.notifyDataSetChanged();
                    showSelecte();
                }
            }
        });
    }

    private void initSelecte(JCSessionListSelecteBean node){
        if (node.isSelected()){
            for (JCSessionListSelecteBean nodeSelecte: selectes) {
                if (nodeSelecte.getJcSessionListBean().getSession_id()==node.getJcSessionListBean().getSession_id()){
                    return;
                }
            }
            selectes.add(node);
            showSelecte();
        }else {
            for (JCSessionListSelecteBean nodeSelecte: selectes) {
                if (nodeSelecte.getJcSessionListBean().getSession_id()==node.getJcSessionListBean().getSession_id()){
                    selectes.remove(nodeSelecte);
                    showSelecte();
                    return;
                }
            }
        }
    }

    private void showSelecte(){
        binding.tvSelecteNum.setText("已选（"+ selectes.size() +"）");
        if (selectes.size() == datas.size()){
            binding.cbAll.setChecked(true);
        }else {
            binding.cbAll.setChecked(false);
        }
    }

    public static String getName(JCSessionListBean dataBean){
        if(null==dataBean)return "";
        String name = ""+dataBean.getOther_id();
        try {
            if (!TextUtils.isEmpty(dataBean.getRemarks())){
                name = dataBean.getRemarks();
            }else if (!TextUtils.isEmpty(dataBean.getOther_name())){
                name = dataBean.getOther_name();
            }else {
                String message = dataBean.getFirst_content();
                JCimMessageBean jCimMessageBean = new Gson().fromJson(message, JCimMessageBean.class);
                if (jCimMessageBean!=null&&TextUtils.equals(JNBasePreferenceSaves.getUserSipId()
                        , jCimMessageBean.getToSipID())
                        &&!TextUtils.isEmpty(jCimMessageBean.getFromName())
                        && TextUtils.equals(jCimMessageBean.getSessionType(), ""+JCSessionType.CHAT_PERSION)){
                    name = jCimMessageBean.getFromName();
//                }else if (TextUtils.equals(JNBasePreferenceSaves.getUserSipId(), jCimMessageBean.getFromID())){
//                    name = jCimMessageBean.getto;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return name;
    }


    //转发消息
    private void transmit(JCSessionListBean bean) {
        JCimMessageBean msg = JCchatFactory.creatIMMessage(messageBean.getContent(),
                messageBean.getMsgType(), bean.getOther_id()+"", JNBasePreferenceSaves.getUserSipId()
                , JNBasePreferenceSaves.getSipAddress(), bean.getOther_id()+"", bean.getSession_party_domain_addr()
                , ""+bean.getType(), JNBasePreferenceSaves.getJavaAddress(),bean.getSession_party_domain_addr_user());

        if (JCSessionUtil.isGroupStateOk(bean.getSession_id())) {
            try {
                JNLogUtil.e("sendMessage: ======================sendMessage=======" + "sip:" + bean.getOther_id() + "@" + bean.getSession_party_domain_addr());
                String userID = JNSpUtils.getString(requireActivity(), "myAccount", JNPjSipConstants.PJSIP_NUMBER_DEFAULT);
                Log.e("sendMessage",new Gson().toJson(msg));
                Box<JCimMessageBean> beanBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
                beanBox.put(msg);
                EventBus.getDefault().post(new JNCodeEvent<JCbaseIMMessage>(JNEventBusType.CODE_MESSAGE_TRANSMIT, "", msg));
                Log.d("SipService","msg==trans=");

                SipServiceCommand.sendMessageToBuddy(getActivity(), new Gson().toJson(msg), userID
                        , "sip:" + bean.getOther_id() + "@" + bean.getSession_party_domain_addr()
                        , JCSessionType.getTypeSIP(""+bean.getType()), false);
            } catch (Exception e) {
                JNLogUtil.e("==JCChatFragment==sendFileMessage==", e);
            }
        }
    }


    @Override
    public void onDestroy() {
//        JCmessageListenerManager.removeMessageListener();
        super.onDestroy();
    }
}

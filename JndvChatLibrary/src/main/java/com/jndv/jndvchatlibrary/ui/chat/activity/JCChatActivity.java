package com.jndv.jndvchatlibrary.ui.chat.activity;

import static com.jndv.jndvchatlibrary.ui.chat.fragment.JCChatManageFragment.INSIPADDRESS;
import static com.jndv.jndvchatlibrary.ui.chat.fragment.JCChatManageFragment.INSIPID;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import com.jndv.jnbaseutils.chat.JCSessionListBean_;
import com.jndv.jnbaseutils.ui.base.JNBaseFragmentActivity;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.ui.JCbaseFragmentActivity;
import com.jndv.jndvchatlibrary.ui.chat.base.JCchatListAdapter;
import com.jndv.jnbaseutils.chat.JCSessionListBean;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCChatManageFragment;
import com.jndv.jndvchatlibrary.ui.chat.fragment.JCchatFragment;
import com.jndv.jndvchatlibrary.ui.crowd.activity.JCCrowdDetailActivity;
import com.jndv.jndvchatlibrary.utils.ActivityStack;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;
import com.jndv.jnbaseutils.chat.JCSessionType;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/4/23
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
//public class JCChatActivity extends JCbaseFragmentActivity {
public class JCChatActivity extends JNBaseFragmentActivity {

    public static final String TAG="JCChatActivity";
    private JCSessionListBean jcSessionListBean;
    public static final String KEY_JCSESSIONLISTBEAN = "jcSessionListBean";

    public static void start(Context context, JCSessionListBean jcSessionListBean){
        Intent intent = new Intent(context, JCChatActivity.class);
        intent.putExtra(KEY_JCSESSIONLISTBEAN, jcSessionListBean);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityStack.getInstance().addActivity(this);
        jcSessionListBean = (JCSessionListBean) getIntent().getSerializableExtra(KEY_JCSESSIONLISTBEAN);
        Bundle bundle = new Bundle();
        bundle.putString(JCEntityUtils.SESSION_OTHER_ID, ""+jcSessionListBean.getOther_id());
        bundle.putString(JCEntityUtils.SESSION_ADDRESS, ""+jcSessionListBean.getSession_party_domain_addr());
        bundle.putString(JCEntityUtils.SESSION_ADDRESS_JAVA, ""+jcSessionListBean.getSession_party_domain_addr_user());
        bundle.putString(JCEntityUtils.SESSION_TYPE, jcSessionListBean.getType());
        bundle.putString(JCEntityUtils.CROWD_ID, jcSessionListBean.getOther_id());
        bundle.putInt(JCEntityUtils.SESSION_ID, jcSessionListBean.getSession_id());
        bundle.putInt(JCEntityUtils.SESSION_NOT_LINE_MSG_NUM, jcSessionListBean.getUnread_number());
        bundle.putInt(JCEntityUtils.GROUP_STATUS,jcSessionListBean.getStatus());
        long time = 0 ;
        try {
            time = Long.parseLong(jcSessionListBean.getFirst_time());
        }catch (Exception e){
            JNLogUtil.e("=====", e);
        }
        bundle.putLong(JCEntityUtils.SESSION_READ_LAST_TIME, time);

        /*
        * {"bubble":"","content":"eyJmb250Q29sb3IiOiIjMjIyMjIyIiwiZm9udFNpemUiOjE2LCJmb250VHlwZSI6MCwicmVwbHkiOjAsInJlcGx5Q29udGVudCI6IiIsInRleHQiOiIzIn0\u003d","deviceType":"0","fromHead":"http://citybrain-storage.jndv.org:10088/group1/M01/01/6B/CrAAGGOb0_iAVgOsAAA7f5CWlS8627.png","fromID":"661005","fromName":"兰山661005","fromRealm":"citybrain.jndv.org:25060","fromRealmJava":"citybrain-api.jndv.org:10088","fromSipID":"661005","id":0,"msgID":"5709357c-4669-4ae3-8f59-ecd58192f270","msgType":"1","readNumAll":0,"receiveTime":"1671679943151","saveUserId":"661005","sendTime":"1671679943150","sessionId":"8056","sessionRealm":"citybrain.jndv.org:25060","sessionRealmJava":"citybrain-api.jndv.org:10088","sessionType":"1","status":"0","toRealm":"citybrain.jndv.org:25060","toRealmJava":"citybrain-api.jndv.org:10088","toSipID":"8056","version":"0.0.6"}
        * */

        Box<JCSessionListBean> imMsgBox = JCobjectBox.get().boxFor(JCSessionListBean.class);
        QueryBuilder<JCSessionListBean> builder = imMsgBox.query();
        JCSessionListBean dataBean = builder.equal(JCSessionListBean_.session_id
                , jcSessionListBean.getSession_id()).build().findFirst();

        JCchatFragment contentFragment = new JCchatFragment();
        contentFragment.setArguments(bundle);
        showFragment(contentFragment);

        String name = "";
        if (null!=dataBean){
            name = JCchatListAdapter.getName(dataBean);
            Log.d(TAG,"status=="+dataBean.getStatus());
            jcSessionListBean.setStatus(dataBean.getStatus());
        }
        if (TextUtils.isEmpty(name)){//新建的群从数据库查不到
            name=JCchatListAdapter.getName(jcSessionListBean);
//            name=jcSessionListBean.getOther_name();
        }
        setCenterTitle(name);
        if (JCSessionType.CHAT_SYSTEM.equals(jcSessionListBean.getType())){
            showMenuClick(false);
        }else {
            showMenuClick(true);
        }
    }

    @Override
    public void onMenuClick() {
        if (jcSessionListBean.getType().equals(JCSessionType.CHAT_PERSION)){
            Bundle bundle = new Bundle();
            bundle.putString(JCEntityUtils.SESSION_ID, ""+jcSessionListBean.getSession_id());
            bundle.putString(INSIPID,jcSessionListBean.getOther_id());
            bundle.putString(INSIPADDRESS,jcSessionListBean.getSession_party_domain_addr());
            JCChatManageFragment contentFragment = new JCChatManageFragment();
            Log.e("TAG", "init: ===============00=sessionId="+jcSessionListBean.getSession_id() );
            JCbaseFragmentActivity.start(JCChatActivity.this, contentFragment, "聊天信息", bundle);
        }else if (jcSessionListBean.getType().equals(JCSessionType.CHAT_GROUP)){
            Log.e("TAG", "init: ===============00=sessionId="+jcSessionListBean.getOther_id() );
            if (1==jcSessionListBean.getStatus()){
                JCChatManager.showToast("群聊已解散!");
            }else if (2==jcSessionListBean.getStatus()){
                JCChatManager.showToast("已退出群聊!");
            }else if (3==jcSessionListBean.getStatus()){
                JCChatManager.showToast("被踢出群聊!");
            }else {
                Intent intent = new Intent(JCChatActivity.this, JCCrowdDetailActivity.class);
                intent.putExtra(JCEntityUtils.CROWD_ID,jcSessionListBean.getOther_id());
                intent.putExtra(JCEntityUtils.GROUP_DOMAIN_ADDR,jcSessionListBean.getSession_party_domain_addr());
                intent.putExtra(JCEntityUtils.GROUP_DOMAIN_ADDR_USER,jcSessionListBean.getSession_party_domain_addr_user());
                intent.putExtra(JCEntityUtils.SESSION_ID,jcSessionListBean.getSession_id());
                startActivity(intent);
            }
        }
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.bean;


import com.jndv.jndvchatlibrary.ui.crowd.entity.JCUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sun on 2019/2/13 11:29.
 * 对应JNIService#OnGetGroupInfo获取的群组数据
 */
public class JCCrowdBean {


    /**
     * 群公告
     */
    private String announcement;
    /**
     * 群验证类型
     */
    private String authtype;
    /**
     * 群组id
     */
    private long creatoruserid;
    /**
     * 群id
     */
    private long id;
    /**
     * 群名称
     */
    private String name;
    private int size;
    private String summary;

    private Date createDate; // 创建日期
    /**
     * 群成员
     */
    private List<JCUser> JCUserList = new ArrayList<>();


    public JCCrowdBean() {
    }

    public JCCrowdBean(long id, long creatoruserid, String name, String authtype) {
        this.id = id;
        this.creatoruserid = creatoruserid;
        this.name = name;
        this.authtype = authtype;
    }

    /**
     * 创建群
     */
    public JCCrowdBean(String name, long creatoruserid) {
        this.id = 0;
        this.creatoruserid = creatoruserid;
        this.name = name;
    }


    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public String getAuthtype() {
        return authtype;
    }

    public void setAuthtype(String authtype) {
        this.authtype = authtype;
    }

    public long getCreatoruserid() {
        return creatoruserid;
    }

    public void setCreatoruserid(long creatoruserid) {
        this.creatoruserid = creatoruserid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name == null ? "未知用户" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSummary() {
        return summary == null ? "" : summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public List<JCUser> getJCUserList() {
        if (JCUserList == null)
            JCUserList = new ArrayList<>();
        return JCUserList;
    }

    public void setJCUserList(List<JCUser> JCUserList) {
        this.JCUserList = JCUserList;
    }

    public void addUser(JCUser JCUser) {
        if (JCUserList == null) {
            return;
        }
        for (JCUser u : JCUserList) {
            if (u.getmUserId() == JCUser.getmUserId()) { //判断是否已经存在，已经存在不做操作
                return;
            }
        }
        JCUserList.add(JCUser);
    }

//    public void addUser(List<User> user) {
//        for (User u : user) {
//            addUser(u);
//        }
//    }

    @Override
    public String toString() {
        return "CrowdBean{" +
                "announcement='" + announcement + '\'' +
                ", authtype='" + authtype + '\'' +
                ", creatoruserid=" + creatoruserid +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", summary='" + summary + '\'' +
                ", userList=" + JCUserList +
                '}';
    }

//    public String toXml() {
//        StringBuffer sb = new StringBuffer();
//        sb.append("<crowd id=\""
//                + this.id
//                + "\" name=\""
//                + EscapedCharactersProcessing.convert(this.name)
//                + "\" authtype=\""
//                + authtype
//                + "\" size=\""
//                + size
//                + "\" announcement=\""
//                + (announcement == null ? "" : EscapedCharactersProcessing
//                .convert(this.announcement))
//                + "\" summary=\""
//                + (summary == null ? "" : EscapedCharactersProcessing
//                .convert(summary)) + "\" creatoruserid=\""
//                + creatoruserid + "\"/>");
//        return sb.toString();
//    }
}

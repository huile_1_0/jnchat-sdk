package com.jndv.jndvchatlibrary.ui.chat.fragment;


import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.http.JNBaseHttpUtils;
import com.jndv.jnbaseutils.http.api.JNGetUserDetails;
import com.jndv.jnbaseutils.utils.JNBaseConstans;
import com.jndv.jndvchatlibrary.http.api.JCGetUserDetailsApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCUserDetailBean;

import static com.jndv.jndvchatlibrary.JCChatManager.showToast;

import android.text.TextUtils;
import android.util.Log;

/**
 * Author: wangguodong
 * Date: 2022/4/20
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description:
 */
public class JCUserSearchAddViewModel extends ViewModel {

    private Fragment fragment;

    private MutableLiveData<JNGetUserDetails.UserDetailBean> chatDetail ;
//    private MutableLiveData<JCUserDetailBean> chatDetail ;

    public JCUserSearchAddViewModel() {
        chatDetail = new MutableLiveData<>();
    }

    public void init(Fragment fragment){
        this.fragment = fragment ;
    }

    public MutableLiveData<JNGetUserDetails.UserDetailBean> getChatManageModel() {
        return chatDetail;
    }
//    public MutableLiveData<JCUserDetailBean> getChatManageModel() {
//        return chatDetail;
//    }

    public void getUser(String sid, String address) {
        JNBaseHttpUtils.getInstance().GetUserDetailMore(EasyHttp.post(fragment), sid
                , new OnHttpListener<JNGetUserDetails.Bean>() {
            @Override
            public void onSucceed(JNGetUserDetails.Bean result) {
                try {
                    if (TextUtils.equals(JNBaseConstans.RESULT_SUCCESS, result.getCode())){
                        chatDetail.setValue(result.getData());
                        showToast("获取到用户！");
                    }else if (TextUtils.equals("2001", result.getCode())){
                        chatDetail.setValue(null);
                        showToast("用户信息不存在！");
                    } else {
                        showToast("获取失败1！");
                        chatDetail.setValue(null);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(Exception e) {
                showToast("获取失败0！");
                chatDetail.setValue(null);
            }
        });
    }

}

package com.jndv.jndvchatlibrary.ui.chat.fragment;

import androidx.recyclerview.widget.RecyclerView;

import com.jndv.jndvchatlibrary.R;
import com.jndv.jnbaseutils.chat.listUi.JCbaseMsgAdapter;
import com.jndv.jnbaseutils.chat.listUi.JCbaseViewHolder;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/11
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 聊天页面消息列表adapter
 */
public class JCmessageAdapter extends JCbaseMsgAdapter<JCbaseIMMessage, JCbaseViewHolder> {

    public JCmessageAdapter(RecyclerView recyclerView, List<JCbaseIMMessage> data) {
        super(recyclerView, R.layout.jc_item_adapter_all, data);
    }

    @Override
    protected String getViewType(JCbaseIMMessage item) {
        return item.getMsgType();
    }

    @Override
    protected String getItemKey(JCbaseIMMessage item) {
        return null==item?"null":item.getMsgID();
    }

}

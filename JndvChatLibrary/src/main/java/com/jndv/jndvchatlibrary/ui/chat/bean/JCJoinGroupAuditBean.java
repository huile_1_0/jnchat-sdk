package com.jndv.jndvchatlibrary.ui.chat.bean;

import java.util.List;

/**
 * @Description 进群审核实体类
 * @Author SunQinzheng
 * @Time 2022/7/15 下午 1:58
 **/
public class JCJoinGroupAuditBean {

    /**
     * list : [{"id":1,"groupId":9938,"userId":1041,"domainAddr":"imapi.jndv.com:5080","domainAddrUser":"192.168.2.205:8000","addTime":1656922634000,"joinNickname":"测试1041","joinPic":"","auditStatus":0,"auditUser":0,"auditAddr":null,"auditAddrUser":null,"auditTime":null}]
     * total : 1
     */

    private int total;
    private List<ListBean> list;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 1
         * groupId : 9938
         * userId : 1041
         * domainAddr : imapi.jndv.com:5080
         * domainAddrUser : 192.168.2.205:8000
         * addTime : 1656922634000
         * joinNickname : 测试1041
         * joinPic :
         * auditStatus : 0
         * auditUser : 0
         * auditAddr : null
         * auditAddrUser : null
         * auditTime : null
         */

        private int id;
        private String groupId;
        private String userId;
        private String domainAddr;
        private String domainAddrUser;
        private long addTime;
        private String joinNickname;
        private String joinPic;
        private int auditStatus;
        private int auditUser;
        private Object auditAddr;
        private Object auditAddrUser;
        private Object auditTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getDomainAddr() {
            return domainAddr;
        }

        public void setDomainAddr(String domainAddr) {
            this.domainAddr = domainAddr;
        }

        public String getDomainAddrUser() {
            return domainAddrUser;
        }

        public void setDomainAddrUser(String domainAddrUser) {
            this.domainAddrUser = domainAddrUser;
        }

        public long getAddTime() {
            return addTime;
        }

        public void setAddTime(long addTime) {
            this.addTime = addTime;
        }

        public String getJoinNickname() {
            return joinNickname;
        }

        public void setJoinNickname(String joinNickname) {
            this.joinNickname = joinNickname;
        }

        public String getJoinPic() {
            return joinPic;
        }

        public void setJoinPic(String joinPic) {
            this.joinPic = joinPic;
        }

        public int getAuditStatus() {
            return auditStatus;
        }

        public void setAuditStatus(int auditStatus) {
            this.auditStatus = auditStatus;
        }

        public int getAuditUser() {
            return auditUser;
        }

        public void setAuditUser(int auditUser) {
            this.auditUser = auditUser;
        }

        public Object getAuditAddr() {
            return auditAddr;
        }

        public void setAuditAddr(Object auditAddr) {
            this.auditAddr = auditAddr;
        }

        public Object getAuditAddrUser() {
            return auditAddrUser;
        }

        public void setAuditAddrUser(Object auditAddrUser) {
            this.auditAddrUser = auditAddrUser;
        }

        public Object getAuditTime() {
            return auditTime;
        }

        public void setAuditTime(Object auditTime) {
            this.auditTime = auditTime;
        }
    }
}

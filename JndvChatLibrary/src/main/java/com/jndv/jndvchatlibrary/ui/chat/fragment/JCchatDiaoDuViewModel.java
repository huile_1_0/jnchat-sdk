package com.jndv.jndvchatlibrary.ui.chat.fragment;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ehome.manager.utils.JNPjSipConstants;
import com.ehome.manager.utils.JNSpUtils;
import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.chat.JCSessionType;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jnbaseutils.chat.JCimMessageBean_;
import com.jndv.jnbaseutils.chat.JCmessageStatusType;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.eventbus.JNCodeEvent;
import com.jndv.jnbaseutils.eventbus.JNEventBusType;
import com.jndv.jnbaseutils.utils.JNLogUtil;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.jndv.jndvchatlibrary.db.JCobjectBox;
import com.jndv.jndvchatlibrary.http.api.JCMessageRecordApi;
import com.jndv.jndvchatlibrary.http.api.JCMessageRecordWebApi;
import com.jndv.jndvchatlibrary.http.api.JCNotLineMessageApi;
import com.jndv.jndvchatlibrary.ui.chat.bean.JCmsgModelItem;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgNotifyContentBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgWebNoticeContentBean;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCChatMsgFactory;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCNotifyType;
import com.jndv.jndvchatlibrary.ui.chat.utils.JCmessageType;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.query.QueryBuilder;

/**
 * Author: wangguodong
 * Date: 2022/2/11
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 聊天页面中，数据管理工具，获取本地、网络消息列表操作实现
 */
public class JCchatDiaoDuViewModel extends ViewModel {

    private MutableLiveData<List<JCimMessageBean>> datas;
    private MutableLiveData<JCmsgModelItem> dataChange;

    private String otherId;//对方id
    private String sessionId;//会话id
    private String sessionType;//会话类型
    private String sessionaddress;//会话域地址
    private LifecycleOwner lifecycleOwner;

    public JCchatDiaoDuViewModel() {
        datas = new MutableLiveData<>();
        dataChange = new MutableLiveData<>();
    }

    public void init(String otherId,String sessionId, String sessionType, String sessionaddress, LifecycleOwner lifecycleOwner){
        this.otherId = otherId ;
        this.sessionId = sessionId ;
        this.sessionType = sessionType ;
        this.sessionaddress = sessionaddress ;
        this.lifecycleOwner = lifecycleOwner ;
    }

    public LiveData<List<JCimMessageBean>> getMessageList() {
        return datas;
    }
    public LiveData<JCmsgModelItem> getDataChange(){
        return dataChange;
    }

//  排序
    public List<JCimMessageBean> sortData(List<JCimMessageBean> mList) {
        try {
            Collections.sort(mList, new Comparator<JCimMessageBean>() {
                @Override
                public int compare(JCimMessageBean o1, JCimMessageBean o2) {
                    long time1 = Long.parseLong(o1.getReceiveTime());
                    long time2 = Long.parseLong(o2.getReceiveTime());
                    if (time1 > time2){
                        return 1;
                    }
                    return -1;
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        return mList;
    }

    /**
     * 给列表中调价一条数据
     * 添加一条消息
     * @param message
     */
    public void addData(JCbaseIMMessage message){
        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
        jCmsgModelItem.setItem(message);
        jCmsgModelItem.setType(0);
        dataChange.setValue(jCmsgModelItem);
    }

    /**
     * 更新消息
     * @param message
     */
    public void updateData(JCbaseIMMessage message){
        JNLogUtil.e("JCchatFragment", "===001==");
        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
        jCmsgModelItem.setItem(message);
        jCmsgModelItem.setIndex(-1);
        jCmsgModelItem.setType(2);
        dataChange.setValue(jCmsgModelItem);
    }
    /**
     * 更新列表中信息的已读状态
     * @param message
     */
    public void updateDataRead(JCbaseIMMessage message){
        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
        jCmsgModelItem.setItem(message);
        jCmsgModelItem.setIndex(-1);
        jCmsgModelItem.setType(6);
        dataChange.setValue(jCmsgModelItem);
    }

    /**
     * 获取旧消息列表
     * 下拉加载消息
     */
    public void getOldMessage(long time){
        if (null==otherId)return;
        Log.e("JCchatFragment", "getNewMessage: ==============time==" + time);
        getMessageRecord(time, "2", 0);

    }

    /**
     * 获取消息列表
     * 上拉加载消息
     * addType  0=第一页直接替换；1=添加到底部add(0,datas)
     */
    public void getNewMessage(long time, int addType){
        JNLogUtil.e("weidu", "getNewMessage: ==============time==" + time);
        if (null==otherId)return;
        getMessageRecord(time, "1", addType);
    }

    public void getMessageRecord(long time,String slideType, int addType){
        String ip = JNBaseUtilManager.getLoginServerUrl();
//        String ip = JNBasePreferenceSaves.getString("USER*IP");
        JCMessageRecordWebApi jcMessageRecordApi = new JCMessageRecordWebApi()
                .setApiName("/im-server-api/messageRecord/systemPage")
                .setDomainAddr(JNBasePreferenceSaves.getJavaAddress())
                .setUserId(JNBasePreferenceSaves.getUserSipId())
                .setSessionId(sessionId)
                .setSessionPartyId(otherId)
                .setSessionPartyDomainAddr(sessionaddress)
                .setSessionType(""+sessionType)
                .setBeginTime(""+time)
                .setSlideType(slideType)
                .setPageNo("1")
                .setPageSize(""+ JCChatManager.pageSize);
//        EasyHttp.get(lifecycleOwner)
        EasyHttp.post(lifecycleOwner)
                .server(ip)
                .api(jcMessageRecordApi)
                .json(new Gson().toJson(jcMessageRecordApi))
                .request(new OnHttpListener<JCMessageRecordWebApi.Bean>() {
                    @Override
                    public void onSucceed(JCMessageRecordWebApi.Bean result) {
                        try {
                            Log.e("JCchatFragment", "getNewMessage: =======onSucceed=======result==" + new Gson().toJson(result));
                            if (null!=result && null!=result.getData()){
                                if (slideType.equals("1")){
                                    Log.e("JCchatFragment", "getNewMessage: =======onSucceed====01===result==" );
                                    if (0==addType){
                                        Log.e("JCchatFragment", "getNewMessage: =======onSucceed====02===result==" );
                                        List<JCimMessageBean> data = initDataList(result.getData().getList());
                                        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
                                        jCmsgModelItem.setDatas(data);
                                        jCmsgModelItem.setType(8);
                                        dataChange.setValue(jCmsgModelItem);
                                    }else {
                                        Log.e("JCchatFragment", "getNewMessage: =======onSucceed====03===result==" );
                                        List<JCimMessageBean> data = initDataList(result.getData().getList());
                                        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
                                        jCmsgModelItem.setDatas(data);
                                        jCmsgModelItem.setType(4);
                                        dataChange.setValue(jCmsgModelItem);
                                    }
                                }else {
                                    Log.e("JCchatFragment", "getNewMessage: =======onSucceed====04===result==" );
                                    List<JCimMessageBean> data = new ArrayList<>();
                                    try {
                                        Log.e("JCchatFragment", "getOldMessage: =======onSucceed=======result==" + new Gson().toJson(result));
                                        if (null!=result && null!=result.getData() && null!=result.getData().getList()){
                                            data = initDataList(result.getData().getList());
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    Log.e("JCchatFragment", "getOldMessage: =======onSucceed=======data.size()==" + data.size());
//                        Log.e("JCchatFragment", "getOldMessage: =======onSucceed=======result==" + new Gson().toJson(data));

                                    JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
                                    jCmsgModelItem.setDatas(data);
                                    jCmsgModelItem.setType(3);
                                    dataChange.setValue(jCmsgModelItem);
                                }

                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFail(Exception e) {
                        netFail();
                    }
                });
    }

    private void netFail(){
        Log.e("JCchatFragment", "getNewMessage: =======netFail=======result==" );
        JCmsgModelItem jCmsgModelItem = new JCmsgModelItem();
        jCmsgModelItem.setType(5);
        dataChange.setValue(jCmsgModelItem);
    }

    private List<JCimMessageBean> initDataList(List<JCMsgWebNoticeContentBean.SipParameterJson> result){

        Log.e("JCchatFragment", "getNewMessage: =======onSucceed====10===result==" +result.size());
        List<JCimMessageBean> data = new ArrayList<>();
        Box<JCimMessageBean> imMsgBox = JCobjectBox.get().boxFor(JCimMessageBean.class);
        for (int i = 0; i < result.size(); i++) {
            Log.e("JCchatFragment", "getNewMessage: =======onSucceed====100===i==" + i);
            QueryBuilder<JCimMessageBean> builder = imMsgBox.query();
            Log.e("JCchatFragment", "getNewMessage: =======onSucceed====100==01=i==" + i);
            JCMsgWebNoticeContentBean.SipParameterJson sipParameterJson = result.get(i);
            Log.e("JCchatFragment", "getNewMessage: =======onSucceed====100==02=i==" + i);
            JCimMessageBean messageBean = null;
            try {
                messageBean = builder.equal(JCimMessageBean_.saveUserId, JNBasePreferenceSaves.getUserSipId(),QueryBuilder.StringOrder.CASE_SENSITIVE)
                        .equal(JCimMessageBean_.msgID, sipParameterJson.getiD(),QueryBuilder.StringOrder.CASE_SENSITIVE).build().findFirst();
            }catch (Exception e){
                e.printStackTrace();
            }

            Log.e("JCchatFragment", "getNewMessage: =======onSucceed====100==03=i==" + i);
            if (null!=messageBean){
                Log.e("JCchatFragment", "getNewMessage: =======onSucceed====101===i==" + i);
                if (TextUtils.equals(""+JCmessageStatusType.sendIng,messageBean.getStatus()))
                    messageBean.setStatus(""+ JCmessageStatusType.sendSuccessceS);
                if (TextUtils.isEmpty(messageBean.getReceiveTime()))
                    messageBean.setReceiveTime(messageBean.getSendTime());
            }else {
                Log.e("JCchatFragment", "getNewMessage: =======onSucceed====102===i==" + i);
                JCMsgWebNoticeContentBean bean = new JCMsgWebNoticeContentBean();
                bean.setSipParameterJson(sipParameterJson);
                bean.setText(sipParameterJson.getBigTypeTitle());
                bean.setType(sipParameterJson.getRemark());
                Log.e("JCchatFragment", "getNewMessage: =======onSucceed====102===getUserSipId==" + sipParameterJson.getUserSipId());
                messageBean = JCchatFactory.creatIMMessage(JNContentEncryptUtils.encodeContent(new Gson().toJson(bean))
                        , sipParameterJson.getBigType(), sipParameterJson.getUserSipId(), sipParameterJson.getUserSipId()
                        , JNBasePreferenceSaves.getSipAddress(), JNBasePreferenceSaves.getUserSipId(), JNBasePreferenceSaves.getSipAddress()
                        , ""+JCSessionType.CHAT_SYSTEM, JNBasePreferenceSaves.getJavaAddress(), JNBasePreferenceSaves.getJavaAddress());
                Log.e("JCchatFragment", "getNewMessage: =======onSucceed====102=1==getUserSipId==" + sipParameterJson.getUserSipId());
                Log.e("JCchatFragment", "getNewMessage: =======onSucceed====102===messageBean==" + new Gson().toJson(messageBean));
                try {
                    String time = sipParameterJson.getSeq();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = sdf.parse(time);
                    long timestamp = date.getTime();
                    messageBean.setSendTime(""+timestamp);
                }catch (Exception e){
                    e.printStackTrace();
                }
                messageBean.setStatus(""+ JCmessageStatusType.sendSuccessceS);
                messageBean.setReceiveTime(messageBean.getSendTime());
                imMsgBox.put(messageBean);
            }

            data.add(messageBean);
        }
        Log.e("JCchatFragment", "getNewMessage: =======onSucceed====10==data=result==" + new Gson().toJson(data));
        return data;
    }


}
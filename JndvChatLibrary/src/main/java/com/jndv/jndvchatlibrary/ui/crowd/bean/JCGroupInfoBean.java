package com.jndv.jndvchatlibrary.ui.crowd.bean;

/**
 * @Description
 * @Author SunQinzheng
 * @Time 2022/5/5 下午 1:06
 **/
public class JCGroupInfoBean {
    private int id;
    private String cgName;
    private String cgPic;
    private long modPicTime;
    private long addTime;
    private int number;
    private String qrTime;
    private String qrPhoto;
    private int isProhibition;
    private String isReasonsForClosure;
    private int renzheng;
    private String groupIds;
    private String disableSendMsg;
    private int groupType;
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setCgName(String cgName) {
        this.cgName = cgName;
    }
    public String getCgName() {
        return cgName;
    }

    public void setCgPic(String cgPic) {
        this.cgPic = cgPic;
    }
    public String getCgPic() {
        return cgPic;
    }

    public void setModPicTime(long modPicTime) {
        this.modPicTime = modPicTime;
    }
    public long getModPicTime() {
        return modPicTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }
    public long getAddTime() {
        return addTime;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    public int getNumber() {
        return number;
    }

    public void setQrTime(String qrTime) {
        this.qrTime = qrTime;
    }
    public String getQrTime() {
        return qrTime;
    }

    public void setQrPhoto(String qrPhoto) {
        this.qrPhoto = qrPhoto;
    }
    public String getQrPhoto() {
        return qrPhoto;
    }

    public void setIsProhibition(int isProhibition) {
        this.isProhibition = isProhibition;
    }
    public int getIsProhibition() {
        return isProhibition;
    }

    public void setIsReasonsForClosure(String isReasonsForClosure) {
        this.isReasonsForClosure = isReasonsForClosure;
    }
    public String getIsReasonsForClosure() {
        return isReasonsForClosure;
    }

    public void setRenzheng(int renzheng) {
        this.renzheng = renzheng;
    }
    public int getRenzheng() {
        return renzheng;
    }

    public void setGroupIds(String groupIds) {
        this.groupIds = groupIds;
    }
    public String getGroupIds() {
        return groupIds;
    }

    public void setDisableSendMsg(String disableSendMsg) {
        this.disableSendMsg = disableSendMsg;
    }
    public String getDisableSendMsg() {
        return disableSendMsg;
    }

    public void setGroupType(int groupType) {
        this.groupType = groupType;
    }
    public int getGroupType() {
        return groupType;
    }
}

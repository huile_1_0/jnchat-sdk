package com.jndv.jndvchatlibrary.ui.chat.utils;

import com.jndv.jnbaseutils.chat.listUi.JCchatFactory;
import com.jndv.jnbaseutils.chat.listUi.JCholderManager;
import com.jndv.jndvchatlibrary.BuildConfig;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCCardAction;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCCollectAction;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCFileAction;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCLocationAction;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCVideoCallAction;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCVoiceCallAction;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCimageAction;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCphotoAction;
import com.jndv.jndvchatlibrary.ui.chat.fragment.actions.JCvideoAction;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderAudioVideoRecord;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderFile;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderImage;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderLocation;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderNameCard;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderNotifyCenter;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderText;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderVideo;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderVoice;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderWebNotice;
import com.jndv.jndvchatlibrary.ui.chat.viewholder.JCmsgViewHolderWithdraw;

public class JCChatMsgFactory {

    public static void init(){
        JCchatFactory.setChatSdkVersion(BuildConfig.chatSdkVersion);

        JCchatFactory.clearP2PActions();

        JCchatFactory.addP2PActions(new JCimageAction());
        JCchatFactory.addP2PActions(new JCphotoAction());
        JCchatFactory.addP2PActions(new JCFileAction());
        JCchatFactory.addP2PActions(new JCvideoAction());
        JCchatFactory.addP2PActions(new JCVoiceCallAction());
        JCchatFactory.addP2PActions(new JCVideoCallAction());
        JCchatFactory.addP2PActions(new JCCollectAction());
        JCchatFactory.addP2PActions(new JCCardAction());
        JCchatFactory.addP2PActions(new JCLocationAction());
        JCchatFactory.clearGroupActions();

        JCchatFactory.addGroupActions(new JCimageAction());
        JCchatFactory.addGroupActions(new JCphotoAction());
        JCchatFactory.addGroupActions(new JCFileAction());
        JCchatFactory.addGroupActions(new JCvideoAction());
        JCchatFactory.addGroupActions(new JCCollectAction());
        JCchatFactory.addGroupActions(new JCCardAction());
        JCchatFactory.addGroupActions(new JCLocationAction());

        JCchatFactory.clearMngBases();
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.TEXT,  JCmsgViewHolderText.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.IMG,  JCmsgViewHolderImage.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.VOICE,  JCmsgViewHolderVoice.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.VIDEO,  JCmsgViewHolderVideo.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.WITHDRAW,  JCmsgViewHolderWithdraw.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.NOTIFY,  JCmsgViewHolderNotifyCenter.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.CARD,  JCmsgViewHolderNameCard.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.AUDIO_VIDEO_RECORD,  JCmsgViewHolderAudioVideoRecord.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.FILE,  JCmsgViewHolderFile.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.LOCATION,  JCmsgViewHolderLocation.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.SYSTEM_EVENT,  JCmsgViewHolderWebNotice.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.SYSTEM_ALARM,  JCmsgViewHolderWebNotice.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.SYSTEM_TASK,  JCmsgViewHolderWebNotice.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.SYSTEM_NOTICE,  JCmsgViewHolderWebNotice.class));
        JCchatFactory.addMngBases(new JCholderManager(JCmessageType.SYSTEM_DISPATCH,  JCmsgViewHolderWebNotice.class));

    }

}

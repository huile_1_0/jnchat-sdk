package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;


public class JCInviteGroupNumbersApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/group/inviteGroupNumbers";
    }


    private String domainAddr;
    private String domainAddrUser;
    private String userId  ;
    private String groupNumbers;
    private String groupId;
    private String groupDomainAddr;
    private String groupDomainAddrUser;


    public JCInviteGroupNumbersApi(String domainAddr, String domainAddrUser, String userId, String groupNumbers, String groupId, String groupDomainAddr, String groupDomainAddrUser) {
        this.domainAddr = domainAddr;
        this.domainAddrUser = domainAddrUser;
        this.userId = userId;
        this.groupNumbers = groupNumbers;
        this.groupId = groupId;
        this.groupDomainAddr = groupDomainAddr;
        this.groupDomainAddrUser = groupDomainAddrUser;
    }
}

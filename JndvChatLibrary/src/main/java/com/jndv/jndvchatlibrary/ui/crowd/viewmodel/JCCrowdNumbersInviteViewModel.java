package com.jndv.jndvchatlibrary.ui.crowd.viewmodel;

import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.jndv.jnbaseutils.JNBasePreferenceSaves;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCAddressBookApi;
import com.jndv.jndvchatlibrary.ui.crowd.api.JCInviteGroupNumbersApi;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCNumbersData;
import com.jndv.jndvchatlibrary.ui.crowd.bean.JCRespondBean;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;


import java.util.List;

public class JCCrowdNumbersInviteViewModel extends ViewModel {

    private static final String TAG = "InviteViewModel";
    private MutableLiveData<String> mText;
    private MutableLiveData<JCRespondBean<List<JCNumbersData>>> addressBookData = new MutableLiveData<>();
    private MutableLiveData<JCRespondBean> inviteRespondData = new MutableLiveData<>();
    private ComponentActivity activity;

    public JCCrowdNumbersInviteViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is crowd fragment");
    }

    public void init(ComponentActivity activity) {
        this.activity = activity;
    }

    public MutableLiveData<JCRespondBean<List<JCNumbersData>>> getAddressBookData() {
        return addressBookData;
    }

    public MutableLiveData<JCRespondBean> getInviteRespondData() {
        return inviteRespondData;
    }

    public JCCrowdNumbersInviteViewModel getAddressBook() {
        EasyHttp.get(activity)
                .api(new JCAddressBookApi()
                        .setUserId(JNBasePreferenceSaves.getUserSipId())
                )
                .request(new OnHttpListener<JCRespondBean<List<JCNumbersData>>>() {
                    @Override
                    public void onSucceed(JCRespondBean<List<JCNumbersData>> result) {
                        addressBookData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });

        return this;
    }

    public void inviteFriend(String groupNumbers, String groupId,String groupDomainAddr, String groupDomainAddrUser) {
        JCInviteGroupNumbersApi jcInviteApi = new JCInviteGroupNumbersApi(JNBasePreferenceSaves.getSipAddress()
                , JNBasePreferenceSaves.getJavaAddress(), JNBasePreferenceSaves.getUserSipId()
                , groupNumbers, groupId, groupDomainAddr, groupDomainAddrUser);

        EasyHttp.post(activity)
                .api(jcInviteApi)
                .json(new Gson().toJson(jcInviteApi))
                .request(new OnHttpListener<JCRespondBean>() {
                    @Override
                    public void onSucceed(JCRespondBean result) {
                        inviteRespondData.postValue(result);
                    }

                    @Override
                    public void onFail(Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "onFail: " + e.getMessage());
                    }
                });
    }
}
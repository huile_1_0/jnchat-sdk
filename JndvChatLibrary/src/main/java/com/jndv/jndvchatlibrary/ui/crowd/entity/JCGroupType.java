package com.jndv.jndvchatlibrary.ui.crowd.entity;

public class JCGroupType {

    /**
     *  ORG(PviewGlobalConstants.GROUP_TYPE_DEPARTMENT), CONTACT(
     *             PviewGlobalConstants.GROUP_TYPE_CONTACT), CHATING(
     *             PviewGlobalConstants.GROUP_TYPE_CROWD), CONFERENCE(
     *             PviewGlobalConstants.GROUP_TYPE_CONFERENCE), DISCUSSION(
     *             PviewGlobalConstants.GROUP_TYPE_DISCUSSION), SINGLE_PERSON(
     *             PviewGlobalConstants.GROUP_TYPE_USER), UNKNOWN(-1);
     */
    /**
     * 组的类型：个人
     */
    public static final int GROUP_TYPE_USER = 0;

    /**
     * 组的类型：组织
     */
    public static final int GROUP_TYPE_DEPARTMENT = 1;

    /**
     * 组的类型：好友
     */
    public static final int GROUP_TYPE_CONTACT = 2;

    /**
     * 组的类型：群组
     */
    public static final int GROUP_TYPE_CROWD = 3;

    /**
     * 组的类型：会议
     */
    public static final int GROUP_TYPE_CONFERENCE = 4;

    /**
     * 组的类型：讨论组
     */
    public static final int GROUP_TYPE_DISCUSSION = 5;
    /**
     * 组类型：音频设备列表
     * */
    public static final int GROUP_TYPE_VOICEDEV = 7;
    /**
     * 组的类型：通知：web->as->app
     */
    public static final int GROUP_TYPE_NOTIFICATION = 33;


    public static final int GROUP_TYPE_CROWD_VIDEO=6;


}
package com.jndv.jndvchatlibrary.ui.chat.bean;

import com.jndv.jnbaseutils.chat.JCSessionListBean;

import java.util.List;

/**
 * Author: wangguodong
 * Date: 2022/2/12
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 会话管理工具类，增删改查
 */
public class JCsessionListModelItem {

    private List<JCSessionListBean> datas ;//新的数据列表
    private int page;//
    private int type;//0=成功；1=失败

    public List<JCSessionListBean> getDatas() {
        return datas;
    }

    public void setDatas(List<JCSessionListBean> datas) {
        this.datas = datas;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}

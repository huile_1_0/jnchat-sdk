package com.jndv.jndvchatlibrary.ui.chat.viewholder.collect;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.http.api.JCGetCollectMessageApi;
import com.ehome.manager.utils.JNLogUtil;
import com.jndv.jnbaseutils.chat.JCbaseIMMessage;
import com.jndv.jnbaseutils.chat.JCimMessageBean;
import com.jndv.jndvchatlibrary.ui.chat.bean.content.JCMsgCardContentBean;
import com.jndv.jnbaseutils.chat.JNContentEncryptUtils;
import com.jndv.jndvchatlibrary.utils.JCglideUtils;

/**
 * Author: wangguodong
 * Date: 2022/6/21
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 收藏消息，viewHolder实体
 */
public class JCCollectMsgNameCardViewHolder extends JCCollectMsgBaseViewHolder{
    private TextView tv_user_name;
    private ImageView iv_user_head;

    public JCCollectMsgNameCardViewHolder(Context context, View itemView) {
        super(context, itemView);
    }

    @Override
    protected int getContentResId() {
        return R.layout.jc_msg_viewholder_namecard_collect;
    }

    @Override
    protected void showContentView(JCGetCollectMessageApi.ColloctBean colloctBean) {
        JNLogUtil.e("===JCCollectMsgTextViewHolder===showContentView====");
        tv_user_name = findViewById(R.id.tv_user_name);
        iv_user_head = findViewById(R.id.iv_user_head);
        try {
            JCbaseIMMessage message = new Gson().fromJson(colloctBean.getBody(), JCimMessageBean.class);
//            JCMsgTextContentBean bean = new Gson().fromJson(JCContentEncryptUtils.decryptContent(message.getContent()), JCMsgTextContentBean.class);
            JCMsgCardContentBean bean = new Gson().fromJson(JNContentEncryptUtils.decryptContent(message.getContent()), JCMsgCardContentBean.class);
            JNLogUtil.e("======SendTime=="+message.getSendTime());
            JNLogUtil.e("======ReceiveTime=="+message.getReceiveTime());
            tv_user_name.setText(bean.getName());
//            JCglideUtils.loadImage(context, bean.getHead(), ivHead);
            JCglideUtils.loadCircleImage(mContext, bean.getHead(), iv_user_head);
        }catch (Exception e){
            JNLogUtil.e("==JCCollectMsgTextViewHolder==showContentView==", e);
            tv_user_name.setText("未识别的消息");
        }
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.jndv.jndvchatlibrary.R;
import com.jndv.jndvchatlibrary.databinding.ActivityCrowdContentUpdateBinding;
import com.jndv.jndvchatlibrary.ui.crowd.utils.CommonUtil;
import com.jndv.jndvchatlibrary.ui.crowd.viewmodel.JCCrowdViewModel;
import com.jndv.jndvchatlibrary.utils.JCEntityUtils;

/**
 * 群公告更新页面
 */
public class JCCrowdContentUpdateActivity extends JCBaseHeadActivity {

    private ActivityCrowdContentUpdateBinding updateBinding;
    private JCCrowdViewModel viewModel;
    private boolean isCreator;
    private String groupId;
    private String content;
    private boolean isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateBinding= ActivityCrowdContentUpdateBinding.inflate(getLayoutInflater(), findViewById(R.id.base_content), true);
        initIntent();
        initView();
    }

    private void initIntent() {
        Intent intent=getIntent();
        isCreator=intent.getBooleanExtra("isCreator",false);
        groupId=intent.getStringExtra(JCEntityUtils.CROWD_ID);
        content=intent.getStringExtra("content");
    }

    private void initView(){
//TODO        这里明明是简介的编辑展示页面，却用的发布公告的接口！！！！不知道什么思想，测试提了就先改公告吧。
        setCenterTitle("公告");
        setView(false);
        if (isCreator){
            setTitleRight(getString(R.string.edit),null);
        }

        updateBinding.crowdContentEt.setText(content);

        viewModel=new JCCrowdViewModel();
        viewModel.initData(this);
    }

    private void setView(boolean isEdit){
        updateBinding.crowdContentEt.setEnabled(isEdit);//去掉点击时编辑框下面横线:
        updateBinding.crowdContentEt.setFocusable(isEdit);//不可编辑
        updateBinding.crowdContentEt.setFocusableInTouchMode(isEdit);//不可编辑
    }

    @Override
    protected void onRightClick() {
        super.onRightClick();
        isEdit=!isEdit;
        setView(isEdit);
        if (isEdit){
            setTitleRight(getString(R.string.save),null);
        }else {
            setTitleRight(getString(R.string.edit),null);
            updateContent();
        }
    }

    //更新群公告
    private void updateContent(){
        String contentStr=updateBinding.crowdContentEt.getText().toString();
        if (!contentStr.equals(content)) {
            viewModel.createGroupNotice(groupId, contentStr);
        }else {
            showToast("重复信息");
        }
    }
}

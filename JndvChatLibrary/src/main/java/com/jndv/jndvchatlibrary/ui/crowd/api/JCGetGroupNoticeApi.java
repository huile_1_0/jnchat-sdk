package com.jndv.jndvchatlibrary.ui.crowd.api;

import com.hjq.http.config.IRequestApi;

import java.util.List;


public class JCGetGroupNoticeApi implements IRequestApi {
    @Override
    public String getApi() {
        return "/groupNotice/";
    }


    private String domainAddr;
    private String groupId;

    public JCGetGroupNoticeApi setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
        return this;
    }

    public JCGetGroupNoticeApi setGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    public final static class Bean {

        private String code;
        private String msg;
        private List<Data> data;

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }

        public List<Data> getData() {
            return data;
        }

        public class Data {

            private int id;
            private int userId;
            private int groupId;
            private String content;
            private long createTime;

            public void setId(int id) {
                this.id = id;
            }

            public int getId() {
                return id;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getUserId() {
                return userId;
            }

            public void setGroupId(int groupId) {
                this.groupId = groupId;
            }

            public int getGroupId() {
                return groupId;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getContent() {
                return content;
            }

            public void setCreateTime(long createTime) {
                this.createTime = createTime;
            }

            public long getCreateTime() {
                return createTime;
            }

        }
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.bean;

/**
 * Author: wangguodong
 * Date: 2022/5/27
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: 创建群组中使用的用户列表入参实体类
 */
public class JCCreateGroupUsersBean {
    private String userid;
    private String domainAddr;
    private String domainAddrUser;
    private String nickname;
    private String pic;

    public JCCreateGroupUsersBean(JCNumbersData jcNumbersData) {
        this.userid = jcNumbersData.getFriendId();
        this.domainAddr = jcNumbersData.getDomainAddr();
        this.domainAddrUser = jcNumbersData.getDomainAddrUser();
        this.nickname = jcNumbersData.getFriendName();
        this.pic = jcNumbersData.getFriendPic();
    }
    public JCCreateGroupUsersBean(String userid, String domainAddr, String domainAddrUser, String nickname, String pic) {
        this.userid = userid;
        this.domainAddr = domainAddr;
        this.domainAddrUser = domainAddrUser;
        this.nickname = nickname;
        this.pic = pic;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDomainAddr() {
        return domainAddr;
    }

    public void setDomainAddr(String domainAddr) {
        this.domainAddr = domainAddr;
    }

    public String getDomainAddrUser() {
        return domainAddrUser;
    }

    public void setDomainAddrUser(String domainAddrUser) {
        this.domainAddrUser = domainAddrUser;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}

package com.jndv.jndvchatlibrary.ui.crowd.bean;

/**
 * @Description
 * @Author SunQinzheng
 * @Time 2022/5/9 下午 3:31
 **/
public class JCQrResult {
    private String groupId;
    private String groupDomainAddr;
    private String groupDomainAddrUser;

    public String getGroupDomainAddr() {
        return groupDomainAddr;
    }

    public void setGroupDomainAddr(String groupDomainAddr) {
        this.groupDomainAddr = groupDomainAddr;
    }

    public String getGroupDomainAddrUser() {
        return groupDomainAddrUser;
    }

    public void setGroupDomainAddrUser(String groupDomainAddrUser) {
        this.groupDomainAddrUser = groupDomainAddrUser;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public JCQrResult(String groupId, String groupDomainAddr, String groupDomainAddrUser) {
        this.groupId = groupId;
        this.groupDomainAddr = groupDomainAddr;
        this.groupDomainAddrUser = groupDomainAddrUser;
    }

    @Override
    public String toString() {
        return "JCQrResult{" +
                "groupId=" + groupId +
                ", groupDomainAddr='" + groupDomainAddr + '\'' +
                ", groupDomainAddrUser='" + groupDomainAddrUser + '\'' +
                '}';
    }
}

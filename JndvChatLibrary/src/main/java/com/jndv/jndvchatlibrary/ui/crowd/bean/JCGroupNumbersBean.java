package com.jndv.jndvchatlibrary.ui.crowd.bean;

/**
 * @Description
 * @Author SunQinzheng
 * @Time 2022/5/5 下午 1:06
 **/
public class JCGroupNumbersBean {
    private String domain_addr;
    private int join_way;
    private int join_userid;
    private String time_stamp;
    private int cgm_status;
    private String avatar;
    private int type;
    private String group_nickname;
    private int group_id;
    private String user_id;
    private int is_forbiddenspeak;
    private String nickname;
    private int id;
    private String add_time;
    private String join_domain_addr;

    //新增群成员邀请权限
    private String invite_permission;
    public void setDomain_addr(String domain_addr) {
        this.domain_addr = domain_addr;
    }
    public String getDomain_addr() {
        return domain_addr;
    }

    public void setJoin_way(int join_way) {
        this.join_way = join_way;
    }
    public int getJoin_way() {
        return join_way;
    }

    public void setJoin_userid(int join_userid) {
        this.join_userid = join_userid;
    }
    public int getJoin_userid() {
        return join_userid;
    }

    public void setTime_stamp(String time_stamp) {
        this.time_stamp = time_stamp;
    }
    public String getTime_stamp() {
        return time_stamp;
    }

    public void setCgm_status(int cgm_status) {
        this.cgm_status = cgm_status;
    }
    public int getCgm_status() {
        return cgm_status;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public String getAvatar() {
        return avatar;
    }

    public void setType(int type) {
        this.type = type;
    }
    public int getType() {
        return type;
    }

    public void setGroup_nickname(String group_nickname) {
        this.group_nickname = group_nickname;
    }
    public String getGroup_nickname() {
        return group_nickname;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }
    public int getGroup_id() {
        return group_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getUser_id() {
        return user_id;
    }

    public void setIs_forbiddenspeak(int is_forbiddenspeak) {
        this.is_forbiddenspeak = is_forbiddenspeak;
    }
    public int getIs_forbiddenspeak() {
        return is_forbiddenspeak;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public String getNickname() {
        return nickname;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }
    public String getAdd_time() {
        return add_time;
    }

    public void setJoin_domain_addr(String join_domain_addr) {
        this.join_domain_addr = join_domain_addr;
    }
    public String getJoin_domain_addr() {
        return join_domain_addr;
    }


    public String getInvite_permission() {
        return invite_permission;
    }

    public void setInvite_permission(String invite_permission) {
        this.invite_permission = invite_permission;
    }
}

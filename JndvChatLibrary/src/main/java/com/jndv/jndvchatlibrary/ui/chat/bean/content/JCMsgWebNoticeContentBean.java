package com.jndv.jndvchatlibrary.ui.chat.bean.content;

/**
 * Author: wangguodong
 * Date: 2022/10/8
 * QQ: 1772889689@qq.com
 * WX: gdihh8180
 * Description: web消息中心消息内容
 */
public class JCMsgWebNoticeContentBean {

    private String text ;
    private String type ;
    private String fromName ;//mqtt接收的消息中增加了此字段
    private SipParameterJson sipParameterJson ;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public SipParameterJson getSipParameterJson() {
        return sipParameterJson;
    }

    public void setSipParameterJson(SipParameterJson sipParameterJson) {
        this.sipParameterJson = sipParameterJson;
    }

    public class SipParameterJson{
        private String sjLevel;
        private String userSipId;
        private String userSipType;
        private String jsOrgOrStation;
        private String lon;
        private String remark;
        private String urlMobile;
        private String userContact;
        private String userName;
        private String distributeMethod;
        private String secondType;
        private String displayMode;//展示模式：Popup=弹框
        private String url;
        private String distributeTarget;
        private String bigType;
        private String iD;
        private String bigTypeTitle;
        private String lat;
        private String seq;
        private String smallType;
        private String info;

        public String getSjLevel() {
            return sjLevel;
        }

        public void setSjLevel(String sjLevel) {
            this.sjLevel = sjLevel;
        }

        public String getUserSipId() {
            return userSipId;
        }

        public void setUserSipId(String userSipId) {
            this.userSipId = userSipId;
        }

        public String getUserSipType() {
            return userSipType;
        }

        public void setUserSipType(String userSipType) {
            this.userSipType = userSipType;
        }

        public String getJsOrgOrStation() {
            return jsOrgOrStation;
        }

        public void setJsOrgOrStation(String jsOrgOrStation) {
            this.jsOrgOrStation = jsOrgOrStation;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getUrlMobile() {
            return urlMobile;
        }

        public void setUrlMobile(String urlMobile) {
            this.urlMobile = urlMobile;
        }

        public String getUserContact() {
            return userContact;
        }

        public void setUserContact(String userContact) {
            this.userContact = userContact;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getDistributeMethod() {
            return distributeMethod;
        }

        public void setDistributeMethod(String distributeMethod) {
            this.distributeMethod = distributeMethod;
        }

        public String getSecondType() {
            return secondType;
        }

        public void setSecondType(String secondType) {
            this.secondType = secondType;
        }

        public String getDisplayMode() {
            return displayMode;
        }

        public void setDisplayMode(String displayMode) {
            this.displayMode = displayMode;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getDistributeTarget() {
            return distributeTarget;
        }

        public void setDistributeTarget(String distributeTarget) {
            this.distributeTarget = distributeTarget;
        }

        public String getBigType() {
            return bigType;
        }

        public void setBigType(String bigType) {
            this.bigType = bigType;
        }

        public String getiD() {
            return iD;
        }

        public void setiD(String iD) {
            this.iD = iD;
        }

        public String getBigTypeTitle() {
            return bigTypeTitle;
        }

        public void setBigTypeTitle(String bigTypeTitle) {
            this.bigTypeTitle = bigTypeTitle;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getSeq() {
            return seq;
        }

        public void setSeq(String seq) {
            this.seq = seq;
        }

        public String getSmallType() {
            return smallType;
        }

        public void setSmallType(String smallType) {
            this.smallType = smallType;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }
    }

}

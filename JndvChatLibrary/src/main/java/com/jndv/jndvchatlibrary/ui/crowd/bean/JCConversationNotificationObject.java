package com.jndv.jndvchatlibrary.ui.crowd.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class JCConversationNotificationObject implements Parcelable {

    private int conversationType;
    private long extId;
    private long msgID; // P2PText notificateConversationUpdate
    private boolean isDeleteConversation;

    public JCConversationNotificationObject(Parcel in) {
        conversationType = in.readInt();
        extId = in.readLong();
        msgID = in.readLong();
        int deleteInt = in.readInt();
        if (deleteInt == 1)
            isDeleteConversation = true;
        else
            isDeleteConversation = false;
    }

    public JCConversationNotificationObject(int conversationType, long extId) {
        this(conversationType, extId, false, -1);
    }

    public JCConversationNotificationObject(int conversationType, long extId,
                                            boolean isDeleteConversation) {
        this(conversationType, extId, isDeleteConversation, -1);
    }

    public JCConversationNotificationObject(int conversationType, long extId,
                                            boolean isDeleteConversation, long msgID) {
        super();
        this.conversationType = conversationType;
        this.extId = extId;
        this.isDeleteConversation = isDeleteConversation;
        this.msgID = msgID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel par, int flag) {
        par.writeInt(conversationType);
        par.writeLong(extId);
        par.writeLong(msgID);
        par.writeInt(isDeleteConversation ? 1 : 0);
    }

    public static final Creator<JCConversationNotificationObject> CREATOR = new Creator<JCConversationNotificationObject>() {
        public JCConversationNotificationObject createFromParcel(Parcel in) {
            return new JCConversationNotificationObject(in);
        }

        public JCConversationNotificationObject[] newArray(int size) {
            return new JCConversationNotificationObject[size];
        }
    };

    public int getConversationType() {
        return conversationType;
    }

    public void setConversationType(int conversationType) {
        this.conversationType = conversationType;
    }

    public long getExtId() {
        return extId;
    }

    public void setExtId(long extId) {
        this.extId = extId;
    }

    public long getMsgID() {
        return msgID;
    }

    public void setMsgID(long msgID) {
        this.msgID = msgID;
    }

    public boolean isDeleteConversation() {
        return isDeleteConversation;
    }

    public void setDeleteConversation(boolean isDeleteConversation) {
        this.isDeleteConversation = isDeleteConversation;
    }
}

package com.jndv.jnchatsdk;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.jndv.jnbaseutils.JNBaseUtilManager;
import com.jndv.jnbaseutils.utils.JNMD5Util;
import com.jndv.jndvchatlibrary.JCChatManager;
import com.tencent.mmkv.MMKV;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String pwd= "$2a$10$pQs0KZx/o9nmIW5no4g3SuOrfdTIdD2Z7i1MRJUJfOzJKTce8XRYq";
        String accountName="xcxxcc1";
        String userSip="37132460117003";

        JCChatManager.loginUser(accountName,pwd,MainActivity.this, false, new JCChatManager.LoginListener() {

            /**
             * 登录vpn/sip成功
             */
            @Override
            public void onSuccess() {
                Log.d("zhang","onSuccess...");
            }

            /**
             * 登录vpn/sip失败
             */
            @Override
            public void onFail() {
                Log.d("zhang","onFail...");
            }
        });

        JCChatManager.setAllOperateListener(new JCChatManager.AllOperateListener() {

            /**
             * 可不实现
             * @param jcSessionListBean
             * @return
             */
            @Override
            public boolean chatListOnClickItem(com.jndv.jnbaseutils.chat.JCSessionListBean jcSessionListBean) {
                return false;
            }

            /**
             * 可不实现
             * @param
             * @return
             */
            @Override
            public void outLoginOther() {

            }

            /**按需实现
             * sip连接状态
             * @param state  0=断线；1=在线;
             */
            @Override
            public void sipConnectState(int state) {

            }

            /**按需实现
             * vpn连接状态
             * @param state 0=断线；1=在线
             */
            @Override
            public void vpnConnectState(int state) {

            }

            /**
             * 可不实现
             * @param
             * @return
             */
            @Override
            public void getChatListState(int state) {

            }

            /**
             * 可不实现
             * @param
             * @return
             */
            @Override
            public boolean webShowActivity(String url, String title, boolean ShowTitle) {
                return false;
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
//        JCChatManager.onPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
//        JCChatManager.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        JCChatManager.onDestroy(this);
    }
    //注意:在app退出登录的地方调用 JCChatManager.logOut();
}